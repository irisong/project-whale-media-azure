CREATE INDEX mb_member_idx1 ON mb_member (member_code);
CREATE INDEX mb_member_idx2 ON mb_member (referral_code);

# Date : 2019-11-02 Eng
CREATE INDEX app_smsq_idx1 ON app_smsq (status);
CREATE INDEX app_smsq_idx2 ON app_smsq (datetime_add);

CREATE INDEX ct_crypto_wallet_idx1 ON ct_crypto_wallet (crypto_type, owner_type, last_balance_check_date);
CREATE INDEX ct_crypto_wallet_idx2 ON ct_crypto_wallet (last_balance_check_date);
