#######################################
# Date : 2019-10-16 (OMPLUS-21) Eng
#######################################

update wl_wallet_type_config set coin_token_type='COIN', deposit_transfer_address = null, deposit_transfer_gas_used = null,
    min_deposit_transfer_amount = 100, token_contract_address = null, token_decimal = null
where crypto_type = 'OMNIC';

#######################################
# Date : 2019-10-21 (OMPLUS-21) Eng
#######################################

update ct_crypto_wallet set crypto_type='OMC' where crypto_type='OMNIC';

update wl_wallet_type_config set crypto_type='OMC', gas_used = null where wallet_type=30;
