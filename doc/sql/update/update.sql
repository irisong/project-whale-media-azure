# date: 2019-08-13
ALTER TABLE `wl_wallet_transfer`
MODIFY COLUMN `admin_fee` decimal(65, 20) NOT NULL AFTER `version`,
MODIFY COLUMN `amount` decimal(65, 20) NOT NULL AFTER `admin_fee`,
MODIFY COLUMN `conf_admin_fee` decimal(65, 20) NOT NULL AFTER `amount`,
MODIFY COLUMN `total_amount` decimal(65, 20) NOT NULL AFTER `remark`;


# date: 2019-08-16
update so_rank_order set package_amount = amount where package_amount = 0;

# date: 2019-08-23
UPDATE `mb_member` SET `vip_star_rank`='15' WHERE `vip_star_rank`='5';
UPDATE `mb_member` SET `vip_star_rank`='10' WHERE `vip_star_rank`='4';
UPDATE `mb_member` SET `vip_star_rank`='9' WHERE `vip_star_rank`='3';
UPDATE `mb_member` SET `vip_star_rank`='8' WHERE `vip_star_rank`='2';
UPDATE `mb_member` SET `vip_star_rank`='7' WHERE `vip_star_rank`='1';

# date: 2019-08-28
update mb_member set lock_transfer=false where lock_transfer is null;
update mb_member set lock_withdrawal=false where lock_withdrawal is null;

# date: 2019-08-27
ALTER TABLE `ct_crypto_wallet_trx`
MODIFY COLUMN `trx_fee` decimal(65, 20) NULL AFTER `trx_datetime`;

# date: 2019-09-02
ALTER TABLE wl_wallet_adjust
MODIFY COLUMN `amount` decimal(65, 20) NOT NULL AFTER `adjust_type`;

# date: 2019-09-12
ALTER TABLE `ev_event3y_file`
MODIFY COLUMN `data` longblob NULL AFTER `content_type`;
