#######################################
# Date : 2019-11-01 (OMPLUS-24) Eng
#######################################

ALTER TABLE `omp_omnipay_rebate`
MODIFY COLUMN `merchant_phoneno` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL AFTER `merchant_member_id`;
