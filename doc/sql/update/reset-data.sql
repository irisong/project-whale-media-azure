-- reset level
UPDATE `mb_member` SET `level`='0', `experience` = '0';

-- undo sending gift
DELETE from wl_wallet_trx where trx_type ='LIVE_STREAM_GIFT';

-- update whale coin balance for undo sending gift
UPDATE wl_wallet_balance wallet
INNER JOIN (
 SELECT SUM(in_amt - out_amt) as total,  owner_id
   FROM wl_wallet_trx where wallet_type = '80'
    GROUP BY owner_id
) acc ON wallet.owner_id = acc.owner_id
SET wallet.total_balance = acc.total, wallet.available_balance = acc.total
WHERE wallet.owner_id = acc.owner_id and wallet.owner_type='MEMBER'
and wallet.wallet_type = '80' and wallet.total_balance != acc.total;

-- delete all rewards earn from spending
DELETE from wl_point_wallet_trx;
DELETE from wl_reward_point_trx;
UPDATE wl_wallet_balance set total_balance = 0, available_balance = 0
where wallet_type = 81;

-- delete all baby whale coin VJ received
DELETE from wl_point_trx;
UPDATE wl_wallet_balance set total_balance = 0, available_balance = 0
where wallet_type = 82;

-- delete old live stream data
DELETE from wl_reward_point_trx;
delete from wl_point_wallet_trx;
delete from wl_point_balance;
delete from rk_member_rank_trx where trx_type='SPEND';