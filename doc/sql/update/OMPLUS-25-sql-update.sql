#######################################
# Date : 2019-11-06 (OMPLUS-25) Eng
#######################################

INSERT INTO app_language_res (res_id, add_by, datetime_add, datetime_update, update_by, version, key_code, language_code, res_value) VALUES ('4028804167c0dc8e0167c0df3d870020', '00000000000000000000000000000000', '2018-12-18 18:31:55', '2019-07-12 15:58:30', '00000000000000000000000000000000', 1, 'WALLET_CUR_55', 'zh', 'INCW');
INSERT INTO app_language_res (res_id, add_by, datetime_add, datetime_update, update_by, version, key_code, language_code, res_value) VALUES ('4028804167c0dc8e0167c0df3d870021', '00000000000000000000000000000000', '2018-12-18 18:31:55', '2019-07-12 15:58:30', '00000000000000000000000000000000', 1, 'WALLET_CUR_55', 'en', 'INCW');

INSERT INTO app_language_res (res_id, add_by, datetime_add, datetime_update, update_by, version, key_code, language_code, res_value) VALUES ('4028804167c0dc8e0167c0df3d870022', '00000000000000000000000000000000', '2018-12-18 18:31:55', '2019-07-12 15:58:30', '00000000000000000000000000000000', 1, 'WALLET_CUR_66', 'zh', 'OMNICTOKEN');
INSERT INTO app_language_res (res_id, add_by, datetime_add, datetime_update, update_by, version, key_code, language_code, res_value) VALUES ('4028804167c0dc8e0167c0df3d870023', '00000000000000000000000000000000', '2018-12-18 18:31:55', '2019-07-12 15:58:30', '00000000000000000000000000000000', 1, 'WALLET_CUR_66', 'en', 'OMNICTOKEN');
