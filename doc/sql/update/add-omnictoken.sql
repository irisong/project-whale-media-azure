#######################################
# Date : 2019-10-27 Eng
#######################################
INSERT INTO wl_wallet_type_config (type_id, add_by, datetime_add, datetime_update, update_by, version, coin_token_type, crypto_type, deposit_transfer_address, deposit_transfer_gas_used, gas_used, min_deposit_transfer_amount, owner_type, status, token_contract_address, token_decimal, wallet_type) VALUES ('402880425bebdc30015bebdffc52000d', '00000000000000000000000000000000', '2017-05-09 14:22:57', '2018-03-15 15:52:16', '4028804161fe97160161fe9929020009', 2, 'TOKEN', 'OMNICTOKEN', null, null, null, null, 'MEMBER', 'ACTIVE', null, null, 66);

INSERT INTO app_language_res (res_id, add_by, datetime_add, datetime_update, update_by, version, key_code, language_code, res_value) VALUES ('402880415d35b6d9015d35bebda30008', '00000000000000000000000000000000', '2017-07-12 15:41:19', '2018-02-28 15:03:11', '4028801461b8e2de0161b8f4b22e0021', 1, 'WALLET_66', 'zh', '爱米代币');
INSERT INTO app_language_res (res_id, add_by, datetime_add, datetime_update, update_by, version, key_code, language_code, res_value) VALUES ('402880415d35b6d9015d35bebda30009', '00000000000000000000000000000000', '2017-07-12 15:41:19', '2018-02-28 15:03:11', '4028801461b8e2de0161b8f4b22e0021', 1, 'WALLET_66', 'en', 'Omnic Token');
