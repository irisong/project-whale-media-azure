################################
# LAST UPDATE : ENG (2019-07-09)
################################

# this script will update time to time.

####################
# GHCOIN - START
####################

delete from app_jwt_token;
delete from lg_rest_log;

delete from ct_btc_wallet;
delete from ct_eth_gas_price;
delete from ct_eth_wallet_file;
delete from ct_eth_wallet;

delete from ct_crypto_wallet_trx;
delete from ct_crypto_wallet;
delete from ct_internal_transfer;
delete from ct_process_data;

delete from tr_trade_chart;
delete from tr_trade_chart_day1;
delete from tr_trade_chart_hour1;
delete from tr_trade_chart_min1;
delete from tr_trade_chart_min5;
delete from tr_trade_chart_month1;

delete from tr_trade_order_data;
delete from tr_trade_order;
delete from tr_trade_summary_24h;

delete from app_third_party_member_temp;
delete from app_third_party_member;
delete from app_third_party;

delete from ws_message;
delete from ws_message_broadcast;

####################
# GHCOIN - END
####################

delete from app_announce;
delete from app_currency_exchange;
delete from app_docfile;
delete from app_emailq_file;
delete from app_emailq;
delete from app_help_desk_reply;
delete from app_help_desk;
delete from app_run_task;
delete from app_sys_param;


delete from bp_sales_order_invoice;
delete from bp_topup_invoice;
delete from bp_invoice_status_log;
delete from bp_invoice;


delete from bs_daily_master_bonus;
delete from bs_matching_bonus;
delete from bs_sponsor_bonus;
delete from lg_session_log;


delete from sh_carousel_image;
delete from sh_carousel;
delete from sh_cart_item;
delete from sh_cart;
delete from sh_featured_product;
delete from sh_featured_collection_image;
delete from sh_featured_collection;
delete from mb_member_address;


delete from so_delivery_order_item;
delete from so_delivery_order;
delete from so_sales_order_payment;
delete from so_sales_order_item;
delete from so_sales_order;


delete from pro_product_price where product_id in (
  select product_id from pro_product where product_type!='REGISTER'
);
delete from pro_product_image where product_id in (
  select product_id from pro_product where product_type!='REGISTER'
);
delete from pro_product where product_type!='REGISTER';
delete from pro_product_cat;

delete from wl_wallet_adjust;
delete from wl_wallet_topup;
delete from wl_wallet_transfer;
delete from wl_wallet_trx;
delete from wl_wallet_withdraw;
delete from wl_wallet_balance;

####################
# OMNIC+ - START
####################

delete from app_smsq;
delete from sms_tag;

delete from mb_member_mnemonic where member_id!='00000000000000000000000000000000';

delete from wl_wallet_exchange;
delete from so_rank_order;

####################
# OMNIC+ - END
####################


delete from mb_sponsor_tree where member_id!='00000000000000000000000000000000';
delete from mb_planb_tree where member_id!='00000000000000000000000000000000';
delete from member_user where member_id!='00000000000000000000000000000000';
delete from admin_user where user_id!='00000000000000000000000000000000';
delete from app_user_in_role where user_id not in ('00000000000000000000000000000000', '00000000000000000000000000000001');
delete from app_user where user_id not in ('00000000000000000000000000000000', '00000000000000000000000000000001');
delete from mb_member where member_id!='00000000000000000000000000000000';
delete from mb_member_detail where member_det_id!='00000000000000000000000000000000';

ALTER SEQUENCE mb_sponsor_tree_id_seq RESTART 2;
ALTER SEQUENCE mb_planb_tree_id_seq RESTART 2;
