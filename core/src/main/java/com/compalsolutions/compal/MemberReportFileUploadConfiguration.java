package com.compalsolutions.compal;

public class MemberReportFileUploadConfiguration {
    public static final String BEAN_NAME = "memberReportFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;

    public static final String FOLDER_NAME = "memberReport";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getMemberReportFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullMemberReportParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }
}
