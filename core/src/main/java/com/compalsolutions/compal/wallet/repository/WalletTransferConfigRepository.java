package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletTransferConfig;

public interface WalletTransferConfigRepository extends JpaRepository<WalletTransferConfig, String> {
    public static final String BEAN_NAME = "walletTransferConfigRepository";
}
