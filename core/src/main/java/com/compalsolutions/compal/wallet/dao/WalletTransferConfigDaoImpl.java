package com.compalsolutions.compal.wallet.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.repository.WalletTransferConfigRepository;
import com.compalsolutions.compal.wallet.vo.WalletTransferConfig;

@Component(WalletTransferConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTransferConfigDaoImpl extends Jpa2Dao<WalletTransferConfig, String> implements WalletTransferConfigDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTransferConfigRepository walletTransferConfigRepository;

    public WalletTransferConfigDaoImpl() {
        super(new WalletTransferConfig(false));
    }

    @Override
    public WalletTransferConfig getDefault() {
        return findUnique(new WalletTransferConfig(false));
    }
}
