package com.compalsolutions.compal.wallet.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.repository.WalletTransferTypeConfigRepository;
import com.compalsolutions.compal.wallet.vo.WalletTransferTypeConfig;

@Component(WalletTransferTypeConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTransferTypeConfigDaoImpl extends Jpa2Dao<WalletTransferTypeConfig, String> implements WalletTransferTypeConfigDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTransferTypeConfigRepository walletTransferTypeConfigRepository;

    public WalletTransferTypeConfigDaoImpl() {
        super(new WalletTransferTypeConfig(false));
    }

    @Override
    public List<WalletTransferTypeConfig> findAll() {
        return findByExample(new WalletTransferTypeConfig(false), "seq");
    }

    @Override
    public List<WalletTransferTypeConfig> findActiveMemberWalletTransferTypeConfigs(boolean selfTransfer) {
        WalletTransferTypeConfig example = new WalletTransferTypeConfig();
        example.setStatus(Global.Status.ACTIVE);
        example.setTransferSelf(selfTransfer);
        return findByExample(example, "seq");
    }

    @Override
    public WalletTransferTypeConfig findActive(boolean transferSelf, Integer walletTypeFrom, Integer walletTypeTo) {
        WalletTransferTypeConfig example = new WalletTransferTypeConfig();
        example.setTransferSelf(transferSelf);
        example.setWalletTypeFrom(walletTypeFrom);
        example.setWalletTypeTo(walletTypeTo);
        example.setStatus(Global.Status.ACTIVE);
        return findUnique(example);
    }

    @Override
    public WalletTransferTypeConfig findWalletTransferTypeConfig(boolean transferSelf, Integer walletTypeFrom, Integer walletTypeTo) {
        WalletTransferTypeConfig example = new WalletTransferTypeConfig();
        example.setTransferSelf(transferSelf);
        example.setWalletTypeFrom(walletTypeFrom);
        example.setWalletTypeTo(walletTypeTo);
        return findUnique(example);
    }
}
