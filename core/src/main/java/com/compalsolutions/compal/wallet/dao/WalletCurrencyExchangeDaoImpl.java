package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.vo.WalletCurrencyExchange;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(WalletCurrencyExchangeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletCurrencyExchangeDaoImpl extends Jpa2Dao<WalletCurrencyExchange, String> implements WalletCurrencyExchangeDao {
    public WalletCurrencyExchangeDaoImpl() {
        super(new WalletCurrencyExchange());
    }

    @Override
    public WalletCurrencyExchange findAuthByTrxIdAndUsername(String authId, String username) {
        if (StringUtils.isBlank(authId) || StringUtils.isBlank(username)) {
            throw new ValidatorException("serviceCode is null for ThirdPartyDao.findActiveByServiceCode");
        }

        WalletCurrencyExchange walletCurrencyExchange = new WalletCurrencyExchange();
        walletCurrencyExchange.setTrxId(authId);
        walletCurrencyExchange.setOwnerId(username);

        return findUnique(walletCurrencyExchange);
    }

    @Override
    public WalletCurrencyExchange getWalletCurrencyExchangeTrxValid(String trxId, String username) {
        if (StringUtils.isBlank(trxId) || StringUtils.isBlank(username)) {
            throw new ValidatorException("transId / username can not be null");
        }
        WalletCurrencyExchange walletCurrencyExchange = new WalletCurrencyExchange();
        walletCurrencyExchange.setTrxId(trxId);
        walletCurrencyExchange.setOwnerId(username);

        WalletCurrencyExchange obj = findFirst(walletCurrencyExchange);
//        Same transaction cannot be reused , always retrieved pending transaction to be processed
        if (obj != null && !obj.getStatus().equalsIgnoreCase(Global.Status.PENDING)) {
            obj = null;
        } else if (obj != null && (DateUtil.getDaysDiffBetween2Dates(obj.getDatetimeAdd(), new Date()) > 1)) {
            obj = null;
        }
        return obj;
    }

    @Override
    public void getAllByOnwerIdAndStatus(DatagridModel<WalletCurrencyExchange> datagridModel, String ownerId, String status, String payTo) {
        if (StringUtils.isBlank(ownerId) || StringUtils.isBlank(status)) {
            throw new ValidatorException("ownerId / status can not be null");
        }
        List<Object> params = new ArrayList<>();
        String hql = "FROM w IN " + WalletCurrencyExchange.class + " WHERE " +
                " w.ownerId = ? AND w.status = ? AND w.payTo = ? order by trxDatetime desc";
        params.add(ownerId);
        params.add(status);
        params.add(payTo);

        findForDatagrid(datagridModel, "w", hql, params.toArray());
//        return findQueryAsList(hql, params.toArray());
    }

    public void doWalletCurrencyExchange(String trxId, double rate, BigDecimal amount, BigDecimal amountTo) {
        List<Object> params = new ArrayList<>();
        String hql = "update WalletCurrencyExchange a set a.rate = ?, " //
                + "a.amount = ?, a.amountTo = ?, a.status = ? "
                + " where a.trxId = ?";
        params.add(rate);
        params.add(amount);
        params.add(amountTo);
        params.add(Global.Status.ACTIVE);
        params.add(trxId);
        bulkUpdate(hql, params.toArray());
    }
}
