package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.vo.DailyActiveUserReport;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(DailyActiveUserReportDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DailyActiveUserReportDaoImpl extends Jpa2Dao<DailyActiveUserReport, String> implements DailyActiveUserReportDao {
    public DailyActiveUserReportDaoImpl() {
        super(new DailyActiveUserReport());
    }

}
