package com.compalsolutions.compal.wallet.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.omnicplus.OmnicplusClient;
import com.compalsolutions.compal.omnicplus.WhalePosCoinTrxDto;
import com.compalsolutions.compal.wallet.dao.PointWalletTrxDao;
import com.compalsolutions.compal.wallet.dao.WalletTrxDao;
import com.compalsolutions.compal.wallet.dao.WhalePosCoinTrxDao;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

@Component(WhalePosService.BEAN_NAME)
public class WhalePosServiceImpl implements WhalePosService{
    private static final Object synchronizedObject = new Object();

    private WhalePosCoinTrxDao whalePosCoinTrxDao;
    private WalletTrxDao walletTrxDao;

    @Autowired
    public WhalePosServiceImpl(WhalePosCoinTrxDao whalePosCoinTrxDao, WalletTrxDao walletTrxDao){
        this.whalePosCoinTrxDao = whalePosCoinTrxDao;
        this.walletTrxDao = walletTrxDao;
    }

    @Override
    public void doCreateWhalePosCoinTrxRecords(WhalePosCoinTrxDto whalePosCoinTrxDto){
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        Member member = memberService.findMemberByMemberCode(whalePosCoinTrxDto.getMemberCode());

        if(member == null){
            member = memberService.doCreateSimpleMember(whalePosCoinTrxDto.getMemberCode(), "dummyPassword*123456");
        }

        WhalePosCoinTrx trx = whalePosCoinTrxDao.findWhalePosCoinTrxByWhalePosOriginalTrxId(whalePosCoinTrxDto.getTrxId());

        if(trx == null)
        {
            WhalePosCoinTrx newTrxRecords = new WhalePosCoinTrx(true);

            newTrxRecords.setOmcMemberId(whalePosCoinTrxDto.getOwnerId());
            newTrxRecords.setOmcMemberCode(whalePosCoinTrxDto.getMemberCode());
            newTrxRecords.setMemberId(member.getMemberId());
            newTrxRecords.setTrxDatetime(new Date());
            newTrxRecords.setInAmt30P(whalePosCoinTrxDto.getInAmt30P());
            newTrxRecords.setRate(whalePosCoinTrxDto.getRate());
            newTrxRecords.setStatus(Global.Status.PENDING);
            newTrxRecords.setOmcWalletTrxId(whalePosCoinTrxDto.getTrxId());

            whalePosCoinTrxDao.save(newTrxRecords);
        }
    }

    @Override
    public List<WhalePosCoinTrx> findWhalePosCoinTrxForProcess(Date date, String status){
        return whalePosCoinTrxDao.findWhalePosCoinTrxByDateAndStatus(date, status);
    }

    @Override
    public void doConvertWhalePosCoin2WhaleCoin(WhalePosCoinTrx whalePosCoinTrx){
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        WalletTrx walletTrx_db = walletService.findWalletTrxByWalletRefId(whalePosCoinTrx.getTrxId());

        if(walletTrx_db == null && WhalePosCoinTrx.STATUS_PENDING.equalsIgnoreCase(whalePosCoinTrx.getStatus())){
            BigDecimal amountIn30P = whalePosCoinTrx.getInAmt30P();
            BigDecimal fiat_amountIn30P = amountIn30P.multiply(whalePosCoinTrx.getRate());
            BigDecimal whaleCoinAmt = BDUtil.roundUp6dp(fiat_amountIn30P.multiply(new BigDecimal("4.5")).divide(new BigDecimal("0.035"), 6, RoundingMode.HALF_UP));

            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(whaleCoinAmt);
            walletTrx.setOutAmt(BigDecimal.ZERO);
            walletTrx.setOwnerId(whalePosCoinTrx.getMemberId());
            walletTrx.setOwnerType(Global.UserType.MEMBER);
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.EXCHANGE_WHALEPOSCOIN);
            walletTrx.setWalletRefId(whalePosCoinTrx.getTrxId());
            walletTrx.setWalletRefno("");
            walletTrx.setWalletType(Global.WalletType.WALLET_80);

            String trxDesc = i18n.getText("totalMinedWhalePosCoin30P", amountIn30P , fiat_amountIn30P, whaleCoinAmt);
            String trxDescCn = i18n.getText("totalMinedWhalePosCoin30P", Global.LOCALE_CN, amountIn30P , fiat_amountIn30P, whaleCoinAmt);

            walletTrx.setRemark(trxDesc);

            // using systemLocale
            walletTrx.setTrxDesc(trxDesc);
            walletTrx.setTrxDescCn(trxDescCn);

            walletService.saveWalletTrx(walletTrx);

            walletService.doCreditAvailableWalletBalance(whalePosCoinTrx.getMemberId(), walletTrx.getOwnerType(), Global.WalletType.WALLET_80,
                    walletTrx.getInAmt());
            /*
            notificationConfProvider.addPushNotificationMessage(Global.AccessModule.WHALE_MEDIA,
                    whalePosCoinTrx.getMemberId(), walletTrx.getWalletRefId(), Payload.WALLET_DEPOSIT,
                    WebUtil.getWalletName(Global.LOCALE_CN, Global.WalletType.WALLET_80), null,
                    i18n.getText("totalMinedWhalePosCoin30P", Global.LOCALE_CN, amountIn30P , fiat_amountIn30P, whaleCoinAmt));
             */
            //Prevent Optimistic Lock
            WhalePosCoinTrx coinTrx = whalePosCoinTrxDao.get(whalePosCoinTrx.getTrxId());

            coinTrx.setWalletRefId(walletTrx.getTrxId());
            coinTrx.setStatus(WhalePosCoinTrx.STATUS_COMPLETED);
            whalePosCoinTrxDao.update(coinTrx);
        }
    }
}
