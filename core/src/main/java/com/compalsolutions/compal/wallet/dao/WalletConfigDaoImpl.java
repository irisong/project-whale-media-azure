package com.compalsolutions.compal.wallet.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.repository.WalletConfigRepository;
import com.compalsolutions.compal.wallet.vo.WalletConfig;

@Component(WalletConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletConfigDaoImpl extends Jpa2Dao<WalletConfig, String> implements WalletConfigDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletConfigRepository walletConfigRepository;

    public WalletConfigDaoImpl() {
        super(new WalletConfig(false));
    }

    @Override
    public WalletConfig getDefault() {
        return findUnique(new WalletConfig(false));
    }
}
