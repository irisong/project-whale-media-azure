package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletConfig;

public interface WalletConfigDao extends BasicDao<WalletConfig, String> {
    public static final String BEAN_NAME = "walletConfigDao";

    public WalletConfig getDefault();
}
