package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletTopup;

public interface WalletTopupDao extends BasicDao<WalletTopup, String> {
    public static final String BEAN_NAME = "walletTopupDao";
}
