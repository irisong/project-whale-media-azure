package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletTransferTypeConfig;

public interface WalletTransferTypeConfigRepository extends JpaRepository<WalletTransferTypeConfig, String> {
    public static final String BEAN_NAME = "walletTransferTypeConfigRepository";
}
