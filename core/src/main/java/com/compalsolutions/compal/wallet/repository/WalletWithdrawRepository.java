package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletWithdraw;

public interface WalletWithdrawRepository extends JpaRepository<WalletWithdraw, String> {
    public static final String BEAN_NAME = "walletWithdrawRepository";
}
