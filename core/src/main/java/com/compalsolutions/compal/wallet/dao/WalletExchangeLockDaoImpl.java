package com.compalsolutions.compal.wallet.dao;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.vo.WalletExchangeLock;

@Component(WalletExchangeLockDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletExchangeLockDaoImpl extends Jpa2Dao<WalletExchangeLock, String> implements WalletExchangeLockDao {
    public WalletExchangeLockDaoImpl() {
        super(new WalletExchangeLock(false));
    }

    @Override
    public WalletExchangeLock findLatestActiveWalletExchangeLockByMemberId(String memberId) {
        Validate.notBlank(memberId);

        WalletExchangeLock example = new WalletExchangeLock();
        example.setOwnerId(memberId);
        example.setOwnerType(Global.UserType.MEMBER);
        example.setStatus(Global.Status.ACTIVE);
        return findFirst(example, "trxDatetime desc");
    }
}
