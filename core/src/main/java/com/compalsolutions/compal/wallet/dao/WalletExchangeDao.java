package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletExchange;

public interface WalletExchangeDao extends BasicDao<WalletExchange, String> {
    public static final String BEAN_NAME = "walletExchangeDao";
}
