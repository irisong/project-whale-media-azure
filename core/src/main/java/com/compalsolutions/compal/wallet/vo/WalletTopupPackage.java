package com.compalsolutions.compal.wallet.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import javax.persistence.*;

@Entity
@Table(name = "wl_wallet_topup_package")
@Access(AccessType.FIELD)
public class WalletTopupPackage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    @ToUpperCase
    @ToTrim
    @Column(name = "product_id")
    private String productId;

    @Id
    @Column(name = "package_amount", nullable = false)
    private Integer packageAmount;

    @Id
    @Column(name = "adjust_amount")
    private Integer adjustAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "fiat_currency")
    private String fiatCurrency;

    //how many fiat currency needed for this package
    @Column(name = "fiat_currency_amount")
    private Double fiatCurrencyAmount;

    @Column(name = "usd_price")
    private Double usdPrice;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20)
    private String status;

    public WalletTopupPackage() {
    }

    public WalletTopupPackage(boolean defaultValue) {
        if (defaultValue) {
            status = Global.WalletPackageStatus.DISPLAY;
        }
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public Integer getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(Integer packageAmount) {
        this.packageAmount = packageAmount;
    }

    public String getFiatCurrency() {
        return fiatCurrency;
    }

    public void setFiatCurrency(String fiatCurrency) {
        this.fiatCurrency = fiatCurrency;
    }

    public Double getFiatCurrencyAmount() {
        return fiatCurrencyAmount;
    }

    public void setFiatCurrencyAmount(Double fiatCurrencyAmount) {
        this.fiatCurrencyAmount = fiatCurrencyAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Double getUsdPrice() {
        return usdPrice;
    }

    public void setUsdPrice(Double usdPrice) {
        this.usdPrice = usdPrice;
    }

    public Integer getAdjustAmount() {
        return adjustAmount;
    }

    public void setAdjustAmount(Integer adjustAmount) {
        this.adjustAmount = adjustAmount;
    }
}
