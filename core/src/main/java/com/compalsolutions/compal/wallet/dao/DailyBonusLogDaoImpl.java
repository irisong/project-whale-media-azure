package com.compalsolutions.compal.wallet.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.vo.DailyBonusLog;

@Component(DailyBonusLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DailyBonusLogDaoImpl extends Jpa2Dao<DailyBonusLog, String> implements DailyBonusLogDao {

    public DailyBonusLogDaoImpl() {
        super(new DailyBonusLog(false));
    }

    @Override
    public Date getLastRecordDate(String bonusTypePairing) {
        // Date lastRecordDate = DateUtil.getDate("2019-03-01", "yyyy-MM-dd");
        Date lastRecordDate = DateUtil.addDate(DateUtil.truncateTime(new Date()), -1);

        List<Object> params = new ArrayList<Object>();
        String hql = "select a FROM DailyBonusLog a WHERE bonusType = ? ORDER BY a.bonusDate DESC";

        params.add(bonusTypePairing);
        DailyBonusLog dailyBonusLogDB = findFirst(hql, params.toArray());

        if (dailyBonusLogDB != null) {
            return dailyBonusLogDB.getBonusDate();
        }

        return lastRecordDate;
    }
}
