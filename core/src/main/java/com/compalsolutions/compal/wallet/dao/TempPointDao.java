package com.compalsolutions.compal.wallet.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.TempPoint;

public interface TempPointDao extends BasicDao<TempPoint, String> {
    public static final String BEAN_NAME = "tempPointDao";

    public List<TempPoint> findByTrxDate(Date trxDate);

    public List<TempPoint> findByNotProcessed();
}
