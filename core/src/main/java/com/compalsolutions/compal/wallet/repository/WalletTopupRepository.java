package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletTopup;

public interface WalletTopupRepository extends JpaRepository<WalletTopup, String> {
    public static final String BEAN_NAME = "walletTrxRepository";
}
