package com.compalsolutions.compal.wallet.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.dao.TempPointDao;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;
import com.compalsolutions.compal.wallet.vo.TempPoint;
import com.compalsolutions.compal.wallet.vo.WalletBalance;

@Component(TempPointService.BEAN_NAME)
public class TempPointServiceImpl implements TempPointService {
    private static final Log log = LogFactory.getLog(TempPointServiceImpl.class);

    private TempPointDao tempPointDao;

    @Autowired
    public TempPointServiceImpl(TempPointDao tempPointDao) {
        this.tempPointDao = tempPointDao;
    }

    @Override
    public void saveTempPoint(TempPoint tempPoint) {
        tempPointDao.save(tempPoint);
    }

    @Override
    public void doProcessConvertTempPoint() {
        Locale locale = Application.lookupBean(Locale.class);
        Locale localeCn = Global.LOCALE_CN;
        final String userType = Global.UserType.MEMBER;
        final int rewardPointWalletType = Global.WalletType.WALLET_40;

        MemberService memberService = Application.lookupBean(MemberService.class);
        WalletService walletService = Application.lookupBean(WalletService.class);
        I18n i18n = Application.lookupBean(I18n.class);

        List<TempPoint> tempPoints = tempPointDao.findByNotProcessed();
        log.debug("total records: " + tempPoints.size());

        int count = 0;
        for (TempPoint tempPoint : tempPoints) {
            BigDecimal amount = new BigDecimal(tempPoint.getAmount());
            amount = BDUtil.roundUp4dp(amount);

            count++;
            Member member = memberService.findMemberByMemberCode(tempPoint.getMemberCode());
            if (member == null) {
                log.debug(count + " - not yet register, " + tempPoint.getMemberCode() + " >> amount: " + amount);
                continue;
            } else {
                log.debug(count + " - memberCode: " + tempPoint.getMemberCode() + " >> memberId: " + member.getMemberId() + " >> amount: " + amount);
            }

            // POINT balance
            WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(member.getMemberId(), userType, rewardPointWalletType);

            String remark = i18n.getText("walletTrxForPurchase", locale, new String[] { DateUtil.format(tempPoint.getTrxDate(), "yyyy-MM-dd") });
            String cnRemark = i18n.getText("walletTrxForPurchase", localeCn, new String[] { DateUtil.format(tempPoint.getTrxDate(), "yyyy-MM-dd") });

            PointWalletTrx pointWalletTrx = new PointWalletTrx(true);
            pointWalletTrx.setInAmt(amount);
            pointWalletTrx.setOutAmt(BigDecimal.ZERO);
            pointWalletTrx.setOwnerId(member.getMemberId());
            pointWalletTrx.setOwnerType(userType);
            pointWalletTrx.setTrxDatetime(new Date());
            pointWalletTrx.setTrxType(Global.WalletTrxType.REBATE);
            pointWalletTrx.setTrxDesc(remark);
            pointWalletTrx.setTrxDescCn(cnRemark);
            pointWalletTrx.setWalletRefId("");
            pointWalletTrx.setWalletRefno("");
            pointWalletTrx.setWalletType(rewardPointWalletType);

            walletService.savePointWalletTrx(pointWalletTrx);

            BigDecimal oldBalance = walletBalance.getAvailableBalance();
            BigDecimal newBalance = walletBalance.getAvailableBalance().add(amount);
            log.debug(">> balance: " + oldBalance + " -->> " + newBalance);

            walletService.doCreditAvailableWalletBalance(member.getMemberId(), userType, rewardPointWalletType, amount);

            tempPoint.setProcessed(true);
            tempPointDao.update(tempPoint);
        }
        tempPoints.clear();
        tempPoints = null;
    }
}
