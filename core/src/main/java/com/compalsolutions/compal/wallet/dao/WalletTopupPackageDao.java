package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletTopupPackage;

import java.util.List;

public interface WalletTopupPackageDao extends BasicDao<WalletTopupPackage, String> {
    public static final String BEAN_NAME = "WalletTopupPackageDao";

    public void findWalletTopupPackagesForDatagrid(DatagridModel<WalletTopupPackage> datagridModel, Integer walletType, Integer packageAmount, String status);

    public List<WalletTopupPackage> findWalletTopupPackages(String status);

    public WalletTopupPackage findWalletTopupPackage(Integer walletType, Integer packageAmount);

    public WalletTopupPackage findOneWalletTopupPackage(Integer walletType, Integer packageAmount, String status);

    public WalletTopupPackage findOneWalletTopupPackageByProdutId(String productId);
}
