package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_point_wallet_trx")
@Access(AccessType.FIELD)
public class PointWalletTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "point_trx_id", unique = true, nullable = false, length = 32)
    private String pointTrxId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    /**
     * Reference No. Eg: Wallet Top-up, save Top-up transaction no. This is store business key. This field needs to
     * display at wallet transaction history.
     */
    @ToTrim
    @Column(name = "wallet_refno", length = 100)
    private String walletRefno;

    /**
     * Reference Id. A database table primary key. Eg: Wallet Top-up id, member id if registered a new member
     */
    @Column(name = "wallet_ref_id", length = 32)
    private String walletRefId;

    @ToUpperCase
    @ToTrim
    @Column(name = "trx_type", length = 50, nullable = false)
    private String trxType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "in_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal inAmt;

    @Column(name = "out_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal outAmt;

    /**
     * description shown at wallet statement.
     */
    @ToTrim
    @Column(name = "trx_desc")
    private String trxDesc;

    @ToTrim
    @Column(name = "trx_desc_cn")
    private String trxDescCn;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Transient
    private BigDecimal balance;

    public PointWalletTrx() {
    }

    public PointWalletTrx(boolean defaultValue) {
        if (defaultValue) {
            inAmt = BigDecimal.ZERO;
            outAmt = BigDecimal.ZERO;
        }
    }

    public String getPointTrxId() {
        return pointTrxId;
    }

    public void setPointTrxId(String pointTrxId) {
        this.pointTrxId = pointTrxId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getWalletRefno() {
        return walletRefno;
    }

    public void setWalletRefno(String walletRefno) {
        this.walletRefno = walletRefno;
    }

    public String getWalletRefId() {
        return walletRefId;
    }

    public void setWalletRefId(String walletRefId) {
        this.walletRefId = walletRefId;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public BigDecimal getInAmt() {
        return inAmt;
    }

    public void setInAmt(BigDecimal inAmt) {
        this.inAmt = inAmt;
    }

    public BigDecimal getOutAmt() {
        return outAmt;
    }

    public void setOutAmt(BigDecimal outAmt) {
        this.outAmt = outAmt;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTrxDesc() {
        return trxDesc;
    }

    public void setTrxDesc(String trxDesc) {
        this.trxDesc = trxDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PointWalletTrx walletTrx = (PointWalletTrx) o;

        if (pointTrxId != null ? !pointTrxId.equals(walletTrx.pointTrxId) : walletTrx.pointTrxId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return pointTrxId != null ? pointTrxId.hashCode() : 0;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public String getTrxDescCn() {
        return trxDescCn;
    }

    public void setTrxDescCn(String trxDescCn) {
        this.trxDescCn = trxDescCn;
    }
}
