package com.compalsolutions.compal.wallet.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.repository.WalletTransferRepository;
import com.compalsolutions.compal.wallet.vo.WalletTransfer;

@Component(WalletTransferDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTransferDaoImpl extends Jpa2Dao<WalletTransfer, String> implements WalletTransferDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTransferRepository walletTransferRepository;

    public WalletTransferDaoImpl() {
        super(new WalletTransfer(false));
    }
}
