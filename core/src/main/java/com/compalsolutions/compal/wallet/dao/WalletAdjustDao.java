package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletAdjust;

import java.util.Date;

public interface WalletAdjustDao extends BasicDao<WalletAdjust, String> {
    public static final String BEAN_NAME = "walletAdjustDao";

    public void findMemberWalletAdjustsForDatagrid(DatagridModel<WalletAdjust> datagridModel, String docno, String memberCode, String adjustType,
                                                   Integer walletType, Date dateFrom, Date dateTo);
}
