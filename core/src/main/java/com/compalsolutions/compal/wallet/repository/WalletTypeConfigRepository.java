package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

public interface WalletTypeConfigRepository extends JpaRepository<WalletTypeConfig, String> {
    public static final String BEAN_NAME = "walletTypeConfigRepository";
}
