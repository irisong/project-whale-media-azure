package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletTrx;

public interface WalletTrxRepository extends JpaRepository<WalletTrx, String> {
    public static final String BEAN_NAME = "walletTrxRepository";
}
