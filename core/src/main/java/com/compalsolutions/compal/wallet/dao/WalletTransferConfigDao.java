package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletTransferConfig;

public interface WalletTransferConfigDao extends BasicDao<WalletTransferConfig, String> {
    public static final String BEAN_NAME = "walletTransferConfigDao";

    public WalletTransferConfig getDefault();
}
