package com.compalsolutions.compal.wallet.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.vo.WalletExchange;

@Component(WalletExchangeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletExchangeDaoImpl extends Jpa2Dao<WalletExchange, String> implements WalletExchangeDao {
    public WalletExchangeDaoImpl() {
        super(new WalletExchange(false));
    }
}
