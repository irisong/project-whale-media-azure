package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;

import java.util.Date;
import java.util.List;

public interface WhalePosCoinTrxDao extends BasicDao<WhalePosCoinTrx, String> {
    public static final String BEAN_NAME = "whalePosCoinTrxDao";

    public List<WhalePosCoinTrx> findWhalePosCoinTrxByDateAndStatus (Date date, String status);

    public WhalePosCoinTrx findWhalePosCoinTrxByWhalePosOriginalTrxId (String trxId);
}
