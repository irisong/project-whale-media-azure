package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component(WhalePosCoinTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WhalePosCoinTrxDaoImpl extends Jpa2Dao<WhalePosCoinTrx, String> implements WhalePosCoinTrxDao {
    public WhalePosCoinTrxDaoImpl(){super(new WhalePosCoinTrx(false));}

    @Override
    public List<WhalePosCoinTrx> findWhalePosCoinTrxByDateAndStatus (Date date, String status){
        Validate.notBlank(status);
        if(date == null){
            throw new ValidatorException("Invalid Date");
        }

        WhalePosCoinTrx example = new WhalePosCoinTrx(false);

        example.addDateFromCondition("trxDatetime", DateUtil.truncateTime(date));
        example.setStatus(status);

        return findByExample(example);
    }

    @Override
    public WhalePosCoinTrx findWhalePosCoinTrxByWhalePosOriginalTrxId (String trxId){
        Validate.notBlank(trxId);

        WhalePosCoinTrx example = new WhalePosCoinTrx(false);
        example.setOmcWalletTrxId(trxId);

        return findFirst(example);
    }
}
