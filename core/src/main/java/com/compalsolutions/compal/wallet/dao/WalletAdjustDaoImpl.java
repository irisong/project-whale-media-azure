package com.compalsolutions.compal.wallet.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.repository.WalletAdjustRepository;
import com.compalsolutions.compal.wallet.vo.WalletAdjust;

@Component(WalletAdjustDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletAdjustDaoImpl extends Jpa2Dao<WalletAdjust, String> implements WalletAdjustDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletAdjustRepository walletAdjustRepository;

    public WalletAdjustDaoImpl() {
        super(new WalletAdjust(false));
    }

    @Override
    public void findMemberWalletAdjustsForDatagrid(DatagridModel<WalletAdjust> datagridModel, String docno, String memberCode, String adjustType,
                                                   Integer walletType, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();

        String hql = "SELECT wa FROM WalletAdjust wa LEFT JOIN Member m ON wa.ownerId=m.memberId AND wa.ownerType=? WHERE 1=1 ";

        params.add(Global.UserType.MEMBER);

        if (StringUtils.isNotBlank(docno)) {
            hql += " and wa.docno like ? ";
            params.add(docno + "%");
        }

        if (StringUtils.isNotBlank(memberCode)) {
            hql += " and m.memberCode like ? ";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(adjustType)) {
            hql += " and wa.adjustType = ? ";
            params.add(adjustType);
        }

        if (walletType != null) {
            hql += " and wa.walletType = ? ";
            params.add(walletType);
        }

        if (dateFrom != null) {
            hql += " and wa.trxDatetime >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and wa.trxDatetime <= ?";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "wa", hql, params.toArray());
    }
}
