package com.compalsolutions.compal.wallet.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DepositHistory {
    private String walletTrxId;

    private String cryptoWalletTrxId;

    private String txHash;

    private String cryptoType;

    private Date trxDatetime;

    private BigDecimal amount;

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getCryptoWalletTrxId() {
        return cryptoWalletTrxId;
    }

    public void setCryptoWalletTrxId(String cryptoWalletTrxId) {
        this.cryptoWalletTrxId = cryptoWalletTrxId;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
