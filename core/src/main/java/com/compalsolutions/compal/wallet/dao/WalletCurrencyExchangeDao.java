package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletCurrencyExchange;

import java.math.BigDecimal;
import java.util.List;

public interface WalletCurrencyExchangeDao extends BasicDao<WalletCurrencyExchange, String> {
    public static final String BEAN_NAME = "walletExchangeCurrencyDao";

    public WalletCurrencyExchange findAuthByTrxIdAndUsername(String trxId, String username);

    public void getAllByOnwerIdAndStatus(DatagridModel<WalletCurrencyExchange> datagridModel, String ownerId, String status, String payTo);

    public WalletCurrencyExchange getWalletCurrencyExchangeTrxValid(String trxId, String username);

    public void doWalletCurrencyExchange(String trxId, double rate, BigDecimal amount, BigDecimal amountTo);
}
