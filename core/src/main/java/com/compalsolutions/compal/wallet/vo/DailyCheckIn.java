package com.compalsolutions.compal.wallet.vo;

import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "daily_check_in")
@Access(AccessType.FIELD)
public class DailyCheckIn extends VoBase {
    private static final long serialVersionUID = 1L;
    public static final BigDecimal DAILY = new BigDecimal(0);
    public static final BigDecimal LAST_DAILY = new BigDecimal(10);
    public static final boolean CONTINUE_CHECK_IN = true;


    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "description", nullable = false, length = 32)
    private String description;

    @Column(name = "gain_amount", nullable = false)
    private Integer gainAmount;

    @Column(name = "complete", nullable = false)
    private Integer complete;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "gain_date")
    private Date gainDate;

    public DailyCheckIn() {
    }

    public DailyCheckIn(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getGainAmount() {
        return gainAmount;
    }

    public void setGainAmount(Integer gainAmount) {
        this.gainAmount = gainAmount;
    }

    public Integer getComplete() {
        return complete;
    }

    public void setComplete(Integer complete) {
        this.complete = complete;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getGainDate() {
        return gainDate;
    }

    public void setGainDate(Date gainDate) {
        this.gainDate = gainDate;
    }
}
