package com.compalsolutions.compal.wallet.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_topup")
@Access(AccessType.FIELD)
public class WalletTopup {
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "topup_id", unique = true, nullable = false, length = 32)
    private String topupId;

    @ToUpperCase
    @ToTrim
    @Column(name = "docno", nullable = false, length = 20)
    private String docno;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Column(name = "wallet_trx_id", length = 32)
    private String walletTrxId;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Column(name = "crytocurrency_type", length = 100)
    private String crytocurrencyType;

    @Column(name = "crytocurrency_amount", length = 100)
    private String crytocurrencyAmount;

    @Column(name = "crytocurrency_rate", length = 100)
    private String crytocurrencyRate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Transient
    private String ownerCode;

    @Transient
    private String ownerName;

    public WalletTopup() {
    }

    public WalletTopup(boolean defaultValue) {
        if (defaultValue) {
            ownerType = Global.UserType.MEMBER;
            status = Global.Status.PAYMENT_PENDING;
        }
    }

    public String getTopupId() {
        return topupId;
    }

    public void setTopupId(String topupId) {
        this.topupId = topupId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getCrytocurrencyType() {
        return crytocurrencyType;
    }

    public void setCrytocurrencyType(String crytocurrencyType) {
        this.crytocurrencyType = crytocurrencyType;
    }

    public String getCrytocurrencyAmount() {
        return crytocurrencyAmount;
    }

    public void setCrytocurrencyAmount(String crytocurrencyAmount) {
        this.crytocurrencyAmount = crytocurrencyAmount;
    }

    public String getCrytocurrencyRate() {
        return crytocurrencyRate;
    }

    public void setCrytocurrencyRate(String crytocurrencyRate) {
        this.crytocurrencyRate = crytocurrencyRate;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerCode() {
        return ownerCode;
    }

    public void setOwnerCode(String ownerCode) {
        this.ownerCode = ownerCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTopup that = (WalletTopup) o;

        if (topupId != null ? !topupId.equals(that.topupId) : that.topupId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return topupId != null ? topupId.hashCode() : 0;
    }
}
