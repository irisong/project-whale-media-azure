package com.compalsolutions.compal.wallet.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "wl_wallet_whale_pos_coin_trx")
@Access(AccessType.FIELD)
public class WhalePosCoinTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    public static String STATUS_PENDING = "PENDING";
    public static String STATUS_COMPLETED = "COMPLETED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "trx_id", unique = true, nullable = false, length = 32)
    private String trxId;

    @Column(name = "omc_member_id", length = 32, nullable = false)
    private String omcMemberId;

    @Column(name = "omc_member_code", length = 32, nullable = false)
    private String omcMemberCode;

    @Column(name = "member_id", length = 32, nullable = false)
    private String memberId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    //30% amount of whale pos coin received from whale pos machine and to be given to whale live
    @Column(name = "in_amt_30P", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal inAmt30P;

    @Column(name = "rate", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal rate;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Column(name = "wallet_ref_id", length = 32)
    private String walletRefId;

    @Column(name = "omc_wallet_trx_id", length = 32, nullable = false)
    private String omcWalletTrxId;

    public WhalePosCoinTrx() {
    }

    public WhalePosCoinTrx(boolean defaultValue) {
        if (defaultValue) {
            inAmt30P = BigDecimal.ZERO;
            rate = BigDecimal.ZERO;
            status = STATUS_PENDING;
        }
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getInAmt30P() {
        return inAmt30P;
    }

    public void setInAmt30P(BigDecimal inAmt30P) {
        this.inAmt30P = inAmt30P;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOmcMemberId() {
        return omcMemberId;
    }

    public void setOmcMemberId(String omcMemberId) {
        this.omcMemberId = omcMemberId;
    }

    public String getOmcMemberCode() {
        return omcMemberCode;
    }

    public void setOmcMemberCode(String omcMemberCode) {
        this.omcMemberCode = omcMemberCode;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getWalletRefId() {
        return walletRefId;
    }

    public void setWalletRefId(String walletRefId) {
        this.walletRefId = walletRefId;
    }

    public String getOmcWalletTrxId() {
        return omcWalletTrxId;
    }

    public void setOmcWalletTrxId(String omcWalletTrxId) {
        this.omcWalletTrxId = omcWalletTrxId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WhalePosCoinTrx walletTrx = (WhalePosCoinTrx) o;

        if (trxId != null ? !trxId.equals(walletTrx.trxId) : walletTrx.trxId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return trxId != null ? trxId.hashCode() : 0;
    }
}
