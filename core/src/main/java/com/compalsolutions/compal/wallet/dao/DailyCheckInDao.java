package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.DailyBonusLog;
import com.compalsolutions.compal.wallet.vo.DailyCheckIn;

import java.util.Date;

public interface DailyCheckInDao extends BasicDao<DailyCheckIn, String> {
    public static final String BEAN_NAME = "dailyCheckInDao";

}
