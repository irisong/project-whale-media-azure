package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.vo.DailyCheckIn;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(DailyCheckInDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DailyCheckInDaoImpl extends Jpa2Dao<DailyCheckIn, String> implements DailyCheckInDao {

    public DailyCheckInDaoImpl() {
        super(new DailyCheckIn(false));
    }

}
