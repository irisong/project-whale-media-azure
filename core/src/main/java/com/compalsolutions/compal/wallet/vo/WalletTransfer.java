package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_transfer")
@Access(AccessType.FIELD)
public class WalletTransfer extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "transfer_id", unique = true, nullable = false, length = 32)
    private String transferId;

    @ToUpperCase
    @ToTrim
    @Column(name = "docno", nullable = false, length = 20)
    private String docno;

    @Column(name = "transfer_self", nullable = false)
    private Boolean transferSelf;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "receiver_id", length = 32, nullable = false)
    private String receiverId;

    @ToUpperCase
    @ToTrim
    @Column(name = "receiver_type", length = 10, nullable = false)
    private String receiverType;

    @Column(name = "wallet_type_from", nullable = false)
    private Integer walletTypeFrom;

    @Column(name = "wallet_type_to", nullable = false)
    private Integer walletTypeTo;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amount;

    @Column(name = "admin_fee", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal adminFee;

    @Column(name = "total_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "wallet_trx_id_from", length = 32)
    private String walletTrxIdFrom;

    @Column(name = "wallet_trx_id_to", length = 32)
    private String walletTrxIdTo;

    @Column(name = "conf_admin_fee_by_amount", nullable = false)
    private Boolean confAdminFeeByAmount;

    @Column(name = "conf_admin_fee", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal confAdminFee;

    public WalletTransfer() {
    }

    public WalletTransfer(boolean defaultValue) {
        if (defaultValue) {
            confAdminFeeByAmount = true;
            confAdminFee = BigDecimal.ZERO;
            ownerType = Global.UserType.MEMBER;
            receiverType = Global.UserType.MEMBER;
        }
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverType() {
        return receiverType;
    }

    public void setReceiverType(String receiverType) {
        this.receiverType = receiverType;
    }

    public Integer getWalletTypeFrom() {
        return walletTypeFrom;
    }

    public void setWalletTypeFrom(Integer walletTypeFrom) {
        this.walletTypeFrom = walletTypeFrom;
    }

    public Integer getWalletTypeTo() {
        return walletTypeTo;
    }

    public void setWalletTypeTo(Integer walletTypeTo) {
        this.walletTypeTo = walletTypeTo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTransfer that = (WalletTransfer) o;

        return transferId != null ? transferId.equals(that.transferId) : that.transferId == null;
    }

    @Override
    public int hashCode() {
        return transferId != null ? transferId.hashCode() : 0;
    }

    public BigDecimal getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(BigDecimal adminFee) {
        this.adminFee = adminFee;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getWalletTrxIdFrom() {
        return walletTrxIdFrom;
    }

    public void setWalletTrxIdFrom(String walletTrxIdFrom) {
        this.walletTrxIdFrom = walletTrxIdFrom;
    }

    public String getWalletTrxIdTo() {
        return walletTrxIdTo;
    }

    public void setWalletTrxIdTo(String walletTrxIdTo) {
        this.walletTrxIdTo = walletTrxIdTo;
    }

    public Boolean getTransferSelf() {
        return transferSelf;
    }

    public void setTransferSelf(Boolean transferSelf) {
        this.transferSelf = transferSelf;
    }

    public Boolean getConfAdminFeeByAmount() {
        return confAdminFeeByAmount;
    }

    public void setConfAdminFeeByAmount(Boolean confAdminFeeByAmount) {
        this.confAdminFeeByAmount = confAdminFeeByAmount;
    }

    public BigDecimal getConfAdminFee() {
        return confAdminFee;
    }

    public void setConfAdminFee(BigDecimal confAdminFee) {
        this.confAdminFee = confAdminFee;
    }
}
