package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.DailyActiveUserReport;

public interface DailyActiveUserReportDao extends BasicDao<DailyActiveUserReport, String> {
    public static final String BEAN_NAME = "dailyActiveUserReportDao";

}
