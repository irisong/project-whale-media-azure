package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_exchange")
@Access(AccessType.FIELD)
public class WalletExchange extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "exchange_id", unique = true, nullable = false, length = 32)
    private String exchangeId;

    @ToUpperCase
    @ToTrim
    @Column(name = "docno", nullable = false, length = 20)
    private String docno;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type_from", nullable = false)
    private Integer walletTypeFrom;

    @Column(name = "wallet_type_to", nullable = false)
    private Integer walletTypeTo;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    /**
     * 8 decimal, rate in cryptoTypeTo
     */
    @Column(name = "rate", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal rate;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amount;

    @Column(name = "amount_to", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amountTo;

    @Column(name = "admin_fee", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal adminFee;

    @Column(name = "total_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "wallet_trx_id_from", length = 32)
    private String walletTrxIdFrom;

    @Column(name = "wallet_trx_id_to", length = 32)
    private String walletTrxIdTo;

    @Column(name = "reject_wallet_trx_id", length = 32)
    private String rejectWalletTrxId;

    @Column(name = "process_by", length = 32)
    private String processBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "process_datetime")
    private Date processDatetime;

    @ToTrim
    @Column(name = "reject_remark", columnDefinition = Global.ColumnDef.TEXT)
    private String rejectRemark;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Transient
    private String ownerCode;

    @Transient
    private String ownerName;

    @Transient
    private String statusDesc;

    @Transient
    private String cryptoTypeFrom;

    @Transient
    private String cryptoTypeTo;

    public WalletExchange() {
    }

    public WalletExchange(boolean defaultValue) {
        if (defaultValue) {
            trxDatetime = new Date();
            status = Global.Status.APPROVED;
        }
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletTypeFrom() {
        return walletTypeFrom;
    }

    public void setWalletTypeFrom(Integer walletTypeFrom) {
        this.walletTypeFrom = walletTypeFrom;
    }

    public Integer getWalletTypeTo() {
        return walletTypeTo;
    }

    public void setWalletTypeTo(Integer walletTypeTo) {
        this.walletTypeTo = walletTypeTo;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(BigDecimal adminFee) {
        this.adminFee = adminFee;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getWalletTrxIdFrom() {
        return walletTrxIdFrom;
    }

    public void setWalletTrxIdFrom(String walletTrxIdFrom) {
        this.walletTrxIdFrom = walletTrxIdFrom;
    }

    public String getWalletTrxIdTo() {
        return walletTrxIdTo;
    }

    public void setWalletTrxIdTo(String walletTrxIdTo) {
        this.walletTrxIdTo = walletTrxIdTo;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getAmountTo() {
        return amountTo;
    }

    public void setAmountTo(BigDecimal amountTo) {
        this.amountTo = amountTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        WalletExchange that = (WalletExchange) o;
        return Objects.equals(exchangeId, that.exchangeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(exchangeId);
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOwnerCode() {
        return ownerCode;
    }

    public void setOwnerCode(String ownerCode) {
        this.ownerCode = ownerCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getProcessBy() {
        return processBy;
    }

    public void setProcessBy(String processBy) {
        this.processBy = processBy;
    }

    public Date getProcessDatetime() {
        return processDatetime;
    }

    public void setProcessDatetime(Date processDatetime) {
        this.processDatetime = processDatetime;
    }

    public String getRejectRemark() {
        return rejectRemark;
    }

    public void setRejectRemark(String rejectRemark) {
        this.rejectRemark = rejectRemark;
    }

    public String getRejectWalletTrxId() {
        return rejectWalletTrxId;
    }

    public void setRejectWalletTrxId(String rejectWalletTrxId) {
        this.rejectWalletTrxId = rejectWalletTrxId;
    }

    public String getCryptoTypeFrom() {
        return cryptoTypeFrom;
    }

    public void setCryptoTypeFrom(String cryptoTypeFrom) {
        this.cryptoTypeFrom = cryptoTypeFrom;
    }

    public String getCryptoTypeTo() {
        return cryptoTypeTo;
    }

    public void setCryptoTypeTo(String cryptoTypeTo) {
        this.cryptoTypeTo = cryptoTypeTo;
    }
}
