package com.compalsolutions.compal.wallet.service;

import com.compalsolutions.compal.omnicplus.WhalePosCoinTrxDto;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;

import java.util.Date;
import java.util.List;

public interface WhalePosService {
    public static final String BEAN_NAME = "whalePosService";

    public void doCreateWhalePosCoinTrxRecords(WhalePosCoinTrxDto whalePosCoinTrxDto);

    public List<WhalePosCoinTrx> findWhalePosCoinTrxForProcess(Date date, String status);

    public void doConvertWhalePosCoin2WhaleCoin(WhalePosCoinTrx whalePosCoinTrx);
}
