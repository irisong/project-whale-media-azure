package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletAdjust;

public interface WalletAdjustRepository extends JpaRepository<WalletAdjust, String> {
    public static final String BEAN_NAME = "walletAdjustRepository";
}
