package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletTransfer;

public interface WalletTransferDao extends BasicDao<WalletTransfer, String> {
    public static final String BEAN_NAME = "walletTransferDao";
}
