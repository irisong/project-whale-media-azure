package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletConfig;

public interface WalletConfigRepository extends JpaRepository<WalletConfig, String> {
    public static final String BEAN_NAME = "walletConfigRepository";
}
