package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.wallet.vo.WalletBalance;

@Component(WalletBalanceDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletBalanceDaoImpl extends Jpa2Dao<WalletBalance, String> implements WalletBalanceDao {

    public WalletBalanceDaoImpl() {
        super(new WalletBalance(false));
    }

    @Override
    public WalletBalance findByOwnerIdAndOwnerTypeAndWalletType(String ownerId, String ownerType, int walletType) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be blank for WalletBalanceDao.findByOwnerIdAndOwnerTypeAndWalletType");
        }

        if (StringUtils.isBlank(ownerType)) {
            throw new ValidatorException("ownerType can not be blank for WalletBalanceDao.findByOwnerIdAndOwnerTypeAndWalletType");
        }

        WalletBalance example = new WalletBalance(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setWalletType(walletType);

        return findFirst(example);
    }

    @Override
    public void doCreditAvailableBalance(String ownerId, String ownerType, int walletType, BigDecimal amount) {
        List<Object> params = new ArrayList<>();
        String hql = "update WalletBalance a set a.availableBalance = a.availableBalance + ?, "
                + "a.totalBalance = a.availableBalance - a.inOrderBalance, datetimeUpdate = now() "
                + "where a.ownerId = ? AND a.ownerType = ? AND a.walletType = ? ";

        params.add(amount);
        params.add(ownerId);
        params.add(ownerType);
        params.add(walletType);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDebitAvailableBalance(String ownerId, String ownerType, int walletType, BigDecimal amount) {
        List<Object> params = new ArrayList<>();
        String hql = "update WalletBalance a set a.availableBalance = a.availableBalance - ?, "
                + "a.totalBalance = a.availableBalance - a.inOrderBalance, datetimeUpdate = now() "
                + "where a.ownerId = ? AND a.ownerType = ? AND a.walletType = ? ";

        params.add(amount);
        params.add(ownerId);
        params.add(ownerType);
        params.add(walletType);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<WalletBalance> findRewardPointWalletPendingForRebate() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM WalletBalance WHERE ownerType = ? AND walletType = ? AND availableBalance > 0";

        params.add(Global.UserType.MEMBER);
        params.add(Global.WalletType.WALLET_40);

        return findQueryAsList(hql, params.toArray());
    }
}
