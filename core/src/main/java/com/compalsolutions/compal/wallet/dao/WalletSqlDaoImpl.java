package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.compalsolutions.compal.wallet.dto.ActiveUserReport;
import com.compalsolutions.compal.wallet.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.jdbc.SingleRowMapper;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.dto.DepositHistory;

@Component(WalletSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletSqlDaoImpl extends AbstractJdbcDao implements WalletSqlDao {

    @Override
    public BigDecimal getDeductBalanceForWalletStatement(String ownerId, String ownerType, int walletType, Date dateFrom, Date dateTo, int pageNo,
                                                         int pageSize) {
        List<Object> params = new ArrayList<>();
        String sql = "select in_amt, out_amt from wl_wallet_trx where owner_id=? and owner_type=? and wallet_type=? ";
        params.add(ownerId);
        params.add(ownerType);
        params.add(walletType);

        if (dateFrom != null) {
            sql += " and trx_datetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and trx_datetime <=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql += " order by trx_datetime desc, trx_id";

        // convert to pagination depends on Database Vendor
        sql = convertToPaginationSql(sql, 0, (pageNo - 1) * pageSize);

        String finalSql = "select sum(in_amt-out_amt) from (\n " + sql + "\n) a ";

        BigDecimal amt = (BigDecimal) queryForObject(finalSql, BigDecimal.class, params.toArray());
        if (amt != null)
            return amt;

        return BigDecimal.ZERO;
    }

    @Override
    public void findWalletWithdrawsForDatagrid(DatagridModel<WalletWithdraw> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
                                               Date processDateFrom, Date processDateTo, String withdrawType, String cryptoType, String status) {
        List<Object> params = new ArrayList<>();

        String sql = "select ww.*, m.member_code, m.full_name\n" + //
                "from wl_wallet_withdraw ww left join mb_member m on ww.owner_id = m.member_id and ww.owner_type=?\n" + //
                "where 1=1 ";

        params.add(Global.UserType.MEMBER);

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " and m.member_code like ?";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(docno)) {
            sql += " and ww.docno like ?";
            params.add(docno + "%");
        }

        if (dateFrom != null) {
            sql += " and ww.trx_datetime >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and ww.trx_datetime <= ?";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (processDateFrom != null) {
            sql += " and ww.process_datetime >= ?";
            params.add(DateUtil.truncateTime(processDateFrom));
        }

        if (processDateTo != null) {
            sql += " and ww.process_datetime <= ?";
            params.add(DateUtil.formatDateToEndTime(processDateTo));
        }

        if (StringUtils.isNotBlank(withdrawType)) {
            sql += " and ww.withdraw_type = ? ";
            params.add(withdrawType);
        }

        if (StringUtils.isNotBlank(cryptoType)) {
            sql += " and ww.crypto_type = ? ";
            params.add(cryptoType);
        }

        if (StringUtils.isNotBlank(status)) {
            sql += " and ww.status = ? ";
            params.add(status);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<WalletWithdraw>() {
            @Override
            public WalletWithdraw mapRow(ResultSet rs, int rowNum) throws SQLException {
                WalletWithdraw walletWithdraw = new WalletWithdraw(false);
                walletWithdraw.setWithdrawId(rs.getString("withdraw_id"));
                walletWithdraw.setDocno(rs.getString("docno"));
                walletWithdraw.setOwnerId(rs.getString("owner_id"));
                walletWithdraw.setOwnerType(rs.getString("owner_type"));
                walletWithdraw.setWalletType(rs.getInt("wallet_type"));
                walletWithdraw.setRemark(rs.getString("remark"));
                walletWithdraw.setAmount(rs.getBigDecimal("amount"));
                walletWithdraw.setAdminFee(rs.getBigDecimal("admin_fee"));
                walletWithdraw.setTotalAmount(rs.getBigDecimal("total_amount"));
                walletWithdraw.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                walletWithdraw.setProcessDatetime(rs.getTimestamp("process_datetime"));
                walletWithdraw.setStatus(rs.getString("status"));
                walletWithdraw.setCryptoType(rs.getString("crypto_type"));

                walletWithdraw.setOwnerCode(rs.getString("member_code"));
                walletWithdraw.setOwnerName(rs.getString("full_name"));

                return walletWithdraw;
            }
        }, params.toArray());
    }

    @Override
    public void findWalletExchanges2CryptoForDatagrid(DatagridModel<WalletExchange> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
                                                      Date processDateFrom, Date processDateTo, String status) {
        List<Object> params = new ArrayList<>();

        String sql = "select ww.*, m.member_code, m.full_name\n" + //
                "from wl_wallet_exchange ww left join mb_member m on ww.owner_id = m.member_id and ww.owner_type=?\n" + //
                "where 1=1 and ww.wallet_type_from=? ";

        params.add(Global.UserType.MEMBER);
        params.add(Global.WalletType.WALLET_50); // USD

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " and m.member_code like ?";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(docno)) {
            sql += " and ww.docno like ?";
            params.add(docno + "%");
        }

        if (dateFrom != null) {
            sql += " and ww.trx_datetime >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and ww.trx_datetime <= ?";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (processDateFrom != null) {
            sql += " and ww.process_datetime >= ?";
            params.add(DateUtil.truncateTime(processDateFrom));
        }

        if (processDateTo != null) {
            sql += " and ww.process_datetime <= ?";
            params.add(DateUtil.formatDateToEndTime(processDateTo));
        }

        if (StringUtils.isNotBlank(status)) {
            sql += " and ww.status = ? ";
            params.add(status);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<WalletExchange>() {
            @Override
            public WalletExchange mapRow(ResultSet rs, int rowNum) throws SQLException {
                WalletExchange walletExchange = new WalletExchange(false);

                walletExchange.setExchangeId(rs.getString("exchange_id"));
                walletExchange.setDocno(rs.getString("docno"));
                walletExchange.setOwnerId(rs.getString("owner_id"));
                walletExchange.setOwnerType(rs.getString("owner_type"));
                walletExchange.setWalletTypeFrom(rs.getInt("wallet_type_from"));
                walletExchange.setWalletTypeTo(rs.getInt("wallet_type_to"));
                walletExchange.setRemark(rs.getString("remark"));
                walletExchange.setAmount(rs.getBigDecimal("amount"));
                walletExchange.setAmountTo(rs.getBigDecimal("amount_to"));
                walletExchange.setAdminFee(rs.getBigDecimal("admin_fee"));
                walletExchange.setTotalAmount(rs.getBigDecimal("total_amount"));
                walletExchange.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                walletExchange.setProcessDatetime(rs.getTimestamp("process_datetime"));
                walletExchange.setStatus(rs.getString("status"));

                walletExchange.setOwnerCode(rs.getString("member_code"));
                walletExchange.setOwnerName(rs.getString("full_name"));

                return walletExchange;
            }
        }, params.toArray());
    }

    @Override
    public void findWalletTopupsForDatagrid(DatagridModel<WalletTopup> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
                                            Integer walletType, String status) {
        List<Object> params = new ArrayList<>();

        String sql = "select wt.*, m.member_code, m.full_name\n" + //
                "from wl_wallet_topup wt left join mb_member m on wt.owner_id = m.member_id and wt.owner_type=?\n" + //
                "where 1=1 ";

        params.add(Global.UserType.MEMBER);

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " and m.member_code like ?";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(docno)) {
            sql += " and wt.docno like ?";
            params.add(docno + "%");
        }

        if (dateFrom != null) {
            sql += " and wt.trx_datetime >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and wt.trx_datetime <= ?";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (walletType != null) {
            sql += " and wt.wallet_type=? ";
            params.add(walletType);
        }

        if (StringUtils.isNotBlank(status)) {
            sql += " and wt.status = ? ";
            params.add(status);
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<WalletTopup>() {
            @Override
            public WalletTopup mapRow(ResultSet rs, int rowNum) throws SQLException {
                WalletTopup walletTopup = new WalletTopup(false);
                walletTopup.setTopupId(rs.getString("topup_id"));
                walletTopup.setDocno(rs.getString("docno"));
                walletTopup.setOwnerId(rs.getString("owner_id"));
                walletTopup.setOwnerType(rs.getString("owner_type"));
                walletTopup.setWalletType(rs.getInt("wallet_type"));
                walletTopup.setAmount(rs.getDouble("amount"));
                walletTopup.setWalletTrxId(rs.getString("wallet_trx_id"));
                walletTopup.setStatus(rs.getString("status"));
                walletTopup.setCrytocurrencyType(rs.getString("crytocurrency_type"));
                walletTopup.setCrytocurrencyAmount(rs.getString("crytocurrency_amount"));
                walletTopup.setCrytocurrencyRate(rs.getString("crytocurrency_rate"));
                walletTopup.setTrxDatetime(rs.getTimestamp("trx_datetime"));
                walletTopup.setRemark(rs.getString("remark"));

                walletTopup.setOwnerCode(rs.getString("member_code"));
                walletTopup.setOwnerName(rs.getString("full_name"));

                return walletTopup;
            }
        }, params.toArray());
    }

    @Override
    public void findDepositHistories(DatagridModel<DepositHistory> datagridModel, String ownerId, String ownerType, String search) {
        List<Object> params = new ArrayList<>();

        String sql = "select w.trx_id wallet_trx_id, w.wallet_ref_id crypto_wallet_trx_id, w.wallet_refno tx_hash,\n" + //
                "  w.trx_datetime, w.in_amt amount, \n" + //
                "  wtc.crypto_type\n" + //
                "from wl_wallet_trx w left join wl_wallet_type_config wtc on w.wallet_type=wtc.wallet_type\n" + //
                "where w.owner_id = ? and w.owner_type =? and w.trx_type = ?"; //

        params.add(ownerId);
        params.add(ownerType);
        params.add(Global.WalletTrxType.DEPOSIT);

        if (StringUtils.isNotBlank(search)) {
            sql += " and (wtc.crypto_type like ? or upper(w.wallet_refno) like ? )";
            params.add("%" + search + "%");
            params.add("%" + search + "%");
        }

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            DepositHistory depositHistory = new DepositHistory();
            depositHistory.setWalletTrxId(rs.getString("wallet_trx_id"));
            depositHistory.setCryptoWalletTrxId(rs.getString("crypto_wallet_trx_id"));
            depositHistory.setTxHash(rs.getString("tx_hash"));
            depositHistory.setTrxDatetime(rs.getTimestamp("trx_datetime"));
            depositHistory.setAmount(rs.getBigDecimal("amount"));
            depositHistory.setCryptoType(rs.getString("crypto_type"));

            return depositHistory;
        }, params.toArray());
    }

    @Override
    public BigDecimal findUsedDailyWithdrawLimit(String ownerId, String ownerType) {
        List<Object> params = new ArrayList<>();

        String sql = "select sum(value_amount) amt from wl_wallet_withdraw where owner_id=? and owner_type=? and trx_datetime > ? and status not in (?)";

        params.add(ownerId);
        params.add(ownerType);
        params.add(DateUtil.addDate(new Date(), -1));
        params.add(Global.Status.CANCELLED);

        BigDecimal usedLimit = this.queryForSingleRow(sql, new SingleRowMapper<BigDecimal>() {
            @Override
            public BigDecimal onMapRow(ResultSet resultSet, int rowNum) throws SQLException {
                return resultSet.getBigDecimal("amt");
            }
        }, params.toArray());

        if (usedLimit == null)
            return BigDecimal.ZERO;
        else
            return usedLimit;
    }

    @Override
    public List<String> findActiveAssetCryptoTypes() {
        List<Object> params = Arrays.asList( //
                Global.WalletType.POINT, //
                Global.WalletType.OMC, //
                Global.WalletType.USD, //
                Global.WalletType.INCW, //

                Global.Status.ACTIVE
        );

        String sql = "select crypto_type from wl_wallet_type_config where crypto_type not in (?,?,?,?) and status=? order by wallet_type asc";

        return query(sql, new RowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getString("crypto_type");
            }
        }, params.toArray());
    }

    @Override
    public void findTopupPackageCountForReport(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo) {
        String sql = "SELECT p.package_amount, count(t.trx_id) package_count FROM wl_wallet_topup_package p " +
                "LEFT JOIN wl_wallet_trx t ON t.in_amt = p.package_amount " +
                "WHERE t.trx_type = ? ";

        List<Object> params = new ArrayList<>();
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);

        if (dateFrom != null) {
            sql += "AND t.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }
        if (dateTo != null) {
            sql += "AND t.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql += "GROUP BY t.in_amt ORDER BY t.in_amt ";

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("packageAmount", rs.getString("package_amount"));
            hashMap.put("topupPackageCount", rs.getString("package_count"));
            return hashMap;
        }, params.toArray());
    }

    @Override
    public void findTopupPackageDetailForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount) {

        List<Object> params = new ArrayList<>();
        String sql = "SELECT t.in_amt, t.datetime_add, m.member_code, t.remark FROM wl_wallet_trx t " +
                "LEFT JOIN mb_member m ON m.member_id = t.owner_id " +
                "WHERE t.trx_type = ? ";
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);

        if (packageAmount != 0) {
            sql += "AND t.in_amt = ? ";
            params.add(packageAmount);
        }

        if (dateFrom != null) {
            sql += "AND t.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += "AND t.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        sql += "ORDER BY t.datetime_add DESC";

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("packageAmount", String.valueOf(rs.getInt("in_amt")));
            hashMap.put("datetimeAdd", rs.getString("datetime_add"));
            hashMap.put("memberCode", rs.getString("member_code"));
            hashMap.put("remark", rs.getString("remark"));
            return hashMap;
        }, params.toArray());
    }

    @Override
    public void findTopupPackageDetailForListing2(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount, String memberCode, String topupWay) {

        List<Object> params = new ArrayList<>();
        String sql = "SELECT t.in_amt, t.datetime_add, m.member_code, t.remark FROM wl_wallet_trx t " +
                "LEFT JOIN mb_member m ON m.member_id = t.owner_id " +
                "WHERE t.trx_type = ? ";
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);

        if (packageAmount != 0) {
            sql += "AND t.in_amt = ? ";
            params.add(packageAmount);
        }

        if (dateFrom != null) {
            sql += "AND t.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += "AND t.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " and m.member_code = ?";
            params.add(memberCode);
        }

        if (StringUtils.isNotBlank(topupWay)) {
            sql += "AND t.remark = ? ";
            params.add(topupWay);
        }

        sql += "ORDER BY t.trx_datetime DESC";

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("packageAmount", String.valueOf(rs.getInt("in_amt")));
            hashMap.put("datetimeAdd", rs.getString("datetime_add"));
            hashMap.put("memberCode", rs.getString("member_code"));
            hashMap.put("remark", rs.getString("remark"));
            return hashMap;
        }, params.toArray());
    }

    @Override
    public void findPrizeForReport(DatagridModel<HashMap<String, String>> datagridModel, String remark) {
        String sql = "SELECT x.remark,x.datetime_add,x.amt,x.price,x.total_amt,m.member_code FROM wl_point_trx x " +
                "JOIN mb_member m ON x.owner_id = m.member_id  " +
                "LEFT JOIN wl_point_symbolic_item y ON x.symbolic_id = y.symbolic_id  " +
                "WHERE x.owner_type = ? ";

        List<Object> params = new ArrayList<>();
        params.add("MEMBER");


        if (StringUtils.isNotBlank(remark)) {
            sql += "AND x.remark LIKE ?";
            params.add("%" + remark + "%");
        }

        sql += "ORDER BY x.datetime_add DESC";

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("remark", rs.getString("remark"));
            hashMap.put("datetimeAdd", String.valueOf(rs.getTimestamp("datetime_add")));
            hashMap.put("amt", String.valueOf(rs.getInt("amt")));
            hashMap.put("price", String.format("%.2f", rs.getBigDecimal("price")));
            hashMap.put("totalAmt", String.format("%.2f", rs.getBigDecimal("total_amt")));
            hashMap.put("memberCode", rs.getString("member_code"));
            return hashMap;
        }, params.toArray());
    }

    @Override
    public void findTopupHistoryForListing(DatagridModel<WalletTrx> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT t.trx_datetime as trxDateTime, t.in_amt, t.trx_desc,t.remark ,t.wallet_ref_id,t.wallet_refno,m.member_code,d.profile_name FROM wl_wallet_trx t " +
                     "join mb_member m on m.member_id = t.owner_id " +
                     "join mb_member_detail d on d.member_det_id = m.member_det_id " +
                "WHERE t.trx_type = ? ";
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " AND m.member_code = ? ";
            params.add(memberCode);
        }

       if (StringUtils.isNotBlank(orderId)) {
            sql += " AND (t.wallet_ref_id = ? OR t.wallet_refno = ? )";
            params.add(orderId);
           params.add(orderId);
        }


        if (dateFrom != null) {
            sql += " AND t.trx_datetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " AND t.trx_datetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(remark)) {
            sql += " AND t.remark = ? ";
            params.add(remark);
        }
        sql += " AND t.remark NOT IN (?,?) ";
        params.add(WalletTrx.OMC_WALLET);
        params.add(WalletTrx.WEBSITE_OMC_WALLET);

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            WalletTrx walletTrx = new WalletTrx();
            walletTrx.setTrxDatetime(rs.getTimestamp("trxDatetime"));
            walletTrx.setInAmt(rs.getBigDecimal("in_amt"));
            walletTrx.setTrxDesc(rs.getString("trx_desc"));
            walletTrx.setMemberCode(rs.getString("member_code"));
            walletTrx.setProfileName(rs.getString("profile_name"));

            String remarks = rs.getString("remark");
            if(remarks.equalsIgnoreCase(WalletTrx.RAZER_PAY))
            {
                walletTrx.setWalletRefId(rs.getString("wallet_refno"));
            }else
            {
                walletTrx.setWalletRefId(rs.getString("wallet_ref_id"));
            }

            return walletTrx;
        }, params.toArray());
    }

    @Override
    public void findOMCWalletHistoryForListing(DatagridModel<HashMap<String, String>> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT t.trx_datetime as trxDateTime, t.in_amt, t.trx_desc,t.remark ,t.wallet_ref_id,t.wallet_refno,m.member_code,d.profile_name FROM wl_wallet_trx t " +
                "join mb_member m on m.member_id = t.owner_id " +
                "join mb_member_detail d on d.member_det_id = m.member_det_id " +
                "WHERE t.trx_type = ? ";
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);


        if (StringUtils.isNotBlank(memberCode)) {
            sql += " AND m.member_code = ? ";
            params.add(memberCode);
        }

        if (StringUtils.isNotBlank(orderId)) {
            sql += " AND t.wallet_ref_id = ?";
            params.add(orderId);
        }

        if (dateFrom != null) {
            sql += " AND t.trx_datetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " AND t.trx_datetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(remark)) {
            sql += " AND t.remark = ? ";
            params.add(remark);
        }
        sql += " AND t.remark LIKE ? ";
        params.add("%" + WalletTrx.OMC_WALLET);

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("trxDatetime", String.valueOf(rs.getTimestamp("trxDatetime")));
            hashMap.put("inAmt", String.valueOf(rs.getInt("in_amt")));
            hashMap.put("trxDesc", rs.getString("trx_desc"));
            hashMap.put("memberCode", rs.getString("member_code"));
            hashMap.put("profileName", rs.getString("profile_name"));
            hashMap.put("walletRefId", rs.getString("wallet_ref_id"));
            return hashMap;
        }, params.toArray());
    }

    @Override
    public List<WalletTrx> findTopupHistoryByRemark(Date dateFrom, Date dateTo, String remark) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT t.trx_datetime, t.in_amt, t.trx_desc, t.wallet_ref_id FROM wl_wallet_trx t " +
                "WHERE t.trx_type = ? ";
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);

        if (dateFrom != null) {
            sql += " AND t.trx_datetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " AND t.trx_datetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(remark)) {
            sql += " AND t.remark = ? ";
            params.add(remark);
        }

        sql += "ORDER BY t.trx_datetime DESC";

        List<WalletTrx> results = query(sql, new RowMapper<WalletTrx>() {
            @Override
            public WalletTrx mapRow(ResultSet resultSet, int i) throws SQLException {
                WalletTrx walletTrx = new WalletTrx();
                walletTrx.setTrxDatetime(resultSet.getDate("trx_datetime"));
                walletTrx.setInAmt(resultSet.getBigDecimal("in_amt"));
                walletTrx.setTrxDesc(resultSet.getString("trx_desc"));
                walletTrx.setWalletRefId(resultSet.getString("wallet_ref_id"));
                return walletTrx;
            }
        }, params.toArray());

        return results;
    }

    @Override
    public List<WalletTrx> findTopupPackageDetailByTopupWay(Date dateFrom, Date dateTo, int packageAmount, String memberCode, String topupWay) {

        List<Object> params = new ArrayList<>();
        String sql = "SELECT t.in_amt, t.datetime_add, m.member_code, t.remark FROM wl_wallet_trx t " +
                "LEFT JOIN mb_member m ON m.member_id = t.owner_id " +
                "WHERE t.trx_type = ? ";
        params.add(Global.WalletTrxType.LIVE_STREAM_TOPUP);

        if (packageAmount != 0) {
            sql += "AND t.in_amt = ? ";
            params.add(packageAmount);
        }

        if (dateFrom != null) {
            sql += "AND t.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += "AND t.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " and m.member_code = ?";
            params.add(memberCode);
        }

        if (StringUtils.isNotBlank(topupWay)) {
            sql += "AND t.remark = ? ";
            params.add(topupWay);
        }

        sql += "ORDER BY t.datetime_add DESC";

        List<WalletTrx> results = query(sql, new RowMapper<WalletTrx>() {
            @Override
            public WalletTrx mapRow(ResultSet resultSet, int i) throws SQLException {
                WalletTrx walletTrx = new WalletTrx();
                walletTrx.setInAmt(resultSet.getBigDecimal("in_amt"));
                walletTrx.setDatetimeAdd(resultSet.getDate("datetime_add"));
                walletTrx.setMemberCode(resultSet.getString("member_code"));
                walletTrx.setRemark(resultSet.getString("remark"));
                return walletTrx;
            }
        }, params.toArray());

        return results;
    }

    public void findActiveUserForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "SELECT audience.date, totalPriceGift.sendGiftCOUNT, audienceCOUNT, newUser.newUserCOUNT, totalAmt.totalAmt FROM( " +
                " SELECT COUNT(*) AS audienceCOUNT, date FROM( " +
                " SELECT member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM app_live_stream_audience " +
                " GROUP BY member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') " +
                ") AS data GROUP BY data.date " +
                ") audience JOIN (" +
                " SELECT COUNT(member_id) AS newUserCOUNT, date FROM( " +
                " SELECT member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM mb_member " +
                " GROUP BY member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d')" +
                ") AS data GROUP BY data.date" +
                ") newUser ON newUser.date = audience.date JOIN(" +
                " SELECT COUNT(contributor_id) AS sendGiftCOUNT, date FROM(" +
                " SELECT contributor_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM wl_point_history " +
                " GROUP BY contributor_id, DATE_FORMAT(datetime_add,'%Y-%m-%d')" +
                ") AS data GROUP BY data.date" +
                ") totalPriceGift ON totalPriceGift.date = audience.date " +
                "JOIN( " +
                "SELECT * FROM( " +
                "SELECT sum(total_amt)as totalAmt, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM wl_point_history " +
                " GROUP BY DATE_FORMAT(datetime_add,'%Y-%m-%d') " +
                ") AS data GROUP BY data.date " +
                ") totalAmt ON totalAmt.date = audience.date " +
                " WHERE 1=1 ";

        if (dateFrom != null) {
            sql += " AND audience.date >= ?";
            String date = sf.format(dateFrom);
            params.add(date);
        }

        if (dateTo != null) {
            sql += " AND audience.date <= ? ";
            params.add(dateTo);
        }

        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("date", rs.getString("date"));
            hashMap.put("sendGiftCount", String.valueOf(rs.getInt("sendGiftCount")));
            hashMap.put("audienceCount", String.valueOf(rs.getInt("audienceCount")));
            hashMap.put("newUserCount", String.valueOf(rs.getInt("newUserCount")));
            hashMap.put("totalAmt", String.valueOf(rs.getDouble("totalAmt")));
            return hashMap;
        }, params.toArray());
    }

    public List<ActiveUserReport> findActiveUser(Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
        String sql = "SELECT audience.date, totalPriceGift.sendGiftCOUNT, audienceCOUNT, newUser.newUserCOUNT, totalPriceGift.totalAmt FROM( " +
                " SELECT COUNT(*) AS audienceCOUNT, date FROM( " +
                " SELECT member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM app_live_stream_audience " +
                " GROUP BY member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') " +
                ") AS data GROUP BY data.date " +
                ") audience JOIN (" +
                " SELECT COUNT(member_id) AS newUserCOUNT, date FROM( " +
                " SELECT member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM mb_member " +
                " GROUP BY member_id, DATE_FORMAT(datetime_add,'%Y-%m-%d')" +
                ") AS data GROUP BY data.date" +
                ") newUser ON newUser.date = audience.date JOIN(" +
                " SELECT SUM(total_amt) AS totalAmt, COUNT(contributor_id) AS sendGiftCOUNT, date FROM(" +
                " SELECT total_amt, contributor_id, DATE_FORMAT(datetime_add,'%Y-%m-%d') AS date FROM wl_point_history " +
                " GROUP BY total_amt, contributor_id, DATE_FORMAT(datetime_add,'%Y-%m-%d')" +
                ") AS data GROUP BY data.date" +
                ") totalPriceGift ON totalPriceGift.date = audience.date " +
                " WHERE 1=1 ";

        if (dateFrom != null) {
            sql += " AND audience.date >= ?";
            String date = sf.format(dateFrom);
            params.add(date);
        }

        if (dateTo != null) {
            sql += " AND audience.date <= ? ";
            params.add(dateTo);
        }

        List<ActiveUserReport> results = query(sql, new RowMapper<ActiveUserReport>() {
            @Override
            public ActiveUserReport mapRow(ResultSet resultSet, int i) throws SQLException {
                ActiveUserReport activeUserReport = new ActiveUserReport();
                activeUserReport.setDate(resultSet.getDate("date"));
                activeUserReport.setSendGiftCount(resultSet.getInt("sendGiftCount"));
                activeUserReport.setAudienceCount(resultSet.getInt("audienceCount"));
                activeUserReport.setNewUserCount(resultSet.getInt("newUserCount"));
                activeUserReport.setTotalAmt(resultSet.getDouble("totalAmt"));
                return activeUserReport;
            }
        }, params.toArray());
        return results;
    }
}
