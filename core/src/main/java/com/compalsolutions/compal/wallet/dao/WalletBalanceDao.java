package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletBalance;

public interface WalletBalanceDao extends BasicDao<WalletBalance, String> {
    public static final String BEAN_NAME = "walletBalanceDao";

    WalletBalance findByOwnerIdAndOwnerTypeAndWalletType(String ownerId, String ownerType, int walletType);

    void doCreditAvailableBalance(String ownerId, String ownerType, int walletType, BigDecimal amount);

    void doDebitAvailableBalance(String ownerId, String ownerType, int walletType, BigDecimal amount);

    List<WalletBalance> findRewardPointWalletPendingForRebate();
}
