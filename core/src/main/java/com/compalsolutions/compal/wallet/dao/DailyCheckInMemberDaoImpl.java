package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.vo.DailyCheckIn;
import com.compalsolutions.compal.wallet.vo.DailyCheckInMember;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(DailyCheckInMemberDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DailyCheckInMemberDaoImpl extends Jpa2Dao<DailyCheckInMember, String> implements DailyCheckInMemberDao {

    public DailyCheckInMemberDaoImpl() {
        super(new DailyCheckInMember(false));
    }

    @Override
    public DailyCheckInMember getTodayCheckIn(String memberId) {

        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + DailyCheckInMember.class + " WHERE 1=1 ";

        hql += " and a.memberId=?";
        hql += " and DATE(a.lastCheckIn)=?";
        params.add(memberId);
        params.add(new Date());

        return findFirst(hql, params.toArray());
    }

    @Override
    public DailyCheckInMember getDailyCheckInDetailsByMemberId(String memberId) {

        DailyCheckInMember example = new DailyCheckInMember(false);
        example.setMemberId(memberId);

        return findFirst(example);
    }

}
