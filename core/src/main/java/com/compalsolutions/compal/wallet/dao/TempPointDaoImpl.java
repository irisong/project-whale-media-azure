package com.compalsolutions.compal.wallet.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.vo.TempPoint;

@Component(TempPointDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class TempPointDaoImpl extends Jpa2Dao<TempPoint, String> implements TempPointDao {
    public TempPointDaoImpl() {
        super(new TempPoint(false));
    }

    @Override
    public List<TempPoint> findByTrxDate(Date trxDate) {
        TempPoint example = new TempPoint(false);
        example.setTrxDate(trxDate);

        return findByExample(example, "memberCode");
    }

    @Override
    public List<TempPoint> findByNotProcessed() {
        TempPoint example = new TempPoint(false);
        example.setProcessed(false);
        return findByExample(example, "memberCode");
    }
}
