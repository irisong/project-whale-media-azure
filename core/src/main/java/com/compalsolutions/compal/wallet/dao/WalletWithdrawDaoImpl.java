package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.wallet.repository.WalletWithdrawRepository;
import com.compalsolutions.compal.wallet.vo.WalletWithdraw;

@Component(WalletWithdrawDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletWithdrawDaoImpl extends Jpa2Dao<WalletWithdraw, String> implements WalletWithdrawDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletWithdrawRepository walletWithdrawRepository;

    public WalletWithdrawDaoImpl() {
        super(new WalletWithdraw(false));
    }

    @Override
    public void findWithdrawHistories(DatagridModel<WalletWithdraw> datagridModel, String ownerId, String ownerType, String search) {
        if (StringUtils.isBlank(ownerId) || StringUtils.isBlank(ownerType)) {
            throw new ValidatorException("WalletWithdrawDao.findWithdrawHistories: ownerId or ownerType is blank");
        }

        String hql = "from WalletWithdraw as ww where ww.ownerId=? and ww.ownerType=? \n";

        List<Object> params = new ArrayList<>();
        params.add(ownerId);
        params.add(ownerType);

        if (StringUtils.isNotBlank(search)) {
            hql += " and (";
            hql += " ww.cryptoType like ? or \n";
            hql += " upper(ww.coinAddress) like ? \n";
            hql += ")";

            params.add("%" + search + "%");
            params.add("%" + search + "%");
        }

        findForDatagrid(datagridModel, "ww", hql, params.toArray());
    }

    @Override
    public List<WalletWithdraw> findWalletWithdrawForProcessing(String cryptoType, int walletType, BigDecimal maxWithdrawAmount, int numberOfRecords) {
        if (StringUtils.isBlank(cryptoType)) {
            throw new ValidatorException("WalletWithdrawDao.findWalletWithdrawForProcessing: cryptoType is blank");
        }

        String hql = "from WalletWithdraw as ww where ww.cryptoType=? and ww.walletType=? and ww.amount<=? and ww.status=? order by ww.trxDatetime ASC";
        List<Object> params = Arrays.asList(cryptoType, walletType, maxWithdrawAmount, Global.Status.SCHEDULED);

        return findBlock(hql, 0, numberOfRecords, params.toArray());
    }

    @Override
    public List<WalletWithdraw> findWalletWithdrawsByCryptoTypesAndStatus(List<String> cryptoTypes, String status) {
        if (CollectionUtil.isEmpty(cryptoTypes) || StringUtils.isBlank(status)) {
            throw new ValidatorException("WalletWithdrawDao.findWalletWithdrawsByStatus: cryptoTypes or status is blank");
        }

        WalletWithdraw example = new WalletWithdraw(false);
        example.setStatus(status);
        example.addInCondition("cryptoType", cryptoTypes.toArray());

        return findByExample(example, "trxDatetime");
    }

    @Override
    public WalletWithdraw findWalletWithdrawByTxHash(String txHash) {
        if (StringUtils.isBlank(txHash)) {
            throw new ValidatorException("WalletWithdrawDao.findWalletWithdrawByTxHash: txHash is blank");
        }

        WalletWithdraw example = new WalletWithdraw(false);
        example.setTxHash(txHash);
        return findUnique(example);
    }
}
