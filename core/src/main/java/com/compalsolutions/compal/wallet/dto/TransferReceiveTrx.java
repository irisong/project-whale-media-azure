package com.compalsolutions.compal.wallet.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TransferReceiveTrx {
    public static final String TRANSFER_RECEIVE = "TRANSFER_RECEIVE";
    public static final String TRANSFER = "TRANSFER";
    public static final String RECEIVE = "RECEIVE";
    public static final String EXCHANGE = "EXCHANGE";
    public static final String REWARD = "REWARD";

    private String walletTrxId;
    private String trxType;
    private String walletRefId;

    private String currencyCode;

    private Date trxDatetime;

    private BigDecimal amount;
    private String status;
    private String desc;

    public TransferReceiveTrx() {
    }

    public TransferReceiveTrx(String walletTrxId, String desc, BigDecimal amount, String trxType, Date trxDatetime, String currencyCode, String status) {
        this.walletTrxId = walletTrxId;
        this.desc = desc;
        this.amount = amount;
        this.trxType = trxType;
        this.trxDatetime = trxDatetime;
        this.currencyCode = currencyCode;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getWalletRefId() {
        return walletRefId;
    }

    public void setWalletRefId(String walletRefId) {
        this.walletRefId = walletRefId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
