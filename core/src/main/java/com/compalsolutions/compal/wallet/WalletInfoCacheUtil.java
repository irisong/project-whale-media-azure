package com.compalsolutions.compal.wallet;

import com.compalsolutions.compal.exception.SystemErrorException;

public class WalletInfoCacheUtil {
    public static WalletInfoCacheProvider walletInfoCacheProvider;

    public static void setWalletInfoCacheProvider(WalletInfoCacheProvider walletInfoCacheProvider) {
        WalletInfoCacheUtil.walletInfoCacheProvider = walletInfoCacheProvider;
    }

    public static void clearCache() {
        if (walletInfoCacheProvider == null)
            throw new SystemErrorException("walletInfoCacheProvider is blank");

        walletInfoCacheProvider.clearCache();
    }
}
