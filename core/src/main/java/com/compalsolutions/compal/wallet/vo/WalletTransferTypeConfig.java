package com.compalsolutions.compal.wallet.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_trf_type_conf")
@Access(AccessType.FIELD)
public class WalletTransferTypeConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "transfer_type_id", unique = true, nullable = false, length = 32)
    private String transferTypeId;

    @Column(name = "transfer_self", nullable = false)
    private Boolean transferSelf;

    @Column(name = "wallet_type_from", nullable = false)
    private Integer walletTypeFrom;

    @Column(name = "wallet_type_to", nullable = false)
    private Integer walletTypeTo;

    @Column(name = "seq", nullable = false)
    private Integer seq;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public WalletTransferTypeConfig() {
    }

    public WalletTransferTypeConfig(boolean defaultValue) {
        if (defaultValue) {
            transferSelf = true;
            status = Global.Status.ACTIVE;
        }
    }

    public String getTransferTypeId() {
        return transferTypeId;
    }

    public void setTransferTypeId(String transferTypeId) {
        this.transferTypeId = transferTypeId;
    }

    public Boolean getTransferSelf() {
        return transferSelf;
    }

    public void setTransferSelf(Boolean transferSelf) {
        this.transferSelf = transferSelf;
    }

    public Integer getWalletTypeFrom() {
        return walletTypeFrom;
    }

    public void setWalletTypeFrom(Integer walletTypeFrom) {
        this.walletTypeFrom = walletTypeFrom;
    }

    public Integer getWalletTypeTo() {
        return walletTypeTo;
    }

    public void setWalletTypeTo(Integer walletTypeTo) {
        this.walletTypeTo = walletTypeTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTransferTypeConfig that = (WalletTransferTypeConfig) o;

        return transferTypeId != null ? transferTypeId.equals(that.transferTypeId) : that.transferTypeId == null;
    }

    @Override
    public int hashCode() {
        return transferTypeId != null ? transferTypeId.hashCode() : 0;
    }
}
