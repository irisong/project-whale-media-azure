package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_type_config")
@Access(AccessType.FIELD)
public class WalletTypeConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String TYPE_TOKEN = "TOKEN";
    public static final String TYPE_COIN = "COIN";
    public static final String TYPE_FIAT = "FIAT";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "type_id", unique = true, nullable = false, length = 32)
    private String typeId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    @ToUpperCase
    @ToTrim
    @Column(name = "crypto_type", length = 20, nullable = false)
    private String cryptoType;

    @ToUpperCase
    @ToTrim
    @Column(name = "coin_token_type", length = 20, nullable = false)
    private String coinTokenType;

    @Column(name = "token_decimal")
    private Integer tokenDecimal;

    @Column(name = "token_contract_address", length = 100)
    private String tokenContractAddress;

    @Column(name = "gas_used")
    private Integer gasUsed;

    @Column(name = "deposit_transfer_gas_used")
    private Integer depositTransferGasUsed;

    @Column(name = "min_deposit_transfer_amount", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal minDepositTransferAmount;

    @Column(name = "deposit_transfer_address", length = 100)
    private String depositTransferAddress;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public WalletTypeConfig() {
    }

    public WalletTypeConfig(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public WalletTypeConfig(String ownerType, Integer walletType, String cryptoType, String coinTokenType, Integer tokenDecimal, String tokenContractAddress,
            Integer gasUsed, Integer depositTransferGasUsed, BigDecimal minDepositTransferAmount, String depositTransferAddress, String status) {
        this.ownerType = ownerType;
        this.walletType = walletType;
        this.cryptoType = cryptoType;
        this.coinTokenType = coinTokenType;
        this.tokenDecimal = tokenDecimal;
        this.tokenContractAddress = tokenContractAddress;
        this.gasUsed = gasUsed;
        this.depositTransferGasUsed = depositTransferGasUsed;
        this.minDepositTransferAmount = minDepositTransferAmount;
        this.depositTransferAddress = depositTransferAddress;
        this.status = status;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public String getCoinTokenType() {
        return coinTokenType;
    }

    public void setCoinTokenType(String coinTokenType) {
        this.coinTokenType = coinTokenType;
    }

    public Integer getTokenDecimal() {
        return tokenDecimal;
    }

    public void setTokenDecimal(Integer tokenDecimal) {
        this.tokenDecimal = tokenDecimal;
    }

    public String getTokenContractAddress() {
        return tokenContractAddress;
    }

    public void setTokenContractAddress(String tokenContractAddress) {
        this.tokenContractAddress = tokenContractAddress;
    }

    public Integer getGasUsed() {
        return gasUsed;
    }

    public void setGasUsed(Integer gasUsed) {
        this.gasUsed = gasUsed;
    }

    public Integer getDepositTransferGasUsed() {
        return depositTransferGasUsed;
    }

    public void setDepositTransferGasUsed(Integer depositTransferGasUsed) {
        this.depositTransferGasUsed = depositTransferGasUsed;
    }

    public BigDecimal getMinDepositTransferAmount() {
        return minDepositTransferAmount;
    }

    public void setMinDepositTransferAmount(BigDecimal minDepositTransferAmount) {
        this.minDepositTransferAmount = minDepositTransferAmount;
    }

    public String getDepositTransferAddress() {
        return depositTransferAddress;
    }

    public void setDepositTransferAddress(String depositTransferAddress) {
        this.depositTransferAddress = depositTransferAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTypeConfig that = (WalletTypeConfig) o;

        return typeId != null ? typeId.equals(that.typeId) : that.typeId == null;
    }

    @Override
    public int hashCode() {
        return typeId != null ? typeId.hashCode() : 0;
    }
}
