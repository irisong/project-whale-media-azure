package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_currency_exchange")
@Access(AccessType.FIELD)
public class WalletCurrencyExchange extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "trx_id", unique = true, nullable = false, length = 32)
    private String trxId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @Column(name = "ref_id", length = 20)
    private String refId;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    /**
     * 8 decimal, rate in cryptoTypeTo
     */
    @Column(name = "rate", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private double rate;

    @Column(name = "amount", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amount;

    @Column(name = "amount_to", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amountTo;

    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "pay_to", length = 32)
    private String payTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "process_datetime")
    private Date processDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;


    public WalletCurrencyExchange() {
    }

    public WalletCurrencyExchange(boolean defaultValue) {
        if (defaultValue) {
            processDatetime = new Date();
            status = Global.Status.PENDING;
            rate = 0;
            amount = BigDecimal.ZERO;
            amountTo = BigDecimal.ZERO;
        }
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAmountTo() {
        return amountTo;
    }

    public void setAmountTo(BigDecimal amountTo) {
        this.amountTo = amountTo;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getPayTo() {
        return payTo;
    }

    public void setPayTo(String payTo) {
        this.payTo = payTo;
    }

    public Date getProcessDatetime() {
        return processDatetime;
    }

    public void setProcessDatetime(Date processDatetime) {
        this.processDatetime = processDatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
