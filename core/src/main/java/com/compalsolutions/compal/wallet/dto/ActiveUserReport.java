package com.compalsolutions.compal.wallet.dto;

import java.util.Date;

public class ActiveUserReport {
    public Date date;
    public int sendGiftCount;
    public int audienceCount;
    public int newUserCount;
    public double totalAmt;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getSendGiftCount() {
        return sendGiftCount;
    }

    public void setSendGiftCount(int sendGiftCount) {
        this.sendGiftCount = sendGiftCount;
    }

    public int getAudienceCount() {
        return audienceCount;
    }

    public void setAudienceCount(int audienceCount) {
        this.audienceCount = audienceCount;
    }

    public int getNewUserCount() {
        return newUserCount;
    }

    public void setNewUserCount(int newUserCount) {
        this.newUserCount = newUserCount;
    }

    public double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(double totalAmt) {
        this.totalAmt = totalAmt;
    }
}
