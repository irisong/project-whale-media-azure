package com.compalsolutions.compal.wallet.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.repository.WalletTopupRepository;
import com.compalsolutions.compal.wallet.vo.WalletTopup;

@Component(WalletTopupDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTopupDaoImpl extends Jpa2Dao<WalletTopup, String> implements WalletTopupDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTopupRepository walletTopupRepository;

    public WalletTopupDaoImpl() {
        super(new WalletTopup(false));
    }
}
