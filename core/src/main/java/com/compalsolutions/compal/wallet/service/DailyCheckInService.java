package com.compalsolutions.compal.wallet.service;

import java.util.*;

public interface DailyCheckInService {
    public static final String BEAN_NAME = "dailyCheckInService";

    public Integer getDailyCheckInDetails(String memberId);

    public boolean getTodayCheckIn(String memberId);

    public boolean isSameWithYtd(Calendar c1);

    public void doProcessDailyCheckIn(Locale locale, String memberId);
}
