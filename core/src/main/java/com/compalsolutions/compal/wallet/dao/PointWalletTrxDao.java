package com.compalsolutions.compal.wallet.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;

public interface PointWalletTrxDao extends BasicDao<PointWalletTrx, String> {
    public static final String BEAN_NAME = "pointWalletTrxDao";

    public List<PointWalletTrx> findPointWalletTrxsByOwnerIdAndOwnerTypeAndWalleType(String ownerId, String ownerType, int walletType, int recordSize);

    public void findPointWalletTrxsForCryptoListing(DatagridModel<PointWalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes);
}
