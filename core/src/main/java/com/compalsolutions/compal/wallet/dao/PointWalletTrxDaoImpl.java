package com.compalsolutions.compal.wallet.dao;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.wallet.repository.WalletTrxRepository;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;

@Component(PointWalletTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointWalletTrxDaoImpl extends Jpa2Dao<PointWalletTrx, String> implements PointWalletTrxDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTrxRepository walletTrxRepository;

    public PointWalletTrxDaoImpl() {
        super(new PointWalletTrx(false));
    }

    @Override
    public List<PointWalletTrx> findPointWalletTrxsByOwnerIdAndOwnerTypeAndWalleType(String ownerId, String ownerType, int walletType, int recordSize) {
        Validate.notBlank(ownerId);
        Validate.notBlank(ownerType);

        PointWalletTrx example = new PointWalletTrx(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setWalletType(walletType);

        return findBlock(example, 0, recordSize,"trxDatetime desc");
    }

    @Override
    public void findPointWalletTrxsForCryptoListing(DatagridModel<PointWalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes) {
        Validate.notBlank(ownerId);
        Validate.notBlank(ownerType);

        String hql = "FROM w IN " + PointWalletTrx.class
                + " WHERE w.ownerId = ? AND w.ownerType = ? AND w.walletType = ? AND (w.inAmt <> 0 OR w.outAmt <> 0) ";

        List<Object> params = new ArrayList<>();
        params.add(ownerId);
        params.add(ownerType);
        params.add(walletType);

        if (trxTypes != null && trxTypes.length > 0) {
            if (trxTypes.length > 1) {
                hql += " AND w.trxType in (";

                for (int i = 0; i < trxTypes.length; i++) {
                    hql += "?";
                    if (i != trxTypes.length - 1) {
                        hql += ",";
                    }

                    params.add(trxTypes[i]);
                }

                hql += ") ";
            } else if (trxTypes.length == 1) {
                hql += " AND w.trxType = ? ";
                params.add(trxTypes[0]);
            }
        }

        hql += "ORDER BY trxDatetime DESC";

        findForDatagrid(datagridModel, "w", hql, params.toArray());
    }
}
