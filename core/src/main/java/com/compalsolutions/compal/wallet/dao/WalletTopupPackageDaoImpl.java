package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.repository.WalletTopupRepository;
import com.compalsolutions.compal.wallet.vo.WalletTopupPackage;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(WalletTopupPackageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTopupPackageDaoImpl extends Jpa2Dao<WalletTopupPackage, String> implements WalletTopupPackageDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTopupRepository walletTopupRepository;

    public WalletTopupPackageDaoImpl() {
        super(new WalletTopupPackage(false));
    }

    @Override
    public void findWalletTopupPackagesForDatagrid(DatagridModel<WalletTopupPackage> datagridModel, Integer walletType, Integer packageAmount, String status) {
        List<Object> params = new ArrayList<>();

        String hql = "FROM wp IN " + WalletTopupPackage.class + " WHERE 1=1 ";

        if (walletType != null) {
            hql += " and wp.walletType = ? ";
            params.add(walletType);
        }

        if (packageAmount != null) {
            hql += " and wp.packageAmount = ? ";
            params.add(packageAmount);
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and wp.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "wp", hql, params.toArray());
    }

    @Override
    public List<WalletTopupPackage> findWalletTopupPackages(String status) {
        WalletTopupPackage walletTopupPackage = new WalletTopupPackage();

        if (StringUtils.isNotBlank(status))
            walletTopupPackage.setStatus(status);

        return findByExample(walletTopupPackage);
    }

    @Override
    public WalletTopupPackage findWalletTopupPackage(Integer walletType, Integer packageAmount) {
        WalletTopupPackage walletTopupPackage = new WalletTopupPackage();
        walletTopupPackage.setWalletType(walletType);
        walletTopupPackage.setPackageAmount(packageAmount);

        return findUnique(walletTopupPackage);
    }

    @Override
    public WalletTopupPackage findOneWalletTopupPackage(Integer walletType, Integer packageAmount, String status) {
        WalletTopupPackage walletTopupPackage = new WalletTopupPackage();
        walletTopupPackage.setWalletType(walletType);
        walletTopupPackage.setPackageAmount(packageAmount);
        walletTopupPackage.setStatus(status);

        return findFirst(walletTopupPackage);
    }

    @Override
    public WalletTopupPackage findOneWalletTopupPackageByProdutId(String productId) {
        WalletTopupPackage walletTopupPackage = new WalletTopupPackage();

        walletTopupPackage.setProductId(productId);

        return findUnique(walletTopupPackage);
    }
}
