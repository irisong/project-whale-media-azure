package com.compalsolutions.compal.wallet.dao;

import java.util.List;

import com.compalsolutions.compal.wallet.vo.WalletWithdraw;

public interface WalletWithdrawSqlDao {
    public static final String BEAN_NAME = "WalletWithdrawSqlDao";

    public List<WalletWithdraw> findWalletWithdrawsForBtcWithdraw(String withdrawStatus, String cryptoType, Integer walletType, int numberOfRecords);
}
