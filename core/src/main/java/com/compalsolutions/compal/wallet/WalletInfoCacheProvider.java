package com.compalsolutions.compal.wallet;

public interface WalletInfoCacheProvider {
    public void clearCache();
}
