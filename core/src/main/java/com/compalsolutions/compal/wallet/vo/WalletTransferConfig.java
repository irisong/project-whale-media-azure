package com.compalsolutions.compal.wallet.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_trf_conf")
@Access(AccessType.FIELD)
public class WalletTransferConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "config_id", unique = true, nullable = false, length = 32)
    private String configId;

    @Column(name = "minimum_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double minimumAmount;

    @Column(name = "admin_fee_by_amount", nullable = false)
    private Boolean adminFeeByAmount;

    @Column(name = "admin_fee", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double adminFee;

    @ToTrim
    @ToUpperCase
    @Column(name = "transfer_target_groups", length = 50, nullable = false)
    private String tranferTargetGroups;

    public WalletTransferConfig() {
    }

    public WalletTransferConfig(boolean defaultValue) {
        if (defaultValue) {
            minimumAmount = 0D;
            adminFeeByAmount = true;
            adminFee = 0D;
        }
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public Double getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(Double minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public Boolean getAdminFeeByAmount() {
        return adminFeeByAmount;
    }

    public void setAdminFeeByAmount(Boolean adminFeeByAmount) {
        this.adminFeeByAmount = adminFeeByAmount;
    }

    public Double getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(Double adminFee) {
        this.adminFee = adminFee;
    }

    public String getTranferTargetGroups() {
        return tranferTargetGroups;
    }

    public void setTranferTargetGroups(String tranferTargetGroups) {
        this.tranferTargetGroups = tranferTargetGroups;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletTransferConfig that = (WalletTransferConfig) o;

        return configId != null ? configId.equals(that.configId) : that.configId == null;
    }

    @Override
    public int hashCode() {
        return configId != null ? configId.hashCode() : 0;
    }
}
