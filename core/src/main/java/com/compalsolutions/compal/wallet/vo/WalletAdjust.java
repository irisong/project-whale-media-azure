package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_adjust")
@Access(AccessType.FIELD)
public class WalletAdjust extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "adjust_id", unique = true, nullable = false, length = 32)
    private String adjustId;

    @ToUpperCase
    @ToTrim
    @Column(name = "docno", nullable = false, length = 20)
    private String docno;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    @ToUpperCase
    @ToTrim
    @Column(name = "adjust_type", nullable = false, length = 10)
    private String adjustType;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "wallet_trx_id", length = 32)
    private String walletTrxId;

    @Transient
    private String ownerName;

    @Transient
    private String ownerUsername;

    public WalletAdjust() {
    }

    public WalletAdjust(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAdjustId() {
        return adjustId;
    }

    public void setAdjustId(String adjustId) {
        this.adjustId = adjustId;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getAdjustType() {
        return adjustType;
    }

    public void setAdjustType(String adjustType) {
        this.adjustType = adjustType;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getOwnerUsername() {
        return ownerUsername;
    }

    public void setOwnerUsername(String ownerUsername) {
        this.ownerUsername = ownerUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletAdjust that = (WalletAdjust) o;

        return adjustId != null ? adjustId.equals(that.adjustId) : that.adjustId == null;
    }

    @Override
    public int hashCode() {
        return adjustId != null ? adjustId.hashCode() : 0;
    }
}
