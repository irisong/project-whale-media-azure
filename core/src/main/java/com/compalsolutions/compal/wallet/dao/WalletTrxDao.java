package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

public interface WalletTrxDao extends BasicDao<WalletTrx, String> {
    public static final String BEAN_NAME = "walletTrxDao";

    public void findWalletsForDatagrid(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
                                       Date dateTo);

    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndWalleType(String ownerId, String ownerType, int walletType, int recordSize);

    public void findWalletsForDatatables(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, Date dateFrom, Date dateTo);

    public List<Object[]> findCurrencyWalletTypes(String ownerId, String userType);

    public BigDecimal findInOutBalance(String ownerId, String userType, int walletType);

    public BigDecimal findInOutBalanceByTrxType(String ownerId, String userType, int walletType, String trxType);

    public BigDecimal getWalletBalanceToDate(String ownerId, String ownerType, int walletType, Date toDate);

    public BigDecimal getWalletBalanceFromDate(String ownerId, String ownerType, int walletType, Date fromDate, String trxType);

    public void findWalletTrxsForCryptoListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes);

    public WalletTrx getWalletTrx(String orderId);

    public BigDecimal getTotalIncome(String memberId, Integer walletType, List<String> trxTypeList, Date dateFrom, Date dateTo);

    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndWalleTypeAndTrxType(String ownerId, String ownerType, int walletType,
                                                                                     String trxType);

    public List<WalletTrx> findWalletTrxsByTrxTypeAndWalletType(int walletType, String trxType);
}
