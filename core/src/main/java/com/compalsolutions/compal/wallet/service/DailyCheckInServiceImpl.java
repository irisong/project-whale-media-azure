package com.compalsolutions.compal.wallet.service;


import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.wallet.dao.*;
import com.compalsolutions.compal.wallet.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;


@Component(DailyCheckInService.BEAN_NAME)
public class DailyCheckInServiceImpl implements DailyCheckInService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private DailyCheckInDao dailyCheckInDao;

    @Autowired
    private DailyCheckInMemberDao dailyCheckInMemberDao;

    @Autowired
    private WalletBalanceDao walletBalanceDao;

    @Autowired
    private WalletTrxDao walletTrxDao;

    @Override
    public Integer getDailyCheckInDetails(String memberId) {
        DailyCheckInMember dailyCheckIn = dailyCheckInMemberDao.getDailyCheckInDetailsByMemberId(memberId);

        if(dailyCheckIn == null) {
            return 0;
        }
        else {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dailyCheckIn.getLastCheckIn());

            if (isSameWithYtd(cal) || !DailyCheckIn.CONTINUE_CHECK_IN) {
                return dailyCheckIn.getDays();

            } else {
                return 0;
            }

        }

    }

    @Override
    public boolean getTodayCheckIn(String memberId) {
        DailyCheckInMember dailyCheckInMember = dailyCheckInMemberDao.getTodayCheckIn(memberId);
        if(dailyCheckInMember != null) {
            return true;
        }
        return false;
    }

    @Override
    public boolean isSameWithYtd(Calendar c) {
        Calendar ytd = Calendar.getInstance();
        ytd.add(Calendar.DAY_OF_YEAR, -1);
        if (c.get(Calendar.YEAR) == ytd.get(Calendar.YEAR)
                && c.get(Calendar.DAY_OF_YEAR) == ytd.get(Calendar.DAY_OF_YEAR)) {
            return true;
        }
        return false;
    }

    @Override
    public void doProcessDailyCheckIn(Locale locale, String memberId) {

        DailyCheckInMember dailyCheckInMember = dailyCheckInMemberDao.getDailyCheckInDetailsByMemberId(memberId);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeCn = Global.LOCALE_CN;
        boolean todayCheckIn = getTodayCheckIn(memberId);
        //check today check in
        if(todayCheckIn) {
            throw new ValidatorException(i18n.getText("invalidDailyCheckIn", locale));
        }

        int complete = 0;
        if(dailyCheckInMember != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(dailyCheckInMember.getLastCheckIn());
            if(isSameWithYtd(cal) || !DailyCheckIn.CONTINUE_CHECK_IN) {
                complete = dailyCheckInMember.getDays() % 7;
            }
        }

        int newComplete = complete + 1;

        BigDecimal pointEarn = newComplete % 7 == 0 ? DailyCheckIn.LAST_DAILY : DailyCheckIn.DAILY;

        //add record to daily check in table
        DailyCheckIn example = new DailyCheckIn(false);
        example.setMemberId(memberId);
        example.setGainDate(new Date());
        example.setGainAmount(pointEarn.intValue());
        example.setComplete(newComplete);
        example.setDescription("Daily Check In gain "+pointEarn);
        dailyCheckInDao.save(example);

        //update record to daily check in member table
        if(dailyCheckInMember != null) {
            dailyCheckInMember.setDays(newComplete);
            dailyCheckInMember.setLastCheckIn(new Date());
            dailyCheckInMemberDao.update(dailyCheckInMember);
        }else {
            DailyCheckInMember dailyCheckInMemberDb = new DailyCheckInMember();
            dailyCheckInMemberDb.setMemberId(memberId);
            dailyCheckInMemberDb.setDays(newComplete);
            dailyCheckInMemberDb.setLastCheckIn(new Date());
            dailyCheckInMemberDao.save(dailyCheckInMemberDb);
        }

        if(BDUtil.gZero(pointEarn)) { // only add record when point earn more than 0
            //add trx to wallet
            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(pointEarn);
            walletTrx.setOutAmt(BigDecimal.ZERO);
            walletTrx.setOwnerId(memberId);
            walletTrx.setOwnerType(Global.UserType.MEMBER);
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_DAILY_CHECK_IN);
            walletTrx.setWalletRefId(example.getId());
            walletTrx.setWalletType(Global.WalletType.WALLET_80);

            walletTrx.setTrxDesc(i18n.getText("dailyCheckinGain",pointEarn));
            walletTrx.setTrxDescCn(i18n.getText("dailyCheckinGain", localeCn, pointEarn));

            walletTrxDao.save(walletTrx);

            //add point into wallet
            walletBalanceDao.doCreditAvailableBalance(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_80,
                    pointEarn);
        }

    }
}