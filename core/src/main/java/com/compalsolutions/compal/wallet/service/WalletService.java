package com.compalsolutions.compal.wallet.service;

import java.math.BigDecimal;
import java.util.*;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.wallet.dto.ActiveUserReport;
import com.compalsolutions.compal.wallet.dto.DepositHistory;
import com.compalsolutions.compal.wallet.vo.*;

public interface WalletService {
    public static final String BEAN_NAME = "walletService";
    public static int DEFAULT_NO_OF_WALLET_TYPES = 4;
    /**
     * Production data stored coupon details in remark column,
     * avoid modify production data by details in remark column and retrieve by key words
     **/
    public static String REMARK_PRODUCT = "Product:";
    public static String REMARK_AMOUNT = ";Amount:";
    public static String REMARK_URL = ";Url:";

    public void doTopupAgent(Locale locale, String agentId, int walletType, double amount, String remark);

    public void findWalletTrxsForListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
                                         Date dateTo);

    public void doGenerateWalletBalanceForAllMembers();

    public void saveWalletTrx(WalletTrx walletTrx);

    public void doDeleteWalletTrx(List<WalletTrx> walletTrxs);

    public void savePointWalletTrx(PointWalletTrx pointWalletTrx);

    public BigDecimal getWalletBalance(String ownerId, String ownerType, int walletType);

    public BigDecimal getWalletBalanceByTrxType(String ownerId, String ownerType, int walletType, String trxType);

    public WalletConfig getDefaultWalletConfig();

    public WalletConfig createDefaultWalletConfig();

    public void doToggleWalletTransferForWalletConfig();

    public void doToggleWalletWithdrawForWalletConfig();

    public List<WalletTypeConfig> findActiveMemberWalletTypeConfigs();

    public List<WalletTypeConfig> findMemberWalletTypeConfigs();

    public void updateWalletTypeConfig(String ownerType, int walletType, String walletCurrency, String walletName, String status);

    public List<OptionBean> findWalletTypeForWalletAdjustment(Locale locale);

    public void createWalletAdjust(Locale locale, WalletAdjust walletAdjust);

    public void createWalletAdjust(Locale locale, WalletAdjust walletAdjust, String trxDesc, String trxDescCn);

    public void createWalletSwap(Locale locale, String memberCode, String cryptoType, double amount);

    public void findMemberWalletAdjustsForListing(DatagridModel<WalletAdjust> datagridModel, String docno, String memberCode, String adjustType,
                                                  Integer walletType, Date dateFrom, Date dateTo);

    public List<Integer> findActiveMemberWalletTypes();

    public List<Integer> findAllMemberWalletTypes();

    public BigDecimal getMemberWalletBalance(String memberId, int walletType);

    public BigDecimal getWalletBalanceFromDate(String memberId, int walletType, Date date, String trxType);

    public void findWalletTrxsForWalletStatement(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, Date dateFrom,
                                                 Date dateTo);

    public WalletTypeConfig findWalletTypeConfig(String ownerType, int walletType);

    public WalletTransferConfig getDefaultWalletTransferConfig();

    public WalletTransferConfig createDefaultWalletTransferConfig();

    public void updateWalletTransferConfig(double minimumAmount, double adminFee, boolean adminFeeByAmount);

    public List<WalletTransferTypeConfig> findAllWalletTransferTypesConfigs();

    public void saveWalletTransferTypeConfig(Locale locale, WalletTransferTypeConfig walletTransferTypeConfig);

    public WalletTransferTypeConfig getWalletTransferTypeConfig(String transferTypeId);

    public void updateWalletTransferTypeConfig(WalletTransferTypeConfig walletTransferTypeConfig);

    public void deleteWalletTransferTypeConfigAndUpdateSeq(String transferTypeId);

    public void doMoveUpWalletTransferTypeConfig(String transferTypeId);

    public void doMoveDownWalletTransferTypeConfig(String transferTypeId);

    public List<WalletTransferTypeConfig> findActiveMemberWalletTransferTypeConfigs(boolean selfTransfer);

    public void saveWalletTransfer(Locale locale, WalletTransfer walletTransfer, String receiverMemberCode);

    public void findWalletWithdrawsForListing(Locale locale, DatagridModel<WalletWithdraw> datagridModel, String memberCode, String docno, Date dateFrom,
                                              Date dateTo, Date processDateFrom, Date processDateTo, String withdrawType, String cryptoType, String status);

    public void findWalletExchanges2CryptoForListing(Locale locale, DatagridModel<WalletExchange> datagridModel, String memberCode, String docno, Date dateFrom,
                                                     Date dateTo, Date processDateFrom, Date processDateTo, String status);

    public WalletWithdraw getWalletWithdraw(String withdrawId);

    public void doApproveRejectWalletWithdraw(Locale locale, String withdrawId, String status, String remark, String processBy);

    public void doBulkApproveRejectWalletWithdraw(Locale locale, List<String> withdrawIds, String status, String remark, String processBy);

    public void doApproveRejectWalletWithdraw2Crypto(Locale locale, String exchangeId, String status, String remark, String processBy);

    public void doBulkApproveRejectWalletWithdraw2Crypto(Locale locale, List<String> exchangeIds, String status, String remark, String processBy);

    public void doCancelWalletWithdrawByMember(Locale locale, String withdrawId, String memberId);

    public void saveWalletTopup(Locale locale, WalletTopup walletTopup);

    public void findWalletTopupPackagesForListing(DatagridModel<WalletTopupPackage> datagridModel, Integer walletType, Integer packageAmount, String status);

    public List<WalletTopupPackage> getWalletTopupPackages(String status);

    public WalletTopupPackage getWalletTopupPackage(Integer walletType, Integer packageAmount, String status);

    public WalletTopupPackage findOneWalletTopupPackageByProdutId(String productId);

    public void doSaveWalletTopupPackage(Locale locale, WalletTopupPackage walletTopupPackage);

    public void doApproveWalletTopup(WalletTopup walletTopup);

    public void findWalletTopupsForListing(DatagridModel<WalletTopup> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
                                           Integer walletType, String status);

    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String cryptoType, int recordSize);

    public List<WalletTrx> findWalletTrxsByTrxTypeAndWalletType(int walletType, String trxType);

    public List<PointWalletTrx> findPointWalletTrxsByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String cryptoType, int recordSize);

    List<WalletBalance> findActiveMemberWalletBalances(String memberId);

    public WalletBalance findMemberWalletBalance(String memberId, int walletType);

    public WalletBalance doCreateWalletBalanceIfNotExists(String ownerId, String ownerType, int walletType);

    public WalletTypeConfig findActiveWalletTypeConfigByOwnerTypeAndCryptoType(String ownerType, String cryptoType);

    public void updateWalletBalance(WalletBalance walletBalance);

    public void findDepositHistories(DatagridModel<DepositHistory> datagridModel, String ownerId, String ownerType, String search);

    public void findWithdrawHistories(DatagridModel<WalletWithdraw> datagridModel, String ownerId, String ownerType, String search);

    public BigDecimal findUsedDailyWithdrawLimit(String ownerId, String ownerType);

    public List<WalletWithdraw> findWalletWithdrawsForBtcWithdraw(String withdrawStatus, String cryptoType, Integer walletType, int numberOfRecords);

    public void updateWalletWithdraw(WalletWithdraw walletWithdraw);

    public List<WalletWithdraw> findWalletWithdrawForProcessing(String cryptoType, int walletType, BigDecimal maxWithdrawAmount, int numberOfRecords);

    public List<WalletWithdraw> findWalletWithdrawsByCryptoTypesAndStatus(List<String> cryptoTypes, String status);

    public WalletWithdraw findWalletWithdrawByTxHash(String txHash);

    public List<WalletTypeConfig> findAllErc20WalletTypeConfigs();

    public WalletBalance doFindMemberWalletBalanceByMemberCodeAndCryptoType(String memberCode, String cryptoType);

    public void findWalletTrxsForCryptoListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes);

    public void findPointWalletTrxsForCryptoListing(DatagridModel<PointWalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes);

    public void saveWalletTypeConfig(WalletTypeConfig walletTypeConfig);

    public List<String> findActiveAssetCryptoTypes();

    public void doExchangeBetweenOwnWallet(Locale locale, String memberId, String currencyCodeFrom, String currencyCodeTo, BigDecimal amount,
                                           BigDecimal exchangeRate, boolean hasAdminFee);

    public WalletExchange doExchangeBetweenOwnWalletWithAmountTo(Locale locale, String memberId, String currencyCodeFrom, String currencyCodeTo,
                                                                 BigDecimal amount, BigDecimal exchangeRate, BigDecimal amountTo, boolean hasAdminFee);

    public void doExchangeLockBetweenOwnWallet(Locale locale, String memberId, String currencyCodeFrom, String currencyCodeTo, BigDecimal amountTo,
                                               BigDecimal exchangeRate, int lockPeriodInMinutes);

    public void doProcessDepositWalletTrx(String ownerId, String ownerType, String trxId, String txHash, int walletType, BigDecimal amount);

    public WalletExchangeLock findLatestActiveWalletExchangeLockByMemberId(String memberId);

    public void doCancelWalletExchangeLock(Locale locale, String memberId);

    public void doCreditAvailableWalletBalance(String ownerId, String ownerType, int walletType, BigDecimal amount);

    public void doDebitAvailableWalletBalance(String ownerId, String ownerType, int walletType, BigDecimal amount);

    public void doProcessExchangeLockIfHaveAny(String memberId, int walletType, BigDecimal amount);

    public WalletExchange getWalletExchange(String exchangeId);

    public WalletExchange getWalletExchangeWithOwner(String exchangeId);

    public void doCreateWalletTransferBetweenUser(Locale locale, String memberId, String cryptoType, String receiverPhoneno, BigDecimal amount,
                                                  String transactionPassword);

    public void getAllByOnwerIdAndStatus(DatagridModel<WalletCurrencyExchange> datagridModel, String ownerId, String status, String payTo);

    public WalletCurrencyExchange getWalletTrxRequestValid(String authId, String username);

    public String doCreateWalletTrxIdMD5(String username, String itemId, BigDecimal amount, String imageUrl);

    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndWalleTypeAndTrxType(String ownerId, String ownerType, Integer walletType, String trxTy);

    public void findTopupPackageCountForReport(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo);

    public void doTopUpWhaleCoinUsingCryptoByWebpage(Locale locale, String paymentOption, String phoneNo, String transactionPassword, BigDecimal whaleCoinAmount);

    public WalletTrx findWalletTrxByWalletRefId(String walletRefId);

    public void findTopupPackageDetailForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount);

    public void findTopupPackageDetailForListing2(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount,String memberId,String topupWay);

    public void findPrizeForReport(DatagridModel<HashMap<String,String>> datagridModel, String remark);

    public void findTopupHistoryForListing(DatagridModel<WalletTrx> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark);

    public void findOMCWalletHistoryForListing(DatagridModel<HashMap<String, String>> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark);

    public List<WalletTrx> findTopupHistoryByRemark (Date dateFrom, Date dateTo, String remark);

    public List<WalletTrx> findTopupPackageDetailByTopupWay (Date dateFrom, Date dateTo, int packageAmount, String memberCode, String topupWay);

    public void findActiveUserForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo);

    public List<ActiveUserReport> findActiveUser(Date dateFrom, Date dateTo);

    public List<Map<String, Object>> findPointWalletTrxForListing(DatagridModel<PointWalletTrx> datagridModel, Locale locale, String ownerId, String ownerType, int walletType);

    public void saveDailyActiveUserReport();
}
