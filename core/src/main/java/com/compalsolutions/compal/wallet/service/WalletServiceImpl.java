package com.compalsolutions.compal.wallet.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.compalsolutions.compal.omnicplus.OmnicplusClient;
import com.compalsolutions.compal.omnicplus.dto.OmcPaymentDto;
import com.compalsolutions.compal.omnicplus.dto.OmnicplusDto;
import com.compalsolutions.compal.paymentGateway.service.PaymentGatewayService;
import com.compalsolutions.compal.wallet.dao.*;
import com.compalsolutions.compal.wallet.dto.ActiveUserReport;
import com.compalsolutions.compal.wallet.dto.TransferReceiveTrx;
import com.compalsolutions.compal.wallet.vo.*;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.SysFormatter;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.LanguageRes;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.WalletInfoCacheUtil;
import com.compalsolutions.compal.wallet.dto.DepositHistory;

@Component(WalletService.BEAN_NAME)
public class WalletServiceImpl implements WalletService {
    private static final Object synchronizedObject = new Object();

    private PointWalletTrxDao pointWalletTrxDao;

    private WalletTrxDao walletTrxDao;

    private WalletTopupDao walletTopupDao;

    private WalletTopupPackageDao walletTopupPackageDao;

    private WalletConfigDao walletConfigDao;

    private WalletTypeConfigDao walletTypeConfigDao;

    private WalletAdjustDao walletAdjustDao;

    private WalletSqlDao walletSqlDao;

    private WalletTransferConfigDao walletTransferConfigDao;

    private WalletTransferTypeConfigDao walletTransferTypeConfigDao;

    private WalletTransferDao walletTransferDao;

    private WalletWithdrawDao walletWithdrawDao;

    private WalletWithdrawSqlDao walletWithdrawSqlDao;

    private WalletBalanceDao walletBalanceDao;

    private WalletExchangeDao walletExchangeDao;

    private WalletExchangeLockDao walletExchangeLockDao;

    private WalletCurrencyExchangeDao walletCurrencyExchangeDao;

    private DailyActiveUserReportDao dailyActiveUserReportDao;

    @Autowired
    public WalletServiceImpl(WalletTrxDao walletTrxDao, WalletTopupDao walletTopupDao, WalletTopupPackageDao walletTopupPackageDao, WalletConfigDao walletConfigDao, WalletTypeConfigDao walletTypeConfigDao,
                             WalletAdjustDao walletAdjustDao, WalletSqlDao walletSqlDao, WalletTransferConfigDao walletTransferConfigDao,
                             WalletTransferTypeConfigDao walletTransferTypeConfigDao, WalletTransferDao walletTransferDao, WalletWithdrawDao walletWithdrawDao,
                             WalletWithdrawSqlDao walletWithdrawSqlDao, WalletBalanceDao walletBalanceDao, WalletExchangeDao walletExchangeDao,
                             PointWalletTrxDao pointWalletTrxDao, WalletExchangeLockDao walletExchangeLockDao, WalletCurrencyExchangeDao walletCurrencyExchangeDao, DailyActiveUserReportDao dailyActiveUserReportDao) {
        this.walletTrxDao = walletTrxDao;
        this.walletTopupDao = walletTopupDao;
        this.walletTopupPackageDao = walletTopupPackageDao;
        this.walletConfigDao = walletConfigDao;
        this.walletTypeConfigDao = walletTypeConfigDao;
        this.walletAdjustDao = walletAdjustDao;
        this.walletSqlDao = walletSqlDao;
        this.walletTransferConfigDao = walletTransferConfigDao;
        this.walletTransferTypeConfigDao = walletTransferTypeConfigDao;
        this.walletTransferDao = walletTransferDao;
        this.walletWithdrawDao = walletWithdrawDao;
        this.walletWithdrawSqlDao = walletWithdrawSqlDao;
        this.walletBalanceDao = walletBalanceDao;
        this.walletExchangeDao = walletExchangeDao;
        this.pointWalletTrxDao = pointWalletTrxDao;
        this.walletExchangeLockDao = walletExchangeLockDao;
        this.walletCurrencyExchangeDao = walletCurrencyExchangeDao;
        this.dailyActiveUserReportDao = dailyActiveUserReportDao;
    }

    @Override
    public void doTopupAgent(Locale locale, String agentId, int walletType, double amount, String remark) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        if (amount <= 0)
            throw new ValidatorException(i18n.getText("invalidAmount", locale));

        /*
        if (!Global.WalletType.contains(walletType))
            throw new ValidatorException(i18n.getText("invalidWalletType", locale));
        */

        Agent agent = agentService.getAgent(agentId);
        if (agent == null)
            throw new ValidatorException(i18n.getText("invalidAgent", locale));

        WalletTrx walletTrx = new WalletTrx(true);
        walletTrx.setOwnerId(agent.getAgentId());
        walletTrx.setOwnerType(Global.UserType.AGENT);
        walletTrx.setWalletType(walletType);
        walletTrx.setWalletRefno("");
        walletTrx.setTrxType(Global.WalletTrxType.TOP_UP);
        walletTrx.setTrxDatetime(new Date());
        walletTrx.setInAmt(new BigDecimal(amount));
        walletTrx.setOutAmt(BigDecimal.ZERO);
        if (StringUtils.isNotBlank(remark))
            walletTrx.setRemark("Top Up : " + remark);
        else
            walletTrx.setRemark("Top Up");
        walletTrxDao.save(walletTrx);

        WalletTopup walletTopup = new WalletTopup(true);
        walletTopup.setOwnerId(walletTrx.getOwnerId());
        walletTopup.setOwnerType(walletTrx.getOwnerType());
        walletTopup.setWalletType(walletTrx.getWalletType());
        walletTopup.setAmount(amount);
        walletTopup.setWalletTrxId(walletTrx.getTrxId());
        walletTopupDao.save(walletTopup);

        walletTrx.setWalletRefId(walletTopup.getWalletTrxId());
        walletTrxDao.update(walletTrx);
    }

    @Override
    public void findWalletTrxsForListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
                                         Date dateTo) {
        walletTrxDao.findWalletsForDatagrid(datagridModel, ownerId, ownerType, walletType, dateFrom, dateTo);
    }

    @Override
    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndWalleTypeAndTrxType(String ownerId, String ownerType, Integer walletType, String trxTy) {
        return walletTrxDao.findWalletTrxsByOwnerIdAndOwnerTypeAndWalleTypeAndTrxType(ownerId, ownerType, walletType, trxTy);
    }

    @Override
    public void saveWalletTrx(WalletTrx walletTrx) {
        walletTrxDao.save(walletTrx);
    }

    @Override
    public void doDeleteWalletTrx(List<WalletTrx> walletTrxs) {
        walletTrxDao.deleteAll(walletTrxs);
    }

    @Override
    public void savePointWalletTrx(PointWalletTrx pointWalletTrx) {
        pointWalletTrxDao.save(pointWalletTrx);
    }

    @Override
    public BigDecimal getWalletBalance(String ownerId, String ownerType, int walletType) {
        return walletTrxDao.findInOutBalance(ownerId, ownerType, walletType);
    }

    @Override
    public BigDecimal getWalletBalanceByTrxType(String ownerId, String ownerType, int walletType, String trxType) {
        return walletTrxDao.findInOutBalanceByTrxType(ownerId, ownerType, walletType, trxType);
    }

    @Override
    public WalletTransferConfig getDefaultWalletTransferConfig() {
        return walletTransferConfigDao.getDefault();
    }

    @Override
    public WalletTransferConfig createDefaultWalletTransferConfig() {
        WalletTransferConfig walletTransferConfig = getDefaultWalletTransferConfig();
        if (walletTransferConfig != null) {
            throw new DataException("walletTransferConfig is not empty. Can not create default walletTransferConfig!");
        }

        walletTransferConfig = new WalletTransferConfig(true);
        walletTransferConfig.setTranferTargetGroups("");
        walletTransferConfigDao.save(walletTransferConfig);

        return walletTransferConfig;
    }

    @Override
    public void doGenerateWalletBalanceForAllMembers() {
        MemberService memberService = Application.lookupBean(MemberService.class);

        List<Member> members = memberService.findAllMembers();

        for (Member member : members) {
            doCreateWalletBalanceIfNotExists(member.getMemberId(), Global.UserType.MEMBER, Global.WalletType.WALLET_81);
            doCreateWalletBalanceIfNotExists(member.getMemberId(), Global.UserType.MEMBER, Global.WalletType.WALLET_50);
        }
        members.clear();
    }

    @Override
    public void updateWalletTransferConfig(double minimumAmount, double adminFee, boolean adminFeeByAmount) {
        WalletTransferConfig walletTransferConfig = getDefaultWalletTransferConfig();
        if (walletTransferConfig == null) {
            throw new DataException("walletTransferConfig is not exists!");
        }

        walletTransferConfig.setMinimumAmount(minimumAmount);
        walletTransferConfig.setAdminFee(adminFee);
        walletTransferConfig.setAdminFeeByAmount(adminFeeByAmount);
        walletTransferConfigDao.update(walletTransferConfig);
    }

    @Override
    public List<WalletTransferTypeConfig> findAllWalletTransferTypesConfigs() {
        return walletTransferTypeConfigDao.findAll();
    }

    @Override
    public void saveWalletTransferTypeConfig(Locale locale, WalletTransferTypeConfig walletTransferTypeConfig) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        WalletTransferTypeConfig walletTransferTypeConfigDb = walletTransferTypeConfigDao.findWalletTransferTypeConfig(
                walletTransferTypeConfig.getTransferSelf(), walletTransferTypeConfig.getWalletTypeFrom(), walletTransferTypeConfig.getWalletTypeTo());
        if (walletTransferTypeConfigDb != null) {
            throw new ValidatorException(i18n.getText("walletTransferTypeExists"));
        }

        List<WalletTransferTypeConfig> walletTransferTypeConfigs = findAllWalletTransferTypesConfigs();

        walletTransferTypeConfig.setSeq(walletTransferTypeConfigs.size() + 1);
        walletTransferTypeConfigDao.save(walletTransferTypeConfig);
    }

    @Override
    public WalletTransferTypeConfig getWalletTransferTypeConfig(String transferTypeId) {
        return walletTransferTypeConfigDao.get(transferTypeId);
    }

    @Override
    public void updateWalletTransferTypeConfig(WalletTransferTypeConfig walletTransferTypeConfig) {
        WalletTransferTypeConfig dbWalletTransferTypeConfig = walletTransferTypeConfigDao.get(walletTransferTypeConfig.getTransferTypeId());

        dbWalletTransferTypeConfig.setTransferSelf(walletTransferTypeConfig.getTransferSelf());
        dbWalletTransferTypeConfig.setWalletTypeFrom(walletTransferTypeConfig.getWalletTypeFrom());
        dbWalletTransferTypeConfig.setWalletTypeTo(walletTransferTypeConfig.getWalletTypeTo());
        dbWalletTransferTypeConfig.setStatus(walletTransferTypeConfig.getStatus());
        walletTransferTypeConfigDao.update(dbWalletTransferTypeConfig);
    }

    @Override
    public void deleteWalletTransferTypeConfigAndUpdateSeq(String transferTypeId) {
        WalletTransferTypeConfig walletTransferTypeConfig = getWalletTransferTypeConfig(transferTypeId);
        int seq = walletTransferTypeConfig.getSeq();

        List<WalletTransferTypeConfig> walletTransferTypeConfigs = findAllWalletTransferTypesConfigs();
        walletTransferTypeConfigs.remove(seq - 1);

        walletTransferTypeConfigDao.delete(walletTransferTypeConfig);
        doUpdateSeqForWalletTransferTypeConfigs(walletTransferTypeConfigs);
    }

    @Override
    public void doMoveUpWalletTransferTypeConfig(String transferTypeId) {
        WalletTransferTypeConfig walletTransferTypeConfig = getWalletTransferTypeConfig(transferTypeId);
        int seq = walletTransferTypeConfig.getSeq();

        List<WalletTransferTypeConfig> walletTransferTypeConfigs = findAllWalletTransferTypesConfigs();
        walletTransferTypeConfigs.remove(seq - 1);
        walletTransferTypeConfigs.add(seq - 2, walletTransferTypeConfig);

        doUpdateSeqForWalletTransferTypeConfigs(walletTransferTypeConfigs);
    }

    @Override
    public void doMoveDownWalletTransferTypeConfig(String transferTypeId) {
        WalletTransferTypeConfig walletTransferTypeConfig = getWalletTransferTypeConfig(transferTypeId);
        int seq = walletTransferTypeConfig.getSeq();

        List<WalletTransferTypeConfig> walletTransferTypeConfigs = findAllWalletTransferTypesConfigs();
        walletTransferTypeConfigs.remove(seq - 1);
        walletTransferTypeConfigs.add(seq, walletTransferTypeConfig);

        doUpdateSeqForWalletTransferTypeConfigs(walletTransferTypeConfigs);
    }

    @Override
    public List<WalletTransferTypeConfig> findActiveMemberWalletTransferTypeConfigs(boolean selfTransfer) {
        return walletTransferTypeConfigDao.findActiveMemberWalletTransferTypeConfigs(selfTransfer);
    }

    @Override
    public void saveWalletTransfer(Locale locale, WalletTransfer walletTransfer, String receiverMemberCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/
            if (!walletTransfer.getWalletTypeFrom().equals(walletTransfer.getWalletTypeTo())) {
                throw new ValidatorException("Unsupported wallet type from " + walletTransfer.getWalletTypeFrom() + ", to " + walletTransfer.getWalletTypeTo());
            }

            boolean isUsd = Global.WalletType.WALLET_50 == walletTransfer.getWalletTypeFrom();

            WalletTransferConfig walletTransferConfig = getDefaultWalletTransferConfig();

            // checking wallet transfer type
            WalletTransferTypeConfig walletTransferTypeConfig = walletTransferTypeConfigDao.findActive(walletTransfer.getTransferSelf(),
                    walletTransfer.getWalletTypeFrom(), walletTransfer.getWalletTypeTo());
            if (walletTransferTypeConfig == null) {
                throw new ValidatorException(i18n.getText("invalidWalletTransferType", locale));
            }

            // checking receiver
            Member receiver = null;
            if (!walletTransfer.getTransferSelf()) {
                receiver = memberService.findMemberByMemberCode(receiverMemberCode);
                if (receiver == null) {
                    throw new ValidatorException(i18n.getText("invalidReceiverId"), locale);
                }
            }

            WalletTypeConfig walletTypeConfig = findWalletTypeConfig(walletTransfer.getOwnerType(), walletTransfer.getWalletTypeFrom());
            if (walletTypeConfig == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            Pair<BigDecimal, BigDecimal> minAmountAndFee = Global.getWalletTransferMiminumAmountAndFee(walletTypeConfig.getCryptoType());

            BigDecimal minAmount = minAmountAndFee.getLeft();
            // checking minimum transfer amount
            if (BDUtil.l(walletTransfer.getAmount(), minAmount)) {
                throw new ValidatorException(i18n.getText("minimumTransferAmountError", locale, minAmount));
            }

            // create walletBalance in case it not exists in DB.
            WalletBalance walletBalance = doCreateWalletBalanceIfNotExists(walletTransfer.getOwnerId(), walletTransfer.getOwnerType(),
                    walletTransfer.getWalletTypeFrom());
            WalletBalance walletBalanceTo = doCreateWalletBalanceIfNotExists(walletTransfer.getReceiverId(), walletTransfer.getReceiverType(),
                    walletTransfer.getWalletTypeTo());

            BigDecimal adminFee = BigDecimal.ZERO;

            if (!walletTransfer.getTransferSelf()) {
                if (walletTransferConfig.getAdminFeeByAmount()) {
                    adminFee = new BigDecimal(walletTransferConfig.getAdminFee());
                    adminFee = BDUtil.roundUp6dp(adminFee);
                } else {
                    adminFee = walletTransfer.getAmount().multiply(new BigDecimal(walletTransferConfig.getAdminFee())).divide(new BigDecimal(100.0),
                            BigDecimal.ROUND_HALF_UP, 6);
                }
            }

            if (isUsd) {
                adminFee = BDUtil.roundUp2dp(adminFee);
            }

            BigDecimal totalAmount = walletTransfer.getAmount().add(adminFee);

            if (isUsd) {
                totalAmount = BDUtil.roundUp2dp(totalAmount);
            }

            if (BDUtil.g(totalAmount, walletBalance.getAvailableBalance())) {
                throw new ValidatorException(i18n.getText("insufficientWalletCredit", locale));
            }

            /***********************
             * VALIDATION - END
             ***********************/

            walletTransfer.setDocno(docNoService.doGetNextWalletTransferDocNo());
            walletTransfer.setReceiverId(walletTransfer.getTransferSelf() ? walletTransfer.getOwnerId() : receiver.getMemberId());
            walletTransfer.setAdminFee(adminFee);
            walletTransfer.setTotalAmount(totalAmount);
            walletTransfer.setTrxDatetime(new Date());

            if (!walletTransfer.getTransferSelf()) {
                walletTransfer.setConfAdminFee(new BigDecimal(walletTransferConfig.getAdminFee()));
                walletTransfer.setConfAdminFeeByAmount(walletTransferConfig.getAdminFeeByAmount());
            }

            walletTransferDao.save(walletTransfer);

            /************************
             * WALLET_TRX - START
             ************************/

            Locale localeCn = Global.LOCALE_CN;
            Member owner = memberService.getMember(walletTransfer.getOwnerId());
            String remark = StringUtils.isBlank(walletTransfer.getRemark()) ? "-" : walletTransfer.getRemark();

            String trxDescFromRemark = null;
            String trxDescFrom = null;
            String trxDescFromCn = null;
            String trxDescToRemark = null;
            String trxDescTo = null;
            String trxDescToCn = null;

            // using systemLocale
            if (walletTransfer.getTransferSelf()) {
                trxDescFrom = i18n.getText("walletTrxForSelfTransferWalletTransferTo", walletTransfer.getDocno(),
                        WebUtil.getWalletNameWithCurrency(locale, walletTransfer.getWalletTypeTo()), remark);
                trxDescFromCn = i18n.getText("walletTrxForSelfTransferWalletTransferTo", localeCn, walletTransfer.getDocno(),
                        WebUtil.getWalletNameWithCurrency(locale, walletTransfer.getWalletTypeTo()), remark);
                trxDescTo = i18n.getText("walletTrxForSelfTransferWalletTransferFrom", walletTransfer.getDocno(),
                        WebUtil.getWalletNameWithCurrency(locale, walletTransfer.getWalletTypeFrom()), remark);
                trxDescToCn = i18n.getText("walletTrxForSelfTransferWalletTransferFrom", localeCn, walletTransfer.getDocno(),
                        WebUtil.getWalletNameWithCurrency(locale, walletTransfer.getWalletTypeFrom()), remark);
            } else {
                trxDescFromRemark = i18n.getText("walletTrxForTransferToMemberWalletTransferTo", walletTransfer.getDocno(), receiver.getMemberCode(), remark);
                trxDescFrom = i18n.getText("walletTrxForTransferToMemberWalletTransferTo", walletTransfer.getDocno(), receiver.getMemberCodeHide(), remark);
                trxDescFromCn = i18n.getText("walletTrxForTransferToMemberWalletTransferTo", localeCn, walletTransfer.getDocno(), receiver.getMemberCodeHide(),
                        remark);

                trxDescToRemark = i18n.getText("walletTrxForTransferToMemberWalletTransferFrom", walletTransfer.getDocno(), owner.getMemberCode(), remark);
                trxDescTo = i18n.getText("walletTrxForTransferToMemberWalletTransferFrom", walletTransfer.getDocno(), owner.getMemberCodeHide(), remark);
                trxDescToCn = i18n.getText("walletTrxForTransferToMemberWalletTransferFrom", localeCn, walletTransfer.getDocno(), owner.getMemberCodeHide(),
                        remark);
            }

            // wallet trx from
            WalletTrx walletTrxFrom = new WalletTrx(true);
            walletTrxFrom.setInAmt(BigDecimal.ZERO);
            walletTrxFrom.setOutAmt(walletTransfer.getTotalAmount());
            walletTrxFrom.setOwnerId(walletTransfer.getOwnerId());
            walletTrxFrom.setOwnerType(walletTransfer.getOwnerType());
            walletTrxFrom.setTrxDatetime(new Date());
            walletTrxFrom.setTrxType(Global.WalletTrxType.TRANSFER);
            walletTrxFrom.setWalletRefId(walletTransfer.getTransferId());
            walletTrxFrom.setWalletRefno(walletTransfer.getDocno());
            walletTrxFrom.setWalletType(walletTransfer.getWalletTypeFrom());
            walletTrxFrom.setTrxDesc(trxDescFrom);
            walletTrxFrom.setTrxDescCn(trxDescFromCn);
            walletTrxFrom.setRemark(trxDescFromRemark);
            walletTrxDao.save(walletTrxFrom);

            // wallet trx to
            WalletTrx walletTrxTo = new WalletTrx(true);
            walletTrxTo.setInAmt(walletTransfer.getAmount());
            walletTrxTo.setOutAmt(BigDecimal.ZERO);
            walletTrxTo.setOwnerId(walletTransfer.getReceiverId());
            walletTrxTo.setOwnerType(walletTransfer.getReceiverType());
            walletTrxTo.setTrxDatetime(new Date());
            walletTrxTo.setTrxType(Global.WalletTrxType.TRANSFER);
            walletTrxTo.setWalletRefId(walletTransfer.getTransferId());
            walletTrxTo.setWalletRefno(walletTransfer.getDocno());
            walletTrxTo.setWalletType(walletTransfer.getWalletTypeTo());
            walletTrxTo.setTrxDesc(trxDescTo);
            walletTrxTo.setTrxDescCn(trxDescToCn);
            walletTrxTo.setRemark(trxDescToRemark);
            walletTrxDao.save(walletTrxTo);

            /************************
             * WALLET_TRX - END
             ************************/

            walletTransfer.setWalletTrxIdFrom(walletTrxFrom.getTrxId());
            walletTransfer.setWalletTrxIdTo(walletTrxTo.getTrxId());
            walletTransferDao.update(walletTransfer);

            walletBalanceDao.doDebitAvailableBalance(walletTransfer.getOwnerId(), walletTransfer.getOwnerType(), walletTransfer.getWalletTypeFrom(),
                    walletTransfer.getTotalAmount());
            walletBalanceDao.doCreditAvailableBalance(walletTransfer.getReceiverId(), walletTransfer.getReceiverType(), walletTransfer.getWalletTypeTo(),
                    walletTransfer.getAmount());
        }
    }

    @Override
    public void findWalletWithdrawsForListing(Locale locale, DatagridModel<WalletWithdraw> datagridModel, String memberCode, String docno, Date dateFrom,
                                              Date dateTo, Date processDateFrom, Date processDateTo, String withdrawType, String cryptoType, String status) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        walletSqlDao.findWalletWithdrawsForDatagrid(datagridModel, memberCode, docno, dateFrom, dateTo, processDateFrom, processDateTo, withdrawType, cryptoType, status);

        for (WalletWithdraw walletWithdraw : datagridModel.getRecords()) {
            walletWithdraw.setStatusDesc(i18n.getText(Global.Status.getI18nKey(walletWithdraw.getStatus())));
        }
    }

    @Override
    public void findWalletExchanges2CryptoForListing(Locale locale, DatagridModel<WalletExchange> datagridModel, String memberCode, String docno, Date dateFrom,
                                                     Date dateTo, Date processDateFrom, Date processDateTo, String status) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        walletSqlDao.findWalletExchanges2CryptoForDatagrid(datagridModel, memberCode, docno, dateFrom, dateTo, processDateFrom, processDateTo, status);

        for (WalletExchange walletExchange : datagridModel.getRecords()) {
            walletExchange.setStatusDesc(i18n.getText(Global.Status.getI18nKey(walletExchange.getStatus())));
            walletExchange.setCryptoTypeFrom(WebUtil.getWalletCurrency(locale, walletExchange.getWalletTypeFrom()));
            walletExchange.setCryptoTypeTo(WebUtil.getWalletCurrency(locale, walletExchange.getWalletTypeTo()));
        }
    }

    @Override
    public WalletWithdraw getWalletWithdraw(String withdrawId) {
        MemberService memberService = Application.lookupBean(MemberService.class);

        WalletWithdraw walletWithdraw = walletWithdrawDao.get(withdrawId);

        if (walletWithdraw == null)
            return null;

        if (Global.UserType.MEMBER.equalsIgnoreCase(walletWithdraw.getOwnerType())) {
            Member member = memberService.getMember(walletWithdraw.getOwnerId());
            walletWithdraw.setOwnerCode(member.getMemberCode());
            walletWithdraw.setOwnerName(member.getFullName());
        }

        return walletWithdraw;
    }

    @Override
    public void doApproveRejectWalletWithdraw(Locale locale, String withdrawId, String status, String remark, String processBy) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeCn = Global.LOCALE_CN;

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/

            WalletWithdraw walletWithdraw = getWalletWithdraw(withdrawId);

            if (walletWithdraw == null) {
                throw new ValidatorException(i18n.getText("invalidWalletWithdrawal", locale));
            }

            if (!Global.Status.PENDING.equalsIgnoreCase(walletWithdraw.getStatus())) {
                throw new ValidatorException("You can not approve or reject this withdrawal because status is " + walletWithdraw.getStatus());
            }

            if (Global.Status.REJECTED.equalsIgnoreCase(status) && StringUtils.isBlank(remark)) {
                throw new ValidatorException(i18n.getText("youMustEnterRemarkWhenYouRejectIt", locale));
            }

            /***********************
             * VALIDATION - END
             ***********************/

            if (Global.Status.REJECTED.equalsIgnoreCase(status)) {
                /************************
                 * WALLET_TRX - START
                 ************************/

                // wallet trx from
                WalletTrx walletTrx = new WalletTrx(true);
                walletTrx.setInAmt(walletWithdraw.getTotalAmount());
                walletTrx.setOutAmt(BigDecimal.ZERO);
                walletTrx.setOwnerId(walletWithdraw.getOwnerId());
                walletTrx.setOwnerType(walletWithdraw.getOwnerType());
                walletTrx.setTrxDatetime(new Date());
                // walletTrx.setTrxType(Global.WalletTrxType.WITHDRAW_REJECT);
                walletTrx.setTrxType(Global.WalletTrxType.TRANSFER_REJECT);
                walletTrx.setWalletRefId(walletWithdraw.getWithdrawId());
                walletTrx.setWalletRefno(walletWithdraw.getDocno());
                walletTrx.setWalletType(walletWithdraw.getWalletType());

                // using systemLocale
                // walletTrx.setTrxDesc(i18n.getText("walletTrxForRejectedWalletWithdraw", walletWithdraw.getDocno(),
                // remark));
                walletTrx.setTrxDesc(i18n.getText("walletTrxForRejectedTransferWithdraw", walletWithdraw.getDocno(), remark));
                walletTrx.setTrxDescCn(i18n.getText("walletTrxForRejectedTransferWithdraw", localeCn, walletWithdraw.getDocno(), remark));
                walletTrxDao.save(walletTrx);

                walletBalanceDao.doCreditAvailableBalance(walletWithdraw.getOwnerId(), walletWithdraw.getOwnerType(), walletWithdraw.getWalletType(),
                        walletWithdraw.getTotalAmount());

                /************************
                 * WALLET_TRX - END
                 ************************/

                walletWithdraw.setRejectRemark(remark);
                walletWithdraw.setRejectWalletTrxId(walletTrx.getTrxId());

            } else if (StringUtils.isNotBlank(remark)) {
                walletWithdraw.setRemark(remark);
            }

            walletWithdraw.setStatus(status);
            walletWithdraw.setProcessBy(processBy);
            walletWithdraw.setProcessDatetime(new Date());
            walletWithdrawDao.update(walletWithdraw);
        }
    }

    @Override
    public void doBulkApproveRejectWalletWithdraw(Locale locale, List<String> withdrawIds, String status, String remark, String processBy) {
        for (String withdrawId : withdrawIds) {
            doApproveRejectWalletWithdraw(locale, withdrawId, status, remark, processBy);
        }
    }

    @Override
    public void doApproveRejectWalletWithdraw2Crypto(Locale locale, String exchangeId, String status, String remark, String processBy) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Locale localeCn = Global.LOCALE_CN;

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/

            WalletExchange walletExchange = getWalletExchange(exchangeId);

            if (walletExchange == null) {
                throw new ValidatorException(i18n.getText("invalidWalletWithdrawal", locale));
            }

            if (!Global.Status.PENDING.equalsIgnoreCase(walletExchange.getStatus())) {
                throw new ValidatorException("You can not approve or reject this withdrawal because status is " + walletExchange.getStatus());
            }

            if (Global.Status.REJECTED.equalsIgnoreCase(status) && StringUtils.isBlank(remark)) {
                throw new ValidatorException(i18n.getText("youMustEnterRemarkWhenYouRejectIt", locale));
            }

            if (!Global.Status.APPROVED.equalsIgnoreCase(status) && !Global.Status.REJECTED.equalsIgnoreCase(status)) {
                throw new ValidatorException("Invalid status " + status);
            }

            /***********************
             * VALIDATION - END
             ***********************/

            if (Global.Status.REJECTED.equalsIgnoreCase(status)) {
                /************************
                 * WALLET_TRX - START
                 ************************/

                // wallet trx
                WalletTrx walletTrx = new WalletTrx(true);
                walletTrx.setInAmt(walletExchange.getTotalAmount());
                walletTrx.setOutAmt(BigDecimal.ZERO);
                walletTrx.setOwnerId(walletExchange.getOwnerId());
                walletTrx.setOwnerType(walletExchange.getOwnerType());
                walletTrx.setTrxDatetime(new Date());
                walletTrx.setTrxType(Global.WalletTrxType.EXCHANGE2CRYPTO_REJECT);
                walletTrx.setWalletRefId(walletExchange.getExchangeId());
                walletTrx.setWalletRefno(walletExchange.getDocno());
                walletTrx.setWalletType(walletExchange.getWalletTypeFrom());

                // using systemLocale
                walletTrx.setTrxDesc(i18n.getText("walletTrxForRejectedWalletWithdraw", walletExchange.getDocno(), remark));
                walletTrx.setTrxDescCn(i18n.getText("walletTrxForRejectedWalletWithdraw", localeCn, walletExchange.getDocno(), remark));
                walletTrxDao.save(walletTrx);

                walletBalanceDao.doCreditAvailableBalance(walletExchange.getOwnerId(), walletExchange.getOwnerType(), walletExchange.getWalletTypeFrom(),
                        walletExchange.getTotalAmount());

                /************************
                 * WALLET_TRX - END
                 ************************/

                walletExchange.setRejectRemark(remark);
                walletExchange.setRejectWalletTrxId(walletTrx.getTrxId());

            } else if (Global.Status.APPROVED.equalsIgnoreCase(status)) {
                /************************
                 * WALLET_TRX - START
                 ************************/

                // wallet trx to
                WalletTrx walletTrx = new WalletTrx(true);
                walletTrx.setInAmt(walletExchange.getAmountTo());
                walletTrx.setOutAmt(BigDecimal.ZERO);
                walletTrx.setOwnerId(walletExchange.getOwnerId());
                walletTrx.setOwnerType(walletExchange.getOwnerType());
                walletTrx.setTrxDatetime(new Date());
                walletTrx.setTrxType(Global.WalletTrxType.EXCHANGE2CRYPTO);
                walletTrx.setWalletRefId(walletExchange.getExchangeId());
                walletTrx.setWalletRefno(walletExchange.getDocno());
                walletTrx.setWalletType(walletExchange.getWalletTypeTo());

                // using systemLocale
                walletTrx.setTrxDesc(i18n.getText("walletTrxForExchange2Crypto", walletExchange.getDocno(), remark));
                walletTrx.setTrxDescCn(i18n.getText("walletTrxForExchange2Crypto", localeCn, walletExchange.getDocno(), remark));
                walletTrxDao.save(walletTrx);

                walletBalanceDao.doCreditAvailableBalance(walletExchange.getOwnerId(), walletExchange.getOwnerType(), walletExchange.getWalletTypeTo(),
                        walletExchange.getAmountTo());

                /************************
                 * WALLET_TRX - END
                 ************************/

                walletExchange.setWalletTrxIdTo(walletTrx.getTrxId());

                if (StringUtils.isNotBlank(remark))
                    walletExchange.setRemark(remark);
            }

            walletExchange.setStatus(status);
            walletExchange.setProcessBy(processBy);
            walletExchange.setProcessDatetime(new Date());
            walletExchangeDao.update(walletExchange);
        }
    }

    @Override
    public void doBulkApproveRejectWalletWithdraw2Crypto(Locale locale, List<String> exchangeIds, String status, String remark, String processBy) {
        for (String exchangeId : exchangeIds) {
            doApproveRejectWalletWithdraw2Crypto(locale, exchangeId, status, remark, processBy);
        }
    }

    @Override
    public void doCancelWalletWithdrawByMember(Locale locale, String withdrawId, String memberId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {

            /***********************
             * VALIDATION - START
             ***********************/
            WalletWithdraw walletWithdraw = getWalletWithdraw(withdrawId);

            if (walletWithdraw == null) {
                throw new ValidatorException(i18n.getText("invalidWalletWithdrawal", locale));
            }

            if (!walletWithdraw.getOwnerId().equalsIgnoreCase(memberId)) {
                throw new ValidatorException(i18n.getText("invalidWalletWithdrawal", locale));
            }

            if (!Global.Status.PENDING.equalsIgnoreCase(walletWithdraw.getStatus())) {
                throw new ValidatorException("You can not cancel this withdrawal because status is " + walletWithdraw.getStatus());
            }

            /***********************
             * VALIDATION - END
             ***********************/

            /************************
             * WALLET_TRX - START
             ************************/

            // wallet trx from
            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(walletWithdraw.getTotalAmount());
            walletTrx.setOutAmt(BigDecimal.ZERO);
            walletTrx.setOwnerId(walletWithdraw.getOwnerId());
            walletTrx.setOwnerType(walletWithdraw.getOwnerType());
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.WITHDRAW_CANCEL);
            walletTrx.setWalletRefId(walletWithdraw.getWithdrawId());
            walletTrx.setWalletRefno(walletWithdraw.getDocno());
            walletTrx.setWalletType(walletWithdraw.getWalletType());

            // using systemLocale
            walletTrx.setTrxDesc(i18n.getText("walletTrxForCanceledWalletWithdraw", walletWithdraw.getDocno()));
            walletTrxDao.save(walletTrx);

            /************************
             * WALLET_TRX - END
             ************************/

            walletWithdraw.setRejectWalletTrxId(walletTrx.getTrxId());
            walletWithdraw.setStatus(Global.Status.CANCELLED);
            walletWithdrawDao.update(walletWithdraw);
        }
    }

    @Override
    public void saveWalletTopup(Locale locale, WalletTopup walletTopup) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/

            Member member = memberService.getMember(walletTopup.getOwnerId());
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            // checking wallet type
            if (!Global.WalletTopupConf.walletTypes.contains(walletTopup.getWalletType())) {
                throw new ValidatorException(i18n.getText("invalidWalletType", locale));
            }

            // checking minimum topup amount
            if (walletTopup.getAmount() < Global.WalletTopupConf.minimumAmount) {
                throw new ValidatorException(i18n.getText("minimumWalletToupAmountIsX", locale, Global.WalletTopupConf.minimumAmount));
            }

            /***********************
             * VALIDATION - END
             ***********************/

            walletTopup.setDocno(docNoService.doGetNextWalletTopupDocNo());
            walletTopup.setTrxDatetime(new Date());
            walletTopupDao.save(walletTopup);
        }
    }

    @Override
    public void findWalletTopupPackagesForListing(DatagridModel<WalletTopupPackage> datagridModel, Integer walletType, Integer packageAmount, String status) {
        walletTopupPackageDao.findWalletTopupPackagesForDatagrid(datagridModel, walletType, packageAmount, status);
    }

    @Override
    public List<WalletTopupPackage> getWalletTopupPackages(String status) {
        return walletTopupPackageDao.findWalletTopupPackages(status);
    }

    @Override
    public WalletTopupPackage getWalletTopupPackage(Integer walletType, Integer packageAmount, String status) {
        return walletTopupPackageDao.findOneWalletTopupPackage(walletType, packageAmount, status);
    }

    @Override
    public WalletTopupPackage findOneWalletTopupPackageByProdutId(String productId) {
        return walletTopupPackageDao.findOneWalletTopupPackageByProdutId(productId);
    }


    @Override
    public void doSaveWalletTopupPackage(Locale locale, WalletTopupPackage walletTopupPackage) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/

            if ((walletTopupPackage.getPackageAmount() != null) && (walletTopupPackage.getPackageAmount() <= 0)) {
                throw new ValidatorException(i18n.getText("minimumWalletToupPackageIsX", 1, locale));
            }

            if (!walletTopupPackage.getStatus().equals(Global.WalletPackageStatus.DISPLAY) && !walletTopupPackage.getStatus().equals(Global.WalletPackageStatus.HIDE)) {
                throw new ValidatorException(i18n.getText("invalidStatus", locale));
            }

            /***********************
             * VALIDATION - END
             ***********************/

            WalletTopupPackage topupPackage = walletTopupPackageDao.findWalletTopupPackage(walletTopupPackage.getWalletType(), walletTopupPackage.getPackageAmount());
            if (topupPackage != null) {
                topupPackage.setStatus(walletTopupPackage.getStatus());
                walletTopupPackageDao.update(topupPackage);
            } else {
                walletTopupPackageDao.save(walletTopupPackage);
            }

        }
    }

    private void doUpdateSeqForWalletTransferTypeConfigs(List<WalletTransferTypeConfig> walletTransferTypeConfigs) {
        int seq = 1;
        for (WalletTransferTypeConfig walletTransferTypeConfig : walletTransferTypeConfigs) {
            if (walletTransferTypeConfig.getSeq() != seq) {
                walletTransferTypeConfig.setSeq(seq);
                walletTransferTypeConfigDao.update(walletTransferTypeConfig);
            }

            seq++;
        }
    }

    @Override
    public BigDecimal getMemberWalletBalance(String memberId, int walletType) {
        return getWalletBalance(memberId, Global.UserType.MEMBER, walletType);
    }

    @Override
    public BigDecimal getWalletBalanceFromDate(String memberId, int walletType, Date date, String trxType) {
        return walletTrxDao.getWalletBalanceFromDate(memberId, Global.UserType.MEMBER, walletType, date, trxType);
    }

    @Override
    public void findWalletTrxsForWalletStatement(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, Date dateFrom,
                                                 Date dateTo) {
        walletTrxDao.findWalletsForDatatables(datagridModel, ownerId, ownerType, walletType, dateFrom, dateTo);

        BigDecimal startBalance = BigDecimal.ZERO;

        if (dateTo != null)
            startBalance = walletTrxDao.getWalletBalanceToDate(ownerId, ownerType, walletType, dateTo);
        else
            startBalance = walletTrxDao.findInOutBalance(ownerId, ownerType, walletType);

        if (datagridModel.getCurrentPage() > 1) {
            BigDecimal deductBalance = walletSqlDao.getDeductBalanceForWalletStatement(ownerId, ownerType, walletType, dateFrom, dateTo,
                    datagridModel.getCurrentPage(), datagridModel.getPageSize());
            startBalance = startBalance.subtract(deductBalance);
        }

        for (WalletTrx walletTrx : datagridModel.getRecords()) {
            walletTrx.setBalance(startBalance);

            // inAmt > 0
            if (BDUtil.gZero(walletTrx.getInAmt())) {
                startBalance = startBalance.subtract(walletTrx.getInAmt());
            } else {
                startBalance = startBalance.add(walletTrx.getOutAmt());
            }
        }
    }

    @Override
    public WalletTypeConfig findWalletTypeConfig(String ownerType, int walletType) {
        return walletTypeConfigDao.findWalletTypeConfig(ownerType, walletType);
    }

    @Override
    public WalletConfig getDefaultWalletConfig() {
        return walletConfigDao.getDefault();
    }

    @Override
    public WalletConfig createDefaultWalletConfig() {
        WalletConfig walletConfig = getDefaultWalletConfig();
        if (walletConfig != null) {
            throw new DataException("walletConfig is not empty. Can not create default walletConfig!");
        }

        walletConfig = new WalletConfig(true);
        walletConfigDao.save(walletConfig);

        List<WalletTypeConfig> walletTypeConfigs = walletTypeConfigDao.findMemberWalletTypesConfigs();
        if (CollectionUtil.isNotEmpty(walletTypeConfigs)) {
            throw new DataException("walletTypeConfigs are not empty. Can not create default walletConfig!");
        }

        for (int i = 1; i <= DEFAULT_NO_OF_WALLET_TYPES; i++) {
            WalletTypeConfig walletTypeConfig = new WalletTypeConfig(true);
            walletTypeConfig.setOwnerType(Global.UserType.MEMBER);
            walletTypeConfig.setWalletType(i * 10);
            walletTypeConfigDao.save(walletTypeConfig);
        }

        return walletConfig;
    }

    @Override
    public void doToggleWalletTransferForWalletConfig() {
        WalletConfig walletConfig = getDefaultWalletConfig();
        if (walletConfig == null) {
            walletConfig = createDefaultWalletConfig();
        }

        walletConfig.setWalletTransfer(!walletConfig.getWalletTransfer());
        walletConfigDao.update(walletConfig);
    }

    @Override
    public void doToggleWalletWithdrawForWalletConfig() {
        WalletConfig walletConfig = getDefaultWalletConfig();
        if (walletConfig == null) {
            walletConfig = createDefaultWalletConfig();
        }

        walletConfig.setWalletWithdraw(!walletConfig.getWalletWithdraw());
        walletConfigDao.update(walletConfig);
    }

    @Override
    public List<WalletTypeConfig> findActiveMemberWalletTypeConfigs() {
        return walletTypeConfigDao.findActiveMemberWalletTypes();
    }

    @Override
    public List<WalletTypeConfig> findMemberWalletTypeConfigs() {
        return walletTypeConfigDao.findMemberWalletTypesConfigs();
    }

    @Override
    public void updateWalletTypeConfig(String ownerType, int walletType, String walletCurrency, String walletName, String status) {
        LanguageFrameworkService languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
        Locale systemLocale = languageFrameworkService.getSystemLocale();

        String walletNameKey = "WALLET_" + walletType;
        List<LanguageRes> walletNameResList = languageFrameworkService.findLanguageResesByKeyCode(walletNameKey);
        if (CollectionUtil.isEmpty(walletNameResList)) {
            LanguageRes languageRes = new LanguageRes(true);
            languageRes.setLanguageCode(systemLocale.toString());
            languageRes.setKeyCode(walletNameKey);
            languageRes.setResValue(walletName);
            languageFrameworkService.saveLanguageRes(languageRes);
        } else {
            for (LanguageRes languageRes : walletNameResList) {
                languageRes.setResValue(walletName);
                languageFrameworkService.updateLanguageRes(languageRes);
            }
        }

        String walletCurrencyKey = "WALLET_CUR_" + walletType;
        List<LanguageRes> walletCurrencyList = languageFrameworkService.findLanguageResesByKeyCode(walletCurrencyKey);
        if (CollectionUtil.isEmpty(walletCurrencyList)) {
            LanguageRes languageRes = new LanguageRes(true);
            languageRes.setLanguageCode(systemLocale.toString());
            languageRes.setKeyCode(walletCurrencyKey);
            languageRes.setResValue(walletCurrency);
            languageFrameworkService.saveLanguageRes(languageRes);
        } else {
            for (LanguageRes languageRes : walletCurrencyList) {
                languageRes.setResValue(walletCurrency);
                languageFrameworkService.updateLanguageRes(languageRes);
            }
        }

        languageFrameworkService.resetLanguageCache();

        WalletTypeConfig walletTypeConfig = findWalletTypeConfig(ownerType, walletType);
        walletTypeConfig.setStatus(status);
        walletTypeConfigDao.update(walletTypeConfig);

        WalletInfoCacheUtil.clearCache();
    }

    @Override
    public List<OptionBean> findWalletTypeForWalletAdjustment(Locale locale) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        List<OptionBean> optionBeans = new ArrayList<>();

        List<WalletTypeConfig> walletTypeConfigs = findActiveMemberWalletTypeConfigs();
        for (WalletTypeConfig walletTypeConfig : walletTypeConfigs) {
            String value = i18n.getText("WALLET_CUR_" + walletTypeConfig.getWalletType()) + " - " + i18n.getText("WALLET_" + walletTypeConfig.getWalletType());

            optionBeans.add(new OptionBean(String.valueOf(walletTypeConfig.getWalletType()), value));
        }

        return optionBeans;
    }

    @Override
    public void createWalletAdjust(Locale locale, WalletAdjust walletAdjust) {
        createWalletAdjust(locale, walletAdjust, null, null);
    }

    @Override
    public void createWalletAdjust(Locale locale, WalletAdjust walletAdjust, String trxDesc, String trxDescCn) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);

        if (!Global.UserType.MEMBER.equalsIgnoreCase(walletAdjust.getOwnerType())) {
            throw new ValidatorException("Invalid ownerType " + walletAdjust.getOwnerType());
        }

        Member member = memberService.getMember(walletAdjust.getOwnerId());
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }
        if (BDUtil.leZero(walletAdjust.getAmount())) {
            throw new ValidatorException("Amount must greater than 0");
        }

        // round up to 8 Decimal Point
        walletAdjust.setAmount(BDUtil.roundUp8dp(walletAdjust.getAmount()));

        BigDecimal amount = walletAdjust.getAmount();

        walletAdjust.setTrxDatetime(new Date());
        walletAdjust.setDocno(docNoService.doGetNextWalletAdjustDocNo());
        walletAdjustDao.save(walletAdjust);
        if (Global.WalletType.WALLET_81 != walletAdjust.getWalletType()) {
            WalletTrx walletTrx = new WalletTrx(true);
            if (Global.WalletAdjustType.IN.equalsIgnoreCase(walletAdjust.getAdjustType())) {
                walletTrx.setInAmt(amount);
                walletTrx.setOutAmt(BigDecimal.ZERO);
            } else {
                walletTrx.setInAmt(BigDecimal.ZERO);
                walletTrx.setOutAmt(amount);
            }
            walletTrx.setOwnerId(walletAdjust.getOwnerId());
            walletTrx.setOwnerType(walletAdjust.getOwnerType());
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.ADJUST);
            walletTrx.setWalletRefId(walletAdjust.getAdjustId());
            walletTrx.setWalletRefno(walletAdjust.getDocno());
            walletTrx.setWalletType(walletAdjust.getWalletType());

            // using systemLocale
            if (StringUtils.isNotBlank(trxDesc) && StringUtils.isNotBlank(trxDescCn)) {
                walletTrx.setTrxDesc(trxDesc);
                walletTrx.setTrxDescCn(trxDescCn);
            } else {
                if (StringUtils.isBlank(walletAdjust.getRemark())) {
                    walletTrx.setTrxDesc(i18n.getText("walletTrxForWalletAdjust", walletAdjust.getDocno()));
                    walletTrx.setTrxDescCn(i18n.getText("walletTrxForWalletAdjust", Global.LOCALE_CN, walletAdjust.getDocno()));
                } else {
                    walletTrx.setTrxDesc(i18n.getText("walletTrxForWalletAdjustWithRemark", walletAdjust.getDocno(), walletAdjust.getRemark()));
                    walletTrx.setTrxDescCn(i18n.getText("walletTrxForWalletAdjustWithRemark", Global.LOCALE_CN, walletAdjust.getDocno(), walletAdjust.getRemark()));
                }
            }
            walletTrxDao.save(walletTrx);

            walletAdjust.setWalletTrxId(walletTrx.getTrxId());
        } else {
            PointWalletTrx walletTrx = new PointWalletTrx(true);
            if (Global.WalletAdjustType.IN.equalsIgnoreCase(walletAdjust.getAdjustType())) {
                walletTrx.setInAmt(amount);
                walletTrx.setOutAmt(BigDecimal.ZERO);
            } else {
                walletTrx.setInAmt(BigDecimal.ZERO);
                walletTrx.setOutAmt(amount);
            }
            walletTrx.setOwnerId(walletAdjust.getOwnerId());
            walletTrx.setOwnerType(walletAdjust.getOwnerType());
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.ADJUST);
            walletTrx.setWalletRefId(walletAdjust.getAdjustId());
            walletTrx.setWalletRefno(walletAdjust.getDocno());
            walletTrx.setWalletType(walletAdjust.getWalletType());

            // using systemLocale
            if (StringUtils.isNotBlank(trxDesc) && StringUtils.isNotBlank(trxDescCn)) {
                walletTrx.setTrxDesc(trxDesc);
                walletTrx.setTrxDescCn(trxDescCn);
            } else {
                if (StringUtils.isBlank(walletAdjust.getRemark())) {
                    walletTrx.setTrxDesc(i18n.getText("walletTrxForWalletAdjust", walletAdjust.getDocno()));
                    walletTrx.setTrxDescCn(i18n.getText("walletTrxForWalletAdjust", Global.LOCALE_CN, walletAdjust.getDocno()));
                } else {
                    walletTrx.setTrxDesc(i18n.getText("walletTrxForWalletAdjustWithRemark", walletAdjust.getDocno(), walletAdjust.getRemark()));
                    walletTrx.setTrxDescCn(i18n.getText("walletTrxForWalletAdjustWithRemark", Global.LOCALE_CN, walletAdjust.getDocno(), walletAdjust.getRemark()));
                }
            }
            pointWalletTrxDao.save(walletTrx);

            walletAdjust.setWalletTrxId(walletTrx.getPointTrxId());
        }

        walletAdjustDao.update(walletAdjust);

        doCreateWalletBalanceIfNotExists(member.getMemberId(), Global.UserType.MEMBER, walletAdjust.getWalletType());
        if (Global.WalletAdjustType.IN.equalsIgnoreCase(walletAdjust.getAdjustType())) {
            walletBalanceDao.doCreditAvailableBalance(walletAdjust.getOwnerId(), walletAdjust.getOwnerType(), walletAdjust.getWalletType(), amount);
        } else {
            walletBalanceDao.doDebitAvailableBalance(walletAdjust.getOwnerId(), walletAdjust.getOwnerType(), walletAdjust.getWalletType(), amount);
        }
    }

    @Override
    public void createWalletSwap(Locale locale, String memberCode, String cryptoType, double amount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);
        WalletService walletService = Application.lookupBean(WalletService.class);

        synchronized (synchronizedObject) {
            Member member = memberService.findMemberByMemberCode(memberCode);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (amount <= 0) {
                throw new ValidatorException("Amount must greater than 0");
            }

            if (StringUtils.isBlank(cryptoType)) {
                throw new ValidatorException("Invalid Crypto");
            }

            WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, cryptoType);
            if (walletTypeConfig == null) {
                throw new ValidatorException("Invalid cryptoType " + cryptoType);
            }
            // CR - PGC9
            if (walletTypeConfig.getWalletType() != 30) { // KTTC1 placeholder
                throw new ValidatorException("Invalid cryptoType " + cryptoType);
            }

            String docRefNo = docNoService.doGetNextWalletSwapDocNo();

            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(new BigDecimal(amount));
            walletTrx.setOutAmt(BigDecimal.ZERO);
            walletTrx.setOwnerId(member.getMemberId());
            walletTrx.setOwnerType(Global.UserType.MEMBER);
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.GHL_SWAP);
            walletTrx.setWalletRefId(member.getMemberId());
            walletTrx.setWalletRefno(docRefNo);
            walletTrx.setWalletType(walletTypeConfig.getWalletType());
            walletTrx.setTrxDesc("Wallet Swap " + " - " + docRefNo);
            walletTrxDao.save(walletTrx);

            WalletBalance walletBalance = doCreateWalletBalanceIfNotExists(member.getMemberId(), Global.UserType.MEMBER, walletTypeConfig.getWalletType());
            walletBalance.setAvailableBalance(walletBalance.getAvailableBalance() //
                    .add(walletTrx.getInAmt()) //
                    .subtract(walletTrx.getOutAmt()));
            walletBalance.updateTotalBalance();
            updateWalletBalance(walletBalance);
        }
    }

    @Override
    public void findMemberWalletAdjustsForListing(DatagridModel<WalletAdjust> datagridModel, String docno, String memberCode, String adjustType,
                                                  Integer walletType, Date dateFrom, Date dateTo) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        walletAdjustDao.findMemberWalletAdjustsForDatagrid(datagridModel, docno, memberCode, adjustType, walletType, dateFrom, dateTo);

        for (WalletAdjust walletAdjust : datagridModel.getRecords()) {
            Member member = memberService.getMember(walletAdjust.getOwnerId());
            walletAdjust.setOwnerName(member.getFullName());
            walletAdjust.setOwnerUsername(member.getMemberCode());
        }
    }

    @Override
    public List<Integer> findActiveMemberWalletTypes() {
        List<WalletTypeConfig> walletTypeConfigs = findActiveMemberWalletTypeConfigs();
        List<Integer> walletTypes = new ArrayList<>();
        for (WalletTypeConfig walletTypeConfig : walletTypeConfigs) {
            walletTypes.add(walletTypeConfig.getWalletType());
        }

        return walletTypes;
    }

    @Override
    public List<Integer> findAllMemberWalletTypes() {
        List<WalletTypeConfig> walletTypeConfigs = findMemberWalletTypeConfigs();

        List<Integer> walletTypes = new ArrayList<>();
        for (WalletTypeConfig walletTypeConfig : walletTypeConfigs) {
            walletTypes.add(walletTypeConfig.getWalletType());
        }

        return walletTypes;
    }

    @Override
    public void doApproveWalletTopup(WalletTopup walletTopup) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        if (!Global.Status.PAYMENT_PENDING.equalsIgnoreCase(walletTopup.getStatus())) {
            throw new ValidatorException(
                    "WalletTopup " + walletTopup.getTopupId() + " status is not " + Global.Status.PAYMENT_PENDING + " (" + walletTopup.getStatus() + ")");
        }

        WalletTrx walletTrx = new WalletTrx(true);
        walletTrx.setInAmt(new BigDecimal(walletTopup.getAmount()));
        walletTrx.setOutAmt(BigDecimal.ZERO);
        walletTrx.setOwnerId(walletTopup.getOwnerId());
        walletTrx.setOwnerType(walletTopup.getOwnerType());
        walletTrx.setTrxDatetime(new Date());
        walletTrx.setTrxType(Global.WalletTrxType.TOP_UP);
        walletTrx.setWalletRefId(walletTopup.getTopupId());
        walletTrx.setWalletRefno(walletTopup.getDocno());
        walletTrx.setWalletType(walletTopup.getWalletType());

        // using systemLocale
        walletTrx.setTrxDesc(i18n.getText("walletTrxForWalletTopup", walletTopup.getDocno()));
        walletTrxDao.save(walletTrx);

        walletTopup.setStatus(Global.Status.APPROVED);
        walletTopup.setWalletTrxId(walletTrx.getTrxId());
        walletTopupDao.update(walletTopup);
    }

    @Override
    public void findWalletTopupsForListing(DatagridModel<WalletTopup> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
                                           Integer walletType, String status) {
        walletSqlDao.findWalletTopupsForDatagrid(datagridModel, memberCode, docno, dateFrom, dateTo, walletType, status);
    }

    @Override
    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String cryptoType, int recordSize) {
        WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(ownerType, cryptoType);

        return walletTrxDao.findWalletTrxsByOwnerIdAndOwnerTypeAndWalleType(ownerId, ownerType, walletTypeConfig.getWalletType(), recordSize);
    }

    @Override
    public List<WalletTrx> findWalletTrxsByTrxTypeAndWalletType(int walletType, String trxType) {
        return walletTrxDao.findWalletTrxsByTrxTypeAndWalletType(walletType, trxType);
    }

    @Override
    public List<PointWalletTrx> findPointWalletTrxsByOwnerIdAndOwnerTypeAndCryptoType(String ownerId, String ownerType, String cryptoType, int recordSize) {
        WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(ownerType, cryptoType);

        return pointWalletTrxDao.findPointWalletTrxsByOwnerIdAndOwnerTypeAndWalleType(ownerId, ownerType, walletTypeConfig.getWalletType(), recordSize);
    }

    @Override
    public List<WalletBalance> findActiveMemberWalletBalances(String memberId) {
        List<Integer> activeWalletTypes = WebUtil.getActiveMemberWalletTypes();
        return activeWalletTypes.stream().map(//
                walletType -> {
                    WalletBalance walletBalance = findMemberWalletBalance(memberId, walletType);
                    if (walletBalance == null) {
                        walletBalance = new WalletBalance(true);
                        walletBalance.setWalletType(walletType);
                        walletBalance.setTotalBalance(BigDecimal.ZERO);
                        walletBalance.setAvailableBalance(BigDecimal.ZERO);
                        walletBalance.setInOrderBalance(BigDecimal.ZERO);
                    }

                    return walletBalance;
                }).collect(Collectors.toList());
    }

    @Override
    public WalletBalance findMemberWalletBalance(String memberId, int walletType) {
        return walletBalanceDao.findByOwnerIdAndOwnerTypeAndWalletType(memberId, Global.UserType.MEMBER, walletType);
    }

    @Override
    public WalletBalance doCreateWalletBalanceIfNotExists(String ownerId, String ownerType, int walletType) {
        WalletBalance walletBalance = walletBalanceDao.findByOwnerIdAndOwnerTypeAndWalletType(ownerId, ownerType, walletType);
        if (walletBalance == null) {
            walletBalance = new WalletBalance(true);
            walletBalance.setOwnerId(ownerId);
            walletBalance.setOwnerType(ownerType);
            walletBalance.setWalletType(walletType);
            walletBalance.setTotalBalance(BigDecimal.ZERO);
            walletBalance.setAvailableBalance(BigDecimal.ZERO);
            walletBalance.setInOrderBalance(BigDecimal.ZERO);
            walletBalanceDao.save(walletBalance);
        } else {
            walletBalanceDao.refresh(walletBalance);
        }

        return walletBalance;
    }

    @Override
    public WalletTypeConfig findActiveWalletTypeConfigByOwnerTypeAndCryptoType(String ownerType, String cryptoType) {
        return walletTypeConfigDao.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, cryptoType);
    }

    @Override
    public void updateWalletBalance(WalletBalance walletBalance) {
        walletBalanceDao.update(walletBalance);
    }

    @Override
    public void findDepositHistories(DatagridModel<DepositHistory> datagridModel, String ownerId, String ownerType, String search) {
        walletSqlDao.findDepositHistories(datagridModel, ownerId, ownerType, search);
    }

    @Override
    public void findWithdrawHistories(DatagridModel<WalletWithdraw> datagridModel, String ownerId, String ownerType, String search) {
        walletWithdrawDao.findWithdrawHistories(datagridModel, ownerId, ownerType, search);
    }

    @Override
    public BigDecimal findUsedDailyWithdrawLimit(String ownerId, String ownerType) {
        return walletSqlDao.findUsedDailyWithdrawLimit(ownerId, ownerType);
    }

    @Override
    public List<WalletWithdraw> findWalletWithdrawsForBtcWithdraw(String withdrawStatus, String cryptoType, Integer walletType, int numberOfRecords) {
        return walletWithdrawSqlDao.findWalletWithdrawsForBtcWithdraw(withdrawStatus, cryptoType, walletType, numberOfRecords);
    }

    @Override
    public void updateWalletWithdraw(WalletWithdraw walletWithdraw) {
        walletWithdrawDao.update(walletWithdraw);
    }

    @Override
    public List<WalletWithdraw> findWalletWithdrawForProcessing(String cryptoType, int walletType, BigDecimal maxWithdrawAmount, int numberOfRecords) {
        return walletWithdrawDao.findWalletWithdrawForProcessing(cryptoType, walletType, maxWithdrawAmount, numberOfRecords);
    }

    @Override
    public List<WalletWithdraw> findWalletWithdrawsByCryptoTypesAndStatus(List<String> cryptoTypes, String status) {
        return walletWithdrawDao.findWalletWithdrawsByCryptoTypesAndStatus(cryptoTypes, status);
    }

    @Override
    public WalletWithdraw findWalletWithdrawByTxHash(String txHash) {
        return walletWithdrawDao.findWalletWithdrawByTxHash(txHash);
    }

    @Override
    public List<WalletTypeConfig> findAllErc20WalletTypeConfigs() {
        return walletTypeConfigDao.findAllErc20WalletTypeConfigs();
    }

    @Override
    public WalletBalance doFindMemberWalletBalanceByMemberCodeAndCryptoType(String memberCode, String cryptoType) {
        MemberService memberService = Application.lookupBean(MemberService.class);
        Member member = memberService.findMemberByMemberCode(memberCode);

        WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(Global.UserType.MEMBER, cryptoType);
        return doCreateWalletBalanceIfNotExists(member.getMemberId(), Global.UserType.MEMBER, walletTypeConfig.getWalletType());
    }

    @Override
    public void findWalletTrxsForCryptoListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes) {
        walletTrxDao.findWalletTrxsForCryptoListing(datagridModel, ownerId, ownerType, walletType, trxTypes);
    }

    @Override
    public void findPointWalletTrxsForCryptoListing(DatagridModel<PointWalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes) {
        pointWalletTrxDao.findPointWalletTrxsForCryptoListing(datagridModel, ownerId, ownerType, walletType, trxTypes);
    }

    @Override
    public void saveWalletTypeConfig(WalletTypeConfig walletTypeConfig) {
        walletTypeConfigDao.save(walletTypeConfig);
    }

    @Override
    public void getAllByOnwerIdAndStatus(DatagridModel<WalletCurrencyExchange> datagridModel, String ownerId, String status, String payTo) {
        walletCurrencyExchangeDao.getAllByOnwerIdAndStatus(datagridModel, ownerId, status, payTo);
    }

    @Override
    public List<String> findActiveAssetCryptoTypes() {
        return walletSqlDao.findActiveAssetCryptoTypes();
    }

    private static final Object synchronizedExchangeBetweenOwnWalletObject = new Object();

    @Override
    public void doExchangeBetweenOwnWallet(Locale locale, String memberId, String currencyCodeFrom, String currencyCodeTo, BigDecimal amount,
                                           BigDecimal exchangeRate, boolean hasAdminFee) {
        doExchangeBetweenOwnWalletWithAmountTo(locale, memberId, currencyCodeFrom, currencyCodeTo, amount, exchangeRate, null, hasAdminFee);
    }

    @Override
    public WalletExchange doExchangeBetweenOwnWalletWithAmountTo(Locale locale, String memberId, String currencyCodeFrom, String currencyCodeTo,
                                                                 BigDecimal amount, BigDecimal exchangeRate, BigDecimal amountTo, boolean hasAdminFee) {
        I18n i18n = Application.lookupBean(I18n.class);
        Locale localeCn = Global.LOCALE_CN;
        DocNoService docNoService = Application.lookupBean(DocNoService.class);
        MemberService memberService = Application.lookupBean(MemberService.class);

        String ownerType = Global.UserType.MEMBER;

        synchronized (synchronizedExchangeBetweenOwnWalletObject) {
            /***********************
             * VALIDATION - START
             ***********************/
            Member member = memberService.getMember(memberId);
            currencyCodeFrom = StringUtils.upperCase(currencyCodeFrom);
            if (StringUtils.isBlank(currencyCodeFrom)) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            WalletTypeConfig walletTypeConfigFrom = findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeFrom);
            if (walletTypeConfigFrom == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            WalletTypeConfig walletTypeConfigTo = findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeTo);
            if (walletTypeConfigTo == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            if (amount == null || BDUtil.leZero(amount)) {
                throw new ValidatorException(i18n.getText("invalidAmount", locale));
            }

            boolean isExchangeToUsd = Global.WalletType.USD.equalsIgnoreCase(walletTypeConfigTo.getCryptoType());

            // checking wallet balance
            WalletBalance walletBalance = doCreateWalletBalanceIfNotExists(memberId, ownerType, walletTypeConfigFrom.getWalletType());

            BigDecimal balance = walletBalance.getAvailableBalance();

            WalletExchange walletExchange = new WalletExchange(true);
            walletExchange.setOwnerId(memberId);
            walletExchange.setOwnerType(ownerType);
            walletExchange.setWalletTypeFrom(walletTypeConfigFrom.getWalletType());
            walletExchange.setWalletTypeTo(walletTypeConfigTo.getWalletType());
            walletExchange.setAmount(amount);

            if (!hasAdminFee) {
                walletExchange.setAdminFee(BigDecimal.ZERO);
                walletExchange.setTotalAmount(walletExchange.getAmount().add(walletExchange.getAdminFee()));
            } else {
                walletExchange.setTotalAmount(amount);

                BigDecimal adminFee = BDUtil.roundUp2dp(walletExchange.getTotalAmount().multiply(new BigDecimal("0.05")));
                walletExchange.setAdminFee(adminFee);

                walletExchange.setAmount(walletExchange.getTotalAmount().subtract(adminFee));
            }
            walletExchange.setRate(exchangeRate);

            if (amountTo != null) {
                walletExchange.setAmountTo(amountTo);
            } else {
                if (isExchangeToUsd) {
                    BigDecimal _amountTo = BDUtil.roundUp2dp(amount.multiply(exchangeRate));
                    walletExchange.setAmountTo(_amountTo);
                } else {
                    BigDecimal _amountTo = BDUtil.roundUp6dp(walletExchange.getAmount().multiply(exchangeRate));

                    if (Global.WalletType.GGS.equalsIgnoreCase(currencyCodeTo)) {
                        if (!BDUtil.g(_amountTo.subtract(Global.GGS_PROCESSING_FEE), BigDecimal.ZERO)) {
                            throw new ValidatorException(i18n.getText("invalidAmount", locale));
                        } else {
                            _amountTo = _amountTo.subtract(Global.GGS_PROCESSING_FEE);
                        }
                    }
                    walletExchange.setAmountTo(_amountTo);
                }
            }

            if (BDUtil.g(walletExchange.getTotalAmount(), balance)) {
                throw new ValidatorException(i18n.getText("insufficientWalletCredit", locale));
            }

            /***********************
             * VALIDATION - END
             ***********************/
            doDebitAvailableWalletBalance(memberId, ownerType, walletTypeConfigFrom.getWalletType(), walletExchange.getTotalAmount());

            doCreateWalletBalanceIfNotExists(memberId, ownerType, walletTypeConfigTo.getWalletType());
            doCreditAvailableWalletBalance(memberId, ownerType, walletTypeConfigTo.getWalletType(), walletExchange.getAmountTo());

            walletExchange.setDocno(isExchangeToUsd ? docNoService.doGetNextWalletExchangeDocNo() : docNoService.doGetNextWalletExchange2CryptoDocNo());
            walletExchange.setTrxDatetime(new Date());

            walletExchangeDao.save(walletExchange);

            /************************
             * WALLET_TRX - START
             ************************/

            // wallet trx from
            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(BigDecimal.ZERO);
            walletTrx.setOutAmt(walletExchange.getTotalAmount());
            walletTrx.setOwnerId(walletExchange.getOwnerId());
            walletTrx.setOwnerType(walletExchange.getOwnerType());
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(!hasAdminFee ? Global.WalletTrxType.EXCHANGE : Global.WalletTrxType.EXCHANGE2CRYPTO);
            walletTrx.setWalletRefId(walletExchange.getExchangeId());
            walletTrx.setWalletRefno(walletExchange.getDocno());
            walletTrx.setWalletType(walletExchange.getWalletTypeFrom());

            // using systemLocale
            if (!hasAdminFee) {
                walletTrx.setTrxDesc(i18n.getText("walletTrxForExchange", walletExchange.getDocno()));
                walletTrx.setTrxDescCn(i18n.getText("walletTrxForExchange", localeCn, walletExchange.getDocno()));
            } else {
                walletTrx.setTrxDesc(i18n.getText("walletTrxForExchange2Crypto", walletExchange.getDocno()));
                walletTrx.setTrxDescCn(i18n.getText("walletTrxForExchange2Crypto", localeCn, walletExchange.getDocno()));
            }
            walletTrxDao.save(walletTrx);

            walletExchange.setWalletTrxIdFrom(walletTrx.getTrxId());

            // wallet trx to
            WalletTrx walletTrxTo = new WalletTrx(true);
            walletTrxTo.setInAmt(walletExchange.getAmountTo());
            walletTrxTo.setOutAmt(BigDecimal.ZERO);
            walletTrxTo.setOwnerId(walletExchange.getOwnerId());
            walletTrxTo.setOwnerType(walletExchange.getOwnerType());
            walletTrxTo.setTrxDatetime(new Date());
            walletTrxTo.setTrxType(!hasAdminFee ? Global.WalletTrxType.EXCHANGE : Global.WalletTrxType.EXCHANGE2CRYPTO);
            walletTrxTo.setWalletRefId(walletExchange.getExchangeId());
            walletTrxTo.setWalletRefno(walletExchange.getDocno());
            walletTrxTo.setWalletType(walletExchange.getWalletTypeTo());

            // using systemLocale
            if (!hasAdminFee) {
                walletTrxTo.setTrxDesc(i18n.getText("walletTrxForExchange", walletExchange.getDocno()));
                walletTrxTo.setTrxDescCn(i18n.getText("walletTrxForExchange", localeCn, walletExchange.getDocno()));
            } else {
                walletTrxTo.setTrxDesc(i18n.getText("walletTrxForExchange2Crypto", walletExchange.getDocno()));
                walletTrxTo.setTrxDescCn(i18n.getText("walletTrxForExchange2Crypto", localeCn, walletExchange.getDocno()));
            }

            walletTrxDao.save(walletTrxTo);

            walletExchange.setWalletTrxIdTo(walletTrxTo.getTrxId());

            /************************
             * WALLET_TRX - END
             ************************/

            walletExchangeDao.update(walletExchange);
            return walletExchange;
        }
    }

    @Override
    public void doExchangeLockBetweenOwnWallet(Locale locale, String memberId, String currencyCodeFrom, String currencyCodeTo, BigDecimal amountTo,
                                               BigDecimal exchangeRate, int lockPeriodInMinutes) {
        I18n i18n = Application.lookupBean(I18n.class);

        /***********************
         * VALIDATION - START
         ***********************/

        currencyCodeFrom = StringUtils.upperCase(currencyCodeFrom);
        if (StringUtils.isBlank(currencyCodeFrom)) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeFrom));
        }

        WalletTypeConfig walletTypeConfigFrom = findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeFrom);
        if (walletTypeConfigFrom == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeFrom));
        }

        currencyCodeTo = StringUtils.upperCase(currencyCodeTo);
        if (StringUtils.isBlank(currencyCodeTo)) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeTo));
        }

        WalletTypeConfig walletTypeConfigTo = findActiveWalletTypeConfigByOwnerTypeAndCryptoType(Global.UserType.MEMBER, currencyCodeTo);
        if (walletTypeConfigTo == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCodeX", locale, currencyCodeTo));
        }

        if (amountTo == null || BDUtil.leZero(amountTo)) {
            throw new ValidatorException(i18n.getText("invalidAmount", locale));
        }

        WalletExchangeLock prevLock = walletExchangeLockDao.findLatestActiveWalletExchangeLockByMemberId(memberId);
        if (prevLock != null && prevLock.getValidUntil().after(new Date())) {
            throw new ValidatorException("Please cancel previous locked price before you lock another new price");
        }

        /***********************
         * VALIDATION - END
         ***********************/

        WalletExchangeLock walletExchangeLock = new WalletExchangeLock(true);
        walletExchangeLock.setOwnerId(memberId);
        walletExchangeLock.setOwnerType(Global.UserType.MEMBER);
        walletExchangeLock.setWalletTypeFrom(walletTypeConfigFrom.getWalletType());
        walletExchangeLock.setWalletTypeTo(walletTypeConfigTo.getWalletType());
        walletExchangeLock.setRate(exchangeRate);
        walletExchangeLock.setAmountTo(amountTo);
        walletExchangeLock.setAmount(amountTo.divide(exchangeRate, 6, BigDecimal.ROUND_HALF_UP));
        walletExchangeLock.setTrxDatetime(new Date());
        walletExchangeLock.setValidUntil(DateUtil.addMinute(new Date(), lockPeriodInMinutes));
        walletExchangeLockDao.save(walletExchangeLock);
    }

    @Override
    public void doProcessDepositWalletTrx(String ownerId, String ownerType, String trxId, String txHash, int walletType, BigDecimal amount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        WalletBalance walletBalance = doCreateWalletBalanceIfNotExists(ownerId, ownerType, walletType);
        if (BDUtil.gZero(amount)) {
            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(amount);
            walletTrx.setOutAmt(BigDecimal.ZERO);
            walletTrx.setOwnerId(ownerId);
            walletTrx.setOwnerType(ownerType);
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.DEPOSIT);
            walletTrx.setWalletRefId(trxId);
            walletTrx.setWalletRefno(txHash);
            walletTrx.setWalletType(walletType);

            // using systemLocale
            String desc = i18n.getText("walletTrxForDeposit", txHash);
            String descCn = i18n.getText("walletTrxForDeposit", Global.LOCALE_CN, txHash);
            walletTrx.setTrxDesc(desc);
            walletTrx.setTrxDescCn(descCn);
            saveWalletTrx(walletTrx);

            doCreditAvailableWalletBalance(ownerId, ownerType, walletType, amount);
            doProcessExchangeLockIfHaveAny(ownerId, walletType, amount);
        }
    }

    @Override
    public WalletExchangeLock findLatestActiveWalletExchangeLockByMemberId(String memberId) {
        return walletExchangeLockDao.findLatestActiveWalletExchangeLockByMemberId(memberId);
    }

    @Override
    public void doCancelWalletExchangeLock(Locale locale, String memberId) {
        WalletExchangeLock walletExchangeLock = findLatestActiveWalletExchangeLockByMemberId(memberId);
        if (walletExchangeLock != null) {
            walletExchangeLock.setStatus(Global.Status.CANCELLED);
            walletExchangeLockDao.update(walletExchangeLock);
        }
    }

    @Override
    public void doCreditAvailableWalletBalance(String ownerId, String ownerType, int walletType, BigDecimal amount) {
        walletBalanceDao.doCreditAvailableBalance(ownerId, ownerType, walletType, amount);
    }

    @Override
    public void doDebitAvailableWalletBalance(String ownerId, String ownerType, int walletType, BigDecimal amount) {
        walletBalanceDao.doDebitAvailableBalance(ownerId, ownerType, walletType, amount);
    }

    @Override
    public void doProcessExchangeLockIfHaveAny(String memberId, int walletType, BigDecimal amount) {
        Locale locale = Application.lookupBean(Locale.class);
        WalletExchangeLock walletExchangeLock = findLatestActiveWalletExchangeLockByMemberId(memberId);

        final String userType = Global.UserType.MEMBER;

        if (walletExchangeLock != null && walletExchangeLock.getValidUntil().after(new Date()) //
                && walletExchangeLock.getWalletTypeFrom() == walletType //
                && BDUtil.ge(amount, walletExchangeLock.getAmount())) {

            WalletTypeConfig walletTypeConfigFrom = findWalletTypeConfig(userType, walletExchangeLock.getWalletTypeFrom());
            WalletTypeConfig walletTypeConfigTo = findWalletTypeConfig(userType, walletExchangeLock.getWalletTypeTo());

            WalletExchange walletExchange = doExchangeBetweenOwnWalletWithAmountTo(locale, memberId, walletTypeConfigFrom.getCryptoType(),
                    walletTypeConfigTo.getCryptoType(), walletExchangeLock.getAmount(), walletExchangeLock.getRate(), walletExchangeLock.getAmountTo(), false);

            walletExchangeLock.setStatus(Global.Status.USED);
            walletExchangeLock.setExchangeId(walletExchange.getExchangeId());
            walletExchangeLockDao.update(walletExchangeLock);
        }
    }

    @Override
    public WalletExchange getWalletExchange(String exchangeId) {
        return walletExchangeDao.get(exchangeId);
    }

    @Override
    public WalletExchange getWalletExchangeWithOwner(String exchangeId) {
        MemberService memberService = Application.lookupBean(MemberService.class);

        WalletExchange walletExchange = walletExchangeDao.get(exchangeId);

        if (walletExchange == null) {
            return null;
        }

        if (Global.UserType.MEMBER.equalsIgnoreCase(walletExchange.getOwnerType())) {
            Member member = memberService.getMember(walletExchange.getOwnerId());
            walletExchange.setOwnerCode(member.getMemberCode());
            walletExchange.setOwnerName(member.getFullName());
        }

        return walletExchange;
    }

    @Override
    public void doCreateWalletTransferBetweenUser(Locale locale, String memberId, String cryptoType, String receiverMemberCode, BigDecimal amount,
                                                  String transactionPassword) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        final String userType = Global.UserType.MEMBER;
        cryptoType = StringUtils.upperCase(cryptoType);

        boolean isUsd = Global.WalletType.USD.equalsIgnoreCase(cryptoType);

        /***********************
         * VALIDATION - START
         ***********************/

        memberId = StringUtils.upperCase(memberId);
        receiverMemberCode = StringUtils.upperCase(receiverMemberCode);

        Member member = memberService.getMember(memberId);
        Member memberReceiver = memberService.findMemberByMemberCode(receiverMemberCode);

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMemberId", locale));
        }

        if (memberReceiver == null) {
            throw new ValidatorException(i18n.getText("invalidReceiverPhoneno", locale));
        }

        if (member.getMemberId().equalsIgnoreCase(memberReceiver.getMemberId())) {
            throw new ValidatorException(i18n.getText("youCanNotTransferToYourself", locale));
        }

        if (StringUtils.isBlank(transactionPassword)) {
            throw new ValidatorException(i18n.getText("invalidTransactionPassword", locale));
        }

        MemberUser memberUser = memberService.findMemberUserByMemberId(memberId);

        if (memberUser == null) {
            throw new ValidatorException(i18n.getText("invalidUser", locale));
        }

        if (!userDetailsService.isEncryptedPasswordMatch(transactionPassword, memberUser.getPassword2())) {
            throw new ValidatorException(i18n.getText("invalidTransactionPassword", locale));
        }

        if (StringUtils.isBlank(cryptoType)) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }

        WalletTypeConfig walletTypeConfig = findActiveWalletTypeConfigByOwnerTypeAndCryptoType(userType, cryptoType);

        if (walletTypeConfig == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }

        if (isUsd) {
            // get number without decimal point
            int _amount = amount.intValue();
            amount = new BigDecimal(_amount);
        } else {
            amount = BDUtil.roundUp6dp(amount);
        }

        Pair<BigDecimal, BigDecimal> minAmountAndFee = Global.getWalletTransferMiminumAmountAndFee(cryptoType);
        BigDecimal minAmount = minAmountAndFee.getLeft();

        if (BDUtil.l(amount, minAmount)) {
            throw new ValidatorException(i18n.getText("minimumTransferAmountIsX", locale, SysFormatter.formatForex(minAmount)));
        }

        /***********************
         * VALIDATION - END
         ***********************/

        WalletTransfer walletTransfer = new WalletTransfer(true);
        walletTransfer.setTransferSelf(false);
        walletTransfer.setOwnerId(member.getMemberId());
        walletTransfer.setOwnerType(userType);
        walletTransfer.setReceiverId(memberReceiver.getMemberId());
        walletTransfer.setReceiverType(userType);
        walletTransfer.setWalletTypeFrom(walletTypeConfig.getWalletType());
        walletTransfer.setWalletTypeTo(walletTypeConfig.getWalletType());
        walletTransfer.setAmount(amount);

        saveWalletTransfer(locale, walletTransfer, receiverMemberCode);
    }

    public WalletCurrencyExchange getWalletTrxRequestValid(String authId, String username) {
        return walletCurrencyExchangeDao.getWalletCurrencyExchangeTrxValid(authId, username);
    }

    /**
     * Create wallet Trx Id to keep track of third party request permission
     * Usage: Third party must be able to provide trxId that exists
     **/
    public String doCreateWalletTrxIdMD5(String username, String itemId, BigDecimal amount, String imageUrl) {
        // wallet trx from
        String trxId;
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String md5Hex = DigestUtils.md5Hex(username + itemId + timestamp);
            trxId = md5Hex;

            WalletCurrencyExchange walletCurrencyExchange = new WalletCurrencyExchange(true);
            walletCurrencyExchange.setTrxId(md5Hex);
            SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmm");
            Calendar c1 = Calendar.getInstance();
//          reference id - easier for comparison / reading
            walletCurrencyExchange.setRefId(sdf.format(c1.getTime()) + RandomStringUtils.randomAlphanumeric(10));
            walletCurrencyExchange.setTrxDatetime(timestamp);
            walletCurrencyExchange.setOwnerId(username);
            walletCurrencyExchange.setAmount(BigDecimal.ZERO);
            walletCurrencyExchange.setPayTo(Global.Ecommerce.TAOBAO);
            walletCurrencyExchange.setRemark(REMARK_PRODUCT + itemId + REMARK_AMOUNT + amount + REMARK_URL + imageUrl);
            walletCurrencyExchangeDao.save(walletCurrencyExchange);
        } catch (Exception e) {
            throw new ValidatorException(e.getMessage());
        }
        return trxId;
    }

    public void findTopupPackageCountForReport(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo) {
        walletSqlDao.findTopupPackageCountForReport(datagridModel, dateFrom, dateTo);
    }

    private static final Object synchronizedCryptoTopUpObject = new Object();

    @Override
    public void doTopUpWhaleCoinUsingCryptoByWebpage(Locale locale, String paymentOption, String phoneNo, String transactionPassword, BigDecimal whaleCoinAmount) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.class);
        PaymentGatewayService paymentGatewayService = Application.lookupBean(PaymentGatewayService.class);
        OmnicplusClient omnicplus = new OmnicplusClient();
        OmcPaymentDto omcPaymentDto = new OmcPaymentDto();
        boolean isCn = locale.equals(Global.LOCALE_CN);

        Map<Double, Double> goldCoin2Omc = paymentGatewayService.goldCoin2Omc();

        Map<Double, Double> goldCoin2OmcPromo = paymentGatewayService.goldCoin2OmcPromo();

        Map<Double, Double> goldCoin2OmcPromoPercent = paymentGatewayService.goldCoin2OmcPromoPercent();

        switch (paymentOption) {
            case Global.TopUpOptionType.OMC:
                break;
            default:
                throw new ValidatorException(i18n.getText("invalidTopUpPaymentType", locale));
        }

        if (StringUtils.isBlank(phoneNo)) {
            throw new ValidatorException(i18n.getText("invalidPhoneno", locale));
        }

        if (StringUtils.isBlank(transactionPassword)) {
            throw new ValidatorException(i18n.getText("invalidTransactionPassword", locale));
        }

        if (BDUtil.leZero(whaleCoinAmount) || whaleCoinAmount == null) {
            throw new ValidatorException(i18n.getText("invalidWhaleCoinAmount_topUp", locale));
        }

        boolean isValid = false;
        BigDecimal cryptoAmount = BigDecimal.ZERO;
        BigDecimal promoAmount = BigDecimal.ZERO;
        Double promoPercent = 0D;
        if (Global.WalletType.OMC.equalsIgnoreCase(paymentOption)) {
            for (Map.Entry<Double, Double> entry : goldCoin2Omc.entrySet()) {
                if (BDUtil.e(whaleCoinAmount, new BigDecimal("" + entry.getKey()))) {
                    promoAmount = new BigDecimal(goldCoin2OmcPromo.get(entry.getKey()).toString());
                    promoPercent = goldCoin2OmcPromoPercent.get(entry.getKey());
                    isValid = true;
                    cryptoAmount = new BigDecimal("" + entry.getValue());
                    break;
                }
            }
        }

        if (!isValid) {
            throw new ValidatorException("invalidWhaleCoinAmount_topUp", locale);
        }

        if (BDUtil.leZero(cryptoAmount)) {
            throw new ValidatorException("invalidCryptoCoinTopUpAmount", locale);
        }

        Member member = memberService.findMemberByMemberCode(phoneNo);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        if (StringUtils.isBlank(transactionPassword)) {
            throw new ValidatorException(i18n.getText("invalidTransactionPassword", locale));
        }

        synchronized (synchronizedCryptoTopUpObject) {
            String cryptoType = paymentOption;

            cryptoType = cryptoType.equalsIgnoreCase(Global.WalletType.OMC) ? "OMCP" : cryptoType;

            omcPaymentDto.setPhoneno(member.getMemberCode());
            omcPaymentDto.setAmount(cryptoAmount);
            omcPaymentDto.setCurrencyCode(cryptoType);
            omcPaymentDto.setRefId(docNoService.doGetNextTopUpRefId());
            omcPaymentDto.setTransactionPassword(transactionPassword);

            OmnicplusDto omnicplusDto = omnicplus.payViaOmcWalletForWhaleCoinTopUp(isCn ? "zh" : "en", omcPaymentDto);

            if (omnicplusDto.getCode() == 200) {
                WalletTrx walletTrx = new WalletTrx(true);
                walletTrx.setInAmt(whaleCoinAmount.add(promoAmount));
                walletTrx.setOutAmt(BigDecimal.ZERO);
                walletTrx.setOwnerId(member.getMemberId());
                walletTrx.setOwnerType(Global.UserType.MEMBER);
                walletTrx.setTrxDatetime(new Date());
                walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_TOPUP);
                walletTrx.setWalletRefId(omcPaymentDto.getRefId());
                walletTrx.setWalletRefno(omcPaymentDto.getDocNo());
                walletTrx.setWalletType(Global.WalletType.WALLET_80);
                walletTrx.setRemark("Website-OMC wallet");

                // using systemLocale
                if (promoPercent > 0) {
                    walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXWithPromo", whaleCoinAmount, promoPercent, walletTrx.getWalletRefId()));
                    walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXWithPromo", Global.LOCALE_CN, whaleCoinAmount, promoPercent,
                            walletTrx.getWalletRefId()));
                } else {
                    walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtX", whaleCoinAmount, walletTrx.getWalletRefId()));
                    walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtX", Global.LOCALE_CN, whaleCoinAmount,
                            walletTrx.getWalletRefId()));
                }
                walletTrxDao.save(walletTrx);

                walletBalanceDao.doCreditAvailableBalance(member.getMemberId(), walletTrx.getOwnerType(), Global.WalletType.WALLET_80,
                        whaleCoinAmount.add(promoAmount));
            } else {
                throw new ValidatorException(omnicplusDto.getMessage());
            }

            goldCoin2Omc.clear();

            goldCoin2OmcPromo.clear();

            goldCoin2OmcPromoPercent.clear();
        }
    }

    @Override
    public WalletTrx findWalletTrxByWalletRefId(String walletRefId) {
        return walletTrxDao.getWalletTrx(walletRefId);
    }

    @Override
    public void findTopupPackageDetailForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount) {
        walletSqlDao.findTopupPackageDetailForListing(datagridModel, dateFrom, dateTo, packageAmount);
    }

    @Override
    public void findTopupPackageDetailForListing2(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount, String memberCode, String topupWay) {
        walletSqlDao.findTopupPackageDetailForListing2(datagridModel, dateFrom, dateTo, packageAmount, memberCode, topupWay);
    }

    public void findPrizeForReport(DatagridModel<HashMap<String, String>> datagridModel, String remark) {
        walletSqlDao.findPrizeForReport(datagridModel, remark);
    }

    @Override
    public void findTopupHistoryForListing(DatagridModel<WalletTrx> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark) {
        walletSqlDao.findTopupHistoryForListing(datagridModel, memberCode,orderId,dateFrom, dateTo, remark);

//        List<HashMap<String, String>> OMCWalletRemark  = datagridModel.getRecords();
//        if(remark.equals("OMC Wallet")){  //remark=null
//            System.out.println(remark);
//        }

    }

    @Override
    public void findOMCWalletHistoryForListing(DatagridModel<HashMap<String, String>> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark) {
        walletSqlDao.findOMCWalletHistoryForListing(datagridModel,memberCode,orderId ,dateFrom, dateTo, remark);
    }

    @Override
    public List<WalletTrx> findTopupHistoryByRemark(Date dateFrom, Date dateTo, String remark) {
        return walletSqlDao.findTopupHistoryByRemark(dateFrom, dateTo, remark);
    }

    @Override
    public List<WalletTrx> findTopupPackageDetailByTopupWay(Date dateFrom, Date dateTo, int packageAmount, String memberCode, String topupWay) {
        return walletSqlDao.findTopupPackageDetailByTopupWay(dateFrom, dateTo, packageAmount, memberCode, topupWay);
    }

    public void findActiveUserForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo) {
        walletSqlDao.findActiveUserForListing(datagridModel, dateFrom, dateTo);
    }

    public List<ActiveUserReport> findActiveUser(Date dateFrom, Date dateTo) {
        return walletSqlDao.findActiveUser(dateFrom, dateTo);
    }

    @Override
    public List<Map<String, Object>> findPointWalletTrxForListing(DatagridModel<PointWalletTrx> datagridModel, Locale locale, String ownerId, String ownerType, int walletType) {
        findPointWalletTrxsForCryptoListing(datagridModel, ownerId, ownerType, walletType, null);

        return datagridModel.getRecords().stream().map(trx -> {
            BigDecimal amount = BDUtil.gZero(trx.getInAmt()) ? trx.getInAmt() : trx.getOutAmt().negate();
            String desc = (Global.LOCALE_CN.equals(locale) && StringUtils.isNotBlank(trx.getTrxDescCn())) ? trx.getTrxDescCn() : trx.getTrxDesc();

            Map<String, Object> trxDetail = new HashMap<>();
            trxDetail.put("id", trx.getPointTrxId());
            trxDetail.put("description", desc);
            trxDetail.put("amount", amount);
            trxDetail.put("trxDatetime", trx.getTrxDatetime());

            return trxDetail;
        }).collect(Collectors.toList());
    }

    @Override
    public void saveDailyActiveUserReport() {
        Date dateFrom =DateUtils.addHours(new Date(),-24);
        Date dateTo = new Date();

        List<DailyActiveUserReport> dailyActiveUserReportList = new ArrayList<>();
        List<ActiveUserReport> activeUserReportList = walletSqlDao.findActiveUser(dateFrom, dateTo);

        if(CollectionUtil.isNotEmpty(activeUserReportList)){
            for(ActiveUserReport report: activeUserReportList){
                DailyActiveUserReport dailyReport = new DailyActiveUserReport();

                dailyReport.setAudienceCount(report.getNewUserCount());
                dailyReport.setNewUserCount(report.getAudienceCount());
                dailyReport.setSendGiftCount(report.getSendGiftCount());
                dailyReport.setTotalAmt(report.getTotalAmt());
                dailyActiveUserReportList.add(dailyReport);
            }
            dailyActiveUserReportDao.saveAll(dailyActiveUserReportList);
        }
    }
}
