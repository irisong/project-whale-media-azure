package com.compalsolutions.compal.wallet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.wallet.vo.WalletTransfer;

public interface WalletTransferRepository extends JpaRepository<WalletTransfer, String> {
    public static final String BEAN_NAME = "walletTransferRepository";
}
