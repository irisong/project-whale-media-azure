package com.compalsolutions.compal.wallet.dao;

import java.util.Date;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.DailyBonusLog;

public interface DailyBonusLogDao extends BasicDao<DailyBonusLog, String> {
    public static final String BEAN_NAME = "dailyBonusLogDao";

    Date getLastRecordDate(String bonusTypePairing);
}
