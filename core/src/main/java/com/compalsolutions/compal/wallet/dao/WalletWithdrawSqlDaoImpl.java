package com.compalsolutions.compal.wallet.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.wallet.vo.WalletWithdraw;

@Component(WalletWithdrawSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletWithdrawSqlDaoImpl extends AbstractJdbcDao implements WalletWithdrawSqlDao {
    @Override
    public List<WalletWithdraw> findWalletWithdrawsForBtcWithdraw(String withdrawStatus, String cryptoType, Integer walletType, int numberOfRecords) {

        List<Object> params = new ArrayList<>();

        String sql = " select * from wl_wallet_withdraw as w where 1=1 ";

        if (StringUtils.isNotBlank(withdrawStatus)) {
            sql += " and w.status=? ";
            params.add(withdrawStatus);
        }

        if (StringUtils.isNotBlank(cryptoType)) {
            sql += " and w.crypto_type=? ";
            params.add(cryptoType);
        }

        if (walletType != null) {
            sql += " and w.wallet_type=? ";
            params.add(walletType);
        }

        sql += " and age(?, w.datetime_update) > interval '10 minutes' ";
        params.add(new Date());

        sql += " order by w.trx_datetime asc ";

        sql = convertToPaginationSql(sql, numberOfRecords);

        // System.out.println(sql);

        return query(sql, new BeanPropertyRowMapper<WalletWithdraw>(WalletWithdraw.class), params.toArray());
    }
}
