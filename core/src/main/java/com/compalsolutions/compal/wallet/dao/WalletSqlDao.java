package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.dto.ActiveUserReport;
import com.compalsolutions.compal.wallet.dto.DepositHistory;
import com.compalsolutions.compal.wallet.vo.*;

public interface WalletSqlDao {
    public static final String BEAN_NAME = "walletSqlDao";

    public BigDecimal getDeductBalanceForWalletStatement(String ownerId, String ownerType, int walletType, Date dateFrom, Date dateTo, int pageNo,
            int pageSize);

    public void findWalletWithdrawsForDatagrid(DatagridModel<WalletWithdraw> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
                                               Date processDateFrom, Date processDateTo, String withdrawType, String cryptoType, String status);

    public void findWalletExchanges2CryptoForDatagrid(DatagridModel<WalletExchange> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
            Date processDateFrom, Date processDateTo, String status);

    public void findWalletTopupsForDatagrid(DatagridModel<WalletTopup> datagridModel, String memberCode, String docno, Date dateFrom, Date dateTo,
            Integer walletType, String status);

    public void findDepositHistories(DatagridModel<DepositHistory> datagridModel, String ownerId, String ownerType, String search);

    public BigDecimal findUsedDailyWithdrawLimit(String ownerId, String ownerType);

    public List<String> findActiveAssetCryptoTypes();

    public void findTopupPackageCountForReport(DatagridModel<HashMap<String,String>> datagridModel, Date dateFrom, Date dateTo);

    public void findTopupPackageDetailForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount);

    public void findTopupPackageDetailForListing2(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo, int packageAmount,String memberId,String topupWay);

    public void findPrizeForReport(DatagridModel<HashMap<String,String>> datagridModel,String remark);

    public void findTopupHistoryForListing(DatagridModel<WalletTrx> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark);

    public void findOMCWalletHistoryForListing(DatagridModel<HashMap<String, String>> datagridModel,String memberCode,String orderId ,Date dateFrom, Date dateTo, String remark);

    public List<WalletTrx> findTopupHistoryByRemark (Date dateFrom, Date dateTo, String remark);

    public List<WalletTrx> findTopupPackageDetailByTopupWay (Date dateFrom, Date dateTo, int packageAmount, String memberCode, String topupWay);

    public void findActiveUserForListing(DatagridModel<HashMap<String, String>> datagridModel, Date dateFrom, Date dateTo);

    public List<ActiveUserReport> findActiveUser(Date dateFrom, Date dateTo);

}