package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletExchangeLock;

public interface WalletExchangeLockDao extends BasicDao<WalletExchangeLock, String> {
    public static final String BEAN_NAME = "walletExchangeLockDao";

    public WalletExchangeLock findLatestActiveWalletExchangeLockByMemberId(String memberId);
}
