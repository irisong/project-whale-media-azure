package com.compalsolutions.compal.wallet.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "wl_wallet_config")
@Access(AccessType.FIELD)
public class WalletConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "config_id", unique = true, nullable = false, length = 32)
    private String configId;

    @Column(name = "member_transfer", nullable = false)
    private Boolean walletTransfer;

    @Column(name = "member_withdraw", nullable = false)
    private Boolean walletWithdraw;

    public WalletConfig() {
    }

    public WalletConfig(boolean defaultValue) {
        if (defaultValue) {
            walletTransfer = true;
            walletWithdraw = true;
        }
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public Boolean getWalletTransfer() {
        return walletTransfer;
    }

    public void setWalletTransfer(Boolean walletTransfer) {
        this.walletTransfer = walletTransfer;
    }

    public Boolean getWalletWithdraw() {
        return walletWithdraw;
    }

    public void setWalletWithdraw(Boolean walletWithdraw) {
        this.walletWithdraw = walletWithdraw;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletConfig that = (WalletConfig) o;

        return configId != null ? configId.equals(that.configId) : that.configId == null;
    }

    @Override
    public int hashCode() {
        return configId != null ? configId.hashCode() : 0;
    }
}
