package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_balance")
@Access(AccessType.FIELD)
public class WalletBalance extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "balance_id", unique = true, nullable = false, length = 32)
    private String balanceId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    @Column(name = "total_balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalBalance;

    @Column(name = "available_balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal availableBalance;

    @Column(name = "in_order_balance", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal inOrderBalance;

    public WalletBalance() {
    }

    public WalletBalance(boolean defaultValue) {
        if (defaultValue) {
            totalBalance = BigDecimal.ZERO;
            availableBalance = BigDecimal.ZERO;
            inOrderBalance = BigDecimal.ZERO;
        }
    }

    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getInOrderBalance() {
        return inOrderBalance;
    }

    public void setInOrderBalance(BigDecimal inOrderBalance) {
        this.inOrderBalance = inOrderBalance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletBalance that = (WalletBalance) o;

        return balanceId != null ? balanceId.equals(that.balanceId) : that.balanceId == null;
    }

    @Override
    public int hashCode() {
        return balanceId != null ? balanceId.hashCode() : 0;
    }

    public void updateTotalBalance() {
        totalBalance = availableBalance.add(inOrderBalance);
    }
}
