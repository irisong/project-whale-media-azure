package com.compalsolutions.compal.wallet.vo;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_wallet_withdraw")
@Access(AccessType.FIELD)
public class WalletWithdraw extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "withdraw_id", unique = true, nullable = false, length = 32)
    private String withdrawId;

    @ToUpperCase
    @ToTrim
    @Column(name = "docno", nullable = false, length = 20)
    private String docno;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "wallet_type", nullable = false)
    private Integer walletType;

    @ToTrim
    @Column(name = "coin_address", length = 100)
    private String coinAddress;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amount;

    @Column(name = "admin_fee", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal adminFee;

    @Column(name = "total_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "wallet_trx_id", length = 32)
    private String walletTrxId;

    @Column(name = "reject_wallet_trx_id", length = 32)
    private String rejectWalletTrxId;

    @Column(name = "process_by", length = 32)
    private String processBy;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "process_datetime")
    private Date processDatetime;

    @ToTrim
    @Column(name = "reject_remark", columnDefinition = Global.ColumnDef.TEXT)
    private String rejectRemark;

    /**
     * 'PENDING' - Pending for approval<br/>
     * 'APPROVED'
     */
    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToUpperCase
    @ToTrim
    @Column(name = "crypto_type", length = 20, nullable = false)
    private String cryptoType;

    @ToTrim
    @Column(name = "tx_hash", length = 100)
    private String txHash;

    @ToTrim
    @Column(name = "tx_hash_url", columnDefinition = Global.ColumnDef.TEXT)
    private String txHashUrl;

    @Column(name = "block")
    private Integer block;

    @Column(name = "no_of_confirm", nullable = false)
    private Integer noOfConfirm;

    @Column(name = "crypto_trx_fee", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal cryptoTrxFee;

    @Column(name = "gas_price", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal gasPrice;

    @Column(name = "nonce")
    private Integer nonce;

    @ToTrim
    @Column(name = "coin_address_from", length = 100)
    private String coinAddressFrom;

    @ToUpperCase
    @ToTrim
    @Column(name = "value_crypto_type", length = 20, nullable = false)
    private String valueCryptoType;

    @Column(name = "value_rate", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal valueRate;

    @Column(name = "value_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal valueAmount;

    @Transient
    private String ownerCode;

    @Transient
    private String ownerName;

    @Transient
    private String statusDesc;

    @Transient
    private String withdrawTypeDesc;

    @Transient
    private String transactionPassword;

    public WalletWithdraw() {
    }

    public WalletWithdraw(boolean defaultValue) {
        if (defaultValue) {
            ownerType = Global.UserType.MEMBER;
            status = Global.Status.PENDING;
        }
    }

    public String getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(String withdrawId) {
        this.withdrawId = withdrawId;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getAdminFee() {
        return adminFee;
    }

    public void setAdminFee(BigDecimal adminFee) {
        this.adminFee = adminFee;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getWalletTrxId() {
        return walletTrxId;
    }

    public void setWalletTrxId(String walletTrxId) {
        this.walletTrxId = walletTrxId;
    }

    public String getProcessBy() {
        return processBy;
    }

    public void setProcessBy(String processBy) {
        this.processBy = processBy;
    }

    public Date getProcessDatetime() {
        return processDatetime;
    }

    public void setProcessDatetime(Date processDatetime) {
        this.processDatetime = processDatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRejectWalletTrxId() {
        return rejectWalletTrxId;
    }

    public void setRejectWalletTrxId(String rejectWalletTrxId) {
        this.rejectWalletTrxId = rejectWalletTrxId;
    }

    public String getRejectRemark() {
        return rejectRemark;
    }

    public void setRejectRemark(String rejectRemark) {
        this.rejectRemark = rejectRemark;
    }

    public String getOwnerCode() {
        return ownerCode;
    }

    public void setOwnerCode(String ownerCode) {
        this.ownerCode = ownerCode;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getWithdrawTypeDesc() {
        return withdrawTypeDesc;
    }

    public void setWithdrawTypeDesc(String withdrawTypeDesc) {
        this.withdrawTypeDesc = withdrawTypeDesc;
    }

    public String getCoinAddress() {
        return coinAddress;
    }

    public void setCoinAddress(String coinAddress) {
        this.coinAddress = coinAddress;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public String getTxHash() {
        return txHash;
    }

    public void setTxHash(String txHash) {
        this.txHash = txHash;
    }

    public String getTxHashUrl() {
        return txHashUrl;
    }

    public void setTxHashUrl(String txHashUrl) {
        this.txHashUrl = txHashUrl;
    }

    public Integer getBlock() {
        return block;
    }

    public void setBlock(Integer block) {
        this.block = block;
    }

    public Integer getNoOfConfirm() {
        return noOfConfirm;
    }

    public void setNoOfConfirm(Integer noOfConfirm) {
        this.noOfConfirm = noOfConfirm;
    }

    public BigDecimal getCryptoTrxFee() {
        return cryptoTrxFee;
    }

    public void setCryptoTrxFee(BigDecimal cryptoTrxFee) {
        this.cryptoTrxFee = cryptoTrxFee;
    }

    public BigDecimal getGasPrice() {
        return gasPrice;
    }

    public void setGasPrice(BigDecimal gasPrice) {
        this.gasPrice = gasPrice;
    }

    public String getValueCryptoType() {
        return valueCryptoType;
    }

    public void setValueCryptoType(String valueCryptoType) {
        this.valueCryptoType = valueCryptoType;
    }

    public BigDecimal getValueRate() {
        return valueRate;
    }

    public void setValueRate(BigDecimal valueRate) {
        this.valueRate = valueRate;
    }

    public BigDecimal getValueAmount() {
        return valueAmount;
    }

    public void setValueAmount(BigDecimal valueAmount) {
        this.valueAmount = valueAmount;
    }

    public String getCoinAddressFrom() {
        return coinAddressFrom;
    }

    public void setCoinAddressFrom(String coinAddressFrom) {
        this.coinAddressFrom = coinAddressFrom;
    }

    public Integer getNonce() {
        return nonce;
    }

    public void setNonce(Integer nonce) {
        this.nonce = nonce;
    }

    public String getTransactionPassword() {
        return transactionPassword;
    }

    public void setTransactionPassword(String transactionPassword) {
        this.transactionPassword = transactionPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WalletWithdraw that = (WalletWithdraw) o;

        return withdrawId != null ? withdrawId.equals(that.withdrawId) : that.withdrawId == null;
    }

    @Override
    public int hashCode() {
        return withdrawId != null ? withdrawId.hashCode() : 0;
    }
}
