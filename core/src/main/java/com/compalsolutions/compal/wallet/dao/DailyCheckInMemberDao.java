package com.compalsolutions.compal.wallet.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.DailyCheckInMember;

public interface DailyCheckInMemberDao extends BasicDao<DailyCheckInMember, String> {
    public static final String BEAN_NAME = "dailyCheckInMemberDao";

    public DailyCheckInMember getTodayCheckIn(String memberId);

    public DailyCheckInMember getDailyCheckInDetailsByMemberId(String memberId);

}
