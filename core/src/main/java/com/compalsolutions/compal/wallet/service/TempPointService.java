package com.compalsolutions.compal.wallet.service;

import com.compalsolutions.compal.wallet.vo.TempPoint;

public interface TempPointService {
    public static final String BEAN_NAME = "tempPointService";

    public void saveTempPoint(TempPoint tempPoint);

    public void doProcessConvertTempPoint();
}
