package com.compalsolutions.compal.wallet.vo;

import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "app_active_user_report")
@Access(AccessType.FIELD)
public class DailyActiveUserReport extends VoBase {

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "date", nullable = false)
    public Date date;

    @Column(name = "send_gift_count", nullable = false)
    public Integer sendGiftCount;

    @Column(name = "audience_count", nullable = false)
    public Integer audienceCount;

    @Column(name = "new_user_count", nullable = false)
    public Integer newUserCount;

    @Column(name = "total_amt", nullable = false)
    public Double totalAmt;

    public DailyActiveUserReport() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getSendGiftCount() {
        return sendGiftCount;
    }

    public void setSendGiftCount(Integer sendGiftCount) {
        this.sendGiftCount = sendGiftCount;
    }

    public Integer getAudienceCount() {
        return audienceCount;
    }

    public void setAudienceCount(Integer audienceCount) {
        this.audienceCount = audienceCount;
    }

    public Integer getNewUserCount() {
        return newUserCount;
    }

    public void setNewUserCount(Integer newUserCount) {
        this.newUserCount = newUserCount;
    }

    public Double getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(Double totalAmt) {
        this.totalAmt = totalAmt;
    }
}
