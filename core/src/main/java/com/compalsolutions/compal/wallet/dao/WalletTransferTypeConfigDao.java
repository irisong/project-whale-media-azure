package com.compalsolutions.compal.wallet.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.wallet.vo.WalletTransferTypeConfig;

public interface WalletTransferTypeConfigDao extends BasicDao<WalletTransferTypeConfig, String> {
    public static final String BEAN_NAME = "walletTransferTypeConfigDao";

    public List<WalletTransferTypeConfig> findAll();

    public List<WalletTransferTypeConfig> findActiveMemberWalletTransferTypeConfigs(boolean selfTransfer);

    public WalletTransferTypeConfig findActive(boolean transferSelf, Integer walletTypeFrom, Integer walletTypeTo);

    WalletTransferTypeConfig findWalletTransferTypeConfig(boolean transferSelf, Integer walletTypeFrom, Integer walletTypeTo);
}
