package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.repository.WalletTrxRepository;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Component(WalletTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WalletTrxDaoImpl extends Jpa2Dao<WalletTrx, String> implements WalletTrxDao {
    @SuppressWarnings("unused")
    @Autowired
    private WalletTrxRepository walletTrxRepository;

    public WalletTrxDaoImpl() {
        super(new WalletTrx(false));
    }

    @Override
    public void findWalletsForDatagrid(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, Integer walletType, Date dateFrom,
                                       Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "FROM w IN " + WalletTrx.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(ownerId)) {
            hql += " and w.ownerId = ? ";
            params.add(ownerId);
        }

        if (StringUtils.isNotBlank(ownerType)) {
            hql += " and w.ownerType = ? ";
            params.add(ownerType);
        }

        if (walletType != null) {
            hql += " and w.walletType = ? ";
            params.add(walletType);
        }

        if (dateFrom != null) {
            hql += " and w.trxDatetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and w.trxDatetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "w", hql, params.toArray());
    }

    @Override
    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndWalleType(String ownerId, String ownerType, int walletType, int recordSize) {
        Validate.notBlank(ownerId);
        Validate.notBlank(ownerType);

        WalletTrx example = new WalletTrx(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setWalletType(walletType);

        return findBlock(example, 0, recordSize, "trxDatetime desc");
    }

    @Override
    public List<WalletTrx> findWalletTrxsByOwnerIdAndOwnerTypeAndWalleTypeAndTrxType(String ownerId, String ownerType, int walletType,
                                                                                     String trxType) {
        Validate.notBlank(ownerId);
        Validate.notBlank(ownerType);
        WalletTrx example = new WalletTrx(false);
        example.setOwnerId(ownerId);
        example.setOwnerType(ownerType);
        example.setWalletType(walletType);
        example.setTrxType(trxType);

        return findByExample(example, "trxDatetime desc");
    }

    @Override
    public List<WalletTrx> findWalletTrxsByTrxTypeAndWalletType(int walletType, String trxType) {
        WalletTrx example = new WalletTrx(false);
        example.setWalletType(walletType);
        example.setTrxType(trxType);

        return findByExample(example, "trxDatetime desc");
    }

    @Override
    public void findWalletsForDatatables(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "FROM w IN " + WalletTrx.class + " WHERE w.ownerId = ? and w.ownerType = ? and w.walletType = ?";
        params.add(ownerId);
        params.add(ownerType);
        params.add(walletType);

        if (dateFrom != null) {
            hql += " and w.trxDatetime >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and w.trxDatetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        hql += " order by w.trxDatetime desc, w.trxId ";

        findForDatagrid(datagridModel, "w", hql, params.toArray());
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findCurrencyWalletTypes(String ownerId, String userType) {
        String hql = "SELECT DISTINCT w.currencyCode, w.walletType FROM w IN " + WalletTrx.class + " WHERE w.ownerId=? and w.ownerType=? "
                + " ORDER BY w.currencyCode, w.walletType ";
        return (List<Object[]>) exFindQueryAsList(hql, ownerId, userType);
    }

    @Override
    public BigDecimal findInOutBalance(String ownerId, String userType, int walletType) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be null for 'findInOutBalance'");
        }

        if (StringUtils.isBlank(userType)) {
            throw new ValidatorException("userType can not be null for 'findInOutBalance'");
        }

        String hql = "SELECT SUM(w.inAmt - w.outAmt) FROM w IN " + WalletTrx.class + " WHERE w.ownerId=? and w.ownerType=? and w.walletType=? ";

        Object result = exFindUnique(hql, ownerId, userType, walletType);
        if (result != null) {
            return (BigDecimal) result;
        }

        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal findInOutBalanceByTrxType(String ownerId, String userType, int walletType, String trxType) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be null for 'findInOutBalance'");
        }

        if (StringUtils.isBlank(userType)) {
            throw new ValidatorException("userType can not be null for 'findInOutBalance'");
        }

        String hql = "SELECT SUM(w.inAmt - w.outAmt) FROM w IN " + WalletTrx.class + " WHERE w.ownerId=? and w.ownerType=? and w.walletType=? " +
                "and w.trxType = ?";

        Object result = exFindUnique(hql, ownerId, userType, walletType, trxType);
        if (result != null) {
            return (BigDecimal) result;
        }

        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getWalletBalanceToDate(String ownerId, String ownerType, int walletType, Date toDate) {
        String hql = "SELECT SUM(w.inAmt - w.outAmt) FROM w IN " + WalletTrx.class
                + " WHERE w.ownerId=? and w.ownerType=? and w.walletType=? and w.trxDatetime <= ? ";

        Object result = exFindUnique(hql, ownerId, ownerType, walletType, DateUtil.formatDateToEndTime(toDate));
        if (result != null) {
            return (BigDecimal) result;
        }

        return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getWalletBalanceFromDate(String ownerId, String ownerType, int walletType, Date fromDate, String trxType) {
        String hql = "SELECT SUM(w.inAmt - w.outAmt) FROM w IN " + WalletTrx.class
                + " WHERE w.ownerId=? and w.ownerType=? and w.walletType=? and w.trxDatetime >= ? and w.trxType= ?";

        Object result = exFindUnique(hql, ownerId, ownerType, walletType, DateUtil.truncateTime(fromDate), trxType);
        if (result != null) {
            return (BigDecimal) result;
        }

        return BigDecimal.ZERO;
    }

    @Override
    public void findWalletTrxsForCryptoListing(DatagridModel<WalletTrx> datagridModel, String ownerId, String ownerType, int walletType, String[] trxTypes) {
        Validate.notBlank(ownerId);
        Validate.notBlank(ownerType);
        // Validate.notEmpty(trxTypes);

        List<Object> params = new ArrayList<>();

        String hql = "FROM w IN " + WalletTrx.class + " WHERE 1=1 " //
                + " and w.ownerId=? " //
                + " and w.ownerType=? " + " and w.walletType=? ";

        params.add(ownerId);
        params.add(ownerType);
        params.add(walletType);

        if (trxTypes != null && trxTypes.length > 0) {
            if (trxTypes.length > 1) {
                hql += " and w.trxType in (";

                for (int i = 0; i < trxTypes.length; i++) {
                    hql += "?";
                    if (i != trxTypes.length - 1) {
                        hql += ",";
                    }

                    params.add(trxTypes[i]);
                }

                hql += ") ";
            } else if (trxTypes.length == 1) {
                hql += " and w.trxType = ? ";
                params.add(trxTypes[0]);
            }
        }

        findForDatagrid(datagridModel, "w", hql, params.toArray());
    }

    @Override
    public WalletTrx getWalletTrx(String orderId) {
        List<Object> params = new ArrayList<Object>();

        String hql = " FROM m IN " + WalletTrx.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(orderId)) {
            hql += " AND m.walletRefId = ? ";
            params.add(orderId);
        }

        return findFirst(hql, params.toArray());
    }

    @Override
    public BigDecimal getTotalIncome(String memberId, Integer walletType, List<String> trxTypeList, Date dateFrom, Date dateTo) {

        List<Object> params = new ArrayList<Object>();
        String hql = " SELECT SUM(inAmt) as _SUM FROM WalletTrx WHERE 1=1 ";

        if (StringUtils.isNotBlank(memberId)) {
            hql += " AND ownerId = ? ";
            params.add(memberId);
        }
        if (walletType != null) {
            hql += " AND walletType = ? ";
            params.add(walletType);
        }

        if (CollectionUtil.isNotEmpty(trxTypeList)) {
            hql += " AND trxType IN (";
            for (String trxType : trxTypeList) {
                hql += "?,";
                params.add(trxType);
            }
            hql = hql.substring(0, hql.length() - 1);
            hql += ")";
        }

        if (dateFrom != null) {
            hql += " AND datetimeAdd >= ? ";
            params.add(dateFrom);
        }

        if (dateTo != null) {
            hql += " AND datetimeAdd <= ? ";
            params.add(dateTo);
        }

        BigDecimal result = (BigDecimal) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return BigDecimal.ZERO;
    }
}
