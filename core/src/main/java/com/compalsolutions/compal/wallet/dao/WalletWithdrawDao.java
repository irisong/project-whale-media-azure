package com.compalsolutions.compal.wallet.dao;

import java.math.BigDecimal;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.wallet.vo.WalletWithdraw;

public interface WalletWithdrawDao extends BasicDao<WalletWithdraw, String> {
    public static final String BEAN_NAME = "walletWithdrawDao";

    public void findWithdrawHistories(DatagridModel<WalletWithdraw> datagridModel, String ownerId, String ownerType, String search);

    public List<WalletWithdraw> findWalletWithdrawForProcessing(String cryptoType, int walletType, BigDecimal maxWithdrawAmount, int numberOfRecords);

    public List<WalletWithdraw> findWalletWithdrawsByCryptoTypesAndStatus(List<String> cryptoTypes, String status);

    public WalletWithdraw findWalletWithdrawByTxHash(String txHash);
}