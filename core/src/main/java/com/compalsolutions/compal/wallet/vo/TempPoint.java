package com.compalsolutions.compal.wallet.vo;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "wl_temp_point")
@Access(AccessType.FIELD)
public class TempPoint {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "point_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long pointId;

    @ToUpperCase
    @ToTrim
    @Column(name = "member_code", length = 50, nullable = false)
    private String memberCode;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_16_2)
    private Double amount;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDate;

    @Column(name = "processed", nullable = false)
    private Boolean processed;

    public TempPoint() {
    }

    public TempPoint(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    public Long getPointId() {
        return pointId;
    }

    public void setPointId(Long pointId) {
        this.pointId = pointId;
    }

    public Boolean getProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        TempPoint tempPoint = (TempPoint) o;
        return Objects.equals(pointId, tempPoint.pointId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pointId);
    }
}
