package com.compalsolutions.compal.identity.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "app_user_identity")
@Access(AccessType.FIELD)
public class Identity extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_NEW = "NEW";
    public static final String STATUS_PENDING_UPLOAD = "PENDING_UPLOAD";
    public static final String STATUS_PENDING_APPROVAL = "PENDING_APPROVAL";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_REJECTED_UPLOAD = "REJECTED_UPLOAD";
    public static final String STATUS_REJECTED_FORM = "REJECTED_FORM";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "identity_id", unique = true, nullable = false, length = 32)
    private String identityId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 200, nullable = false)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_no", length = 100, nullable = false)
    private String identityNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Column(name = "verify_remark")
    private String verifyRemark;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "verify_datetime")
    private Date verifyDatetime;


    @Transient
    private List<String> icImageIds = new ArrayList<>();

    @Transient
    private List<String> passImageIds = new ArrayList<>();

    @Transient
    private List<String> icImageUrls = new ArrayList<>();

    @Transient
    private List<String> passImageUrls = new ArrayList<>();

    public Identity() {
    }

    public Identity(boolean defaultValue){
        if(defaultValue){
            status = STATUS_PENDING_APPROVAL;
        }
    }

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public Date getVerifyDatetime() {
        return verifyDatetime;
    }

    public void setVerifyDatetime(Date verifyDatetime) {
        this.verifyDatetime = verifyDatetime;
    }


    public List<String> getIcImageIds() {
        return icImageIds;
    }

    public void setIcImageIds(List<String> icImageIds) {
        this.icImageIds = icImageIds;
    }

    public List<String> getPassImageIds() {
        return passImageIds;
    }

    public void setPassImageIds(List<String> passImageIds) {
        this.passImageIds = passImageIds;
    }

    public List<String> getIcImageUrls() {
        return icImageUrls;
    }

    public void setIcImageUrls(List<String> icImageUrls) {
        this.icImageUrls = icImageUrls;
    }

    public List<String> getPassImageUrls() {
        return passImageUrls;
    }

    public void setPassImageUrls(List<String> passImageUrls) {
        this.passImageUrls = passImageUrls;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Identity that = (Identity) o;
        return Objects.equals(identityId, that.identityId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identityId);
    }
}
