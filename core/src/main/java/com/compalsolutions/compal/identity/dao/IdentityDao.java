package com.compalsolutions.compal.identity.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.identity.vo.Identity;

import java.util.Date;
import java.util.List;

public interface IdentityDao extends BasicDao<Identity, String> {
    public static final String BEAN_NAME = "identityDao";

    public Identity findByMemberId(String memberId);

    public void findIdentityForListing(DatagridModel<Identity> datagridModel, String memberId, String status, Date dateFrom, Date dateTo);

    public Identity findByIdentityId(String eventId);

    public List<Identity> findAllIdentity();
}
