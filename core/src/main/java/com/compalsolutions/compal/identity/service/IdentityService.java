package com.compalsolutions.compal.identity.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.identity.vo.Identity;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface IdentityService {
    public static final String BEAN_NAME = "identityService";

    public Identity findIdentityByMemberId(String memberId);


    public void doApproveOrRejectIdentity(Locale locale, String updateStatus, String remark, List<String> identityIds);

    public void doApproveOrRejectIdentity(Locale locale, String updateStatus, String identityId, String remark);

    public Identity getIdentity(String identityId);

    public void findIdentityForListing(DatagridModel<Identity> datagridModel, String memberCode, String status, Date dateFrom, Date dateTo);


    public void doUpdateIdentityByAdmin(Locale locale, Identity identity);


    public void createOrUpdateIdentity(Locale locale, Identity identity);

}
