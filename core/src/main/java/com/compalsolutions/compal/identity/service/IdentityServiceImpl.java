package com.compalsolutions.compal.identity.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.identity.IdentityConfiguration;
import com.compalsolutions.compal.identity.dao.IdentityDao;
import com.compalsolutions.compal.identity.vo.Identity;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(IdentityService.BEAN_NAME)
public class IdentityServiceImpl implements IdentityService {
    private static final Log log = LogFactory.getLog(IdentityServiceImpl.class);

    private IdentityDao identityDao;


    @Autowired
    public IdentityServiceImpl(IdentityDao identityDao) {
        this.identityDao = identityDao;
    }

    @Override
    public Identity findIdentityByMemberId(String memberId) {
        return identityDao.findByMemberId(memberId);
    }

    @Override
    public void createOrUpdateIdentity(Locale locale, Identity identity) {
        MemberService memberService = Application.lookupBean(MemberService.class);

        if (StringUtils.isBlank(identity.getMemberId())) {
            throw new ValidatorException("Invalid Member");
        }

        Member member = memberService.getMember(identity.getMemberId());
        if (member == null) {
            throw new ValidatorException("Invalid Member");
        }

        Identity dbIdentity = findIdentityByMemberId(identity.getMemberId());

        if (dbIdentity == null) {
            createIdentityWithChecking(locale, identity, member);
            dbIdentity = identity;
        } else {
            updateIdentityWithChecking(locale, identity, member, dbIdentity);
        }



        if (Identity.STATUS_PENDING_APPROVAL.equalsIgnoreCase(dbIdentity.getStatus())) {
            dbIdentity.setVerifyRemark("");
            identityDao.update(dbIdentity);
        }
    }



    private void updateIdentityWithChecking(Locale locale, Identity identity, Member member, Identity dbIdentity) {
        I18n i18n = Application.lookupBean(I18n.class);

        /**********************
         * VALIDATION - START
         **********************/
        validate(identity, locale, i18n);
        /**********************
         * VALIDATION - END
         **********************/

        dbIdentity.setFullName(identity.getFullName());
        dbIdentity.setIdentityNo(identity.getIdentityNo());
        identityDao.update(dbIdentity);
    }

    private void createIdentityWithChecking(Locale locale, Identity identity, Member member) {
        I18n i18n = Application.lookupBean(I18n.class);

        String memberId = identity.getMemberId();
        String ownerType = Global.UserType.MEMBER;

        /**********************
         * VALIDATION - START
         **********************/

        validate(identity, locale, i18n);

        /**********************
         * VALIDATION - END
         **********************/
        identity.setTrxDatetime(new Date());
        identityDao.save(identity);
    }

    @Override
    public void doApproveOrRejectIdentity(Locale locale, String updateStatus, String remark, List<String> identityIds) {
        for (String identityId : identityIds) {
            doApproveOrRejectIdentity(locale, updateStatus, identityId, remark);
        }
    }

    @Override
    public void doApproveOrRejectIdentity(Locale locale, String updateStatus, String identityId, String remark) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);

        /************************
         * VERIFICATION - START
         ************************/

        Identity identity = identityDao.get(identityId);

        Member member = memberService.getMember(identity.getMemberId());
        String memberCode = member.getMemberCode();

        switch (identity.getStatus()) {
            case Identity.STATUS_PENDING_APPROVAL:
            case Identity.STATUS_APPROVED:
            case Identity.STATUS_REJECTED_FORM:
            case Identity.STATUS_REJECTED_UPLOAD:
                // does nothing
                break;
            default:
                String message = i18n.getText("youCanNotApproveOrRejectThisApplicationFormByMemberCodeXXBecauseTheStatusIsXX", locale, memberCode,
                        identity.getStatus());
                throw new ValidatorException(message);
        }

        if (Identity.STATUS_REJECTED_FORM.equalsIgnoreCase(updateStatus) && StringUtils.isBlank(remark)) {
            throw new ValidatorException(i18n.getText("youNeedToPutRemarkWhenYouRejectThisForm", locale));
        } else if (Identity.STATUS_REJECTED_UPLOAD.equalsIgnoreCase(updateStatus) && StringUtils.isBlank(remark)) {
            throw new ValidatorException(i18n.getText("youNeedToPutRemarkWhenYouRejectThisUpload", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        identity.setStatus(updateStatus);

        // clear previous remark & verify date
        identity.setVerifyRemark("");
        identity.setVerifyDatetime(null);

        if (StringUtils.isNotBlank(remark)) {
            identity.setVerifyRemark(remark);
        }

        if (Identity.STATUS_APPROVED.equalsIgnoreCase(identity.getStatus())) {
            identity.setVerifyDatetime(new Date());
        }

        identityDao.update(identity);
    }

    @Override
    public Identity getIdentity(String identityId) {
        return identityDao.get(identityId);
    }

    @Override
    public void findIdentityForListing(DatagridModel<Identity> datagridModel, String memberCode, String status, Date dateFrom, Date dateTo) {
        identityDao.findIdentityForListing(datagridModel, memberCode, status, dateFrom, dateTo);

        IdentityConfiguration config = Application.lookupBean(IdentityConfiguration.BEAN_NAME, IdentityConfiguration.class);
        final String fileServerUrl = config.getFullParentFileUrl();

//        for (Identity identity : datagridModel.getRecords()) {
           /* for (IdentityFile identityFile : identity.getDetailFiles()) {
                identityFile.setFileUrlWithParentPath(fileServerUrl);
                getIdentityImage(identityFile, identity);
            }*/
//        }
    }





    @Override
    public void doUpdateIdentityByAdmin(Locale locale, Identity identity) {
        /************************
         * VERIFICATION - START
         ************************/

        Identity identityDb = identityDao.get(identity.getIdentityId());

        if (identityDb == null) {
            throw new ValidatorException("Invalid Identity Form");
        }

        /************************
         * VERIFICATION - END
         ************************/

        identityDb.setFullName(identity.getFullName());
        identityDb.setIdentityNo(identity.getIdentityNo());
        identityDao.update(identityDb);
    }



    private void deleteFile(String filePath) {
        // Delete the File
        try {
            log.debug("File Path: " + filePath);

            Files.deleteIfExists(Paths.get(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void validate(Identity identity, Locale locale, I18n i18n) {
        /**********************
         * VALIDATION - START
         **********************/

        if (StringUtils.isBlank(identity.getFullName())) {
            throw new ValidatorException(i18n.getText("invalidName", locale));
        }

        if (StringUtils.isBlank(identity.getIdentityNo())) {
            throw new ValidatorException(i18n.getText("invalidIcOrPassportNumber", locale));
        }

        /**********************
         * VALIDATION - END
         **********************/
    }
}
