package com.compalsolutions.compal.identity;

public class IdentityConfiguration {
    public static final String BEAN_NAME = "identityConfiguration";

    private String serverUrl;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getFullParentFileUrl(){
        return serverUrl + getParentFileUrl();
    }
    public String getParentFileUrl(){
        return "/file/identity/";
    }
}
