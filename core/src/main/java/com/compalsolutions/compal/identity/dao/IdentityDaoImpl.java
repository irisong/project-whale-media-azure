package com.compalsolutions.compal.identity.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.identity.vo.Identity;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(IdentityDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IdentityDaoImpl extends Jpa2Dao<Identity, String> implements IdentityDao {

    public IdentityDaoImpl() {
        super(new Identity(false));
    }

    @Override
    public Identity findByMemberId(String memberId) {
        Validate.notBlank(memberId);

        Identity example = new Identity(false);
        example.setMemberId(memberId);

        return findUnique(example);
    }

    @Override
    public void findIdentityForListing (DatagridModel<Identity> datagridModel, String memberCode, String status, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();

        String hql = "Select i FROM Identity i join i.member mb WHERE 1=1 ";

        if(StringUtils.isNotBlank(memberCode)){
            hql += " AND mb.memberCode like ? ";
            params.add(memberCode + "%");
        }

        if(StringUtils.isNotBlank(status)){
            hql += " AND i.status = ? ";
            params.add(status);
        }

        if(dateFrom != null){
            hql += " AND i.trxDatetime >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if(dateTo != null){
            hql += " AND i.trxDatetime <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "i", hql, params.toArray());
    }

    @Override
    public Identity findByIdentityId(String identityId) {
        Validate.notBlank(identityId);

        Identity example = new Identity(false);
        example.setIdentityId(identityId);

        return findUnique(example);
    }

    @Override
    public List<Identity> findAllIdentity() {
        return findByExample(new Identity(false));
    }
}
