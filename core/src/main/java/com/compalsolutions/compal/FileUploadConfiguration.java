package com.compalsolutions.compal;

public class FileUploadConfiguration {
    public static final String BEAN_NAME = "fileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;
    private String azureBlobStorageConnection;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getMemberFileUploadPath() {
        return uploadPath + MemberFileUploadConfiguration.FOLDER_NAME;
    }

    public String getFullMemberFileParentFileUrl() {
        return serverUrl + "/file/" + MemberFileUploadConfiguration.FOLDER_NAME;
    }

    public String getAzureBlobStorageConnection() {
        return azureBlobStorageConnection;
    }

    public void setAzureBlobStorageConnection(String azureBlobStorageConnection) {
        this.azureBlobStorageConnection = azureBlobStorageConnection;
    }
}
