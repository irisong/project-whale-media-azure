package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;

import java.util.Date;
import java.util.List;

public interface LiveStreamAudienceSqlDao {
    public static final String BEAN_NAME = "liveStreamAudienceSqlDao";

   // public void findActiveMemberInLiveStreamByChannelId(DatagridModel<LiveStreamAudience> datagridModel, String channelId);

    public List<String> findActiveMemberInLiveStreamByChannelId(String channelId);

    public List<String> findEligibleWinnerForLuckyDraw(String channelId, String packageNo);

    public void updateLiveStreamAudience(Date endDate, long duration, String id);

    public void doProcessUntrackAudience(String channelId, String memberId);
}
