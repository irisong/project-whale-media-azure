package com.compalsolutions.compal.live.chatroom.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "app_live_chat_room")
@Access(AccessType.FIELD)
public class LiveChatRoom extends VoBase {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @ToTrim
    @Column(name = "room_id", nullable = false, length = 32)
    private String roomId;

    @ToTrim
    @Column(name = "channel_id", nullable = false, length = 32)
    private String channelId;

    @ToTrim
    @Column(name = "valid")
    private boolean valid;

    @ToTrim
    @Column(name = "announcement", columnDefinition = Global.ColumnDef.TEXT)
    private String announcement;

    @ToTrim
    @Column(name = "name")
    private String name;

    @ToTrim
    @Column(name = "creator")
    private String creator;

    @ToTrim
    @Column(name = "broadcasturl")
    private String broadcastUrl;

    @ToTrim
    @Column(name = "mute_list")
    private String muteList;

    public LiveChatRoom() {
    }

    public LiveChatRoom(boolean defaultValue) {
        if (defaultValue) {
            this.muteList = "";
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getBroadcastUrl() {
        return broadcastUrl;
    }

    public void setBroadcastUrl(String broadcastUrl) {
        this.broadcastUrl = broadcastUrl;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMuteList() {
        return muteList;
    }

    public void setMuteList(String muteList) {
        this.muteList = muteList;
    }
}
