package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamLog;

public interface LiveStreamLogDao extends BasicDao<LiveStreamLog, String> {
    public static final String BEAN_NAME = "liveStreamLogDao";

}
