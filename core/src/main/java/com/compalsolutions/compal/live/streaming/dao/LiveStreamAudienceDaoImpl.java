package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(LiveStreamAudienceDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveStreamAudienceDaoImpl extends Jpa2Dao<LiveStreamAudience, String> implements LiveStreamAudienceDao {

    public LiveStreamAudienceDaoImpl() {
        super(new LiveStreamAudience(false));
    }

    @Override
    public LiveStreamAudience findChannelAudienceByRefNo(String refNo, String channelId, String memberId) {
        Validate.notBlank(refNo);
        Validate.notBlank(channelId);

        LiveStreamAudience example = new LiveStreamAudience(false);
        example.setRefNo(refNo);
        example.setChannelId(channelId);

        if (StringUtils.isNotBlank(memberId)) {
            example.setMemberId(memberId);
        }

        return findFirst(example, "startDate desc");
    }

    @Override
    public List<LiveStreamAudience> findNewestAudience() {
        String hql = "FROM l IN " + LiveStreamAudience.class + " WHERE l.startDate >= ? AND l.duration = ? ";
        Date dt = new Date();
        Date targetDate = new Date(dt.getTime() - (1000*60*60*48));
        List<Object> params = new ArrayList<Object>();
        params.add(targetDate);
        params.add(new Long(0));

        return findQueryAsList(hql, params.toArray());
    }

}
