package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;

import java.util.List;

public interface LiveStreamAudienceDao extends BasicDao<LiveStreamAudience, String> {
    public static final String BEAN_NAME = "liveStreamAudienceDao";

    public LiveStreamAudience findChannelAudienceByRefNo(String refNo, String channelId, String memberId);

    public List<LiveStreamAudience> findNewestAudience();

}
