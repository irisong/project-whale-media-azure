package com.compalsolutions.compal.live.streaming.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "app_vj_connect_reject")
@Access(AccessType.FIELD)
public class VjConnectReject extends VoBase {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @ToTrim
    @Column(name = "vj_channel_id", nullable = false, length = 32)
    private String vjChannelId;

    @ToTrim
    @Column(name = "host_member_id", nullable = false, length = 32)
    private String hostMemberId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "allow_connect_datetime")
    private Date allowConnectDatetime;

    @ToTrim
    @Column(name = "reject_duration", length = 30)
    private String rejectDuration;

    public VjConnectReject() {
    }

    public VjConnectReject(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVjChannelId() {
        return vjChannelId;
    }

    public void setVjChannelId(String vjChannelId) {
        this.vjChannelId = vjChannelId;
    }

    public String getHostMemberId() {
        return hostMemberId;
    }

    public void setHostMemberId(String hostMemberId) {
        this.hostMemberId = hostMemberId;
    }

    public Date getAllowConnectDatetime() {
        return allowConnectDatetime;
    }

    public void setAllowConnectDatetime(Date allowConnectDatetime) {
        this.allowConnectDatetime = allowConnectDatetime;
    }

    public String getRejectDuration() {
        return rejectDuration;
    }

    public void setRejectDuration(String rejectDuration) {
        this.rejectDuration = rejectDuration;
    }
}
