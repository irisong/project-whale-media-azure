package com.compalsolutions.compal.live.streaming;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.live.streaming.client.dto.Channel;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;

import java.util.List;
import java.util.Locale;

public interface LiveStreamProvider {
    public static final String BEAN_NAME = "liveStreamProvider";

    public Channel doCreateVCloudLiveStreamChannel(String memberId, String category, String title, boolean privateLive, String accessCode);

    public void doCloseLiveStreamChannel(String channelId, int totalView, long duration);

    public void doDeleteVCloudLiveStreamChannel(String channelId, int totalView, long duration);

    public void doDeleteOldLiveStreamChannelIfExists(String memberId);

    public void doUpdateOldPendingChannelIfExists(String memberId);

    public boolean doCheckActiveLiveStream(String memberId);

    public LiveStreamDto getChannelDetail(String channelId);

    public List<Channel> getChannelList(Locale locale, DatagridModel<LiveStreamChannel> datagridModel,
                                            String userMemberId, int pageNo, String category);

    public Channel getChannelInfo(Locale locale, String memberId, String channelId, String memberCode);

    public List<Channel> getFollowingUsersIsOnLiveInList(Locale locale, DatagridModel datagridModel, String memberId);
}
