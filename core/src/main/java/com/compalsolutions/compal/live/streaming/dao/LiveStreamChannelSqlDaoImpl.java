package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

@Component(LiveStreamChannelSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveStreamChannelSqlDaoImpl extends AbstractJdbcDao implements LiveStreamChannelSqlDao {

    @Override
    public void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String memberId, List<String> followingIds) {
        List<Object> params = new ArrayList<Object>();

        if (CollectionUtil.isNotEmpty(followingIds)) {
            String sql = "select channel_id, member_id, channel_name, create_time, "
//                    + " http_pull_url, push_url, hls_pull_url, rtmp_pull_url, "
                    + " category, title, private_live, filename, accumulate_view, has_vj_connect "
                    + " from app_live_stream_channel c "
                    + " where status = ? AND member_id IN ( ";
            params.add(Global.Status.ACTIVE);

            for (String followingId : followingIds) {
                sql += " ?,";
                params.add(followingId);
            }

            sql = sql.substring(0, sql.length() - 1);
            sql += ") "
                +  "AND NOT EXISTS ( "
                +  "    SELECT b.member_id FROM mb_member_block b "
                +  "    WHERE b.member_id = c.member_id "
                +  "    AND b.block_member_id = ? "
                +  ") ";
            params.add(memberId);

            queryForDatagrid(datagridModel, sql, new RowMapper<LiveStreamChannel>() {
                public LiveStreamChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
                    LiveStreamChannel obj = new LiveStreamChannel();
                    obj.setChannelId(rs.getString("channel_id"));
                    obj.setMemberId(rs.getString("member_id"));
                    obj.setChannelName(rs.getString("channel_name"));
                    obj.setCreateTime(rs.getLong("create_time"));
//                    obj.setHttpPullUrl(rs.getString("http_pull_url"));
//                    obj.setPushUrl(rs.getString("push_url"));
//                    obj.setHlsPullUrl(rs.getString("hls_pull_url"));
//                    obj.setRtmpPullUrl(rs.getString("rtmp_pull_url"));
                    obj.setCategory(rs.getString("category"));
                    obj.setTitle(rs.getString("title"));
                    obj.setPrivateLive(rs.getBoolean("private_live"));
                    obj.setFilename(rs.getString("filename"));
                    obj.setAccumulateView(rs.getInt("accumulate_view"));
                    obj.setHasVjConnect(rs.getBoolean("has_vj_connect"));
                    return obj;
                }
            }, params.toArray());
        }
    }

    @Override
    public void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String userType,
                                                 String memberCode, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        boolean isContributor = userType.equalsIgnoreCase(PointTrx.CONTRIBUTOR);

        String sql = "select m.member_code, l.channel_id, l.start_date, l.end_date, l.total_view, l.member_id from app_live_stream_channel l\n" +
                "join mb_member m on m.member_id = l.member_id\n" +
                "where 1=1 And l.status =? ";

        if (isContributor) {
            sql = " select sum(total_amt) as total, l.channel_id, m.member_code, p.owner_id," +
                    "l.start_date, l.end_date, l.total_view from wl_point_history p" +
                    " join mb_member m on m.member_id = p.contributor_id" +
                    " join app_live_stream_channel l on l.channel_id = p.channel_id" +
                    " where 1=1 And l.status =?";
        }

        params.add(Global.Status.INACTIVE);
        if (StringUtils.isNotBlank(memberCode)) {
            sql += " AND m.member_code like ?";
            params.add("%" + memberCode + "%");
        }

        if (dateFrom != null) {
            sql += " AND l.datetime_add >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += "AND l.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }
        sql += isContributor ? "group by channel_id, m.member_code, l.start_date, l.end_date, l.total_view, p.owner_id" : "";


        queryForDatagrid(datagridModel, sql, new RowMapper<LiveStreamChannel>() {
            public LiveStreamChannel mapRow(ResultSet rs, int rowNum) throws SQLException {

                LiveStreamChannel obj = new LiveStreamChannel();
                obj.setMemberCode(rs.getString("member_code"));
                obj.setChannelId(rs.getString("channel_id"));
                obj.setStartDate(rs.getTimestamp("start_date"));
                obj.setEndDate(rs.getTimestamp("end_date"));
                obj.setTotalView(rs.getInt("total_view"));
                if (isContributor) {
                    obj.setTotalPoint(rs.getBigDecimal("total"));
                    obj.setMemberId(rs.getString("owner_id"));
                } else {
                    obj.setMemberId(rs.getString("member_id"));
                }
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public int getNoOfLiveStreamChannel() {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT count(*) as _total FROM app_live_stream_channel where status= ?";
        params.add(Global.Status.ACTIVE);
        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("_total");
            }
        }, params.toArray());
        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }
        return results.get(0);
    }

    @Override
    public int getNoOfDayForLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        String sql = "select count(distinct(DATE_FORMAT(start_date,'%Y/%m%d'))) as totalDay from " +
                "app_live_stream_channel where member_id=? and start_date>=? and start_date <=?";

        params.add(ownerId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        List<Integer> results = query(sql, new RowMapper<Integer>() {
            public Integer mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getInt("totalDay");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }
        return results.get(0);
    }

    @Override
    public LinkedHashMap getDatesOfLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dateFormat2 = new SimpleDateFormat("dd-MM-yyyy 23:59:59");
        String sql = "select * from " +
                "app_live_stream_channel where member_id=? and start_date>=? and start_date <=? and status = ? " +
                "or (member_id=? and DATE_FORMAT(end_date,'%m%Y') =? and status =?)" +
                "order by start_date";

        params.add(ownerId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));
        params.add(Global.Status.INACTIVE);

        params.add(ownerId);
        String dateparam = new DecimalFormat("00").format(dateFrom.getMonth()+1)+dateFrom.getYear();
        params.add(dateparam);
        params.add(Global.Status.INACTIVE);

        LinkedHashMap startDuration = new LinkedHashMap();

        query(sql, new RowMapper<Date>() {
            public Date mapRow(ResultSet rs, int arg1) throws SQLException {
                try {
                        String startDate = dateFormat.format(rs.getDate("start_date"));
                        String endDate   = dateFormat.format(rs.getDate("end_date"));

                        Date start = rs.getTimestamp("start_date");
                        Date end = rs.getTimestamp("end_date");

                        long channelduration = 0l;
                        //get duration
                        if(startDate.equalsIgnoreCase(endDate)) {
                             channelduration = (end.getTime() - start.getTime()) / 1000;
                        }else
                        {
                            Date dayend = DateUtil.formatDateToEndTime(start);
                            channelduration = (dayend.getTime() - start.getTime()) / 1000;
                        }

                        //start date
                        if(start.getMonth()==dateFrom.getMonth()) {
                            if (!startDuration.containsKey(startDate)) {
                                startDuration.put(startDate, channelduration);
                            } else {
                                channelduration = Long.parseLong(startDuration.get(startDate).toString()) + channelduration;
                                startDuration.put(startDate, channelduration);
                            }
                        }

                        //end date
                        if(end.getMonth()==dateFrom.getMonth()) {
                            if (!startDate.equalsIgnoreCase(endDate)) {
                                Date starttime = DateUtil.truncateTime(end);
                                long endduration = (end.getTime() - starttime.getTime()) / 1000;

                                startDuration.put(endDate, endduration);

                            }
                        }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                return rs.getDate("start_date");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(startDuration)) {
            return new LinkedHashMap();
        }
        return startDuration;
    }

    @Override
    public long getTotalStreamDuration(String memberId, Date dateFrom, Date dateTo) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be blank for LiveStreamChannelSqlDao.getTotalStreamDuration");
        }

        List<Object> params = new ArrayList<>();
        String sql;

        if (dateFrom != null && dateTo != null) {
            sql = "SELECT sum(UNIX_TIMESTAMP(a.new_end_date) - UNIX_TIMESTAMP(a.new_start_date)) totalDuration " //in seconds
                + "FROM ("
                + "     SELECT "
                + "     CASE WHEN (start_date < ?) "
                + "         THEN ? "
                + "         ELSE start_date"
                + "         END AS new_start_date,"
                + "     CASE WHEN (end_date > ?) "
                + "         THEN ? "
                + "         ELSE end_date "
                + "         END AS new_end_date "
                + "     FROM app_live_stream_channel "
                + "     WHERE member_id = ? AND status = ? " //include blocked channel
//                + "     WHERE member_id = ? AND status = ? AND blocked = 0 "
                + "     AND ((start_date BETWEEN ? AND ?) OR (end_date BETWEEN ? AND ?)) "
                + ") a ";

            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(memberId);
            params.add(Global.Status.INACTIVE);
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        } else {
            sql = "SELECT sum(UNIX_TIMESTAMP(end_date) - UNIX_TIMESTAMP(start_date)) totalDuration " //in seconds
                + "FROM app_live_stream_channel "
                + "WHERE member_id = ? AND status = ?"; //include blocked channel
//                + "WHERE member_id = ? AND status = ? AND blocked = 0 ";
            params.add(memberId);
            params.add(Global.Status.INACTIVE);
        }

        List<Long> results = query(sql, new RowMapper<Long>() {
            public Long mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getLong("totalDuration");
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return 0;
        }

        return results.get(0);
    }

    @Override
    public void findLiveStreamListing(DatagridModel<LiveStreamChannel> datagridModel, String category) {
        String sql = "SELECT c.* " //, (select sum(total_amt) from wl_point_trx where channel_id = c.channel_id) total_point
                + "FROM app_live_stream_channel c "
                + "WHERE c.status = ? ";

        List<Object> params = new ArrayList<>();
        params.add(Global.Status.ACTIVE);

        if (StringUtils.isNotBlank(category)) {
            if (!category.equalsIgnoreCase(LiveCategory.VIRTUAL_CATEGORY)) { //filter china ppl for TRENDING
//            if (category.equalsIgnoreCase(LiveCategory.VIRTUAL_CATEGORY)) { //filter china ppl for TRENDING
//                sql += "AND c.member_id NOT IN (select member_id from mb_member where member_code like '86%' and c.category = ?) ";
//                params.add(LiveCategory.DEFAULT_CHINA_CATEGORY.toUpperCase());
//            } else {
                sql += "AND c.category = ? ";
                params.add(category);
            }
        }

//        sql += "ORDER BY total_point DESC ";

        queryForDatagrid(datagridModel, sql, new RowMapper<LiveStreamChannel>() {
            public LiveStreamChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LiveStreamChannel obj = new LiveStreamChannel();
                obj.setMemberId(rs.getString("member_id"));
                obj.setChannelId(rs.getString("channel_id"));
                obj.setChannelName(rs.getString("channel_name"));
                obj.setCategory(rs.getString("category"));
                obj.setPrivateLive(rs.getBoolean("private_live"));
                obj.setTitle(rs.getString("title"));
                obj.setAccumulateView(rs.getInt("accumulate_view"));
//                obj.setTotalView(rs.getInt("total_view"));
//                obj.setHlsPullUrl(rs.getString("hls_pull_url"));
//                obj.setHttpPullUrl(rs.getString("http_pull_url"));
//                obj.setRtmpPullUrl(rs.getString("rtmp_pull_url"));
//                obj.setPushUrl(rs.getString("push_url"));
                obj.setFilename(rs.getString("filename"));
                obj.setStartDate(rs.getTimestamp("start_date"));
                obj.setEndDate(rs.getTimestamp("end_date"));
                obj.setHasVjConnect(rs.getBoolean("has_vj_connect"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamChannelByMemberCodeAndUserType(String memberCode, String userType, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        boolean isContributor = userType.equalsIgnoreCase(PointTrx.CONTRIBUTOR);

        String sql = "select m.member_code, l.channel_id, l.start_date, l.end_date, l.total_view, l.member_id from app_live_stream_channel l\n" +
                "join mb_member m on m.member_id = l.member_id\n" +
                "where 1=1 And l.status =? ";

        if (isContributor) {
            sql = " select sum(total_amt) as total, l.channel_id, m.member_code, p.owner_id," +
                    "l.start_date, l.end_date, l.total_view from wl_point_history p" +
                    " join mb_member m on m.member_id = p.contributor_id" +
                    " join app_live_stream_channel l on l.channel_id = p.channel_id" +
                    " where 1=1 And l.status =?";
        }

        params.add(Global.Status.INACTIVE);
        if (StringUtils.isNotBlank(memberCode)) {
            sql += " AND m.member_code like ?";
            params.add("%" + memberCode + "%");
        }

        if (dateFrom != null) {
            sql += " AND l.datetime_add >= ?";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += "AND l.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }
        sql += isContributor ? "group by channel_id, m.member_code, l.start_date, l.end_date, l.total_view, p.owner_id" : "";

        List<LiveStreamChannel> results =query(sql, new RowMapper<LiveStreamChannel>() {
            public LiveStreamChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LiveStreamChannel obj = new LiveStreamChannel();
                obj.setMemberCode(rs.getString("member_code"));
                obj.setChannelId(rs.getString("channel_id"));
                obj.setStartDate(rs.getTimestamp("start_date"));
                obj.setEndDate(rs.getTimestamp("end_date"));
                obj.setTotalView(rs.getInt("total_view"));
                if (isContributor) {
                    obj.setTotalPoint(rs.getBigDecimal("total"));
                    obj.setMemberId(rs.getString("owner_id"));
                } else {
                    obj.setMemberId(rs.getString("member_id"));
                }
                return obj;
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return new ArrayList<>();
        }
        return results;
    }

    @Override
    public void updateChannelActive(String channelName, Date startDate) {
        if (StringUtils.isBlank(channelName)) {
            throw new ValidatorException("channelName can not be blank for LiveStreamChannelDao.updateLiveStreamChannelActive");
        }

        if (startDate == null) {
            startDate = new Date();
        }

        List<Object> params = new ArrayList<Object>();
        String sql = "UPDATE app_live_stream_channel SET status = ?, start_date = ? "
                + "WHERE channel_name = ? AND status = ? "
//                + "AND (select total.active from (select count(1) as active from app_live_stream_channel b where b.channel_name = ? and b.status = ?) as total) = 0 " //avoid 2 msg run at the same time
                + "ORDER BY datetime_add DESC "
                + "LIMIT 1 ";

        params.add(Global.Status.ACTIVE);
        params.add(startDate);
        params.add(channelName);
        params.add(Global.Status.PENDING);
//        params.add(channelName);
//        params.add(Global.Status.ACTIVE);

        update(sql, params.toArray());
    }

    @Override
    public void findLiveStreamConnectListing(DatagridModel<LiveStreamChannel> datagridModel, String memberId, String searchContent) {
        String sql = "SELECT c.channel_id, c.member_id ,md.profile_name FROM app_live_stream_channel c join mb_member m on m.member_id = c.member_id join mb_member_detail md on m.member_det_id = md.member_det_id" +
                " WHERE c.status = ? AND c.member_id != ? AND c.channel_id NOT IN " +
                "(SELECT vj_channel_id FROM app_vj_connect_reject WHERE host_member_id = ? AND allow_connect_datetime > ? )";

        List<Object> params = new ArrayList<>();
        params.add(Global.Status.ACTIVE);
        params.add(memberId);
        params.add(memberId);
        params.add(new Date());
        if (StringUtils.isNotBlank(searchContent)) {
            params.add("%"+searchContent+"%");
            sql += " AND md.profile_name LIKE ? ";
        }

        sql += "ORDER BY md.profile_name ASC ";

        queryForDatagrid(datagridModel, sql, new RowMapper<LiveStreamChannel>() {
            public LiveStreamChannel mapRow(ResultSet rs, int rowNum) throws SQLException {
                LiveStreamChannel obj = new LiveStreamChannel();
                obj.setMemberId(rs.getString("member_id"));
                obj.setWhaleLiveId(rs.getString("profile_name"));
                obj.setChannelId(rs.getString("channel_id"));
                return obj;
            }
        }, params.toArray());
    }
}
