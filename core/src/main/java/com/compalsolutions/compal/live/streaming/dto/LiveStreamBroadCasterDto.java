package com.compalsolutions.compal.live.streaming.dto;

public class LiveStreamBroadCasterDto {

    private int agoraUuid;
    private String memberId;
    private String profileName;
    private String memberProfileUrl;


    public LiveStreamBroadCasterDto() {
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberProfileUrl() {
        return memberProfileUrl;
    }

    public void setMemberProfileUrl(String memberProfileUrl) {
        this.memberProfileUrl = memberProfileUrl;
    }

    public int getAgoraUuid() {
        return agoraUuid;
    }

    public void setAgoraUuid(int agoraUuid) {
        this.agoraUuid = agoraUuid;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }
}
