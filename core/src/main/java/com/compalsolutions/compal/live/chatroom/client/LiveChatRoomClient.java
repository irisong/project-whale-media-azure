package com.compalsolutions.compal.live.chatroom.client;

import com.compalsolutions.compal.CheckSumBuilder;
import com.compalsolutions.compal.GiftFileUploadConfiguration;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.live.chatroom.client.dto.LiveChatResponse;
import com.compalsolutions.compal.live.chatroom.service.LiveChatService;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoomLog;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberFansService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.point.dto.SymbolicItemDto;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.web.BaseRestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.core.env.Environment;

import javax.ws.rs.client.Client;
import java.io.IOException;
import java.util.*;

public class LiveChatRoomClient {
    private static final Log log = LogFactory.getLog(LiveChatRoomClient.class);

    private String serverUrl;
    private String appKey;
    private String appSecret;
    private String nonce;
    private String curTime;
    private String checkSum;

    public LiveChatRoomClient() {
        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("yunxin.url");
        appKey = env.getProperty("yunxin.appKey");
        appSecret = env.getProperty("yunxin.appSecret");
    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient(60000, 60000);
    }

    private void refreshhttpHeader() {
        nonce = String.valueOf((10000000 + (int) (Math.random() * ((99999999 - 10000000) + 1))));
        curTime = String.valueOf((new Date()).getTime() / 1000L);
        checkSum = CheckSumBuilder.getCheckSum(appSecret, nonce, curTime);
    }


    private void closeHttpClientConnection(CloseableHttpClient httpClient) {
        try {
            if (httpClient != null) {
                httpClient.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeHttpResponseConnection(CloseableHttpResponse response) {
        try {
            if (response != null) {
                response.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public LiveChatResponse createChatRoom(String channelId, String name, String announcement, String accId, String accessModule) {
        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
        refreshhttpHeader();
        LiveChatResponse liveChatResponse = new LiveChatResponse();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = generateHeader("/chatroom/create.action");
        LiveChatRoomLog liveChatRoomLog;
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("creator", accId));
        nvps.add(new BasicNameValuePair("name", name));
        nvps.add(new BasicNameValuePair("announcement", announcement));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            liveChatRoomLog = liveChatService.doCreateLiveChatLog(accessModule, LiveChatRoomLog.ACTION_CREATE_CHATROOM,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);

                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                log.debug("---------PUSH Live Chat Room Response--------------");
                log.debug("Response: " + jsonString);
                liveChatRoomLog.setResponseBody(jsonString);
                liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
                ObjectMapper objectMapper = new ObjectMapper();
                liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
            } catch (IOException e) {
                liveChatResponse.setCode(401);
                liveChatResponse.setMsg(e.getMessage());
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
            liveChatResponse.setCode(401);
            liveChatResponse.setMsg(e.getMessage());
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return liveChatResponse;
    }

    public LiveChatResponse getLiveChatDetail(String roomId) {
        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
        refreshhttpHeader();
        LiveChatResponse liveChatResponse = new LiveChatResponse();
        LiveChatRoomLog liveChatRoomLog;
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = generateHeader("/chatroom/get.action");
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("roomid", roomId));
        nvps.add(new BasicNameValuePair("needOnlineUserCount", "true"));
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            liveChatRoomLog = liveChatService.doCreateLiveChatLog(Global.AccessModule.WHALE_MEDIA, LiveChatRoomLog.ACTION_GET_CHATROOM,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);

                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                log.debug("---------GET Live Chat Room Response--------------");
                log.debug("Response data: " + jsonString);
                liveChatRoomLog.setResponseBody(jsonString);
                liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
                ObjectMapper objectMapper = new ObjectMapper();
                liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
            } catch (IOException e) {
                liveChatResponse.setCode(401);
                liveChatResponse.setMsg(e.getMessage());
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
            liveChatResponse.setCode(401);
            liveChatResponse.setMsg(e.getMessage());
        } finally {
            closeHttpClientConnection(httpClient);
        }

        return liveChatResponse;
    }

    public LiveChatResponse sendMsgToChatRoom(String roomId, String accId, String senderName, String senderAvatarUrl, String message, String type, String accessModule) {
        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
        refreshhttpHeader();

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = generateHeader("/chatroom/sendMsg.action");

        Map<String, String> content = new HashMap<>(1);
        content.put("senderID", accId);
        content.put("senderName", senderName);
        content.put("senderAvatarURL", senderAvatarUrl);
        content.put("content", message);
        content.put("type", type);
        Gson gson = new Gson();
        String attach = gson.toJson(content);

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("roomid", roomId));
        nvps.add(new BasicNameValuePair("msgId", RandomStringUtils.randomAlphanumeric(32).toLowerCase()));
        nvps.add(new BasicNameValuePair("fromAccid", accId));
        nvps.add(new BasicNameValuePair("msgType", "100")); //100: custom
        nvps.add(new BasicNameValuePair("attach", attach));
        nvps.add(new BasicNameValuePair("highPriority", "true"));
        nvps.add(new BasicNameValuePair("needHighPriorityMsgResend", "false"));

        LiveChatResponse liveChatResponse = new LiveChatResponse();
        LiveChatRoomLog liveChatRoomLog;
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            liveChatRoomLog = liveChatService.doCreateLiveChatLog(accessModule, LiveChatRoomLog.ACTION_SEND_MSG_TO_CHATROOM,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);
                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                log.debug("---------PUSH Live Chat Room Response--------------");
                log.debug("Response: " + jsonString);
                liveChatRoomLog.setResponseBody(jsonString);
                liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
                ObjectMapper objectMapper = new ObjectMapper();
                liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
            } catch (IOException e) {
                liveChatResponse.setCode(401);
                liveChatResponse.setMsg(e.getMessage());
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
            liveChatResponse.setCode(401);
            liveChatResponse.setMsg(e.getMessage());
            e.printStackTrace();
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return liveChatResponse;
    }

    public LiveChatResponse sendRedirectMsgToChatRoom(String roomId, String accId, String senderName, String senderAvatarUrl, String message, String type, Member member, String channelMemberId, String accessModule) {
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);
        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.BEAN_NAME, MemberFansService.class);
        RankService rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
        refreshhttpHeader();

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = generateHeader("/chatroom/sendMsg.action");

        String levelUrl = cryptoProvider.getServerUrl() + "/asset/image/rank/" + Member.getRankInString(rankService.getCurrentRank(member.getTotalExp())) + ".png";
        Triple<String, String, String> fansBadgeInfoTriple = memberFansService.getDisplayFansBadgeInfo(member.getMemberId(), channelMemberId);
        String fansBadgeUrl = fansBadgeInfoTriple.getLeft();
        String fansBadgeName = fansBadgeInfoTriple.getMiddle();
        String fansBadgeTextColor = fansBadgeInfoTriple.getRight();

        Map<String, String> content = new HashMap<>(1);
        content.put("senderID", accId);
        content.put("senderName", senderName);
        content.put("senderAvatarURL", senderAvatarUrl);
        content.put("content", message);
        content.put("type", type);
        content.put("levelURL", levelUrl);
        content.put("fansBadgeURL", fansBadgeUrl);
        content.put("fansBadgeName", fansBadgeName);
        content.put("fansBadgeTextColor", fansBadgeTextColor);
        content.put("whaleLiveID", member.getMemberId());

        Gson gson = new Gson();
        String attach = gson.toJson(content);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("roomid", roomId));
        nvps.add(new BasicNameValuePair("msgId", RandomStringUtils.randomAlphanumeric(32).toLowerCase()));
        nvps.add(new BasicNameValuePair("fromAccid", accId));
        nvps.add(new BasicNameValuePair("msgType", "100")); //100: custom
        nvps.add(new BasicNameValuePair("attach", attach));
        nvps.add(new BasicNameValuePair("highPriority", "true"));
        nvps.add(new BasicNameValuePair("needHighPriorityMsgResend", "false"));

        LiveChatResponse liveChatResponse = new LiveChatResponse();
        LiveChatRoomLog liveChatRoomLog;
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            liveChatRoomLog = liveChatService.doCreateLiveChatLog(accessModule, LiveChatRoomLog.ACTION_SEND_MSG_TO_CHATROOM,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);
                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                log.debug("---------PUSH Live Chat Room Response--------------");
                log.debug("Response: " + jsonString);
                liveChatRoomLog.setResponseBody(jsonString);
                liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
                ObjectMapper objectMapper = new ObjectMapper();
                liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
            } catch (IOException e) {
                liveChatResponse.setCode(401);
                liveChatResponse.setMsg(e.getMessage());
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
            liveChatResponse.setCode(401);
            liveChatResponse.setMsg(e.getMessage());
            e.printStackTrace();
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return liveChatResponse;
    }

    public LiveChatResponse sendGiftToChatRoom(String roomId, String accId, String type, String channelId, String memberId, String symbolicItemId, int qty, String accessModule) {
        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
        PointService pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.BEAN_NAME, MemberFansService.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.BEAN_NAME, LiveStreamService.class);
        RankService rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
        refreshhttpHeader();

        Member member = memberService.getMember(memberId);
        MemberDetail memberDetail = member.getMemberDetail();
        LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannelByChannelId(channelId);
        String levelUrl = cryptoProvider.getServerUrl() + "/asset/image/rank/" + Member.getRankInString(rankService.getCurrentRank(member.getTotalExp())) + ".png";

        Triple<String, String, String> fansBadgeInfoTriple = memberFansService.getDisplayFansBadgeInfo(memberId, liveStreamChannel.getMemberId());
        String fansBadgeUrl = fansBadgeInfoTriple.getLeft();
        String fansBadgeName = fansBadgeInfoTriple.getMiddle();
        String fansBadgeTextColor = fansBadgeInfoTriple.getRight();

        SymbolicItemDto symbolicItemDto = new SymbolicItemDto();
        SymbolicItem symbolicItem = pointService.findSymbolicItem(symbolicItemId);
        symbolicItem.setFileUrlWithParentPath(config.getFullGiftParentFileUrl());
        symbolicItem.setGifFileUrlWithParentPath(config.getFullGiftParentFileUrl());
        Map<String, String> itemName = new HashMap<>();
        itemName.put(Global.Language.CHINESE, symbolicItem.getNameCn());
        itemName.put(Global.Language.ENGLISH, symbolicItem.getName());
        symbolicItemDto.setItemId(symbolicItemId);
        symbolicItemDto.setName(symbolicItem.getName());
        symbolicItemDto.setType(symbolicItem.getType());
        symbolicItemDto.setAnimationUrl(symbolicItem.getGifFilename());
        symbolicItemDto.setAnimationDownloadUrl(symbolicItem.getGifFileUrl());
        symbolicItemDto.setImageUrl(symbolicItem.getFileUrl());
        symbolicItemDto.setNameList(itemName);
        symbolicItemDto.setQty(qty);
        symbolicItemDto.setPrice(symbolicItem.getPrice());

        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpPost httpPost = generateHeader("/chatroom/sendMsg.action");
        Gson giftObject = new Gson();
        String giftObjectJsonString = giftObject.toJson(symbolicItemDto);
        Map<String, String> content = new HashMap<>(1);
        content.put("senderID", accId);
        content.put("senderName", memberDetail != null ? memberDetail.getProfileName() : "");
        content.put("senderAvatarURL", null); //no need show profile pic in IM
        content.put("content", giftObjectJsonString);
        content.put("type", type);
        content.put("levelURL", levelUrl);
        content.put("fansBadgeURL", fansBadgeUrl);
        content.put("fansBadgeName", fansBadgeName);
        content.put("fansBadgeTextColor", fansBadgeTextColor);
        content.put("whaleLiveID", memberId);

        Gson gson = new Gson();
        String attach = gson.toJson(content);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("roomid", roomId));
        nvps.add(new BasicNameValuePair("msgId", RandomStringUtils.randomAlphanumeric(32).toLowerCase()));
        nvps.add(new BasicNameValuePair("fromAccid", accId));
        nvps.add(new BasicNameValuePair("msgType", "100")); //100: custom
        nvps.add(new BasicNameValuePair("attach", attach));
        nvps.add(new BasicNameValuePair("highPriority", "true"));
        nvps.add(new BasicNameValuePair("needHighPriorityMsgResend", "false"));

        LiveChatResponse liveChatResponse = new LiveChatResponse();
        LiveChatRoomLog liveChatRoomLog;
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            liveChatRoomLog = liveChatService.doCreateLiveChatLog(accessModule, LiveChatRoomLog.ACTION_SEND_MSG_TO_CHATROOM,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);

                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                log.debug("---------PUSH Live Chat Room Response--------------");
                log.debug("Response: " + jsonString);
                liveChatRoomLog.setResponseBody(jsonString);
                liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
                ObjectMapper objectMapper = new ObjectMapper();
                liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
            } catch (IOException e) {
                liveChatResponse.setCode(401);
                liveChatResponse.setMsg(e.getMessage());
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
            liveChatResponse.setCode(401);
            liveChatResponse.setMsg(e.getMessage());
            e.printStackTrace();
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return liveChatResponse;
    }

    private HttpPost generateHttpHeader(HttpPost httpPost) {
        httpPost.addHeader("AppKey", appKey);
        httpPost.addHeader("Nonce", nonce);
        httpPost.addHeader("CurTime", curTime);
        httpPost.addHeader("CheckSum", checkSum);
        return httpPost;
    }

    private HttpPost generateHeader(String uri) {
        HttpPost httpPost = new HttpPost(serverUrl + uri);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        httpPost = generateHttpHeader(httpPost);
        return httpPost;
    }

//    public LiveChatResponse setTempMuteMember(String roomId, String accId, String targetAcctId, String duration, String needNotify, String notifyMsg, String accessModule) {
//        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
//
//        refreshhttpHeader();
//        LiveChatResponse liveChatResponse;
//        CloseableHttpClient httpClient = HttpClients.createDefault();
//        HttpPost httpPost = generateHeader("/chatroom/temporaryMute.action");
//        LiveChatRoomLog liveChatRoomLog;
//        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//        nvps.add(new BasicNameValuePair("roomid", roomId));
//        nvps.add(new BasicNameValuePair("operator", accId));
//        nvps.add(new BasicNameValuePair("target", targetAcctId));
//        nvps.add(new BasicNameValuePair("muteDuration", duration));
//        nvps.add(new BasicNameValuePair("needNotify", needNotify));
//        nvps.add(new BasicNameValuePair("notifyExt", notifyMsg));
//
//        try {
//            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));
//
//            liveChatRoomLog = liveChatService.doCreateLiveChatLog(accessModule, LiveChatRoomLog.ACTION_MUTE_AUDIENCE,
//                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
//            HttpResponse response = null;
//            try {
//                response = httpClient.execute(httpPost);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            String jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
//            log.debug("---------PUSH Live Chat Room Response--------------");
//            log.debug("Response: " + jsonString);
//            liveChatRoomLog.setResponseBody(jsonString);
//            liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
//            ObjectMapper objectMapper = new ObjectMapper();
//            liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
//        } catch (IOException e) {
//            throw new SystemErrorException(e.getMessage(), e);
//        }
//
//        return liveChatResponse;
//    }

//    public LiveChatResponse setMemberFansRole(String roomId, String accId, String targetAcctId, String roleType, String optvalue, String notifyMsg, String accessModule) {
//        LiveChatService liveChatService = Application.lookupBean(LiveChatService.BEAN_NAME, LiveChatService.class);
//
//        refreshhttpHeader();
//        LiveChatResponse liveChatResponse;
//        CloseableHttpClient httpClient = HttpClients.createDefault();
//        HttpPost httpPost = generateHeader("/chatroom/setMemberRole.action");
//        LiveChatRoomLog liveChatRoomLog;
//        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
//        nvps.add(new BasicNameValuePair("roomid", roomId));
//        nvps.add(new BasicNameValuePair("operator", accId));
//        nvps.add(new BasicNameValuePair("target", targetAcctId));
//        nvps.add(new BasicNameValuePair("opt", roleType)); //1: 管理员 2:普通等级用户
//        nvps.add(new BasicNameValuePair("optvalue", optvalue));
//        nvps.add(new BasicNameValuePair("notifyExt", notifyMsg));
//
//        try {
//            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));
//
//            liveChatRoomLog = liveChatService.doCreateLiveChatLog(accessModule, LiveChatRoomLog.ACTION_SET_FANS_ROLE,
//                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));
//            HttpResponse response = null;
//            try {
//                response = httpClient.execute(httpPost);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//
//            String jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
//            log.debug("---------PUSH Live Chat Room Response--------------");
//            log.debug("Response: " + jsonString);
//            liveChatRoomLog.setResponseBody(jsonString);
//            liveChatService.doUpdateLiveChatRoomLog(liveChatRoomLog);
//            ObjectMapper objectMapper = new ObjectMapper();
//            liveChatResponse = objectMapper.readValue(jsonString, LiveChatResponse.class);
//        } catch (IOException e) {
//            throw new SystemErrorException(e.getMessage(), e);
//        }
//
//        return liveChatResponse;
//    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getCurTime() {
        return curTime;
    }

    public void setCurTime(String curTime) {
        this.curTime = curTime;
    }

}
