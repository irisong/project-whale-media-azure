package com.compalsolutions.compal.live.streaming.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_live_stream_log")
@Access(AccessType.FIELD)
public class LiveStreamLog extends VoBase {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_CREATE_CHANNEL = "CREATE_CHANNEL";
    public static final String ACTION_DELETE_CHANNEL = "DELETE_CHANNEL";
    public static final String ACTION_CREATE_VCLOUD_CHANNEL = "CREATE_VCLOUD_CHANNEL";
    public static final String ACTION_GET_CHANNEL_INFO = "CREATE_GET_CHANNEL_INFO";
    public static final String ACTION_GET_CHANNEL_LIST = "CREATE_GET_CHANNEL_LIST";
    public static final String ACTION_GET_ADDRESS = "CREATE_GET_ADDRESS";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @ToUpperCase
    @ToTrim
    @Column(name = "action_type", nullable = false, length = 50)
    private String actionType;

    @ToTrim
    @Column(name = "req_body", columnDefinition = Global.ColumnDef.TEXT)
    private String requestBody;

    @ToTrim
    @Column(name = "resp_body", columnDefinition = Global.ColumnDef.TEXT)
    private String responseBody;

    @ToUpperCase
    @ToTrim
    @Column(name = "access_module", length = 50)
    private String accessModule;

    @ToTrim
    @Column(name = "status", length = 32)
    private String status;


    public LiveStreamLog() {
    }

    public LiveStreamLog(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.COMPLETED;
        }
    }


    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccessModule() {
        return accessModule;
    }

    public void setAccessModule(String accessModule) {
        this.accessModule = accessModule;
    }
}
