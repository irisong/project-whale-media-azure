package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component(LiveStreamAudienceSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveStreamAudienceSqlDaoImpl extends AbstractJdbcDao implements LiveStreamAudienceSqlDao {

//    @Override
//    public void findActiveMemberInLiveStreamByChannelId(DatagridModel<LiveStreamAudience> datagridModel, String channelId) {
//        Validate.notBlank(channelId);
//
//        String sql  = "SELECT m.channel_id, m.member_id, (SELECT sum(p.price) from wl_point_trx p where p.contributor_id = m.member_id AND p.channel_id = m.channel_id ) AS accPrice "
//                + " FROM app_live_stream_audience m INNER JOIN app_live_stream_channel l ON l.channel_id = m.channel_id "
//                + " WHERE m.channel_id = ? "
//                + " AND l.status = ? "
//                + " AND m.duration = 0 "
//                + " GROUP BY m.member_id "
//                + " ORDER BY accPrice desc";
//
//        List<Object> params = new ArrayList<>();
//        params.add(channelId);
//        params.add(Global.Status.ACTIVE);
//
//        queryForDatagrid(datagridModel, sql, new RowMapper<LiveStreamAudience>() {
//            public LiveStreamAudience mapRow(ResultSet rs, int rowNum) throws SQLException {
//                LiveStreamAudience liveStreamAudience = new LiveStreamAudience();
//                liveStreamAudience.setMemberId(rs.getString("channel_id"));
//                liveStreamAudience.setMemberId(rs.getString("member_id"));
//                System.out.println("memberID "+rs.getString("member_id")+"  :"+rs.getString("accPrice") +rowNum);
//                return liveStreamAudience;
//            }
//        }, params.toArray());
//    }

    @Override
    public List<String> findActiveMemberInLiveStreamByChannelId(String channelId) {
        Validate.notBlank(channelId);
        String sql  = "SELECT distinct m.member_id, m.start_date , (SELECT sum(p.price) from wl_point_trx p where p.contributor_id = m.member_id AND p.channel_id = m.channel_id ) AS accPrice , (SELECT z.intimacy_value from mb_member_fans z where z.member_id = l.member_id AND z.fans_member_id = m.member_id ) AS fans "
                + " FROM app_live_stream_audience m INNER JOIN app_live_stream_channel l ON l.channel_id = m.channel_id "
                + " WHERE m.channel_id = ? "
                + " AND l.status = ? "
                + " AND m.duration = 0 "
                + " AND m.end_date IS NULL "
                + " ORDER BY fans desc ,accPrice desc , m.start_date";

        List<Object> params = new ArrayList<>();
        params.add(channelId);
        params.add(Global.Status.ACTIVE);
        return query(sql, (rs, rowNum) ->
                        rs.getString("member_id")
                , params.toArray());
    }

    @Override
    public List<String> findEligibleWinnerForLuckyDraw(String channelId, String packageNo) {
        Validate.notBlank(channelId);

        String sql  = "SELECT distinct m.member_id "
                + "FROM app_live_stream_audience m "
                + "INNER JOIN app_live_stream_channel c ON c.channel_id = m.channel_id "
                + "WHERE m.channel_id = ? "
                + "AND c.status = ? "
                + "AND m.end_date IS NULL "
                + "AND m.duration = 0 "
                + "AND m.member_id NOT IN (select winner_member_id from ev_lucky_draw_winner where package_no = ?) "
                + "AND m.member_id NOT IN (select winner_member_id from ev_lucky_draw_default_winner where package_no = ?) ";

        List<Object> params = new ArrayList<>();
        params.add(channelId);
        params.add(Global.Status.ACTIVE);
        params.add(packageNo);
        params.add(packageNo);

        return query(sql, (rs, rowNum) ->
                        rs.getString("member_id")
                , params.toArray());
    }

    @Override
    public void updateLiveStreamAudience(Date endDate, long duration, String id){

        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dt.format(endDate);
        String sql ="UPDATE app_live_stream_audience SET duration = '"+duration+"' , end_date = '"+date+"' "+
                    " WHERE id ='"+id+"' ";
        update(sql);
    }

    @Override
    public void doProcessUntrackAudience(String channelId, String memberId) {
        SimpleDateFormat dt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date = dt.format(new Date());
        String sql ="UPDATE app_live_stream_audience SET duration = '0', end_date = '"+date+"' "+
                " WHERE member_id ='"+memberId+"' AND channel_id ='"+channelId+"' AND end_date IS NULL";
        update(sql);
    }
}
