package com.compalsolutions.compal.live.streaming;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.LiveCategoryService;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.chatroom.service.LiveChatService;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.live.streaming.client.dto.Channel;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamActiveUserDto;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.dto.VjConnectDto;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.live.streaming.vo.VjConnect;
import com.compalsolutions.compal.member.service.FollowerService;
import com.compalsolutions.compal.member.service.MemberBlockService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service(LiveStreamProvider.BEAN_NAME)
public class LiveStreamProviderImpl implements LiveStreamProvider {
    private Log log = LogFactory.getLog(LiveStreamProviderImpl.class);

    private LiveStreamService liveStreamService;

    private LiveCategoryService liveCategoryService;

    private LiveChatService liveChatService;

    private MemberService memberService;

    private MemberBlockService memberBlockService;

    private FollowerService followerService;

    private PointService pointService;

    private LuckyDrawService luckyDrawService;

    private NotificationService notificationService;

    private LiveChatRoomProvider liveChatRoomProvider;

    private Environment env;

    private boolean isProdServer;

    @Autowired
    public LiveStreamProviderImpl(Environment env, LiveStreamService liveStreamService, LiveCategoryService liveCategoryService, LiveChatService liveChatService,
                                  MemberService memberService, MemberBlockService memberBlockService, FollowerService followerService,
                                  PointService pointService, LuckyDrawService luckyDrawService, NotificationService notificationService,
                                  LiveChatRoomProvider liveChatRoomProvider) {
        this.liveStreamService = liveStreamService;
        this.liveCategoryService = liveCategoryService;
        this.liveChatService = liveChatService;
        this.memberService = memberService;
        this.memberBlockService = memberBlockService;
        this.followerService = followerService;
        this.pointService = pointService;
        this.luckyDrawService = luckyDrawService;
        this.notificationService = notificationService;
        this.liveChatRoomProvider = liveChatRoomProvider;
        this.env = env;
        isProdServer = env.getProperty("server.production", Boolean.class, true);
    }

    @Override
    public Channel doCreateVCloudLiveStreamChannel(String memberId, String category, String title, boolean privateLive, String accessCode) {
        Environment env = Application.lookupBean(Environment.class);
        Channel channel = new Channel();

        Member member = memberService.getMember(memberId);
        if (member != null) {
            doDeleteOldLiveStreamChannelIfExists(memberId);
            doUpdateOldPendingChannelIfExists(memberId);

            LiveStreamChannel liveStreamChannel = liveStreamService.doCreateLiveStreamChannel(member, category, title, privateLive, accessCode);
            if (liveStreamChannel != null) {
                String parentUrl = env.getProperty("fileServerUrl");

                // todo : need to take actual path after done profile picture upload
                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(liveStreamChannel.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl + "/file/memberFile");
                    channel.setAvatarUrl(memberFile.getFileUrl());
                }

                channel.setFansBadgeName(member.getFansBadgeName());
                MemberDetail md = member.getMemberDetail();
                if (md != null) {
                    channel.setWhaleLiveId(md.getWhaleliveId());
                    channel.setProfileName(md.getProfileName());
                }
                channel.setTotalPoint(pointService.getTotalPointReceivedByOwnerId(liveStreamChannel.getMemberId(), false));

                Pair<Integer, String> tokenInfo = memberService.doGetChannelTokenInfo(member.getMemberId(), liveStreamChannel.getChannelName());
                channel.setMemberId(liveStreamChannel.getMemberId());
                channel.setChannelId(liveStreamChannel.getChannelId());
                channel.setChannelName(liveStreamChannel.getChannelName());
                channel.setVjAgoraUid(tokenInfo.getLeft());
                channel.setUserAgoraUid(tokenInfo.getLeft());
                channel.setAgoraToken(tokenInfo.getRight());
                channel.setCategory(liveStreamChannel.getCategory());
                channel.setTitle(liveStreamChannel.getTitle());
                channel.setPrivateLive(liveStreamChannel.getPrivateLive());
            }
        }

        return channel;
    }

    @Override
    public void doCloseLiveStreamChannel(String channelId, int totalView, long duration) {
        doDeleteVCloudLiveStreamChannel(channelId, totalView, duration);

        // send message to chat room to inform the audience mobile vj already end live
        NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        if (account != null) {
            liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", channelId, account.getAccountId(), "ADMIN", "", "", "END_LIVE", "", null, "", 0);
        }
    }

    @Override
    public void doDeleteVCloudLiveStreamChannel(String channelId, int totalView, long duration) {
        liveStreamService.doDeleteLiveStreamChannel(channelId, totalView, duration);
    }

    @Override
    public List<Channel> getChannelList(Locale locale, DatagridModel<LiveStreamChannel> datagridModel, String userMemberId, int pageNo, String category) {
        liveStreamService.findLiveStreamListing(datagridModel, category);
        List<Channel> channelList = datagridModel.getRecords().stream()
                .map(c -> {
                    Channel channel = setLiveStreamChannelInfo(locale, c, userMemberId);
                    return channel;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        return channelList;
    }

    @Override
    public Channel getChannelInfo(Locale locale, String memberId, String channelId, String memberCode) {
        return setChannelInfo(locale, null, channelId, "", memberId);
    }

    private Channel setLiveStreamChannelInfo(Locale locale, LiveStreamChannel liveStreamChannel, String userMemberId) {
        return setChannelInfo(locale, liveStreamChannel, liveStreamChannel.getChannelId(), liveStreamChannel.getCategory(), userMemberId);
    }

    private Channel setChannelInfo(Locale locale, LiveStreamChannel liveStreamChannel, String channelId, String channelCategory, String userMemberId) {
        Channel channel = new Channel();

        //shared function, some function already pass in liveStreamChannel
        if (liveStreamChannel == null) {
            liveStreamChannel = liveStreamService.getLiveStreamChannel(channelId, channelCategory, false);
        }

        if (liveStreamChannel == null) {
            return null;
        } else {
            boolean isBlocked = memberBlockService.isMemberBlocked(liveStreamChannel.getMemberId(), userMemberId);
            if (isBlocked) {
                return null;
            }
        }

        channel.setChannelId(liveStreamChannel.getChannelId());

        LiveChatRoom liveChatRoom = liveChatService.findChatRoomByChannelId(channelId);
        if (liveChatRoom != null) {
            channel.setRoomId(liveChatRoom.getRoomId());
        }

        retChannelInfo(locale, userMemberId, liveStreamChannel, channel);

        return channel;
    }

    private Channel retChannelInfo(Locale locale, String userMemberId, LiveStreamChannel liveStreamChannel, Channel channel) {
        Environment env = Application.lookupBean(Environment.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        log.debug("==== retChannelInfo ===");
        String memberId = liveStreamChannel.getMemberId();
        if (!StringUtils.isBlank(memberId)) {
            String serverUrl = env.getProperty("serverUrl");
            String fileServerUrl = env.getProperty("fileServerUrl");
            log.debug("==== defaultViewer ===");
            List<String> defaultViewer = luckyDrawService.getLuckyDrawDefaultWinnerbySenderId(memberId);
            int defaultViewerCount = defaultViewer != null ? defaultViewer.size() : 0;

            channel.setCategory(liveStreamChannel.getCategory());
            log.debug("==== getLocalizeCategoryNameByType ===");
            channel.setLocalizeCategory(liveCategoryService.getLocalizeCategoryNameByType(locale, liveStreamChannel.getCategory()));
            channel.setTitle(liveStreamChannel.getTitle());
            channel.setPrivateLive(liveStreamChannel.getPrivateLive());
            channel.setAccumulateView(liveStreamChannel.getAccumulateView() + defaultViewerCount);
            log.debug("==== getAccumulateView ===");
            // todo : need to take actual path after done live stream cover photo upload
            if (StringUtils.isNotBlank(liveStreamChannel.getFilename())) {
                channel.setCoverUrl(liveStreamChannel.getFileUrlWithParentPath(fileServerUrl + "/file/liveStreamCover"));
            }

            // todo : need to take actual path after done profile picture upload
            MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_PROFILE_PIC);
            if (memberFile != null) {
                memberFile.setFileUrlWithParentPath(fileServerUrl + "/file/memberFile");
                channel.setAvatarUrl(memberFile.getFileUrl());
            }
            log.debug("==== findMemberFileByMemberIdAndType ===");
            Member vjMember = memberService.getMember(memberId);
            if (vjMember != null) {
                log.debug("==== vjMember ===");
                vjMember.setAgoraUid(memberService.doGetMemberAgoraUid(vjMember));
                channel.setVjAgoraUid(vjMember.getAgoraUid());
                channel.setChannelName(liveStreamChannel.getChannelName());

                Member userMember = memberService.getMember(userMemberId);
                if (userMember != null) {
                    log.debug("==== tokenInfo ===");
                    Pair<Integer, String> tokenInfo = memberService.doGetChannelTokenInfo(userMember.getMemberId(), liveStreamChannel.getChannelName());
                    channel.setUserAgoraUid(tokenInfo.getLeft());
                    channel.setAgoraToken(tokenInfo.getRight());
                }

                channel.setMemberId(vjMember.getMemberId());
                channel.setFansBadgeName(vjMember.getFansBadgeName());

                MemberDetail md = vjMember.getMemberDetail();
                if (md != null) {
                    channel.setWhaleLiveId(md.getWhaleliveId());
                    channel.setProfileName(md.getProfileName());
                }
                log.debug("==== setTotalPoint ===");
                channel.setTotalPoint(pointService.getTotalPointReceivedByOwnerId(channel.getMemberId(), false));
            }
            log.debug("==== vjMember done===");

            Follower follower = followerService.getFollower(userMemberId, memberId);
            boolean isFollowing = false;
            if (follower != null) {
                isFollowing = true;
            }
            log.debug("==== follower done===");
            channel.setFollowing(isFollowing);
            channel.setLiveUrl(i18n.getText("streamDefaultMsg", Global.LOCALE_CN, channel.getProfileName())
                    + "  " + serverUrl + "/live?chatRoomID=" + channel.getRoomId() +
                    "&channelID=" + channel.getChannelId() + "&whaleLiveID=" + channel.getWhaleLiveId() +
                    "&profileName=" + channel.getProfileName() + "&isPrivate=" + channel.isPrivateLive() +
                    "&avatarURL=" + channel.getAvatarUrl() + "&coverURL=" + channel.getCoverUrl());
            log.debug("==== check getHasVjConnect ===");
            if (liveStreamChannel.getHasVjConnect()) {
                log.debug("==== getHasVjConnect ===");
                VjConnect vjConnect = liveStreamService.findVjConnectByMemberId(memberId);
                if (vjConnect != null) {
                    log.debug("==== vjConnect IS NOT NULL ===");
                    if (memberId.equals(vjConnect.getHostMemberId())) { //is host (display at left)
                        log.debug("==== memberId Is equals getHostMemberId===");
                        // get info of connector
                        LiveStreamChannel connectorChannel = liveStreamService.getLiveStreamChannel(vjConnect.getConnectorChannelId(), null, false);
                        log.debug("==== connectorChannel===");
                        if (connectorChannel != null && connectorChannel.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                            log.debug("==== connectorChannel IN ===");
                            List<VjConnectDto> vjConnectDtoList = liveStreamService.getVjConnectDtoList(liveStreamChannel.getChannelId(), connectorChannel.getChannelId(),
                                    liveStreamChannel.getChannelName(), connectorChannel.getChannelName(), vjMember.getAgoraUid(), vjConnect.getConnectorMember().getAgoraUid());
                            channel.setVjConnectInfo(vjConnectDtoList);
                        }
                    } else if (memberId.equals(vjConnect.getConnectorMemberId())) { //is connector (display at right)
                        log.debug("==== vjConnect IS NULL ===");
                        // get info of host
                        LiveStreamChannel hostChannel = liveStreamService.getLiveStreamChannel(vjConnect.getHostChannelId(), null, false);
                        log.debug("==== hostChannel ===");
                        if (hostChannel != null && hostChannel.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                            log.debug("==== hostChannel IN ===");
                            List<VjConnectDto> vjConnectDtoList = liveStreamService.getVjConnectDtoList(hostChannel.getChannelId(), liveStreamChannel.getChannelId(),
                                    hostChannel.getChannelName(), liveStreamChannel.getChannelName(), vjConnect.getHostMember().getAgoraUid(), vjMember.getAgoraUid());
                            channel.setVjConnectInfo(vjConnectDtoList);
                        }
                    }
                }
            } else {

                log.debug("==== NOT VjConnect!! ===");
            }
            log.debug("==== COMPLETED ===");
        }
        log.debug("==== retChannelInfo END===");
        return channel;
    }

    @Override
    public LiveStreamDto getChannelDetail(String channelId) {
        int topViewer = 20;
        int totalViewerOnline = 0;
        int secondViewer = 20;
        List<String> liveStreamAudiencesList = liveStreamService.findActiveMemberInLiveStreamByChannelId(channelId);
        LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannelByChannelId(channelId);
        List<String> defaultViewer = luckyDrawService.getLuckyDrawDefaultWinnerbySenderId(liveStreamChannel.getMemberId());
        List<LiveStreamActiveUserDto> memberList = new ArrayList<>();

        final String parentUrl = env.getProperty("fileServerUrl") + "/file/memberFile";

        if (liveStreamAudiencesList != null) {
            totalViewerOnline = liveStreamAudiencesList.size();
            topViewer = Math.min(totalViewerOnline, 20);

            for (int i = 0; i < topViewer; i++) {
                LiveStreamActiveUserDto member = new LiveStreamActiveUserDto();
                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(liveStreamAudiencesList.get(i), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                member.setMemberProfileUrl("");
                member.setMemberId(liveStreamAudiencesList.get(i));
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl);
                    member.setMemberProfileUrl(memberFile.getFileUrl());
                }
                memberList.add(member);
            }
        }

        if (defaultViewer != null && defaultViewer.size() > 0 && topViewer < 20) {
            int counter = secondViewer - memberList.size();
            int defaultCounter = Math.min(defaultViewer.size(), counter);
            for (int i = 0; i < defaultCounter; i++) {

                LiveStreamActiveUserDto member = new LiveStreamActiveUserDto();
                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(defaultViewer.get(i), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                member.setMemberProfileUrl("");
                member.setMemberId(defaultViewer.get(i));
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl);
                    member.setMemberProfileUrl(memberFile.getFileUrl());
                }

                if (memberList.stream().noneMatch(o -> o.getMemberId().equals(member.getMemberId()))) {
                    memberList.add(member);
                }
            }
        }

        int defaultViewCount = defaultViewer == null ? 0 : (defaultViewer.size());
        long view = 0L;

        if (liveStreamChannel != null) {
            view = liveStreamChannel.getAccumulateView() + (long) defaultViewCount;
        }

        LiveStreamDto liveStreamDto = new LiveStreamDto();
        liveStreamDto.setChannelId(channelId);
        liveStreamDto.setView(view);
        liveStreamDto.setMemberList(memberList);

        return liveStreamDto;
    }

    @Override
    public List<Channel> getFollowingUsersIsOnLiveInList(Locale locale, DatagridModel datagridModel, String memberId) {
        List<Follower> followerList = followerService.getFollowingByMemberId(memberId);
        List<String> followers = new ArrayList<>();
        for (Follower follower : followerList) {
            followers.add(follower.getMemberId());
        }

        liveStreamService.findLiveStreamChannelForDatagrid(datagridModel, memberId, followers);
        List<LiveStreamChannel> list = datagridModel.getRecords();
        List<Channel> channels = list.stream()
                .map(c -> {
                    Channel channel = new Channel();
                    LiveChatRoom liveChatRoom = liveChatService.findChatRoomByChannelId(c.getChannelId());
                    if (liveChatRoom != null) {
                        channel.setRoomId(liveChatRoom.getRoomId());
                    }
                    channel.setChannelId(c.getChannelId());

                    channel = retChannelInfo(locale, memberId, c, channel);
                    channel.setFollowing(true);
                    return channel;
                })
                .collect(Collectors.toList());

        return channels;
    }

    @Override
    public void doDeleteOldLiveStreamChannelIfExists(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            List<LiveStreamChannel> oldChannels = liveStreamService.findLiveStreamChannelsByMemberId(memberId, Global.Status.ACTIVE);
            for (LiveStreamChannel liveStreamChannel : oldChannels) {
                liveStreamService.doDeleteOldLiveStreamChannel(liveStreamChannel);
            }
            oldChannels.clear();
        }
    }

    @Override
    public void doUpdateOldPendingChannelIfExists(String memberId) {
        if (StringUtils.isNotBlank(memberId)) {
            List<LiveStreamChannel> pendingChannels = liveStreamService.findLiveStreamChannelsByMemberId(memberId, Global.Status.PENDING);
            for (LiveStreamChannel liveStreamChannel : pendingChannels) {
                liveStreamService.doLiveStreamChannelUpdateError(liveStreamChannel);
            }
            pendingChannels.clear();
        }
    }

    @Override
    public boolean doCheckActiveLiveStream(String memberId) {
        List<LiveStreamChannel> oldChannels = liveStreamService.findLiveStreamChannelsByMemberId(memberId, Global.Status.ACTIVE);
        if (oldChannels.size() > 0 && oldChannels != null) {
            oldChannels.clear();
            oldChannels = null;
            return true;
        } else {
            return false;
        }
    }
}
