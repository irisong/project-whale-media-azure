package com.compalsolutions.compal.live.chatroom.service;

import com.compalsolutions.compal.live.chatroom.client.dto.ChatRoomDto;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoomLog;

import java.util.Locale;

public interface LiveChatService {
    public static final String BEAN_NAME = "livechatService";

    public LiveChatRoomLog doCreateLiveChatLog(String accessModule, String actionType, String requestBody);

    public void doUpdateLiveChatRoomLog(LiveChatRoomLog liveChatRoomLog);

    public LiveChatRoom doCreateLiveChatroom(String channelId, ChatRoomDto chatRoomDto);

    public void doUpdateLiveChatRoom(LiveChatRoom liveChatRoom);

    public void doDeleteLiveChat(String channelId);

    public LiveChatRoom findChatRoomByChannelId(String channelId);

    public LiveChatRoom findChatRoomByCreator(String creator);

    public void doProcessMuteUser(Locale locale, String channelId, boolean action, String userMemberId, String targetMemberId);

    public boolean isMemberMuted(Locale locale, String channelId, String memberId);

    public void doRemoveMuteUser(String channelId, String memberId);
}
