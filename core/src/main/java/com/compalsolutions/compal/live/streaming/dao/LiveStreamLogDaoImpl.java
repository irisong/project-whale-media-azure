package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamLog;
import com.compalsolutions.compal.notification.vo.NotificationLog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(LiveStreamLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveStreamLogDaoImpl extends Jpa2Dao<LiveStreamLog, String> implements LiveStreamLogDao {

    public LiveStreamLogDaoImpl() {
        super(new LiveStreamLog(false));
    }
}
