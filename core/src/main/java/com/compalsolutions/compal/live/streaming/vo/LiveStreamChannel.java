package com.compalsolutions.compal.live.streaming.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "app_live_stream_channel")
@Access(AccessType.FIELD)
//@RedisHash("live_stream_channel")
public class LiveStreamChannel extends VoBase {

    private static final long serialVersionUID = 1L;
    public static final int MAX_LIVE_STREAM_STAGING = 8;
    public static final int MAX_IDLE_COUNT = 5;

    public static final String LAST_7_DAYS = "LAST_7_DAYS";
    public static final String LAST_14_DAYS = "LAST_14_DAYS";
    public static final String THIS_MONTH = "THIS_MONTH";
    public static final String LAST_MONTH = "LAST_MONTH";
    public static final String CUSTOM = "CUSTOM";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @ToTrim
    @Column(name = "channel_id", nullable = false, length = 32)
    private String channelId;

    @ToTrim
    @Column(name = "member_id", length = 32)
    private String memberId;

    @ToTrim
    @Column(name = "create_time")
    private Long createTime;

    @ToTrim
    @Column(name = "channel_name", columnDefinition = Global.ColumnDef.TEXT)
    private String channelName;

    @ToTrim
    @Column(name = "push_url", columnDefinition = Global.ColumnDef.TEXT)
    private String pushUrl;

    @ToTrim
    @Column(name = "http_pull_url", columnDefinition = Global.ColumnDef.TEXT)
    private String httpPullUrl;

    @ToTrim
    @Column(name = "hls_pull_url", columnDefinition = Global.ColumnDef.TEXT)
    private String hlsPullUrl;

    @ToTrim
    @Column(name = "rtmp_pull_url", columnDefinition = Global.ColumnDef.TEXT)
    private String rtmpPullUrl;

    @ToTrim
    @Column(name = "category", length = 30)
    private String category;

    @ToTrim
    @Column(name = "title")
    private String title;

    @ToTrim
    @Column(name = "private_live")
    private Boolean privateLive;

    @ToTrim
    @Column(name = "private_access_code", length = 10)
    private String privateAccessCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "blocked")
    private Boolean blocked;

    @ToTrim
    @Column(name = "remark", length = 100)
    private String remark;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "total_view")
    private Integer totalView;

    @Column(name = "accumulate_view")
    private Integer accumulateView;

    @ToTrim
    @Column(name = "filename", length = 100)
    private String filename;

    @ToTrim
    @Column(name = "token")
    private String token;

    @ToTrim
    @Column(name = "content_type", length = 100)
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "idle_count")
    private Integer idleCount;

    @Column(name = "link_user_agora_uid", length = 10)
    private String linkUserAgoraUid;

    @Column(name = "has_vj_connect")
    private Boolean hasVjConnect;

//    /** current channel is the main vj **/
//    @Column(name = "vj_connect_with_uid", length = 10)
//    private String vjConnectWithUid;
//
//    /** current channel is a connector **/
//    @Column(name = "vj_connect_to_uid", length = 10)
//    private String vjConnectToUid;

    @Transient
    private String memberCode;

    @Transient
    private BigDecimal totalPoint;

    @Transient
    private String hostName;

    @Transient
    private String whaleLiveId;

    @Transient
    private String renamedFilename;

    @Transient
    private String coverPhotoUrl;

    public LiveStreamChannel() {
    }

    public LiveStreamChannel(boolean defaultValue) {
        if (defaultValue) {
//            status = Global.Status.ACTIVE;
            status = Global.Status.PENDING; //only change to ACTIVE after feedback from agora listener
            totalView = 0;
            accumulateView = 0;
            blocked = false;
            idleCount = 0;
            hasVjConnect = false;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getPushUrl() {
        return pushUrl;
    }

    public void setPushUrl(String pushUrl) {
        this.pushUrl = pushUrl;
    }

    public String getHttpPullUrl() {
        return httpPullUrl;
    }

    public void setHttpPullUrl(String httpPullUrl) {
        this.httpPullUrl = httpPullUrl;
    }

    public String getHlsPullUrl() {
        return hlsPullUrl;
    }

    public void setHlsPullUrl(String hlsPullUrl) {
        this.hlsPullUrl = hlsPullUrl;
    }

    public String getRtmpPullUrl() {
        return rtmpPullUrl;
    }

    public void setRtmpPullUrl(String rtmpPullUrl) {
        this.rtmpPullUrl = rtmpPullUrl;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getPrivateLive() {
        return privateLive;
    }

    public void setPrivateLive(Boolean privateLive) {
        this.privateLive = privateLive;
    }

    public String getPrivateAccessCode() {
        return privateAccessCode;
    }

    public void setPrivateAccessCode(String privateAccessCode) {
        this.privateAccessCode = privateAccessCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getTotalView() {
        return totalView;
    }

    public void setTotalView(Integer totalView) {
        this.totalView = totalView;
    }

    public Integer getAccumulateView() {
        return accumulateView;
    }

    public void setAccumulateView(Integer accumulateView) {
        this.accumulateView = accumulateView;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public BigDecimal getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(BigDecimal totalPoint) {
        this.totalPoint = totalPoint;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName;
    }

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(channelId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = channelId + "." + fileExtension;
            }
        }
        return renamedFilename;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public String getCoverPhotoUrl() {
        return coverPhotoUrl;
    }

    public void setCoverPhotoUrl(String coverPhotoUrl) {
        this.coverPhotoUrl = coverPhotoUrl;
    }

    public Integer getIdleCount() {
        return idleCount;
    }

    public void setIdleCount(Integer idleCount) {
        this.idleCount = idleCount;
    }

    public String getLinkUserAgoraUid() {
        return linkUserAgoraUid;
    }

    public void setLinkUserAgoraUid(String linkUserAgoraUid) {
        this.linkUserAgoraUid = linkUserAgoraUid;
    }

    public Boolean getHasVjConnect() {
        return hasVjConnect;
    }

    public void setHasVjConnect(Boolean hasVjConnect) {
        this.hasVjConnect = hasVjConnect;
    }

    @Override
    public String toString() {
        return "LiveStreamChannel{" +
                "id='" + id + '\'' +
                ", channelId='" + channelId + '\'' +
                ", memberId='" + memberId + '\'' +
                ", createTime=" + createTime +
                ", channelName='" + channelName + '\'' +
                ", pushUrl='" + pushUrl + '\'' +
                ", httpPullUrl='" + httpPullUrl + '\'' +
                ", hlsPullUrl='" + hlsPullUrl + '\'' +
                ", rtmpPullUrl='" + rtmpPullUrl + '\'' +
                ", category='" + category + '\'' +
                ", title='" + title + '\'' +
                ", privateLive=" + privateLive +
                ", privateAccessCode='" + privateAccessCode + '\'' +
                ", status='" + status + '\'' +
                ", blocked=" + blocked +
                ", remark='" + remark + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", totalView=" + totalView +
                ", accumulateView=" + accumulateView +
                ", filename='" + filename + '\'' +
                ", token='" + token + '\'' +
                ", contentType='" + contentType + '\'' +
                ", fileSize=" + fileSize +
                ", idleCount=" + idleCount +
                ", linkUserAgoraUid=" + linkUserAgoraUid +
                ", hasVjConnect=" + hasVjConnect +
                ", memberCode='" + memberCode + '\'' +
                ", totalPoint=" + totalPoint +
                ", hostName='" + hostName + '\'' +
                ", whaleLiveId='" + whaleLiveId + '\'' +
                ", renamedFilename='" + renamedFilename + '\'' +
                ", coverPhotoUrl='" + coverPhotoUrl + '\'' +
                '}';
    }
}
