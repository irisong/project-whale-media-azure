package com.compalsolutions.compal.live.streaming.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "app_vj_connect")
@Access(AccessType.FIELD)
public class VjConnect extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_ACTIVE = Global.Status.ACTIVE;
    public static final String STATUS_SWITCH = "SWITCH";
    public static final String STATUS_INACTIVE = Global.Status.INACTIVE;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "connect_id", unique = true, nullable = false, length = 32)
    private String connectId;

    @ToTrim
    @Column(name = "host_channel_id", nullable = false, length = 32)
    private String hostChannelId;

    @ToTrim
    @Column(name = "connector_channel_id", nullable = false, length = 32)
    private String connectorChannelId;

    @Column(name = "host_member_id", length = 32)
    private String hostMemberId;

    @ManyToOne
    @JoinColumn(name = "host_member_id", insertable = false, updatable = false, nullable = false)
    private Member hostMember;

    @Column(name = "connector_member_id", length = 32)
    private String connectorMemberId;

    @ManyToOne
    @JoinColumn(name = "connector_member_id", insertable = false, updatable = false, nullable = false)
    private Member connectorMember;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_datetime")
    private Date startDatetime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_datetime")
    private Date endDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Column(name = "host_total_point", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal hostTotalPoint;

    @Column(name = "connector_total_point", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal connectorTotalPoint;

    public VjConnect() {
    }

    public VjConnect(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
            startDatetime = new Date();
            hostTotalPoint = BigDecimal.ZERO;
            connectorTotalPoint = BigDecimal.ZERO;
        }
    }

    public String getConnectId() {
        return connectId;
    }

    public void setConnectId(String connectId) {
        this.connectId = connectId;
    }

    public String getHostChannelId() {
        return hostChannelId;
    }

    public void setHostChannelId(String hostChannelId) {
        this.hostChannelId = hostChannelId;
    }

    public String getConnectorChannelId() {
        return connectorChannelId;
    }

    public void setConnectorChannelId(String connectorChannelId) {
        this.connectorChannelId = connectorChannelId;
    }

    public String getHostMemberId() {
        return hostMemberId;
    }

    public void setHostMemberId(String hostMemberId) {
        this.hostMemberId = hostMemberId;
    }

    public Member getHostMember() {
        return hostMember;
    }

    public void setHostMember(Member hostMember) {
        this.hostMember = hostMember;
    }

    public String getConnectorMemberId() {
        return connectorMemberId;
    }

    public void setConnectorMemberId(String connectorMemberId) {
        this.connectorMemberId = connectorMemberId;
    }

    public Member getConnectorMember() {
        return connectorMember;
    }

    public void setConnectorMember(Member connectorMember) {
        this.connectorMember = connectorMember;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public BigDecimal getHostTotalPoint() {
        return hostTotalPoint;
    }

    public void setHostTotalPoint(BigDecimal hostTotalPoint) {
        this.hostTotalPoint = hostTotalPoint;
    }

    public BigDecimal getConnectorTotalPoint() {
        return connectorTotalPoint;
    }

    public void setConnectorTotalPoint(BigDecimal connectorTotalPoint) {
        this.connectorTotalPoint = connectorTotalPoint;
    }
}
