package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.live.streaming.vo.VjConnectReject;

public interface VjConnectRejectDao extends BasicDao<VjConnectReject, String> {
    public static final String BEAN_NAME = "vjConnectRejectDao";

}
