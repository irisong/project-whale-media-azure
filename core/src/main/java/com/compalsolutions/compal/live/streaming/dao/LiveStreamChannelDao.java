package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;

import java.util.Date;
import java.util.List;

public interface LiveStreamChannelDao extends BasicDao<LiveStreamChannel, String> {
    public static final String BEAN_NAME = "liveStreamChannelDao";

    public LiveStreamChannel findLiveStreamChannel(String channelId, String category);

    public LiveStreamChannel findLiveStreamChannelByChannelId(String channelId);

    public LiveStreamChannel findLiveStreamChannelByStatus(String channelId, String status);

    public List<LiveStreamChannel> findAllLiveStreamChannel(String status);

    public List<LiveStreamChannel> findLiveStreamChannelsByMemberId(String memberId, String status);

    public List<LiveStreamChannel> findLiveStreamChannelsByMemberIdAndDate(String memberId, String status, Date dateFrom, Date dateTo);

    public LiveStreamChannel findLiveStreamChannelByMemberId(String memberId, String status);

    public void findLiveStreamRecordsPointListing(DatagridModel<LiveStreamChannel> datagridModel, String memberId, Date dateFrom, Date dateTo);

    public List<LiveStreamChannel> findLiveStreamRecordsForTotalDuration(String memberId, Date dateFrom, Date dateTo);

    public void doUpdateLatestAccumulateView(String channelId, String status);

    public LiveStreamChannel getLiveStreamChannelByChannelName(String channelName, String status);

    public void updateUserLinkingChannel(String channelId, String uid);

    public void updateVjConnectChannel(String channelId, boolean hasVjConnect);

    public void updateChannelDisconnected(String channelName, int idleCount, Date endDate, String status);

    public void doUpdateChannelClose(String channelId, String status, Date endDate, int totalView);
}
