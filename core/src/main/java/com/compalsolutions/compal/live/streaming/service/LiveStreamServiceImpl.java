package com.compalsolutions.compal.live.streaming.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.GuildFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.client.AgoraClient;
import com.compalsolutions.compal.general.client.dto.AgoraListener;
import com.compalsolutions.compal.general.dao.AgoraMessageLogDao;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.general.vo.AgoraMessageLog;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.streaming.dao.*;
import com.compalsolutions.compal.live.streaming.dto.VjConnectDto;
import com.compalsolutions.compal.live.streaming.vo.*;
import com.compalsolutions.compal.member.service.*;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.point.dao.PointGlobalTrxDao;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointGlobalTrx;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.telegram.BotClient;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.websocket.service.WsMessageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.redisson.client.RedisConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

@Component(LiveStreamService.BEAN_NAME)
public class LiveStreamServiceImpl implements LiveStreamService {

    private static final Log log = LogFactory.getLog(LiveStreamServiceImpl.class);

    private static final Object synchronizedObject = new Object();

    @Autowired
    private LiveStreamLogDao liveStreamLogDao;

    @Autowired
    private LiveStreamChannelDao liveStreamChannelDao;

    @Autowired
    private LiveStreamChannelSqlDao liveStreamChannelSqlDao;

    @Autowired
    private PointGlobalTrxDao pointGlobalTrxDao;

    @Autowired
    private LiveStreamAudienceDao liveStreamAudienceDao;

    @Autowired
    private LiveStreamAudienceSqlDao liveStreamAudienceSqlDao;

    @Autowired
    private AgoraMessageLogDao agoraMessageLogDao;

    @Autowired
    private VjConnectRejectDao vjConnectRejectDao;

    @Autowired
    private VjConnectDao vjConnectDao;

    @Override
    public LiveStreamLog doCreateLiveStreamLog(String accessModule, String actionType, String requestBody) {
        LiveStreamLog liveStreamLog = new LiveStreamLog(true);
        liveStreamLog.setAccessModule(accessModule);
        liveStreamLog.setActionType(actionType);
        liveStreamLog.setRequestBody(requestBody);
        liveStreamLogDao.save(liveStreamLog);
        return liveStreamLog;
    }

    @Override
    public void doUpdateLiveStreamLog(LiveStreamLog liveStreamLog) {
        liveStreamLogDao.update(liveStreamLog);
    }

    @Override
    public void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String userType, String memberCode, Date dateFrom, Date dateTo) {
        liveStreamChannelSqlDao.findLiveStreamChannelForDatagrid(datagridModel, userType, memberCode, dateFrom, dateTo);
    }

    @Override
    public int getNoOfTotalLiveStreamChannel() {
        return liveStreamChannelSqlDao.getNoOfLiveStreamChannel();
    }

    @Override
    public int getNoOfDayForLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo) {
        return liveStreamChannelSqlDao.getNoOfDayForLiveStreamChannel(ownerId, dateFrom, dateTo);
    }

    @Override
    public LiveStreamChannel doCreateLiveStreamChannel(Member member, String category, String title, boolean privateLive, String accessCode) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedObject) {
            String channelName = memberService.doGetMemberChannelName(member);
            String channelId = StringUtils.replace(UUID.randomUUID().toString(), "-", "");

            LiveStreamChannel liveStreamChannel = new LiveStreamChannel(true);
            liveStreamChannel.setChannelId(channelId);
            liveStreamChannel.setChannelName(channelName);
            liveStreamChannel.setCreateTime(new Date().getTime());
            liveStreamChannel.setMemberId(member.getMemberId());
            liveStreamChannel.setStartDate(new Date());
            liveStreamChannel.setCategory(category);
            liveStreamChannel.setTitle(title);
            liveStreamChannel.setPrivateLive(privateLive);

            if (privateLive) {
                liveStreamChannel.setPrivateAccessCode(accessCode);
            }

            liveStreamChannelDao.save(liveStreamChannel);

            return liveStreamChannel;
        }
    }

    @Override
    public void doDeleteOldLiveStreamChannel(LiveStreamChannel liveStreamChannel) {
        liveStreamChannel.setStatus(Global.Status.INACTIVE);
        liveStreamChannel.setEndDate(new Date());
        liveStreamChannel.setTotalView(0);
        liveStreamChannelDao.update(liveStreamChannel);
    }

    @Override
    public void doLiveStreamChannelUpdateError(LiveStreamChannel liveStreamChannel) {
        liveStreamChannel.setStatus(Global.Status.ERROR);
        liveStreamChannelDao.update(liveStreamChannel);
    }

    @Override
    public void doDeleteLiveStreamChannel(String channelId, int totalView, long duration) {
        MemberService memberService = Application.lookupBean(MemberService.class);

        LiveStreamChannel liveStreamChannel = liveStreamChannelDao.findLiveStreamChannelByChannelId(channelId);
        if (liveStreamChannel != null) {
            Date endDate;
            if (duration > 0) {
                long endTime = (duration * 1000) + liveStreamChannel.getStartDate().getTime();
                endDate = new Date(endTime);
            } else {
                endDate = new Date();
            }

            liveStreamChannelDao.doUpdateChannelClose(channelId, Global.Status.INACTIVE, endDate, totalView);

            if (liveStreamChannel.getMemberId().equals("ff8081817714c47b017714dd952a09a2")) { //小琪
                Member member = memberService.getMember(liveStreamChannel.getMemberId());
                String msg = liveStreamChannel.getEndDate() + ": " + member.getMemberCode() + " [" + member.getMemberDetail().getProfileName() + "] is end streaming.";
                BotClient botClient = new BotClient();
                botClient.doSendReportMessage(msg);
            }
        }
    }

    @Override
    public LiveStreamChannel getLiveStreamChannel(String channelId, String category, boolean localDatabase) {
        return liveStreamChannelDao.findLiveStreamChannel(channelId, category);
    }

    @Override
    public void findLiveStreamListing(DatagridModel datagridModel, String category) {
        liveStreamChannelSqlDao.findLiveStreamListing(datagridModel, category);
    }

    @Override
    public void findLiveStreamConnectListing(DatagridModel datagridModel, String memberId, String searchContent) {
        liveStreamChannelSqlDao.findLiveStreamConnectListing(datagridModel, memberId, searchContent);
    }

    @Override
    public LiveStreamChannel getLiveStreamChannelByChannelId(String channelId) {
        return liveStreamChannelDao.findLiveStreamChannelByChannelId(channelId);
    }

    @Override
    public List<LiveStreamChannel> findAllLiveStreamChannel(String status, boolean localDatabase) {
        return liveStreamChannelDao.findAllLiveStreamChannel(status);
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamChannelsByMemberId(String memberId, String status) {
        return liveStreamChannelDao.findLiveStreamChannelsByMemberId(memberId, status);
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamChannelsByMemberIdAndDate(String memberId, String status, Date dateFrom, Date dateTo) {
        return liveStreamChannelDao.findLiveStreamChannelsByMemberIdAndDate(memberId, status, dateFrom, dateTo);
    }

    @Override
    public boolean checkIsBlockedMemberChannel(String memberId, boolean localDatabase) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);

        if (!localDatabase) {
            if (redisCacheProvider.getRedisStatus()) {
                try {
                    return redisCacheProvider.getBlockedChannelMemberRList().stream().anyMatch(mbrId -> mbrId.equals(memberId));
                } catch (RedisConnectionException redisEx) {
                    redisCacheProvider.setRedisStatus(false);
                } catch (Exception e) {
                    log.debug(e.getMessage());
                }
            }
        }

        LiveStreamChannel blockedChannel = liveStreamChannelDao.findLiveStreamChannelByMemberId(memberId, Global.Status.DISABLED);
        return (blockedChannel != null);
    }

    @Override
    public LiveStreamChannel getLiveStreamChannelByMemberId(String memberId, boolean localDatabase) {
        return liveStreamChannelDao.findLiveStreamChannelByMemberId(memberId, Global.Status.ACTIVE);
    }

    @Override
    public LiveStreamChannel getLiveStreamChannelByChannelName(String channelName, String status) {
        return liveStreamChannelDao.getLiveStreamChannelByChannelName(channelName, status);
    }

    @Override
    public void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String memberId, List<String> followingIds) {
        liveStreamChannelSqlDao.findLiveStreamChannelForDatagrid(datagridModel, memberId, followingIds);
    }

    @Override
    public void findLiveStreamRecordsInfoForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String memberId, Date dateFrom, Date dateTo) {
        liveStreamChannelDao.findLiveStreamRecordsPointListing(datagridModel, memberId, dateFrom, dateTo);
    }

    @Override
    public Triple<Long, Integer, BigDecimal> getLiveRecordTotal(String memberId, Date dateFrom, Date dateTo) {
        PointService pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);

        List<LiveStreamChannel> liveStreamRecords = liveStreamChannelDao.findLiveStreamRecordsForTotalDuration(memberId, dateFrom, dateTo);

        Map<String, Long> durationByDate = new HashMap<>();
        for (LiveStreamChannel channel : liveStreamRecords) {
            List<Pair<Date, Date>> recordList = new ArrayList<>();
            if (DateUtil.truncateTime(channel.getEndDate()).compareTo(DateUtil.truncateTime(channel.getStartDate())) != 0) { //streaming start & end date different
                if (dateFrom != null && dateTo != null) {
                    if (channel.getEndDate().compareTo(dateTo) > 0) { //if start date between date range but end date out of date range
                        recordList.add(new MutablePair<>(channel.getStartDate(), DateUtil.formatDateToEndTime(channel.getStartDate())));
                    } else if ((channel.getStartDate().compareTo(dateFrom) < 0) && (channel.getEndDate().compareTo(dateFrom) > 0) && (channel.getEndDate().compareTo(dateTo) < 0)) { //if end date between date range but start date out of date range
                        recordList.add(new MutablePair<>(DateUtil.truncateTime(channel.getEndDate()), channel.getEndDate()));
                    } else { //separate record by date
                        recordList.add(new MutablePair<>(DateUtil.truncateTime(channel.getEndDate()), channel.getEndDate()));
                        recordList.add(new MutablePair<>(channel.getStartDate(), DateUtil.formatDateToEndTime(channel.getStartDate())));
                    }
                } else { //separate record by date
                    recordList.add(new MutablePair<>(DateUtil.truncateTime(channel.getEndDate()), channel.getEndDate()));
                    recordList.add(new MutablePair<>(channel.getStartDate(), DateUtil.formatDateToEndTime(channel.getStartDate())));
                }
            } else {
                recordList.add(new MutablePair<>(channel.getStartDate(), channel.getEndDate()));
            }

            for (Pair<Date, Date> records : recordList) {
                Date startDate = records.getLeft();
                Date endDate = records.getRight();
                long duration = (endDate.getTime() - startDate.getTime()) / 1000;

                String strDate = DateUtil.format(startDate, "yyyy-MM-dd");
                long dailyAccumulatedDuration = durationByDate.get(strDate) != null ? durationByDate.get(strDate) : 0;
                durationByDate.put(strDate, dailyAccumulatedDuration + duration);
            }
        }

        int totalDay = 0;
        long totalDuration = 0;
        for (Map.Entry<String, Long> dailyDuration : durationByDate.entrySet()) {
            long duration = dailyDuration.getValue();

            if (duration < 3600) { //min 1 hours
                duration = 0;
            } else if (duration > 10800) { //max 3 hours
                duration = 10800;
                totalDay += 1;
            } else {
                totalDay += 1;
            }

            totalDuration += duration;
        }

        BigDecimal totalPoint = pointService.getPointByDate(memberId, dateFrom, dateTo);

        return new ImmutableTriple<>(totalDuration, totalDay, totalPoint);
//        return liveStreamChannelSqlDao.getTotalStreamDuration(memberId, dateFrom, dateTo);
    }

    @Override
    public Pair<Date, Date> getFromToDate(String dateRangeType, Date dateFrom, Date dateTo) {
        Calendar cal = Calendar.getInstance();

        if (StringUtils.isNotBlank(dateRangeType)) {
            switch (dateRangeType.toUpperCase()) {
                case LiveStreamChannel.LAST_7_DAYS:
                    dateFrom = DateUtil.truncateTime(DateUtils.addDays(new Date(), -6));
                    dateTo = DateUtil.formatDateToEndTime(new Date());
                    break;
                case LiveStreamChannel.LAST_14_DAYS:
                    dateFrom = DateUtil.truncateTime(DateUtils.addDays(new Date(), -13));
                    dateTo = DateUtil.formatDateToEndTime(new Date());
                    break;
                case LiveStreamChannel.THIS_MONTH:
                    dateFrom = DateUtil.truncateTime(DateUtil.getFirstDayOfMonth());
                    dateTo = DateUtil.formatDateToEndTime(new Date());
                    break;
                case LiveStreamChannel.LAST_MONTH:
                    cal.setTime(new Date());
                    cal.add(Calendar.MONTH, -1); //last month
                    cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH)); //last day of the month
                    dateFrom = DateUtil.truncateTime(DateUtils.addMonths(DateUtil.getFirstDayOfMonth(), -1));
                    dateTo = DateUtil.formatDateToEndTime(cal.getTime());
                    break;
            }
        }

        return new ImmutablePair<>(dateFrom, dateTo);
    }

    @Override
    public List<String> findActiveMemberInLiveStreamByChannelId(String channelId) {
        return liveStreamAudienceSqlDao.findActiveMemberInLiveStreamByChannelId(channelId);
    }

    @Override
    public List<String> findEligibleWinnerForLuckyDraw(String channelId, String packageNo) {
        return liveStreamAudienceSqlDao.findEligibleWinnerForLuckyDraw(channelId, packageNo);
    }

    @Override
    public void doBlockLiveStreamChannel(LiveStreamChannel liveStreamChannel, String status, String reason) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        if (liveStreamChannel != null) {
            liveStreamChannel.setStatus(status);

            if (StringUtils.isNotBlank(reason))
                liveStreamChannel.setRemark(reason);

            if (status.equalsIgnoreCase(Global.Status.DISABLED)) {
                liveStreamChannel.setEndDate(new Date());
                liveStreamChannel.setBlocked(true); // wont chg even unblocked
            }

            liveStreamChannelDao.update(liveStreamChannel);

            if (redisCacheProvider.getRedisStatus()) {
                if (status.equalsIgnoreCase(Global.Status.DISABLED)) {
                    redisCacheProvider.addBlockedChannelMember(liveStreamChannel.getMemberId());
                } else {
                    redisCacheProvider.removeBlockedChannelMember(liveStreamChannel.getMemberId());
                }
            }
        }
    }

    @Override
    public LiveStreamChannel getLatestLiveStreamChannelByMemberId(String memberId, String status) {
        return liveStreamChannelDao.findLiveStreamChannelByMemberId(memberId, status);
    }

    @Override
    public LiveStreamChannel doProcessLiveStreamCoverUploadFile(Locale locale, String memberId, File fileUpload, String contentType, String fileName) {
        I18n i18n = Application.lookupBean(I18n.class);
        Environment env = Application.lookupBean(Environment.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);


        /************************
         * VERIFICATION - START
         ************************/

        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        Member member = memberService.getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        String uploadPath = env.getProperty("file.upload.path") + "/liveStreamCover";
        LiveStreamChannel channel = getLatestLiveStreamChannelByMemberId(memberId, Global.Status.PENDING);

        if (channel == null) {
            throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
        } else {
            String storeFileName = channel.getRenamedFilename();
            if (StringUtils.isNotBlank(storeFileName)) {
                if (!isProdServer) {
                    File file = new File(uploadPath, storeFileName);
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
            channel.setFilename(fileName);
            channel.setContentType(contentType);
            channel.setFileSize(fileUpload.length());
            liveStreamChannelDao.update(channel);
            if (!isProdServer) {
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
            }
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(fileUpload.getAbsolutePath()));

                if (isProdServer) {
                    azureBlobStorageService.uploadFileToAzureBlobStorage(channel.getRenamedFilename(), storeFileName,
                            null, channel.getContentType(), "liveStreamCover",
                            bytes, channel.getFileSize());
                } else {
                    Path path = Paths.get(uploadPath, channel.getRenamedFilename());
                    Files.write(path, bytes);
                }
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        }

        return channel;
    }

    private static final Object synchronizedTrackWatchLiveTimeObject = new Object();

    @Override
    public String doProcessTrackWatchLiveTime(Locale locale, String memberId, String actionType,
                                              String channelId, String refNo, boolean isOffline, long duration) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        RankService rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);
        FansIntimacyService fansIntimacyService = Application.lookupBean(FansIntimacyService.BEAN_NAME, FansIntimacyService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.BEAN_NAME, MemberFansService.class);
        WsMessageService wsMessageService = Application.lookupBean(WsMessageService.BEAN_NAME, WsMessageService.class);
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);

        synchronized (synchronizedTrackWatchLiveTimeObject) {
            log.debug("Begin Audience");
            if (!actionType.equalsIgnoreCase(LiveStreamAudience.ENTER_CHANNEL) && !actionType.equalsIgnoreCase(LiveStreamAudience.EXIT_CHANNEL)) {
                throw new ValidatorException(i18n.getText("invalidAction", locale));
            }

            boolean isFans = false;
            LiveStreamChannel channel = getLiveStreamChannelByChannelId(channelId);
            if (channel == null) {
                if (actionType.equalsIgnoreCase(LiveStreamAudience.ENTER_CHANNEL)) {
                    throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
                }
                return refNo;
            }

            MemberFans memberFans = memberFansService.findMemberFans(memberId, channel.getMemberId(), "");
            if (memberFans != null) {
                isFans = true;
            }

            if (actionType.equalsIgnoreCase(LiveStreamAudience.ENTER_CHANNEL)) {
                Member member = memberService.getMember(memberId);
                if (member == null) {
                    throw new ValidatorException(i18n.getText("invalidMember", locale));
                }

                doProcessUntrackAudience(channelId, memberId);
                refNo = docNoService.doGetNextAudienceRefId();

                LiveStreamAudience liveStreamAudience = new LiveStreamAudience(true);
                liveStreamAudience.setRefNo(refNo);
                liveStreamAudience.setMemberId(memberId);
                liveStreamAudience.setChannelId(channelId);
                liveStreamAudience.setStartDate(new Date());
                liveStreamAudienceDao.save(liveStreamAudience);

              //  redisCacheProvider.addAudience(liveStreamAudience);

                if (isFans) {
                    log.debug("Begin Start Fans");
                    fansIntimacyService.doIncreaseFanIntimacyValue(locale, memberId, channel.getMemberId(), FansIntimacyTrx.TYPE_FIRST_TIME_ENTRY, new BigDecimal(5));
                }
            } else {
                LiveStreamAudience liveStreamAudience = findChannelAudienceByRefNo(refNo, channelId, null);
                Date endDate;
                if (liveStreamAudience != null) {
                    if (isOffline) { //if user disconnected, get duration from frontend
                        long endTime = (duration * 1000) + liveStreamAudience.getStartDate().getTime();
                        endDate = new Date(endTime);
                    } else {
                        endDate = new Date();
                        duration = (endDate.getTime() - liveStreamAudience.getStartDate().getTime()) / 1000;
                    }

                    liveStreamAudienceSqlDao.updateLiveStreamAudience(endDate, duration, liveStreamAudience.getId());

                    //add member experience
                    rankService.doProcessMemberExperience(locale, liveStreamAudience.getMemberId(),
                            "LiveStreamAudience", liveStreamAudience.getRefNo(), liveStreamAudience.getId(),
                            MemberRankTrx.VIEW_STREAM, duration, BigDecimal.ZERO, "");

                    //add fan intimacy value
                    if (isFans) {
                        log.debug("Begin End Fans");
                        // LiveStreamAudience liveStreamAudienceDuration = liveStreamAudienceDao.findChannelAudienceByRefNo(refNo, channelId, null);
                        if (duration >= MemberFans.CONTINUE_WATCHING) {
                            fansIntimacyService.doIncreaseFanIntimacyValue(locale, memberId, channel.getMemberId(), FansIntimacyTrx.TYPE_CONT_FIVE_MIN_VIEW, new BigDecimal(5));
                        }
                    }
                }
            }
        }
        return refNo;
    }

    @Override
    public void doProcessLiveStreamNotification(LiveStreamChannel liveStreamChannel) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        FollowerService followerService = Application.lookupBean(FollowerService.BEAN_NAME, FollowerService.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        MemberBlockService memberBlockService = Application.lookupBean(MemberBlockService.BEAN_NAME, MemberBlockService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
        Environment env = Application.lookupBean(Environment.class);

        String serverUrl = env.getProperty("serverUrl");
        Member liveMember = memberService.getMember(liveStreamChannel.getMemberId());

        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.LIVE_STREAM);
        payload.setRedirectUrl(liveStreamChannel.getChannelId());
        payload.setPrivateLive(liveStreamChannel.getPrivateLive());
        payload.setId(liveMember.getMemberDetail().getWhaleliveId());
        payload.setName(liveMember.getMemberDetail().getProfileName());

        MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(liveStreamChannel.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(serverUrl + "/file/memberFile");
            payload.setAvatarUrl(memberFile.getFileUrl());
        } else {
            payload.setAvatarUrl("");
        }

        if (StringUtils.isNotBlank(liveStreamChannel.getFilename())) {
            payload.setCoverUrl(liveStreamChannel.getFileUrlWithParentPath(serverUrl + "/file/liveStreamCover"));
        } else {
            payload.setCoverUrl("");
        }

        // get followers acct id
        NotificationAccount userAcct = notificationService.findNotifAccByMemberAndAccessModule(liveStreamChannel.getMemberId(), Global.AccessModule.WHALE_MEDIA);
        if (userAcct != null) {
            List<String> acc_ids_en = new ArrayList<>();
            List<String> acc_ids_zh = new ArrayList<>();
            List<String> acc_ids_ms = new ArrayList<>();

            List<Follower> followers = followerService.getFollowerByMemberId(liveStreamChannel.getMemberId());
            for (Follower follower : followers) {
                boolean isBlocking = memberBlockService.isMemberBlocked(liveStreamChannel.getMemberId(), follower.getFollowerId()); //whether vj block follower
                if (isBlocking)
                    continue;

                Member followerMember = memberService.getMember(follower.getFollowerId());
                if (!followerMember.getSendNotifFollowing()) //if follower turn off receive notification, skip.
                    continue;

                NotificationAccount followerAcct = notificationService.findNotifAccByMemberAndAccessModule(follower.getFollowerId(), Global.AccessModule.WHALE_MEDIA);
                if (followerAcct != null) {
                    if (followerMember.getPreferLanguage().equals(Global.Language.CHINESE)) {
                        acc_ids_zh.add(followerAcct.getAccountId());
                    } else if (followerMember.getPreferLanguage().equals(Global.Language.MALAY)) {
                        acc_ids_ms.add(followerAcct.getAccountId());
                    } else {
                        acc_ids_en.add(followerAcct.getAccountId());
                    }
                }
            }

            notificationConfProvider.doSendMessageAll(userAcct.getAccountId(), acc_ids_en, Global.AccessModule.WHALE_MEDIA,
                    "", i18n.getText("message.userOnStreamingMessage", Global.LOCALE_EN, liveMember.getMemberDetail().getProfileName()),
                    Global.Language.ENGLISH, payload);

            notificationConfProvider.doSendMessageAll(userAcct.getAccountId(), acc_ids_zh, Global.AccessModule.WHALE_MEDIA,
                    "", i18n.getText("message.userOnStreamingMessage", Global.LOCALE_CN, liveMember.getMemberDetail().getProfileName()),
                    Global.Language.CHINESE, payload);

            notificationConfProvider.doSendMessageAll(userAcct.getAccountId(), acc_ids_ms, Global.AccessModule.WHALE_MEDIA,
                    "", i18n.getText("message.userOnStreamingMessage", Global.LOCALE_MS, liveMember.getMemberDetail().getProfileName()),
                    Global.Language.MALAY, payload);
        }
    }

    private static final Object synchronizedDoAddChannelViewObject = new Object();

    @Override
    public void doAddChannelView(String channelId) {
        synchronized (synchronizedDoAddChannelViewObject) {
            liveStreamChannelDao.doUpdateLatestAccumulateView(channelId, Global.Status.ACTIVE);
            LiveStreamChannel liveStreamChannel = liveStreamChannelDao.findLiveStreamChannelByStatus(channelId, Global.Status.ACTIVE);
            if (liveStreamChannel != null) {
                liveStreamChannelDao.refresh(liveStreamChannel);
            }
        }
    }

    @Override
    public ImmutableTriple<Boolean, String, String> doVerifyThreeDaysChecking(List<Date> dates, Date dateFrom, Date dateTo) {
        if (dates.size() == 0) {
            return new ImmutableTriple<Boolean, String, String>(true, new SimpleDateFormat("yyyy-MM-dd").format(dateFrom), new SimpleDateFormat("yyyy-MM-dd").format(dateTo));
        }

        if (!dates.contains(dateFrom)) {
            long diff = DateUtil.getDaysDiffBetween2Dates(dateFrom, dates.get(0));
            Date date = DateUtil.addDate(dates.get(0), -1);
            if (diff > 3) {
                return new ImmutableTriple<Boolean, String, String>(true, new SimpleDateFormat("yyyy-MM-dd").format(dateFrom), new SimpleDateFormat("yyyy-MM-dd").format(date));
            }
        }

        for (int i = 0; i < dates.size() - 1; i++) {
            Date date1 = dates.get(i);
            Date date2 = dates.get(i + 1);
            long diff = DateUtil.getDaysDiffBetween2Dates(date1, date2);

            if (diff > 3) {
                date1 = DateUtil.addDate(date1, 1);
                date2 = DateUtil.addDate(date2, -1);
                return new ImmutableTriple<Boolean, String, String>(true, new SimpleDateFormat("yyyy-MM-dd").format(date1), new SimpleDateFormat("yyyy-MM-dd").format(date2));
            }
        }

        if (!dates.contains(dateTo)) {
            long diff = DateUtil.getDaysDiffBetween2Dates( dates.get(dates.size()-1),dateTo);
            if (diff > 3) {
                return new ImmutableTriple<Boolean, String, String>(true, new SimpleDateFormat("yyyy-MM-dd").format(dates.get(dates.size()-1)), new SimpleDateFormat("yyyy-MM-dd").format(dateTo));
            }
        }

        return new ImmutableTriple<Boolean, String, String>(false, "", "");
    }

    @Override
    public void doProcessUntrackAudience(String channelId, String memberId) {
        liveStreamAudienceSqlDao.doProcessUntrackAudience(channelId, memberId);
    }

    @Override
    public void doSendGiftAnnouncementToAllRoom(String accountId, String contributorMemberId, String
            receiverId, SymbolicItem symbolicItem) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        LiveStreamChannel directToChannel = getLiveStreamChannelByMemberId(receiverId, false);
        Member receiverMember = memberService.getMember(directToChannel.getMemberId());
        Member contributeMember = memberService.getMember(contributorMemberId);
        MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(receiverMember.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

        HashMap<String, String> localizeMsg = new HashMap<>();
        localizeMsg.put(Global.Language.ENGLISH, i18n.getText("sendGiftGlobalAnnouncementMsg", Global.LOCALE_EN,
                contributeMember.getMemberDetail().getProfileName(), receiverMember.getMemberDetail().getProfileName(), symbolicItem.getName()));
        localizeMsg.put(Global.Language.CHINESE, i18n.getText("sendGiftGlobalAnnouncementMsg", Global.LOCALE_CN,
                contributeMember.getMemberDetail().getProfileName(), receiverMember.getMemberDetail().getProfileName(), symbolicItem.getNameCn()));

        String vjAvatarUrl = "";
        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(cryptoProvider.getFileUploadUrl() + "/file/memberFile");
            vjAvatarUrl = memberFile.getFileUrl();
        }

        String coverUrl = "";
        if (StringUtils.isNotBlank(directToChannel.getFilename())) {
            coverUrl = directToChannel.getFileUrlWithParentPath(cryptoProvider.getFileUploadUrl() + "/file/liveStreamCover");
        }

        HashMap<String, Object> map = new HashMap<>();
        map.put("announcementMsg", localizeMsg);
        map.put("textColor", "#ffffff");
        map.put("backgroundColor", "#f48e8e");

        //channel info
        map.put("channelId", directToChannel.getChannelId());
        map.put("privateLive", String.valueOf(directToChannel.getPrivateLive()));
        map.put("whaleLiveId", receiverMember.getMemberDetail().getWhaleliveId());
        map.put("profileName", receiverMember.getMemberDetail().getProfileName());
        map.put("vjAvatarUrl", vjAvatarUrl);
        map.put("coverUrl", coverUrl);
        map.put("httpPullUrl", directToChannel.getHttpPullUrl());
        map.put("hlsPullUrl", directToChannel.getHlsPullUrl());
        map.put("rtmpPullUrl", directToChannel.getRtmpPullUrl());

        Gson contentObject = new Gson();
        String contentObjectJsonString = contentObject.toJson(map);

        List<PointGlobalTrx> pointGlobalTrxList = new ArrayList<>();

        List<LiveStreamChannel> activeChannelList = findAllLiveStreamChannel(Global.Status.ACTIVE, false);
        for (LiveStreamChannel liveStreamChannel : activeChannelList) {
            if (liveStreamChannel.getChannelId().equals(directToChannel.getChannelId()))
                continue;

//            liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", liveStreamChannel.getChannelId(), accountId, "SYSTEM", "",
//                    contentObjectJsonString, "ANNOUNCEMENT", "", null, "", 0);
            PointGlobalTrx pointGlobalTrx = new PointGlobalTrx();
            pointGlobalTrx.setAccountId(accountId);
            pointGlobalTrx.setChannelId(liveStreamChannel.getChannelId());
            pointGlobalTrx.setContent(contentObjectJsonString);
            pointGlobalTrxList.add(pointGlobalTrx);
        }
        pointGlobalTrxDao.saveAll(pointGlobalTrxList);
    }

    @Override
    public LinkedHashMap getDatesOfLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo) {
        return liveStreamChannelSqlDao.getDatesOfLiveStreamChannel(ownerId, dateFrom, dateTo);

    }

    @Override
    public LiveStreamAudience findChannelAudienceByRefNo(String refNo, String channelId, String memberId) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
//        if(redisCacheProvider.getRedisStatus()) {
//            try {
//                LiveStreamAudience liveStreamAudience = redisCacheProvider.getAudienceList().get(refNo);
//                if(liveStreamAudience == null) {
//                    log.debug("Audience redis not found");
//                    liveStreamAudience = liveStreamAudienceDao.findChannelAudienceByRefNo(refNo, channelId, null);
//                } else {
//                    redisCacheProvider.removeAudience(liveStreamAudience);
//                }
//                return liveStreamAudience;
//            } catch (RedisConnectionException redisEx) {
//                redisCacheProvider.setRedisStatus(false);
//            }
//        }
        return liveStreamAudienceDao.findChannelAudienceByRefNo(refNo, channelId, null);
    }

    @Override
    public List<LiveStreamAudience> findNewestAudience() {
        return liveStreamAudienceDao.findNewestAudience();
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamChannelByMemberCodeAndUserType(String memberCode, String userType, Date dateFrom, Date dateTo) {
        return liveStreamChannelSqlDao.findLiveStreamChannelByMemberCodeAndUserType(memberCode, userType, dateFrom, dateTo);
    }

    @Override
    public LiveStreamChannel doUpdateUserLinkingChannel(String channelName, String uid) {
        synchronized (synchronizedObject) {
            LiveStreamChannel liveStreamChannel = liveStreamChannelDao.getLiveStreamChannelByChannelName(channelName, Global.Status.ACTIVE);
            if (liveStreamChannel != null) {
                liveStreamChannelDao.updateUserLinkingChannel(liveStreamChannel.getChannelId(), uid);
                liveStreamChannel = liveStreamChannelDao.findLiveStreamChannelByStatus(liveStreamChannel.getChannelId(), Global.Status.ACTIVE);
            }

            return liveStreamChannel;
        }
    }

    @Override
    public String doProcessAgoraCallBack(String signature, String body) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        WsMessageService wsMessageService = Application.lookupBean(WsMessageService.BEAN_NAME, WsMessageService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        LiveChatRoomProvider liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.BEAN_NAME, LiveChatRoomProvider.class);

        synchronized (synchronizedObject) {
            String errorMsg;

            AgoraMessageLog agoraMessageLog = new AgoraMessageLog(true);
            agoraMessageLog.setBody(body);
            agoraMessageLogDao.save(agoraMessageLog);

            boolean valid = false;
            try {
                AgoraClient agoraClient = new AgoraClient();
                valid = agoraClient.checkSignature(signature, body);
            } catch (Exception e) {
                agoraMessageLog.setErrorMessage(e.getMessage());
                agoraMessageLogDao.update(agoraMessageLog);
            }

            if (!valid) {
                errorMsg = "Incorrect agora signature";
                agoraMessageLog.setErrorMessage(errorMsg);
                agoraMessageLogDao.update(agoraMessageLog);
                return errorMsg;
            }

            AgoraListener agoraListener;
            boolean skipProcess = false;
            try {
                ObjectMapper objectMapper = new ObjectMapper();
                agoraListener = objectMapper.readValue(body, AgoraListener.class);
                Gson gson = new Gson();
                String jsonPayload = gson.toJson(agoraListener.getPayload());

                agoraMessageLog.setSid(agoraListener.getSid());
                agoraMessageLog.setNoticeId(agoraListener.getNoticeId());
                agoraMessageLog.setProductId(agoraListener.getProductId());
                agoraMessageLog.setEventType(agoraListener.getEventType());
                agoraMessageLog.setNotifyMs(agoraListener.getNotifyMs());
                agoraMessageLog.setPayload(jsonPayload);
                agoraMessageLogDao.update(agoraMessageLog);
            } catch (Exception e) {
                errorMsg = "Agora object mapping failed";
                agoraMessageLog.setErrorMessage(errorMsg);
                agoraMessageLogDao.update(agoraMessageLog);
                e.printStackTrace();
                return errorMsg;
            }

            if (agoraListener.getEventType() == null) {
                errorMsg = "EventType is null";
                agoraMessageLog.setErrorMessage(errorMsg);
                agoraMessageLogDao.update(agoraMessageLog);
                return errorMsg;
            }

            if (StringUtils.isNotBlank(agoraListener.getNoticeId())) {
                List<AgoraMessageLog> agoraMessageList = agoraMessageLogDao.findAgoraMessageByNoticeId(agoraListener.getNoticeId());
                if (agoraMessageList.size() > 1) {
                    skipProcess = true;
                }
            }

            try {
                if (!skipProcess) {
                    LiveStreamChannel liveStreamChannel;
                    LiveStreamChannel oriLiveStreamChannel;
                    String channelName = agoraListener.getPayload().getChannelName();

                    System.out.println("signature = " + signature);
                    System.out.println("requestBody = " + body);

                    System.out.println("\n EVENT TYPE = " + agoraListener.getEventType());
                    switch (agoraListener.getEventType()) {
                        case AgoraListener.EVENT_TYPE_CREATE_CHANNEL:
                            System.out.println("EVENT_TYPE_CREATE_CHANNEL");
                            break;
                        case AgoraListener.EVENT_TYPE_DESTROY_CHANNEL:
                            System.out.println("EVENT_TYPE_DESTROY_CHANNEL");
                            break;
                        case AgoraListener.EVENT_TYPE_BC_JOIN_CHANNEL:
                            System.out.println("EVENT_TYPE_BC_JOIN_CHANNEL");
                            if (doCheckChannelNameExists(channelName, agoraMessageLog)) {
                                Member member = memberService.findMemberByAgoraUid(agoraListener.getPayload().getUid()); //check is vj OR voice/video linking audience
                                if (member != null) {
                                    if (StringUtils.isNotBlank(member.getChannelName()) && member.getChannelName().equals(channelName)) { //is vj
                                        long timeStamp = (agoraListener.getPayload().getTs() != null) ? (agoraListener.getPayload().getTs() * 1000) : System.currentTimeMillis(); //to milliseconds

                                        liveStreamChannelSqlDao.updateChannelActive(channelName, new Date(timeStamp));
                                        liveStreamChannel = getLiveStreamChannelByChannelName(channelName, Global.Status.ACTIVE);
                                        if (liveStreamChannel != null) {
                                            if (liveStreamChannel.getMemberId().equals("ff8081817714c47b017714dd952a09a2")) { //小琪
                                                String msg = member.getMemberCode() + " [" + member.getMemberDetail().getProfileName() + "] is on streaming.";
                                                BotClient botClient = new BotClient();
                                                botClient.doSendReportMessage(msg);
                                            }
                                            doProcessLiveStreamNotification(liveStreamChannel); // pop up notification
                                        }
                                    }
                                }
                            }
                            break;
                        case AgoraListener.EVENT_TYPE_BC_LEAVE_CHANNEL:
                            System.out.println("EVENT_TYPE_BC_LEAVE_CHANNEL");
                            if (doCheckChannelNameExists(channelName, agoraMessageLog)) {
                                Member member = memberService.findMemberByAgoraUid(agoraListener.getPayload().getUid()); //check is vj OR voice/video linking audience
                                if (member != null) {
                                    if (StringUtils.isNotBlank(member.getChannelName()) && member.getChannelName().equals(channelName)) { //is vj
                                        long timeStamp = (agoraListener.getPayload().getTs() != null) ? (agoraListener.getPayload().getTs() * 1000) : System.currentTimeMillis(); //to milliseconds
                                        int idleSeconds = (agoraListener.getPayload().getReason() != null && agoraListener.getPayload().getReason() == 2) ? 10 : 0; //1 : normal end live, 2: disconnected

                                        liveStreamChannel = getLiveStreamChannelByChannelName(channelName, Global.Status.ACTIVE);
                                        if (liveStreamChannel != null) {
                                            liveStreamChannelDao.updateChannelDisconnected(channelName, idleSeconds, new Date(timeStamp), Global.Status.INACTIVE);
                                            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
                                            if (account != null) {
                                                liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", liveStreamChannel.getChannelId(), account.getAccountId(), "ADMIN", "", "", "END_LIVE", "", null, "", 0);
                                            }
                                        }
                                    } else { //voice or video link audience
                                        liveStreamChannel = doUpdateUserLinkingChannel(channelName, "");
                                        if (liveStreamChannel != null) {
                                            wsMessageService.updateBroadCasterInfo(liveStreamChannel, null);
                                        }
                                    }
                                }
                            }
                            break;
                        case AgoraListener.EVENT_TYPE_AUD_JOIN_CHANNEL:
                            System.out.println("EVENT_TYPE_AUD_JOIN_CHANNEL");
                            break;
                        case AgoraListener.EVENT_TYPE_AUD_LEAVE_CHANNEL:
                            System.out.println("EVENT_TYPE_AUD_LEAVE_CHANNEL");
                            break;
                        case AgoraListener.EVENT_TYPE_AUD_TO_BC: //voice or video link
                            // if user normal close linking, agora return EVENT_TYPE_BC_TO_AUD else if disconnected, return EVENT_TYPE_BC_LEAVE_CHANNEL
                            System.out.println("EVENT_TYPE_AUD_TO_BC");
                            oriLiveStreamChannel = liveStreamChannelDao.getLiveStreamChannelByChannelName(channelName, Global.Status.ACTIVE);
                            if (oriLiveStreamChannel != null && oriLiveStreamChannel.getLinkUserAgoraUid() == null) {
                                liveStreamChannel = doUpdateUserLinkingChannel(channelName, agoraListener.getPayload().getUid().toString());
                                wsMessageService.updateBroadCasterInfo(liveStreamChannel, agoraListener.getPayload().getUid().toString());
                            }
                            break;
                        case AgoraListener.EVENT_TYPE_BC_TO_AUD:
                            System.out.println("EVENT_TYPE_BC_TO_AUD");
                            oriLiveStreamChannel = liveStreamChannelDao.getLiveStreamChannelByChannelName(channelName, Global.Status.ACTIVE);
                            if (oriLiveStreamChannel != null && oriLiveStreamChannel.getLinkUserAgoraUid() != null) {
                                liveStreamChannel = doUpdateUserLinkingChannel(channelName, "");
                                if (liveStreamChannel != null) {
                                    wsMessageService.updateBroadCasterInfo(liveStreamChannel, null);
                                }
                            }
                            break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }

    private boolean doCheckChannelNameExists(String channelName, AgoraMessageLog agoraMessageLog) {
        if (StringUtils.isBlank(channelName)) {
            agoraMessageLog.setErrorMessage("Payload.channelName is blank");
            agoraMessageLogDao.update(agoraMessageLog);
            return false;
        }

        return true;
    }

    @Override
    public void doProcessVjConnectReject(String userMemberId, String hostMemberId, String rejectDuration) {
        synchronized (synchronizedObject) {
            if (StringUtils.isNotBlank(userMemberId) && StringUtils.isNotBlank(hostMemberId) && !rejectDuration.equals("0")) {
                LiveStreamChannel channel = getLatestLiveStreamChannelByMemberId(userMemberId, Global.Status.ACTIVE);

                if (channel != null) {
                    VjConnectReject vjConnectReject = new VjConnectReject(true);
                    vjConnectReject.setVjChannelId(channel.getChannelId());
                    vjConnectReject.setHostMemberId(hostMemberId);
                    vjConnectReject.setRejectDuration(rejectDuration);
                    vjConnectReject.setAllowConnectDatetime(DateUtil.addMinute(new Date(), Integer.parseInt(rejectDuration)));
                    vjConnectRejectDao.save(vjConnectReject);
                }
            }
        }
    }

    @Override
    public void doProcessVjConnectAccept(Locale locale, String hostMemberId, String connectorMemberId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        LiveChatRoomProvider liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.BEAN_NAME, LiveChatRoomProvider.class);

        synchronized (synchronizedObject) {
            /************************
             * VERIFICATION - START
             ************************/

            if (StringUtils.isBlank(hostMemberId)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            Member hostMember = memberService.getMember(hostMemberId);
            if (hostMember == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (StringUtils.isBlank(connectorMemberId)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            Member connectorMember = memberService.getMember(connectorMemberId);
            if (connectorMember == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            LiveStreamChannel hostChannel = getLatestLiveStreamChannelByMemberId(hostMemberId, Global.Status.ACTIVE);
            if (hostChannel == null) {
                throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
            }

            LiveStreamChannel connectorChannel = getLatestLiveStreamChannelByMemberId(connectorMemberId, Global.Status.ACTIVE);
            if (connectorChannel == null) {
                throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
            }

            /************************
             * VERIFICATION - END
             ************************/

            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
//            NotificationAccount hostAccount = notificationService.findNotifAccByMemberAndAccessModule(hostMemberId, Global.AccessModule.WHALE_MEDIA);
//            NotificationAccount connectorAccount = notificationService.findNotifAccByMemberAndAccessModule(connectorMemberId, Global.AccessModule.WHALE_MEDIA);
            if (account != null) {
                //close old vj connect
                doProcessEndVjConnect(hostChannel.getMemberId(), VjConnect.STATUS_SWITCH, account.getAccountId());
                doProcessEndVjConnect(connectorChannel.getMemberId(), VjConnect.STATUS_SWITCH, account.getAccountId());

                //close new vj connect
                VjConnect vjConnect = new VjConnect(true);
                vjConnect.setHostChannelId(hostChannel.getChannelId());
                vjConnect.setConnectorChannelId(connectorChannel.getChannelId());
                vjConnect.setHostMemberId(hostChannel.getMemberId());
                vjConnect.setConnectorMemberId(connectorChannel.getMemberId());
                vjConnectDao.save(vjConnect);

                liveStreamChannelDao.updateVjConnectChannel(hostChannel.getChannelId(), true);
                liveStreamChannelDao.updateVjConnectChannel(connectorChannel.getChannelId(), true);

                List<VjConnectDto> dtoList = getVjConnectDtoList(hostChannel.getChannelId(), connectorChannel.getChannelId(),
                        hostChannel.getChannelName(), connectorChannel.getChannelName(), hostMember.getAgoraUid(), connectorMember.getAgoraUid());

                Gson contentObject = new Gson();
                String contentObjectJsonString = contentObject.toJson(dtoList);
                liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", connectorChannel.getChannelId(), account.getAccountId(), "ADMIN", "", contentObjectJsonString, "VJ_CONNECT_START", "", null, "", 0);
                liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", hostChannel.getChannelId(), account.getAccountId(), "ADMIN", "", contentObjectJsonString, "VJ_CONNECT_START", "", null, "", 0);
            }
        }
    }

    @Override
    public List<VjConnectDto> getVjConnectDtoList(String hostChannelId, String connectorChannelId, String hostChannelName, String connectorChannelName, Integer hostUid, Integer connectorUid) {
        String hostSrcToken = null;
        String hostDestToken = null;
        String connectorSrcToken = null;
        String connectorDestToken= null;
        try {
            AgoraClient agoraClient = new AgoraClient();
            hostSrcToken = agoraClient.getRtcToken(hostChannelName, 0);
            hostDestToken = agoraClient.getRtcToken(connectorChannelName, hostUid);
            connectorSrcToken = agoraClient.getRtcToken(connectorChannelName, 0);
            connectorDestToken = agoraClient.getRtcToken(hostChannelName, connectorUid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<VjConnectDto> dtoList = new ArrayList<>();
        //host info
        VjConnectDto vjConnectDto = new VjConnectDto(hostChannelId, hostUid, hostSrcToken, hostDestToken, hostChannelName, connectorChannelName, 0, hostUid);
        dtoList.add(vjConnectDto);

        //connector info
        vjConnectDto = new VjConnectDto(connectorChannelId, connectorUid, connectorSrcToken, connectorDestToken, connectorChannelName, hostChannelName, 0, connectorUid);
        dtoList.add(vjConnectDto);

        return dtoList;
    }

    @Override
    public VjConnect findVjConnectByMemberId(String memberId) {
        return vjConnectDao.findVjConnectByMemberId(memberId);
    }

    @Override
    public void doProcessEndVjConnect(String memberId, String status, String accountId) {
        LiveChatRoomProvider liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.BEAN_NAME, LiveChatRoomProvider.class);

        VjConnect vjConnect = findVjConnectByMemberId(memberId);
        if (vjConnect != null) {
            vjConnectDao.updateVjConnectStatus(vjConnect.getConnectId(), status, new Date());
            liveStreamChannelDao.updateVjConnectChannel(vjConnect.getHostChannelId(), false);
            liveStreamChannelDao.updateVjConnectChannel(vjConnect.getConnectorChannelId(), false);
            liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", vjConnect.getHostChannelId(), accountId, "ADMIN", "", "", "VJ_CONNECT_END", "", null, "", 0);
            liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", vjConnect.getConnectorChannelId(), accountId, "ADMIN", "", "", "VJ_CONNECT_END", "", null, "", 0);
        }
    }
}