package com.compalsolutions.compal.live.chatroom.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoomLog;

public interface LiveChatRoomLogDao extends BasicDao<LiveChatRoomLog, String> {
    public static final String BEAN_NAME = "liveChatRoomLogDao";

}
