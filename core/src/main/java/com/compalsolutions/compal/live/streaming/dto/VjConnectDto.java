package com.compalsolutions.compal.live.streaming.dto;

public class VjConnectDto {
//    private String vjChannelId;
//    private String vjSrcToken;
//    private String vjDestToken;
//    private String vjSrcChannelName;
//    private String vjDestChannelName;
//    private String vjSrcUid;
//    private String vjDestUid;

    private String channelId;
    private Integer uid;
    private String srcToken;
    private String destToken;
    private String srcChannelName;
    private String destChannelName;
    private Integer srcUid;
    private Integer destUid;

    public VjConnectDto() {
    }

    public VjConnectDto(String channelId, Integer uid, String srcToken, String destToken, String srcChannelName, String destChannelName, Integer srcUid, Integer destUid) {
        this.channelId = channelId;
        this.uid = uid;
        this.srcToken = srcToken;
        this.destToken = destToken;
        this.srcChannelName = srcChannelName;
        this.destChannelName = destChannelName;
        this.srcUid = srcUid;
        this.destUid = destUid;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getSrcToken() {
        return srcToken;
    }

    public void setSrcToken(String srcToken) {
        this.srcToken = srcToken;
    }

    public String getDestToken() {
        return destToken;
    }

    public void setDestToken(String destToken) {
        this.destToken = destToken;
    }

    public String getSrcChannelName() {
        return srcChannelName;
    }

    public void setSrcChannelName(String srcChannelName) {
        this.srcChannelName = srcChannelName;
    }

    public String getDestChannelName() {
        return destChannelName;
    }

    public void setDestChannelName(String destChannelName) {
        this.destChannelName = destChannelName;
    }

    public Integer getSrcUid() {
        return srcUid;
    }

    public void setSrcUid(Integer srcUid) {
        this.srcUid = srcUid;
    }

    public Integer getDestUid() {
        return destUid;
    }

    public void setDestUid(Integer destUid) {
        this.destUid = destUid;
    }
}
