package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.live.streaming.vo.VjConnect;

import java.math.BigDecimal;
import java.util.Date;

public interface VjConnectDao extends BasicDao<VjConnect, String> {
    public static final String BEAN_NAME = "vjConnectDao";

    public VjConnect findVjConnectByMemberId(String memberId);

    public void updateVjConnectStatus(String connectId, String status, Date endDatetime);

    public void updateVjConnectTotalPoint(BigDecimal point, String connectId, boolean isHost);
}
