package com.compalsolutions.compal.live.chatroom;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.live.chatroom.client.LiveChatRoomClient;
import com.compalsolutions.compal.live.chatroom.client.dto.LiveChatResponse;
import com.compalsolutions.compal.live.chatroom.service.LiveChatService;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.member.vo.Member;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service(LiveChatRoomProvider.BEAN_NAME)
public class LiveChatRoomProviderImpl implements LiveChatRoomProvider {
    private Log log = LogFactory.getLog(LiveChatRoomProviderImpl.class);

    private LiveChatService liveChatService;

    @Autowired
    public LiveChatRoomProviderImpl(LiveChatService liveChatService) {
        this.liveChatService = liveChatService;
    }

    @Override
    public LiveChatResponse doCreateLiveChatRoom(String channelId, String accId, String roomName, String announcement) {
        LiveChatRoomClient liveChatRoomClient = new LiveChatRoomClient();

        LiveChatResponse response;
        LiveChatRoom liveChatRoom = liveChatService.findChatRoomByCreator(accId);
        if (liveChatRoom != null) {
            response = liveChatRoomClient.getLiveChatDetail(liveChatRoom.getRoomId());
            if (response.getCode() != 200) {
                throw new ValidatorException(response.getMsg());
            }

            if (response.getChatroom() != null) { //already have chatroom, update local db channel id
                liveChatRoom.setChannelId(channelId);
                liveChatRoom.setMuteList("");
                liveChatService.doUpdateLiveChatRoom(liveChatRoom);
            } else { //chat room not found, create new chatroom
                response = liveChatRoomClient.createChatRoom(channelId, roomName, announcement, accId, Global.AccessModule.WHALE_MEDIA);

                if (response.getChatroom() != null) {
                    liveChatService.doCreateLiveChatroom(channelId, response.getChatroom());
                }
            }
        } else {
            response = liveChatRoomClient.createChatRoom(channelId, roomName, announcement, accId, Global.AccessModule.WHALE_MEDIA);

            if (response.getChatroom() != null) {
                liveChatService.doCreateLiveChatroom(channelId, response.getChatroom());
            }
        }

        return response;
    }

    @Override
    public LiveChatResponse doSendMessageToChatRoom(String msgType, String channelId, String accId, String senderName, String senderAvatarUrl, String message, String type, String memberId, Member member, String symbolicItemId, int qty) {
        LiveChatRoomClient liveChatRoomClient = new LiveChatRoomClient();
        log.debug("doSendMessageToChatRoom: " + channelId);
        LiveChatResponse response = null;
        LiveChatRoom liveChatRoom = liveChatService.findChatRoomByChannelId(channelId);
        if (liveChatRoom != null) {
            response = liveChatRoomClient.getLiveChatDetail(liveChatRoom.getRoomId());
            if (response.getCode() != 200) {
                throw new ValidatorException(response.getMsg());
            }
            log.debug("channelId response 200");

            if (response.getChatroom() != null) { //already have chatroom, update local db channel id
                    if (type.equalsIgnoreCase("GIFT_MESSAGE")) {
                        response = liveChatRoomClient.sendGiftToChatRoom(response.getChatroom().getRoomid(), accId, type, channelId, memberId, symbolicItemId, qty, Global.AccessModule.WHALE_MEDIA);
                    } else if (msgType.equalsIgnoreCase("REDIRECT")) { //click chat msg get user profile info
                        response = liveChatRoomClient.sendRedirectMsgToChatRoom(response.getChatroom().getRoomid(), accId, senderName, senderAvatarUrl, message, type, member, memberId, Global.AccessModule.WHALE_MEDIA);
                    } else {
                        response = liveChatRoomClient.sendMsgToChatRoom(response.getChatroom().getRoomid(), accId, senderName, senderAvatarUrl, message, type, Global.AccessModule.WHALE_MEDIA);
                    }
            }
        } else {
            System.out.println("type: "+type+", Chat Room Not Exists.");
//            throw new ValidatorException("Chat Room Not Exists");
        }

        return response;
    }

}
