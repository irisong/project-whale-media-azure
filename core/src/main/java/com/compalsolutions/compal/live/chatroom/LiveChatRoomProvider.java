package com.compalsolutions.compal.live.chatroom;

import com.compalsolutions.compal.live.chatroom.client.dto.LiveChatResponse;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.dto.SymbolicItemDto;
import com.compalsolutions.compal.point.vo.SymbolicItem;

public interface LiveChatRoomProvider {
    public static final String BEAN_NAME = "liveChatRoomProvider";

    public LiveChatResponse doCreateLiveChatRoom(String channelId, String accId, String roomName, String announcement);

    public LiveChatResponse doSendMessageToChatRoom(String msgType, String channelId, String accId, String senderName, String senderAvatarUrl, String message, String type, String memberId, Member member, String symbolicItemId, int qty);
}
