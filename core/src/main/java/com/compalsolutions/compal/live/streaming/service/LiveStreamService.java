package com.compalsolutions.compal.live.streaming.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.live.streaming.dto.VjConnectDto;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamLog;
import com.compalsolutions.compal.live.streaming.vo.VjConnect;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.File;
import java.math.BigDecimal;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

public interface LiveStreamService {
    public static final String BEAN_NAME = "liveStreamService";

    LiveStreamLog doCreateLiveStreamLog(String accessModule, String actionType, String requestBody);

    void doUpdateLiveStreamLog(LiveStreamLog liveStreamLog);

    void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String userType, String memberCode, Date dateFrom, Date dateTo);

    public int getNoOfTotalLiveStreamChannel();

    public int getNoOfDayForLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo);

    LiveStreamChannel doCreateLiveStreamChannel(Member member, String category, String title, boolean privateLive, String accessCode);

    void doDeleteOldLiveStreamChannel(LiveStreamChannel liveStreamChannel);

    public void doLiveStreamChannelUpdateError(LiveStreamChannel liveStreamChannel);

    void doDeleteLiveStreamChannel(String channelId, int totalView, long duration);

    LiveStreamChannel getLiveStreamChannel(String channelId, String category, boolean localDatabase);

    public void findLiveStreamListing(DatagridModel datagridModel, String category);

    public void findLiveStreamConnectListing(DatagridModel datagridModel, String memberId, String searchContent);

    public LiveStreamChannel getLiveStreamChannelByChannelId(String channelId);

    public List<LiveStreamChannel> findAllLiveStreamChannel(String status, boolean localDatabase);

    List<LiveStreamChannel> findLiveStreamChannelsByMemberId(String memberId, String status);

    List<LiveStreamChannel> findLiveStreamChannelsByMemberIdAndDate(String memberId, String status,Date dateFrom, Date dateTo);

    public boolean checkIsBlockedMemberChannel(String memberId, boolean localDatabase);

    LiveStreamChannel getLiveStreamChannelByMemberId(String memberId, boolean localDatabase);

    public LiveStreamChannel getLiveStreamChannelByChannelName(String channelName, String status);

    void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String memberId, List<String> followerIds);

    public void findLiveStreamRecordsInfoForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String memberId, Date dateFrom, Date dateTo);

    public Triple<Long, Integer, BigDecimal> getLiveRecordTotal(String memberId, Date dateFrom, Date dateTo);

    public Pair<Date, Date> getFromToDate(String dateRangeType, Date dateFrom, Date dateTo);

   // public void findActiveMemberInLiveStreamByChannelId(DatagridModel<LiveStreamAudience> datagridModel, String channelId);

    public List<String> findActiveMemberInLiveStreamByChannelId(String channelId);

    public List<String> findEligibleWinnerForLuckyDraw(String channelId, String packageNo);

    public void doBlockLiveStreamChannel(LiveStreamChannel liveStreamChannel, String status, String reason);

    public LiveStreamChannel getLatestLiveStreamChannelByMemberId(String memberId, String status);

    public LiveStreamChannel doProcessLiveStreamCoverUploadFile(Locale locale, String memberId, File fileUpload, String contentType, String fileName);

    public String doProcessTrackWatchLiveTime(Locale locale, String memberId, String actionType, String channelId, String refNo, boolean isOffline, long duration);

    public void doProcessLiveStreamNotification(LiveStreamChannel liveStreamChannel);

    public void doAddChannelView(String channelId);

    public ImmutableTriple<Boolean,String,String> doVerifyThreeDaysChecking(List<Date> dates, Date dateFrom, Date dateTo);

    public void doProcessUntrackAudience(String channelId, String memberId);

    public void doSendGiftAnnouncementToAllRoom(String accountId, String contributorMemberId, String receiverId, SymbolicItem symbolicItem);

    public LinkedHashMap getDatesOfLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo);

    public LiveStreamAudience findChannelAudienceByRefNo(String refNo, String channelId, String memberId);

    public List<LiveStreamAudience> findNewestAudience();

    List<LiveStreamChannel> findLiveStreamChannelByMemberCodeAndUserType(String memberCode, String userType, Date dateFrom, Date dateTo);

    public LiveStreamChannel doUpdateUserLinkingChannel(String channelName, String uid);

    public String doProcessAgoraCallBack(String signature, String body);

    public void doProcessVjConnectReject(String userMemberId, String hostMemberId, String rejectDuration);

    public void doProcessVjConnectAccept(Locale locale, String hostMemberId, String connectorMemberId);

    public List<VjConnectDto> getVjConnectDtoList(String userChannelId, String connectorChannelId, String userChannelName, String connectorChannelName, Integer userUid, Integer connectorUid);

    public VjConnect findVjConnectByMemberId(String memberId);

    public void doProcessEndVjConnect(String memberId, String status, String accountId);
}
