package com.compalsolutions.compal.live.chatroom.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoomLog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(LiveChatRoomLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveChatRoomLogDaoImpl extends Jpa2Dao<LiveChatRoomLog, String> implements LiveChatRoomLogDao {

    public LiveChatRoomLogDaoImpl() {
        super(new LiveChatRoomLog(false));
    }
}
