package com.compalsolutions.compal.live.chatroom.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;

public interface LiveChatRoomDao extends BasicDao<LiveChatRoom, String> {
    public static final String BEAN_NAME = "liveChatRoomDao";

    public LiveChatRoom findLiveChatRoom(String channelId);

    public LiveChatRoom findLiveChatRoomByCreator(String creator);
}
