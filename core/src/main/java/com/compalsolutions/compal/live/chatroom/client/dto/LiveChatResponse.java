package com.compalsolutions.compal.live.chatroom.client.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LiveChatResponse {
    private int code;
    private String msg;
   // private String desc;
    private ChatRoomDto chatroom;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

//    public String getDesc() {
//        return desc;
//    }
//
//    public void setDesc(String desc) {
//        this.desc = desc;
//    }

    public ChatRoomDto getChatroom() {
        return chatroom;
    }

    public void setChatroom(ChatRoomDto chatroom) {
        this.chatroom = chatroom;
    }
}