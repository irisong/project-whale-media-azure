package com.compalsolutions.compal.live.chatroom.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(LiveChatRoomDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveChatRoomDaoImpl extends Jpa2Dao<LiveChatRoom, String> implements LiveChatRoomDao {

    public LiveChatRoomDaoImpl() {
        super(new LiveChatRoom(false));
    }

    @Override
    public LiveChatRoom findLiveChatRoom(String channelId) {
        List<Object> params = new ArrayList<Object>();

        String hql = " FROM m IN " + LiveChatRoom.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(channelId)) {
            hql += " AND m.channelId = ? ";
            params.add(channelId);
        }

        return findFirst(hql, params.toArray());
    }

    @Override
    public LiveChatRoom findLiveChatRoomByCreator(String creator) {
        List<Object> params = new ArrayList<Object>();

        String hql = " FROM m IN " + LiveChatRoom.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(creator)) {
            hql += " AND m.creator = ? ";
            params.add(creator);
        }

        return findFirst(hql, params.toArray());
    }
}
