package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(LiveStreamChannelDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveStreamChannelDaoImpl extends Jpa2Dao<LiveStreamChannel, String> implements LiveStreamChannelDao {

    public LiveStreamChannelDaoImpl() {
        super(new LiveStreamChannel(false));
    }

    @Override
    public LiveStreamChannel findLiveStreamChannel(String channelId, String category) {
        Validate.notBlank(channelId);

        LiveStreamChannel example = new LiveStreamChannel(false);
        example.setChannelId(channelId);
        example.setStatus(Global.Status.ACTIVE);

        if (StringUtils.isNotBlank(category)) {
            example.setCategory(category);
        }

        return findUnique(example);
    }

    @Override
    public LiveStreamChannel findLiveStreamChannelByStatus(String channelId, String status) {
        Validate.notBlank(channelId);
        Validate.notBlank(status);

        LiveStreamChannel example = new LiveStreamChannel(false);
        example.setChannelId(channelId);
        example.setStatus(status);

        return findUnique(example);
    }

    @Override
    public LiveStreamChannel findLiveStreamChannelByChannelId(String channelId) {
        Validate.notBlank(channelId);

        LiveStreamChannel example = new LiveStreamChannel(false);
        example.setChannelId(channelId);

        return findUnique(example);
    }

    @Override
    public List<LiveStreamChannel> findAllLiveStreamChannel(String status) {
        LiveStreamChannel example = new LiveStreamChannel(false);

        if (StringUtils.isNotBlank(status)) {
            example.setStatus(status);
        }

        return findByExample(example, "startDate desc");
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamChannelsByMemberId(String memberId, String status) {
        Validate.notBlank(memberId);

        LiveStreamChannel example = new LiveStreamChannel(false);
        example.setMemberId(memberId);
        example.setStatus(status);

        return findByExample(example);
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamChannelsByMemberIdAndDate(String memberId, String status, Date dateFrom, Date dateTo) {
        Validate.notBlank(memberId);

        LiveStreamChannel example = new LiveStreamChannel(false);
        example.setMemberId(memberId);
        example.setStatus(status);

        if (dateFrom != null)
            example.addDateFromCondition("startDate", DateUtil.truncateTime(dateFrom));
        if (dateTo != null)
            example.addDateToCondition("startDate", DateUtil.formatDateToEndTime(dateTo));

        return findByExample(example,"startDate");
    }

    @Override
    public LiveStreamChannel findLiveStreamChannelByMemberId(String memberId, String status) {
        Validate.notBlank(memberId);

        LiveStreamChannel example = new LiveStreamChannel(false);
        example.setMemberId(memberId);

        if (StringUtils.isNotBlank(status))
            example.setStatus(status);

        return findFirst(example, "datetimeUpdate desc");
    }

    @Override
    public void findLiveStreamRecordsPointListing(DatagridModel<LiveStreamChannel> datagridModel, String memberId, Date dateFrom, Date dateTo) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be blank for LiveStreamChannelDao.findLiveStreamRecordsPointListing");
        }

        String hql = "SELECT c FROM LiveStreamChannel c "
                + "WHERE c.memberId = ? AND c.status = ? "; //include blocked channel
//                + "WHERE c.memberId = ? AND c.status = ? AND blocked = 0 ";

        List<Object> params = new ArrayList<>();
        params.add(memberId);
        params.add(Global.Status.INACTIVE);

        if (dateFrom != null && dateTo != null) {
            hql += "AND ((startDate BETWEEN ? AND ?) OR (endDate BETWEEN ? AND ?)) ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "c", hql, params.toArray());
    }

    @Override
    public List<LiveStreamChannel> findLiveStreamRecordsForTotalDuration(String memberId, Date dateFrom, Date dateTo) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be blank for LiveStreamChannelDao.findLiveStreamRecordsPointListing");
        }

        String hql = "SELECT c FROM LiveStreamChannel c "
                + "WHERE c.memberId = ? AND c.status = ? "; //include blocked channel
//                + "WHERE c.memberId = ? AND c.status = ? AND blocked = 0 ";

        List<Object> params = new ArrayList<>();
        params.add(memberId);
        params.add(Global.Status.INACTIVE);

        if (dateFrom != null && dateTo != null) {
            hql += "AND ((startDate BETWEEN ? AND ?) OR (endDate BETWEEN ? AND ?)) ";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doUpdateLatestAccumulateView(String channelId, String status) {
        List<Object> params = new ArrayList<>();
        String hql = "update LiveStreamChannel c set c.accumulateView = c.accumulateView+1 "
                + " where c.channelId = ? AND c.status = ? ";

        params.add(channelId);
        params.add(status);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public LiveStreamChannel getLiveStreamChannelByChannelName(String channelName, String status) {
        LiveStreamChannel liveStreamChannel = new LiveStreamChannel(false);
        liveStreamChannel.setChannelName(channelName);
        liveStreamChannel.setStatus(status);

        return findFirst(liveStreamChannel, "startDate desc");
    }

    @Override
    public void updateUserLinkingChannel(String channelId, String uid) {
        if (StringUtils.isBlank(channelId)) {
            throw new ValidatorException("channelId can not be blank for LiveStreamChannelDao.updateUserLinkingChannel");
        }

        List<Object> params = new ArrayList<>();

        String setValue;
        if (StringUtils.isNotBlank(uid)) {
            setValue = "SET c.linkUserAgoraUid = ?, c.datetimeUpdate = ? ";
            params.add(uid);
        } else {
            setValue = "SET c.linkUserAgoraUid = null, c.datetimeUpdate = ? ";
        }

        String hql = "UPDATE LiveStreamChannel c " + setValue
                + " WHERE c.channelId = ? AND c.status = ? ";

        params.add(new Date());
        params.add(channelId);
        params.add(Global.Status.ACTIVE);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateVjConnectChannel(String channelId, boolean hasVjConnect) {
        if (StringUtils.isBlank(channelId)) {
            throw new ValidatorException("channelId can not be blank for LiveStreamChannelDao.updateVjConnectChannel");
        }

        List<Object> params = new ArrayList<>();
        String hql = "UPDATE LiveStreamChannel c SET c.hasVjConnect = ?, c.datetimeUpdate = ? "
                + " WHERE c.channelId = ? AND c.status = ? ";

        params.add(hasVjConnect);
        params.add(new Date());
        params.add(channelId);
        params.add(Global.Status.ACTIVE);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateChannelDisconnected(String channelName, int idleCount, Date endDate, String status) {
        if (StringUtils.isBlank(channelName)) {
            throw new ValidatorException("channelName can not be blank for LiveStreamChannelDao.updateChannelDisconnected");
        }

        List<Object> params = new ArrayList<>();

        String hql = "UPDATE LiveStreamChannel SET idleCount = ?, status = ?, endDate = ?, datetimeUpdate = ? "
                + " WHERE channelName = ? AND status = ?";

        params.add(idleCount);
        params.add(status);
        params.add(endDate);
        params.add(new Date());
        params.add(channelName);
        params.add(Global.Status.ACTIVE);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doUpdateChannelClose(String channelId, String status, Date endDate, int totalView) {
        if (StringUtils.isBlank(channelId)) {
            throw new ValidatorException("channelId can not be blank for LiveStreamChannelDao.doUpdateChannelClose");
        }

        List<Object> params = new ArrayList<>();
        String hql = "UPDATE LiveStreamChannel SET status = ?, endDate = ?, totalView = ?, datetimeUpdate = ? "
                + "WHERE channelId = ?";

        params.add(status);
        params.add(endDate);
        params.add(totalView);
        params.add(new Date());
        params.add(channelId);

        bulkUpdate(hql, params.toArray());
    }
}
