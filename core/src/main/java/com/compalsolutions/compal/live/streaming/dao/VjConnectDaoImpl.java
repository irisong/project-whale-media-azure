package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.live.streaming.vo.VjConnect;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(VjConnectDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjConnectDaoImpl extends Jpa2Dao<VjConnect, String> implements VjConnectDao {

    public VjConnectDaoImpl() {
        super(new VjConnect(false));
    }

    @Override
    public VjConnect findVjConnectByMemberId(String memberId) {
        String hql = "FROM c IN " + VjConnect.class + " WHERE c.status = ? AND (c.hostMemberId = ? OR c.connectorMemberId = ?) ORDER BY startDatetime DESC";
        List<Object> params = new ArrayList<>();
        params.add(VjConnect.STATUS_ACTIVE);
        params.add(memberId);
        params.add(memberId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void updateVjConnectStatus(String connectId, String status, Date endDatetime) {
        if (StringUtils.isBlank(connectId)) {
            throw new ValidatorException("connectId can not be blank for VjConnectDao.updateVjConnectStatus");
        }
        if (StringUtils.isBlank(status)) {
            throw new ValidatorException("status can not be blank for VjConnectDao.updateVjConnectStatus");
        }

        List<Object> params = new ArrayList<>();
        String setValue = "";
        if (endDatetime != null) {
            setValue = "c.endDatetime = ?, ";
            params.add(endDatetime);
        }

        String hql = "UPDATE VjConnect c SET " + setValue + "c.status = ?, c.datetimeUpdate = ? "
                + " WHERE c.connectId = ? ";

        params.add(status);
        params.add(new Date());
        params.add(connectId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateVjConnectTotalPoint(BigDecimal point, String connectId, boolean isHost) {
        List<Object> params = new ArrayList<>();
        String hql = "UPDATE VjConnect a set ";

        if(isHost) {
            hql += "a.hostTotalPoint = a.hostTotalPoint + ? ";
        } else {
            hql += "a.connectorTotalPoint = a.connectorTotalPoint + ? ";
        }
        hql += "WHERE a.status = ? AND a.connectId = ? ";

        params.add(point);
        params.add(VjConnect.STATUS_ACTIVE);
        params.add(connectId);

        bulkUpdate(hql, params.toArray());
    }
}
