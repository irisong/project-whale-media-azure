package com.compalsolutions.compal.live.streaming.dto;

import java.math.BigDecimal;
import java.util.List;

public class LiveStreamDto {
    private String channelId;
    private BigDecimal point;
    private Long view;
    private Integer fans;
    private Integer follower;
    private List<LiveStreamActiveUserDto> memberList;
    private LiveStreamBroadCasterDto broadCaster;

    public LiveStreamDto() {
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public Long getView() {
        return view;
    }

    public void setView(Long view) {
        this.view = view;
    }

    public Integer getFans() {
        return fans;
    }

    public void setFans(Integer fans) {
        this.fans = fans;
    }

    public Integer getFollower() {
        return follower;
    }

    public void setFollower(Integer follower) {
        this.follower = follower;
    }

    public List<LiveStreamActiveUserDto> getMemberList() {
        return memberList;
    }

    public void setMemberList(List<LiveStreamActiveUserDto> memberList) {
        this.memberList = memberList;
    }

    public LiveStreamBroadCasterDto getBroadCaster() {
        return broadCaster;
    }

    public void setBroadCaster(LiveStreamBroadCasterDto broadCaster) {
        this.broadCaster = broadCaster;
    }
}
