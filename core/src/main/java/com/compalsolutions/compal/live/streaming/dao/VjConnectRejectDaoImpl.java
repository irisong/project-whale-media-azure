package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.live.streaming.vo.VjConnectReject;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(VjConnectRejectDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjConnectRejectDaoImpl extends Jpa2Dao<VjConnectReject, String> implements VjConnectRejectDao {

    public VjConnectRejectDaoImpl() {
        super(new VjConnectReject(false));
    }

}
