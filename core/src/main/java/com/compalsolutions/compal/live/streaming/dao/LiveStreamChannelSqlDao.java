package com.compalsolutions.compal.live.streaming.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

public interface LiveStreamChannelSqlDao {
    public static final String BEAN_NAME = "liveStreamChannelSqlDao";

    public void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String memberId, List<String> followingIds);

    public void findLiveStreamChannelForDatagrid(DatagridModel<LiveStreamChannel> datagridModel, String userType, String memberCode, Date dateFrom, Date dateTo);

    public int getNoOfLiveStreamChannel();

    public int getNoOfDayForLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo);

    public LinkedHashMap getDatesOfLiveStreamChannel(String ownerId, Date dateFrom, Date dateTo);

    public long getTotalStreamDuration(String memberId, Date dateFrom, Date dateTo);

    public void findLiveStreamListing(DatagridModel<LiveStreamChannel> datagridModel, String category);

    List<LiveStreamChannel> findLiveStreamChannelByMemberCodeAndUserType(String memberCode, String userType, Date dateFrom, Date dateTo);

    public void updateChannelActive(String channelName, Date startDate);

    public void findLiveStreamConnectListing(DatagridModel<LiveStreamChannel> datagridModel, String memberId, String searchContent);
}
