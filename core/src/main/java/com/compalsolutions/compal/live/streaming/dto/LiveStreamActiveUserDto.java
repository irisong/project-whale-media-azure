package com.compalsolutions.compal.live.streaming.dto;

import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.List;

public class LiveStreamActiveUserDto {
    private String memberId;
    private String memberProfileUrl;


    public LiveStreamActiveUserDto() {
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberProfileUrl() {
        return memberProfileUrl;
    }

    public void setMemberProfileUrl(String memberProfileUrl) {
        this.memberProfileUrl = memberProfileUrl;
    }
}
