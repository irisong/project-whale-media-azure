package com.compalsolutions.compal.live.chatroom.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_live_chat_room_log")
@Access(AccessType.FIELD)
public class LiveChatRoomLog extends VoBase {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_CREATE_CHATROOM = "CREATE_CHATROOM";
    public static final String ACTION_GET_CHATROOM = "GET_CHATROOM";
    public static final String ACTION_SEND_MSG_TO_CHATROOM = "SEND_MESSAGE_TO_CHATROOM";
    public static final String ACTION_SET_FANS_ROLE = "SET_FANS_ROLE";
    public static final String ACTION_MUTE_AUDIENCE = "MUTE_AUDIENCE";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @ToUpperCase
    @ToTrim
    @Column(name = "action_type", nullable = false, length = 50)
    private String actionType;

    @ToTrim
    @Column(name = "req_body", columnDefinition = Global.ColumnDef.TEXT)
    private String requestBody;

    @ToTrim
    @Column(name = "resp_body", columnDefinition = Global.ColumnDef.TEXT)
    private String responseBody;

    @ToUpperCase
    @ToTrim
    @Column(name = "access_module", length = 50)
    private String accessModule;

    @ToTrim
    @Column(name = "status", length = 32)
    private String status;


    public LiveChatRoomLog() {
    }

    public LiveChatRoomLog(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.COMPLETED;
        }
    }


    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccessModule() {
        return accessModule;
    }

    public void setAccessModule(String accessModule) {
        this.accessModule = accessModule;
    }
}
