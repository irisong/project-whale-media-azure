package com.compalsolutions.compal.live.streaming.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OptimisticLockType;
import org.hibernate.annotations.OptimisticLocking;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "app_live_stream_audience")
@Access(AccessType.FIELD)
public class LiveStreamAudience extends VoBase {

    private static final long serialVersionUID = 1L;

    public static final String ENTER_CHANNEL = "START";
    public static final String EXIT_CHANNEL = "END";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "ref_no", length = 32, nullable = false)
    private String refNo;

    @ToTrim
    @Column(name = "channel_id", nullable = false, length = 32)
    private String channelId;

    @ToTrim
    @Column(name = "member_id", length = 32)
    private String memberId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "duration")
    private long duration;

    public LiveStreamAudience() {
    }

    public LiveStreamAudience(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "LiveStreamAudience{" +
                "id='" + id + '\'' +
                ", refNo='" + refNo + '\'' +
                ", channelId='" + channelId + '\'' +
                ", memberId='" + memberId + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", duration=" + duration +
                '}';
    }
}
