package com.compalsolutions.compal.live.chatroom.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.live.chatroom.client.LiveChatRoomClient;
import com.compalsolutions.compal.live.chatroom.client.dto.ChatRoomDto;
import com.compalsolutions.compal.live.chatroom.client.dto.LiveChatResponse;
import com.compalsolutions.compal.live.chatroom.dao.LiveChatRoomDao;
import com.compalsolutions.compal.live.chatroom.dao.LiveChatRoomLogDao;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoomLog;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberFansService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberFans;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component(LiveChatService.BEAN_NAME)
public class LiveChatServiceImpl implements LiveChatService {

    private static final Object synchronizedObject = new Object();

    @Autowired
    private LiveChatRoomLogDao liveChatRoomLogDao;

    @Autowired
    private LiveChatRoomDao liveChatRoomDao;

    @Override
    public LiveChatRoomLog doCreateLiveChatLog(String accessModule, String actionType, String requestBody) {
        LiveChatRoomLog liveChatRoomLog = new LiveChatRoomLog(true);
        liveChatRoomLog.setAccessModule(accessModule);
        liveChatRoomLog.setActionType(actionType);
        liveChatRoomLog.setRequestBody(requestBody);
        liveChatRoomLogDao.save(liveChatRoomLog);
        return liveChatRoomLog;
    }

    @Override
    public void doDeleteLiveChat(String channelId) {
        LiveChatRoom liveChatRoom = findChatRoomByChannelId(channelId);
        if (liveChatRoom != null) {
            liveChatRoomDao.delete(liveChatRoom);
        }
    }

    @Override
    public void doUpdateLiveChatRoomLog(LiveChatRoomLog liveChatRoomLog) {
        liveChatRoomLogDao.update(liveChatRoomLog);
    }

    @Override
    public LiveChatRoom doCreateLiveChatroom(String channelId, ChatRoomDto chatRoomDto) {
        LiveChatRoom liveChatRoom = new LiveChatRoom(true);
        liveChatRoom.setAnnouncement(chatRoomDto.getAnnouncement());
        liveChatRoom.setBroadcastUrl(chatRoomDto.getBroadcasturl());
        liveChatRoom.setCreator(chatRoomDto.getCreator());
        liveChatRoom.setRoomId(chatRoomDto.getRoomid());
        liveChatRoom.setName(chatRoomDto.getName());
        liveChatRoom.setChannelId(channelId);
        liveChatRoom.setValid(chatRoomDto.isValid());
        liveChatRoomDao.save(liveChatRoom);
        return liveChatRoom;
    }

    @Override
    public void doUpdateLiveChatRoom(LiveChatRoom liveChatRoom) {
        liveChatRoomDao.update(liveChatRoom);
    }

    @Override
    public LiveChatRoom findChatRoomByChannelId(String channelId) {
        return liveChatRoomDao.findLiveChatRoom(channelId);
    }

    @Override
    public LiveChatRoom findChatRoomByCreator(String creator) {
        return liveChatRoomDao.findLiveChatRoomByCreator(creator);
    }

    @Override
    public void doProcessMuteUser(Locale locale, String channelId, boolean action, String userMemberId, String targetMemberId) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.class);

        /************************
         * VERIFICATION - START *
         ************************/
        boolean isAdmin;
        boolean isSeniorAdmin;

        LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannel(channelId, null, false);
        if (liveStreamChannel != null) {
            isAdmin = memberFansService.isMemberFans(userMemberId, liveStreamChannel.getMemberId(), MemberFans.ROLE_ADMIN);
            isSeniorAdmin = memberFansService.isMemberFans(userMemberId, liveStreamChannel.getMemberId(), MemberFans.ROLE_SENIOR_ADMIN);
        } else {
            throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
        }

        if (!isAdmin && !isSeniorAdmin && !(userMemberId.equals(liveStreamChannel.getMemberId()))) {
            if (action) {
                throw new ValidatorException(i18n.getText("noRightToMute", locale));
            } else {
                throw new ValidatorException(i18n.getText("noRightToUnmute", locale));
            }
        }

        if (StringUtils.isBlank(targetMemberId)) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        Member member = memberService.getMember(targetMemberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }
        /**********************
         * VERIFICATION - END *
         **********************/

        synchronized (synchronizedObject) {
            LiveChatRoom liveChatRoom = findChatRoomByChannelId(channelId);

            if (action) {
                String muteList = liveChatRoom.getMuteList();
                if (!muteList.contains(targetMemberId)) {
                    if (StringUtils.isNotBlank(muteList))
                        muteList += "|";

                    muteList += targetMemberId;
                }

                liveChatRoom.setMuteList(muteList);
                liveChatRoomDao.update(liveChatRoom);
            } else { //unmute
                String newMuteList = "";
                String[] muteList = StringUtils.split(liveChatRoom.getMuteList(), "|");
                for (String muteMemberId : muteList) {
                    if (!muteMemberId.equals(targetMemberId)) {
                        if (StringUtils.isNotBlank(newMuteList))
                            newMuteList += "|";

                        newMuteList += muteMemberId;
                    }
                }

                liveChatRoom.setMuteList(newMuteList);
                liveChatRoomDao.update(liveChatRoom);
                muteList = null;
            }
        }
    }

    @Override
    public boolean isMemberMuted(Locale locale, String channelId, String memberId) {
        LiveChatRoom liveChatRoom = findChatRoomByChannelId(channelId);
        if (liveChatRoom != null) {
            if (StringUtils.isNotBlank(liveChatRoom.getMuteList())) {
                if (liveChatRoom.getMuteList().indexOf(memberId) >= 0) {
                    return true;
                }
            }
        }

        return false;
    }

    @Override
    public void doRemoveMuteUser(String channelId, String memberId) {
        LiveChatRoom liveChatRoom = findChatRoomByChannelId(channelId);

        if (liveChatRoom != null) {
            String[] muteList = StringUtils.split(liveChatRoom.getMuteList(), "|");
            if (muteList != null) {
                String newMuteList = "";
                for (String muteMemberId : muteList) {
                    if (!muteMemberId.equals(memberId)) {
                        if (StringUtils.isNotBlank(newMuteList))
                            newMuteList += "|";

                        newMuteList += muteMemberId;
                    }
                }
                liveChatRoom.setMuteList(newMuteList);
                liveChatRoomDao.update(liveChatRoom);
            }
        }
    }
}
