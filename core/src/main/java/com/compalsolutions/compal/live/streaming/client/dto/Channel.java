package com.compalsolutions.compal.live.streaming.client.dto;

import com.compalsolutions.compal.live.streaming.dto.VjConnectDto;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.math.BigDecimal;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Channel {
    private String channelId;
    private String channelName;
    private String agoraToken;
    private Integer vjAgoraUid;
    private Integer userAgoraUid;
    private String memberId;
    private String avatarUrl;
    private String coverUrl;
    private String profileName;
    private String whaleLiveId;
    private String category;
    private String localizeCategory;
    private String title;
    private boolean privateLive;
    private int status;
    private String msg;
    private String roomId;
    private int accumulateView;
    private BigDecimal totalPoint;
    private boolean isFollowing;
    private String fansBadgeName;
    private String liveUrl;
    private String action;
    private List<VjConnectDto> vjConnectInfo;

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getAgoraToken() {
        return agoraToken;
    }

    public void setAgoraToken(String agoraToken) {
        this.agoraToken = agoraToken;
    }

    public Integer getVjAgoraUid() {
        return vjAgoraUid;
    }

    public void setVjAgoraUid(Integer vjAgoraUid) {
        this.vjAgoraUid = vjAgoraUid;
    }

    public Integer getUserAgoraUid() {
        return userAgoraUid;
    }

    public void setUserAgoraUid(Integer userAgoraUid) {
        this.userAgoraUid = userAgoraUid;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLocalizeCategory() {
        return localizeCategory;
    }

    public void setLocalizeCategory(String localizeCategory) {
        this.localizeCategory = localizeCategory;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPrivateLive() {
        return privateLive;
    }

    public void setPrivateLive(boolean privateLive) {
        this.privateLive = privateLive;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId;
    }

    public int getAccumulateView() {
        return accumulateView;
    }

    public void setAccumulateView(int accumulateView) {
        this.accumulateView = accumulateView;
    }

    public BigDecimal getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(BigDecimal totalPoint) {
        this.totalPoint = totalPoint;
    }

    public boolean isFollowing() {
        return isFollowing;
    }

    public void setFollowing(boolean following) {
        isFollowing = following;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }

    public String getLiveUrl() {
        return liveUrl;
    }

    public void setLiveUrl(String liveUrl) {
        this.liveUrl = liveUrl;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public List<VjConnectDto> getVjConnectInfo() {
        return vjConnectInfo;
    }

    public void setVjConnectInfo(List<VjConnectDto> vjConnectInfo) {
        this.vjConnectInfo = vjConnectInfo;
    }
}
