package com.compalsolutions.compal;

import java.util.*;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import org.apache.commons.logging.Log;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.MemberUserType;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.general.vo.PaymentInfo;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

public class WebUtil {
    private static SystemConfig systemConfig;
    private static List<Integer> walletTypes;
    private static Map<String, WalletTypeConfig> walletTypeConfigMap;
    private static Map<Locale, Map<String, String>> statusDescMap;
    private static List<Language> languages;

    public static List<Language> getLanguages() {
        if (CollectionUtil.isEmpty(languages)) {
            languages = Application.lookupBean(LanguageFrameworkService.class).findLanguages();
        }

        return languages;
    }

    public static String getLoginMemberId(LoginInfo loginInfo) {
        Member member = getLoginMember(loginInfo);
        if (member != null)
            return member.getMemberId();
        return null;
    }

    public static Member getLoginMember(LoginInfo loginInfo) {
        if (loginInfo == null)
            return null;

        Member member = null;

        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;
        if (isAdmin) {
            member = ((MrmAdminUserType) loginInfo.getUserType()).getFrontEndMember();

            if (member == null)
                throw new ValidatorException("You no select any member to login into front end");
            return member;
        } else {
            return ((MemberUser) loginInfo.getUser()).getMember();
        }
    }

    public static boolean isAdmin(LoginInfo loginInfo) {
        return loginInfo.getUserType() instanceof AdminUserType;
    }

    public static boolean isMember(LoginInfo loginInfo) {
        return loginInfo.getUserType() instanceof MemberUserType;
    }

    public static boolean isAgent(LoginInfo loginInfo) {
        return loginInfo.getUserType() instanceof AgentUserType;
    }

    public static boolean isMemberSecondPasswordValid(Locale locale, LoginInfo loginInfo, String transactionPassword) {
        String memberId = getLoginMemberId(loginInfo);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        return memberService.isMemberSecondPasswordValid(locale, memberId, transactionPassword);
    }

    public static void checkMemberSecondPassword(Locale locale, LoginInfo loginInfo, String transactionPassword) {
        if (!isMemberSecondPasswordValid(locale, loginInfo, transactionPassword)) {
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            throw new ValidatorException(i18n.getText("security_password_is_not_valid", locale));
        }
    }

    public static String getWalletName(Locale locale, int walletType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        return i18n.getText("WALLET_" + walletType, locale);
    }

    public static String getWalletCurrency(Locale locale, int walletType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        return i18n.getText("WALLET_CUR_" + walletType, locale);
    }

    public static String getWalletCurrencyWithName(Locale locale, int walletType) {
        return getWalletCurrency(locale, walletType) + " - " + getWalletName(locale, walletType);
    }

    public static String getWalletNameWithCurrency(Locale locale, int walletType) {
        return getWalletName(locale, walletType) + " (" + getWalletCurrency(locale, walletType) + ")";
    }

    public static SystemConfig getSystemConfig() {
        if (systemConfig == null) {
            SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
            systemConfig = systemConfigService.getDefaultSystemConfig();
            if (systemConfig == null) {
                systemConfig = new SystemConfig(true);
            }
        }
        return systemConfig;
    }

    public static void setSystemConfig(SystemConfig systemConfig) {
        WebUtil.systemConfig = systemConfig;
    }

    public static void clearWalletTypes() {
        walletTypes = null;
        walletTypeConfigMap = null;
    }

    public static List<Integer> getActiveMemberWalletTypes() {
        if (walletTypes == null) {
            WalletService walletService = Application.lookupBean(WalletService.class);
            walletTypes = walletService.findActiveMemberWalletTypes();
        }

        return walletTypes;
    }

    public static WalletTypeConfig getActiveWalletTypeConfig(String ownerType, String cryptoType) {
        if (walletTypeConfigMap == null) {
            walletTypeConfigMap = new HashMap<>();
        }

        String key = ownerType + "-" + cryptoType;
        if (walletTypeConfigMap.containsKey(key)) {
            return walletTypeConfigMap.get(key);
        } else {
            WalletService walletService = Application.lookupBean(WalletService.class);

            WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, cryptoType);
            if (walletTypeConfig == null) {
                throw new ValidatorException("Invalid walletTypeConfig [ownerType, cryptoType] = [" + ownerType + ", " + cryptoType + "]");
            }
            walletTypeConfigMap.put(key, walletTypeConfig);
            return walletTypeConfig;
        }
    }

    public static Integer getDefaultMemberWalletType() {
        List<Integer> walletTypes = getActiveMemberWalletTypes();

        if (CollectionUtil.isNotEmpty(walletTypes)) {
            return walletTypes.get(0);
        } else {
            return null;
        }
    }

    public static String getStatusDesc(Locale locale, String status) {
        if (statusDescMap == null) {
            statusDescMap = new HashMap<>();
        }

        Map<String, String> statusMap = null;

        if (statusDescMap.containsKey(locale)) {
            statusMap = statusDescMap.get(locale);
        } else {
            statusMap = new HashMap<>();
            statusDescMap.put(locale, statusMap);

            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

            String[][] statusKeys = Global.Status.getAllStatusWithI18nKey();

            for (String[] statusKey : statusKeys) {
                statusMap.put(statusKey[0], i18n.getText(statusKey[1], locale));
            }
        }

        if (statusMap.containsKey(status))
            return statusMap.get(status);

        return status;
    }

    public static String getPaymentInfoLabel(Locale locale, PaymentInfo paymentInfo) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        if (locale == null) {
            LanguageFrameworkService languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
            locale = languageFrameworkService.getSystemLocale();
        }

        String label = null;
        switch (paymentInfo.getPaymentMethod()) {
        case Global.PaymentMethod.WALLET:
            label = getWalletName(locale, paymentInfo.getWalletType());
            break;
        case Global.PaymentMethod.CREDIT_DEBIT_CARD:
            label = i18n.getText("creditDebitCard", locale);
            break;
        case Global.PaymentMethod.CRYPTOCURRENCY:
            if (Global.CryptocurrencyPaymentType.BITCOIN.equalsIgnoreCase(paymentInfo.getCrytocurrencyType())) {
                label = i18n.getText("bitcoin", locale);
            } else {
                label = paymentInfo.getCrytocurrencyType();
            }
            break;
        default:
            label = paymentInfo.getPaymentMethod();
        }

        return label;
    }

    public static String getFullAddress(Locale locale, Address address, String lineSeparator) {
        if (locale == null) {
            LanguageFrameworkService languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
            locale = languageFrameworkService.getSystemLocale();
        }

        return address.getFullAddress(locale, lineSeparator);
    }

    public static String getInternationalFullAddress(Locale locale, Address address, String lineSeparator) {
        if (locale == null) {
            LanguageFrameworkService languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
            locale = languageFrameworkService.getSystemLocale();
        }

        return address.getInternationalFullAddress(locale, lineSeparator);
    }

    public static String getSystemConfigImageType(String imageType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        if (Global.SystemConfigImageType.ADMIN_DASHBOARD.equalsIgnoreCase(imageType)) {
            return i18n.getText("adminDashboard");
        } else if (Global.SystemConfigImageType.STORE_FRONT.equalsIgnoreCase(imageType)) {
            return i18n.getText("storeFront");
        } else if (Global.SystemConfigImageType.EMAIL_TEMPLATE.equalsIgnoreCase(imageType)) {
            return i18n.getText("emailTemplate");
        }
        return i18n.getText("Unknown");
    }

    public static String getCountryNameByCountryCode(Locale locale, String countryCode) {
//        CountryService countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
//        CountryDesc countryDesc = countryService.findCountryDescByLocaleAndCountryCode(locale, countryCode);
        return "";
    }

    public static void logMessage(RunTaskLogger logger, Log log, String msg) {
        if(logger != null)
            logger.log(SysFormatter.formatDateTime(new Date()) + " - " + msg);
        log.debug(msg);
    }
}
