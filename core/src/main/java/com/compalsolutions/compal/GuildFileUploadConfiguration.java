package com.compalsolutions.compal;

public class GuildFileUploadConfiguration {

    public static final String BEAN_NAME = "guildFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;

    public static final String FOLDER_NAME = "guild";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getGuildFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullGuildParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }

    public String getFullLocalGuildParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }
}
