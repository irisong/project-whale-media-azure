package com.compalsolutions.compal.payment.dto;

public class TopUpWalletTypeInfoDto {
    private String option;
    private String rateDesc;
    private String topUpOption;
    private Double whaleCoinAmt;
    private Double fromWalletAmount;
    private String promoteDesc;
    private String walletAmountDesc;

    public String getOption() {
        return option;
    }

    public void setOption(String option) {
        this.option = option;
    }

    public String getTopUpOption() {
        return topUpOption;
    }

    public void setTopUpOption(String topUpOption) {
        this.topUpOption = topUpOption;
    }

    public Double getWhaleCoinAmt() {
        return whaleCoinAmt;
    }

    public void setWhaleCoinAmt(Double whaleCoinAmt) {
        this.whaleCoinAmt = whaleCoinAmt;
    }

    public String getRateDesc() {
        return rateDesc;
    }

    public void setRateDesc(String rateDesc) {
        this.rateDesc = rateDesc;
    }

    public Double getFromWalletAmount() {
        return fromWalletAmount;
    }

    public void setFromWalletAmount(Double fromWalletAmount) {
        this.fromWalletAmount = fromWalletAmount;
    }

    public String getPromoteDesc() {
        return promoteDesc;
    }

    public void setPromoteDesc(String promoteDesc) {
        this.promoteDesc = promoteDesc;
    }

    public String getWalletAmountDesc() {
        return walletAmountDesc;
    }

    public void setWalletAmountDesc(String walletAmountDesc) {
        this.walletAmountDesc = walletAmountDesc;
    }
}
