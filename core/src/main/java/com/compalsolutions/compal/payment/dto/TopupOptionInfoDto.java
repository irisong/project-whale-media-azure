package com.compalsolutions.compal.payment.dto;

import java.util.List;

public class TopupOptionInfoDto{
    private String paymentOption;
    private String paymentOptionName;
    private String iconUrl;
    private List<TopUpWalletTypeInfoDto> topUpWalletTypeInfoDtoList;

    public String getPaymentOption() {
        return paymentOption;
    }

    public void setPaymentOption(String paymentOption) {
        this.paymentOption = paymentOption;
    }

    public String getPaymentOptionName() {
        return paymentOptionName;
    }

    public void setPaymentOptionName(String paymentOptionName) {
        this.paymentOptionName = paymentOptionName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<TopUpWalletTypeInfoDto> getTopupWalletTypeInfoDtoList() {
        return topUpWalletTypeInfoDtoList;
    }

    public void setTopupWalletTypeInfoDtoList(List<TopUpWalletTypeInfoDto> topUpWalletTypeInfoDtoList) {
        this.topUpWalletTypeInfoDtoList = topUpWalletTypeInfoDtoList;
    }
}
