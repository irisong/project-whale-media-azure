package com.compalsolutions.compal.thirdparty.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_third_party")
public class ThirdParty extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "third_party_id", unique = true, nullable = false, length = 32)
    private String thirdPartyId;

    @ToUpperCase
    @ToTrim
    @Column(name = "service_name", length = 50, nullable = false)
    private String serviceName;

    @ToUpperCase
    @ToTrim
    @Column(name = "service_code", length = 50, nullable = false)
    private String serviceCode;

    @ToTrim
    @Column(name = "service_ip", length = 50, nullable = false)
    private String serviceIp;

    @ToTrim
    @Column(name = "service_url", length = 250, nullable = false)
    private String serviceUrl;

    @ToTrim
    @Column(name = "hash_seed", length = 100, nullable = false)
    private String hashSeed;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public ThirdParty() {
    }

    public ThirdParty(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(String serviceId) {
        this.thirdPartyId = serviceId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getServiceIp() {
        return serviceIp;
    }

    public void setServiceIp(String serviceIp) {
        this.serviceIp = serviceIp;
    }

    public String getServiceUrl() {
        return serviceUrl;
    }

    public void setServiceUrl(String serviceUrl) {
        this.serviceUrl = serviceUrl;
    }

    public String getHashSeed() {
        return hashSeed;
    }

    public void setHashSeed(String hashSeed) {
        this.hashSeed = hashSeed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ThirdParty that = (ThirdParty) o;

        return thirdPartyId != null ? thirdPartyId.equals(that.thirdPartyId) : that.thirdPartyId == null;
    }

    @Override
    public int hashCode() {
        return thirdPartyId != null ? thirdPartyId.hashCode() : 0;
    }
}
