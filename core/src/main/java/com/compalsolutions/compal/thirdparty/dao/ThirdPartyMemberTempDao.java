package com.compalsolutions.compal.thirdparty.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMemberTemp;

public interface ThirdPartyMemberTempDao extends BasicDao<ThirdPartyMemberTemp, String> {
    public static final String BEAN_NAME = "thirdPartyMemberTempDao";

    public ThirdPartyMemberTemp findByLoginKey(String loginKey);

    public ThirdPartyMemberTemp findByLoginKeyAndServiceCode(String loginKey, String serviceCode);
}
