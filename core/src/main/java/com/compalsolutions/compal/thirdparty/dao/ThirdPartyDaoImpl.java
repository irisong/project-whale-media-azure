package com.compalsolutions.compal.thirdparty.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.thirdparty.vo.ThirdParty;

@Component(ThirdPartyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ThirdPartyDaoImpl extends Jpa2Dao<ThirdParty, String> implements ThirdPartyDao {
    public ThirdPartyDaoImpl() {
        super(new ThirdParty(false));
    }

    @Override
    public ThirdParty findActiveByServiceCode(String serviceCode) {
        if (StringUtils.isBlank(serviceCode)) {
            throw new ValidatorException("serviceCode is null for ThirdPartyDao.findActiveByServiceCode");
        }

        ThirdParty example = new ThirdParty(false);
        example.setServiceCode(serviceCode);
        example.setStatus(Global.Status.ACTIVE);

        return findUnique(example);
    }
}
