package com.compalsolutions.compal.thirdparty.service;

import java.math.BigDecimal;
import java.util.Locale;

import org.apache.commons.lang3.tuple.Pair;

import com.compalsolutions.compal.thirdparty.vo.ThirdParty;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMember;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMemberTemp;

public interface ThirdPartyService {
    public static final String BEAN_NAME = "thirdPartyService";

    public ThirdParty findActiveThirdPartyByServiceCode(String serviceCode);

    public void saveThirdParty(ThirdParty thirdParty);

    public boolean isThirdPartyAccessGranted(String serviceCode, String clientIpAddress);

    public String doGenerateLoginKey(Locale locale, String serviceCode);

    public ThirdPartyMemberTemp findThirdPartyMemberTempByLoginKey(String loginKey);

    public ThirdPartyMemberTemp findThirdPartyMemberTempByLoginKeyAndServiceCode(String loginKey, String serviceCode);

    public void doProcessLoginSuccess(Locale locale, ThirdPartyMemberTemp thirdPartyMemberTemp, String memberId);

    public ThirdPartyMember findActiveThirdPartyMemberByThirdPartyIdAndThirdPartyUsername(String thirdPartyId, String thirdPartyUsername);
}
