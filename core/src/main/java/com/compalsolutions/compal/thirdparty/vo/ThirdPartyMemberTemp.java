package com.compalsolutions.compal.thirdparty.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;

@Entity
@Table(name = "app_third_party_member_temp")
@Access(AccessType.FIELD)
public class ThirdPartyMemberTemp extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_temp_id", unique = true, nullable = false, length = 32)
    private String memberTempId;

    @ToTrim
    @Column(name = "third_party_id", nullable = false, length = 32)
    private String thirdPartyId;

    @ManyToOne
    @JoinColumn(name = "third_party_id", insertable = false, updatable = false)
    private ThirdParty thirdParty;

    @ToTrim
    @Column(name = "member_id", length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @ToTrim
    @Column(name = "third_party_username", length = 100)
    private String thirdPartyUsername;

    @ToTrim
    @Column(name = "login_key", nullable = false, length = 100)
    private String loginKey;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @ToTrim
    @Column(name = "third_party_member_id", length = 32)
    private String thirdPartyMemberId;

    @ManyToOne
    @JoinColumn(name = "third_party_member_id", insertable = false, updatable = false)
    private ThirdPartyMember thirdPartyMember;

    public ThirdPartyMemberTemp() {
    }

    public ThirdPartyMemberTemp(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getMemberTempId() {
        return memberTempId;
    }

    public void setMemberTempId(String memberTempId) {
        this.memberTempId = memberTempId;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(String thirdPartyId) {
        this.thirdPartyId = thirdPartyId;
    }

    public ThirdParty getThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(ThirdParty thirdParty) {
        this.thirdParty = thirdParty;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getLoginKey() {
        return loginKey;
    }

    public void setLoginKey(String loginKey) {
        this.loginKey = loginKey;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getThirdPartyUsername() {
        return thirdPartyUsername;
    }

    public void setThirdPartyUsername(String thirdPartyUsername) {
        this.thirdPartyUsername = thirdPartyUsername;
    }

    public String getThirdPartyMemberId() {
        return thirdPartyMemberId;
    }

    public void setThirdPartyMemberId(String thirdPartyMemberId) {
        this.thirdPartyMemberId = thirdPartyMemberId;
    }

    public ThirdPartyMember getThirdPartyMember() {
        return thirdPartyMember;
    }

    public void setThirdPartyMember(ThirdPartyMember thirdPartyMember) {
        this.thirdPartyMember = thirdPartyMember;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ThirdPartyMemberTemp that = (ThirdPartyMemberTemp) o;

        return memberTempId != null ? memberTempId.equals(that.memberTempId) : that.memberTempId == null;
    }

    @Override
    public int hashCode() {
        return memberTempId != null ? memberTempId.hashCode() : 0;
    }
}
