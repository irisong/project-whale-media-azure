package com.compalsolutions.compal.thirdparty.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.thirdparty.vo.ThirdParty;

public interface ThirdPartyDao extends BasicDao<ThirdParty, String> {
    public static final String BEAN_NAME = "thirdPartyDao";

    public ThirdParty findActiveByServiceCode(String serviceCode);
}
