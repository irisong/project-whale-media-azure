package com.compalsolutions.compal.thirdparty.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_third_party_member")
@Access(AccessType.FIELD)
public class ThirdPartyMember extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "third_party_member_id", unique = true, nullable = false, length = 32)
    private String thirdPartyMemberId;

    @ToTrim
    @Column(name = "third_party_id", nullable = false, length = 32)
    private String thirdPartyId;

    @ManyToOne
    @JoinColumn(name = "third_party_id", insertable = false, updatable = false)
    private ThirdParty thirdParty;

    @ToTrim
    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @ToTrim
    @Column(name = "third_party_username", length = 100)
    private String thirdPartyUsername;

    /**
     * 'PENDING' - thirdparty logged-in or registered success in Exchange, however haven't send Exchange the
     * thirdPartyUsername<br/>
     * 'ACTIVE' - thirdparty has updated thirdPartyUsername
     */
    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public ThirdPartyMember() {
    }

    public ThirdPartyMember(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.PENDING;
        }
    }

    public String getThirdPartyMemberId() {
        return thirdPartyMemberId;
    }

    public void setThirdPartyMemberId(String memberThirdPartyId) {
        this.thirdPartyMemberId = memberThirdPartyId;
    }

    public String getThirdPartyId() {
        return thirdPartyId;
    }

    public void setThirdPartyId(String serviceId) {
        this.thirdPartyId = serviceId;
    }

    public ThirdParty getThirdParty() {
        return thirdParty;
    }

    public void setThirdParty(ThirdParty thirdParty) {
        this.thirdParty = thirdParty;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getThirdPartyUsername() {
        return thirdPartyUsername;
    }

    public void setThirdPartyUsername(String thirdPartyUsername) {
        this.thirdPartyUsername = thirdPartyUsername;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        ThirdPartyMember that = (ThirdPartyMember) o;

        return thirdPartyMemberId != null ? thirdPartyMemberId.equals(that.thirdPartyMemberId) : that.thirdPartyMemberId == null;
    }

    @Override
    public int hashCode() {
        return thirdPartyMemberId != null ? thirdPartyMemberId.hashCode() : 0;
    }
}
