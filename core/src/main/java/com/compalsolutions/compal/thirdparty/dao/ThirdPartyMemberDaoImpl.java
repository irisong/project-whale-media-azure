package com.compalsolutions.compal.thirdparty.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMember;

@Component(ThirdPartyMemberDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ThirdPartyMemberDaoImpl extends Jpa2Dao<ThirdPartyMember, String> implements ThirdPartyMemberDao {
    public ThirdPartyMemberDaoImpl() {
        super(new ThirdPartyMember(false));
    }

    @Override
    public ThirdPartyMember findActiveThirdPartyMemberByThirdPartyIdAndThirdPartyUsername(String thirdPartyId, String thirdPartyUsername) {
        if (StringUtils.isBlank(thirdPartyId) && StringUtils.isBlank(thirdPartyUsername)) {
            throw new DataException(
                    "thirdPartyId or thirdPartyUsername is blank for ThirdPartyMemberDaoImpl.findActiveThirdPartyMemberByThirdPartyIdAndThirdPartyUsername");
        }

        ThirdPartyMember example = new ThirdPartyMember(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setThirdPartyId(thirdPartyId);
        example.setThirdPartyUsername(thirdPartyUsername);

        return findUnique(example);
    }
}
