package com.compalsolutions.compal.thirdparty.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMemberTemp;

@Component(ThirdPartyMemberTempDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ThirdPartyMemberTempDaoImpl extends Jpa2Dao<ThirdPartyMemberTemp, String> implements ThirdPartyMemberTempDao {
    public ThirdPartyMemberTempDaoImpl() {
        super(new ThirdPartyMemberTemp(false));
    }

    @Override
    public ThirdPartyMemberTemp findByLoginKey(String loginKey) {
        if (StringUtils.isBlank(loginKey))
            throw new ValidatorException("loginKey is blank for ThirdPartyMemberTempDaoImpl.findByLoginKey");

        ThirdPartyMemberTemp example = new ThirdPartyMemberTemp(false);
        example.setLoginKey(loginKey);

        return findUnique(example);
    }

    @Override
    public ThirdPartyMemberTemp findByLoginKeyAndServiceCode(String loginKey, String serviceCode) {
        if (StringUtils.isBlank(loginKey) || StringUtils.isBlank(serviceCode))
            throw new ValidatorException("loginKey or serviceCode is blank for ThirdPartyMemberTempDaoImpl.findByLoginKeyAndServiceCode");

        String hql = "select mt from ThirdPartyMemberTemp as mt where mt.loginKey = ? and mt.thirdParty.serviceCode = ? ";

        return findUnique(hql, loginKey, serviceCode);
    }
}
