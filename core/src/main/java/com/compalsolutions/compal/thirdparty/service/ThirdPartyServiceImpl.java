package com.compalsolutions.compal.thirdparty.service;

import java.util.Date;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.thirdparty.dao.ThirdPartyDao;
import com.compalsolutions.compal.thirdparty.dao.ThirdPartyMemberDao;
import com.compalsolutions.compal.thirdparty.dao.ThirdPartyMemberTempDao;
import com.compalsolutions.compal.thirdparty.vo.ThirdParty;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMember;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMemberTemp;
import com.compalsolutions.compal.util.Pin;

@Component(ThirdPartyService.BEAN_NAME)
public class ThirdPartyServiceImpl implements ThirdPartyService {
    private ThirdPartyDao thirdPartyDao;
    private ThirdPartyMemberDao thirdPartyMemberDao;
    private ThirdPartyMemberTempDao thirdPartyMemberTempDao;

    @Autowired
    public ThirdPartyServiceImpl(ThirdPartyDao thirdPartyDao, ThirdPartyMemberDao thirdPartyMemberDao, ThirdPartyMemberTempDao thirdPartyMemberTempDao) {
        this.thirdPartyDao = thirdPartyDao;
        this.thirdPartyMemberDao = thirdPartyMemberDao;
        this.thirdPartyMemberTempDao = thirdPartyMemberTempDao;
    }

    @Override
    public ThirdParty findActiveThirdPartyByServiceCode(String serviceCode) {
        return thirdPartyDao.findActiveByServiceCode(serviceCode);
    }

    @Override
    public void saveThirdParty(ThirdParty thirdParty) {
        thirdPartyDao.save(thirdParty);
    }

    @Override
    public boolean isThirdPartyAccessGranted(String serviceCode, String clientIpAddress) {
        ThirdParty thirdParty = findActiveThirdPartyByServiceCode(serviceCode);

        if (thirdParty == null)
            return false;

        return clientIpAddress.equalsIgnoreCase(thirdParty.getServiceIp());
    }

    @Override
    public String doGenerateLoginKey(Locale locale, String serviceCode) {
        I18n i18n = Application.lookupBean(I18n.class);

        ThirdParty thirdParty = findActiveThirdPartyByServiceCode(serviceCode);

        if (thirdParty == null)
            throw new ValidatorException(i18n.getText("invalidServiceCode", locale));

        String loginKey = null;
        do {
            loginKey = Pin.getGenerateAlphanumericPinCode(20);
        } while (findThirdPartyMemberTempByLoginKey(loginKey) != null);

        ThirdPartyMemberTemp thirdPartyMemberTemp = new ThirdPartyMemberTemp();
        thirdPartyMemberTemp.setThirdPartyId(thirdParty.getThirdPartyId());
        thirdPartyMemberTemp.setTrxDatetime(new Date());
        thirdPartyMemberTemp.setLoginKey(loginKey);
        thirdPartyMemberTempDao.save(thirdPartyMemberTemp);

        return loginKey;
    }

    @Override
    public ThirdPartyMemberTemp findThirdPartyMemberTempByLoginKey(String loginKey) {
        return thirdPartyMemberTempDao.findByLoginKey(loginKey);
    }

    @Override
    public ThirdPartyMemberTemp findThirdPartyMemberTempByLoginKeyAndServiceCode(String loginKey, String serviceCode) {
        return thirdPartyMemberTempDao.findByLoginKeyAndServiceCode(loginKey, serviceCode);
    }

    @Override
    public void doProcessLoginSuccess(Locale locale, ThirdPartyMemberTemp thirdPartyMemberTemp, String memberId) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);

        Member member = memberService.getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        // throw a general message because somebody may try to hack this system
        if (StringUtils.isNotBlank(thirdPartyMemberTemp.getMemberId())) {
            throw new ValidatorException(i18n.getText("errorMessage.generalErrorMessage", locale));
        }

        thirdPartyMemberTemp.setMemberId(memberId);
        thirdPartyMemberTempDao.update(thirdPartyMemberTemp);

        /*
        ThirdPartyMember thirdPartyMember = new ThirdPartyMember(true);
        thirdPartyMember.setThirdPartyId(thirdPartyMemberTemp.getThirdPartyId());
        thirdPartyMember.setMemberId(memberId);
        thirdPartyMember.setStatus(Global.Status.PENDING);
        thirdPartyMemberDao.save(thirdPartyMember);
        */
    }

    private static final Object synchronizedLinkAccount = new Object();

    public ThirdPartyMember findActiveThirdPartyMemberByThirdPartyIdAndThirdPartyUsername(String thirdPartyId, String thirdPartyUsername) {
        return thirdPartyMemberDao.findActiveThirdPartyMemberByThirdPartyIdAndThirdPartyUsername(thirdPartyId, thirdPartyUsername);
    }
}
