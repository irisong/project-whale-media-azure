package com.compalsolutions.compal.thirdparty.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMember;

public interface ThirdPartyMemberDao extends BasicDao<ThirdPartyMember, String> {
    public static final String BEAN_NAME = "thirdPartyMemberDao";

    public ThirdPartyMember findActiveThirdPartyMemberByThirdPartyIdAndThirdPartyUsername(String thirdPartyId, String thirdPartyUsername);
}
