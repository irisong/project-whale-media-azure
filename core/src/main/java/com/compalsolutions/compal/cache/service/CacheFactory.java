package com.compalsolutions.compal.cache.service;

import java.util.List;

public interface CacheFactory {
    public static final String BEAN_NAME = "cacheFactory";

    public void evictAll();

    public void evictAll(String beanName);

    public List<CacheEvictableService> findCacheServices();
}
