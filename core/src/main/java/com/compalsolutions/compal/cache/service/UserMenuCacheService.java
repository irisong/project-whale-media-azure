package com.compalsolutions.compal.cache.service;

import java.util.List;

import com.compalsolutions.compal.function.user.vo.UserMenu;

public interface UserMenuCacheService extends CacheEvictableService {
    public static final String BEAN_NAME = "userMenuCacheService";

    public List<UserMenu> getUserMenus(String userId);

    public void evictUserMenu(String userId);
}
