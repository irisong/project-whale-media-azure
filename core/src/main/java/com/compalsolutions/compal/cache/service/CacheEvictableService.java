package com.compalsolutions.compal.cache.service;

public interface CacheEvictableService {
    public String getCacheServiceDescription();

    public String getI18nCacheServiceDescription();

    public void evictAll();
}
