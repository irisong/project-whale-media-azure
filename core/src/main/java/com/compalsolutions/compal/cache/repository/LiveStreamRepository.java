package com.compalsolutions.compal.cache.repository;

import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LiveStreamRepository extends CrudRepository<LiveStreamChannel, String> {

}