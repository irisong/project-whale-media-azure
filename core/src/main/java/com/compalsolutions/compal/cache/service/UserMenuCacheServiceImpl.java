package com.compalsolutions.compal.cache.service;

import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.util.CollectionUtil;

@Component(UserMenuCacheService.BEAN_NAME)
public class UserMenuCacheServiceImpl implements UserMenuCacheService {
    @Override
    public String getCacheServiceDescription() {
        return "This service used to cache user menus";
    }

    @Override
    public String getI18nCacheServiceDescription() {
        return null;
    }

    @CacheEvict(value = "userMenusCache", allEntries = true)
    @Override
    public void evictAll() {
    }

    @Cacheable("userMenusCache")
    public List<UserMenu> getUserMenus(String userId) {
        UserService userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);

        List<UserMenu> mainMenus = userService.findAuthorizedUserMenu(userId);
        for (UserMenu menu : mainMenus) {
            List<UserMenu> subMenus = userService.findAuthorizedLevel2Menu(userId, menu.getMenuId());
            if (CollectionUtil.isNotEmpty(subMenus))
                menu.setSubMenus(subMenus);
        }

        return mainMenus;
    }

    @CacheEvict(value = "userMenusCache")
    @Override
    public void evictUserMenu(String userId) {
        // does nothing
    }
}
