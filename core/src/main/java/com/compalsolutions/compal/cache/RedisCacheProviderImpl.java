package com.compalsolutions.compal.cache;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointBalance;
import com.compalsolutions.compal.point.vo.PointSummary;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.redisson.Redisson;
import org.redisson.api.RList;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.client.RedisConnectionException;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Service(RedisCacheProvider.BEAN_NAME)
public class RedisCacheProviderImpl implements RedisCacheProvider {
    private static final Log log = LogFactory.getLog(RedisCacheProviderImpl.class);

    private Boolean redisStatus;

    private Boolean bannerStatus;

    private RList<SymbolicItem> symbolicItems;

    private RMap<String, String> globalData;

    private RedissonClient redissonClient;

    private LiveStreamService liveStreamService;

    private PointService pointService;

    private FansIntimacyService fansIntimacyService;

    private RList<FansIntimacyConfig> fansIntimacyConfigsRList;

    private RMap<String, BigDecimal> pointSummaryRMap;

    private RMap<String, BigDecimal> contributionSummaryRMap;

    private RMap<String, LiveStreamAudience> liveStreamAudienceRMap;

    private RMap<String, LiveCategory> liveCategoryList;

//    private RList<LiveStreamChannel> liveStreamChannelRList;

    private RList<String> blockedChannelMemberRList;

    @Autowired
    public RedisCacheProviderImpl(Environment env, LiveStreamService liveStreamService, PointService pointService, FansIntimacyService fansIntimacyService) {
        this.liveStreamService = liveStreamService;
        this.pointService = pointService;
        this.fansIntimacyService = fansIntimacyService;

        try {
            boolean isProd = env.getProperty("server.production", Boolean.class, true);
            if (isProd) {
                Config config = new Config();
                config.useSingleServer()
                        .setPassword(env.getProperty("redis.password"))
                        .setAddress("redis://" + env.getProperty("redis.host"));
                redissonClient = Redisson.create(config);
            } else {
                redissonClient = Redisson.create();
            }
            // flush first before add records into redis, else old data will cache into redis
            redissonClient.getKeys().flushdb();

            symbolicItems = redissonClient.getList("gifFile");
            fansIntimacyConfigsRList = redissonClient.getList("fansIntimacyConfig");
            pointSummaryRMap = redissonClient.getMap("pointSummary");
            contributionSummaryRMap = redissonClient.getMap("contributionSummary");
//            liveStreamChannelRList = redissonClient.getList("liveStreamChannel");
            blockedChannelMemberRList = redissonClient.getList("blockedChannelMember");
            liveStreamAudienceRMap = redissonClient.getMap("liveStreamAudience");
            liveCategoryList = redissonClient.getMap("liveCategoryList");
            globalData = redissonClient.getMap("globalData");

            init();

            redisStatus = true;

        } catch (Exception e) {
            redisStatus = false;
            log.debug("Redis failed:" + e.getMessage());
        } finally {
            bannerStatus = false;
            symbolicItems = null;
            fansIntimacyConfigsRList = null;
            globalData = null;
            pointSummaryRMap = null;
            contributionSummaryRMap = null;
//            liveStreamChannelRList = null;
            blockedChannelMemberRList = null;
            liveStreamAudienceRMap = null;
        }
    }

    private void init() {
        initSymbolicItem();

        initFansIntimacyConfig();
        initPointSummary();
        initContributionSummary();
//        initLiveStreamChannel();
        initBlockedChannelByAdmin();
//        initAudience();

        // gif version
        globalData.fastPutIfAbsent("gifVersion", String.valueOf(pointService.getLatestGifVersion()));
    }

    // ----------------------------------------------------------------- //
    // ---------------- INITIATE REDIS VARIABLE (START) ---------------- //

    private void initSymbolicItem() {
        for (SymbolicItem symbolicItem : pointService.findActiveSymbolicItems()) {
            symbolicItems.addAsync(symbolicItem);
        }
        log.debug("List of stored keys:: " + symbolicItems.size());
    }

    private void initFansIntimacyConfig() {
        for (FansIntimacyConfig fansIntimacyConfig : fansIntimacyService.getAllFansIntimacyConfig()) {
            fansIntimacyConfigsRList.addAsync(fansIntimacyConfig);
        }
        log.debug("FansIntimacyConfig size : " + fansIntimacyConfigsRList.size());
    }

    private void initPointSummary() {
        for (PointSummary pointSummary : pointService.getAllPointSummary()) {
            pointSummaryRMap.fastPutIfAbsentAsync(pointSummary.getOwnerId(), pointSummary.getTotalAmt());
        }
        log.debug("PointSummary size : " + pointSummaryRMap.size());
    }

    private void initContributionSummary() {
        for (Map<String, BigDecimal> contribution : pointService.getAllContributionSummary()) {
            contributionSummaryRMap.putAllAsync(contribution);
        }
        log.debug("contribution size : " + contributionSummaryRMap.size());
    }

    //    private void initLiveStreamChannel() {
//        for (LiveStreamChannel liveStreamChannel : liveStreamService.findAllLiveStreamChannel(Global.Status.ACTIVE, true)) {
//            liveStreamChannelRList.addAsync(liveStreamChannel);
//        }
//    }
    private void initBlockedChannelByAdmin() {
        for (LiveStreamChannel liveStreamChannel : liveStreamService.findAllLiveStreamChannel(Global.Status.DISABLED, true)) {
            blockedChannelMemberRList.addAsync(liveStreamChannel.getMemberId());
        }
        log.debug("BlockedChannelByAdmin size : " + blockedChannelMemberRList.size());
    }

    private void initAudience() {
        for (LiveStreamAudience liveStreamAudience : liveStreamService.findNewestAudience()) {
            liveStreamAudienceRMap.fastPutIfAbsentAsync(liveStreamAudience.getRefNo(), liveStreamAudience);
        }
        log.debug("initAudience size : " + liveStreamAudienceRMap.size());
    }

    // ----------------- INITIATE REDIS VARIABLE (END) ----------------- //
    // ----------------------------------------------------------------- //

    @Override
    public List<SymbolicItem> getSymbolicItems() {
        return redissonClient.getList("gifFile");
    }

    @Override
    public void removeSymbolicItem(SymbolicItem symbolicItem) {
        redissonClient.getList("gifFile").remove(symbolicItem);
    }

    @Override
    public void removeAudience(LiveStreamAudience liveStreamAudience) {
        redissonClient.getMap("liveStreamAudience").remove(liveStreamAudience);
    }

    @Override
    public void addSymbolicItem(SymbolicItem symbolicItem) {
        redissonClient.getList("gifFile").add(symbolicItem);
    }

    @Override
    public void updateSymbolicItem(SymbolicItem symbolicItem) {
        try {
            RList<SymbolicItem> symbolicItems = redissonClient.getList("gifFile");
            int index = IntStream.range(0, symbolicItems.size())
                    .filter(ind -> symbolicItems.get(ind).getId().equals(symbolicItem.getId()))
                    .findFirst().orElse(-1);
            if (index >= 0) {
                symbolicItems.fastSetAsync(index, symbolicItem);
            } else {
                redisStatus = false;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public void setGlobalData(RMap<String, String> globalData) {
        this.globalData = globalData;
    }


    @Override
    public RMap<String, String> getGlobalDataMap() {
        return this.redissonClient.getMap("globalData");
    }

    @Override
    public int getGlobalDataInt(String key) {
        try {
            return Integer.parseInt(getGlobalDataMap().get(key));
        } catch (Exception e) {
            return pointService.getLatestGifVersion();
        }
    }

    public void updateGlobalDataByKey(String key, int value) {
        this.redissonClient.getMap("globalData").fastPut(key, String.valueOf(value));
    }

    @Override
    public Boolean getRedisStatus() {
        return redisStatus;
    }

    @Override
    public void setRedisStatus(Boolean redisStatus) {
        this.redisStatus = redisStatus;
    }

    public RList<FansIntimacyConfig> getFansIntimacyConfigsRList() {
        return redissonClient.getList("fansIntimacyConfig");
    }

    public void setFansIntimacyConfigsRList(RList<FansIntimacyConfig> fansIntimacyConfigsRList) {
        this.fansIntimacyConfigsRList = fansIntimacyConfigsRList;
    }

    public void updateFansIntimacyConfigs(FansIntimacyConfig fansIntimacyConfig) {
        int index = IntStream.range(0, getFansIntimacyConfigsRList().size())
                .filter(ind -> getFansIntimacyConfigsRList().get(ind).getId().equals(fansIntimacyConfig.getId()))
                .findFirst().orElse(-1);
        if (index >= 0) {
            getFansIntimacyConfigsRList().fastSetAsync(index, fansIntimacyConfig);
        } else {
            redisStatus = false;
        }
    }

    @Override
    public void addFansIntimacyConfigs(FansIntimacyConfig fansIntimacyConfig) {
        redissonClient.getList("fansIntimacyConfig").add(fansIntimacyConfig);
    }

    @Override
    public RMap<String, BigDecimal> getPointSummaryRMap() {
        return redissonClient.getMap("pointSummary");
    }

    @Override
    public void addPointSummary(String ownerId, BigDecimal totalAmt) {
        try {
            getPointSummaryRMap().fastPutAsync(ownerId, totalAmt);
        } catch (RedisConnectionException redisEx) {
            redisStatus = false;
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
    }

    @Override
    public RMap<String, BigDecimal> getContributionSummaryRMap() {
        return redissonClient.getMap("contributionSummary");
    }

    @Override
    public void addContributionSummary(String contributorId, BigDecimal totalAmt) {
        try {
            getContributionSummaryRMap().fastPutAsync(contributorId, totalAmt);
        } catch (RedisConnectionException redisEx) {
            redisStatus = false;
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
    }

//    @Override
//    public RList<PointBalance> getPointBalanceRList() {
//        return redissonClient.getList("pointBalance");
//    }
//
//
//    @Override
//    public void addPointBalance(PointBalance pointBalance) {
//        try {
//            int index = IntStream.range(0, getPointBalanceRList().size())
//                    .filter(i -> getPointBalanceRList().get(i).getContributorId().equals(pointBalance.getContributorId())
//                            && getPointBalanceRList().get(i).getOwnerId().equals(pointBalance.getOwnerId()))
//                    .findFirst().orElse(-1);
//
//            if (index >= 0) {
//                getPointBalanceRList().fastSetAsync(index, pointBalance);
//            } else {
//                getPointBalanceRList().addAsync(pointBalance);
//            }
//        } catch (Exception e) {
//            log.debug(e.getMessage());
//        }
//    }

//    @Override
//    public RList<LiveStreamChannel> getLiveStreamChannelList() {
//        return redissonClient.getList("liveStreamChannel");
//    }
//
//    @Override
//    public void addLiveStreamChannel(LiveStreamChannel liveStreamChannel) {
//        try {
//            getLiveStreamChannelList().addAsync(liveStreamChannel);
//        } catch (Exception e) {
//            log.debug(e.getMessage());
//        }
//    }
//
//    @Override
//    public void updateLiveStreamChannel(LiveStreamChannel liveStreamChannel) {
//        log.debug("LiveStreamChannel info: " + liveStreamChannel.toString());
//        try {
//            int index = IntStream.range(0, getLiveStreamChannelList().size())
//                    .filter(ind -> getLiveStreamChannelList().get(ind).getId().equals(liveStreamChannel.getId()))
//                    .findFirst().orElse(-1);
//            if (index >= 0) {
//                getLiveStreamChannelList().fastSetAsync(index, liveStreamChannel);
//            } else {
//                addLiveStreamChannel(liveStreamChannel);
//            }
//        } catch (Exception e) {
//            log.debug(e.getMessage());
//        }
//    }

    @Override
    public RList<String> getBlockedChannelMemberRList() {
        return redissonClient.getList("blockedChannelMember");
    }

    @Override
    public void addBlockedChannelMember(String memberId) {
        try {
            getBlockedChannelMemberRList().addAsync(memberId);
        } catch (RedisConnectionException redisEx) {
            redisStatus = false;
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
    }

    @Override
    public void removeBlockedChannelMember(String memberId) {
        try {
            getBlockedChannelMemberRList().removeAsync(memberId);
        } catch (RedisConnectionException redisEx) {
            redisStatus = false;
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
    }

    @Override
    public void addAudience(LiveStreamAudience liveStreamAudience) {
        try {
            getAudienceList().fastPutAsync(liveStreamAudience.getRefNo(), liveStreamAudience);
        } catch (RedisConnectionException redisEx) {
            redisStatus = false;
        } catch (Exception e) {
            log.debug(e.getMessage());
        }
    }

    @Override
    public RMap<String, LiveStreamAudience> getAudienceList() {
        return redissonClient.getMap("liveStreamAudience");
    }

    @Override
    public boolean getBannerStatus() {
        return bannerStatus;
    }

    @Override
    public void setBannerStatus(Boolean bannerStatus) {
        this.bannerStatus = bannerStatus;
    }

    @Override
    public RMap<String, List<LiveCategory>> getLiveCategoryRList() {
        return redissonClient.getMap("liveCategoryList");

    }

    @Override
    public void setLiveCategoryRList(String fileLanguage, List<LiveCategory> liveCategoryList) {
        try {
            redissonClient.getMap("liveCategoryList").fastPutAsync(fileLanguage, liveCategoryList);

            bannerStatus = true;
        }catch (RedisConnectionException redisEx) {
            redisStatus = false;
            log.debug(redisEx.getMessage());
        }catch(Exception e) {
            log.debug(e.getMessage());
        }
    }
}
