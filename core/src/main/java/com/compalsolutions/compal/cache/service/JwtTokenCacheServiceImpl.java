package com.compalsolutions.compal.cache.service;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.security.service.TokenService;
import com.compalsolutions.compal.security.vo.JwtToken;

@Component(JwtTokenCacheService.BEAN_NAME)
public class JwtTokenCacheServiceImpl implements JwtTokenCacheService {
    @Override
    public String getCacheServiceDescription() {
        return "This service used to cache JWT Token";
    }

    @Override
    public String getI18nCacheServiceDescription() {
        return null;
    }

    @CacheEvict(value = "jwtTokenCache", allEntries = true)
    @Override
    public void evictAll() {
    }

    @Cacheable("jwtTokenCache")
    @Override
    public JwtToken getJwtToken(String jwtId) {
        TokenService tokenService = Application.lookupBean(TokenService.BEAN_NAME, TokenService.class);

        JwtToken jwtToken = tokenService.findJwtTokenByJwtId(jwtId);
        if (jwtToken == null) {
            throw new ValidatorException("invalid jwtToken in jwtTokenCache");
        }

        return jwtToken;
    }

    @CacheEvict("jwtTokenCache")
    @Override
    public void evictJwtToken(String jwtId) {
        // does nothing
    }
}
