package com.compalsolutions.compal.cache.service;

import com.compalsolutions.compal.security.vo.JwtToken;

public interface JwtTokenCacheService extends CacheEvictableService {
    public static final String BEAN_NAME = "jwtTokenCacheService";

    public JwtToken getJwtToken(String jwtId);

    public void evictJwtToken(String jwtId);
}
