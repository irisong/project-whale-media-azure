package com.compalsolutions.compal.cache;

import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import org.redisson.api.RList;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;

import java.math.BigDecimal;
import java.util.List;

public interface RedisCacheProvider {
    public static final String BEAN_NAME = "redisCacheProvider";

    public List<SymbolicItem> getSymbolicItems();

    public void updateGlobalDataByKey(String key, int value);

    public void removeSymbolicItem(SymbolicItem symbolicItem);

    public void removeAudience(LiveStreamAudience liveStreamAudience);

    public void addSymbolicItem(SymbolicItem symbolicItem);

    public void updateSymbolicItem(SymbolicItem symbolicItem);

    public int getGlobalDataInt(String key);

    public RMap<String, String> getGlobalDataMap();

    public Boolean getRedisStatus();

    public void setRedisStatus(Boolean redisStatus);

    public RList<FansIntimacyConfig> getFansIntimacyConfigsRList();

    public void updateFansIntimacyConfigs(FansIntimacyConfig fansIntimacyConfig);

    public void addFansIntimacyConfigs(FansIntimacyConfig fansIntimacyConfig);

    public RMap<String, BigDecimal> getPointSummaryRMap();

    public void addPointSummary(String ownerId, BigDecimal totalAmt);

    public RMap<String, BigDecimal> getContributionSummaryRMap();

    public void addContributionSummary(String contributorId, BigDecimal totalAmt);

//    public RList<PointBalance> getPointBalanceRList();
//
//    public void addPointBalance(PointBalance pointBalance);

//    public RList<LiveStreamChannel> getLiveStreamChannelList();
//
//    public void addLiveStreamChannel(LiveStreamChannel liveStreamChannel);
//
//    public void updateLiveStreamChannel(LiveStreamChannel liveStreamChannel);

    public RList<String> getBlockedChannelMemberRList();

    public void addBlockedChannelMember(String memberId);

    public void removeBlockedChannelMember(String memberId);

    public void addAudience(LiveStreamAudience liveStreamAudience);

    public RMap<String, LiveStreamAudience> getAudienceList();

    public boolean getBannerStatus();

    public void setBannerStatus(Boolean bannerStatus);

    public RMap<String, List<LiveCategory>> getLiveCategoryRList();

    public void setLiveCategoryRList(String fileLanguage, List<LiveCategory> liveCategoryList);

}
