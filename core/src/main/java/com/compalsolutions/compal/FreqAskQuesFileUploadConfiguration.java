package com.compalsolutions.compal;

public class FreqAskQuesFileUploadConfiguration {

    public static final String BEAN_NAME = "freqAskQuesFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;

    public static final String FOLDER_NAME = "faq";


    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getFaqFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullFaqParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }
}
