package com.compalsolutions.compal.azure;

import com.compalsolutions.compal.FileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component(AzureBlobStorageService.BEAN_NAME)
public class AzureBlobStorageServiceImpl implements AzureBlobStorageService {

    @Override
    public void uploadFileToAzureBlobStorage(String fileName, String oldFileName, File sourceFile,
                                             String contentType, String folderName) {
        uploadFileToAzureBlobStorage(fileName, oldFileName, sourceFile, contentType, folderName, null, 0);
    }

    @Override
    public void uploadFileToAzureBlobStorage(String fileName, String oldFileName, File sourceFile,
                                             String contentType, String folderName, byte[] bytes, long fileSize) {
        FileUploadConfiguration config = Application.lookupBean(FileUploadConfiguration.class);
        CloudStorageAccount storageAccount;
        CloudBlobClient blobClient;
        CloudBlobContainer container;
        // Parse the connection string and create a blob client to interact with Blob storage
        try {
            System.out.println("uploadFileToAzureBlobStorage");
            storageAccount = CloudStorageAccount.parse(config.getAzureBlobStorageConnection());
            blobClient = storageAccount.createCloudBlobClient();
            container = blobClient.getContainerReference("file");

            // Create the container if it does not exist with public access.
            System.out.println("Creating container: " + container.getName());
            container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(),
                    new OperationContext());
//            CloudBlockBlob blob = container.getBlockBlobReference(fileName);
            CloudBlobDirectory cloudBlobDirectory = container.getDirectoryReference(folderName);
            CloudBlockBlob blob = cloudBlobDirectory.getBlockBlobReference(fileName);
            if (StringUtils.isNotBlank(oldFileName)) {
                CloudBlockBlob blobOld = cloudBlobDirectory.getBlockBlobReference(oldFileName);
                blobOld.deleteIfExists();
            }

            blob.getProperties().setContentType(contentType);
            //Creating blob and uploading file to it
            if (sourceFile != null) {
                System.out.println("Uploading the sample file: " + contentType + ";" + sourceFile.getName());
//            blob.uploadFromByteArray(sourceFile.getAbsolutePath());
                bytes = Files.readAllBytes(Paths.get(sourceFile.getAbsolutePath()));
//            inputStream = new FileInputStream(sourceFile);
//            blob.uploadFromByteArray(bytes, 0, (int) sourceFile.length());
//            blob.upload(inputStream, sourceFile.length());
                blob.uploadFromByteArray(bytes, 0, (int) sourceFile.length());
            } else {
                blob.uploadFromByteArray(bytes, 0, (int) fileSize);
            }

            //Listing contents of container
//            System.out.println(blob.getUri());
//            for (ListBlobItem blobItem : container.listBlobs()) {
//                System.out.println("URI of blob is: " + blobItem.getUri());
//            }
        } catch (StorageException ex) {
            System.out.println(String.format("Error returned from the service. Http code: %d and error code: %s", ex.getHttpStatusCode(), ex.getErrorCode()));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
//            try {
//                if (inputStream != null) {
//                    inputStream.close();
//                }
//            } catch (Exception e) {
//                System.out.println(e.toString());
//            }
            System.out.println("The program has completed successfully.");
            System.out.println("Press the 'Enter' key while in the console to delete the sample files, example container, and exit the application.");
        }
    }

    @Override
    public void uploadFileToAzureBlobStorageByOutputStream(String fileName, File sourceFile,
                                                           String contentType, String folderName) {
        FileUploadConfiguration config = Application.lookupBean(FileUploadConfiguration.class);
        CloudStorageAccount storageAccount;
        CloudBlobClient blobClient;
        CloudBlobContainer container;
        // Parse the connection string and create a blob client to interact with Blob storage
        try {
            System.out.println("uploadFileToAzureBlobStorage");
            storageAccount = CloudStorageAccount.parse(config.getAzureBlobStorageConnection());
            blobClient = storageAccount.createCloudBlobClient();
            container = blobClient.getContainerReference("file");

            // Create the container if it does not exist with public access.
            System.out.println("Creating container: " + container.getName());
            container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(),
                    new OperationContext());
//            CloudBlockBlob blob = container.getBlockBlobReference(fileName);
            CloudBlobDirectory cloudBlobDirectory = container.getDirectoryReference(folderName);
            CloudBlockBlob blob = cloudBlobDirectory.getBlockBlobReference(fileName);
            blob.getProperties().setContentType(contentType);
            //Creating blob and uploading file to it
            System.out.println("Uploading the sample file: " + contentType + ";" + sourceFile.getName());
            InputStream inputStream = new FileInputStream(sourceFile);
            blob.upload(inputStream, sourceFile.length());

            //Listing contents of container
//            System.out.println(blob.getUri());
//            for (ListBlobItem blobItem : container.listBlobs()) {
//                System.out.println("URI of blob is: " + blobItem.getUri());
//            }
            inputStream.close();
        } catch (StorageException ex) {
            System.out.println(String.format("Error returned from the service. Http code: %d and error code: %s", ex.getHttpStatusCode(), ex.getErrorCode()));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        } finally {
//            try {
//                if (inputStream != null) {
//                    inputStream.close();
//                }
//            } catch (Exception e) {
//                System.out.println(e.toString());
//            }
            System.out.println("The program has completed successfully.");
            System.out.println("Press the 'Enter' key while in the console to delete the sample files, example container, and exit the application.");
        }
    }

    @Override
    public void deleteFileByName(String folderName, String oldFileName) {
        FileUploadConfiguration config = Application.lookupBean(FileUploadConfiguration.class);
        CloudStorageAccount storageAccount;
        CloudBlobClient blobClient;
        CloudBlobContainer container;
        try {
            storageAccount = CloudStorageAccount.parse(config.getAzureBlobStorageConnection());
            blobClient = storageAccount.createCloudBlobClient();
            container = blobClient.getContainerReference("file");
            container.createIfNotExists(BlobContainerPublicAccessType.CONTAINER, new BlobRequestOptions(),
                    new OperationContext());
            CloudBlobDirectory cloudBlobDirectory = container.getDirectoryReference(folderName);
            if (StringUtils.isNotBlank(oldFileName)) {
                CloudBlockBlob blobOld = cloudBlobDirectory.getBlockBlobReference(oldFileName);
                blobOld.deleteIfExists();
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
