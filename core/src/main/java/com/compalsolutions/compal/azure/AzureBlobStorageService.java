package com.compalsolutions.compal.azure;

import com.microsoft.azure.storage.blob.CloudBlobDirectory;

import java.io.File;

public interface AzureBlobStorageService {
    public static final String BEAN_NAME = "azureBlobStorageService";


    public void uploadFileToAzureBlobStorage(String fileName, String oldFileName, File sourceFile,
                                             String contentType, String folderName);

    public void uploadFileToAzureBlobStorage(String fileName, String oldFileName, File sourceFile,
                                             String contentType, String folderName, byte[] bytes, long fileSize);

    public void uploadFileToAzureBlobStorageByOutputStream(String fileName, File sourceFile,
                                                           String contentType, String folderName);

    public void deleteFileByName(String folderName, String oldFileName);
}
