package com.compalsolutions.compal.currency.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.currency.client.ExchangeRateConnector;
import com.compalsolutions.compal.currency.client.dto.CurrencyExchangeDto;
import com.compalsolutions.compal.exception.ValidatorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.currency.dao.CurrencyDao;
import com.compalsolutions.compal.currency.dao.CurrencyExchangeDao;
import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;

@Component(CurrencyService.BEAN_NAME)
public class CurrencyServiceImpl implements CurrencyService {
    @Autowired
    private CurrencyDao currencyDao;
    @Autowired
    private CurrencyExchangeDao currencyExchangeDao;

    @Override
    public Currency findCurrencyByCode(String currencyCode) {
        return currencyDao.get(currencyCode);
    }

    @Override
    public void saveCurrency(Currency currency) {
        currencyDao.save(currency);
    }

    @Override
    public List<Currency> findAllCurrencies() {
        return currencyDao.findAllCurrencies();
    }

    @Override
    public void doProcessLatestCurrencyExchangeRate(RunTaskLogger logger) {
        logger.debug("Process currency Exchange");
        ExchangeRateConnector exchangeRateRest = new ExchangeRateConnector();
        CurrencyExchangeDto currencyExchangeDto = exchangeRateRest.getResponseBody();
        java.util.Date utilDate = new java.util.Date();
        String baseFiat = Global.WalletType.USD;
        Double rateInFiatBase = 0D;
        for (Map.Entry<String, Double> entry : currencyExchangeDto.getRates().entrySet()) {
            if(baseFiat.equalsIgnoreCase(entry.getKey())){
                rateInFiatBase = entry.getValue();
            }
        }

        if(rateInFiatBase <= 0){
            throw new ValidatorException("Invalid Base Rate In Fiat(USD)");
        }

        for (Map.Entry<String, Double> entry : currencyExchangeDto.getRates().entrySet()) {
            String key = entry.getKey();
            Double value = entry.getValue()/rateInFiatBase;
            CurrencyExchange currencyExchange = new CurrencyExchange();
            currencyExchange.setCurrencyCodeFrom(key);
            currencyExchange.setCurrencyCodeTo(baseFiat);
            currencyExchange.setSite("https://fixer.io");
            currencyExchange.setExchangeDatetime(utilDate);
            currencyExchange.setRate(value);
            currencyExchange.setStatus(Global.Status.INACTIVE);
            currencyExchangeDao.save(currencyExchange);
        }
        updateCurrencyCodeToStatusActive(currencyExchangeDto.getRates().size());
        currencyExchangeDto = null;
    }

    @Override
    public List<CurrencyExchange> findLatestCurrencyExchanges() {
        List<CurrencyExchange> currencyExchanges = new ArrayList<>();
        List<String[]> exchangeTypes = currencyExchangeDao.findAllCurrencyExchangeTypes();
        for (String[] type : exchangeTypes) {
            CurrencyExchange currencyExchange = findLatestCurrencyExchange(type[0], type[1]);
            if (currencyExchange != null)
                currencyExchanges.add(currencyExchange);
        }
        return currencyExchanges;
    }

    @Override
    public List<CurrencyExchange> findLatestActiveCurrencyExchanges() {
        List<CurrencyExchange> currencyExchanges = new ArrayList<>();
        List<String[]> exchangeTypes = currencyExchangeDao.findAllActiveCurrencyExchangeTypes();
        for (String[] type : exchangeTypes) {
            CurrencyExchange currencyExchange = findLatestCurrencyExchange(type[0], type[1]);
            if (currencyExchange != null)
                currencyExchanges.add(currencyExchange);
        }
        exchangeTypes.clear();
        exchangeTypes = null;
        return currencyExchanges;
    }

    @Override
    public CurrencyExchange findLatestCurrencyExchange(String currencyFrom, String currencyTo) {
        return currencyExchangeDao.findLatestCurrencyExchange(currencyFrom, currencyTo);
    }

    @Override
    public BigDecimal getWhaleCoinExchangeRate(String currencyTo) {
        BigDecimal price;
        switch (currencyTo) {
            case Global.WalletType.MYR:
                price = new BigDecimal("0.015");
                break;
            default:
                price = BigDecimal.ONE;
        }
        return price;
    }

    @Override
    public void saveCurrencyExchange(CurrencyExchange currencyExchange) {
        if (StringUtils.isBlank(currencyExchange.getSite())) {
            currencyExchange.setSite("SELF");
        }
        currencyExchangeDao.save(currencyExchange);
    }

    @Override
    public void deleteCurrencyExchangeByCurrencyCodeFromAndCurrencyCodeTo(String currencyCodeFrom, String currencyCodeTo) {
        currencyExchangeDao.deleteByCurrencyCodeFromAndCurrencyCodeTo(currencyCodeFrom, currencyCodeTo);
    }

    @Override
    public void updateCurrencyCodeToStatusActive(int row) {
        currencyExchangeDao.updateCurrencyCodeToStatusActive(row);
    }


}
