package com.compalsolutions.compal.currency.service;

import java.math.BigDecimal;
import java.util.List;

import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;

public interface CurrencyService {
    public static final String BEAN_NAME = "currencyService";

    public Currency findCurrencyByCode(String currencyCode);

    public void saveCurrency(Currency currency);

    public List<Currency> findAllCurrencies();

    public void doProcessLatestCurrencyExchangeRate(RunTaskLogger logger);

    public List<CurrencyExchange> findLatestCurrencyExchanges();

    public List<CurrencyExchange> findLatestActiveCurrencyExchanges();

    public CurrencyExchange findLatestCurrencyExchange(String currencyFrom, String currencyTo);

    public void saveCurrencyExchange(CurrencyExchange currencyExchange);

    public void deleteCurrencyExchangeByCurrencyCodeFromAndCurrencyCodeTo(String currencyCodeFrom, String currencyCodeTo);

    public void updateCurrencyCodeToStatusActive(int row);

    public BigDecimal getWhaleCoinExchangeRate(String currencyTo);

}
