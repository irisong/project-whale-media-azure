package com.compalsolutions.compal.currency.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.currency.vo.Currency;

public interface CurrencyRepository extends JpaRepository<Currency, String> {
    public static final String BEAN_NAME = "currencyRepository";
}
