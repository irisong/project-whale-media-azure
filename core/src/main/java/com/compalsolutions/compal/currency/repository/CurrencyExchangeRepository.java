package com.compalsolutions.compal.currency.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.currency.vo.CurrencyExchange;

public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange, String> {
    public static final String BEAN_NAME = "currencyExchangeRepository";
}
