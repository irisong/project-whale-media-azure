package com.compalsolutions.compal.currency.client;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.client.dto.CurrencyExchangeDto;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.springframework.core.env.Environment;

import java.io.IOException;

public class ExchangeRateConnector {
    private static final Log log = LogFactory.getLog(ExchangeRateConnector.class);

    private String serverUrl;

    private String apiKey;

    public ExchangeRateConnector() {
        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("currency.exchange.api.url");
        apiKey = env.getProperty("currency.exchange.api.key");
    }

    public CurrencyExchangeDto getResponseBody() {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CurrencyExchangeDto currencyExchangeDto;

        HttpResponse httpResponse;
        try {
            httpResponse = httpClient.execute(new HttpGet(serverUrl + "/latest?access_key=" + apiKey));
            String responseData = EntityUtils.toString(httpResponse.getEntity(), "utf-8");
            ObjectMapper objectMapper = new ObjectMapper();
            currencyExchangeDto = objectMapper.readValue(responseData, CurrencyExchangeDto.class);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            log.error(e.getMessage(), e.fillInStackTrace());
            throw new SystemErrorException(e.getMessage());
        }
        return currencyExchangeDto;
    }
}
