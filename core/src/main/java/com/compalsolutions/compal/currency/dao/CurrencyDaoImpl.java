package com.compalsolutions.compal.currency.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.currency.repository.CurrencyRepository;
import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(CurrencyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CurrencyDaoImpl extends Jpa2Dao<Currency, String> implements CurrencyDao {
    @SuppressWarnings("unused")
    @Autowired
    private CurrencyRepository currencyRepository;

    public CurrencyDaoImpl() {
        super(new Currency(false));
    }

    @Override
    public List<Currency> findAllCurrencies() {
        Currency example = new Currency(false);
        example.setStatus(Global.Status.ACTIVE);
        return findByExample(example, "currencyCode");
    }
}
