package com.compalsolutions.compal.currency.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.compalsolutions.compal.Global;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.currency.repository.CurrencyExchangeRepository;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.dao.Jpa2Dao;

@Component(CurrencyExchangeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CurrencyExchangeDaoImpl extends Jpa2Dao<CurrencyExchange, String> implements CurrencyExchangeDao {
    @SuppressWarnings("unused")
    @Autowired
    private CurrencyExchangeRepository currencyExchangeRepository;

    public CurrencyExchangeDaoImpl() {
        super(new CurrencyExchange(false));
    }

    //  China, Indonesia, South Korea, Malaysia, Taiwan, Thailand, United States
    private static final String CURRENCY_IN_USED = "'CNY', 'IDR', 'KRW', 'MYR', 'TWD', 'THB', 'USD'";

    @Override
    public List<String[]> findAllCurrencyExchangeTypes() {
        String hql = "select distinct x.currencyCodeFrom, x.currencyCodeTo from x in " + CurrencyExchange.class + " order by x.currencyCodeFrom";
        return getCurrencyExchangeTypes(hql);
    }

    @Override
    public List<String[]> findAllActiveCurrencyExchangeTypes() {
        String hql = "select distinct x.currencyCodeFrom, x.currencyCodeTo from x in " + CurrencyExchange.class +
                " where x.status='ACTIVE' order by x.currencyCodeFrom";
        return getCurrencyExchangeTypes(hql);
    }

    private List<String[]> getCurrencyExchangeTypes(String hql) {
        List<String[]> currencyExchangeTypes = new ArrayList<String[]>();
        @SuppressWarnings("unchecked")
        List<Object[]> results = (List<Object[]>) exFindQueryAsList(hql);
        for (Object[] r : results) {
            currencyExchangeTypes.add(new String[]{(String) r[0], (String) r[1]});
        }
        return currencyExchangeTypes;
    }

    @Override
    public CurrencyExchange findLatestCurrencyExchange(String currencyFrom, String currencyTo) {
        CurrencyExchange example = new CurrencyExchange(false);
        example.setCurrencyCodeFrom(currencyFrom);
        example.setCurrencyCodeTo(currencyTo);
        return findFirst(example, "datetimeAdd desc");
    }

    @Override
    public void deleteByCurrencyCodeFromAndCurrencyCodeTo(String currencyCodeFrom, String currencyCodeTo) {
        String hql = "delete from CurrencyExchange x where x.currencyCodeFrom=? and x.currencyCodeTo=? ";
        this.bulkUpdate(hql, currencyCodeFrom, currencyCodeTo);
    }

    @Override
    public void updateCurrencyCodeToStatusActive(int row) {
//        List<String> currencyInUsed = new ArrayList(Arrays.asList(CURRENCY_IN_USED.split(",")));
        List<Object> params = new ArrayList<>();
        String hql = "update CurrencyExchange c set c.status = ?" +
                " where c.currencyCodeFrom in (" + CURRENCY_IN_USED + ") " +
                " order by c.exchange_datetime desc limit " + row;
        params.add(Global.Status.ACTIVE);
        this.bulkUpdate(hql, params.toArray());
    }
}
