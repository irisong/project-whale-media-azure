package com.compalsolutions.compal.merchant.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.general.vo.Province;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.MemberAddress;
import com.compalsolutions.compal.merchant.dao.*;
import com.compalsolutions.compal.merchant.dto.MerchantOrderDto;
import com.compalsolutions.compal.merchant.vo.*;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.dao.PointWalletTrxDao;
import com.compalsolutions.compal.wallet.dao.WalletBalanceDao;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;
import com.compalsolutions.compal.wallet.vo.WalletBalance;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Component(MerchantOrderService.BEAN_NAME)
public class MerchantOrderServiceImpl implements MerchantOrderService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private MerchantOrderDao merchantOrderDao;

    @Autowired
    private MerchantOrderItemDao merchantOrderItemDao;

    @Autowired
    private MerchantOrderPaymentDao merchantOrderPaymentDao;

    @Autowired
    private MerchantOrderShipmentDao merchantOrderShipmentDao;

    @Autowired
    private MerchantShippingConfigDao merchantShippingConfigDao;

    @Autowired
    private ProductDetailDao productDetailDao;

    @Autowired
    private WalletBalanceDao walletBalanceDao;

    @Autowired
    private PointWalletTrxDao pointWalletTrxDao;

    @Override
    public MerchantOrderDto getCheckoutInfo(Locale locale, String memberId, String currencyCode, String addressId, String productId, String productDetId, int orderQty) {
        I18n i18n = Application.lookupBean(I18n.class);
        MerchantService merchantService = Application.lookupBean(MerchantService.class);
        ProductService productService = Application.lookupBean(ProductService.class);
        WalletService walletService = Application.lookupBean(WalletService.class);
        MemberService memberService = Application.lookupBean(MemberService.class);

        MerchantOrderDto merchantOrderDto = new MerchantOrderDto();

        /************************
         * VERIFICATION - START
         ************************/
        if (orderQty <= 0) {
            throw new ValidatorException(i18n.getText("invalidQty", locale));
        }

        if (StringUtils.isBlank(currencyCode)) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }
        MerchantCurrency merchantCurrency = merchantService.getMerchantCurrencyByCurrencyCode(currencyCode, Global.Status.ACTIVE);
        if (merchantCurrency == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }

        ProductDetail productDetail = productService.doCheckProductDetailErrorType(locale, productId, productDetId, currencyCode);
        if (StringUtils.isNotBlank(productDetail.getErrorType())) {
            merchantOrderDto.setErrorType(productDetail.getErrorType());
            merchantOrderDto.setErrorMsg(productDetail.getErrorMsg());
            return merchantOrderDto;
        } else if (orderQty > productDetail.getAvailableQty()) {
            throw new ValidatorException(i18n.getText("orderQtyExceedAvailableQty", locale), productDetail.getAvailableQty());
        }
        /************************
         * VERIFICATION - END
         ************************/

        /** SHIPPING ADDRESS **/
        MemberAddress memberAddress;
        if (StringUtils.isBlank(addressId)) {
            memberAddress = memberService.findDefaultMemberAddress(memberId);
        } else {
            memberAddress = memberService.findMemberAddressById(addressId);
        }

        if (memberAddress != null) {
            Address address = memberAddress.getAddress();

            String addressCountryCode = address.getCountryCode();
            Product product = productDetail.getProduct();
            String[] shipCountry = product.getShipCountry().split("\\|");
            boolean allowShipping = Arrays.stream(shipCountry).anyMatch(i -> i.equalsIgnoreCase(addressCountryCode));
            if (allowShipping) {
                address.setAddressId(memberAddress.getMemberAddrId());
                address.setLongAddress(address.getInternationalFullAddress(locale, ""));
                merchantOrderDto.setAddress(address);
            }
        }

        /** PRODUCT LIST **/
        productDetail = getProductDetailDisplay(locale, productDetail, orderQty, merchantCurrency);

        List<ProductDetail> productDetailList = new ArrayList();
        productDetailList.add(productDetail);
        merchantOrderDto.setProductDetailList(productDetailList); //return as array incase implement cart in future

        /** AMOUNT **/
        merchantOrderDto.setShippingFee(calculateShippingFee(currencyCode, productDetail.getWeight(), orderQty, ((memberAddress!=null)?memberAddress.getAddress():null)));
        merchantOrderDto.setCurrencyCode(currencyCode);

        if (currencyCode.equalsIgnoreCase(MerchantCurrency.DEFAULT_CURRENCY)) {
            WalletBalance rewardPointBalance = walletService.doCreateWalletBalanceIfNotExists(memberId,Global.UserType.MEMBER, Global.WalletType.WALLET_81);
            merchantOrderDto.setCurrentBalance(rewardPointBalance.getAvailableBalance());
        } else {
            merchantOrderDto.setUploadSlip(true);
        }

        return merchantOrderDto;
    }

    @Override
    public ProductDetail getProductDetailDisplay(Locale locale, ProductDetail productDetail, int orderQty, MerchantCurrency merchantCurrency) {
        ProductService productService = Application.lookupBean(ProductService.BEAN_NAME, ProductService.class);
        Environment env = Application.lookupBean(Environment.class);
        String serverUrl = env.getProperty("serverUrl") + "/file/productFile";

        if (locale.equals(Global.LOCALE_CN)) {
            productDetail.setProductName(productDetail.getProduct().getProductNameCn());
            productDetail.setVariant(productDetail.getVariantCn());
        } else {
            productDetail.setProductName(productDetail.getProduct().getProductName());
        }
        productDetail.setProductQty(orderQty);

        List<ProductPrice> productPrices = productService.findProductPriceByProductDetIdByCurrencyId(productDetail.getProductDetId(), merchantCurrency.getCurrencyId()); //need retrieve again to handle lazy load
        if (productPrices.size() > 0) {
            ProductPrice productPrice = productPrices.get(0);
            productDetail.setCurrencyCode(merchantCurrency.getCurrencyCode());
            productDetail.setPrice(productPrice.getPrice());
        }

        ProductFile productDetFile = productService.findProductFileByProductDetId(productDetail.getProductDetId());
        if (productDetFile != null) {
            productDetail.setFileUrl(productDetFile.getFileUrlWithParentPath(serverUrl));
        }

        return productDetail;
    }

    @Override
    public ProductDetail getMerchantOrderItemDisplay(Locale locale, MerchantOrderItem item, String currencyCode) {
        Environment env = Application.lookupBean(Environment.class);
        String serverUrl = env.getProperty("serverUrl") + "/file/merchantOrderItem";

        ProductDetail itemDisplay = new ProductDetail();
        itemDisplay.setProductName(item.getProductName());
        itemDisplay.setVariant(item.getVariant());
        if (locale.equals(Global.LOCALE_CN)) {
            itemDisplay.setProductName(item.getProductNameCn());
            itemDisplay.setVariant(item.getVariantCn());
        }

        itemDisplay.setProductDetId(item.getProductDetId());
        itemDisplay.setCurrencyCode(currencyCode);
        itemDisplay.setPrice(item.getAmount());
        itemDisplay.setProductQty(item.getQuantity());
        itemDisplay.setFileUrl(item.getFileUrlWithParentPath(serverUrl));

        return itemDisplay;
    }

    @Override
    public MerchantOrderDto doProcessMerchantOrder(Locale locale, String memberId, String currencyCode, String addressId, String productId, String productDetId, int orderQty, BigDecimal grandTotal) {
        synchronized (synchronizedObject) {
            Environment env = Application.lookupBean(Environment.class);
            I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
            MerchantService merchantService = Application.lookupBean(MerchantService.BEAN_NAME, MerchantService.class);
            ProductService productService = Application.lookupBean(ProductService.BEAN_NAME, ProductService.class);
            WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
            MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
            DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);
            SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);

            MerchantOrderDto merchantOrderDto = new MerchantOrderDto();

            /************************
             * VERIFICATION - START
             ************************/

            if (orderQty <= 0) {
                throw new ValidatorException(i18n.getText("invalidQty", locale));
            }

            if (StringUtils.isBlank(currencyCode)) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }
            MerchantCurrency merchantCurrency = merchantService.getMerchantCurrencyByCurrencyCode(currencyCode, Global.Status.ACTIVE);
            if (merchantCurrency == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            if (StringUtils.isBlank(addressId)) {
                throw new ValidatorException(i18n.getText("invalidAddressId", locale));
            }
            MemberAddress memberAddress = memberService.findMemberAddressById(addressId);
            if (memberAddress == null) {
                throw new ValidatorException(i18n.getText("addressNotFound", locale));
            }

            /** PRODUCT LIST **/
            ProductDetail productDetail = productService.doCheckProductDetailErrorType(locale, productId, productDetId, currencyCode);
            if (StringUtils.isNotBlank(productDetail.getErrorType())) {
                throw new ValidatorException(productDetail.getErrorType() + "@" + productDetail.getErrorMsg());
            } else if (orderQty > productDetail.getAvailableQty()) {
                throw new ValidatorException(i18n.getText("orderQtyExceedAvailableQty", locale), productDetail.getAvailableQty());
            }
            productDetail.setAvailableQty(productDetail.getAvailableQty() - orderQty);
            productDetailDao.update(productDetail);

            /** SHIPPING ADDRESS **/
            Address address = memberAddress.getAddress();
            Product product = productDetail.getProduct();
            String[] shipCountry = product.getShipCountry().split("\\|");
            boolean allowShipping = Arrays.stream(shipCountry).anyMatch(i -> i.equalsIgnoreCase(address.getCountryCode()));
            if (!allowShipping) {
                throw new ValidatorException(Product.ERROR_CHECKOUT + "@" + i18n.getText("productDoNotSupportAddress", locale));
            }

            /** AMOUNT **/
            List<ProductPrice> productPrices = productService.findProductPriceByProductDetIdByCurrencyId(productDetail.getProductDetId(), merchantCurrency.getCurrencyId()); //need retrieve again to handle lazy load
            if (productPrices.size() == 0) {
                throw new ValidatorException(Product.ERROR_CHECKOUT + "@" + i18n.getText("productDoNotExceptThisCurrency", locale));
            }
            BigDecimal productPrice = productPrices.get(0).getPrice();

            Pair<BigDecimal, BigDecimal> total = calculateTotal(productDetail, merchantCurrency.getCurrencyCode(), productPrice, orderQty, address);
            BigDecimal orderAmount = total.getLeft();
            BigDecimal shippingFee = total.getRight();

            System.out.println("productPrice = " + productPrice);
            System.out.println("orderAmount = " + orderAmount);
            System.out.println("shippingFee = " + shippingFee);
            System.out.println("grandTotal = " + grandTotal);
            if (!BDUtil.e(grandTotal, orderAmount.add(shippingFee))) { //in case product price change after checkout
                throw new ValidatorException(Product.ERROR_CHECKOUT + "@" + i18n.getText("productPriceChanged", locale));
            }

            System.out.println("DONE VERIFY");
            /************************
             * VERIFICATION - END
             ************************/

            System.out.println("Insert into MerchantOrder");
            MerchantOrder merchantOrder = new MerchantOrder(true);
            merchantOrder.setOrderRefNo(docNoService.doGetNextMerchantOrderRefNo());
            merchantOrder.setMemberId(memberId);
            merchantOrder.setOrderDatetime(new Date());
            merchantOrder.setExpiryDatetime(DateUtil.addDate(merchantOrder.getOrderDatetime(), 1));
            merchantOrder.setCurrencyCode(currencyCode);
            merchantOrder.setGrandTotal(grandTotal);
            merchantOrder.setStatus(MerchantOrder.STATUS_PENDING_PAYMENT);
            merchantOrderDao.save(merchantOrder);

            System.out.println("Insert into MerchantOrderItem");
            MerchantOrderItem orderItem = new MerchantOrderItem(true);
            orderItem.setOrderId(merchantOrder.getOrderId());
            orderItem.setProductDetId(productDetail.getProductDetId());
            orderItem.setProductName(productDetail.getProduct().getProductName());
            orderItem.setProductNameCn(productDetail.getProduct().getProductNameCn());
            orderItem.setVariant(productDetail.getVariant());
            orderItem.setVariantCn(productDetail.getVariantCn());
            orderItem.setQuantity(orderQty);
            orderItem.setAmount(productPrice);
            orderItem.setTotalAmount(orderAmount);
            merchantOrderItemDao.save(orderItem);

            System.out.println("Insert into MerchantOrderPayment");
            MerchantOrderPayment payment = new MerchantOrderPayment(true);
            payment.setOrderId(merchantOrder.getOrderId());
            payment.setPaymentRefNo(docNoService.doGetNextMerchantPaymentRefNo());
            payment.setCurrencyCode(currencyCode);
            payment.setOrderAmount(orderAmount);
            payment.setShippingFee(shippingFee);
            payment.setPaymentAmount(grandTotal);
            payment.setStatus(MerchantOrderPayment.STATUS_PENDING_PAYMENT);
            merchantOrderPaymentDao.save(payment);

            System.out.println("Insert into MerchantOrderShipment");
            MerchantOrderShipment shipping = new MerchantOrderShipment(true);
            shipping.setOrderId(merchantOrder.getOrderId());
//        shipping.setShippingRefNo(docNoService.doGetNextMerchantShippingRefNo());
            shipping.setShippingFee(shippingFee);
            shipping.setShippingAddress(address);
            shipping.setStatus(MerchantOrderShipment.STATUS_PENDING);
            merchantOrderShipmentDao.save(shipping);

            if (currencyCode.equalsIgnoreCase(MerchantCurrency.DEFAULT_CURRENCY)) { //Straight proceed Payment
                WalletBalance rewardPointBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_81);
                System.out.println("rewardPointBalance.getAvailableBalance() == " + rewardPointBalance.getAvailableBalance());
                if (BDUtil.g(grandTotal, rewardPointBalance.getAvailableBalance())) {
                    throw new ValidatorException(Product.ERROR_CHECKOUT + "@" + i18n.getText("insufficientPointBalance", locale));
                }

                //insert rewards point transaction
                PointWalletTrx pointWalletTrx = new PointWalletTrx(true);
                pointWalletTrx.setInAmt(BigDecimal.ZERO);
                pointWalletTrx.setOutAmt(grandTotal);
                pointWalletTrx.setOwnerId(memberId);
                pointWalletTrx.setOwnerType(Global.UserType.MEMBER);
                pointWalletTrx.setTrxDatetime(new Date());
                pointWalletTrx.setTrxType(Global.WalletTrxType.MERCHANT_SPENDING);
                pointWalletTrx.setWalletRefno(merchantOrder.getOrderRefNo());
                pointWalletTrx.setWalletRefId(merchantOrder.getOrderId());
                pointWalletTrx.setWalletType(Global.WalletType.WALLET_81);
                pointWalletTrx.setTrxDesc(i18n.getText("merchantSpendingDesc", merchantOrder.getOrderRefNo()));
                pointWalletTrx.setTrxDescCn(i18n.getText("merchantSpendingDesc", Global.LOCALE_CN, merchantOrder.getOrderRefNo()));
                pointWalletTrxDao.save(pointWalletTrx);

                //deduct Reward Point
                walletBalanceDao.doDebitAvailableBalance(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_81, grandTotal);
                rewardPointBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_81);
                merchantOrderDto.setCurrentBalance(rewardPointBalance.getAvailableBalance());

                merchantOrder.setStatus(MerchantOrder.STATUS_PAID);
                merchantOrderDao.update(merchantOrder);

                payment.setStatus(MerchantOrderPayment.STATUS_PAID);
                payment.setPaymentDatetime(new Date());
                merchantOrderPaymentDao.update(payment);

//                merchantOrderDto.setUploadSlip(false);
//                System.out.println("UploadSlip = false");
            }
//            else {
////                merchantOrderDto.setUploadSlip(true);
//                System.out.println("UploadSlip = true");
//            }

            /************************
             * SAVE IMAGE - START
             ************************/
            ProductFile productDetFile = productService.findProductFileByProductDetId(productDetail.getProductDetId());
            if (productDetFile != null) {
                orderItem.setFilename(productDetFile.getFilename());
                orderItem.setContentType(productDetFile.getContentType());
                orderItem.setFileSize(productDetFile.getFileSize());
                merchantOrderItemDao.update(orderItem);

                String uploadPath = env.getProperty("file.upload.path");
                File directory = new File(uploadPath+"/merchantOrderItem");
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                try {
                    Path srcPath = Paths.get(uploadPath+"/productFile", productDetFile.getRenamedFilename());
                    Path path = Paths.get(uploadPath+"/merchantOrderItem", orderItem.getRenamedFilename());
                    Files.copy(srcPath, path);
                } catch (IOException e) {
                    throw new ValidatorException("Couldn't retrieve product image!");
//                    throw new SystemErrorException(e.getMessage(), e);
                }
            }
            /************************
             * SAVE IMAGE - END
             ************************/

            //set content for product display
//            List<ProductDetail> productDetailList = new ArrayList<>();
//            ProductDetail itemDisplay = getMerchantOrderItemDisplay(locale, orderItem, merchantCurrency.getCurrencyCode());
//            productDetailList.add(itemDisplay);

            //set content for address display
//            shipping.getShippingAddress().setLongAddress(address.getInternationalFullAddress(locale, ""));

            System.out.println("Insert into DTO");
            merchantOrderDto = getMerchantOrderDto(merchantOrderDto, merchantOrder, payment, shipping, null);

//            if (merchantOrderDto.isUploadSlip()) {
//                SystemConfig systemConfig = systemConfigService.getDefaultSystemConfig();
//                if (systemConfig != null) {
//                    merchantOrderDto.setPaymentBank(systemConfig.getMerchantBank());
//                    merchantOrderDto.setPaymentAccountName(systemConfig.getMerchantAccountName());
//                    merchantOrderDto.setPaymentAccountNo(systemConfig.getMerchantAccountNo());
//                }
//            }
            System.out.println("DONE Insert into DTO");

            return merchantOrderDto;
        }
    }

    @Override
    public Pair<BigDecimal, BigDecimal> calculateTotal(ProductDetail productDetail, String currencyCode, BigDecimal productPrice, int orderQty, Address address) {
        BigDecimal orderAmount = productPrice.multiply(BigDecimal.valueOf(orderQty));
        BigDecimal shippingFee = calculateShippingFee(currencyCode, productDetail.getWeight(), orderQty, address);

        return new ImmutablePair<>(orderAmount, shippingFee);
    }

    @Override
    public BigDecimal calculateShippingFee(String currencyCode, double weight, int orderQty, Address address) {
        CountryService countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);

        String countryCode = null;
        String category = null;
        if (address != null) {
            countryCode = address.getCountryCode();
            Province province = countryService.getProvince(address.getState());
            if (province != null) {
                category = province.getCategory();
            }
        }

        BigDecimal charges = BigDecimal.ONE;
        if (currencyCode.equalsIgnoreCase("MYR")) {
            charges = new BigDecimal(50);
            MerchantShippingConfig config = merchantShippingConfigDao.findConfigByDestination(countryCode, category);
            if (config != null) {
                if (weight >= config.getWeightFrom() && weight <= config.getWeightTo()) {
                    charges = config.getCharges();
                } else if (weight > config.getWeightTo()) {
                    double nextUnit = weight - config.getWeightTo();
                    BigDecimal nextUnitCharges = config.getNextUnitCharges().multiply(new BigDecimal(nextUnit));
                    charges = config.getCharges().add(nextUnitCharges);
                }
            }
        }

        return charges.multiply(new BigDecimal(orderQty));
    }

    @Override
    public MerchantOrderDto getMerchantOrderDto(MerchantOrderDto dto, MerchantOrder merchantOrder, MerchantOrderPayment payment, MerchantOrderShipment shipping, List<ProductDetail> productDetailList) {
        if (dto == null)
            dto = new MerchantOrderDto();

        dto.setOrderId(merchantOrder.getOrderId());
        dto.setOrderRefNo(merchantOrder.getOrderRefNo());
        dto.setOrderStatus(merchantOrder.getStatus());
        dto.setOrderDatetime(merchantOrder.getOrderDatetime());
        dto.setExpiryDatetime(merchantOrder.getExpiryDatetime());
        dto.setRemark(payment.getRemark());
        dto.setAddress(shipping.getShippingAddress());
        dto.setProductDetailList(productDetailList);
        dto.setCurrencyCode(payment.getCurrencyCode());
        dto.setOrderTotal(payment.getOrderAmount());
        dto.setShippingFee(payment.getShippingFee());
        dto.setGrandTotal(payment.getPaymentAmount());

        return dto;
    }

    @Override
    public MerchantOrderDto doUploadMerchantBankInSlip(Locale locale, String orderId, String referenceNo, File fileUpload, String fileUploadFileName, String fileUploadContentType) {
        Environment env = Application.lookupBean(Environment.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        String uploadPath = env.getProperty("file.upload.path") + "/merchantOrderPayment";

        synchronized (synchronizedObject) {
            if (StringUtils.isBlank(orderId)) {
                throw new ValidatorException(i18n.getText("invalidOrderId", locale));
            }
            MerchantOrder merchantOrder = merchantOrderDao.get(orderId);
            if (merchantOrder == null) {
                throw new ValidatorException(i18n.getText("orderNotFound", locale));
            }

            if (merchantOrder.getStatus().equalsIgnoreCase(MerchantOrder.STATUS_ORDER_EXPIRED)) {
                throw new ValidatorException(i18n.getText("orderAlreadyExpired", locale));
            } else if (merchantOrder.getStatus().equalsIgnoreCase(MerchantOrder.STATUS_PROCESSING_PAYMENT)) {
                throw new ValidatorException(i18n.getText("paymentProcessing", locale));
            } else if (!merchantOrder.getStatus().equalsIgnoreCase(MerchantOrder.STATUS_PENDING_PAYMENT)
                    && !merchantOrder.getStatus().equalsIgnoreCase(MerchantOrder.STATUS_PAYMENT_FAILED)) {
                throw new ValidatorException(i18n.getText("orderStatusNotPendingPayment", locale));
            }

            merchantOrder.setStatus(MerchantOrder.STATUS_PROCESSING_PAYMENT);
            merchantOrderDao.update(merchantOrder);

            MerchantOrderPayment payment = merchantOrderPaymentDao.getMerchantOrderPaymentByOrderId(merchantOrder.getOrderId());
            payment.setFilename(fileUploadFileName);
            payment.setContentType(fileUploadContentType);
            payment.setFileSize(fileUpload.length());
            payment.setPaymentDatetime(new Date());
            payment.setReference(referenceNo);
            payment.setStatus(MerchantOrderPayment.STATUS_PROCESSING_PAYMENT);
            merchantOrderPaymentDao.update(payment);

            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            try {
                byte[] bytes = Files.readAllBytes(Paths.get(fileUpload.getAbsolutePath()));

                Path path = Paths.get(uploadPath, payment.getRenamedFilename());
                Files.write(path, bytes);
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }

            MerchantOrderShipment shipping = merchantOrderShipmentDao.getMerchantOrderShipmentByOrderId(merchantOrder.getOrderId());
            shipping.getShippingAddress().setLongAddress(shipping.getShippingAddress().getInternationalFullAddress(locale, ""));

            List<MerchantOrderItem> merchantOrderItemList = merchantOrder.getMerchantOrderItems();
            List<ProductDetail> productDetailList = new ArrayList<>();
            ProductDetail productDetail;
            for (MerchantOrderItem item : merchantOrderItemList) {
                productDetail = getMerchantOrderItemDisplay(locale, item, payment.getCurrencyCode());
                productDetailList.add(productDetail);
            }

            return getMerchantOrderDto(null, merchantOrder, payment, shipping, productDetailList);
        }
    }

    @Override
    public void doGetOrderHistory(DatagridModel<MerchantOrder> datagridModel, String memberId) {
        merchantOrderDao.findOrderHistoryForDatagrid(datagridModel, memberId);
    }

    @Override
    public MerchantOrderDto getOrderDetails(Locale locale, String orderId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Environment env = Application.lookupBean(Environment.class);
        SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);

        if (StringUtils.isBlank(orderId)) {
            throw new ValidatorException(i18n.getText("invalidOrderId", locale));
        }
        MerchantOrder merchantOrder = merchantOrderDao.get(orderId);
        if (merchantOrder == null) {
            throw new ValidatorException(i18n.getText("orderNotFound", locale));
        }

        //set content for product display
        List<MerchantOrderItem> orderItemList = merchantOrder.getMerchantOrderItems();
        List<ProductDetail> productDetailList = new ArrayList<>();
        for (MerchantOrderItem item : orderItemList) {
            ProductDetail itemDisplay = getMerchantOrderItemDisplay(locale, item, merchantOrder.getCurrencyCode());
            productDetailList.add(itemDisplay);
        }

        MerchantOrderPayment payment = merchantOrderPaymentDao.getMerchantOrderPaymentByOrderId(merchantOrder.getOrderId());

        MerchantOrderShipment shipping = merchantOrderShipmentDao.getMerchantOrderShipmentByOrderId(merchantOrder.getOrderId());
        shipping.getShippingAddress().setLongAddress(shipping.getShippingAddress().getInternationalFullAddress(locale, ""));

        MerchantOrderDto merchantOrderDto = getMerchantOrderDto(null, merchantOrder, payment, shipping, productDetailList);
        merchantOrderDto.setLogisticCompany(shipping.getLogisticCompany());
        merchantOrderDto.setTrackingNo(shipping.getTrackingNo());

        if (merchantOrder.getStatus().equalsIgnoreCase(MerchantOrder.STATUS_PENDING_PAYMENT) || merchantOrder.getStatus().equalsIgnoreCase(MerchantOrder.STATUS_PAYMENT_FAILED)) {
            merchantOrderDto.setUploadSlip(true);
            merchantOrderDto.setMaxUploadFile(1);

            SystemConfig systemConfig = systemConfigService.getDefaultSystemConfig();
            if (systemConfig != null) {
                merchantOrderDto.setPaymentBank(systemConfig.getMerchantBank());
                merchantOrderDto.setPaymentAccountName(systemConfig.getMerchantAccountName());
                merchantOrderDto.setPaymentAccountNo(systemConfig.getMerchantAccountNo());
            }
        } else if (!merchantOrder.getCurrencyCode().equalsIgnoreCase(MerchantCurrency.DEFAULT_CURRENCY)) {
            List<String> bankInSlipList = new ArrayList<>();
            bankInSlipList.add(payment.getFileUrlWithParentPath(env.getProperty("serverUrl") + "/file/merchantOrderPayment"));
            merchantOrderDto.setBankinSlipUrls(bankInSlipList);
        }

        System.out.println("DONE Insert into DTO");

        return merchantOrderDto;
    }
}
