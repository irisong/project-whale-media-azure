package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.Product;

import java.util.List;

public interface ProductDao extends BasicDao<Product, String> {
    public static final String BEAN_NAME = "productDao";

    public void findProductForDatagrid(DatagridModel<Product> datagridModel, String categoryId, String currencyCode, String status);

    public void findProductByNameForDatagrid(DatagridModel<Product> datagridModel, String name, String currencyCode, String status);

    public List<Product> findProductByCurrencyCode(String currencyCode, String categoryId, String status);
}
