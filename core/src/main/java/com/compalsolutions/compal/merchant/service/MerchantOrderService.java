package com.compalsolutions.compal.merchant.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.merchant.dto.MerchantOrderDto;
import com.compalsolutions.compal.merchant.vo.*;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

public interface MerchantOrderService {
    public static final String BEAN_NAME = "merchantOrderService";

    public MerchantOrderDto getCheckoutInfo(Locale locale, String memberId, String currencyCode, String addressId, String productId, String productDetId, int orderQty);

    public ProductDetail getProductDetailDisplay(Locale locale, ProductDetail productDetail, int orderQty, MerchantCurrency merchantCurrency);

    public ProductDetail getMerchantOrderItemDisplay(Locale locale, MerchantOrderItem item, String currencyCode);

    public MerchantOrderDto doProcessMerchantOrder(Locale locale, String memberId, String currencyCode, String addressId, String productId, String productDetId, int orderQty, BigDecimal grandTotal);

    public Pair<BigDecimal, BigDecimal> calculateTotal(ProductDetail productDetail, String currencyCode, BigDecimal productPrice, int orderQty, Address address);

    public BigDecimal calculateShippingFee(String currencyCode, double weight, int orderQty, Address address);

    public MerchantOrderDto getMerchantOrderDto(MerchantOrderDto dto, MerchantOrder merchantOrder, MerchantOrderPayment payment, MerchantOrderShipment shipping, List<ProductDetail> productDetailList);

    public MerchantOrderDto doUploadMerchantBankInSlip(Locale locale, String orderId, String referenceNo, File fileUpload, String fileUploadFileName, String fileUploadContentType);

    public void doGetOrderHistory(DatagridModel<MerchantOrder> datagridModel, String memberId);

    public MerchantOrderDto getOrderDetails(Locale locale, String orderId);
}
