package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.merchant.vo.Product;

import java.util.ArrayList;
import java.util.List;

@Component(ProductDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProductDaoImpl extends Jpa2Dao<Product, String> implements ProductDao {

    public ProductDaoImpl() {
        super(new Product(false));
    }

    @Override
    public void findProductForDatagrid(DatagridModel<Product> datagridModel, String categoryId, String currencyCode, String status) {
        List<Object> params = new ArrayList<>();

        String innerJoin = "";
        if (StringUtils.isNotBlank(categoryId)) {
            innerJoin += "INNER JOIN ProductCategoryDetails d ON d.productId = p.productId AND d.categoryId = ? ";
            params.add(categoryId);
        }

        String hql = "SELECT p FROM Product p "+innerJoin+"WHERE 1=1 ";

        if (StringUtils.isNotBlank(currencyCode)) {
            hql += "AND p.currency LIKE ? ";
            params.add("%" + currencyCode + "%");
        }
        if (StringUtils.isNotBlank(status)) {
            hql += "AND p.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public void findProductByNameForDatagrid(DatagridModel<Product> datagridModel, String name, String currencyCode, String status) {
        String hql = "FROM p IN " + Product.class + " "
                + "WHERE status = ? "
                + "AND (UPPER(productName) like ? OR UPPER(productNameCn) like ?) ";

        List<Object> params = new ArrayList<>();
        params.add(status);
        params.add("%" + name.toUpperCase() + "%");
        params.add("%" + name.toUpperCase() + "%");

        if(StringUtils.isNotBlank(currencyCode)) {
            hql += " AND currency like ? ";
            params.add("%" + currencyCode + "%");
        }


        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public List<Product> findProductByCurrencyCode(String currencyCode, String categoryId, String status) {
        List<Object> params = new ArrayList<>();

        String innerJoin = "";
        if (StringUtils.isNotBlank(categoryId)) {
            innerJoin += "INNER JOIN ProductCategoryDetails d ON d.productId = p.productId AND d.categoryId = ? ";
            params.add(categoryId);
        }

        String hql = "SELECT p FROM Product p "+innerJoin+"WHERE 1=1 ";

        if (StringUtils.isNotBlank(currencyCode)) {
            hql += "AND p.currency LIKE ? ";
            params.add("%" + currencyCode + "%");
        }
        if (StringUtils.isNotBlank(status)) {
            hql += "AND p.status = ? ";
            params.add(status);
        }

        return findQueryAsList(hql, params.toArray());
    }
}
