package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.merchant.vo.ProductPrice;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(ProductPriceDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProductPriceDaoImpl extends Jpa2Dao<ProductPrice, String> implements ProductPriceDao {

    public ProductPriceDaoImpl() {
        super(new ProductPrice(false));
    }

    @Override
    public List<ProductPrice> findProductPriceByProductDetIdByCurrencyId(String productDetId, String currencyId) {
        ProductPrice productPrice = new ProductPrice(false);
        productPrice.setProductDetId(productDetId);
        productPrice.setCurrencyId(currencyId);

        return findByExample(productPrice);
    }
}
