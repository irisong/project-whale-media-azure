package com.compalsolutions.compal.merchant.vo;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "mct_product_category")
@Access(AccessType.FIELD)
public class ProductCategory extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "category_id", unique = true, nullable = false, length = 32)
    private String categoryId;

    @ToTrim
    @Column(name = "category", nullable = false, length = 100)
    private String category;

    @ToTrim
    @Column(name = "category_cn", nullable = false, length = 100)
    private String categoryCn;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", nullable = false, length = 20)
    private String status;

    @Column(name = "sort_order", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer sortOrder;

    public ProductCategory() {
    }

    public ProductCategory(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryCn() {
        return categoryCn;
    }

    public void setCategoryCn(String categoryCn) {
        this.categoryCn = categoryCn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }
}
