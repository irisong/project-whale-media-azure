package com.compalsolutions.compal.merchant.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.*;

import java.util.List;
import java.util.Locale;

public interface ProductService {
    public static final String BEAN_NAME = "productService";

    public void doCreateMerchantSearchHistory(String memberId, String keyword);

    public List<MerchantSearchHistory> findAllMerchantSearchHistoryByMemberId(String memberId);

    public void doRemoveMerchantSearchHistory(String memberId, String keyword);

    public List<ProductCategory> getProductCategoryByStatus(String status);

    public void findProductForListing(DatagridModel<Product> datagridModel, String categoryId, String currencyCode, String status);

    public void findProductCategoryForDatagrid(DatagridModel<ProductCategory> datagridModel, String status);

    public void findProductByNameForListing(DatagridModel<Product> datagridModel, String name, String currencyCode, String status);

    public void doCreateNewProductCategory(ProductCategory productCategory_new);

    public void doUpdateProductCategory(ProductCategory productCategory_new);

    public int getNextProductCategorySortOrder();

    public ProductCategory findProductCategory(String productCategoryId);

    public void doUpdateProductCategoryStatus(String categoryId, String status);

    public void doUpdateProductCategoryStatusByCategory(String categoryId, String status);

    public List<Product> findProductByCurrencyCode(String currencyCode, String categoryId, String status);

    public List<Product> getProductInfo(Locale locale, List<Product> products, String currencyCode);

    public Product getProductDetailInfo(Locale locale, String currencyCode, String productId, String productDetId, boolean inclProductFile);

    public List<ProductPrice> findProductPriceByProductDetIdByCurrencyId(String productDetId, String currencyId);

    public ProductFile findProductFileByProductDetId(String productDetId);

    public Product findProductByProductId(String productId);

    public ProductDetail findProductDetailById(String productDetId);

    public Product doCheckProductErrorType(Locale locale, String productId, String currencyCode);

    public ProductDetail doCheckProductDetailErrorType(Locale locale, String productId, String productDetId, String currencyCode);
}