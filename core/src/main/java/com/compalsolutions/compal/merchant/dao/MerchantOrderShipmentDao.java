package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.MerchantOrderShipment;

public interface MerchantOrderShipmentDao extends BasicDao<MerchantOrderShipment, String> {
    public static final String BEAN_NAME = "merchantOrderShipmentDao";

    public MerchantOrderShipment getMerchantOrderShipmentByOrderId(String orderId);
}
