package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.ProductFile;

public interface ProductFileDao extends BasicDao<ProductFile, String> {
    public static final String BEAN_NAME = "productFileDao";

    public ProductFile findProductFileByProductDetId(String productDetId);
}
