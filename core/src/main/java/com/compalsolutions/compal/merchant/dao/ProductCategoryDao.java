package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.ProductCategory;

import java.util.List;

public interface ProductCategoryDao extends BasicDao<ProductCategory, String> {
    public static final String BEAN_NAME = "productCategoryDao";

    public List<ProductCategory> getProductCategoryByStatus(String status);

    public void findProductCategoryForDatagrid(DatagridModel<ProductCategory> datagridModel, String status);

    public List<ProductCategory> findAllActiveProductCategory();

    public ProductCategory findOneProductCategoryByName(String productCategory, String productCategoryCn);

    public int getNextProductCategorySortOrder();

    public void updateProductCategorySortOrder(String type, int sortOrder);

    public void updateProductCategoryStatusByCategory(String categoryId, String status);

}
