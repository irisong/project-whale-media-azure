package com.compalsolutions.compal.merchant.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.merchant.dao.*;
import com.compalsolutions.compal.merchant.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component(ProductService.BEAN_NAME)
public class ProductServiceImpl implements ProductService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private MerchantSearchHistoryDao merchantSearchHistoryDao;

    @Autowired
    private ProductCategoryDao productCategoryDao;

    @Autowired
    private ProductDao productDao;

    @Autowired
    private ProductDetailDao productDetailDao;

    @Autowired
    private ProductPriceDao productPriceDaoImpl;

    @Autowired
    private ProductFileDao productFileDao;

    @Override
    public void doCreateMerchantSearchHistory(String memberId, String keyword) {
        synchronized (synchronizedObject) {
            Validate.notBlank(memberId);
            Validate.notBlank(keyword);

            MerchantSearchHistory searchHistory = merchantSearchHistoryDao.findMerchantSearchHistory(memberId, keyword);

            if (searchHistory != null) {
                searchHistory.setDatetimeUpdate(new Date());
                merchantSearchHistoryDao.update(searchHistory);
            } else {
                searchHistory = new MerchantSearchHistory();
                searchHistory.setMemberId(memberId);
                searchHistory.setKeyword(keyword);
                merchantSearchHistoryDao.save(searchHistory);
            }
        }
    }

    @Override
    public List<MerchantSearchHistory> findAllMerchantSearchHistoryByMemberId(String memberId) {
        return merchantSearchHistoryDao.findAllMerchantSearchHistoryByMemberId(memberId);
    }

    @Override
    public void doRemoveMerchantSearchHistory(String memberId, String keyword) {
        if (StringUtils.isNotBlank(keyword)) {
            MerchantSearchHistory merchantSearchHistory = merchantSearchHistoryDao.findMerchantSearchHistory(memberId, keyword);

            if (merchantSearchHistory != null) {
                merchantSearchHistoryDao.delete(merchantSearchHistory);
            }
        } else {
            List<MerchantSearchHistory> merchantSearchHistoryList = merchantSearchHistoryDao.findAllMerchantSearchHistoryByMemberId(memberId);

            if (merchantSearchHistoryList.size() > 0) {
                merchantSearchHistoryDao.deleteAll(merchantSearchHistoryList);
            }
        }
    }

    @Override
    public List<ProductCategory> getProductCategoryByStatus(String status) {
        return productCategoryDao.getProductCategoryByStatus(status);
    }

    @Override
    public void findProductForListing(DatagridModel<Product> datagridModel, String categoryId, String currencyCode, String status) {
        productDao.findProductForDatagrid(datagridModel, categoryId, currencyCode, status);
    }

    @Override
    public void findProductCategoryForDatagrid(DatagridModel<ProductCategory> datagridModel, String status) {
        productCategoryDao.findProductCategoryForDatagrid(datagridModel, status);
    }

    @Override
    public void findProductByNameForListing(DatagridModel<Product> datagridModel, String name, String currencyCode, String status) {
        productDao.findProductByNameForDatagrid(datagridModel, name, currencyCode, status);
    }

    @Override
    public void doCreateNewProductCategory(ProductCategory productCategory_new) {
        ProductCategory productCategory = productCategoryDao.findOneProductCategoryByName(productCategory_new.getCategory(), productCategory_new.getCategoryCn());

        if (productCategory != null) {
            throw new ValidatorException("Category name already exists");
        } else {
            updateProductCategorySortOrder("ADD", 0, productCategory_new.getSortOrder());

            productCategory = new ProductCategory(true);
            productCategory.setStatus(Global.Status.ACTIVE);
            productCategory.setCategory(productCategory_new.getCategory());
            productCategory.setCategoryCn(productCategory_new.getCategoryCn());
            productCategory.setSortOrder(productCategory_new.getSortOrder());
            productCategoryDao.save(productCategory);
        }
    }

    @Override
    public void doUpdateProductCategory(ProductCategory productCategory_new) {
        ProductCategory productCategory = findProductCategory(productCategory_new.getCategoryId());
        if (productCategory != null) {
            ProductCategory duplicateProductCategory = productCategoryDao.findOneProductCategoryByName(productCategory_new.getCategory(), productCategory_new.getCategoryCn());
            if (duplicateProductCategory != null && !duplicateProductCategory.getCategoryId().equals(productCategory.getCategoryId())) { //not own record
                throw new ValidatorException("Category name already exists");
            } else {
                updateProductCategorySortOrder("UPDATE", productCategory.getSortOrder(), productCategory_new.getSortOrder());

                productCategory.setSortOrder(productCategory_new.getSortOrder());
                productCategory.setCategory(productCategory_new.getCategory());
                productCategory.setCategoryCn(productCategory_new.getCategoryCn());
                productCategory.setStatus(productCategory_new.getStatus());
                productCategoryDao.update(productCategory);
            }
        }
    }

    private void updateProductCategorySortOrder(String action, int oldSortOrder, int newSortOrder) {
        if (action.equalsIgnoreCase("UPDATE")) {
            if (oldSortOrder != newSortOrder) {
                productCategoryDao.updateProductCategorySortOrder("OLD", oldSortOrder);
                productCategoryDao.updateProductCategorySortOrder("NEW", newSortOrder);
            }
        } else { //ADD
            productCategoryDao.updateProductCategorySortOrder("NEW", newSortOrder);
        }
    }

    @Override
    public int getNextProductCategorySortOrder() {
        return productCategoryDao.getNextProductCategorySortOrder();
    }

    @Override
    public ProductCategory findProductCategory(String productCategoryId) {
        return productCategoryDao.get(productCategoryId);
    }

    @Override
    public void doUpdateProductCategoryStatus(String categoryId, String status) {
        ProductCategory productCategory = productCategoryDao.get(categoryId);
        if (productCategory != null) {
            ProductCategory duplicateCategory = productCategoryDao.findOneProductCategoryByName(productCategory.getCategory(), productCategory.getCategoryCn());
            if (duplicateCategory != null && !duplicateCategory.getCategoryId().equals(productCategory.getCategoryId())) { //not own record
                throw new ValidatorException("Category name already exists");
            }

            productCategory.setStatus(status);
            productCategoryDao.update(productCategory);
        }
    }

    @Override
    public void doUpdateProductCategoryStatusByCategory(String categoryId, String status) {
        productCategoryDao.updateProductCategoryStatusByCategory(categoryId, status);
    }

    @Override
    public List<Product> findProductByCurrencyCode(String currencyCode, String categoryId, String status) {
        return productDao.findProductByCurrencyCode(currencyCode, categoryId, status);
    }

    @Override
    public List<Product> getProductInfo(Locale locale, List<Product> products, String currencyCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        Environment env = Application.lookupBean(Environment.class);
        MerchantService merchantService = Application.lookupBean(MerchantService.class);

        currencyCode = StringUtils.isNotBlank(currencyCode) ? currencyCode : MerchantCurrency.DEFAULT_CURRENCY;
        MerchantCurrency merchantCurrency = merchantService.getMerchantCurrencyByCurrencyCode(currencyCode, Global.Status.ACTIVE);
        if (merchantCurrency == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }

        List<Product> productList = new ArrayList<>();
        products.forEach(productDb -> {
            Product product = new Product(false); //declare new object to avoid auto save

            for (ProductDetail productDetail : productDb.getProductDetails()) {
                if(productDetail.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                    List<ProductPrice> productPrices = productDetail.getProductPrices().stream().filter(pp -> pp.getCurrencyId().equals(merchantCurrency.getCurrencyId())).collect(Collectors.toList());
                    if (productPrices.size() > 0) {
                        ProductPrice productPrice = productPrices.get(0);

                        product.setCurrencyCode(merchantCurrency.getCurrencyCode());
                        product.setPrice(productPrice.getPrice());
                        product.setProductId(productDb.getProductId());

                        if (locale.equals(Global.LOCALE_CN)) {
                            product.setProductName(productDb.getProductNameCn());
                        } else {
                            product.setProductName(productDb.getProductName());
                        }

                        if (productDb.getProductFiles().size() > 0) {
                            ProductFile productFile = productDb.getProductFiles().get(0); //get 1st image file only
                            if (productFile != null) {
                                product.setFileUrl(productFile.getFileUrlWithParentPath(env.getProperty("serverUrl")+"/file/productFile"));
                            }
                        }

                        productList.add(product);
                        break;
                    }
                }
            }
        });

        return productList;
    }

    @Override
    public Product getProductDetailInfo(Locale locale, String currencyCode, String productId, String productDetId, boolean inclProductFile) {
        I18n i18n = Application.lookupBean(I18n.class);
        Environment env = Application.lookupBean(Environment.class);
        MerchantService merchantService = Application.lookupBean(MerchantService.class);
        String serverUrl = env.getProperty("serverUrl") + "/file/productFile";

        currencyCode = StringUtils.isNotBlank(currencyCode) ? currencyCode : MerchantCurrency.DEFAULT_CURRENCY;
        MerchantCurrency merchantCurrency = merchantService.getMerchantCurrencyByCurrencyCode(currencyCode, Global.Status.ACTIVE);
        if (merchantCurrency == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode",locale));
        }

        Product product;
        if (StringUtils.isNotBlank(productDetId)) { //edit variant in checkout
            ProductDetail productDetail = doCheckProductDetailErrorType(locale, productId, productDetId, currencyCode);
            if (StringUtils.isNotBlank(productDetail.getErrorType())) {
                product = new Product();
                product.setErrorType(productDetail.getErrorType());
                product.setErrorMsg(productDetail.getErrorMsg());
                return product;
            }

            product = productDetail.getProduct();
        } else {
            product = doCheckProductErrorType(locale, productId, currencyCode);
            if (StringUtils.isNotBlank(product.getErrorType())) {
                return product;
            }
        }

        //set Product
        if (locale.equals(Global.LOCALE_CN)) {
            product.setProductName(product.getProductNameCn());
            product.setProductDesc(product.getProductDescCn());
        }

        if (inclProductFile) {
            List<ProductFile> productFileList = product.getProductFiles();
            productFileList = productFileList.stream()
                    .filter(productFile -> StringUtils.isBlank(productFile.getProductDetId())) //get only image for product
                    .map(productFile -> {
                        productFile.setFileUrl(productFile.getFileUrlWithParentPath(serverUrl));
                        return productFile;
                    }).collect(Collectors.toList());
            product.setProductFiles(productFileList);

            product.setProductUrl(serverUrl + "?productId=" + productId); //share link
        }

        //set Product Details
        boolean variantSelected = false;
        Iterator<ProductDetail> detIterator = product.getProductDetails().iterator();
        while (detIterator.hasNext()) {
            ProductDetail productDetail = detIterator.next();

            if (!productDetail.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                detIterator.remove();
                continue;
            }

            if (locale.equals(Global.LOCALE_CN)) {
                productDetail.setVariant(productDetail.getVariantCn());
            }

            List<ProductPrice> productPrices = productDetail.getProductPrices().stream().filter(pp -> pp.getCurrencyId().equals(merchantCurrency.getCurrencyId())).collect(Collectors.toList());
            if (productPrices.size() > 0) {
                ProductPrice productPrice = productPrices.get(0);
                productDetail.setCurrencyCode(merchantCurrency.getCurrencyCode());
                productDetail.setPrice(productPrice.getPrice());

                if (!variantSelected) {
                    //(when edit, straight select variant that was selected before checkout) || (when first access variant, auto choose variant that have qty > 0)
                    if ((StringUtils.isNotBlank(productDetId) && productDetail.getProductDetId().equals(productDetId)) //for edit variant
                            || (StringUtils.isBlank(productDetId) && productDetail.getAvailableQty() > 0)) { //for select variant
                        productDetail.setSelectedVariant(true);
                        variantSelected = true;
                    }

                    //put out of availableQty checking, in case out of stock, show price of last variant
                    product.setCurrencyCode(merchantCurrency.getCurrencyCode());
                    product.setPrice(productPrice.getPrice());
                }
            }

            //get variant image
            ProductFile productDetFile = findProductFileByProductDetId(productDetail.getProductDetId());
            if (productDetFile != null) {
                productDetail.setFileUrl(productDetFile.getFileUrlWithParentPath(serverUrl));

                if (inclProductFile) {
                    //add to product image
                    productDetFile.setFileUrl(productDetFile.getFileUrlWithParentPath(serverUrl));
                    product.getProductFiles().add(productDetFile);
                }
            }
        }

        if (!variantSelected) {
            product.setOutOfStock(true);
        }

        return product;
    }

    @Override
    public List<ProductPrice> findProductPriceByProductDetIdByCurrencyId(String productDetId, String currencyId) {
        return productPriceDaoImpl.findProductPriceByProductDetIdByCurrencyId(productDetId, currencyId);
    }

    @Override
    public ProductFile findProductFileByProductDetId(String productDetId) {
        return productFileDao.findProductFileByProductDetId(productDetId);
    }

    @Override
    public Product findProductByProductId(String productId) {
        return productDao.get(productId);
    }

    @Override
    public ProductDetail findProductDetailById(String productDetId) {
        return productDetailDao.get(productDetId);
    }

//    @Override
//    public Triple<Product, String, String> doCheckProductErrorType(Locale locale, String productId, String currencyCode) {
//        I18n i18n = Application.lookupBean(I18n.class);
//        ProductService productService = Application.lookupBean(ProductService.class);
//        String errorType = Product.ERROR_PRODUCT;
//        String errorMsg;
//
//        if (StringUtils.isBlank(productId)) {
//            throw new ValidatorException(i18n.getText("invalidProductId", locale));
//        }
//        Product product = productService.findProductByProductId(productId);
//        if (product == null || (!product.getStatus().equalsIgnoreCase(Global.Status.ACTIVE))) {
//            errorMsg = i18n.getText("productHasBeenRemoved", locale);
//            return new ImmutableTriple<>(null, errorType, errorMsg);
//        } else {
//            if (StringUtils.isNotBlank(currencyCode)) {
//                if (!product.getCurrency().contains(currencyCode)) {
//                    errorMsg = i18n.getText("productDoNotExceptThisCurrency", locale, currencyCode);
//                    return new ImmutableTriple<>(null, errorType, errorMsg);
//                }
//            }
//
//            return new ImmutableTriple<>(product, null, null);
//        }
//    }

    //Remark: Please ensure this method will no return null
    @Override
    public Product doCheckProductErrorType(Locale locale, String productId, String currencyCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        ProductService productService = Application.lookupBean(ProductService.class);
        String errorType = Product.ERROR_PRODUCT;
        String errorMsg;

        if (StringUtils.isBlank(productId)) {
            throw new ValidatorException(i18n.getText("invalidProductId", locale));
        }
        Product product = productService.findProductByProductId(productId);
        if (product == null || (!product.getStatus().equalsIgnoreCase(Global.Status.ACTIVE))) {
            if (product == null) {
                product = new Product();
            }

            errorMsg = i18n.getText("productHasBeenRemoved", locale);
            product.setErrorType(errorType);
            product.setErrorMsg(errorMsg);
            return product;
        } else {
            if (StringUtils.isNotBlank(currencyCode)) {
                if (!product.getCurrency().contains(currencyCode)) {
                    errorMsg = i18n.getText("productDoNotExceptThisCurrency", locale, currencyCode);
                    product.setErrorType(errorType);
                    product.setErrorMsg(errorMsg);
                    return product;
                }
            }

            return product;
        }
    }

    @Override
    public ProductDetail doCheckProductDetailErrorType(Locale locale, String productId, String productDetId, String currencyCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        ProductService productService = Application.lookupBean(ProductService.class);

        String errorType = Product.ERROR_VARIANT;
        String errorMsg;

        //check product first, if got error, straight throw to productList
        if (StringUtils.isNotBlank(productId)) {
            Product product = doCheckProductErrorType(locale, productId, currencyCode);
            if (StringUtils.isNotBlank(product.getErrorType())) {
                ProductDetail productDetail = new ProductDetail();
                productDetail.setErrorType(product.getErrorType());
                productDetail.setErrorMsg(product.getErrorMsg());
                return productDetail;
            }
        }

        if (StringUtils.isBlank(productDetId)) {
            throw new ValidatorException(i18n.getText("invalidProductDetId", locale));
        }
        ProductDetail productDetail = productService.findProductDetailById(productDetId);
        if (productDetail == null || !productDetail.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
            //if variant deleted, product checking is a must
            if (StringUtils.isBlank(productId)) {
                throw new ValidatorException(i18n.getText("invalidProductId", locale));
            }

            if (productDetail == null) {
                productDetail = new ProductDetail();
            }
            errorMsg = i18n.getText("productVariantHasBeenRemoved", locale);
            productDetail.setErrorType(errorType);
            productDetail.setErrorMsg(errorMsg);
            return productDetail;
        } else {
            if (productDetail.getAvailableQty() == 0) {
                errorMsg = i18n.getText("outOfStock", locale);
                productDetail.setErrorType(errorType);
                productDetail.setErrorMsg(errorMsg);
                return productDetail;
            }

            return productDetail;
        }
    }
}