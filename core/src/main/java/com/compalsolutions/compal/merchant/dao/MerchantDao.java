package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.Merchant;

public interface MerchantDao extends BasicDao<Merchant, String> {
    public static final String BEAN_NAME = "merchantDao";

    public Merchant findOneMerchantByName(String name, String nameCn);

    public void findMerchantForDatagrid(DatagridModel<Merchant> datagridModel, String merchantType, String status);

    public void updateProductStatusByMerchant(String merchantId, String status);
}
