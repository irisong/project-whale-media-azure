package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.vo.MerchantOrderItem;
import com.compalsolutions.compal.merchant.vo.MerchantOrderShipment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MerchantOrderItemDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantOrderItemDaoImpl extends Jpa2Dao<MerchantOrderItem, String> implements MerchantOrderItemDao {

    public MerchantOrderItemDaoImpl() {
        super(new MerchantOrderItem(false));
    }
}
