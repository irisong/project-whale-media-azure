package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.vo.MerchantOrderShipment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MerchantOrderShipmentDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantOrderShipmentDaoImpl extends Jpa2Dao<MerchantOrderShipment, String> implements MerchantOrderShipmentDao {

    public MerchantOrderShipmentDaoImpl() {
        super(new MerchantOrderShipment(false));
    }

    @Override
    public MerchantOrderShipment getMerchantOrderShipmentByOrderId(String orderId) {
        if (StringUtils.isBlank(orderId)) {
            throw new ValidatorException("orderId can not be blank for MerchantOrderShipmentDaoImpl.getMerchantOrderShipmentByOrderId");
        }

        MerchantOrderShipment shipment = new MerchantOrderShipment(false);
        shipment.setOrderId(orderId);
        return findUnique(shipment);
    }
}
