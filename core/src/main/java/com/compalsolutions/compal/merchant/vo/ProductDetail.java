package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_product_detail")
public class ProductDetail extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "product_det_id", unique = true, nullable = false, length = 32)
    private String productDetId;

    @Column(name = "product_id", nullable = false, length = 32)
    private String productId;

    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private Product product;

    @ToTrim
    @Column(name = "variant", nullable = false, length = 100)
    private String variant;

    @ToTrim
    @Column(name = "variant_cn", nullable = false, length = 100)
    private String variantCn;

    @ToTrim
    @Column(name = "product_qty", nullable = false)
    private Integer productQty;

    @ToTrim
    @Column(name = "available_qty", nullable = false)
    private Integer availableQty;

    @Column(name = "weight", nullable = false)
    private Integer weight;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @OneToMany
    @JoinColumn(name = "product_det_id", insertable = false, updatable = false)
    private List<ProductPrice> productPrices = new ArrayList<>();

    @Transient
    private String fileUrl;

    @Transient
    private boolean selectedVariant;

    @Transient
    private String currencyCode;

    @Transient
    private BigDecimal price;

    @Transient
    private String productName;

    @Transient
    private String errorType;

    @Transient
    private String errorMsg;

    public ProductDetail() {
    }

    public ProductDetail(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getProductDetId() {
        return productDetId;
    }

    public void setProductDetId(String productDetId) {
        this.productDetId = productDetId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVariantCn() {
        return variantCn;
    }

    public void setVariantCn(String variantCn) {
        this.variantCn = variantCn;
    }

    public Integer getProductQty() {
        return productQty;
    }

    public void setProductQty(Integer productQty) {
        this.productQty = productQty;
    }

    public Integer getAvailableQty() {
        return availableQty;
    }

    public void setAvailableQty(Integer availableQty) {
        this.availableQty = availableQty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public List<ProductPrice> getProductPrices() {
        return productPrices;
    }

    public void setProductPrices(List<ProductPrice> productPrices) {
        this.productPrices = productPrices;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public boolean isSelectedVariant() {
        return selectedVariant;
    }

    public void setSelectedVariant(boolean selectedVariant) {
        this.selectedVariant = selectedVariant;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
