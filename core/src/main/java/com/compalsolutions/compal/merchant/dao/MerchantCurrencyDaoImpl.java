package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.vo.MerchantCurrency;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(MerchantCurrencyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantCurrencyDaoImpl extends Jpa2Dao<MerchantCurrency, String> implements MerchantCurrencyDao {

    public MerchantCurrencyDaoImpl() {
        super(new MerchantCurrency(false));
    }

    @Override
    public List<MerchantCurrency> getAllMerchantCurrency(String status) {
        MerchantCurrency merchantCurrency = new MerchantCurrency(false);
        if (StringUtils.isNotBlank(status)) {
            merchantCurrency.setStatus(status);
        }

        return findByExample(merchantCurrency);
    }

    @Override
    public MerchantCurrency getMerchantCurrencyByCurrencyCode(String currencyCode, String status) {
        if (StringUtils.isBlank(currencyCode)) {
            throw new ValidatorException("currencyCode can not be blank for MerchantCurrencyDaoImpl.getMerchantCurrencyByCurrencyCode");
        }

        MerchantCurrency merchantCurrency = new MerchantCurrency(false);
        merchantCurrency.setCurrencyCode(currencyCode);

        if (StringUtils.isNotBlank(status)) {
            merchantCurrency.setStatus(status);
        }

        return findUnique(merchantCurrency);
    }
}
