package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.merchant.vo.ProductDetail;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(ProductDetailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProductDetailDaoImpl extends Jpa2Dao<ProductDetail, String> implements ProductDetailDao {

    public ProductDetailDaoImpl() {
        super(new ProductDetail(false));
    }

}
