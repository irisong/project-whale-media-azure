package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_product_price")
public class ProductPrice extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "price_id", unique = true, nullable = false, length = 32)
    private String priceId;

    @Column(name = "product_det_id", nullable = false, length = 32)
    private String productDetId;

    @ManyToOne
    @JoinColumn(name = "product_det_id", insertable = false, updatable = false)
    private ProductDetail productDetail;

    @Column(name = "currency_id", nullable = false, length = 32)
    private String currencyId;

    @ManyToOne
    @JoinColumn(name = "currency_id", insertable = false, updatable = false)
    private MerchantCurrency merchantCurrency;

    @Column(name = "price", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal price;

    @Transient
    private String currencyCode;

    public ProductPrice() {
    }

    public ProductPrice(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getPriceId() {
        return priceId;
    }

    public void setPriceId(String priceId) {
        this.priceId = priceId;
    }

    public String getProductDetId() {
        return productDetId;
    }

    public void setProductDetId(String productDetId) {
        this.productDetId = productDetId;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public MerchantCurrency getMerchantCurrency() {
        return merchantCurrency;
    }

    public void setMerchantCurrency(MerchantCurrency merchantCurrency) {
        this.merchantCurrency = merchantCurrency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }
}
