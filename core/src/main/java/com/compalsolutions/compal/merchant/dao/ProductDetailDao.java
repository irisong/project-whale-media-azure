package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.ProductDetail;

public interface ProductDetailDao extends BasicDao<ProductDetail, String> {
    public static final String BEAN_NAME = "productDetailDao";
}
