package com.compalsolutions.compal.merchant.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.dao.MerchantCurrencyDao;
import com.compalsolutions.compal.merchant.dao.MerchantDao;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.merchant.vo.MerchantCurrency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@Component(MerchantService.BEAN_NAME)
public class MerchantServiceImpl implements MerchantService {

    @Autowired
    private MerchantDao merchantDao;

    @Autowired
    private MerchantCurrencyDao merchantCurrencyDao;

    @Override
    public Merchant getMerchant(String merchantId) {
        return merchantDao.get(merchantId);
    }

    @Override
    public List<MerchantCurrency> getAllMerchantCurrency(String status) {
        return merchantCurrencyDao.getAllMerchantCurrency(status);
    }

    @Override
    public MerchantCurrency getMerchantCurrencyByCurrencyCode(String currencyCode, String status) {
        return merchantCurrencyDao.getMerchantCurrencyByCurrencyCode(currencyCode, status);
    }

    @Override
    public void findMerchantForListing(DatagridModel<Merchant> datagridModel, String merchantType, String status) {
        merchantDao.findMerchantForDatagrid(datagridModel, merchantType, status);
    }

    @Override
    public void doCreateNewMerchant(Locale locale, Merchant merchant) {
        Merchant dbMerchant = merchantDao.findOneMerchantByName(merchant.getName(), merchant.getNameCn());
        if(dbMerchant != null) {
            throw new ValidatorException("Merchant name already exists");
        } else {
            dbMerchant = new Merchant(true);
            dbMerchant.setType(merchant.getType().toUpperCase());
            dbMerchant.setName(merchant.getName());
            dbMerchant.setNameCn(merchant.getNameCn());
            dbMerchant.setContactNo(merchant.getContactNo());
            dbMerchant.setEmail(merchant.getEmail());
            dbMerchant.setLicenseNo(merchant.getLicenseNo());
            merchantDao.save(dbMerchant);
        }
    }

    private static final Object synchronizedMerchantObject = new Object();

    @Override
    public void doUpdateMerchant(Locale locale, Merchant merchant) {
        synchronized (synchronizedMerchantObject) {
            Merchant dbMerchant = merchantDao.get(merchant.getMerchantId());

            if(dbMerchant != null) {
                Merchant duplicateMerchant = merchantDao.findOneMerchantByName(merchant.getName(), merchant.getNameCn());

                if (duplicateMerchant != null && !duplicateMerchant.getMerchantId().equalsIgnoreCase(merchant.getMerchantId())) { //not own record
                    throw new ValidatorException("Merchant name already exists");
                } else {
                    dbMerchant.setType(merchant.getType().toUpperCase());
                    dbMerchant.setName(merchant.getName());
                    dbMerchant.setNameCn(merchant.getNameCn());
                    dbMerchant.setContactNo(merchant.getContactNo());
                    dbMerchant.setEmail(merchant.getEmail());
                    dbMerchant.setLicenseNo(merchant.getLicenseNo());
                    dbMerchant.setStatus(merchant.getStatus());
                    merchantDao.update(dbMerchant);
                }
            }
        }
    }

    @Override
    public void doUpdateMerchantStatus(String merchantId, String status) {
        Merchant dbMerchant = merchantDao.get(merchantId);

        if (dbMerchant != null) {
            if (status.equalsIgnoreCase(Global.Status.INACTIVE)) {
                doUpdateProductStatusByMerchant(dbMerchant.getMerchantId(), status); //Inactive all product under this merchant
            }
            else { //if ACTIVE
                Merchant duplicateMerchant = merchantDao.findOneMerchantByName(dbMerchant.getName(), dbMerchant.getNameCn());

                if (duplicateMerchant != null && !duplicateMerchant.getMerchantId().equalsIgnoreCase(dbMerchant.getMerchantId())) { //not own record
                    throw new ValidatorException("Merchant name already exists");
                }
            }
            dbMerchant.setStatus(status);
            merchantDao.update(dbMerchant);
        }
    }

    @Override
    public void doUpdateProductStatusByMerchant(String merchantId, String status) {
        merchantDao.updateProductStatusByMerchant(merchantId, status);
    }

}
