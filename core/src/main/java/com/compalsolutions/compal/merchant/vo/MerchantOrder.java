package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_merchant_order")
public class MerchantOrder extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_PENDING_PAYMENT = "PENDING_PAYMENT";
    public static final String STATUS_PROCESSING_PAYMENT = "PROCESSING_PAYMENT";
    public static final String STATUS_PAID = "PAID";
    public static final String STATUS_PAYMENT_FAILED = "PAYMENT_FAILED";
    public static final String STATUS_ORDER_EXPIRED = "ORDER_EXPIRED";
    public static final String STATUS_ORDER_CANCELLED = "ORDER_CANCELLED";
    public static final String STATUS_SHIPPED = "SHIPPED";
    public static final String STATUS_REFUND = "REFUND";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "order_id", unique = true, nullable = false, length = 32)
    private String orderId;

    @ToTrim
    @Column(name = "order_ref_no", nullable = false, length = 20)
    private String orderRefNo;

    @Column(name = "member_id", length = 32, nullable = false)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false, nullable = true)
    private Member member;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "order_datetime", nullable = false)
    private Date orderDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", length = 20, nullable = false)
    private String currencyCode;

    @Column(name = "grant_total", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal grandTotal;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", nullable = false, length = 20)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiry_datetime")
    private Date expiryDatetime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "cancel_datetime")
    private Date cancelDatetime;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    private List<MerchantOrderItem> merchantOrderItems = new ArrayList<>();

    public MerchantOrder() {
    }

    public MerchantOrder(boolean defaultValue) {
        if (defaultValue) {
            status = STATUS_PENDING_PAYMENT;
            orderDatetime = new Date();
            expiryDatetime = DateUtil.addDate(orderDatetime, 1);
        }
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderRefNo() {
        return orderRefNo;
    }

    public void setOrderRefNo(String orderRefNo) {
        this.orderRefNo = orderRefNo;
    }

    public String getMemberId() {
        return memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getOrderDatetime() {
        return orderDatetime;
    }

    public void setOrderDatetime(Date orderDatetime) {
        this.orderDatetime = orderDatetime;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getExpiryDatetime() {
        return expiryDatetime;
    }

    public void setExpiryDatetime(Date expiryDatetime) {
        this.expiryDatetime = expiryDatetime;
    }

    public Date getCancelDatetime() {
        return cancelDatetime;
    }

    public void setCancelDatetime(Date cancelDatetime) {
        this.cancelDatetime = cancelDatetime;
    }

    public List<MerchantOrderItem> getMerchantOrderItems() {
        return merchantOrderItems;
    }

    public void setMerchantOrderItems(List<MerchantOrderItem> merchantOrderItems) {
        this.merchantOrderItems = merchantOrderItems;
    }
}
