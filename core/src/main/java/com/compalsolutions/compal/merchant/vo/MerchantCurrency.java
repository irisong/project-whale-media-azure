package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "mct_merchant_currency")
@Access(AccessType.FIELD)
public class MerchantCurrency extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String DEFAULT_CURRENCY = "RP";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "currency_id", unique = true, nullable = false, length = 32)
    private String currencyId;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", length = 20, nullable = false)
    private String currencyCode;

    @ToTrim
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @ToTrim
    @Column(name = "name_cn", length = 100, nullable = false)
    private String nameCn;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Column(name = "compulsory")
    private Boolean compulsory;

    public MerchantCurrency() {
    }

    public MerchantCurrency(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getCurrencyId() {
        return currencyId;
    }

    public void setCurrencyId(String currencyId) {
        this.currencyId = currencyId;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getCompulsory() {
        return compulsory;
    }

    public void setCompulsory(Boolean compulsory) {
        this.compulsory = compulsory;
    }
}
