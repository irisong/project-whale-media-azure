package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.merchant.vo.ProductFile;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(ProductFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProductFileDaoImpl extends Jpa2Dao<ProductFile, String> implements ProductFileDao {

    public ProductFileDaoImpl() {
        super(new ProductFile(false));
    }

    @Override
    public ProductFile findProductFileByProductDetId(String productDetId) {
        ProductFile productFile = new ProductFile(false);
        productFile.setProductDetId(productDetId);

        return findUnique(productFile);
    }
}
