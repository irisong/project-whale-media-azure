package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;

import com.compalsolutions.compal.merchant.vo.MerchantShippingConfig;

public interface MerchantShippingConfigDao extends BasicDao<MerchantShippingConfig, String> {
    public static final String BEAN_NAME = "merchantShippingConfigDao";

    public MerchantShippingConfig findConfigByDestination(String destinationCountry, String destination);
}
