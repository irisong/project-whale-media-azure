package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.vo.MerchantShippingConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MerchantShippingConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantShippingConfigDaoImpl extends Jpa2Dao<MerchantShippingConfig, String> implements MerchantShippingConfigDao {

    public MerchantShippingConfigDaoImpl() {
        super(new MerchantShippingConfig(false));
    }

    @Override
    public MerchantShippingConfig findConfigByDestination(String destinationCountry, String destination) {
        if (StringUtils.isBlank(destinationCountry)) {
            throw new ValidatorException("destinationCountry can not be blank for MerchantShippingConfigDaoImpl.findConfigByDestination");
        }

        MerchantShippingConfig config = new MerchantShippingConfig();
        config.setDestinationCountry(destinationCountry);

        if (StringUtils.isNotBlank(destination)) {
            config.setDestination(destination);
        }

        return findUnique(config);
    }
}
