package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.MerchantCurrency;

import java.util.List;

public interface MerchantCurrencyDao extends BasicDao<MerchantCurrency, String> {
    public static final String BEAN_NAME = "merchantCurrencyDao";

    public List<MerchantCurrency> getAllMerchantCurrency(String status);

    public MerchantCurrency getMerchantCurrencyByCurrencyCode(String currencyCode, String status);
}
