package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_merchant_order_shipment")
public class MerchantOrderShipment extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_PENDING = "PENDING";
    public static final String STATUS_SHIPPED = "SHIPPED";
    public static final String STATUS_CANCELLED = "CANCELLED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "shipping_id", unique = true, nullable = false, length = 32)
    private String shippingId;

    @Column(name = "order_id", nullable = false, length = 32)
    private String orderId;

    @ManyToOne
    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    private MerchantOrder merchantOrder;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "contactName", column = @Column(name = "recipient_name", length = 200)),
            @AttributeOverride(name = "contactNo", column = @Column(name = "recipient_contact_no", length = 100)),
            @AttributeOverride(name = "address", column = @Column(name = "shipping_address", columnDefinition = Global.ColumnDef.TEXT)),
            @AttributeOverride(name = "address2", column = @Column(name = "shipping_address2", columnDefinition = Global.ColumnDef.TEXT)),
            @AttributeOverride(name = "area", column = @Column(name = "shipping_area", columnDefinition = Global.ColumnDef.TEXT)),
            @AttributeOverride(name = "city", column = @Column(name = "shipping_city", length = 200)),
            @AttributeOverride(name = "state", column = @Column(name = "shipping_state", length = 200)),
            @AttributeOverride(name = "postcode", column = @Column(name = "shipping_postcode", length = 50)),
            @AttributeOverride(name = "countryCode", column = @Column(name = "shipping_country_code", length = 10))
    })
    private Address shippingAddress = new Address();

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ship_datetime")
    private Date shipDatetime;

    @Column(name = "shipping_fee", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal shippingFee;

    @Column(name = "logistic_company", length = 50)
    private String logisticCompany;

    @Column(name = "tracking_no", length = 50)
    private String trackingNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public MerchantOrderShipment() {
    }

    public MerchantOrderShipment(boolean defaultValue) {
        if (defaultValue) {
            status = STATUS_PENDING;
        }
    }

    public String getShippingId() {
        return shippingId;
    }

    public void setShippingId(String shippingId) {
        this.shippingId = shippingId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public MerchantOrder getMerchantOrder() {
        return merchantOrder;
    }

    public void setMerchantOrder(MerchantOrder merchantOrder) {
        this.merchantOrder = merchantOrder;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Date getShipDatetime() {
        return shipDatetime;
    }

    public void setShipDatetime(Date shipDatetime) {
        this.shipDatetime = shipDatetime;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public String getLogisticCompany() {
        return logisticCompany;
    }

    public void setLogisticCompany(String logisticCompany) {
        this.logisticCompany = logisticCompany;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
