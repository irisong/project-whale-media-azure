package com.compalsolutions.compal.merchant.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.merchant.vo.MerchantCurrency;

import java.util.List;
import java.util.Locale;

public interface MerchantService {
    public static final String BEAN_NAME = "merchantService";

    public Merchant getMerchant(String merchantId);

    public List<MerchantCurrency> getAllMerchantCurrency(String status);

    public MerchantCurrency getMerchantCurrencyByCurrencyCode(String currencyCode, String status);

    public void findMerchantForListing(DatagridModel<Merchant> datagridModel, String merchantType, String status);

    public void doCreateNewMerchant(Locale locale, Merchant merchant);

    public void doUpdateMerchant(Locale locale, Merchant merchant);

    public void doUpdateMerchantStatus(String merchantId, String status);

    public void doUpdateProductStatusByMerchant(String merchantId, String status);


}
