package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.ProductCategory;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(ProductCategoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProductCategoryDaoImpl extends Jpa2Dao<ProductCategory, String> implements ProductCategoryDao {

    public ProductCategoryDaoImpl() {
        super(new ProductCategory(false));
    }

    @Override
    public List<ProductCategory> getProductCategoryByStatus(String status) {
        ProductCategory productCategory = new ProductCategory(false);

        if (StringUtils.isNotBlank(status)) {
            productCategory.setStatus(status);
        }

        return findByExample(productCategory, "sortOrder");
    }

    @Override
    public void findProductCategoryForDatagrid(DatagridModel<ProductCategory> datagridModel, String status) {
        ProductCategory productCategory = new ProductCategory(false);

        if (StringUtils.isNotBlank(status)) {
            productCategory.setStatus(status);
        }

        findForDatagrid(datagridModel, productCategory);
    }

    @Override
    public List<ProductCategory> findAllActiveProductCategory() {
        String hql = " FROM p IN " + ProductCategory.class + " WHERE 1=1 "
                + " AND p.status = ? ORDER BY sortOrder";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

    public ProductCategory findOneProductCategoryByName(String productCategory, String productCategoryCn){
        String hql = " FROM p IN " + ProductCategory.class + " WHERE 1=1 "
                + " AND (p.category = ? OR p.categoryCn = ?) ORDER BY sortOrder";

        List<Object> params = new ArrayList<Object>();
        params.add(productCategory);
        params.add(productCategoryCn);

        return findFirst(hql, params.toArray());
    }

    @Override
    public int getNextProductCategorySortOrder() {
        String hql = "SELECT sortOrder FROM ProductCategory ORDER BY sortOrder DESC";

        Integer result = (Integer) exFindFirst(hql);
        if (result != null)
            return result.intValue()+1;

        return 0;
    }

    @Override
    public void updateProductCategorySortOrder(String type, int sortOrder) {
        boolean moveSortOrder = false;

        ProductCategory productCategory = new ProductCategory(false);
        productCategory.setSortOrder(sortOrder);

        List<ProductCategory> duplicateSortOrder = findByExample(productCategory);
        if (type.equalsIgnoreCase("NEW")) {
            if (duplicateSortOrder.size() > 0) {
                moveSortOrder = true;
            }
        } else {
            if (duplicateSortOrder.size() == 1) { //no other record having same seq
                moveSortOrder = true;
            }
        }

        if (moveSortOrder) {
            String action = "+1";
            if (type.equalsIgnoreCase("OLD")) {
                action = "-1";
            }

            String hql = "UPDATE ProductCategory SET sortOrder = sortOrder" + action + " WHERE sortOrder >= ? ";

            List<Object> params = new ArrayList<>();
            params.add(sortOrder);

            bulkUpdate(hql, params.toArray());
        }
    }

    @Override
    public void updateProductCategoryStatusByCategory(String categoryId, String status) {
        String hql = "update ProductCategory set status = ?, datetimeUpdate = now() where categoryId = ? ";

        List<Object> params = new ArrayList<>();
        params.add(status);
        params.add(categoryId);

        bulkUpdate(hql, params.toArray());
    }

}
