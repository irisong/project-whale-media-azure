package com.compalsolutions.compal.merchant.dto;

import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.merchant.vo.ProductDetail;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class MerchantOrderDto {

    private String orderId;
    private String orderRefNo;
    private String orderStatus;
    private Date orderDatetime;
    private Date expiryDatetime;
    private String remark;
    private String logisticCompany;
    private String trackingNo;
    private Address address;
    private List<ProductDetail> productDetailList;
    private String currencyCode;
    private BigDecimal orderTotal;
    private BigDecimal shippingFee;
    private BigDecimal currentBalance;
    private BigDecimal grandTotal;
    private String paymentBank;
    private String paymentAccountName;
    private String paymentAccountNo;
    private String paymentReference;
    private boolean uploadSlip;
    private int maxUploadFile;
    private List<String> bankinSlipUrls;
    private String errorType;
    private String errorMsg;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrderRefNo() {
        return orderRefNo;
    }

    public void setOrderRefNo(String orderRefNo) {
        this.orderRefNo = orderRefNo;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getOrderDatetime() {
        return orderDatetime;
    }

    public void setOrderDatetime(Date orderDatetime) {
        this.orderDatetime = orderDatetime;
    }

    public Date getExpiryDatetime() {
        return expiryDatetime;
    }

    public void setExpiryDatetime(Date expiryDatetime) {
        this.expiryDatetime = expiryDatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getLogisticCompany() {
        return logisticCompany;
    }

    public void setLogisticCompany(String logisticCompany) {
        this.logisticCompany = logisticCompany;
    }

    public String getTrackingNo() {
        return trackingNo;
    }

    public void setTrackingNo(String trackingNo) {
        this.trackingNo = trackingNo;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<ProductDetail> getProductDetailList() {
        return productDetailList;
    }

    public void setProductDetailList(List<ProductDetail> productDetailList) {
        this.productDetailList = productDetailList;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(BigDecimal orderTotal) {
        this.orderTotal = orderTotal;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(BigDecimal currentBalance) {
        this.currentBalance = currentBalance;
    }

    public BigDecimal getGrandTotal() {
        return grandTotal;
    }

    public void setGrandTotal(BigDecimal grandTotal) {
        this.grandTotal = grandTotal;
    }

    public String getPaymentBank() {
        return paymentBank;
    }

    public void setPaymentBank(String paymentBank) {
        this.paymentBank = paymentBank;
    }

    public String getPaymentAccountName() {
        return paymentAccountName;
    }

    public void setPaymentAccountName(String paymentAccountName) {
        this.paymentAccountName = paymentAccountName;
    }

    public String getPaymentAccountNo() {
        return paymentAccountNo;
    }

    public void setPaymentAccountNo(String paymentAccountNo) {
        this.paymentAccountNo = paymentAccountNo;
    }

    public String getPaymentReference() {
        return paymentReference;
    }

    public void setPaymentReference(String paymentReference) {
        this.paymentReference = paymentReference;
    }

    public boolean isUploadSlip() {
        return uploadSlip;
    }

    public void setUploadSlip(boolean uploadSlip) {
        this.uploadSlip = uploadSlip;
    }

    public int getMaxUploadFile() {
        return maxUploadFile;
    }

    public void setMaxUploadFile(int maxUploadFile) {
        this.maxUploadFile = maxUploadFile;
    }

    public List<String> getBankinSlipUrls() {
        return bankinSlipUrls;
    }

    public void setBankinSlipUrls(List<String> bankinSlipUrls) {
        this.bankinSlipUrls = bankinSlipUrls;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
