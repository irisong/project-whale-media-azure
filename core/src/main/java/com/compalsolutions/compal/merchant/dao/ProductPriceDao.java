package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.ProductPrice;

import java.util.List;

public interface ProductPriceDao extends BasicDao<ProductPrice, String> {
    public static final String BEAN_NAME = "productPriceDao";

    public List<ProductPrice> findProductPriceByProductDetIdByCurrencyId(String productDetId, String currencyId);
}
