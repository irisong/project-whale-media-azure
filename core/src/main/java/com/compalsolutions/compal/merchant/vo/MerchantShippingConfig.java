package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToLowerCase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "mct_shipping_config")
@Access(AccessType.FIELD)
public class MerchantShippingConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "config_id", unique = true, nullable = false, length = 32)
    private String configId;

    @ToTrim
    @Column(name = "logistic_company", length = 50, nullable = false)
    private String logisticCompany;

    @ToTrim
    @Column(name = "logistic_company_name", length = 100, nullable = false)
    private String logisticCompanyName;

    @ToTrim
    @Column(name = "pick_up_point", length = 100, nullable = false)
    private String pickUpPoint;

    @ToTrim
    @Column(name = "destination_country", length = 100, nullable = false)
    private String destinationCountry;

    @ToTrim
    @Column(name = "destination", length = 100, nullable = false)
    private String destination;

    @ToLowerCase
    @ToTrim
    @Column(name = "weight_unit", length = 10, nullable = false)
    private String weightUnit;

    @Column(name = "weight_from")
    private Double weightFrom;

    @Column(name = "weight_to")
    private Double weightTo;

    @Column(name = "charges", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal charges;

    @Column(name = "next_unit_charges", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal nextUnitCharges;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public MerchantShippingConfig() {
    }

    public MerchantShippingConfig(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getLogisticCompany() {
        return logisticCompany;
    }

    public void setLogisticCompany(String logisticCompany) {
        this.logisticCompany = logisticCompany;
    }

    public String getLogisticCompanyName() {
        return logisticCompanyName;
    }

    public void setLogisticCompanyName(String logisticCompanyName) {
        this.logisticCompanyName = logisticCompanyName;
    }

    public String getPickUpPoint() {
        return pickUpPoint;
    }

    public void setPickUpPoint(String pickUpPoint) {
        this.pickUpPoint = pickUpPoint;
    }

    public String getDestinationCountry() {
        return destinationCountry;
    }

    public void setDestinationCountry(String destinationCountry) {
        this.destinationCountry = destinationCountry;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getWeightUnit() {
        return weightUnit;
    }

    public void setWeightUnit(String weightUnit) {
        this.weightUnit = weightUnit;
    }

    public Double getWeightFrom() {
        return weightFrom;
    }

    public void setWeightFrom(Double weightFrom) {
        this.weightFrom = weightFrom;
    }

    public Double getWeightTo() {
        return weightTo;
    }

    public void setWeightTo(Double weightTo) {
        this.weightTo = weightTo;
    }

    public BigDecimal getCharges() {
        return charges;
    }

    public void setCharges(BigDecimal charges) {
        this.charges = charges;
    }

    public BigDecimal getNextUnitCharges() {
        return nextUnitCharges;
    }

    public void setNextUnitCharges(BigDecimal nextUnitCharges) {
        this.nextUnitCharges = nextUnitCharges;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
