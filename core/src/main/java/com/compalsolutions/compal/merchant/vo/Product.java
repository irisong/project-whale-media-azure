package com.compalsolutions.compal.merchant.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_product")
public class Product extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String ERROR_PRODUCT = "ERROR_PRODUCT"; //redirect to product list page
    public static final String ERROR_VARIANT = "ERROR_VARIANT"; //redirect to product
    public static final String ERROR_CHECKOUT = "ERROR_CHECKOUT"; //stay in checkout page

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "product_id", unique = true, nullable = false, length = 32)
    private String productId;

    @Column(name = "merchant_id", nullable = false, length = 32)
    private String merchantId;

    @ManyToOne
    @JoinColumn(name = "merchant_id", insertable = false, updatable = false)
    private Merchant merchant;

    @ToTrim
    @Column(name = "product_name", nullable = false, length = 100)
    private String productName;

    @ToTrim
    @Column(name = "product_name_cn", nullable = false, length = 100)
    private String productNameCn;

    @ToTrim
    @Column(name = "product_desc", columnDefinition = Global.ColumnDef.TEXT)
    private String productDesc;

    @ToTrim
    @Column(name = "product_desc_cn", columnDefinition = Global.ColumnDef.TEXT)
    private String productDescCn;

    @Column(name = "sensitive_item", nullable = false)
    private Boolean sensitiveItem;

    @ToTrim
    @Column(name = "ship_country", nullable = false, length = 100)
    private String shipCountry;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", nullable = false, length = 20)
    private String status;

    @ToTrim
    @Column(name = "currency", nullable = false, length = 100)
    private String currency;

    @OneToMany(fetch = FetchType.LAZY)
    @OrderBy("seq asc")
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private List<ProductFile> productFiles = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private List<ProductDetail> productDetails = new ArrayList<>();

    @Transient
    private String productUrl;

    @Transient
    private String fileUrl;

    @Transient
    private String currencyCode;

    @Transient
    private BigDecimal price;

    @Transient
    private boolean outOfStock;

    @Transient
    private String errorType;

    @Transient
    private String errorMsg;

    public Product() {
    }

    public Product(boolean defaultValue) {
        if (defaultValue) {
            sensitiveItem = true;
            status = Global.Status.ACTIVE;
        }
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNameCn() {
        return productNameCn;
    }

    public void setProductNameCn(String productNameCn) {
        this.productNameCn = productNameCn;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductDescCn() {
        return productDescCn;
    }

    public void setProductDescCn(String productDescCn) {
        this.productDescCn = productDescCn;
    }

    public Boolean getSensitiveItem() {
        return sensitiveItem;
    }

    public void setSensitiveItem(Boolean sensitiveItem) {
        this.sensitiveItem = sensitiveItem;
    }

    public String getShipCountry() {
        return shipCountry;
    }

    public void setShipCountry(String shipCountry) {
        this.shipCountry = shipCountry;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public List<ProductFile> getProductFiles() {
        return productFiles;
    }

    public void setProductFiles(List<ProductFile> productFiles) {
        this.productFiles = productFiles;
    }

    public List<ProductDetail> getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(List<ProductDetail> productDetails) {
        this.productDetails = productDetails;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean isOutOfStock() {
        return outOfStock;
    }

    public void setOutOfStock(boolean outOfStock) {
        this.outOfStock = outOfStock;
    }

    public String getErrorType() {
        return errorType;
    }

    public void setErrorType(String errorType) {
        this.errorType = errorType;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    public void setErrorMsg(String errorMsg) {
        this.errorMsg = errorMsg;
    }
}
