package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_merchant_order_item")
public class MerchantOrderItem extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "order_item_id", unique = true, nullable = false, length = 32)
    private String orderItemId;

    @Column(name = "order_id", nullable = false, length = 100)
    private String orderId;

    @ManyToOne
    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    private MerchantOrder merchantOrder;

    @Column(name = "product_det_id", nullable = false, length = 32)
    private String productDetId;

    @ToTrim
    @Column(name = "product_name", nullable = false, length = 100)
    private String productName;

    @ToTrim
    @Column(name = "product_name_cn", nullable = false, length = 100)
    private String productNameCn;

    @ToTrim
    @Column(name = "variant", nullable = false, length = 100)
    private String variant;

    @ToTrim
    @Column(name = "variant_cn", nullable = false, length = 100)
    private String variantCn;

    @ToTrim
    @Column(name = "quantity", nullable = false, length = 20)
    private Integer quantity;

    @Column(name = "amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amount;

    @Column(name = "total_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmount;

    @ToTrim
    @Column(name = "filename", length = 100)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100)
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    public MerchantOrderItem() {
    }

    public MerchantOrderItem(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getOrderItemId() {
        return orderItemId;
    }

    public void setOrderItemId(String orderItemId) {
        this.orderItemId = orderItemId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public MerchantOrder getMerchantOrder() {
        return merchantOrder;
    }

    public void setMerchantOrder(MerchantOrder merchantOrder) {
        this.merchantOrder = merchantOrder;
    }

    public String getProductDetId() {
        return productDetId;
    }

    public void setProductDetId(String productDetId) {
        this.productDetId = productDetId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNameCn() {
        return productNameCn;
    }

    public void setProductNameCn(String productNameCn) {
        this.productNameCn = productNameCn;
    }

    public String getVariant() {
        return variant;
    }

    public void setVariant(String variant) {
        this.variant = variant;
    }

    public String getVariantCn() {
        return variantCn;
    }

    public void setVariantCn(String variantCn) {
        this.variantCn = variantCn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(orderItemId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length-1];
                renamedFilename = orderItemId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath  + "/" + getRenamedFilename();
    }
}
