package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToLowerCase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "mct_merchant")
@Access(AccessType.FIELD)
public class Merchant extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String TYPE_COMPANY = "COMPANY";
    public static final String TYPE_PERSONAL = "PERSONAL";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "merchant_id", unique = true, nullable = false, length = 32)
    private String merchantId;

    @ToUpperCase
    @ToTrim
    @Column(name = "type", length = 20, nullable = false)
    private String type;

    @ToTrim
    @Column(name = "name", length = 100, nullable = false)
    private String name;

    @ToTrim
    @Column(name = "name_cn", length = 100)
    private String nameCn;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "contact_no", length = 100)
    private String contactNo;

    @ToLowerCase
    @ToTrim
    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "license_no", length = 100)
    private String licenseNo;

    public Merchant() {
    }

    public Merchant(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLicenseNo() {
        return licenseNo;
    }

    public void setLicenseNo(String licenseNo) {
        this.licenseNo = licenseNo;
    }

}
