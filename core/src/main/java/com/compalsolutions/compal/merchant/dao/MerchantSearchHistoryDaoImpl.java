package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.merchant.vo.MerchantSearchHistory;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(MerchantSearchHistoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantSearchHistoryDaoImpl extends Jpa2Dao<MerchantSearchHistory, String> implements MerchantSearchHistoryDao {

    public MerchantSearchHistoryDaoImpl() {
        super(new MerchantSearchHistory());
    }

    @Override
    public MerchantSearchHistory findMerchantSearchHistory(String memberId, String keyword) {
        MerchantSearchHistory merchantSearchHistory = new MerchantSearchHistory();
        merchantSearchHistory.setMemberId(memberId);
        merchantSearchHistory.setKeyword(keyword);

        return findUnique(merchantSearchHistory);
    }

    @Override
    public List<MerchantSearchHistory> findAllMerchantSearchHistoryByMemberId(String memberId) {
        Validate.notBlank(memberId);

        MerchantSearchHistory allHistory = new MerchantSearchHistory();
        allHistory.setMemberId(memberId);

        return  findByExample(allHistory, "datetimeUpdate desc");
    }
}
