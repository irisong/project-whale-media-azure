package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.merchant.vo.MerchantOrder;

public interface MerchantOrderDao extends BasicDao<MerchantOrder, String> {
    public static final String BEAN_NAME = "merchantOrderDao";

    public void findOrderHistoryForDatagrid(DatagridModel<MerchantOrder> datagridModel, String memberId);
}
