package com.compalsolutions.compal.merchant.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "mct_merchant_order_payment")
public class MerchantOrderPayment extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_PENDING_PAYMENT = "PENDING_PAYMENT";
    public static final String STATUS_PROCESSING_PAYMENT = "PROCESSING_PAYMENT";
    public static final String STATUS_PAID = "PAID";
    public static final String STATUS_PAYMENT_FAILED = "PAYMENT_FAILED";
    public static final String STATUS_CANCELLED = "CANCELLED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "payment_id", unique = true, nullable = false, length = 32)
    private String paymentId;

    @ToTrim
    @Column(name = "payment_ref_no", nullable = false, length = 20)
    private String paymentRefNo;

    @Column(name = "order_id", nullable = false, length = 100)
    private String orderId;

    @ManyToOne
    @JoinColumn(name = "order_id", insertable = false, updatable = false)
    private MerchantOrder merchantOrder;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "payment_datetime")
    private Date paymentDatetime;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", length = 20, nullable = false)
    private String currencyCode;

    @Column(name = "order_amount", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal orderAmount;

    @Column(name = "shipping_fee", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal shippingFee;

    @Column(name = "payment_amount", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal paymentAmount;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "reference", length = 100)
    private String reference;

    @ToTrim
    @Column(name = "filename", length = 100)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100)
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String remark;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    public MerchantOrderPayment() {
    }

    public MerchantOrderPayment(boolean defaultValue) {
        if (defaultValue) {
            status = STATUS_PENDING_PAYMENT;
            paymentDatetime = new Date();
        }
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getPaymentRefNo() {
        return paymentRefNo;
    }

    public void setPaymentRefNo(String paymentRefNo) {
        this.paymentRefNo = paymentRefNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public MerchantOrder getMerchantOrder() {
        return merchantOrder;
    }

    public void setMerchantOrder(MerchantOrder merchantOrder) {
        this.merchantOrder = merchantOrder;
    }

    public Date getPaymentDatetime() {
        return paymentDatetime;
    }

    public void setPaymentDatetime(Date paymentDatetime) {
        this.paymentDatetime = paymentDatetime;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public BigDecimal getOrderAmount() {
        return orderAmount;
    }

    public void setOrderAmount(BigDecimal orderAmount) {
        this.orderAmount = orderAmount;
    }

    public BigDecimal getShippingFee() {
        return shippingFee;
    }

    public void setShippingFee(BigDecimal shippingFee) {
        this.shippingFee = shippingFee;
    }

    public BigDecimal getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(BigDecimal paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(paymentId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length-1];
                renamedFilename = paymentId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath  + "/" + getRenamedFilename();
    }
}
