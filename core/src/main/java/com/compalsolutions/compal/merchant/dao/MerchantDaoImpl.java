package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.merchant.vo.Merchant;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(MerchantDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantDaoImpl extends Jpa2Dao<Merchant, String> implements MerchantDao {

    public MerchantDaoImpl() {
        super(new Merchant(false));
    }

    @Override
    public Merchant findOneMerchantByName(String name, String nameCn) {
        String hql = " FROM m IN " + Merchant.class + " WHERE 1=1 "
                + " AND (m.name = ? OR m.nameCn = ?)";
        List<Object> params = new ArrayList<Object>();
        params.add(name);
        params.add(nameCn);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void findMerchantForDatagrid(DatagridModel<Merchant> datagridModel, String merchantType, String status) {
        Merchant merchant = new Merchant(false);

        if (StringUtils.isNotBlank(merchantType)) {
            merchant.setType(merchantType);
        }

        if (StringUtils.isNotBlank(status)) {
            merchant.setStatus(status);
        }

        findForDatagrid(datagridModel, merchant);
    }

    @Override
    public void updateProductStatusByMerchant(String merchantId, String status) {
        String hql = "update Product set status = ?, datetimeUpdate = now() where merchantId = ? ";

        List<Object> params = new ArrayList<>();
        params.add(status);
        params.add(merchantId);

        bulkUpdate(hql, params.toArray());
    }
}
