package com.compalsolutions.compal.merchant.vo;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;

@Entity
@Table(name = "mct_product_file")
public class ProductFile extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "file_id", unique = true, nullable = false, length = 32)
    private String fileId;

    @Column(name = "product_id", nullable = false, length = 32)
    private String productId;

    @ManyToOne
    @JoinColumn(name = "product_id", insertable = false, updatable = false)
    private Product product;

    @Column(name = "product_det_id", length = 32)
    private String productDetId;

    @OneToOne
    @JoinColumn(name = "product_det_id", insertable = false, updatable = false)
    private ProductDetail productDetail;

    @Column(name = "seq", nullable = false)
    private Integer seq;

    @ToTrim
    @Column(name = "filename", nullable = false, length = 100)
    private String filename;

    @ToTrim
    @Column(name = "content_type", nullable = false, length = 100)
    private String contentType;

    @Column(name = "file_size", nullable = false)
    private Long fileSize;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    public ProductFile() {
    }

    public ProductFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getProductDetId() {
        return productDetId;
    }

    public void setProductDetId(String productDetId) {
        this.productDetId = productDetId;
    }

    public ProductDetail getProductDetail() {
        return productDetail;
    }

    public void setProductDetail(ProductDetail productDetail) {
        this.productDetail = productDetail;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length-1];
                renamedFilename = fileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }
}
