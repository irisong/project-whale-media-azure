package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.MerchantOrderPayment;

public interface MerchantOrderPaymentDao extends BasicDao<MerchantOrderPayment, String> {
    public static final String BEAN_NAME = "merchantOrderPaymentDao";

    public MerchantOrderPayment getMerchantOrderPaymentByOrderId(String orderId);
}
