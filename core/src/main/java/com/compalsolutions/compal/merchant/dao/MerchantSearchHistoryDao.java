package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.MerchantSearchHistory;

import java.util.List;

public interface MerchantSearchHistoryDao extends BasicDao<MerchantSearchHistory, String> {
    public static final String BEAN_NAME = "merchantSearchHistoryDao";

    public MerchantSearchHistory findMerchantSearchHistory(String memberId, String keyword);

    public List<MerchantSearchHistory> findAllMerchantSearchHistoryByMemberId(String memberId);

}
