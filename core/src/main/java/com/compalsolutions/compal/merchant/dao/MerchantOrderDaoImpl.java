package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.merchant.vo.MerchantOrder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(MerchantOrderDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantOrderDaoImpl extends Jpa2Dao<MerchantOrder, String> implements MerchantOrderDao {

    public MerchantOrderDaoImpl() {
        super(new MerchantOrder(false));
    }

    @Override
    public void findOrderHistoryForDatagrid(DatagridModel<MerchantOrder> datagridModel, String memberId) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be blank for MerchantOrderDaoImpl.findOrderHistoryForDatagrid");
        }

        String hql = "FROM o IN " + MerchantOrder.class + " WHERE o.memberId = ? ORDER BY o.orderDatetime DESC";
        List<Object> params = new ArrayList<>();
        params.add(memberId);

        findForDatagrid(datagridModel, "o", hql, params.toArray());
    }
}
