package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.merchant.vo.MerchantOrderItem;

public interface MerchantOrderItemDao extends BasicDao<MerchantOrderItem, String> {
    public static final String BEAN_NAME = "merchantOrderItemDao";
}
