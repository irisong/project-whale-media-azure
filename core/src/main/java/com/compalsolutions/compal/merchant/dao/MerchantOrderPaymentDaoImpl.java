package com.compalsolutions.compal.merchant.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.merchant.vo.MerchantOrderPayment;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MerchantOrderPaymentDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MerchantOrderPaymentDaoImpl extends Jpa2Dao<MerchantOrderPayment, String> implements MerchantOrderPaymentDao {

    public MerchantOrderPaymentDaoImpl() {
        super(new MerchantOrderPayment(false));
    }

    @Override
    public MerchantOrderPayment getMerchantOrderPaymentByOrderId(String orderId) {
        if (StringUtils.isBlank(orderId)) {
            throw new ValidatorException("orderId can not be blank for MerchantOrderPaymentDaoImpl.getMerchantOrderPaymentByOrderId");
        }

        MerchantOrderPayment payment = new MerchantOrderPayment(false);
        payment.setOrderId(orderId);
        return findUnique(payment);
    }
}
