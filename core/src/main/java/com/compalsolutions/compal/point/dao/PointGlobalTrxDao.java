package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.point.vo.PointGlobalTrx;

import java.util.List;


public interface PointGlobalTrxDao extends BasicDao<PointGlobalTrx, String> {
    public static final String BEAN_NAME = "pointGlobalTrxDao";

    public List<PointGlobalTrx> getAllRecord();
 }
