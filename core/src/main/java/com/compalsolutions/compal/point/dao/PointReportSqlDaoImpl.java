package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


@Component(PointReportSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointReportSqlDaoImpl extends AbstractJdbcDao implements PointReportSqlDao {

//    @Override
//    public void findPointReportForListing(DatagridModel<PointReport> datagridModel, String guildId) {
//
//        List<Object> params = new ArrayList<>();
//
//        String sql = "SELECT pr.*, m.member_id, md.profile_name, b.guild_id\n" +
//                "FROM whale_media.wl_point_report pr\n" +
//                "left join whale_media.bc_broadcast_guild_member b on pr.owner_id = b.member_id\n" +
//                "left join whale_media.mb_member m on b.member_id = m.member_id\n" +
//                "left join whale_media.mb_member_detail md on m.member_det_id = md.member_det_id " +
//                "where b.guild_id = ?";
//
//        params.add(guildId);
//
//        queryForDatagrid(datagridModel, sql, (rs, rowNum) -> {
//            PointReport pointReport = new PointReport();
//
//            pointReport.setProfileName(rs.getString("profile_name"));
//            pointReport.setInAmt(rs.getBigDecimal("in_amt"));
//            pointReport.setOutAmt(rs.getBigDecimal("out_amt"));
//            pointReport.setReportDate(rs.getString("report_date"));
//            pointReport.setReportId(rs.getString("report_id"));
//            pointReport.setMemberId(rs.getString("member_id"));//member Id will be used when they want to make withdraw request
//
//            return pointReport;
//        },params.toArray());
//    }
}
