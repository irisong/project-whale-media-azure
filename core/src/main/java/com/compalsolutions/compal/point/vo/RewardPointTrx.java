package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "wl_reward_point_trx")
@Access(AccessType.FIELD)
public class RewardPointTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "trx_id", unique = true, nullable = false, length = 32)
    private String trxId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Temporal(TemporalType.DATE)
    @Column(name = "trx_date", nullable = false)
    private Date trxDate;

    @Column(name = "reward_point", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal rewardPoint;

    @Column(name = "reward_percent", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal rewardPercent;

    @Column(name = "owner_level", length = 32, nullable = false)
    private Integer ownerLevel;

    @Column(name = "point_trx_id", length = 32)
    private String pointTrxId;

    @OneToOne
    @JoinColumn(name = "point_trx_id", insertable = false, updatable = false, nullable = true)
    private PointTrxHistory pointTrxHistory;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;


    public RewardPointTrx() {
    }

    public RewardPointTrx(boolean defaultValue) {
        if (defaultValue) {

        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        RewardPointTrx walletTrx = (RewardPointTrx) o;

        if (trxId != null ? !trxId.equals(walletTrx.trxId) : walletTrx.trxId != null)
            return false;

        return true;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    public BigDecimal getRewardPoint() {
        return rewardPoint;
    }

    public void setRewardPoint(BigDecimal rewardPoint) {
        this.rewardPoint = rewardPoint;
    }

    public BigDecimal getRewardPercent() {
        return rewardPercent;
    }

    public void setRewardPercent(BigDecimal rewardPercent) {
        this.rewardPercent = rewardPercent;
    }

    public String getPointTrxId() {
        return pointTrxId;
    }

    public void setPointTrxId(String pointTrxId) {
        this.pointTrxId = pointTrxId;
    }

    public PointTrxHistory getPointTrxHistory() {
        return pointTrxHistory;
    }

    public void setPointTrxHistory(PointTrxHistory pointTrxHistory) {
        this.pointTrxHistory = pointTrxHistory;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getOwnerLevel() {
        return ownerLevel;
    }

    public void setOwnerLevel(Integer ownerLevel) {
        this.ownerLevel = ownerLevel;
    }
}
