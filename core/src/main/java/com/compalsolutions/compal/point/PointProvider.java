package com.compalsolutions.compal.point;

public interface PointProvider {
    public static final String BEAN_NAME = "PointProvider";

    public void getListOfOwnerIdsFromPointBalance();

    public void getListOfOwnerIdsFromPointTrxForReport();

}
