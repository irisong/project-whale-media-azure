package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name = "wl_point_summary")
@Access(AccessType.FIELD)
public class PointSummary extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "summary_id", unique = true, nullable = false, length = 32)
    private String summaryId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @Column(name = "total_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmt;


    public PointSummary() {
    }

    public PointSummary(boolean defaultValue) {
        if (defaultValue) {
            totalAmt = BigDecimal.ZERO;
        }
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }
}
