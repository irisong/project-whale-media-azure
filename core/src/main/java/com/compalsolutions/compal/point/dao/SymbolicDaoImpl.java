package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.Symbolic;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(SymbolicDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SymbolicDaoImpl extends Jpa2Dao<Symbolic, String> implements SymbolicDao {

    public SymbolicDaoImpl() {
        super(new Symbolic(false));
    }

    @Override
    public List<Symbolic> findAllActiveSymbolic() {
        String hql = " FROM s IN " + Symbolic.class + " WHERE 1=1 "
                + " AND s.status = ? ORDER BY seq";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public Symbolic findOneSymbolicByName(String category, String categoryCn) {
        String hql = " FROM s IN " + Symbolic.class + " WHERE 1=1 "
                + " AND (s.category = ? OR s.categoryCn = ?) ORDER BY seq";

        List<Object> params = new ArrayList<Object>();
        params.add(category);
        params.add(categoryCn);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void findSymbolicForDatagrid(DatagridModel<Symbolic> datagridModel, String category, String categoryCn, String status) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM s IN " + Symbolic.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(category)) {
            hql += " and s.category like ? ";
            params.add("%" + category + "%");
        }

        if (StringUtils.isNotBlank(categoryCn)) {
            hql += " and s.categoryCn like ? ";
            params.add("%" + categoryCn + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and s.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "s", hql, params.toArray());
    }

    @Override
    public int getNextSymbolicSeq() {
        String hql = "SELECT seq FROM Symbolic ORDER BY seq DESC";

        Integer result = (Integer) exFindFirst(hql);
        if (result != null)
            return result.intValue()+1;

        return 0;
    }

    @Override
    public void updateSymbolicSortOrder(String type, int seq) {
        boolean moveSeq = false;

        Symbolic symbolic = new Symbolic(false);
        symbolic.setSeq(seq);

        List<Symbolic> duplicateSequence = findByExample(symbolic);
        if (type.equalsIgnoreCase("NEW")) {
            if (duplicateSequence.size() > 0) {
                moveSeq = true;
            }
        } else {
            if (duplicateSequence.size() == 1) { //no other record having same seq
                moveSeq = true;
            }
        }

        if (moveSeq) {
            String action = "+1";
            if (type.equalsIgnoreCase("OLD")) {
                action = "-1";
            }

            String hql = "UPDATE Symbolic SET seq = seq" + action + " WHERE seq >= ? ";

            List<Object> params = new ArrayList<>();
            params.add(seq);

            bulkUpdate(hql, params.toArray());
        }
    }
}
