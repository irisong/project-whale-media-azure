package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(PointReportDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointReportDaoImpl extends Jpa2Dao<PointReport, String> implements PointReportDao {

    public PointReportDaoImpl() {
        super(new PointReport(false));
    }

    @Override
    public PointReport findInReportByOwnerIdAndDate(String ownerId, Date date) {
        List<Object> params = new ArrayList<>();
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be blank for PointReport.findByOwnerIdAndDate");
        }

        if (date == null) {
            throw new ValidatorException("date can not be blank for PointReport.findByOwnerIdAndDate");
        }
        DateFormat sdf = new SimpleDateFormat("yyyy/MM");
        String reportDate = sdf.format(date);

        String hql = "From p in " + PointReport.class + " Where 1=1 ";

        hql += "AND p.ownerId = ? ";
        hql += "AND p.reportDate = ? ";
        hql += "AND p.outAmt <= ? ";

        params.add(ownerId);
        params.add(reportDate);
        params.add(BigDecimal.ZERO);

        return findFirst(hql,params.toArray());

    }

    @Override
    public void doCreditPointReport(String ownerId, BigDecimal amount, Date date, String reportDate) {
        List<Object> params = new ArrayList<>();
        String hql = "update PointReport a set a.totalAmt = a.totalAmt + ?," //
                + " a.inAmt = a.inAmt + ?"
                + " where a.ownerId = ? AND a.reportDate = ?"
                + " AND a.outAmt <= ?";//this will exclude out report

        params.add(amount);
        params.add(amount);
        params.add(ownerId);
        if (StringUtils.isNotBlank(reportDate)) {
            params.add(reportDate);
        } else {
            DateFormat sdf = new SimpleDateFormat("yyyy/MM");
            String reportDateStr = sdf.format(date);
            params.add(reportDateStr);
        }
        params.add(BigDecimal.ZERO);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public PointReport getLatestOutPointReportByMemberId(String memberId) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM p IN " + PointReport.class + " Where 1 = 1 ";//AND ";

        hql += "AND p.ownerId = ? ";
        params.add(memberId);

        hql += "AND p.outAmt > ? ";
        params.add(BigDecimal.ZERO);

        hql += "order by p.reportDate desc";

        return findFirst(hql, params.toArray());
    }

    @Override
    public BigDecimal doCalculateOutAmount(String memberId, String reportDate) {
        List<Object> params = new ArrayList<>();
        String hql = "Select sum(p.inAmt - p.outAmt) FROM PointReport p WHERE 1=1 ";

        hql += "AND p.ownerId = ? ";
        params.add(memberId);

        //only will get march report, april wont get
//        if(StringUtils.isNotBlank(lastReportDate)){
//            hql += "AND p.reportDate >= ? ";
//            params.add(lastReportDate);
//        }
        hql += "AND p.reportDate < ? ";
        params.add(reportDate);
        return (BigDecimal) exFindFirst(hql,params.toArray());
    }

    @Override
    public PointReport findOutReportByOwnerIdAndDate(String memberId, Date date) {
        List<Object> params = new ArrayList<>();
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("ownerId can not be blank for PointReport.findByOwnerIdAndDate");
        }

        if (date == null) {
            throw new ValidatorException("date can not be blank for PointReport.findByOwnerIdAndDate");
        }
        DateFormat sdf = new SimpleDateFormat("yyyy/MM");
        String reportDate = sdf.format(date);

        String hql = "From p in " + PointReport.class + " Where 1=1 ";

        hql += "AND p.ownerId = ? ";
        hql += "AND p.reportDate = ? ";
        hql += "AND p.outAmt > ? ";

        params.add(memberId);
        params.add(reportDate);
        params.add(BigDecimal.ZERO);

        return findFirst(hql,params.toArray());
    }


}
