package com.compalsolutions.compal.point.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.GiftFileUploadConfiguration;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MemberFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionRanking;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.streaming.dao.VjConnectDao;
import com.compalsolutions.compal.live.streaming.dao.VjConnectDaoImpl;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.dto.VjConnectDto;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.live.streaming.vo.VjConnect;
import com.compalsolutions.compal.member.service.FansBadgeService;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.member.service.MemberFansService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.omnicplus.OmnicplusClient;
import com.compalsolutions.compal.omnicplus.dto.CredentialsDto;
import com.compalsolutions.compal.omnicplus.dto.OmcPaymentDto;
import com.compalsolutions.compal.omnicplus.dto.OmnicplusDto;
import com.compalsolutions.compal.point.dao.*;
import com.compalsolutions.compal.point.dto.PaymentDto;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.dto.PointTrxResultDto;
import com.compalsolutions.compal.point.vo.*;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.dao.PointWalletTrxDao;
import com.compalsolutions.compal.wallet.dao.WalletBalanceDao;
import com.compalsolutions.compal.wallet.dao.WalletTrxDao;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.*;
import com.compalsolutions.compal.websocket.service.WsMessageService;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.redisson.client.RedisConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Component(PointService.BEAN_NAME)
public class PointServiceImpl implements PointService {

    private Log log = LogFactory.getLog(PointServiceImpl.class);

    private static final Object synchronizedObject = new Object();

    @Autowired
    private PointTrxDao pointTrxDao;

    @Autowired
    private PointTrxHistoryDao pointTrxHistoryDao;

    @Autowired
    private PointGlobalTrxDao pointGlobalTrxDao;

    @Autowired
    private PointTrxSqlDao pointTrxSqlDao;

    @Autowired
    private WalletBalanceDao walletBalanceDao;

    @Autowired
    private WalletTrxDao walletTrxDao;

    @Autowired
    private PointWalletTrxDao pointWalletTrxDao;

    @Autowired
    private SymbolicDao symbolicDao;

    @Autowired
    private SymbolicItemDao symbolicItemDao;

    @Autowired
    private RewardPointTrxDao rewardPointTrxDao;

    @Autowired
    private PointBalanceDao pointBalanceDao;

    @Autowired
    private PointSummaryDao pointSummaryDao;

    @Autowired
    private PointReportDao pointReportDao;

    @Autowired
    private VjConnectDao vjConnectDao;

    @Autowired
    private PointReportSqlDao pointReportSqlDao;

    @Override
    public void doCreditPointSummary(String ownerId, BigDecimal total) {
        doCreatePointSummaryIfNotExists(ownerId);
        pointSummaryDao.doCreditAvailableBalance(ownerId, total);

        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        if (redisCacheProvider.getRedisStatus()) {
            PointSummary pointSummary = doCreatePointSummaryIfNotExists(ownerId); //retake current amt to avoid redis amt not updated
            redisCacheProvider.addPointSummary(pointSummary.getOwnerId(), pointSummary.getTotalAmt()); //update/add new pointSummary record in redis
        }
    }

    @Override
    public void doCreditPointBalance(String contributorId, String ownerId, BigDecimal total) {
        doCreatePointBalanceIfNotExists(ownerId, contributorId);
        pointBalanceDao.doCreditAvailableBalance(ownerId, contributorId, total);

        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        if (redisCacheProvider.getRedisStatus()) {
//            PointBalance pointBalance = doCreatePointBalanceIfNotExists(ownerId, contributorId);
//            redisCacheProvider.addPointBalance(pointBalance); //update/add new pointBalance record in redis

            BigDecimal totalContribution = pointTrxSqlDao.getTotalPointSentByMemberId(contributorId); //retake current amt to avoid redis amt not updated
            redisCacheProvider.addContributionSummary(contributorId, totalContribution);
        }
    }

    @Override
    public void doCreditPointReport(String ownerId, BigDecimal total, Date date, String reportDate) {
        pointReportDao.doCreditPointReport(ownerId, total, date, reportDate);
    }

    @Override
    public int getLatestGifVersion() {
        int version = 0;
        List<SymbolicItem> symbolicItemList = findGifAnimation();
        if (symbolicItemList.size() > 0) {
            version = symbolicItemList.get(0).getGifVersion();
        }

        return version;
    }

    @Override
    public int getNextSymbolicSeq() {
        return symbolicDao.getNextSymbolicSeq();
    }

    @Override
    public int getNextSymbolicItemSeq(String symbolicId) {
        return symbolicItemDao.getNextSymbolicItemSeq(symbolicId);
    }

    @Override
    public void doCreateNewGiftCategory(Symbolic symbolic_new) {
        Symbolic symbolic = symbolicDao.findOneSymbolicByName(symbolic_new.getCategory(), symbolic_new.getCategoryCn());

        if (symbolic != null) {
            throw new ValidatorException("Similar category name exists");
        } else {
            updateSymbolicSortOrder("ADD", 0, symbolic_new.getSeq());

            symbolic = new Symbolic(true);
            symbolic.setType(symbolic_new.getCategory().toUpperCase());
            symbolic.setSeq(symbolic_new.getSeq());
            symbolic.setCategory(symbolic_new.getCategory());
            symbolic.setCategoryCn(symbolic_new.getCategoryCn());
            symbolicDao.save(symbolic);
        }
    }

    @Override
    public void doUpdateGiftCategory(Symbolic symbolic_new) {
        Symbolic symbolic = findSymbolic(symbolic_new.getSymbolicId());
        if (symbolic != null) {
            Symbolic duplicateSymbolic = symbolicDao.findOneSymbolicByName(symbolic_new.getCategory(), symbolic_new.getCategoryCn());
            if (duplicateSymbolic != null && !duplicateSymbolic.getSymbolicId().equalsIgnoreCase(symbolic.getSymbolicId())) { //not own record
                throw new ValidatorException("Similar category name exists");
            } else {
                updateSymbolicSortOrder("UPDATE", symbolic.getSeq(), symbolic_new.getSeq());

                symbolic.setType(symbolic_new.getCategory().toUpperCase());
                symbolic.setSeq(symbolic_new.getSeq());
                symbolic.setCategory(symbolic_new.getCategory());
                symbolic.setCategoryCn(symbolic_new.getCategoryCn());
                symbolicDao.update(symbolic);
            }
        }
    }

    private void updateSymbolicSortOrder(String action, int oldSeq, int newSeq) {
        if (action.equalsIgnoreCase("UPDATE")) {
            if (oldSeq != newSeq) {
                symbolicDao.updateSymbolicSortOrder("OLD", oldSeq);
                symbolicDao.updateSymbolicSortOrder("NEW", newSeq);
            }
        } else { //ADD
            symbolicDao.updateSymbolicSortOrder("NEW", newSeq);
        }
    }

    private void updateSymbolicItemSortOrder(String action, int oldSeq, int newSeq, String oldSymbolicId, String newSymbolicId) {
        if (action.equalsIgnoreCase("UPDATE")) {
            if (oldSeq != newSeq) {
                symbolicItemDao.updateSymbolicItemSortOrder("OLD", oldSeq, oldSymbolicId);
                symbolicItemDao.updateSymbolicItemSortOrder("NEW", newSeq, newSymbolicId);
            }
        } else { //ADD
            symbolicItemDao.updateSymbolicItemSortOrder("NEW", newSeq, newSymbolicId);
        }
    }

    @Override
    public void doUpdateSymbolicCategoryStatus(String symbolicId, String status) {
        Symbolic symbolic = symbolicDao.get(symbolicId);
        if (symbolic != null) {
            if (status.equalsIgnoreCase(Global.Status.INACTIVE)) {
                doUpdateSymbolicItemStatusByCategory(symbolic.getSymbolicId(), status); //Inactive all item under this category
            }

            symbolic.setStatus(status);
            symbolicDao.update(symbolic);
        }
    }

    @Override
    public void doUpdateSymbolicItemStatus(Locale locale, String id, String status) {
        SymbolicItem symbolicItem = symbolicItemDao.get(id);
        if (symbolicItem != null) {
            if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION) && status.equalsIgnoreCase(Global.Status.ACTIVE)) {
                symbolicItem.setGifVersion(getLatestGifVersion() + 1);
            }

            symbolicItem.setStatus(status);
            symbolicItemDao.update(symbolicItem);
        }
    }

    @Override
    public void doUpdateSymbolicItemStatusByCategory(String symbolicId, String status) {
        symbolicItemDao.updateSymbolicItemStatusByCategory(symbolicId, status);
    }

    @Override
    public List<SymbolicItem> findActiveSymbolicItems() {
        return symbolicItemDao.findAllActiveSymbolicItems();
    }

    @Override
    public List<SymbolicItem> findAllActiveSymbolicItems(String symbolicId) {
        return symbolicItemDao.findActiveItemBySymbolicId(symbolicId);
    }

    @Override
    public List<SymbolicItem> findActiveSymbolicItemsByLevel(String symbolicId, Integer level) {
//        return symbolicItemDao.findActiveSymbolicItemsByLevel(symbolicId, level);
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME,
                RedisCacheProvider.class);
        if (redisCacheProvider.getRedisStatus()) {
            try {
                return redisCacheProvider.getSymbolicItems().stream().filter(i ->
                        i.getSymbolicId().equalsIgnoreCase(symbolicId) &&
                                i.getLevel() <= level && i.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)
                )
                        .collect(Collectors.toList());
            } catch (RedisConnectionException redisEx) {
                redisCacheProvider.setRedisStatus(false);
            }
        }

        return symbolicItemDao.findActiveSymbolicItemsByLevel(symbolicId, level);
    }

    @Override
    public List<SymbolicItem> findGifAnimation() {
        return symbolicItemDao.findGifAnimation();
    }

    @Override
    public List<SymbolicItem> findGifAnimationByVersion(int version, String status) {
        return symbolicItemDao.findGifAnimationByVersion(version, status);
    }

    @Override
    public List<SymbolicItem> findCacheGifAnimationByVersion(int version, String status) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        if (redisCacheProvider.getRedisStatus()) {
            try {
                return redisCacheProvider.getSymbolicItems().stream()                // convert list to stream
                        .filter(i -> SymbolicItem.GIFT_ANIMATION.equals(i.getType())
                                && i.getGifVersion() >= version && i.getStatus().equalsIgnoreCase(status))
                        .collect(Collectors.toList());
            } catch (RedisConnectionException redisEx) {
                redisCacheProvider.setRedisStatus(false);
            }
        }
        return findGifAnimationByVersion(version, status);
    }

    @Override
    public Symbolic findSymbolic(String symbolicId) {
        return symbolicDao.get(symbolicId);
    }

    @Override
    public SymbolicItem findSymbolicItem(String id) {
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        final String fileServerUrl = config.getFullGiftParentFileUrl();

        SymbolicItem symbolicItem = symbolicItemDao.get(id);
        symbolicItem.setFileUrlWithParentPath(fileServerUrl);
        symbolicItem.setGifFileUrlWithParentPath(fileServerUrl);
        symbolicItem.setStatusDesc(i18n.getText(Global.Status.getI18nKey(symbolicItem.getStatus())));

        return symbolicItem;
    }

    @Override
    public List<SymbolicItem> findGifAnimationByGifId(String gifId) {
        return symbolicItemDao.findGifAnimationByGifId(gifId);
    }

    @Override
    public List<SymbolicItem> findSymbolicItemByGifFileName(String gifFileName) {
        return symbolicItemDao.findGifAnimationWithSameName(gifFileName);
    }

    @Override
    public void findSymbolicForListing(DatagridModel<Symbolic> datagridModel, String category, String categoryCn, String status) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        symbolicDao.findSymbolicForDatagrid(datagridModel, category, categoryCn, status);
        for (Symbolic symbolic : datagridModel.getRecords()) {
            symbolic.setStatusDesc(i18n.getText(Global.Status.getI18nKey(symbolic.getStatus())));
        }
    }

    @Override
    public void findSymbolicItemForDatagrid(DatagridModel<SymbolicItem> datagridModel, String symbolicId,
                                            String name, String nameCn, String status) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        final String fileServerUrl = config.getFullGiftParentFileUrl();

        symbolicItemDao.findSymbolicItemForDatagrid(datagridModel, symbolicId, name, nameCn, status);
        datagridModel.getRecords().forEach(gift -> {
                    gift.setFileUrlWithParentPath(fileServerUrl);
                    gift.setGifFileUrlWithParentPath(fileServerUrl);
                    gift.setStatusDesc(i18n.getText(Global.Status.getI18nKey(gift.getStatus())));
                }
        );
    }

    @Override
    public List<Symbolic> findAllActiveSymbolic() {
        return symbolicDao.findAllActiveSymbolic();
    }

    @Override
    public List<Symbolic> findAllActiveSymbolicWithItems(Integer level) {
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        final String fileServerUrl = config.getFullGiftParentFileUrl();

        List<Symbolic> list = findAllActiveSymbolic();
        if (list != null)
            list.forEach(e -> {
                List<SymbolicItem> symbolicItems = findActiveSymbolicItemsByLevel(e.getSymbolicId(), level);
                symbolicItems.forEach(s -> {
                    s.setFileUrlWithParentPath(fileServerUrl);
//                    String iconUrl = cryptoProvider.getServerUrl() + "/asset/image/emoticon/" + s.getFileName().toLowerCase() + ".png";
                    s.setImageUrl(s.getFileUrl());
                });
                e.setSymbolicItemList(symbolicItems);
            });

        return list;
    }

    @Override
    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String type, String dateRangeType, String ownerId) {
        pointTrxSqlDao.findPointTrxForDatagrid(datagridModel, ownerId, type, dateRangeType);
    }

    @Override
    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String type, String dateRangeType, String ownerId,
                                        Date dateFrom, Date dateTo, String month) {
        pointTrxSqlDao.findPointTrxForDatagrid(datagridModel, ownerId, type, dateRangeType, dateFrom, dateTo, month);
    }

    @Override
    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, boolean isGiftListing, String channelId, String contributorId) {
        if (isGiftListing) {
            pointTrxDao.findPointTransactionForDatagrid(datagridModel, channelId, contributorId);
        } else {
            pointTrxSqlDao.getPointReportSqlAndParam(datagridModel, channelId);
        }
    }

    @Override
    public PointTrx findOnePointTrxForPersonal(String ownerId, String memberId, String type, String dateRangeType, boolean isLiveStream, boolean isLiveStreamOwner) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        if (StringUtils.isBlank(type)) {
            throw new ValidatorException("Type cannot be blank");
        }
        if (StringUtils.isNotBlank(ownerId)) {
            Member member = memberService.getMember(ownerId);
            if (member == null) {
                throw new ValidatorException("Invalid OwnerId");
            }
        }
        boolean isContributor = type.equalsIgnoreCase(PointTrx.CONTRIBUTOR);
        List<PointTrx> pointTrxList = pointTrxSqlDao.findPointTrxForListing(ownerId, type, dateRangeType);
        // return last record to be display
        PointTrx pointTrx = null;
        if (isContributor && pointTrxList.size() > 0) {
            if (isLiveStream && isLiveStreamOwner) {
                pointTrx = pointTrxList.get(pointTrxList.size() - 1);
                pointTrx.setRowNum(pointTrxList.size());
                return pointTrx;
            } else {
                pointTrx = getLastRowPointResultWithMemberId(memberId, true, pointTrxList);
            }
        } else {
            pointTrx = getLastRowPointResultWithMemberId(ownerId, isLiveStream, pointTrxList);
        }
        return pointTrx;
    }

    @Override
    public BigDecimal getTotalPointReceivedByOwnerId(String ownerId, boolean localDatabase) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        if (!localDatabase && redisCacheProvider.getRedisStatus()) {
            try {
                BigDecimal pointReceived = redisCacheProvider.getPointSummaryRMap().get(ownerId);
                if (pointReceived != null && BDUtil.gZero(pointReceived)) {
                    return pointReceived;
                }
            } catch (RedisConnectionException redisEx) {
                redisCacheProvider.setRedisStatus(false);
            } catch (Exception e) {
                return pointTrxSqlDao.getTotalPointReceivedByMemberId(ownerId);
            }
        }

        return pointTrxSqlDao.getTotalPointReceivedByMemberId(ownerId);
    }

    @Override
    public BigDecimal getTotalPointSentByContributorId(String contributorId, boolean localDatabase) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        if (!localDatabase && redisCacheProvider.getRedisStatus()) {
            try {
//                BigDecimal totalContribution = redisCacheProvider.getPointBalanceRList().stream()
//                        .filter(balance -> balance.getContributorId().equals(contributorId))
//                        .map(PointBalance::getTotalAmt)
//                        .reduce(BigDecimal.ZERO, BigDecimal::add);

                BigDecimal contribution = redisCacheProvider.getContributionSummaryRMap().get(contributorId);
                if (contribution != null && BDUtil.gZero(contribution)) {
                    return contribution;
                }
            } catch (RedisConnectionException redisEx) {
                redisCacheProvider.setRedisStatus(false);
            } catch (Exception e) {
                return pointTrxSqlDao.getTotalPointSentByMemberId(contributorId);
            }
        }

        return pointTrxSqlDao.getTotalPointSentByMemberId(contributorId);
    }

    @Override
    public BigDecimal getTotalPointReceivedByChannelId(String channelId) {
        return pointTrxSqlDao.getTotalPointReceivedByChannelId(channelId);
    }

    private PointTrx getLastRowPointResultWithMemberId(String ownerId, boolean isLiveStream, List<PointTrx> pointTrxList) {
        int i = 1;
        for (PointTrx pointTrx : pointTrxList) {
            pointTrx.setRowNum(i);
            // if isPopular details, return user personal ranking
            if (isLiveStream) {
                if (ownerId.equals(pointTrx.getMemberId())) {
                    return pointTrx;
                }
                i++;
            }
        }
        return null;
    }

    @Override
    public List<String> getReportAvailableDate() {
        return pointTrxSqlDao.getReportAvailableDate();
    }

    @Override
    public List<PointBalanceDto> getOwnerIdsFromPointBalance() {
        return pointTrxSqlDao.getOwnerIdsFromPointBalance();
    }

    @Override
    public List<PointBalance> getPointBalanceDuplicateRecord(String ownerId, String contributorId) {
        return pointBalanceDao.findByOwnerIdAndContributorIdDuplicate(ownerId, contributorId);
    }

    @Override
    public void doProcessDeleteDuplicateRecord(PointBalance pointBalance) {
        pointBalanceDao.delete(pointBalance);
    }

    @Override
    public List<PointReportDto> getOwnerIdsFromPointReport() {
        return pointTrxSqlDao.getOwnerIdsFromPointReport();
    }

    @Override
    public List<String> getMemberContainsGiftPointWithinDate(Date dateFrom, Date dateTo) {
        return pointTrxSqlDao.getMemberContainsGiftPointWithinDate(dateFrom, dateTo);
    }

    @Override
    public BigDecimal getTotalGiftPointWithinDate(String ownerId, Date dateFrom, Date dateTo) {
        return pointTrxDao.getTotalGiftPointWithinDate(ownerId, dateFrom, dateTo);
    }

    @Override
    public BigDecimal getTotalGiftPoint(String ownerId) {
        return pointTrxDao.getTotalGiftPoint(ownerId);
    }

    @Override
    public void getGiftPointIncomeHistory(DatagridModel<PointReport> datagridModel, String ownerId) {
        pointTrxSqlDao.getGiftPointIncomeHistory(datagridModel, ownerId);
    }

    @Override
    public void doCreateSymbol(Symbolic symbolic) {
        Symbolic symbolicDb = new Symbolic(true);
        symbolicDb.setCategory(symbolic.getCategory());
        symbolicDb.setCategoryCn(symbolic.getCategoryCn());
        symbolicDb.setSeq(symbolic.getSeq());
        symbolicDb.setType(symbolic.getType());
        symbolicDao.save(symbolicDb);
    }

    @Override
    public void doCreateSymbolItems(SymbolicItem symbolicItem) {
        SymbolicItem symbolicItemDb = new SymbolicItem(true);
        Symbolic symbolic = symbolicDao.get(symbolicItem.getSymbolicId());
        if (symbolic != null) {
            symbolicItemDb.setSymbolicId(symbolic.getSymbolicId());
            symbolicItemDb.setSymbolic(symbolic);
            symbolicItemDb.setSeq(symbolicItem.getSeq());
            symbolicItemDb.setFilename(symbolicItem.getFilename());
            symbolicItemDb.setName(symbolicItem.getName());
            symbolicItemDb.setNameCn(symbolicItem.getNameCn());
            symbolicItemDb.setPrice(symbolicItem.getPrice());
            symbolicItemDao.save(symbolicItemDb);
        } else {
            throw new ValidatorException("category of symbol not found");

        }
    }

    @Override
    public List<PaymentDto> doGetOMCWalletPaymentMethod(BigDecimal priceInUsd, boolean isCn, String phoneno) {
        OmnicplusClient omnicplus = new OmnicplusClient();
        CurrencyService currencyService = Application.lookupBean(CurrencyService.class);
        CredentialsDto credentialsDto = new CredentialsDto();
        credentialsDto.setPhoneno(phoneno);

        String currencyCode, currencyName;
        List<PaymentDto> paymentDtos = new ArrayList<>();
        OmnicplusDto omnicplusDto = omnicplus.getUserPaymentList(credentialsDto);
//        BigDecimal myr2UsdRate = BDUtil.roundUp2dp(new BigDecimal(currencyService.findLatestCurrencyExchange(Global.WalletType.MYR, Global.WalletType.USD).getRate()));
//        BigDecimal priceInUsd = priceInMyr.divide(myr2UsdRate, 2, BigDecimal.ROUND_HALF_UP);
        if (omnicplusDto.getUserType().equalsIgnoreCase("VALID")) {
            for (OmcPaymentDto omc : omnicplusDto.getPaymentType()) {
                currencyCode = omc.getCurrencyCode().equalsIgnoreCase("OMCP") ? Global.WalletType.OMC : omc.getCurrencyCode();

                currencyName = isCn ? omc.getCurrencyNameCn() : omc.getCurrencyName();
                currencyName = currencyName.equalsIgnoreCase("OMCP") ? Global.WalletType.OMC : currencyName;

                PaymentDto paymentDto = new PaymentDto();
                paymentDto.setPaymentMethod(PointTrx.TOPUP_VIA_OMCWALLET);
                paymentDto.setAvailableBalance(omc.getAvailableBalance());
                paymentDto.setCurrencyCode(currencyCode);
                paymentDto.setCurrencyName(currencyName);
                paymentDto.setExchangeRate(omc.getExchangeRate());
                paymentDto.setPrice(BDUtil.roundUp6dp(priceInUsd.multiply(omc.getExchangeRate())));
                paymentDtos.add(paymentDto);
            }
            return paymentDtos;
        }
        return null;
    }

    private static final Object synchronizedTopUpPointObject = new Object();

    @Override
    public void doCreateTopUpPointTrxViaOmcWallet(Locale locale, String currencyCode, Integer packageAmount,
                                                  String memberId, String transactionPassword) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.class);
        CurrencyService currencyService = Application.lookupBean(CurrencyService.class);
        WalletService walletService = Application.lookupBean(WalletService.class);
        OmnicplusClient omnicplus = new OmnicplusClient();
        OmcPaymentDto omcPaymentDto = new OmcPaymentDto();
        boolean isCn = locale.equals(Global.LOCALE_CN);

        synchronized (synchronizedTopUpPointObject) {
            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (packageAmount <= 0) {
                throw new ValidatorException(i18n.getText("invalidAmount", locale));
            }

            WalletTopupPackage walletTopupPackage = walletService.getWalletTopupPackage(Global.WalletType.WALLET_80, packageAmount, Global.WalletPackageStatus.DISPLAY);

            if (walletTopupPackage == null)
                throw new ValidatorException("Invalid Package");

            BigDecimal priceInUsd = new BigDecimal(String.valueOf(walletTopupPackage.getUsdPrice()));

            if (StringUtils.isBlank(transactionPassword)) {
                throw new ValidatorException(i18n.getText("invalidTransactionPassword", locale));
            }

            currencyCode = currencyCode.equalsIgnoreCase(Global.WalletType.OMC) ? "OMCP" : currencyCode;

            omcPaymentDto.setPhoneno(member.getMemberCode());
            BigDecimal rate = currencyService.getWhaleCoinExchangeRate(Global.WalletType.MYR);
//            BigDecimal priceInMyr = BDUtil.roundUp2dp(packageAmount.multiply(rate));
//            BigDecimal myr2UsdRate = BDUtil.roundUp2dp(new BigDecimal(currencyService.findLatestCurrencyExchange(Global.WalletType.MYR, Global.WalletType.USD).getRate()));
//            BigDecimal priceInUsd = priceInMyr.divide(myr2UsdRate, 2, BigDecimal.ROUND_HALF_UP);

            omcPaymentDto.setAmount(priceInUsd);
            omcPaymentDto.setCurrencyCode(currencyCode);
            omcPaymentDto.setRefId(docNoService.doGetNextTopUpRefId());
            omcPaymentDto.setTransactionPassword(transactionPassword);

            /// testing purpose
            OmnicplusDto omnicplusDto = omnicplus.payViaOmcWalletForWhaleCoinTopUp(isCn ? "zh" : "en", omcPaymentDto);

            if (omnicplusDto.getCode() == 200) {
                WalletTrx walletTrx = new WalletTrx(true);
                walletTrx.setInAmt(new BigDecimal(String.valueOf(packageAmount)));
                walletTrx.setOutAmt(BigDecimal.ZERO);
                walletTrx.setOwnerId(memberId);
                walletTrx.setOwnerType(Global.UserType.MEMBER);
                walletTrx.setTrxDatetime(new Date());
                walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_TOPUP);
                walletTrx.setWalletRefId(omcPaymentDto.getRefId());
                walletTrx.setWalletRefno(omcPaymentDto.getDocNo());
                walletTrx.setWalletType(Global.WalletType.WALLET_80);
                walletTrx.setRemark("OMC wallet");

                // using systemLocale
                walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXViaX", packageAmount, walletTopupPackage.getFiatCurrencyAmount(), walletTrx.getWalletRefId()));
                walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXViaX", Global.LOCALE_CN, packageAmount, walletTopupPackage.getFiatCurrencyAmount(),
                        walletTrx.getWalletRefId()));

                walletTrxDao.save(walletTrx);

                walletBalanceDao.doCreditAvailableBalance(memberId, walletTrx.getOwnerType(), Global.WalletType.WALLET_80,
                        walletTrx.getInAmt());
            } else {
                throw new ValidatorException(omnicplusDto.getMessage());
            }
        }
    }

    @Override
    public void doSavePointWalletTrx(PointWalletTrx pointWalletTrx) {
        pointWalletTrxDao.save(pointWalletTrx);
    }

    @Override
    public void doProcessGiftItemAndFile(SymbolicItem symbolicItem) {
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        final String uploadPath = config.getGiftFileUploadPath();

        if (StringUtils.isNotBlank(symbolicItem.getFilename())) {
            updateSymbolicItemSortOrder("ADD", 0, symbolicItem.getSeq(), null, symbolicItem.getSymbolicId());

            SymbolicItem symbolicItemDb = new SymbolicItem(true);
            symbolicItemDb.setSymbolicId(symbolicItem.getSymbolicId());
            symbolicItemDb.setName(symbolicItem.getName());
            symbolicItemDb.setNameCn(symbolicItem.getNameCn());
            symbolicItemDb.setSeq(symbolicItem.getSeq());
            symbolicItemDb.setPrice(symbolicItem.getPrice());
            symbolicItemDb.setType(symbolicItem.getType());
            symbolicItemDb.setStatus(symbolicItem.getStatus());
            symbolicItemDb.setRemark(symbolicItem.getRemark());
            symbolicItemDb.setSendGlobalMsg(symbolicItem.getSendGlobalMsg());
            symbolicItemDb.setFilename(symbolicItem.getFilename());
            symbolicItemDb.setContentType(symbolicItem.getContentType());
            symbolicItemDb.setFileSize(symbolicItem.getFileSize());

            if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
                List<SymbolicItem> symbolicItemList;
                String gifId;
                do {
                    Random rand = new Random();
                    gifId = String.valueOf(rand.nextInt(99999999));
                    symbolicItemList = findGifAnimationByGifId(gifId);
                } while (symbolicItemList.size() > 0);

                symbolicItemDb.setGifId(gifId);
                symbolicItemDb.setGifVersion(getLatestGifVersion() + 1);
                symbolicItemDb.setGifFilename(symbolicItem.getGifFilename());
                symbolicItemDb.setGifContentType(symbolicItem.getGifContentType());
                symbolicItemDb.setGifFileSize(symbolicItem.getGifFileSize());
            }

            symbolicItemDao.save(symbolicItemDb);

            if (redisCacheProvider.getRedisStatus()) {
                try {
                    redisCacheProvider.addSymbolicItem(symbolicItemDb);
                    redisCacheProvider.updateGlobalDataByKey("gifVersion", symbolicItemDb.getGifVersion());
                } catch (Exception e) {
                    redisCacheProvider.setRedisStatus(false);
                }
            }
            // currently only production upload file to difference instance, azure blob storage
            if (isProdServer) {
                azureBlobStorageService.uploadFileToAzureBlobStorage(symbolicItemDb.getRenamedFilename(), null,
                        null, symbolicItem.getContentType(), GiftFileUploadConfiguration.FOLDER_NAME,
                        symbolicItem.getData(), symbolicItem.getFileSize());
                if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
                    byte[] gifBytes = symbolicItem.getGifData();
                    azureBlobStorageService.uploadFileToAzureBlobStorage(symbolicItemDb.getGifRenamedFilename(), null,
                            null, symbolicItem.getGifContentType(), GiftFileUploadConfiguration.FOLDER_NAME,
                            gifBytes, symbolicItemDb.getGifFileSize());
                }
            } else {
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                try {
                    byte[] bytes = symbolicItem.getData();
                    Path path = Paths.get(uploadPath, symbolicItemDb.getRenamedFilename());
                    Files.write(path, bytes);

                    if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
                        byte[] gifBytes = symbolicItem.getGifData();
                        Path gifPath = Paths.get(uploadPath, symbolicItemDb.getGifRenamedFilename());
                        Files.write(gifPath, gifBytes);
                    }
                } catch (IOException e) {
                    throw new SystemErrorException(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void doUpdateGiftItemAndFile(Locale locale, SymbolicItem symbolicItem, boolean chgImage, boolean chgGifImage) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME,
                RedisCacheProvider.class);
        final String uploadPath = config.getGiftFileUploadPath();

        SymbolicItem symbolicItemDb = findSymbolicItem(symbolicItem.getId());
        if (symbolicItemDb == null) {
            throw new ValidatorException(i18n.getText("giftNotExist", locale));
        }

        //either new or old file not exist, throw error
        if (StringUtils.isBlank(symbolicItem.getFilename()) && StringUtils.isBlank(symbolicItemDb.getFilename())) {
            throw new ValidatorException(i18n.getText("imagePngRequired", locale));
        }
        if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION) &&
                StringUtils.isBlank(symbolicItem.getGifFilename()) && StringUtils.isBlank(symbolicItemDb.getGifFilename())) {
            throw new ValidatorException(i18n.getText("imageGifRequired", locale));
        }

        String idOld = symbolicItemDb.getId();
        String gifId = symbolicItemDb.getGifId();
        int gifVersion = symbolicItemDb.getGifVersion();


        if (!isProdServer) {
            //check file directory
            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
        }

        if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
            if (chgGifImage) {
                //to get new id & latest version in doUpdateSymbolicItem
                gifId = null;
                gifVersion = 0;

                //delete old gif image
                if (isProdServer) {
                    azureBlobStorageService.deleteFileByName(GiftFileUploadConfiguration.FOLDER_NAME, symbolicItemDb.getGifRenamedFilename());
                } else {
                    File file = new File(uploadPath, symbolicItemDb.getGifRenamedFilename());
                    if (file.exists()) {
                        file.delete();
                    }
                }
            }
        } else if (symbolicItemDb.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) { //prev is animation, but now is image
            //delete old gif image
            if (isProdServer) {
                azureBlobStorageService.deleteFileByName(GiftFileUploadConfiguration.FOLDER_NAME, symbolicItemDb.getGifRenamedFilename());
            } else {
                File file = new File(uploadPath, symbolicItemDb.getGifRenamedFilename());
                if (file.exists()) {
                    file.delete();
                }
            }
        }

        //get old data before delete existing record
        symbolicItem.setStatus(symbolicItemDb.getStatus());
        symbolicItem.setGifId(gifId);
        symbolicItem.setGifVersion(gifVersion);

        /************************
         * START - UPDATE DB
         ************************/

        if (chgImage) {
            String oldImageRenamedFileName = symbolicItemDb.getRenamedFilename();

            if (!chgGifImage) {
                symbolicItem.setGifFilename(symbolicItemDb.getGifFilename());
                symbolicItem.setGifContentType(symbolicItemDb.getGifContentType());
                symbolicItem.setGifFileSize(symbolicItemDb.getGifFileSize());
            }

            updateSymbolicItemSortOrder("UPDATE", symbolicItemDb.getSeq(), symbolicItem.getSeq(), symbolicItemDb.getSymbolicId(), symbolicItem.getSymbolicId());

            //delete existing record & reinsert to get new id (incase change image, to avoid image cache in mobile)
            symbolicItemDao.delete(symbolicItemDb);
            SymbolicItem oldSymbolicItem = symbolicItemDb;

            symbolicItemDb = new SymbolicItem(true);
            symbolicItemDb = doUpdateSymbolicItem(symbolicItemDb, symbolicItem, uploadPath);
            symbolicItemDao.save(symbolicItemDb);

            if (redisCacheProvider.getRedisStatus()) {
                try {
                    redisCacheProvider.removeSymbolicItem(oldSymbolicItem);
                    redisCacheProvider.addSymbolicItem(symbolicItemDb);
                    redisCacheProvider.updateGlobalDataByKey("gifVersion", symbolicItemDb.getGifVersion());
                } catch (Exception e) {
                    redisCacheProvider.setRedisStatus(false);
                }
            }

            try {
                //delete old image
                if (isProdServer) {
                    azureBlobStorageService.deleteFileByName(GiftFileUploadConfiguration.FOLDER_NAME, oldImageRenamedFileName);
                } else {
                    File file = new File(uploadPath, oldImageRenamedFileName);
                    if (file.exists()) {
                        file.delete();
                    }
                }

                //save new image
                if (isProdServer) {
                    azureBlobStorageService.uploadFileToAzureBlobStorage(symbolicItemDb.getRenamedFilename(),
                            null, null, symbolicItem.getContentType(), GiftFileUploadConfiguration.FOLDER_NAME,
                            symbolicItem.getData(), symbolicItem.getFileSize());
                } else {
                    byte[] bytes = symbolicItem.getData();
                    Path path = Paths.get(uploadPath, symbolicItemDb.getRenamedFilename());
                    Files.write(path, bytes);
                }
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        } else { //no change image
            updateSymbolicItemSortOrder("UPDATE", symbolicItemDb.getSeq(), symbolicItem.getSeq(), symbolicItemDb.getSymbolicId(), symbolicItem.getSymbolicId());
            symbolicItemDb = doUpdateSymbolicItem(symbolicItemDb, symbolicItem, uploadPath);
            symbolicItemDao.update(symbolicItemDb);

            if (redisCacheProvider.getRedisStatus()) {
                try {
                    redisCacheProvider.updateSymbolicItem(symbolicItemDb);
                } catch (Exception e) {
                    redisCacheProvider.setRedisStatus(false);
                }
            }
        }

        pointTrxDao.updatePointSymbolicId(symbolicItemDb.getId(), idOld);
    }

    private SymbolicItem doUpdateSymbolicItem(SymbolicItem symbolicItemDb, SymbolicItem symbolicItem, String uploadPath) {
        GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        symbolicItemDb.setSymbolicId(symbolicItem.getSymbolicId());
        symbolicItemDb.setName(symbolicItem.getName());
        symbolicItemDb.setNameCn(symbolicItem.getNameCn());
        symbolicItemDb.setSeq(symbolicItem.getSeq());
        symbolicItemDb.setPrice(symbolicItem.getPrice());
        symbolicItemDb.setType(symbolicItem.getType());
        symbolicItemDb.setStatus(symbolicItem.getStatus());
        symbolicItemDb.setRemark(symbolicItem.getRemark());
        symbolicItemDb.setSendGlobalMsg(symbolicItem.getSendGlobalMsg());

        if (StringUtils.isNotBlank(symbolicItem.getFilename())) {
            symbolicItemDb.setFilename(symbolicItem.getFilename());
            symbolicItemDb.setContentType(symbolicItem.getContentType());
            symbolicItemDb.setFileSize(symbolicItem.getFileSize());
        }

        List<SymbolicItem> symbolicItemList;
        if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
            String gifId = symbolicItem.getGifId();
            if (StringUtils.isBlank(gifId)) { //if change new gif image / change from IMAGE to ANIMATION
                do {
                    Random rand = new Random();
                    gifId = String.valueOf(rand.nextInt(99999999));
                    symbolicItemList = findGifAnimationByGifId(gifId);
                } while (symbolicItemList.size() > 0);
            }

            int version = symbolicItem.getGifVersion() == null ? 0 : symbolicItem.getGifVersion();
            if (version == 0 && symbolicItemDb.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) { //if change new gif image / change from IMAGE to ANIMATION
                version = getLatestGifVersion() + 1;
            }

            symbolicItemDb.setGifId(gifId);
            symbolicItemDb.setGifVersion(version);
            symbolicItemDb.setGifFilename(symbolicItem.getGifFilename());
            symbolicItemDb.setGifContentType(symbolicItem.getGifContentType());
            symbolicItemDb.setGifFileSize(symbolicItem.getGifFileSize());

            //replace GIF image
            try {
                if (symbolicItem.getGifData() != null) {
                    symbolicItemDb.setGifRenamedFilename(null); //to get new gifRenamedFilename for new gifId
                    byte[] gifBytes = symbolicItem.getGifData();

                    if (isProdServer) {
                        azureBlobStorageService.uploadFileToAzureBlobStorage(symbolicItemDb.getGifRenamedFilename(),
                                null, null, symbolicItem.getGifContentType(), GiftFileUploadConfiguration.FOLDER_NAME,
                                gifBytes, symbolicItemDb.getGifFileSize());
                    } else {
                        Path gifPath = Paths.get(uploadPath, symbolicItemDb.getGifRenamedFilename());
                        Files.write(gifPath, gifBytes);
                    }
                }
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        } else { //needed, if change from ANIMATION to IMAGE
            symbolicItemDb.setGifId(null);
            symbolicItemDb.setGifVersion(0);
            symbolicItemDb.setGifFilename(null);
            symbolicItemDb.setGifContentType(null);
            symbolicItemDb.setGifFileSize(null);
        }

        return symbolicItemDb;
    }

    @Override
    public PointTrxResultDto doCreatePointTrx(Locale locale, String memberId, String receiverId, BigDecimal qty, String itemId) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        WalletService walletService = Application.lookupBean(WalletService.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.class);
        RankService rankService = Application.lookupBean(RankService.class);
        FansIntimacyService fansIntimacyService = Application.lookupBean(FansIntimacyService.class);
        FansBadgeService fansBadgeService = Application.lookupBean(FansBadgeService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.class);
        MemberFileUploadConfiguration memberFileUploadConfiguration = Application.lookupBean(MemberFileUploadConfiguration.class);
        PointService pointService = Application.lookupBean(PointService.class);
        WsMessageService wsMessageService = Application.lookupBean(WsMessageService.BEAN_NAME, WsMessageService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.class);
        LiveChatRoomProvider liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.class);
        VjMissionService vjMissionService = Application.lookupBean(VjMissionService.class);
        Locale localeCn = Global.LOCALE_CN;
        SimpleDateFormat sdf = new SimpleDateFormat(" E MMM dd hh:mm:ss.SSS yyyy");

        synchronized (synchronizedObject) {
            if (StringUtils.isBlank(itemId)) {
                throw new ValidatorException("Item ID must be provided");
            }

            if (StringUtils.isBlank(receiverId)) {
                throw new ValidatorException("Receiver Id must be provided");
            }

            SymbolicItem symbolicItem = symbolicItemDao.get(itemId);
            PointTrxResultDto pointTrxResultDto = new PointTrxResultDto();
            Member receiver = memberService.getMember(receiverId);
            Member member = memberService.getMember(memberId);
            LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannelByMemberId(receiver.getMemberId(),
                    true);

            /***********************
             * VALIDATION - START
             ***********************/
            int availableQuantity = qty.intValue();
            pointTrxResultDto.setSentQty(availableQuantity);
            if (BDUtil.le(qty, BigDecimal.ZERO)) {
                throw new ValidatorException(i18n.getText("invalidQuantity", locale));
            }
            if (symbolicItem == null) {
                throw new ValidatorException("Item not found");
            }
            if (receiver == null) {
                throw new ValidatorException("Invalid receiver Id");
            }
            if (member == null) {
                throw new ValidatorException("Invalid member Id");
            }
            if (liveStreamChannel == null) {
                throw new ValidatorException("Invalid channel");
            }

            WalletTypeConfig walletTypeConfigFrom = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(
                    Global.UserType.MEMBER, Global.WalletType.WC);
            if (walletTypeConfigFrom == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            // checking wallet balance
            WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId,
                    Global.UserType.MEMBER, walletTypeConfigFrom.getWalletType());

            // checking reward point balance
            WalletBalance rewardPointBalance = walletService.doCreateWalletBalanceIfNotExists(memberId,
                    Global.UserType.MEMBER, Global.WalletType.WALLET_81); //temporary using WALLET_81 , while waiting walletTypeConfigFrom.getWalletType()

            BigDecimal rewardPercent = rankService.getRewardPercentByRank(rankService.getCurrentRank(member.getTotalExp()));
            BigDecimal total = BDUtil.roundUp2dp(symbolicItem.getPrice().multiply(qty));

            if (BDUtil.g(total, walletBalance.getAvailableBalance())) {
                //   availableQuantity = (walletBalance.getAvailableBalance().divide(symbolicItem.getPrice(), 2)).intValue();
                availableQuantity = (int) (walletBalance.getAvailableBalance().doubleValue() / symbolicItem.getPrice().doubleValue());
                if (availableQuantity < 1) {
                    // direct throw exception if quantity sent is lesser than 1
                    throw new ValidatorException(i18n.getText("insufficientWalletCredit", locale));
//                    pointTrxResultDto.setSentQty(availableQuantity);
//                    pointTrxResultDto.setReturnCode("0");
//                    return pointTrxResultDto;
                } else {
                    qty = new BigDecimal(availableQuantity);
                    total = BDUtil.roundUp2dp(symbolicItem.getPrice().multiply(qty));
                    pointTrxResultDto.setReturnMsg(i18n.getText("partialGiftSent", qty));
                    pointTrxResultDto.setSentQty(availableQuantity);
                }
            }

            BigDecimal rewardPoint = BigDecimal.ZERO;
            if (BDUtil.gZero(rewardPercent)) {
                rewardPoint = BDUtil.roundUp2dp(total.multiply((rewardPercent.divide(new BigDecimal(100)))));
            }
            /***********************
             * VALIDATION - END
             ***********************/

            // contributor deduct
            walletBalanceDao.doDebitAvailableBalance(memberId, receiver.getUserRole(), Global.WalletType.WALLET_80, total);

            PointTrx pointTrx = new PointTrx(true);
            pointTrx.setAmt(qty);
            pointTrx.setPrice(symbolicItem.getPrice());
            pointTrx.setTotalAmt(total);
            pointTrx.setContributorId(memberId);
            pointTrx.setOwnerId(receiverId);
            pointTrx.setOwnerType(receiver.getUserRole());
            pointTrx.setTrxDate(new Date());
            pointTrx.setChannelId(liveStreamChannel.getChannelId());
            pointTrx.setRemark(symbolicItem.getName() + "/" + symbolicItem.getNameCn());
            pointTrx.setSymbolicId(itemId);
            pointTrxDao.save(pointTrx);

            // history will store all record, pointTrx table will be deleted on monthly basis as many access
            PointTrxHistory history = new PointTrxHistory(true);
            history.setAmt(qty);
            history.setPrice(symbolicItem.getPrice());
            history.setTotalAmt(total);
            history.setContributorId(memberId);
            history.setOwnerId(receiverId);
            history.setOwnerType(receiver.getUserRole());
            history.setTrxDate(new Date());
            history.setChannelId(liveStreamChannel.getChannelId());
            history.setRemark(symbolicItem.getName() + "/" + symbolicItem.getNameCn());
            history.setSymbolicId(itemId);
            pointTrxHistoryDao.save(history);

            // gift point will be credited to VJ's Wallet 82 (baby coin)
            walletService.doCreateWalletBalanceIfNotExists(pointTrx.getOwnerId(), Global.UserType.MEMBER, Global.WalletType.WALLET_82);
            walletBalanceDao.doCreditAvailableBalance(pointTrx.getOwnerId(), pointTrx.getOwnerType(), Global.WalletType.WALLET_82, total);

            //add point balance (link with owner(vj) and contributor(viewer))
            doCreditPointBalance(memberId, receiverId, total);

            //add point to summarized record (VJ total point)
            doCreditPointSummary(receiverId, total);

            //add monthly point Report for VJ
            pointService.doCreateCreditPointReportTypeIfNotExists(receiverId, pointTrx.getTrxDate(), null);
            pointReportDao.doCreditPointReport(receiverId, total, pointTrx.getTrxDate(), null);

            if (BDUtil.gZero(rewardPoint)) {
                RewardPointTrx rewardPointTrx = new RewardPointTrx(true);
                rewardPointTrx.setPointTrxId(history.getTrxId());
                rewardPointTrx.setRewardPoint(rewardPoint);
                rewardPointTrx.setRewardPercent(rewardPercent);
                rewardPointTrx.setTrxDate(new Date());
                rewardPointTrx.setOwnerType(receiver.getUserRole());
                rewardPointTrx.setOwnerLevel(rankService.getCurrentRank(member.getTotalExp()));
                rewardPointTrx.setRemark(Global.WalletTrxType.LIVE_STREAM_GIFT);
                rewardPointTrxDao.save(rewardPointTrx);

                // Wallet Config Type COIN_TOKEN type = POINT
                PointWalletTrx pointWalletTrx = new PointWalletTrx(true);
                pointWalletTrx.setInAmt(rewardPoint);
                pointWalletTrx.setOutAmt(BigDecimal.ZERO);
                pointWalletTrx.setOwnerId(memberId);
                pointWalletTrx.setOwnerType(member.getUserRole());
                pointWalletTrx.setTrxDatetime(new Date());
                pointWalletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_GIFT_REWARD);
                pointWalletTrx.setWalletRefId(rewardPointTrx.getTrxId());
                pointWalletTrx.setWalletType(Global.WalletType.WALLET_81);
                // using systemLocale
                pointWalletTrx.setTrxDesc(i18n.getText("rewardPointDesc", rewardPoint, rewardPointTrx.getTrxId()));
                pointWalletTrx.setTrxDescCn(i18n.getText("rewardPointDesc", localeCn, rewardPoint, rewardPointTrx.getTrxId()));

                pointWalletTrxDao.save(pointWalletTrx);
            }

            WalletTrx walletTrx = new WalletTrx(true);
            walletTrx.setInAmt(BigDecimal.ZERO);
            walletTrx.setOutAmt(pointTrx.getTotalAmt());
            walletTrx.setOwnerId(memberId);
            walletTrx.setOwnerType(member.getUserRole());
            walletTrx.setTrxDatetime(new Date());
            walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_GIFT);
            walletTrx.setWalletRefId(history.getTrxId());
            walletTrx.setWalletType(Global.WalletType.WALLET_80);

            // using systemLocale
            walletTrx.setTrxDesc(i18n.getText("sendXGiftXQuantityToX", symbolicItem.getName(), qty, receiver.getMemberDetail()
                    != null ? receiver.getMemberDetail().getWhaleliveId() : member.getMemberCode()));
            walletTrx.setTrxDescCn(i18n.getText("sendXGiftXQuantityToX", localeCn, symbolicItem.getNameCn(), qty, receiver.getMemberDetail()
                    != null ? receiver.getMemberDetail().getWhaleliveId() : member.getMemberCode()));

            walletTrxDao.save(walletTrx);

            // Point and coin are not same
//            WalletTrx walletTrxTo = new WalletTrx(true);
//            walletTrxTo.setInAmt(pointTrx.getTotalAmt());
//            walletTrxTo.setOutAmt(BigDecimal.ZERO);
//            walletTrxTo.setOwnerId(receiverId);
//            walletTrxTo.setOwnerType(Global.UserType.MEMBER);
//            walletTrxTo.setTrxDatetime(new Date());
//            walletTrxTo.setTrxType(Global.WalletTrxType.LIVE_STREAM_GIFT);
//            walletTrxTo.setWalletRefId(pointTrx.getTrxId());
//            walletTrxTo.setWalletType(Global.WalletType.WALLET_80);
//
//            // using systemLocale
//            walletTrxTo.setTrxDesc(i18n.getText("receiveXGiftXQuantityToX", symbolicItem.getName(), qty, receiver.getMemberDetail()
//                    != null ? receiver.getMemberDetail().getWhaleliveId() : member.getMemberCode()));
//            walletTrxTo.setTrxDescCn(i18n.getText("receiveXGiftXQuantityToX", localeCn, symbolicItem.getNameCn(), qty, receiver.getMemberDetail()
//                    != null ? receiver.getMemberDetail().getWhaleliveId() : member.getMemberCode()));

//            walletTrxDao.save(walletTrxTo);


            // add reward point to contributor's reward wallet
            walletBalanceDao.doCreditAvailableBalance(pointTrx.getContributorId(), pointTrx.getOwnerType(), Global.WalletType.WALLET_81, rewardPoint);
//            // receiver earn
//            walletBalanceDao.doCreditAvailableBalance(pointTrx.getOwnerId(), pointTrx.getOwnerType(), Global.WalletType.WALLET_80,
//                    pointTrx.getTotalAmt());

            //check mission
            VjMission vjMission = vjMissionService.doGetLatestVjMissionByMemberId(receiverId);
            if (vjMission != null && vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)) {
                vjMissionService.doIncreasePointForRanking(memberId, vjMission.getMissionId(), total);
            }

            //add fan intimacy value
            //add try catch to avoid other function failure

            try {
                boolean isFans = memberFansService.isMemberFans(memberId, receiverId, "");
                if (isFans) {
                    fansIntimacyService.doIncreaseFanIntimacyValue(locale, memberId, receiverId, FansIntimacyTrx.TYPE_SEND_GIFT, total);
                } else {
                    boolean isWhaleCard = findSpecialRemarkSymbol(itemId);
                    if (isWhaleCard) {
                        boolean isFanBadgeExist = StringUtils.isNotBlank(receiver.getFansBadgeName());
                        if (isFanBadgeExist) {
                            memberFansService.doAddMemberFan(locale, receiverId, memberId);
                            fansIntimacyService.doIncreaseFanIntimacyValue(locale, memberId, receiverId, FansIntimacyTrx.TYPE_FIRST_TIME_ENTRY, new BigDecimal(5));
                            fansIntimacyService.doIncreaseFanIntimacyValue(locale, memberId, receiverId, FansIntimacyTrx.TYPE_SEND_GIFT, total);
                            pointTrxResultDto.setFansBadgeName(receiver.getFansBadgeName());
                        }
                    }
                }
            } catch (Exception e) {
                log.debug("doIncreaseFanIntimacyValue Error");
            }
            System.out.println("END FAN " + sdf.format(new Date()));
            //add member experience
            rankService.doProcessMemberExperience(locale, memberId, "PointTrx", "", pointTrx.getTrxId(),
                    MemberRankTrx.SPEND, 0, total, "");
            pointTrxResultDto.setReturnMsg(i18n.getText("sendGiftSuccessfully", locale));
            pointTrxResultDto.setChannelId(liveStreamChannel.getChannelId());
            pointTrxResultDto.setReturnCode("1");
            //send latest point & fans count via websocket
            BigDecimal point = getTotalPointReceivedByOwnerId(liveStreamChannel.getMemberId(), false);
            if ((pointTrxResultDto.getFansBadgeName() != null)) { //if join fans
                wsMessageService.updateTotalPointAndFansCount(liveStreamChannel.getChannelId(), point);
            } else {
                LiveStreamDto liveStreamDto = new LiveStreamDto();
                liveStreamDto.setChannelId(liveStreamChannel.getChannelId());
                liveStreamDto.setPoint(point);
                wsMessageService.broadCastMessageLiveStreamData(liveStreamDto);
            }

            //process Vj Connect Point
            if (liveStreamChannel.getHasVjConnect()) {
                VjConnect vjConnect = liveStreamService.findVjConnectByMemberId(receiverId);
                if (vjConnect != null) {
                    if (receiverId.equals(vjConnect.getHostMemberId())) {
                        vjConnectDao.updateVjConnectTotalPoint(total, vjConnect.getConnectId(), true);

                    } else if (receiverId.equals(vjConnect.getConnectorMemberId())) {
                        vjConnectDao.updateVjConnectTotalPoint(total, vjConnect.getConnectId(), false);
                    }
                }
            }

            //send message to IM
            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(memberId, Global.AccessModule.WHALE_MEDIA);
            if (account != null) {
                liveChatRoomProvider.doSendMessageToChatRoom("", pointTrxResultDto.getChannelId(), account.getAccountId(), "", "",
                        "", "GIFT_MESSAGE", memberId, null, itemId, pointTrxResultDto.getSentQty());

                if (pointTrxResultDto.getFansBadgeName() != null) { //became fans
                    Gson contentObject = new Gson();

                    FansBadge fansBadge = fansBadgeService.getLatestFansBadgeRequested(receiverId, Global.Status.ACTIVE);

                    HashMap<String, Object> map = new HashMap<>();
                    map.put("fansBadgeId", fansBadge.getFansBadgeId());
                    map.put("fansBadgeName", fansBadge.getFansBadgeName());

                    MemberDetail memberDetail = memberService.findMemberDetailByMemberId(member.getMemberId());
                    MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

                    final String parentUrl = memberFileUploadConfiguration.getFullMemberFileParentFileUrl();
                    if (memberFile != null) {
                        memberFile.setFileUrlWithParentPath(parentUrl);
                    }

                    String senderAvatarUrl = memberFile != null ? memberFile.getFileUrl() : "";
                    String contentObjectJsonString = contentObject.toJson(map);

                    liveChatRoomProvider.doSendMessageToChatRoom("REDIRECT", pointTrxResultDto.getChannelId(), account.getAccountId(), memberDetail.getProfileName(), senderAvatarUrl,
                            contentObjectJsonString, "NEW_JOIN_FAN_ANNOUNCEMENT", receiverId, member, null, 0);
                }

                if (symbolicItem.getSendGlobalMsg() != null && symbolicItem.getSendGlobalMsg()) {
                    liveStreamService.doSendGiftAnnouncementToAllRoom(account.getAccountId(), memberId, receiverId, symbolicItem);
                }
            }
            return pointTrxResultDto;
        }
    }

    @Override
    public boolean findSpecialGift(String memberId, String receiverId, String itemId) {
        List<PointTrx> pointTrxList = pointTrxDao.findPointTransactionForSpecialGift(memberId, receiverId, itemId);
        BigDecimal total = new BigDecimal(0);
        for (int i = 0; i < pointTrxList.size(); i++) {
            PointTrx p = pointTrxList.get(i);
            total = total.add(p.getAmt());
        }

        //send 1 special gift to become fans
        return total.compareTo(new BigDecimal(1)) >= 0 ? true : false;
    }

    @Override
    public BigDecimal getTotalPointBySenderAndReceiver(String senderId, String receiverId) {
        return pointTrxDao.getTotalPointBySenderAndReceiver(senderId, receiverId);
    }

    @Override
    public boolean findSpecialRemarkSymbol(String symbolicItemId) {
        SymbolicItem symbolicItem;
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME,
                RedisCacheProvider.class);
        try {
            if (redisCacheProvider.getRedisStatus()) {
                symbolicItem = redisCacheProvider.getSymbolicItems().stream().filter(
                        s -> s.getId().equalsIgnoreCase(symbolicItemId) && s.getRemark().equalsIgnoreCase(SymbolicItem.WHALE_VIP_CARD)).findFirst().orElse(null);
            } else {
                symbolicItem = symbolicItemDao.findItemBySymbolicIdAndRemark(symbolicItemId);
            }
        } catch (RedisConnectionException redisEx) {
            redisCacheProvider.setRedisStatus(false);
            symbolicItem = symbolicItemDao.findItemBySymbolicIdAndRemark(symbolicItemId);
        }
        return (symbolicItem != null);
    }

    @Override
    public List<String> findPointTrxByMemberAndSymbolId(String memberId, String symbolicItemId) {
        return pointTrxHistoryDao.findMemberTransactionForSpecialGift(memberId, symbolicItemId);

    }

    @Override
    public String findWhaleCard() {
        SymbolicItem symbolicItem = symbolicItemDao.findWhaleCard();
        if (symbolicItem != null) {
            return symbolicItem.getId();
        }
        return "";

    }

    @Override
    public PointBalance doCreatePointBalanceIfNotExists(String ownerId, String contributorId) {
        PointBalance pointBalance = pointBalanceDao.findByOwnerIdAndContributorId(ownerId, contributorId);
        if (pointBalance == null) {
            pointBalance = new PointBalance(true);
            pointBalance.setOwnerId(ownerId);
            pointBalance.setContributorId(contributorId);
            pointBalance.setTotalAmt(BigDecimal.ZERO);
            pointBalanceDao.save(pointBalance);
        } else {
            pointBalanceDao.refresh(pointBalance);
        }

        return pointBalance;
    }

    @Override
    public PointSummary doCreatePointSummaryIfNotExists(String ownerId) {
        PointSummary pointSummary = pointSummaryDao.findByOwnerId(ownerId);
        if (pointSummary == null) {
            pointSummary = new PointSummary(true);
            pointSummary.setOwnerId(ownerId);
            pointSummary.setTotalAmt(BigDecimal.ZERO);
            pointSummaryDao.save(pointSummary);
        } else {
            pointSummaryDao.refresh(pointSummary);
        }

        return pointSummary;
    }

    @Override
    public PointReport doCreateCreditPointReportTypeIfNotExists(String ownerId, Date date, String reportDate) {
        PointReport pointReport = pointReportDao.findInReportByOwnerIdAndDate(ownerId, date);
        if (pointReport == null) {
            pointReport = new PointReport(true);
            pointReport.setOwnerId(ownerId);
            pointReport.setTotalAmt(BigDecimal.ZERO);
            pointReport.setInAmt(BigDecimal.ZERO);
            pointReport.setOutAmt(BigDecimal.ZERO);
            pointReport.setStatus(PointReport.STATUS_ACTIVE);
            pointReport.setTrxDate(DateUtil.truncateTime(date));
            if (StringUtils.isNotBlank(reportDate)) {
                pointReport.setReportDate(reportDate);
            } else {
                DateFormat sdf = new SimpleDateFormat("yyyy/MM");
                String strDate = sdf.format(pointReport.getTrxDate());
                pointReport.setReportDate(strDate);
            }
            pointReportDao.save(pointReport);
        } else {
            pointReportDao.refresh(pointReport);
        }

        return pointReport;
    }

    @Override
    public BigDecimal getPointByDate(String ownerId, Date dateFrom, Date dateTo) {
        return pointTrxHistoryDao.getPointByDate(ownerId, dateFrom, dateTo);
    }

    @Override
    public BigDecimal getPointByChannelByDate(String channelId, Date dateFrom, Date dateTo) {
        return pointTrxHistoryDao.getPointByChannelByDate(channelId, dateFrom, dateTo);
    }

    @Override
    public List<PointSummary> getAllPointSummary() {
        return pointSummaryDao.getAllPointSummary();
    }

    @Override
    public List<Map<String, BigDecimal>> getAllContributionSummary() {
        return pointTrxSqlDao.getAllContributionSummary();
    }

    @Override
    public byte[] getScaledImage(byte[] srcImg, Integer width, Integer height) {
        ByteArrayInputStream in = new ByteArrayInputStream(srcImg);
        try {
            BufferedImage img = ImageIO.read(in);
            if (height == 0) {
                height = (width * img.getHeight()) / img.getWidth();
            }
            if (width == 0) {
                width = (height * img.getWidth()) / img.getHeight();
            }
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "png", buffer);
            buffer.close();

            return buffer.toByteArray();
        } catch (IOException e) {
            throw new ValidatorException("Image cannot be save.");
        }
    }

    @Override
    public PointReport getLatestOutPointReportByMemberId(String memberId) {
        return pointReportDao.getLatestOutPointReportByMemberId(memberId);
    }

    @Override
    public BigDecimal doCalculateOutAmount(String memberId, String reportDate) {
        return pointReportDao.doCalculateOutAmount(memberId, reportDate);
    }

    @Override
    public PointReport doCreateOutPointReportIfNotExist(String memberId, Date date, String reportDate, BigDecimal totalOutAmt) {
        PointReport pointReport = pointReportDao.findOutReportByOwnerIdAndDate(memberId, date);
        if (pointReport == null) {
            pointReport = new PointReport(true);
            pointReport.setOwnerId(memberId);
            pointReport.setTotalAmt(totalOutAmt);
            pointReport.setOutAmt(totalOutAmt);
            pointReport.setInAmt(BigDecimal.ZERO);
            pointReport.setStatus(PointReport.STATUS_ACTIVE);
            pointReport.setTrxDate(DateUtil.truncateTime(date));

            if (StringUtils.isNotBlank(reportDate)) {
                pointReport.setReportDate(reportDate);
            } else {
                DateFormat sdf = new SimpleDateFormat("yyyy/MM");
                String strDate = sdf.format(pointReport.getTrxDate());
                pointReport.setReportDate(strDate);
            }

            pointReportDao.save(pointReport);
        } else {
            pointReportDao.refresh(pointReport);
        }

        return pointReport;
    }

//    @Override
//    public void doDebitPointReport(String ownerId, BigDecimal outAmt, Date date, String reportDate) {
//        pointReportDao.doDebitPointReport(ownerId, outAmt, date, reportDate);
//    }

    //    public void findPointReportForListing(DatagridModel<PointReport> datagridModel, String guildId) {
//        pointReportSqlDao.findPointReportForListing(datagridModel, guildId);
//    }

    @Override
    public List<PointGlobalTrx> getPointGlobalTrx() {
        return pointGlobalTrxDao.getAllRecord();
    }

    @Override
    public void doRemovePointGlobalRecord(PointGlobalTrx pointGlobalTrx) {
        pointGlobalTrxDao.delete(pointGlobalTrx);
    }

    @Override
    public void doSavePointGlobal(PointGlobalTrx pointGlobalTrx) {
        pointGlobalTrxDao.save(pointGlobalTrx);
    }

    @Override
    public void doProcessGlobalGift(RunTaskLogger logger) {
        LiveChatRoomProvider liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.BEAN_NAME,
                LiveChatRoomProvider.class);
        getPointGlobalTrx().forEach(e -> {
            try {
                liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", e.getChannelId(), e.getAccountId(), "SYSTEM", "",
                        e.getContent(), "ANNOUNCEMENT", "", null, "", 0);
                doRemovePointGlobalRecord(e);
            } catch (Exception ex) {
                doRemovePointGlobalRecord(e);
            }
        });
    }
}
