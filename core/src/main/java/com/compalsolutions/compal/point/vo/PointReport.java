package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


@Entity
@Table(name = "wl_point_report")
@Access(AccessType.FIELD)
public class PointReport extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_ACTIVE = "ACTIVE";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "report_id", unique = true, nullable = false, length = 32)
    private String reportId;

    @Temporal(TemporalType.DATE)
    @Column(name = "trx_date", nullable = false)
    private Date trxDate;

    @Column(name = "report_date", length = 15)
    private String reportDate;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @Column(name = "total_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmt;

    @Column(name = "out_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal outAmt;

    @Column(name = "in_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal inAmt;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20)
    private String status;

    @Transient
    private String date;

    public PointReport() {
    }

    public PointReport(boolean defaultValue) {
        if (defaultValue) {
            totalAmt = BigDecimal.ZERO;
            inAmt = BigDecimal.ZERO;
            outAmt = BigDecimal.ZERO;
        }
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public BigDecimal getOutAmt() {
        return outAmt;
    }

    public void setOutAmt(BigDecimal outAmt) {
        this.outAmt = outAmt;
    }

    public BigDecimal getInAmt() {
        return inAmt;
    }

    public void setInAmt(BigDecimal inAmt) {
        this.inAmt = inAmt;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
