package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.vo.PointTrxHistory;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component(PointTrxHistoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointTrxHistoryDaoImpl extends Jpa2Dao<PointTrxHistory, String> implements PointTrxHistoryDao {
    public PointTrxHistoryDaoImpl() {
        super(new PointTrxHistory(false));
    }


    @Override
    public List<String> findMemberTransactionForSpecialGift(String receiverId, String itemId) {
        String hql = "SELECT distinct p.contributorId FROM p IN " + PointTrxHistory.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(receiverId)) {
            hql += " and p.ownerId = '"+receiverId+"' ";

        }

        if (StringUtils.isNotBlank(itemId)) {
            hql += " and p.symbolicId = '"+itemId+ "'";

        }
        List<String> memberList = new ArrayList<String>();
        List<Object> results = (List<Object>) exFindQueryAsList(hql);
        for (Object r : results) {
            memberList.add(new String(r.toString()));
        }
        return memberList;
    }

    @Override
    public BigDecimal getPointByChannelByDate(String channelId, Date dateFrom, Date dateTo) {
        if (StringUtils.isBlank(channelId)) {
            throw new ValidatorException("channelId can not be blank for PointTrxHistoryDaoImpl.getPointByChannelByDate");
        }

        List<Object> params = new ArrayList<>();

        String hql = "select sum(d.totalAmt) from PointTrxHistory d where d.channelId=? and d.trxDate >= ? " +
                " and d.trxDate <= ?";
        params.add(channelId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getPointByDate(String ownerId, Date dateFrom, Date dateTo) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be blank for PointTrxHistoryDaoImpl.getPointByDate");
        }

        List<Object> params = new ArrayList<>();
        String hql = "SELECT sum(d.totalAmt) FROM PointTrxHistory d WHERE ownerId = ? ";
        params.add(ownerId);

        if (dateFrom != null) {
            hql += "AND d.trxDate >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }
        if (dateTo != null) {
            hql += "AND d.trxDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }
}
