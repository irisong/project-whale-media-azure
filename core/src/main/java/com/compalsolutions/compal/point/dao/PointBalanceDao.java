package com.compalsolutions.compal.point.dao;

import java.math.BigDecimal;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.point.vo.PointBalance;

public interface PointBalanceDao extends BasicDao<PointBalance, String> {
    public static final String BEAN_NAME = "pointBalanceDao";

    PointBalance findByOwnerIdAndContributorId(String ownerId, String contributorId);

    void doCreditAvailableBalance(String ownerId, String contributorId, BigDecimal amount);

    public List<PointBalance> findByOwnerIdAndContributorIdDuplicate(String ownerId, String contributorId);
}
