package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.SymbolicItem;

import java.util.List;

public interface SymbolicItemDao extends BasicDao<SymbolicItem, String> {
    public static final String BEAN_NAME = "symbolicItemDao";

    public List<SymbolicItem> findActiveItemBySymbolicId(String symbolicId);

    public List<SymbolicItem> findAllActiveSymbolicItems();

    public List<SymbolicItem> findActiveSymbolicItemsByLevel(String symbolicId, Integer level);

    public List<SymbolicItem> findItemBySymbolicId(String symbolicId);

    public List<SymbolicItem> findGifAnimation();

    public List<SymbolicItem> findGifAnimationByVersion(int version, String status);

    public List<SymbolicItem> findGifAnimationByGifId(String gifId);

    public SymbolicItem findItemBySymbolicIdAndRemark(String symbolicId);

    public void findSymbolicItemForDatagrid(DatagridModel<SymbolicItem> datagridModel, String symbolicId,
                                        String name, String nameCn, String status);

    public List<SymbolicItem> findGifAnimationWithSameName(String gifFileName);

    public SymbolicItem findWhaleCard();

    public void updateSymbolicItemStatusByCategory(String symbolicId, String status);

    public int getNextSymbolicItemSeq(String symbolicId);

    public void updateSymbolicItemSortOrder(String type, int seq, String symbolicId);
}
