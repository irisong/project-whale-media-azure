package com.compalsolutions.compal.point.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.point.dto.PaymentDto;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.dto.PointTrxResultDto;
import com.compalsolutions.compal.point.vo.*;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface PointService {
    public static final String BEAN_NAME = "pointService";

    public void doSavePointWalletTrx(PointWalletTrx pointWalletTrx);

    public List<PointBalance> getPointBalanceDuplicateRecord(String ownerId, String contributorId);

    public void doProcessDeleteDuplicateRecord(PointBalance pointBalance);

    public List<SymbolicItem> findActiveSymbolicItems();

    public List<SymbolicItem> findAllActiveSymbolicItems(String symbolicId);

    public List<Symbolic> findAllActiveSymbolic();

    public Symbolic findSymbolic(String symbolicId);

    public SymbolicItem findSymbolicItem(String id);

    public List<SymbolicItem> findGifAnimationByGifId(String gifId);

    public List<SymbolicItem> findSymbolicItemByGifFileName(String gifFileName);

    public List<SymbolicItem> findActiveSymbolicItemsByLevel(String symbolicId, Integer level);

    public int getLatestGifVersion();

    public int getNextSymbolicSeq();

    public int getNextSymbolicItemSeq(String symbolicId);

    public void doCreateNewGiftCategory(Symbolic symbolic_new);

    public void doUpdateGiftCategory(Symbolic symbolic_new);

    public void doUpdateSymbolicCategoryStatus(String symbolicId, String status);

    public void doUpdateSymbolicItemStatus(Locale locale, String id, String status);

    public void doUpdateSymbolicItemStatusByCategory(String symbolicId, String status);

    public void findSymbolicForListing(DatagridModel<Symbolic> datagridModel, String category, String categoryCn, String status);

    public void findSymbolicItemForDatagrid(DatagridModel<SymbolicItem> datagridModel, String symbolicId,
                                            String name, String nameCn, String status);

    public List<Symbolic> findAllActiveSymbolicWithItems(Integer level);

    public void doCreateSymbolItems(SymbolicItem symbolicItem);

    public List<SymbolicItem> findGifAnimation();

    public List<SymbolicItem> findGifAnimationByVersion(int version, String status);

    public List<SymbolicItem> findCacheGifAnimationByVersion(int version, String status);

    public void doCreateSymbol(Symbolic symbolic);

    public BigDecimal getTotalPointReceivedByOwnerId(String ownerId, boolean localDatabase);

    public BigDecimal getTotalPointSentByContributorId(String contributorId, boolean localDatabase);

    public List<PointBalanceDto> getOwnerIdsFromPointBalance();

    public List<PointReportDto> getOwnerIdsFromPointReport();

    public void doCreditPointSummary(String ownerId, BigDecimal total);

    public void doCreditPointBalance(String contributorId, String ownerId, BigDecimal total);

    public void doCreditPointReport(String ownerId, BigDecimal total, Date date, String reportDate);

    public PointTrxResultDto doCreatePointTrx(Locale locale, String memberId, String receiverId, BigDecimal qty, String itemId);

    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String type, String dateRangeType, String ownerId);

    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String type, String dateRangeType, String ownerId,
                                        Date dateFrom, Date dateTo, String month);

    public List<PaymentDto> doGetOMCWalletPaymentMethod(BigDecimal priceInUsd, boolean isCn, String phoneno);

    public void doCreateTopUpPointTrxViaOmcWallet(Locale locale, String currencyCode, Integer amount,
                                                  String memberId, String transactionPassword);

    public PointTrx findOnePointTrxForPersonal(String ownerId, String memberId, String type, String dateRangeType, boolean isLiveStream, boolean isLiveStreamOwner);

    public void doProcessGiftItemAndFile(SymbolicItem symbolicItem);

    public void doUpdateGiftItemAndFile(Locale locale, SymbolicItem symbolicItem, boolean chgImage, boolean chgGifImage);

//    public void doActivateBatchVersion();

    public List<String> getReportAvailableDate();

    public List<String> getMemberContainsGiftPointWithinDate(Date dateFrom, Date dateTo);

    public BigDecimal getTotalPointReceivedByChannelId(String channelId);

    public BigDecimal getTotalGiftPointWithinDate(String ownerId, Date dateFrom, Date dateTo);

    public BigDecimal getTotalGiftPoint(String ownerId);

    public BigDecimal getTotalPointBySenderAndReceiver(String senderId, String receiverId);

    public void getGiftPointIncomeHistory(DatagridModel<PointReport> datagridModel, String ownerId);

    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, boolean isGiftListing, String channelId, String contributorId);

    public boolean findSpecialGift(String memberId, String receiverId, String itemId);

    public boolean findSpecialRemarkSymbol(String symbolicItemId);

    public String findWhaleCard();

    public List<String> findPointTrxByMemberAndSymbolId(String memberId, String symbolicItemId);

    public PointBalance doCreatePointBalanceIfNotExists(String ownerId, String contributorId);

    public PointSummary doCreatePointSummaryIfNotExists(String ownerId);

    public PointReport doCreateCreditPointReportTypeIfNotExists(String ownerId, Date date, String reportDate);

    public BigDecimal getPointByDate(String ownerId, Date dateFrom, Date dateTo);

    public BigDecimal getPointByChannelByDate(String channelId, Date dateFrom, Date dateTo);

    public List<PointSummary> getAllPointSummary();

    public List<Map<String, BigDecimal>> getAllContributionSummary();

//    public List<PointBalance> getAllPointBalance();

    public byte[] getScaledImage(byte[] srcImg, Integer width, Integer height);

    public List<PointGlobalTrx> getPointGlobalTrx();

    public void doRemovePointGlobalRecord(PointGlobalTrx pointGlobalTrxList);

    public void doSavePointGlobal(PointGlobalTrx pointGlobalTrx);

    public void doProcessGlobalGift(RunTaskLogger logger);

    public PointReport getLatestOutPointReportByMemberId(String memberId);

    public BigDecimal doCalculateOutAmount(String memberId, String reportDate);

    public PointReport doCreateOutPointReportIfNotExist(String memberId, Date date, String reportDate, BigDecimal totalOutAmt);

//    public void doDebitPointReport(String ownerId, BigDecimal outAmt, Date date, String reportDate);
}
