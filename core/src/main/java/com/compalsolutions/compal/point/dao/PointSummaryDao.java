package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.point.vo.PointSummary;

import java.math.BigDecimal;
import java.util.List;


public interface PointSummaryDao extends BasicDao<PointSummary, String> {
    public static final String BEAN_NAME = "pointSummaryDao";

    PointSummary findByOwnerId(String ownerId);

    void doCreditAvailableBalance(String ownerId, BigDecimal amount);

    public List<PointSummary> getAllPointSummary();
}
