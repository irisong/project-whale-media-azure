package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;


@Entity
@Table(name = "wl_point_balance")
@Access(AccessType.FIELD)
public class PointBalance extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "balance_id", unique = true, nullable = false, length = 32)
    private String balanceId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @Column(name = "contributor_id", length = 32, nullable = false)
    private String contributorId;

    @Column(name = "total_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmt;


    public PointBalance() {
    }

    public PointBalance(boolean defaultValue) {
        if (defaultValue) {
            totalAmt = BigDecimal.ZERO;
        }
    }

    public String getBalanceId() {
        return balanceId;
    }

    public void setBalanceId(String balanceId) {
        this.balanceId = balanceId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }
}
