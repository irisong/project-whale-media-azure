package com.compalsolutions.compal.point.dto;

public class PointTrxResultDto {
    private String returnMsg;
    private String fansBadgeName;
    private int sentQty;
    private String returnCode;
    private String channelId;

    public String getReturnMsg() {
        return returnMsg;
    }

    public void setReturnMsg(String returnMsg) {
        this.returnMsg = returnMsg;
    }

    public int getSentQty() {
        return sentQty;
    }

    public void setSentQty(int sentQty) {
        this.sentQty = sentQty;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(String returnCode) {
        this.returnCode = returnCode;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }
}
