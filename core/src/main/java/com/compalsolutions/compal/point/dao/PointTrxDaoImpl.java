package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(PointTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointTrxDaoImpl extends Jpa2Dao<PointTrx, String> implements PointTrxDao {

    public PointTrxDaoImpl() {
        super(new PointTrx(false));
    }

    @Override
    public void findPointTransactionForDatagrid(DatagridModel<PointTrx> datagridModel, String channelId, String contributorId) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM p IN " + PointTrx.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(channelId)) {
            hql += " and p.channelId = ?";
            params.add(channelId);
        }

        if (StringUtils.isNotBlank(contributorId)) {
            hql += " and p.contributorId = ? ";
            params.add(contributorId);
        }

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public BigDecimal getTotalGiftPointWithinDate(String ownerId, Date dateFrom, Date dateTo) {
        Validate.notNull(ownerId);
        List<Object> params = new ArrayList<>();

        String hql = "select sum(d.totalAmt) from PointTrxHistory d where d.ownerId=? and d.trxDate >= ? " +
                " and d.trxDate <= ?";
        params.add(ownerId);
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal getTotalGiftPoint(String ownerId) {
        Validate.notNull(ownerId);
        List<Object> params = new ArrayList<>();

        String hql = "select sum(d.totalAmt) from PointTrx d where d.ownerId=?";
        params.add(ownerId);

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }

    @Override
    public List<PointTrx> findPointTransactionForSpecialGift(String memberId, String receiverId, String itemId) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM p IN " + PointTrx.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(memberId)) {
            hql += " and p.contributorId = ?";
            params.add(memberId);
        }

        if (StringUtils.isNotBlank(receiverId)) {
            hql += " and p.ownerId = ? ";
            params.add(receiverId);
        }

        if (StringUtils.isNotBlank(itemId)) {
            hql += " and p.symbolicId = ? ";
            params.add(itemId);
        }

        return findQueryAsList(hql, params.toArray());

    }

    @Override
    public void updatePointSymbolicId(String newSymbolicId, String oldSymbolicId) {
        List<Object> params = new ArrayList<>();
        String hql = "update PointTrx c set c.symbolicId = ?" +
                " where c.symbolicId = ?";
        params.add(newSymbolicId);
        params.add(oldSymbolicId);
        this.bulkUpdate(hql, params.toArray());
    }

    @Override
    public BigDecimal getTotalPointBySenderAndReceiver(String senderId, String receiverId){
       // Validate.notNull(ownerId);
        List<Object> params = new ArrayList<>();

        String hql = "select sum(d.totalAmt) from PointTrx d where d.ownerId=? AND d.contributorId=? AND d.trxDate=?";
        params.add(receiverId);
        params.add(senderId);
        params.add(new Date());

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }
}
