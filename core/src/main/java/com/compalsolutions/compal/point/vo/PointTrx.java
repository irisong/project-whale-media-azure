package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "wl_point_trx")
@Access(AccessType.FIELD)
public class PointTrx extends VoBase {
    private static final long serialVersionUID = 1L;
    public static final String DAILY = "DAILY";
    public static final String WEEKLY = "WEEKLY";
    public static final String MONTHLY = "MONTHLY";
    public static final String CUSTOM_DATE = "CUSTOM_DATE";
    public static final String ALL = "ALL";

    public static final String POPULAR = "POPULAR";
    public static final String CONTRIBUTOR = "CONTRIBUTOR";

    public static final String TOPUP_ONLINE_BANKING = "ONLINE_BANKING";
    public static final String TOPUP_ATM_CASH_DEPOSIT = "ATM_CASH_DEPOSIT";
    public static final String TOPUP_CREDIT_CARD = "CREDIT_CARD";
    public static final String TOPUP_VIA_OMCWALLET = "OMCWALLET";
    public static final String APPLE_PAY = "APPLE_PAY";
    public static final String GOOGLE_PAY = "GOOGLE_PAY";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "trx_id", unique = true, nullable = false, length = 32)
    private String trxId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @Column(name = "contributor_id", length = 32, nullable = false)
    private String contributorId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Temporal(TemporalType.DATE)
    @Column(name = "trx_date", nullable = false)
    private Date trxDate;

    @Column(name = "amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal amt;

    @Column(name = "price", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal price;

    @Column(name = "total_amt", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalAmt;

    @Column(name = "channel_id", length = 32)
    private String channelId;

    @Column(name = "symbolic_id", length = 32, nullable = false)
    private String symbolicId;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Transient
    private String profileName;

    @Transient
    private String memberCode;

    @Transient
    private BigDecimal point;

    @Transient
    private String memberId;

    @Transient
    private int rowNum;

    @Transient
    private String date;

    public PointTrx() {
    }

    public PointTrx(boolean defaultValue) {
        if (defaultValue) {
            amt = BigDecimal.ZERO;
        }
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        PointTrx walletTrx = (PointTrx) o;

        if (trxId != null ? !trxId.equals(walletTrx.trxId) : walletTrx.trxId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return trxId != null ? trxId.hashCode() : 0;
    }

    public String getContributorId() {
        return contributorId;
    }

    public void setContributorId(String contributorId) {
        this.contributorId = contributorId;
    }

    public BigDecimal getAmt() {
        return amt;
    }

    public void setAmt(BigDecimal amt) {
        this.amt = amt;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getTotalAmt() {
        return totalAmt;
    }

    public void setTotalAmt(BigDecimal totalAmt) {
        this.totalAmt = totalAmt;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSymbolicId() {
        return symbolicId;
    }

    public void setSymbolicId(String symbolicId) {
        this.symbolicId = symbolicId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
