package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.point.vo.PointTrxHistory;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface PointTrxHistoryDao extends BasicDao<PointTrxHistory, String> {
    public static final String BEAN_NAME = "pointTrxHistoryDao";

    public List<String> findMemberTransactionForSpecialGift(String receiverId, String itemId);

    public BigDecimal getPointByChannelByDate(String channelId, Date dateFrom, Date dateTo);

    public BigDecimal getPointByDate(String ownerId, Date dateFrom, Date dateTo);
}
