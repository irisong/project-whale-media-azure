package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "wl_point_symbolic")
@Access(AccessType.FIELD)
public class Symbolic extends VoBase {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "symbolic_id", unique = true, nullable = false, length = 32)
    private String symbolicId;

    @ToTrim
    @Column(name = "type", length = 100)
    private String type;

    @ToTrim
    @Column(name = "category", columnDefinition = Global.ColumnDef.TEXT)
    private String category;

    @ToTrim
    @Column(name = "category_cn", columnDefinition = Global.ColumnDef.TEXT)
    private String categoryCn;

    @ToTrim
    @Column(name = "seq")
    private Integer seq;

    @ToTrim
    @Column(name = "status", length = 20)
    private String status;

    @Transient
    private List<SymbolicItem> symbolicItemList;

    @Transient
    private String statusDesc;

    public Symbolic() {
    }

    public Symbolic(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
            seq = 0;
        }
    }

    public String getSymbolicId() {
        return symbolicId;
    }

    public void setSymbolicId(String symbolicId) {
        this.symbolicId = symbolicId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryCn() {
        return categoryCn;
    }

    public void setCategoryCn(String categoryCn) {
        this.categoryCn = categoryCn;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<SymbolicItem> getSymbolicItemList() {
        return symbolicItemList;
    }

    public void setSymbolicItemList(List<SymbolicItem> symbolicItemList) {
        this.symbolicItemList = symbolicItemList;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }
}
