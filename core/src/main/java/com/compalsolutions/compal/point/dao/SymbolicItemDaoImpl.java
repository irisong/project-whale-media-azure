package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(SymbolicItemDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SymbolicItemDaoImpl extends Jpa2Dao<SymbolicItem, String> implements SymbolicItemDao {

    public SymbolicItemDaoImpl() {
        super(new SymbolicItem(false));
    }

    @Override
    public List<SymbolicItem> findActiveItemBySymbolicId(String symbolicId) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + " AND s.status = ? AND s.symbolicId = ? order by seq ";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);
        params.add(symbolicId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findAllActiveSymbolicItems() {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + "AND s.status = ? order by s.symbolicId";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findActiveSymbolicItemsByLevel(String symbolicId, Integer level) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + "AND s.status = ? AND s.symbolicId = ? AND s.level <= ? order by seq ";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);
        params.add(symbolicId);
        params.add(level);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findSymbolicItemForDatagrid(DatagridModel<SymbolicItem> datagridModel, String symbolicId,
                                            String name, String nameCn, String status) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM s IN " + SymbolicItem.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(symbolicId)) {
            hql += " and s.symbolicId = ?";
            params.add(symbolicId);
        }

        if (StringUtils.isNotBlank(name)) {
            hql += " and s.name like ? ";
            params.add("%" + name + "%");
        }

        if (StringUtils.isNotBlank(nameCn)) {
            hql += " and s.nameCn like ? ";
            params.add("%" + name + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and s.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "s", hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findItemBySymbolicId(String symbolicId) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + " AND s.symbolicId = ? order by seq ";

        List<Object> params = new ArrayList<Object>();
        params.add(symbolicId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public SymbolicItem findItemBySymbolicIdAndRemark(String symbolicId) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + " AND s.id = ? AND s.remark = ?  order by seq ";

        List<Object> params = new ArrayList<Object>();
        params.add(symbolicId);
        params.add(SymbolicItem.WHALE_VIP_CARD);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findGifAnimationWithSameName(String gifFileName) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + " AND s.gifFilename = ? order by s.gifVersion desc";

        List<Object> params = new ArrayList<Object>();
        params.add(gifFileName);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findGifAnimation() {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
//                + " AND s.type = ? ORDER BY s.gifVersion DESC";
                + " AND s.type = ? And s.status = ? order by s.gifVersion desc";
// include inactive status, to tally with mobile version

        List<Object> params = new ArrayList<Object>();
        params.add(SymbolicItem.GIFT_ANIMATION);
        params.add(Global.Status.ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findGifAnimationByGifId(String gifId) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE s.gifId = ? ";

        List<Object> params = new ArrayList<Object>();
        params.add(gifId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<SymbolicItem> findGifAnimationByVersion(int version, String status) {
        String hql = " FROM s IN " + SymbolicItem.class + " WHERE 1=1 "
                + " AND s.gifVersion >= ? and s.type = ? and s.status = ?";

        List<Object> params = new ArrayList<Object>();
        params.add(version);
        params.add(SymbolicItem.GIFT_ANIMATION);
        params.add(status);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public SymbolicItem findWhaleCard() {
        String hql = "from a IN " + SymbolicItem.class + " WHERE a.remark = ? ";

        List<Object> params = new ArrayList<Object>();
        params.add(SymbolicItem.WHALE_VIP_CARD);

        return findFirst(hql, params.toArray());
    }

    @Override
    public void updateSymbolicItemStatusByCategory(String symbolicId, String status) {
        String hql = "update SymbolicItem set status = ?, datetimeUpdate = now() where symbolicId = ? ";

        List<Object> params = new ArrayList<>();
        params.add(status);
        params.add(symbolicId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public int getNextSymbolicItemSeq(String symbolicId) {
        String hql = "SELECT seq FROM SymbolicItem WHERE symbolicId = ? ORDER BY seq DESC";

        List<Object> params = new ArrayList<>();
        params.add(symbolicId);

        Integer result = (Integer) exFindFirst(hql, params.toArray());
        if (result != null)
            return result.intValue() + 1;

        return 0;
    }

    @Override
    public void updateSymbolicItemSortOrder(String type, int seq, String symbolicId) {
        boolean moveSeq = false;

        SymbolicItem symbolicItem = new SymbolicItem(false);
        symbolicItem.setSymbolicId(symbolicId);
        symbolicItem.setSeq(seq);

        List<SymbolicItem> duplicateSequence = findByExample(symbolicItem);
        if (type.equalsIgnoreCase("NEW")) {
            if (duplicateSequence.size() > 0) { //this seq already in use
                moveSeq = true;
            }
        } else {
            if (duplicateSequence.size() == 1) { //no other record having same seq
                moveSeq = true;
            }
        }

        if (moveSeq) {
            String action = "seq+1";
            if (type.equalsIgnoreCase("OLD")) {
                action = "seq-1";
            }

            String hql = "UPDATE SymbolicItem SET seq = " + action + " WHERE seq >= ? AND symbolicId = ? ";

            List<Object> params = new ArrayList<>();
            params.add(seq);
            params.add(symbolicId);

            bulkUpdate(hql, params.toArray());
        }
    }
}
