package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.Symbolic;

import java.util.List;

public interface SymbolicDao extends BasicDao<Symbolic, String> {
    public static final String BEAN_NAME = "symbolicDao";

    public List<Symbolic> findAllActiveSymbolic();

    public Symbolic findOneSymbolicByName(String category, String categoryCn);

    public void findSymbolicForDatagrid(DatagridModel<Symbolic> datagridModel, String category, String categoryCn, String status);

    public int getNextSymbolicSeq();

    public void updateSymbolicSortOrder(String type, int seq);
}
