package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "wl_point_global_trx")
@Access(AccessType.FIELD)
public class PointGlobalTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", nullable = false, length = 32)
    private String id;

    @Column(name = "channel_id", length = 32, nullable = false)
    private String channelId;

    @Column(name = "acc_id", length = 32, nullable = false)
    private String accountId;

    @Column(name = "content" ,columnDefinition = Global.ColumnDef.TEXT)
    private String content;

    public PointGlobalTrx() {
    }

    public PointGlobalTrx(boolean defaultValue) {
        if (defaultValue) {

        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
