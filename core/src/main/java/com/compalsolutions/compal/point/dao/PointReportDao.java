package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.point.vo.PointReport;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public interface PointReportDao extends BasicDao<PointReport, String> {
    public static final String BEAN_NAME = "pointReportDao";

    PointReport findInReportByOwnerIdAndDate(String ownerId, Date date);

    void doCreditPointReport(String ownerId, BigDecimal amount, Date date, String reportDate);

    PointReport getLatestOutPointReportByMemberId(String memberId);

    BigDecimal doCalculateOutAmount(String memberId, String reportDate);

    PointReport findOutReportByOwnerIdAndDate(String memberId, Date date);
}
