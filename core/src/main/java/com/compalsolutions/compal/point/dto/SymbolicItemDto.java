package com.compalsolutions.compal.point.dto;

import java.math.BigDecimal;
import java.util.Map;

public class SymbolicItemDto {

    private String itemId;
    private String name;
    private String imageUrl;
    private String animationUrl;
    private String animationDownloadUrl;
    private int qty;
    private Map nameList;
    private BigDecimal price;
    private String type;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getAnimationUrl() {
        return animationUrl;
    }

    public void setAnimationUrl(String animationUrl) {
        this.animationUrl = animationUrl;
    }

    public String getAnimationDownloadUrl() {
        return animationDownloadUrl;
    }

    public void setAnimationDownloadUrl(String animationDownloadUrl) {
        this.animationDownloadUrl = animationDownloadUrl;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public Map getNameList() {
        return nameList;
    }

    public void setNameList(Map nameList) {
        this.nameList = nameList;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
