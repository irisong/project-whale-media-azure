package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.vo.PointBalance;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component(PointBalanceDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointBalanceDaoImpl extends Jpa2Dao<PointBalance, String> implements PointBalanceDao {

    public PointBalanceDaoImpl() {
        super(new PointBalance(false));
    }

    @Override
    public PointBalance findByOwnerIdAndContributorId(String ownerId, String contributorId) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be blank for PointBalanceDao.findByOwnerIdAndContributorId");
        }

        if (StringUtils.isBlank(contributorId)) {
            throw new ValidatorException("contributorId can not be blank for PointBalanceDao.findByOwnerIdAndContributorId");
        }

        PointBalance example = new PointBalance(false);
        example.setOwnerId(ownerId);
        example.setContributorId(contributorId);

        return findFirst(example);
    }

    @Override
    public List<PointBalance> findByOwnerIdAndContributorIdDuplicate(String ownerId, String contributorId) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be blank for PointBalanceDao.findByOwnerIdAndContributorId");
        }

        if (StringUtils.isBlank(contributorId)) {
            throw new ValidatorException("contributorId can not be blank for PointBalanceDao.findByOwnerIdAndContributorId");
        }

        PointBalance example = new PointBalance(false);
        example.setOwnerId(ownerId);
        example.setContributorId(contributorId);

        return findByExample(example, "datetimeAdd desc");
    }

    @Override
    public void doCreditAvailableBalance(String ownerId, String contributorId, BigDecimal amount) {
        List<Object> params = new ArrayList<>();
        String hql = "update PointBalance a set a.totalAmt = a.totalAmt + ? " //
                + " where a.ownerId = ? AND a.contributorId = ?";

        params.add(amount);
        params.add(ownerId);
        params.add(contributorId);

        bulkUpdate(hql, params.toArray());
    }
}
