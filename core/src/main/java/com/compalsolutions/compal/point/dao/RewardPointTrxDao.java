package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.RewardPointTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface RewardPointTrxDao extends BasicDao<RewardPointTrx, String> {
    public static final String BEAN_NAME = "rewardPointTrxDao";


}
