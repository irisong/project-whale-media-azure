package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component(PointTrxSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointTrxSqlDaoImpl extends AbstractJdbcDao implements PointTrxSqlDao {

    @Override
    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String ownerId, String type, String dateRangeType) {
        findPointTrxForDatagrid(datagridModel, ownerId, type, dateRangeType,
                null, null, null);
    }

    @Override
    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String ownerId, String type, String dateRangeType,
                                        Date dateFrom, Date dateTo, String month) {
        Pair<String, List<Object>> sqlAndParams = getRankingSqlAndParam(ownerId, type, dateRangeType, dateFrom, dateTo, month);
        List<Object> params = sqlAndParams.getRight();
        String sql = sqlAndParams.getLeft();
        int pageRank = ((datagridModel.getCurrentPage() > 0 ? datagridModel.getCurrentPage() - 1
                : datagridModel.getCurrentPage()) * datagridModel.getPageSize());
        queryForDatagrid(datagridModel, sql, new RowMapper<PointTrx>() {
            public PointTrx mapRow(ResultSet rs, int rowNum) throws SQLException {
                PointTrx obj = new PointTrx();
                obj.setPoint(new BigDecimal(rs.getString("total")));
                obj.setMemberId(rs.getString(getRankingColumnName(type)));
                obj.setRowNum(pageRank + (rowNum + 1));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<PointTrx> findPointTrxForListing(String ownerId, String type, String dateRangeType) {
        Pair<String, List<Object>> sqlAndParams = getRankingSqlAndParam(ownerId, type, dateRangeType);
        List<Object> params = sqlAndParams.getRight();
        String sql = sqlAndParams.getLeft();
        return query(sql, new RowMapper<PointTrx>() {
            public PointTrx mapRow(ResultSet rs, int rowNum) throws SQLException {
                PointTrx obj = new PointTrx();
                obj.setPoint(new BigDecimal(rs.getString("total")));
                obj.setMemberId(rs.getString(getRankingColumnName(type)));
                obj.setRowNum(rowNum + 1);
                return obj;
            }
        }, params.toArray());
    }

    private String getRankingColumnName(String type) {
        String columnName = "owner_id";
        if (type.equalsIgnoreCase(PointTrx.CONTRIBUTOR)) {
            columnName = "contributor_id";
        }
        return columnName;
    }

    private Pair<String, List<Object>> getRankingSqlAndParam(String ownerId, String type, String dateRangeType) {
        return getRankingSqlAndParam(ownerId, type, dateRangeType, null, null, null);
    }

    private Pair<String, List<Object>> getRankingSqlAndParam(String ownerId, String type, String dateRangeType,
                                                             Date dateFrom, Date dateTo, String monthInStr) {
        if (StringUtils.isBlank(type)) {
            throw new ValidatorException("type cannot be blank");
        }

        List<Object> params = new ArrayList<>();
        boolean isAdminScreenDate = false;
        boolean allRecords = false;
        String groupByChannelSql = "";
        String personalListQuery;

        if (dateFrom != null || dateTo != null) {
            isAdminScreenDate = true;
            dateRangeType = PointTrx.CUSTOM_DATE;
        }
        final String columnName = getRankingColumnName(type);

        if (StringUtils.isBlank(dateRangeType)) {
            dateRangeType = PointTrx.ALL;
        }

        switch (dateRangeType) {
            case PointTrx.MONTHLY:
                dateTo = new Date();
                dateFrom = DateUtil.getFirstDayOfMonth();
                if (StringUtils.isNotBlank(monthInStr)) {
                    String today = DateUtil.format(dateTo, "yyyy/MM");
                    if (!today.equalsIgnoreCase(monthInStr)) {
                        DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        String dateFromString = monthInStr.replace("/", "-").concat("-01");
                        String dateToString = monthInStr.replace("/", "-").concat("-31");
                        try {
                            dateFrom = sdf.parse(dateFromString);
                            dateTo = sdf.parse(dateToString);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            case PointTrx.WEEKLY:
                dateTo = new Date();
                Calendar c = Calendar.getInstance();
                // sunday is first day of week
                c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                dateFrom = c.getTime();
//                        DateUtil.getFirstDayOfWeek();
                break;
            case PointTrx.DAILY:
                dateTo = new Date();
                dateFrom = dateTo;
                break;
            case PointTrx.CUSTOM_DATE:
                groupByChannelSql = ", channel_id";
                break;
            default:
                allRecords = true;
                break;
        }

        String sql = "select coalesce(sum(total_amt), 0) as total, " + columnName + groupByChannelSql
                + " from wl_point_trx where 1=1 ";

        // if all records, then grab data from wl_point_balance for better performance
        if (allRecords) {
            if (type.equalsIgnoreCase(PointTrx.CONTRIBUTOR)) {
                sql = "select coalesce(sum(total_amt), 0) as total, " + columnName + groupByChannelSql
                        + " from wl_point_balance where 1=1 ";
            } else {
                sql = "select coalesce(sum(total_amt), 0) as total, " + columnName + groupByChannelSql
                        + " from wl_point_summary where 1=1 ";
            }
        }

        if (isAdminScreenDate) {
            if (dateFrom != null) {
                sql += " and trx_date >= ?";
                params.add(DateUtil.truncateTime(dateFrom));
            }
            if (dateTo != null) {
                sql += " and trx_date <= ?";
                params.add(DateUtil.truncateMillisecond(dateTo));
            }
        } else if (!allRecords) {
            sql += " and trx_date between ? and ?";
            params.add(DateUtil.truncateTime(dateFrom));
            params.add(DateUtil.truncateMillisecond(dateTo));
        }

        if (type.equalsIgnoreCase(PointTrx.CONTRIBUTOR)) {
            // if user want to see only own contributor listing, need to filter by owner_id
            if (StringUtils.isNotBlank(ownerId)) {
                personalListQuery = " and owner_id = ?";
                sql += personalListQuery;
                params.add(ownerId);
            }
        }
        sql += " group by " + columnName + groupByChannelSql
                + " order by total desc";

        return new MutablePair<>(sql, params);
    }

    @Override
    public void getPointReportSqlAndParam(DatagridModel<PointTrx> datagridModel, String channelId) {

        List<Object> params = new ArrayList<>();

        String sql = "select sum(total_amt) totalPoint, contributor_id, channel_id " +
                "from wl_point_trx where 1=1 ";

        if (StringUtils.isNotBlank(channelId)) {
            sql += " and channel_id = ? ";
            params.add(channelId);
        }
        sql += " group by contributor_id, channel_id";
        queryForDatagrid(datagridModel, sql, new RowMapper<PointTrx>() {
            public PointTrx mapRow(ResultSet rs, int rowNum) throws SQLException {
                PointTrx obj = new PointTrx();
                obj.setPoint(new BigDecimal(rs.getString("totalPoint")));
                obj.setMemberId(rs.getString("contributor_id"));
                obj.setChannelId(rs.getString("channel_id"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public BigDecimal getTotalPointReceivedByMemberId(String memberId) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("Invalid memberId");
        }
        List<Object> params = new ArrayList<>();

        String sql = "select total_amt as total, owner_id " +
                "from wl_point_summary " +
                "WHERE 1=1 AND owner_id = ? ";
        params.add(memberId);

        List<BigDecimal> results = query(sql, (rs, rowNum) -> {
            return rs.getBigDecimal("total");
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            BigDecimal result = results.get(0);
            if (result == null)
                return BigDecimal.ZERO;
            else
                return result;
        } else {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public BigDecimal getTotalPointSentByMemberId(String contributorId) {
        if (StringUtils.isBlank(contributorId)) {
            throw new ValidatorException("Invalid memberId");
        }

        List<Object> params = new ArrayList<>();
        String sql = "select coalesce(sum(total_amt), 0) as total, contributor_id " +
                "from wl_point_balance " +
                "WHERE 1=1 AND contributor_id = ? ";
        params.add(contributorId);

        List<BigDecimal> results = query(sql, (rs, rowNum) -> {
            return rs.getBigDecimal("total");
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            BigDecimal result = results.get(0);
            if (result == null)
                return BigDecimal.ZERO;
            else
                return result;
        } else {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public BigDecimal getTotalPointReceivedByChannelId(String channelId) {
        if (StringUtils.isBlank(channelId)) {
            throw new ValidatorException("Invalid channelId");
        }
        List<Object> params = new ArrayList<>();

        String sql = "select coalesce(sum(total_amt), 0) as total\n" +
                "from wl_point_history " +
                "WHERE 1=1 AND channel_id = ?\n";
        params.add(channelId);
        List<BigDecimal> results = query(sql, (rs, rowNum) -> {
            return rs.getBigDecimal("total");
        }, params.toArray());

        if (CollectionUtil.isNotEmpty(results)) {
            BigDecimal result = results.get(0);
            if (result == null)
                return BigDecimal.ZERO;
            else
                return result;
        } else {
            return BigDecimal.ZERO;
        }
    }

    @Override
    public List<String> getReportAvailableDate() {
        String sql = "select distinct(DATE_FORMAT(trx_date,'%Y/%m')) dates from wl_point_report";
        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("dates");
            }
        });
    }

    @Override
    public List<String> getMemberContainsGiftPointWithinDate(Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();
        String sql = "select distinct(owner_id) ownerId from wl_point_trx where trx_date >=? and trx_date <=?";
        params.add(DateUtil.truncateTime(dateFrom));
        params.add(DateUtil.formatDateToEndTime(dateTo));
        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("ownerId");
            }
        }, params.toArray());
    }


    // todo: change to pointReport table
    @Override
    public void getGiftPointIncomeHistory(DatagridModel<PointReport> datagridModel, String ownerId) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("Invalid ownerId");
        }
        List<Object> params = new ArrayList<>();

        String sql = "select total_amt as total, report_date as date, trx_date as trxDate, status, in_amt as inAmt, out_amt as outAmt \n" + //
                "from wl_point_report " +
                "WHERE 1=1 AND owner_id = ? ";
//                " Group by DATE_FORMAT(trx_date,'%Y/%m')";
        params.add(ownerId);
        queryForDatagrid(datagridModel, sql, new RowMapper<PointReport>() {
            public PointReport mapRow(ResultSet rs, int rowNum) throws SQLException {
                PointReport obj = new PointReport();
                obj.setTotalAmt(new BigDecimal(rs.getString("total")));
                obj.setInAmt(new BigDecimal(rs.getString("inAmt")));
                obj.setOutAmt(new BigDecimal(rs.getString("outAmt")));
                obj.setDate(rs.getString("date"));
                obj.setReportDate(rs.getString("date"));
                obj.setStatus(rs.getString("status"));
                try {
                    DateFormat sdf = new SimpleDateFormat("yyyy/MM");
                    Date date = sdf.parse(obj.getReportDate());
                    obj.setTrxDate(date);
                } catch (Exception e) {
                    obj.setTrxDate(rs.getDate("trxDate"));
                }
                return obj;
            }
        }, params.toArray());
    }

//    @Override
//    public List<PointReportDto> getPointTrxForPointReport(Date dateFrom, Date dateTo) {
//        List<Object> params = new ArrayList<>();
//        String sql = "select coalesce(sum(total_amt), 0) as total, owner_id," +
//                " DATE_FORMAT(trx_date,'%Y/%m') as date " +
//                " from wl_point_trx where trx_date >=? and trx_date <=? group by owner_id, DATE_FORMAT(trx_date,'%Y/%m')";
//        params.add(DateUtil.truncateTime(dateFrom));
//        params.add(DateUtil.formatDateToEndTime(dateTo));
//        return query(sql, new RowMapper<PointReportDto>() {
//            public PointReportDto mapRow(ResultSet rs, int arg1) throws SQLException {
//
//                PointReportDto obj = new PointReportDto();
//                obj.setOwnerId(rs.getString("ownerId"));
//                obj.setTotal(rs.getBigDecimal("total"));
//                try {
//                    Date date = new SimpleDateFormat("YYYY/MM").parse(rs.getString("date"));
//                    obj.setDate(date);
//                } catch (Exception e) {
//                    obj.setDate(dateTo);
//                }
//                return obj;
//            }
//        }, params.toArray());
//    }


    @Override
    public List<PointBalanceDto> getOwnerIdsFromPointBalance() {
        List<Object> params = new ArrayList<>();
        String sql = " select count(*) as record,contributor_id, owner_id from wl_point_balance group by contributor_id, owner_id having count(*) > 1";
        return query(sql, new RowMapper<PointBalanceDto>() {
            public PointBalanceDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PointBalanceDto obj = new PointBalanceDto();
                obj.setOwnerId(rs.getString("owner_id"));
                obj.setContributorId(rs.getString("contributor_id"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<PointReportDto> getOwnerIdsFromPointReport() {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT b.owner_id, DATE_FORMAT(b.trx_date,'%Y/%m') as date , sum(b.total_amt) as total FROM wl_point_history b LEFT JOIN wl_point_report s ON s.owner_id = b.owner_id WHERE s.owner_id IS NULL group by b.owner_id," +
                " DATE_FORMAT(b.trx_date,'%Y/%m')";
        return query(sql, new RowMapper<PointReportDto>() {
            public PointReportDto mapRow(ResultSet rs, int arg1) throws SQLException {
                PointReportDto obj = new PointReportDto();
                obj.setOwnerId(rs.getString("owner_id"));
                obj.setTotal(rs.getBigDecimal("total"));
                obj.setReportDate(rs.getString("date"));
                try {
                    Date date = new SimpleDateFormat("YYYY/MM").parse(rs.getString("date"));
                    obj.setDate(date);
                } catch (Exception e) {
                    obj.setDate(DateUtil.truncateTime(new Date()));
                }
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<Map<String, BigDecimal>> getAllContributionSummary() {
        String sql = "SELECT contributor_id, coalesce(sum(total_amt), 0) as total " +
                "FROM wl_point_balance " +
                "GROUP BY contributor_id";

        return query(sql, (rs, rowNum) -> {
            Map<String, BigDecimal> map = new HashMap<>();
            map.put(rs.getString("contributor_id"), rs.getBigDecimal("total"));
            return map;
        });
    }
}
