package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.point.vo.PointGlobalTrx;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(PointGlobalTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointGlobalTrxDaoImpl extends Jpa2Dao<PointGlobalTrx, String> implements PointGlobalTrxDao {

    public PointGlobalTrxDaoImpl() {
        super(new PointGlobalTrx(false));
    }

    @Override
    public List<PointGlobalTrx> getAllRecord() {
        String hql = " FROM s IN " + PointGlobalTrx.class + " WHERE 1=1 ";
        List<Object> params = new ArrayList<Object>();
        return findQueryAsList(hql, params.toArray());
    }

}
