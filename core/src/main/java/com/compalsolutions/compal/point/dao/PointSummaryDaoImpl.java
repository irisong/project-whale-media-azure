package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.vo.PointSummary;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component(PointSummaryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PointSummaryDaoImpl extends Jpa2Dao<PointSummary, String> implements PointSummaryDao {

    public PointSummaryDaoImpl() {
        super(new PointSummary(false));
    }

    @Override
    public PointSummary findByOwnerId(String ownerId) {
        if (StringUtils.isBlank(ownerId)) {
            throw new ValidatorException("ownerId can not be blank for PointSummaryDao.findByOwnerId");
        }

        PointSummary example = new PointSummary(false);
        example.setOwnerId(ownerId);

        return findFirst(example);
    }

    @Override
    public void doCreditAvailableBalance(String ownerId, BigDecimal amount) {
        List<Object> params = new ArrayList<>();
        String hql = "update PointSummary a set a.totalAmt = a.totalAmt + ? "
                + " where a.ownerId = ?";

        params.add(amount);
        params.add(ownerId);
        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<PointSummary> getAllPointSummary() {
        PointSummary pointSummary = new PointSummary(false);
        return findByExample(pointSummary);
    }
}
