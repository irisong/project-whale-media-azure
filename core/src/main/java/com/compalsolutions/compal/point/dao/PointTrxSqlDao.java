package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface PointTrxSqlDao {
    public static final String BEAN_NAME = "pointTrxSqlDao";

    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String ownerId, String type, String dateRangeType);

    public void findPointTrxForDatagrid(DatagridModel<PointTrx> datagridModel, String ownerId, String type, String dateRangeType,
                                        Date dateFrom, Date dateTo, String month);

    public void getPointReportSqlAndParam(DatagridModel<PointTrx> datagridModel, String channelId);

    public List<PointTrx> findPointTrxForListing(String ownerId, String type, String dateRangeType);

    public BigDecimal getTotalPointReceivedByMemberId(String contributorId);

    public BigDecimal getTotalPointSentByMemberId(String memberId);

    public List<String> getReportAvailableDate();

    public BigDecimal getTotalPointReceivedByChannelId(String channelId);

    public void getGiftPointIncomeHistory(DatagridModel<PointReport> datagridModel, String ownerId);

    public List<String> getMemberContainsGiftPointWithinDate(Date dateFrom, Date dateTo);

    public List<PointBalanceDto> getOwnerIdsFromPointBalance();

    public List<PointReportDto> getOwnerIdsFromPointReport();

    public List<Map<String, BigDecimal>> getAllContributionSummary();
}
