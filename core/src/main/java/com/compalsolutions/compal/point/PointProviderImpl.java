package com.compalsolutions.compal.point;

import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointBalance;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointSummary;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service(PointProvider.BEAN_NAME)
public class PointProviderImpl implements PointProvider {
    private Log log = LogFactory.getLog(PointProviderImpl.class);

    private PointService pointService;

    @Autowired
    public PointProviderImpl(Environment env, PointService pointService) {
        this.pointService = pointService;
    }

    @Override
    public void getListOfOwnerIdsFromPointBalance() {
        List<PointBalanceDto> pointBalanceDtos = pointService.getOwnerIdsFromPointBalance();
        log.debug("Point balance duplicate size : " + pointBalanceDtos.size());
        int i = 0;
        for (PointBalanceDto balance : pointBalanceDtos) {
            //add point balance(VJ total point)
            List<PointBalance> recordsToClear = pointService.getPointBalanceDuplicateRecord(balance.getOwnerId(), balance.getContributorId());
            if (recordsToClear.size() > 1) {
                pointService.doProcessDeleteDuplicateRecord(recordsToClear.get(0));
            }
//            pointService.doCreditPointSummary(balance.getOwnerId(), balance.getTotal());
            log.debug("Completed : " + i);
            i++;
        }
        pointBalanceDtos.clear();
        pointBalanceDtos = null;


//        List<PointBalanceDto> balances = pointService.getOwnerIdsFromPointBalance();
//        log.debug("Point balance size : " + balances.size());
//        int i = 0;
//        for (PointBalanceDto balance : balances) {
//            //add point balance(VJ total point)
//            pointService.doCreditPointSummary(balance.getOwnerId(), balance.getTotal());
//            log.debug("Completed : " + i);
//            i++;
//        }
    }

    @Override
    public void getListOfOwnerIdsFromPointTrxForReport() {
        List<PointReportDto> report = pointService.getOwnerIdsFromPointReport();
        log.debug("Point report size : " + report.size());
        int i = 0;
        for (PointReportDto balance : report) {
            //add point balance(VJ total point)
            PointReport pointReport = pointService.doCreateCreditPointReportTypeIfNotExists(balance.getOwnerId(), new Date()
                    , balance.getReportDate());
            pointService.doCreditPointReport(balance.getOwnerId(), balance.getTotal(), DateUtil.truncateTime(balance.getDate()),
                    balance.getReportDate());
            log.debug("Completed : " + i);
            i++;
        }
    }
}
