package com.compalsolutions.compal.point.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.vo.PointTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface PointTrxDao extends BasicDao<PointTrx, String> {
    public static final String BEAN_NAME = "pointTrxDao";

    public void findPointTransactionForDatagrid(DatagridModel<PointTrx> datagridModel, String channelId, String contributorId);

    public BigDecimal getTotalGiftPointWithinDate(String ownerId, Date dateFrom, Date dateTo);

    public void updatePointSymbolicId(String newSymbolicId, String oldSymbolicId);

    public List<PointTrx> findPointTransactionForSpecialGift(String memberId, String receiverId, String itemId);

    public BigDecimal getTotalGiftPoint(String ownerId);

    public BigDecimal getTotalPointBySenderAndReceiver(String senderId, String receiverId);

}
