package com.compalsolutions.compal.point.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "wl_point_symbolic_item")
@Access(AccessType.FIELD)
public class SymbolicItem extends VoBase {

    private static final long serialVersionUID = 1L;

    public final static String WHALE_VIP_CARD = "WHALE_VIP_CARD";

    public static final String GIFT_ANIMATION = "ANIMATION";
    public static final String GIFT_IMAGE = "IMAGE";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @ToTrim
    @Column(name = "symbolic_id", length = 32)
    private String symbolicId;

    @ToTrim
    @Column(name = "type", length = 20)
    private String type;

    @ManyToOne
    @JoinColumn(name = "symbolic_id", insertable = false, updatable = false, nullable = true)
    private Symbolic symbolic;

    @ToTrim
    @Column(name = "name", columnDefinition = Global.ColumnDef.TEXT)
    private String name;

    @ToTrim
    @Column(name = "name_cn", columnDefinition = Global.ColumnDef.TEXT)
    private String nameCn;

    @Column(name = "price", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal price;

    @ToTrim
    @Column(name = "seq")
    private Integer seq;

    @ToTrim
    @Column(name = "status", length = 20)
    private String status;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "gif_filename", length = 100)
    private String gifFilename;

    @Column(name = "gif_id", length = 32)
    private String gifId;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @ToTrim
    @Column(name = "gif_content_type", length = 100)
    private String gifContentType;

    @Column(name = "file_size", nullable = false)
    private Long fileSize;

    @Column(name = "gif_file_size")
    private Long gifFileSize;

    @ToTrim
    @Column(name = "gif_version")
    private Integer gifVersion;

    @ToTrim
    @Column(name = "level")
    private Integer level;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @ToTrim
    @Column(name = "send_global_msg")
    private Boolean sendGlobalMsg;

    @Transient
    private String tempId;

    @Transient
    private String imageUrl;

    @Transient
    private String renamedFilename;

    @Transient
    private String gifRenamedFilename;

    @Transient
    private String fileUrl;

    @Transient
    private String statusDesc;

    @Transient
    private String gifTempId;

    @Transient
    private String gifImageUrl;

    @Transient
    private String gifFileUrl;

    @Transient
    private byte[] data;

    @Transient
    private byte[] gifData;

    public SymbolicItem() {
    }

    public SymbolicItem(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
            type = GIFT_IMAGE;
            level = 0;
            seq = 0;
            gifVersion = 0;
            sendGlobalMsg = false;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSymbolicId() {
        return symbolicId;
    }

    public void setSymbolicId(String symbolicId) {
        this.symbolicId = symbolicId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Symbolic getSymbolic() {
        return symbolic;
    }

    public void setSymbolic(Symbolic symbolic) {
        this.symbolic = symbolic;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public void setFileUrlWithParentPath(String parentPath) {
        fileUrl = parentPath + "/" + getRenamedFilename();
    }

    public void setGifFileUrlWithParentPath(String parentPath) {
        String gifFileName = getGifRenamedFilename();
        if (StringUtils.isNotBlank(gifFileName)) {
            gifFileUrl = parentPath + "/" + gifFileName;
        } else {
            gifFileUrl = "";
        }
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = id + "." + fileExtension;
            }
        }
        return renamedFilename;
    }

    public String getGifRenamedFilename() {
        if (StringUtils.isBlank(gifRenamedFilename)) {
            if (StringUtils.isNotBlank(gifId) && StringUtils.isNotBlank(gifFilename)) {
                String[] ss = StringUtils.split(gifFilename, ".");
                String fileExtension = ss[ss.length - 1];
                gifRenamedFilename = gifId + "." + fileExtension;
            } else {
                gifRenamedFilename = "";
            }
        }
        return gifRenamedFilename;
    }

    public void setGifRenamedFilename(String gifRenamedFilename) {
        this.gifRenamedFilename = gifRenamedFilename;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGifFilename() {
        return gifFilename;
    }

    public void setGifFilename(String gifFilename) {
        this.gifFilename = gifFilename;
    }

    public String getGifContentType() {
        return gifContentType;
    }

    public void setGifContentType(String gifContentType) {
        this.gifContentType = gifContentType;
    }

    public byte[] getGifData() {
        return gifData;
    }

    public void setGifData(byte[] gifData) {
        this.gifData = gifData;
    }

    public Long getGifFileSize() {
        return gifFileSize;
    }

    public void setGifFileSize(Long gifFileSize) {
        this.gifFileSize = gifFileSize;
    }

    public Integer getGifVersion() {
        return gifVersion;
    }

    public void setGifVersion(Integer gifVersion) {
        this.gifVersion = gifVersion;
    }

    public String getGifTempId() {
        return gifTempId;
    }

    public void setGifTempId(String gifTempId) {
        this.gifTempId = gifTempId;
    }

    public String getGifImageUrl() {
        return gifImageUrl;
    }

    public void setGifImageUrl(String gifImageUrl) {
        this.gifImageUrl = gifImageUrl;
    }

    public String getGifFileUrl() {
        return gifFileUrl;
    }

    public void setGifFileUrl(String gifFileUrl) {
        this.gifFileUrl = gifFileUrl;
    }

    public String getGifId() {
        return gifId;
    }

    public void setGifId(String gifId) {
        this.gifId = gifId;
    }

    public Boolean getSendGlobalMsg() {
        return sendGlobalMsg;
    }

    public void setSendGlobalMsg(Boolean sendGlobalMsg) {
        this.sendGlobalMsg = sendGlobalMsg;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
