package com.compalsolutions.compal;

public class AnnouncementFileUploadConfiguration {
    public static final String BEAN_NAME = "announcementFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;
    public static final String FOLDER = "announce";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getAnnouncementFileUploadPath() {
        return uploadPath + "/" + FOLDER;
    }

    public String getFullAnnouncementParentFileUrl() {
        return serverUrl + "/file/" + FOLDER;
    }

    public String getFullLocalAnnouncementParentFileUrl() {
        return "http://localhost:9010" + "/file/" + FOLDER;
    }
}
