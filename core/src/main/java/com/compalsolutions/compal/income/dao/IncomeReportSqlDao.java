package com.compalsolutions.compal.income.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.income.dto.IncomeProviderDto;
import com.compalsolutions.compal.income.vo.IncomeReport;

import java.util.List;

public interface IncomeReportSqlDao {
    public static final String BEAN_NAME = "incomeReportSqlDao";

    public void findIncomeForDatagrid(DatagridModel<IncomeReport> datagridModel, String guildName, String incomeMonth,String incomeYear);

    public List<IncomeReport> findIncomeReportByGuildAndMonth(String guildName, String incomeMonth,String incomeYear);

    public IncomeProviderDto findIncomeDetailToExportPdf(String incomeReportId);

}
