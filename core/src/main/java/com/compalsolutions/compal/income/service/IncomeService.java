package com.compalsolutions.compal.income.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.income.dto.IncomeProviderDto;
import com.compalsolutions.compal.income.vo.IncomeReport;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface IncomeService {
    public static final String BEAN_NAME = "incomeService";

    public void doSaveIncomeReport(IncomeReport incomeReport);

    public void findIncomeForReport(DatagridModel<IncomeReport> datagridModel, String guildName,String incomeMonth,String incomeYear);

    public List<IncomeReport> findIncomeReportByGuildAndMonth(String guildName, String incomeMonth,String incomeYear);

    public void doDeleteExistIncomeReportwithinDate(Date dateTo, Date dateFrom);

    public IncomeReport findIncomeReportById(String incomeReportId);

    public IncomeProviderDto findIncomeDetailToExportPdf(String incomeReportId);
}
