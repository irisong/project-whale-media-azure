package com.compalsolutions.compal.income.dao;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.Sorter;
import com.compalsolutions.compal.income.dto.IncomeProviderDto;
import com.compalsolutions.compal.income.vo.IncomeReport;
import com.compalsolutions.compal.jdbc.SingleRowMapper;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component(IncomeReportSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IncomeReportSqlDaoImpl extends AbstractJdbcDao implements IncomeReportSqlDao {

    @Override
    public void findIncomeForDatagrid(DatagridModel<IncomeReport> datagridModel, String guildName, String incomeMonth,String incomeYear) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT i.id, i.guild_id, g.name, m.member_code,md.profile_name,gm.broadcast_config_id,md.whalelive_id,i.point,bc.rank_name,i.point_earn, i.total_income, i.trx_date,i.date_from,i.basic_salary FROM bc_income_report i " +
                "JOIN mb_member m ON m.member_id = i.member_id " +
                "JOIN mb_member_detail md ON md.member_det_id = m.member_det_id " +
                "JOIN bc_broadcast_guild g ON g.id = i.guild_id "+
                "JOIN bc_broadcast_guild_member gm ON gm.member_id = i.member_id "+
                "JOIN bc_broadcast_config bc ON bc.id = gm.broadcast_config_id "+
                "WHERE 1 =1 ";

        if (StringUtils.isNotBlank(guildName) ) {
            sql += "AND g.name = ?";
            params.add(guildName);
        }

        if (StringUtils.isNotBlank(incomeMonth) ) {
            sql += "AND DATE_FORMAT(i.date_from,'%m') = ?";
            params.add(incomeMonth);
        }

        if (StringUtils.isNotBlank(incomeYear) ) {
            sql += "AND DATE_FORMAT(i.date_from,'%Y') = ?";
            params.add(incomeYear);
        }

        sql += "ORDER BY bc.sequence";

        queryForDatagrid(datagridModel, sql, new RowMapper<IncomeReport>() {
            @Override
            public IncomeReport mapRow(ResultSet rs, int rowNum) throws SQLException {
                IncomeReport incomeReport = new IncomeReport();
                incomeReport.setId(rs.getString("id"));
                incomeReport.setGuildId(rs.getString("guild_id"));
                incomeReport.setGuildId(rs.getString("name"));
                incomeReport.setMemberId(rs.getString("member_code"));
                incomeReport.setPoint(rs.getBigDecimal("point"));
                incomeReport.setPointEarn(rs.getBigDecimal("point_earn"));
                incomeReport.setTotalIncome(rs.getBigDecimal("total_income"));
                incomeReport.setTrxDate(rs.getDate("trx_date"));
                incomeReport.setWhaleLiveId(rs.getString("whalelive_id"));
                incomeReport.setProfileName(rs.getString("profile_name"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");
                String month = dateFormat.format(rs.getDate("date_from"));

                incomeReport.setIncomeMonth(month);

                incomeReport.setRankName(rs.getString("rank_name"));
                incomeReport.setBasicSalary(rs.getBigDecimal("basic_salary"));
                return incomeReport;
            }
        }, params.toArray());
    }

    @Override
    public List<IncomeReport> findIncomeReportByGuildAndMonth( String guildName, String incomeMonth,String incomeYear) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT i.id,i.day,i.duration ,i.guild_id,i.datetime_add,g.display_id,i.income_remark,gm.broadcast_config_id ,g.name, m.member_code,i.basic_salary,bc.rank_name,md.profile_name,md.whalelive_id,i.point, i.point_earn, i.total_income, i.trx_date,i.date_from,i.day_archieved,i.duration_archieved, i.withdrawable_bwp, i.total_available_bwp FROM bc_income_report i " +
                "JOIN mb_member m ON m.member_id = i.member_id " +
                "JOIN mb_member_detail md ON md.member_det_id = m.member_det_id " +
                "JOIN bc_broadcast_guild g ON g.id = i.guild_id " +
                "JOIN bc_broadcast_guild_member gm ON gm.member_id = i.member_id "+
                "LEFT JOIN bc_broadcast_config bc ON bc.id = gm.broadcast_config_id "+
                "WHERE 1 =1 ";

        if (StringUtils.isNotBlank(guildName)) {
            sql += "AND g.name = ?";
            params.add(guildName);
        }

        if (StringUtils.isNotBlank(incomeMonth)) {
            sql += "AND DATE_FORMAT(i.date_from,'%m') = ?";
            params.add(incomeMonth);
        }

        if (StringUtils.isNotBlank(incomeYear)) {
            sql += "AND DATE_FORMAT(i.date_from,'%Y') = ?";
            params.add(incomeYear);
        }

        sql += "ORDER BY bc.sequence";

        List<IncomeReport> results = query(sql, new RowMapper<IncomeReport>() {
            public IncomeReport mapRow(ResultSet rs, int arg1) throws SQLException {
                IncomeReport incomeReport = new IncomeReport();
                incomeReport.setGuildName(rs.getString("name"));
                incomeReport.setProfileName(rs.getString("profile_name"));
                incomeReport.setWhaleLiveId(rs.getString("whalelive_id"));
                incomeReport.setDay(rs.getInt("day"));
                incomeReport.setDuration(rs.getInt("duration"));
                incomeReport.setPoint(rs.getBigDecimal("point"));
                incomeReport.setPointEarn(rs.getBigDecimal("point_earn"));
                incomeReport.setIncomeRemark(rs.getString("income_remark"));
                incomeReport.setRankName(rs.getString("rank_name"));
                incomeReport.setBasicSalary(rs.getBigDecimal("basic_salary"));
                incomeReport.setGuildDisplayId(rs.getString("display_id"));
                incomeReport.setDurationArchieved(rs.getInt("duration_archieved"));
                incomeReport.setDayArchieved(rs.getInt("day_archieved"));
                incomeReport.setWithdrawableBWP(rs.getBigDecimal("withdrawable_bwp"));
                incomeReport.setTotalAvailableBWP(rs.getBigDecimal("total_available_bwp"));
                SimpleDateFormat dateFormat = new SimpleDateFormat("MMM-yyyy");
                String month = dateFormat.format(rs.getDate("date_from"));

                incomeReport.setIncomeMonth(month);
                incomeReport.setDatetimeAdd(rs.getDate("datetime_add"));
                return incomeReport;
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return new ArrayList<>();
        }
        return results;

    }

    @Override
    public IncomeProviderDto findIncomeDetailToExportPdf(String incomeReportId)
    {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT m.member_id,i.income_remark,g.name, m.member_code,bc.rank_name,md.profile_name,md.whalelive_id FROM bc_income_report i " +
                "JOIN mb_member m ON m.member_id = i.member_id " +
                "JOIN mb_member_detail md ON md.member_det_id = m.member_det_id " +
                "JOIN bc_broadcast_guild g ON g.id = i.guild_id " +
                "JOIN bc_broadcast_guild_member gm ON gm.member_id = i.member_id "+
                "LEFT JOIN bc_broadcast_config bc ON bc.id = gm.broadcast_config_id "+
                "WHERE 1=1 ";

        if (StringUtils.isNotBlank(incomeReportId)) {
            sql += "AND i.id = ?";
            params.add(incomeReportId);
        }
        IncomeProviderDto dto = queryForSingleRow(sql, new SingleRowMapper<IncomeProviderDto>() {
            @Override
            public IncomeProviderDto onMapRow(ResultSet rs, int rowNum) throws SQLException {
                IncomeProviderDto incomeReport = new IncomeProviderDto();
                incomeReport.setGuildName(rs.getString("name"));
                incomeReport.setMemberId(rs.getString("member_id"));
                incomeReport.setMemberCode(rs.getString("member_code"));
                incomeReport.setRankName(rs.getString("rank_name"));
                incomeReport.setProfileName(rs.getString("profile_name"));
                incomeReport.setWhaleliveId(rs.getString("whalelive_id"));
                return incomeReport;
            }
        }, params.toArray());

        return dto;
    }


}