package com.compalsolutions.compal.income.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.income.vo.IncomeReport;
import com.compalsolutions.compal.member.vo.Member;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface IncomeReportDao extends BasicDao<IncomeReport, String> {
    public static final String BEAN_NAME = "incomeReportDao";

    public List<IncomeReport> findIncomeReportByDate(Date dateFrom, Date dateTo);

    public IncomeReport findIncomeReportById(String incomeReportId);
}
