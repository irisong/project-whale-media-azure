package com.compalsolutions.compal.income.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.income.dao.IncomeReportDao;
import com.compalsolutions.compal.income.dao.IncomeReportSqlDao;
import com.compalsolutions.compal.income.dto.IncomeProviderDto;
import com.compalsolutions.compal.income.vo.IncomeReport;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component(IncomeService.BEAN_NAME)
public class IncomeServiceImpl implements IncomeService {

    private static final Log log = LogFactory.getLog(IncomeServiceImpl.class);

    @Autowired
    private IncomeReportDao incomeReportDao;

    @Autowired
    private IncomeReportSqlDao incomeReportSqlDao;

    @Override
    public void doSaveIncomeReport(IncomeReport incomeReport) {
        incomeReportDao.save(incomeReport);
    }

    @Override
    public void findIncomeForReport(DatagridModel<IncomeReport> datagridModel, String guildName,String incomeMonth,String incomeYear) {
        incomeReportSqlDao.findIncomeForDatagrid(datagridModel, guildName,incomeMonth,incomeYear);
    }

    @Override
    public List<IncomeReport> findIncomeReportByGuildAndMonth(String guildName, String incomeMonth,String incomeYear) {
        return incomeReportSqlDao.findIncomeReportByGuildAndMonth(guildName,incomeMonth,incomeYear);
    }

    @Override
    public void doDeleteExistIncomeReportwithinDate(Date dateFrom, Date dateTo) {

        List<IncomeReport> incomeReportList = incomeReportDao.findIncomeReportByDate(dateFrom,dateTo);
        incomeReportDao.deleteAll(incomeReportList);
        incomeReportList.clear();
    }

    @Override
    public IncomeReport findIncomeReportById(String incomeReportId) {

        return incomeReportDao.findIncomeReportById(incomeReportId);

    }

    @Override
    public IncomeProviderDto findIncomeDetailToExportPdf(String incomeReportId) {
        return incomeReportSqlDao.findIncomeDetailToExportPdf(incomeReportId);

    }

}
