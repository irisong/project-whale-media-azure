package com.compalsolutions.compal.income.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "bc_income_report")
@Access(AccessType.FIELD)
public class IncomeReport extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "guild_id", nullable = false, length = 32)
    private String guildId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @Column(name = "comp_duration")
    private Boolean completeDuration;

    @Column(name = "comp_day")
    private Boolean completeDay;

    @Column(name = "comp_point")
    private Boolean completePoint;

    //in minutes
    @Column(name = "duration")
    private Integer duration;

    //in days
    @Column(name = "day")
    private Integer day;

    //in minutes
    @Column(name = "duration_archieved")
    private Integer durationArchieved;

    //in days
    @Column(name = "day_archieved")
    private Integer dayArchieved;

    //point received from viewer
    @Column(name = "point")
    private BigDecimal point;

    //point VJ earn in final income
    @Column(name = "point_earn")
    private BigDecimal pointEarn;

    //basic income VJ earn
    @Column(name = "total_income")
    private BigDecimal totalIncome;

    //ori basic salary base on half or full month agreement
    @Column(name = "basic_salary")
    private BigDecimal basicSalary;

    @Column(name = "withdrawable_bwp")
    private BigDecimal withdrawableBWP;

    @Column(name = "total_available_bwp")
    private BigDecimal totalAvailableBWP;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_from", nullable = false)
    private Date dateFrom;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_to", nullable = false)
    private Date dateTo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_date", nullable = false)
    private Date trxDate;

    @Column(name = "income_remark", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String incomeRemark;

    @Transient
    private String profileName;

    @Transient
    private String whaleLiveId;

    @Transient
    private String IncomeMonth;

    @Transient
    private String guildName;

    @Transient
    private String rankName;

    @Transient
    private String guildDisplayId;

    public IncomeReport() {
    }

    public IncomeReport(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Boolean getCompleteDuration() {
        return completeDuration;
    }

    public void setCompleteDuration(Boolean completeDuration) {
        this.completeDuration = completeDuration;
    }

    public Boolean getCompleteDay() {
        return completeDay;
    }

    public void setCompleteDay(Boolean completeDay) {
        this.completeDay = completeDay;
    }

    public Boolean getCompletePoint() {
        return completePoint;
    }

    public void setCompletePoint(Boolean completePoint) {
        this.completePoint = completePoint;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }

    public String getIncomeRemark() {
        return incomeRemark;
    }

    public void setIncomeRemark(String incomeRemark) {
        this.incomeRemark = incomeRemark;
    }

    public BigDecimal getPointEarn() {
        return pointEarn;
    }

    public void setPointEarn(BigDecimal pointEarn) {
        this.pointEarn = pointEarn;
    }

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public String getIncomeMonth() {
        return IncomeMonth;
    }

    public void setIncomeMonth(String incomeMonth) {
        IncomeMonth = incomeMonth;
    }

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public String getGuildDisplayId() {
        return guildDisplayId;
    }

    public void setGuildDisplayId(String guildDisplayId) {
        this.guildDisplayId = guildDisplayId;
    }

    public Integer getDurationArchieved() {
        return durationArchieved;
    }

    public void setDurationArchieved(Integer durationArchieved) {
        this.durationArchieved = durationArchieved;
    }

    public Integer getDayArchieved() {
        return dayArchieved;
    }

    public void setDayArchieved(Integer dayArchieved) {
        this.dayArchieved = dayArchieved;
    }

    public BigDecimal getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(BigDecimal basicSalary) {
        this.basicSalary = basicSalary;
    }

    public BigDecimal getWithdrawableBWP() {
        return withdrawableBWP;
    }

    public void setWithdrawableBWP(BigDecimal withdrawableBWP) {
        this.withdrawableBWP = withdrawableBWP;
    }

    public BigDecimal getTotalAvailableBWP() {
        return totalAvailableBWP;
    }

    public void setTotalAvailableBWP(BigDecimal totalAvailableBWP) {
        this.totalAvailableBWP = totalAvailableBWP;
    }
}
