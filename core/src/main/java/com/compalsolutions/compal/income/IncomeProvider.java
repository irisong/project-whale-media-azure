package com.compalsolutions.compal.income;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;

import java.text.ParseException;

public interface IncomeProvider {
    public static final String BEAN_NAME = "incomeProvider";

    public void doProcessVjIncome(RunTaskLogger logger) throws ParseException;

    public void doConfigReprocessVjIncome(String incomeMonth,String incomeYear) throws ParseException;

    public void doGenTempIncomeReport()throws ParseException;

    public void  doGenClientTempIncomeReport(String guildId)throws ParseException;

}
