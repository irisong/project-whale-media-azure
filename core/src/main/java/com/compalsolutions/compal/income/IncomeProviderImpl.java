package com.compalsolutions.compal.income;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.income.service.IncomeService;
import com.compalsolutions.compal.income.vo.IncomeReport;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletBalance;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

@Service(IncomeProvider.BEAN_NAME)
public class IncomeProviderImpl implements IncomeProvider {
    private static final Log log = LogFactory.getLog(IncomeProviderImpl.class);

    public IncomeProviderImpl() {
    }

    private void logMessage(RunTaskLogger logger, String msg) {
        WebUtil.logMessage(logger, log, msg);
    }


    @Override
    public void doProcessVjIncome(RunTaskLogger logger) throws ParseException {

        BroadcastService broadcastService = Application.lookupBean(BroadcastService.class);
        Date now = new Date();

        Date[] dates = DateUtil.getFirstAndLastDateOfMonth(now);
        // if income distributed twice a month, every 16th (1th - 15th income)
        // and every first day of month (16th - last day of month)

        /**
         * one month process once the bonus, not split to 2 month
         Date halfMonth = DateUtil.addDate(dates[0], 15);
         boolean isHalfMonth = DateUtil.truncateTime(now).equals(DateUtil.truncateTime(halfMonth));
         **/
        boolean isFirstOfMonth = DateUtil.truncateTime(now).equals(DateUtil.truncateTime(dates[0]));
        if (!isFirstOfMonth) {
//        if (!(isHalfMonth || isEndOfMonth)) {
            logMessage(logger, "Today is not income released date");
            return;
        }
        // if today is last day, get 16th data until current date data
        // if today is 16th, get first day of month until 16th data
        Date previousMonth = DateUtil.addDate(now, -1);
        Date[] previousMonthDates = DateUtil.getFirstAndLastDateOfMonth(previousMonth);
        Date dateFrom = previousMonthDates[0];
        Date dateTo = previousMonthDates[1];

        List<String> memberIds = broadcastService.findAllGuildMemberWithStatusApprove();
//        if (isFirstOfMonth) {
//            dateFrom = halfMonth;
//            // end of month
//            dateTo = dates[1];
//        }
        processVjIncome(now, dateFrom, dateTo, false,memberIds);//now is the date at the moment, and then the date from and date end
    }

    @Override
    public void doConfigReprocessVjIncome(String incomeMonth, String incomeYear) throws ParseException {

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        BroadcastService broadcastService = Application.lookupBean(BroadcastService.class);


        Calendar cal = Calendar.getInstance();
        cal.set(Integer.parseInt(incomeYear), Integer.parseInt(incomeMonth) - 1, 1, 00, 00, 00); //Year, month, day of month, hours, minutes and seconds
        Date now = cal.getTime();

        Date[] previousMonthDates = DateUtil.getFirstAndLastDateOfMonth(now);
        Date dateFrom = previousMonthDates[0];
        Date dateTo = previousMonthDates[1];

        List<String> memberIds = broadcastService.findAllGuildMemberWithStatusApprove();

        processVjIncome(now, dateFrom, dateTo, false,memberIds);

    }

    @Override
    public void doGenTempIncomeReport() throws ParseException {

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        BroadcastService broadcastService = Application.lookupBean(BroadcastService.class);

        Date now = new Date();

        Date[] dates = DateUtil.getFirstAndLastDateOfMonth(now);

        Date[] previousMonthDates = DateUtil.getFirstAndLastDateOfMonth(now);
        Date dateFrom = previousMonthDates[0];

        List<String> memberIds = broadcastService.findAllGuildMemberWithStatusApprove();

        processVjIncome(now, dateFrom, now, true,memberIds);

    }

    @Override
    public void doGenClientTempIncomeReport(String guildId) throws ParseException {

        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        BroadcastService broadcastService = Application.lookupBean(BroadcastService.class);

        Date now = new Date();

        Date[] dates = DateUtil.getFirstAndLastDateOfMonth(now);

        Date[] previousMonthDates = DateUtil.getFirstAndLastDateOfMonth(now);
        Date dateFrom = previousMonthDates[0];

        List<String> memberIds = broadcastService.findGuildMemberWithStatusApproveGuildId(guildId);

        processVjIncome(now, dateFrom, now, true,memberIds);

    }





    private void processVjIncome(Date now, Date dateFrom, Date dateTo, boolean tempReport,List<String> memberIds) throws ParseException {
        IncomeService incomeService = Application.lookupBean(IncomeService.class);
        PointService pointService = Application.lookupBean(PointService.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.class);
        WalletService walletService = Application.lookupBean(WalletService.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        BroadcastService broadcastService = Application.lookupBean(BroadcastService.class);

        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");

        incomeService.doDeleteExistIncomeReportwithinDate(dateFrom, dateTo);
        log.debug("Temporary:" + tempReport);

        for (String memberId : memberIds) {
            log.debug("memberID = " + memberId);
            String incomeRemark = "";
            Member member = memberService.getMember(memberId);
            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberApproved(memberId);
            if (broadcastGuildMember != null && StringUtils.isNotBlank(broadcastGuildMember.getBroadcastConfigId())) {
                // get Guild info
                BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(broadcastGuildMember.getGuildId());
                BroadcastConfig broadcastConfig = broadcastService.getBroadcast(broadcastGuildMember.getBroadcastConfigId());


                if (broadcastConfig != null && broadcastGuild != null) {
                    BigDecimal totalGiftPoint = pointService.getTotalGiftPointWithinDate(memberId, dateFrom, dateTo);

                    BigDecimal basicSalary = broadcastConfig.getBasicSalary();
                    Integer minDay = broadcastConfig.getMinDay();
                    Integer minDuration = broadcastConfig.getMinDuration();
                    BigDecimal minPoint = broadcastConfig.getMinPoint();


                    boolean isHalf = false;

                    if (broadcastGuildMember.getHalfMonthAgreementStart() == true) {
                        if (broadcastGuildMember.getServedStartDate().getMonth() == now.getMonth()) {
                            isHalf = true;
                        }
                    }

                    if (broadcastGuildMember.getHalfMonthAgreementEnd() == true) {
                        if (broadcastGuildMember.getServedEndDate().getMonth() == now.getMonth()) {
                            isHalf = true;
                        }
                    }
                    if (isHalf) {
                        basicSalary = basicSalary.divide(new BigDecimal(2));
                        minDay = Math.round(minDay / 2);
                        minDuration = minDuration / 2;
                        minPoint = minPoint.divide(new BigDecimal(2));

                    }

                    // make sure status must be inactive (means live stream ended)
                    log.debug("Get Dates of live stream channel");
                    LinkedHashMap startDuration = liveStreamService.getDatesOfLiveStreamChannel(memberId, dateFrom, dateTo);
                    long duration = 0l;
                    int noOfDay = 0;
                    long initDuration = 0l;
                    int initDay = 0;
                    List<Date> dateList = new ArrayList<>();
                    Set<String> keys = startDuration.keySet();
                    for (String k : keys) {
                        long channelduration = Long.parseLong(startDuration.get(k).toString());

                        if (channelduration > 3600) {
                            dateList.add(dateFormat.parse(k));
                            noOfDay++;
                            if (channelduration < 10800) {
                                duration += channelduration;
                            } else {
                                duration += 10800;
                            }
                        }
//                        else
//                        {
//                            duration += channelduration;
//                        }
                        initDuration += channelduration;
                        initDay++;
                    }

                    ImmutableTriple<Boolean, String, String> triple = liveStreamService.doVerifyThreeDaysChecking(dateList, dateFrom, dateTo);
                    boolean threedays = triple.left;
                    //logMessage(logger, "Member:" + member.getMemberCode() + ";duration in sec:" + duration + ";no Of day:" + noOfDay + "; GiftPoint:" + totalGiftPoint);
                    BigDecimal entitleIncome = basicSalary;
                    BigDecimal entitleGiftPercentage = broadcastConfig.getSplitPercentage();
                    // long minutes = (milliseconds / 1000) / 60;
                    boolean minimumMin = false;
                    boolean minimumDay = false;
                    boolean minimumGift = false;

                    int min = 0;
                    int initmin = 0;
                    int outstandingMin = 0;
                    int outstandingDay = 0;
                    try {
                        long minutes = TimeUnit.SECONDS.toMinutes(duration);
                        long initminutes = TimeUnit.SECONDS.toMinutes(initDuration);
                        min = (int) minutes;
                        initmin = (int) initminutes;
                    } catch (Exception e) {
                        //logMessage(logger, e.getMessage());
                    }
                    // check if VJ meet minimum requirement for income
                    if (min >= minDuration) {
                        minimumMin = true;
                    } else {
                        outstandingMin = minDuration - min;
                    }
                    if (noOfDay >= minDay) {
                        minimumDay = true;
                    } else {
                        outstandingDay = minDay - noOfDay;
                    }
                    if (BDUtil.ge(totalGiftPoint, minPoint)) {
                        minimumGift = true;
                    }
                    boolean allPassed = minimumDay && minimumMin && minimumGift;
                    String rankPrefix = broadcastConfig.getRankName().substring(0, 1);


                    //rank consists of A1, A2, A3 and etc. will group the checking by prefix
                    switch (rankPrefix) {
                        case "N":
                            break;
                        case "V":
                            if (!allPassed) {
                                entitleIncome = BigDecimal.ZERO;


                            }
                            break;
                        case "A":
                        case "B":
                            if (!allPassed) {
                                if (minimumMin && minimumDay) {
                                    entitleGiftPercentage = new BigDecimal("80").multiply(entitleGiftPercentage.divide(new BigDecimal("100"), 2, BigDecimal.ROUND_HALF_UP));
                                }

                                if (outstandingMin > 60) {
                                    BigDecimal deductMinIncome = new BigDecimal((outstandingMin / 60) * 50);
                                    entitleIncome = entitleIncome.subtract(deductMinIncome);
                                }

                                if (outstandingDay > 0) {
                                    BigDecimal deductDayIncome = new BigDecimal(outstandingDay * 200);
                                    entitleIncome = entitleIncome.subtract(deductDayIncome);
                                }

                            }
                            break;
//                        case BroadcastConfig.RANK_C:
//                            if (!allPassed) {
//                                if (!(minimumMin && minimumDay)) {
//                                    entitleIncome = new BigDecimal("500");
//                                    entitleGiftPercentage = new BigDecimal("40");
//                                }
//                            }
//                            break;
//                        case BroadcastConfig.RANK_F:
//                            break;
                        default:
                            throw new ValidatorException("Invalid Rank: " + broadcastConfig.getRankName());
                    }

                    if (threedays) {
                        allPassed = false;
                        entitleIncome = entitleIncome.subtract(new BigDecimal("200"));
                    }

                    if (!allPassed) {
                        if (outstandingDay > 0) {
                            incomeRemark += "Outstanding day : " + outstandingDay + " days. ";
                        }
                        if (outstandingMin > 0) {
                            incomeRemark += "Outstanding duration: " + outstandingMin + " minutes. ";
                        }
                        if (!minimumGift) {
                            incomeRemark += "Current gift point : " + totalGiftPoint + ". ";
                        }
                        if (threedays) {

                            incomeRemark += "From : " + triple.middle + " to " + triple.right + " did not open live. ";
                        }


                    }

                    if (isHalf) {
                        incomeRemark += "Member is on half month agreement this month";
                    }

                    if (entitleIncome.compareTo(BigDecimal.ZERO) <= 0) {
                        entitleIncome = BigDecimal.ZERO;
                    }
                    System.out.println("Baby Whale Coin");
                    //************Baby whale point deduction***************
                    //find out latest amount by using owner Id
                    PointReport latestOutPointReport = pointService.getLatestOutPointReportByMemberId(memberId);
                    String lastReportDate = latestOutPointReport != null ? latestOutPointReport.getReportDate() : null;

                    DateFormat sdf = new SimpleDateFormat("yyyy/MM");
                    String reportDate = sdf.format(dateTo);
                    String currentDate = sdf.format(new Date());
//                    Date joinedDate= DateUtil.getFirstAndLastDateOfMonth(broadcastGuildMember.getJoinedDate())[0];
//                    String joinedGuildDate = sdf.format(DateUtil.truncateTime(joinedDate));
//                    log.debug("joinedGuildDate:" + joinedGuildDate);
//                    boolean isCreatedThisMonth = false;
                    BigDecimal totalBWPAmt = BigDecimal.ZERO;
                    WalletBalance babyWhaleCoinWalletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_82);

//                    if (lastReportDate != null) {//check last report date creation
//                        if (reportDate.equalsIgnoreCase(lastReportDate)) {
//                            isCreatedThisMonth = true;
//                        }
//                    }

                    // Temporary report from admin portal action won't generate/deduct baby whale coin for withdrawal
                    if (!tempReport) {
                        totalBWPAmt = pointService.doCalculateOutAmount(memberId, currentDate);
                        log.debug("totalBWPAmt:" + totalBWPAmt + "memberId" + memberId + "currentDate" + currentDate);
                        if (totalBWPAmt != null) {
                            if (BDUtil.ge(totalBWPAmt, new BigDecimal("10000"))) {
                                log.debug("Can withdraw!" + memberId);
                                PointReport pointReport = pointService.doCreateOutPointReportIfNotExist(memberId, new Date(), reportDate, totalBWPAmt);
                                if (pointReport != null) {
                                    walletService.doDebitAvailableWalletBalance(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_82, totalBWPAmt);
                                }
                            } else //reset the value of big decimal
                                totalBWPAmt = BigDecimal.ZERO;
                        } else
                            totalBWPAmt = BigDecimal.ZERO;
                    }
                    //************Baby whale point deduction***************

                    BigDecimal totalIncome = (totalGiftPoint.multiply(entitleGiftPercentage)).divide(new BigDecimal("100"), 6, BigDecimal.ROUND_HALF_UP);
                    // basic salary
            /*        if (BDUtil.gZero(entitleIncome)) {
                        WalletTrx walletTrx = new WalletTrx(true);
                        walletTrx.setInAmt(entitleIncome);
                        walletTrx.setOutAmt(BigDecimal.ZERO);
                        walletTrx.setOwnerId(memberId);
                        walletTrx.setOwnerType(Global.UserType.MEMBER);
                        walletTrx.setTrxDatetime(new Date());
                        walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_BASIC_INCOME);
                        walletTrx.setWalletType(Global.WalletType.WALLET_51);

                        // using systemLocale
                        String desc = i18n.getText("basicIncome", min, totalGiftPoint, noOfDay);
                        String descCn = i18n.getText("basicIncome", Global.LOCALE_CN, min, totalGiftPoint, noOfDay);
                        walletTrx.setTrxDesc(desc);
                        walletTrx.setTrxDescCn(descCn);
                        walletService.saveWalletTrx(walletTrx);
                        walletService.doCreditAvailableWalletBalance(memberId, Global.UserType.MEMBER, walletTrx.getWalletType(), walletTrx.getInAmt());
                    }

                    // gift point income

                    if (BDUtil.gZero(totalIncome)) {
                        WalletTrx income = new WalletTrx(true);
                        income.setInAmt(totalIncome);
                        income.setOutAmt(BigDecimal.ZERO);
                        income.setOwnerId(memberId);
                        income.setOwnerType(Global.UserType.MEMBER);
                        income.setTrxDatetime(new Date());
                        income.setTrxType(Global.WalletTrxType.LIVE_STREAM_GIFT_INCOME);
                        income.setWalletType(Global.WalletType.WALLET_81);

                        // using systemLocale
                        String desc = i18n.getText("giftPointIncome", totalGiftPoint, entitleGiftPercentage);
                        String descCn = i18n.getText("giftPointIncome", Global.LOCALE_CN, totalGiftPoint, entitleGiftPercentage);
                        income.setTrxDesc(desc);
                        income.setTrxDescCn(descCn);
                        walletService.saveWalletTrx(income);
                        walletService.doCreditAvailableWalletBalance(memberId, Global.UserType.MEMBER, income.getWalletType(), income.getInAmt());
                    }*/
                    IncomeReport incomeReport = new IncomeReport();
                    incomeReport.setMemberId(memberId);
                    incomeReport.setGuildId(broadcastGuild.getId());
                    incomeReport.setDay(noOfDay);
                    incomeReport.setDuration(min);
                    incomeReport.setPoint(totalGiftPoint);
                    incomeReport.setCompleteDay(minimumDay);
                    incomeReport.setCompleteDuration(minimumMin);
                    incomeReport.setCompletePoint(minimumGift);
                    incomeReport.setTrxDate(new Date());
                    incomeReport.setPointEarn(totalIncome);
                    incomeReport.setTotalIncome(entitleIncome);
                    incomeReport.setDateFrom(dateFrom);
                    incomeReport.setDateTo(dateTo);
                    incomeReport.setIncomeRemark(incomeRemark);
                    incomeReport.setDayArchieved(initDay);
                    incomeReport.setDurationArchieved(initmin);
                    incomeReport.setBasicSalary(basicSalary);
                    incomeReport.setWithdrawableBWP(totalBWPAmt);
                    incomeReport.setTotalAvailableBWP(babyWhaleCoinWalletBalance.getAvailableBalance());
                    incomeService.doSaveIncomeReport(incomeReport);
                } else {
                    //logMessage(logger, "broadcastConfig:" + broadcastGuildMember.getBroadcastConfigId() + " not found");
                }
            } else {
                //logMessage(logger, "Member:" + memberId + " is not under VJ guild");
            }
        }
        memberIds.clear();
        memberIds = null;
    }
}
