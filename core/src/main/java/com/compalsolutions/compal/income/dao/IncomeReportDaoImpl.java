package com.compalsolutions.compal.income.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.income.vo.IncomeReport;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.compalsolutions.compal.dao.AbstractJdbcDao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;


@Component(IncomeReportDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class IncomeReportDaoImpl extends Jpa2Dao<IncomeReport, String> implements IncomeReportDao {

    public IncomeReportDaoImpl() {
        super(new IncomeReport(false));
    }

    public List<IncomeReport> findIncomeReportByDate(Date dateFrom, Date dateTo){

        IncomeReport example = new IncomeReport(false);
        if (dateFrom != null)
            example.addDateFromCondition("dateFrom", DateUtil.truncateTime(dateFrom));
        if (dateTo != null)
            example.addDateToCondition("dateFrom", DateUtil.formatDateToEndTime(dateTo));

        return findByExample(example,"dateFrom");

    }

    public IncomeReport findIncomeReportById(String id)
    {
        IncomeReport example = new IncomeReport(false);
        example.setId(id);

        return findUnique(example);
    }
}
