package com.compalsolutions.compal;

public class BannerFileUploadConfiguration {

    public static final String BEAN_NAME = "bannerFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;
    public static final String FOLDER_NAME = "banner";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getBannerFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullBannerParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }

    public String getFullLocalBannerParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }
}
