package com.compalsolutions.compal;

public class GiftFileUploadConfiguration {

    public static final String BEAN_NAME = "giftFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;
    public static final String FOLDER_NAME = "giftFile";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getGiftFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullGiftParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }

    public String getFullLocalGiftParentFileUrl() {
        return "http://localhost:9010" + "/file/" + FOLDER_NAME;
    }
}
