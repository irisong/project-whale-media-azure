package com.compalsolutions.compal.agent.service;

import java.util.Locale;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

public interface AgentService {
    public static final String BEAN_NAME = "agentService";

    public void findAgentForListing(DatagridModel<Agent> datagridModel, String agentCode, String agentName, String status);

    public void doCreateAgent(Locale locale, Agent agent, String password, String compId, boolean enableRemote, RemoteAgentUser remoteAgentUser);

    public Agent getAgent(String agentId);

    public void updateAgent(Locale locale, Agent agent, boolean enableRemote, RemoteAgentUser remoteAgentUser);

    public void updateAgentUserProfile(Locale locale, Agent agent);

    public void doResetSuperAgentUserPasswordByAgentId(String agentId, String password);

    public Agent findAgentByAgentCode(String agentCode);

    public AgentUser findSuperAgentUserByAgentId(String agentId);

    public AgentUser findAgentUserByAgentId(String agentId);

    public RemoteAgentUser findRemoteAgentUserByAgentId(String agentId);
}
