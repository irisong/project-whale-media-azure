package com.compalsolutions.compal.agent.service;

import java.util.Arrays;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.Global.UserRoleGroup;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.dao.RemoteAgentUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

@Component(AgentService.BEAN_NAME)
public class AgentServiceImpl implements AgentService {
    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private RemoteAgentUserDao remoteAgentUserDao;

    @Override
    public void findAgentForListing(DatagridModel<Agent> datagridModel, String agentCode, String agentName, String status) {
        agentDao.findAgentForDatagrid(datagridModel, agentCode, agentName, status);
    }

    @Override
    public void doCreateAgent(Locale locale, Agent agent, String password, String compId, boolean enableRemote, RemoteAgentUser remoteAgentUser) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        if (agentDao.findAgentByAgentCode(agent.getAgentCode()) != null) {
            throw new ValidatorException(i18n.getText("agentCodeExist"));
        }

        // save Agent
        agent.setStatus(Global.Status.ACTIVE);
        agentDao.save(agent);

        // save Agent User
        AgentUser agentUser = new AgentUser(true);
        agentUser.setCompId(compId);
        agentUser.setSuperUser(true);
        agentUser.setAgentId(agent.getAgentId());
        agentUser.setUsername(agent.getAgentCode());
        agentUser.setPassword(password);
        UserRole agentRole = userDetailsService.findUserRoleByRoleName(compId, UserRoleGroup.AGENT_GROUP);
        if (agentRole == null)
            userDetailsService.saveUser(agentUser, null);
        else
            userDetailsService.saveUser(agentUser, Arrays.asList(agentRole));

        if (enableRemote) {
            if (userDetailsService.findUserByUsername(remoteAgentUser.getUsername()) != null) {
                throw new ValidatorException(i18n.getText("remoteUsernameExist"));
            }
            remoteAgentUser.setCompId(compId);
            remoteAgentUser.setUserType(Global.UserType.REMOTE_AGENT);
            remoteAgentUser.setAgentId(agent.getAgentId());
            userDetailsService.saveUser(remoteAgentUser, null);
        }
    }

    @Override
    public Agent getAgent(String agentId) {
        return agentDao.get(agentId);
    }

    @Override
    public void updateAgent(Locale locale, Agent agent, boolean enableRemote, RemoteAgentUser remoteAgentUser) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Agent dbAgent = agentDao.get(agent.getAgentId());
        if (dbAgent == null)
            throw new ValidatorException(i18n.getText("invalidAgent"));

        dbAgent.setAgentName(agent.getAgentName());
        dbAgent.setPhoneNo(agent.getPhoneNo());
        dbAgent.setEmail(agent.getEmail());
        dbAgent.setDefaultCurrencyCode(agent.getDefaultCurrencyCode());

        agentDao.update(dbAgent);

        if (enableRemote) {
            RemoteAgentUser dbRemoteAgentUser = findRemoteAgentUserByAgentId(agent.getAgentId());
            boolean isRemoteUserCreated = dbRemoteAgentUser != null;
            if (isRemoteUserCreated) {
                if (!dbRemoteAgentUser.getUsername().equalsIgnoreCase(remoteAgentUser.getUsername())) {
                    if (userDetailsService.findUserByUsername(remoteAgentUser.getUsername()) != null) {
                        throw new ValidatorException(i18n.getText("remoteUsernameExist"));
                    }
                    dbRemoteAgentUser.setUsername(remoteAgentUser.getUsername());
                }
                dbRemoteAgentUser.setAllowIp(remoteAgentUser.getAllowIp());
                userDetailsService.updateUser(dbRemoteAgentUser);

                if (StringUtils.isNotBlank(remoteAgentUser.getPassword())) {
                    userDetailsService.doResetPassword(dbRemoteAgentUser.getUserId(), remoteAgentUser.getPassword());
                }
            } else {
                dbRemoteAgentUser = new RemoteAgentUser(true);
                if (userDetailsService.findUserByUsername(remoteAgentUser.getUsername()) != null) {
                    throw new ValidatorException(i18n.getText("remoteUsernameExist"));
                }

                AgentUser agentUser = findSuperAgentUserByAgentId(agent.getAgentId());

                dbRemoteAgentUser.setUsername(remoteAgentUser.getUsername());
                dbRemoteAgentUser.setCompId(agentUser.getCompId());
                dbRemoteAgentUser.setUserType(Global.UserType.REMOTE_AGENT);
                dbRemoteAgentUser.setAgentId(agent.getAgentId());
                dbRemoteAgentUser.setAllowIp(remoteAgentUser.getAllowIp());
                dbRemoteAgentUser.setPassword(remoteAgentUser.getPassword());
                userDetailsService.saveUser(dbRemoteAgentUser, null);
            }
        } else {
            RemoteAgentUser dbRemoteAgentUser = findRemoteAgentUserByAgentId(agent.getAgentId());
            if (dbRemoteAgentUser != null) {
                dbRemoteAgentUser.setStatus(Global.Status.ACTIVE);
                userDetailsService.updateUser(dbRemoteAgentUser);
            }
        }
    }

    @Override
    public void updateAgentUserProfile(Locale locale, Agent agent) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Agent dbAgent = agentDao.get(agent.getAgentId());
        if (dbAgent == null)
            throw new ValidatorException(i18n.getText("invalidAgent"));

        dbAgent.setAgentName(agent.getAgentName());
        dbAgent.setPhoneNo(agent.getPhoneNo());
        dbAgent.setEmail(agent.getEmail());
        dbAgent.setDefaultCurrencyCode(agent.getDefaultCurrencyCode());

        agentDao.update(dbAgent);
    }

    @Override
    public void doResetSuperAgentUserPasswordByAgentId(String agentId, String password) {
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Agent dbAgent = agentDao.get(agentId);
        if (dbAgent == null)
            throw new SystemErrorException("Invalid Agent");

        AgentUser agentUser = agentUserDao.findSuperAgentUserByAgentId(agentId);

        String encryptedNewPassword = userDetailsService.encryptPassword(agentUser, password);
        agentUser.setPassword(encryptedNewPassword);
        agentUserDao.update(agentUser);
    }

    @Override
    public Agent findAgentByAgentCode(String agentCode) {
        return agentDao.findAgentByAgentCode(agentCode);
    }

    @Override
    public AgentUser findSuperAgentUserByAgentId(String agentId) {
        return agentUserDao.findSuperAgentUserByAgentId(agentId);
    }

    @Override
    public AgentUser findAgentUserByAgentId(String agentId) {
        return agentUserDao.findAgentUserByAgentId(agentId);
    }

    @Override
    public RemoteAgentUser findRemoteAgentUserByAgentId(String agentId) {
        return remoteAgentUserDao.findRemoteAgentUserByAgentId(agentId);
    }
}
