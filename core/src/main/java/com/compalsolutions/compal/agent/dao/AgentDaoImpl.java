package com.compalsolutions.compal.agent.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.repository.AgentRepository;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;

@Component(AgentDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentDaoImpl extends Jpa2Dao<Agent, String> implements AgentDao {
    @SuppressWarnings("unused")
    @Autowired
    private AgentRepository agentRepository;

    public AgentDaoImpl() {
        super(new Agent(false));
    }

    @Override
    public void findAgentForDatagrid(DatagridModel<Agent> datagridModel, String agentCode, String agentName, String status) {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM a IN " + Agent.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(agentCode)) {
            hql += " and a.agentCode like ? ";
            params.add(agentCode + "%");
        }

        if (StringUtils.isNotBlank(agentName)) {
            hql += " and a.agentName like ? ";
            params.add(agentName + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public Agent findAgentByAgentCode(String agentCode) {
        Agent example = new Agent(false);
        example.setAgentCode(agentCode);
        return findFirstByExample(example);
    }

    @Override
    public List<Agent> findAll() {
        return findByExample(new Agent(false), "agentCode");
    }
}
