package com.compalsolutions.compal.agent.dao;

import java.util.List;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface AgentDao extends BasicDao<Agent, String> {
    public static final String BEAN_NAME = "agentDao";

    public void findAgentForDatagrid(DatagridModel<Agent> datagridModel, String agentCode, String agentName, String status);

    public Agent findAgentByAgentCode(String agentCode);

    public List<Agent> findAll();
}
