package com.compalsolutions.compal.agent.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.agent.vo.Agent;

public interface AgentRepository extends JpaRepository<Agent, String> {
    public static final String BEAN_NAME = "agentRepository";
}
