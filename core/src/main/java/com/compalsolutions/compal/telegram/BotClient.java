package com.compalsolutions.compal.telegram;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ExternalConnectionTimeoutException;
import com.compalsolutions.compal.web.BaseRestUtil;
import org.apache.commons.lang3.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.env.Environment;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class BotClient {
    private static Log log = LogFactory.getLog(BotClient.class);

    private String serverUrl;
    private String token;
    private String groupChatId;
    private boolean isProdServer;

    public BotClient() {
        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("telegram.serverUrl");
        token = env.getProperty("telegram.bot.token");
        groupChatId = env.getProperty("telegram.group.chat.id");
        isProdServer = env.getProperty("server.production", Boolean.class, true);
        Validate.notBlank(serverUrl);
    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient(60000, 60000);
    }

    public void doSendReportMessage(String msg) {
        if (isProdServer) {
            Client client = newClient();
//        if (!isProdServer) {
//            finalMsg = "Staging Server[Please Ignore the msg] :" + msg;
//        }
            WebTarget webTarget = client.target(serverUrl + "/bot" + token + "/sendMessage") //
                    .queryParam("chat_id", groupChatId) //
                    .queryParam("text", msg);

            Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);

            Response response = null;
            try {
                response = invocationBuilder.get();
                String jsonString = response.readEntity(String.class);
                log.debug(jsonString);
            } catch (Exception ex) {
                throw new ExternalConnectionTimeoutException("Unable to connect telegram Server");
            } finally {
                if (response != null) {
                    response.close();
                }
            }
            client.close();
        }
    }
}
