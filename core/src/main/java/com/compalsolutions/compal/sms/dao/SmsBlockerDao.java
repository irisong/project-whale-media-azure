package com.compalsolutions.compal.sms.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.sms.vo.SmsBlocker;

public interface SmsBlockerDao extends BasicDao<SmsBlocker, String> {
    public static final String BEAN_NAME = "smsBlockerDao";
    
    public SmsBlocker doFindSmsBlockerByPhoneNo(String phoneNo);
}
