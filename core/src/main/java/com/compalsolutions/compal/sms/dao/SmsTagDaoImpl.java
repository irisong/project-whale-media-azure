package com.compalsolutions.compal.sms.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.sms.vo.SmsTag;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(SmsTagDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SmsTagDaoImpl extends Jpa2Dao<SmsTag, String> implements SmsTagDao {

    public SmsTagDaoImpl() {
        super(new SmsTag(false));
    }

    @Override
    public SmsTag findLastNewSmsTagByMemberId(String memberId) {
        SmsTag example = new SmsTag(false);
        example.setMemberId(memberId);
        example.setStatus(Global.Status.NEW);

        return findFirst(example, "datetimeAdd DESC");
    }

    @Override
    public SmsTag findLastNewSmsTagByContact(String phoneno) {
        SmsTag example = new SmsTag(false);
        example.setContact(phoneno);
        example.setStatus(Global.Status.NEW);

        return findFirst(example, "datetimeAdd DESC");
    }
}
