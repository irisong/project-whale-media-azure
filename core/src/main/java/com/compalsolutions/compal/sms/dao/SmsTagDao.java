package com.compalsolutions.compal.sms.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.sms.vo.SmsTag;

public interface SmsTagDao extends BasicDao<SmsTag, String> {
    public static final String BEAN_NAME = "smsTagDao";

    public SmsTag findLastNewSmsTagByMemberId(String memberId);

    public SmsTag findLastNewSmsTagByContact(String phoneno);
}
