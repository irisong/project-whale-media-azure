package com.compalsolutions.compal.sms.service;

import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.sms.vo.SmsBlocker;
import com.compalsolutions.compal.sms.vo.SmsTag;

import java.util.List;
import java.util.Locale;

public interface SmsService {
    public static final String BEAN_NAME = "smsService";

    public String doAuthenticationAndGenerateSmsTag(Locale locale, String username, String password, String securityCode, String expectedSecurityKey,
                                                    String clientIpAddress);

    public void doVerifySmsTag(Locale locale, String memberCode, String tag, int durationInMinutes);

    public void doWelcomeSms(Locale locale, String memberCode);

    public void doWalletTransferSms(Locale locale, String senderId, String receiverId, Double amount);

    public void doWalletWithdrawalSms(Locale locale, String memberId, String status, Double amount, String remark);

    public void doGenerateVerifySms(Locale locale, String phoneno);

    public SmsTag doVerifyRegistrationVerifyCodeWithoutUpdateStatus(Locale locale, String phoneno, String verifyCode, int durationInMinutes);

    public void updateSmsTagStatusToUsed(SmsTag smsTag);

    public SmsQueue getFirstNotProcessSms();

    public void updateSmsQueue(SmsQueue smsQueue);

    void doSendSmsToSupportNumbers(String accessModule, String msg, List<String> supportNumbers);

    public void doUpdateSmsBlocker(SmsBlocker smsBlocker);

    public void doSaveSmsBlocker(String phoneNo);

    public SmsBlocker doGetSmsBlocker(String blockerId);

    public SmsBlocker doFindSmsBlockerByPhoneNumber(String phoneNo);
}
