package com.compalsolutions.compal.sms.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.sms.vo.SmsBlocker;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(SmsBlockerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SmsBlockerDaoImpl extends Jpa2Dao<SmsBlocker, String> implements SmsBlockerDao{


    public SmsBlockerDaoImpl() {
        super(new SmsBlocker(false));
    }

    @Override
    public SmsBlocker doFindSmsBlockerByPhoneNo(String phoneNo) {
        List<Object> params = new ArrayList<>();
        String hql = " FROM s IN " + SmsBlocker.class + " WHERE 1=1 ";
        SmsBlocker example = new SmsBlocker(false);

        if (StringUtils.isNotBlank(phoneNo)) {
            hql += " AND s.phoneNo = ? ";
            params.add(phoneNo);
        }

        return findUnique(hql,params.toArray());
    }
}
