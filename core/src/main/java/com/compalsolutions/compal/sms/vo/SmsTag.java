package com.compalsolutions.compal.sms.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "sms_tag")
@Access(AccessType.FIELD)
public class SmsTag extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "tag_id", unique = true, nullable = false, length = 32)
    private String tagId;

    @Column(name = "member_id", length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @Column(name = "contact", length = 50)
    private String contact;

    @Column(name = "sms_tag", length = 20)
    private String smsTag;

    @Column(name = "st_tag_type", length = 30)
    private String tagType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "used_datetime")
    private Date usedDatatime;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public SmsTag() {
    }

    public SmsTag(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.NEW;
        }
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getSmsTag() {
        return smsTag;
    }

    public void setSmsTag(String smsTag) {
        this.smsTag = smsTag;
    }

    public String getTagType() {
        return tagType;
    }

    public void setTagType(String tagType) {
        this.tagType = tagType;
    }

    public Date getUsedDatatime() {
        return usedDatatime;
    }

    public void setUsedDatatime(Date usedDatatime) {
        this.usedDatatime = usedDatatime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
