package com.compalsolutions.compal.sms.vo;


import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "sms_blocker")
@Access(AccessType.FIELD)
public class SmsBlocker extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "blocker_id", unique = true, nullable = false, length = 32)
    private String blockerId;

    @ToTrim
    @Column(name = "retry_count")
    private Integer retryCount;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 30, nullable = false)
    private String phoneNo;

    public SmsBlocker() {
    }

    public SmsBlocker(boolean defaultValue) {
        if(defaultValue){
            retryCount = 0;
        }
    }

    public String getBlockerId() {
        return blockerId;
    }

    public void setBlockerId(String blockerId) {
        this.blockerId = blockerId;
    }

    public Integer getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(Integer retryCount) {
        this.retryCount = retryCount;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }
}
