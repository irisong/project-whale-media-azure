package com.compalsolutions.compal.sms;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URLEncoder;

public class NexmoConnector {
    private static final Log log = LogFactory.getLog(NexmoConnector.class);

    public JSONObject sendSms(String mobile, String msg) {
        NexmoConfig nexmoConfig = Application.lookupBean(NexmoConfig.BEAN_NAME, NexmoConfig.class);

        String appName = WebUtil.getSystemConfig().getApplicationName();

        CloseableHttpClient httpClient = HttpClients.createDefault();

        String content = null;
        HttpResponse httpResponse;
        try {
            String url = "https://rest.nexmo.com/sms/json?api_key=" + nexmoConfig.getApiKey() //
                    + "&api_secret=" + nexmoConfig.getApiSecret();
            if (mobile.startsWith("1")) {
                url = url + "&from=" + "12256128888";
            } else {
                url = url + "&from=" + URLEncoder.encode(appName, "UTF-8");
            }
            url = url + "&to=" + mobile //
                    + "&type=unicode"//
                    + "&text=" + URLEncoder.encode(msg, "UTF-8");
            // log.debug(url);
            httpResponse = httpClient.execute(new HttpGet(url));
            content = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            log.debug("SMS RESULT : " + content);
        } catch (IOException e) {
            log.error(e.getMessage(), e.fillInStackTrace());
            throw new SystemErrorException(e.getMessage());
        }

        if (StringUtils.isNotBlank(content)) {
            return new JSONObject(content);
        }

        return null;
    }
}
