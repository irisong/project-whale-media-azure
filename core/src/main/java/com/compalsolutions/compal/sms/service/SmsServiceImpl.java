package com.compalsolutions.compal.sms.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.SysFormatter;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.exception.InvalidCredentialsException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.function.user.service.impl.UserDetailsServiceImpl;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.dao.SmsQueueDao;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.sms.NexmoConnector;
import com.compalsolutions.compal.sms.dao.SmsBlockerDao;
import com.compalsolutions.compal.sms.dao.SmsTagDao;
import com.compalsolutions.compal.sms.vo.SmsBlocker;
import com.compalsolutions.compal.sms.vo.SmsTag;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.UniqueNumberGenerator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(SmsService.BEAN_NAME)
public class SmsServiceImpl extends UserDetailsServiceImpl implements SmsService {
    private static final Log log = LogFactory.getLog(SmsServiceImpl.class);

    @Autowired
    private SmsQueueDao smsQueueDao;

    @Autowired
    private SmsBlockerDao smsBlockerDao;

    @Override
    public SmsQueue getFirstNotProcessSms() {
        return smsQueueDao.getFirstNotProcessSms();
    }

    @Override
    public void updateSmsQueue(SmsQueue smsQueue) {
        smsQueueDao.update(smsQueue);
    }

    @Override
    public void doSendSmsToSupportNumbers(String accessModule, String message, List<String> supportNumbers) {
        log.debug("sendSupportSms: " + message);
        for(String phoneno : supportNumbers) {
            String platformName;
            // when send sms to China, it need a 'fat' bracket.
            if (phoneno.startsWith("86")) {
                platformName = "【OMNIC】: ";
                if (accessModule.equals(Global.AccessModule.WHALE_MEDIA)) {
                    platformName = "【OMCWALLET】: ";
                }
            } else {
                platformName = "[OMNIC+]: ";
                if (accessModule.equals(Global.AccessModule.WHALE_MEDIA)) {
                    platformName = "[OMCWALLET]: ";
                }
            }
            String msg = platformName + message;
            SmsQueue smsQueue = new SmsQueue();
            smsQueue.setBody(msg);
            smsQueue.setSmsTo(phoneno);
            smsQueue.setStatus(SmsQueue.SMS_STATUS_PENDING);
            smsQueueDao.save(smsQueue);
        }
    }

    @Autowired
    private SmsTagDao smsTagDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        UserDetails userDetails = super.loadUserByUsername(username);

        if (userDetails instanceof MemberUser) {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

            /*
            Boolean skipSms = request.getSession().getAttribute(LoginAction.SESSION_LOGIN_SKIP_SMS) != null
                    ? (Boolean) request.getSession().getAttribute(LoginAction.SESSION_LOGIN_SKIP_SMS)
                    : false;
            Boolean verifiedSms = request.getSession().getAttribute(LoginAction.SESSION_VERIFIED_SMS) != null
                    ? (Boolean) request.getSession().getAttribute(LoginAction.SESSION_VERIFIED_SMS)
                    : false;
             */

            Boolean skipSms = false;
            Boolean verifiedSms = false;

            // does nothing
            if (skipSms) {
                return userDetails;
            }

            if (!verifiedSms) {
                throw new InvalidCredentialsException(i18n.getText("invalidOneTimePasswordOrExpired"));
            }
        }

        return userDetails;
    }

    @Override
    public String doAuthenticationAndGenerateSmsTag(Locale locale, String username, String password, String securityCode, String expectedSecurityKey,
                                                    String clientIpAddress) {
        I18n i18n = Application.lookupBean(I18n.class);
        ServerConfiguration serverConfiguration = Application.lookupBean(ServerConfiguration.class);

        SessionLogService sessionLogService = Application.lookupBean(SessionLogService.class);

        // skip captcha checking if is development mode
        if (serverConfiguration.isProductionMode() && !StringUtils.equalsIgnoreCase(securityCode, expectedSecurityKey)) {
            sessionLogService.saveSessionLog(username, clientIpAddress, SessionLog.LOGIN_STATUS_FAILED_CAPTCHA, null);

            throw new ValidatorException(i18n.getText("errorMessage.invalid.security.code", locale));
        }

        User user = findUserByUsername(username);
        if (user == null || !(user instanceof MemberUser)) {
            throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
        }

        if (!isEncryptedPasswordMatch(password, user.getPassword())) {
            throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
        }

        Member member = ((MemberUser) user).getMember();
        MemberDetail memberDetail = member.getMemberDetail();

        String tag = generateRandomTag();
        // String tag = "654321";
        SmsTag smsTag = new SmsTag(true);
        smsTag.setMemberId(member.getMemberId());
        smsTag.setSmsTag(tag);
        smsTag.setTagType(Global.SmsTagType.LOGIN);
        smsTag.setStatus(Global.Status.NEW);
        smsTagDao.save(smsTag);

        log.debug(String.format("======= SMS Tag: %s, Member Code %s, Member ID %s", tag, member.getMemberCode(), member.getMemberId()));

        if (serverConfiguration.isProductionMode() && StringUtils.isNotBlank(memberDetail.getContactNo())) {
            String msg = i18n.getText("otpSms", WebUtil.getSystemConfig().getApplicationName(), tag);
            sendSms(memberDetail.getContactNo(), msg);
        }

        return memberDetail.getContactNo();
    }

    @Override
    public void doVerifySmsTag(Locale locale, String memberCode, String tag, int durationInMinutes) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        ServerConfiguration serverConfiguration = Application.lookupBean(ServerConfiguration.BEAN_NAME, ServerConfiguration.class);

        // skip verification if development mode
        if (serverConfiguration.isProductionMode()) {
            return;
        }

        Member member = memberService.findMemberByMemberCode(memberCode);

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalid_member_id", locale));
        }

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, -durationInMinutes);
        Date date = cal.getTime();

        SmsTag smsTag = smsTagDao.findLastNewSmsTagByMemberId(member.getMemberId());

        if (smsTag == null || date.after(smsTag.getDatetimeAdd())) {
            throw new ValidatorException(i18n.getText("invalidOneTimePasswordOrExpired", locale));
        }

        if (!smsTag.getSmsTag().equalsIgnoreCase(tag)) {
            throw new ValidatorException(i18n.getText("invalidOneTimePasswordOrExpired", locale));
        }

        smsTag.setStatus(Global.Status.USED);
        smsTag.setUsedDatatime(new Date());
        smsTagDao.update(smsTag);
    }

    @Override
    public void doWelcomeSms(Locale locale, String memberCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        ServerConfiguration serverConfiguration = Application.lookupBean(ServerConfiguration.class);

        // skip verification if development mode
        if (!serverConfiguration.isProductionMode()) {
            return;
        }

        Member member = memberService.findMemberByMemberCode(memberCode);

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalid_member_id", locale));
        }

        MemberDetail memberDetail = member.getMemberDetail();

        log.debug(String.format("======= SMS : Welcome to %s Your ID is %s ", WebUtil.getSystemConfig().getApplicationName(), member.getMemberCode()));

        if (serverConfiguration.isProductionMode() && StringUtils.isNotBlank(memberDetail.getContactNo())) {
            String msg = i18n.getText("welcomeSms", WebUtil.getSystemConfig().getApplicationName(), memberCode);
            sendSms(memberDetail.getContactNo(), msg);
        }
    }

    @Override
    public void doWalletTransferSms(Locale locale, String senderId, String receiverId, Double amount) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        ServerConfiguration serverConfiguration = Application.lookupBean(ServerConfiguration.class);

        // skip verification if development mode
        if (!serverConfiguration.isProductionMode()) {
            return;
        }

        Member sender = memberService.getMember(senderId);
        if (sender == null) {
            throw new ValidatorException(i18n.getText("invalid_member_id", locale));
        }

        Member receiver = memberService.getMember(receiverId);
        if (receiver == null) {
            throw new ValidatorException(i18n.getText("invalid_member_id", locale));
        }

        MemberDetail senderDetail = sender.getMemberDetail();
        MemberDetail receiverDetail = receiver.getMemberDetail();

        log.debug(String.format("======= SMS : You have transferred amount %s to %s ", SysFormatter.formatDecimal(amount), sender.getMemberCode()));

        if (serverConfiguration.isProductionMode() && StringUtils.isNotBlank(senderDetail.getContactNo())) {
            String msg = i18n.getText("wtfSms", SysFormatter.formatDecimal(amount), receiver.getMemberCode());
            sendSms(senderDetail.getContactNo(), msg);
        }

        log.debug(String.format("======= SMS : %s has transferred amount %s to you ", sender.getMemberCode(), SysFormatter.formatDecimal(amount)));

        if (serverConfiguration.isProductionMode() && StringUtils.isNotBlank(receiverDetail.getContactNo())) {
            String msg = i18n.getText("wttSms", sender.getMemberCode(), SysFormatter.formatDecimal(amount));
            sendSms(receiverDetail.getContactNo(), msg);
        }
    }

    @Override
    public void doWalletWithdrawalSms(Locale locale, String memberId, String status, Double amount, String remark) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        ServerConfiguration serverConfiguration = Application.lookupBean(ServerConfiguration.class);

        // skip verification if development mode
        if (!serverConfiguration.isProductionMode()) {
            return;
        }

        Member member = memberService.getMember(memberId);

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalid_member_id", locale));
        }

        MemberDetail memberDetail = member.getMemberDetail();

        String msg = null;

        /*
        if (Global.WithdrawalStatus.REQUESTED.equalsIgnoreCase(status)) {
            msg = i18n.getText("wwrqSms", SysFormatter.formatDecimal(amount));
            log.debug(String.format("======= SMS : You have make a withdrawal request amount %s ", SysFormatter.formatDecimal(amount)));
        } else if (Global.WithdrawalStatus.APPROVED.equalsIgnoreCase(status)) {
            msg = i18n.getText("wwapSms", SysFormatter.formatDecimal(amount));
            log.debug(String.format("======= SMS : Your withdrawal request amount %s has been approved", SysFormatter.formatDecimal(amount)));
        } else if (Global.WithdrawalStatus.REJECTED.equalsIgnoreCase(status)) {
            remark = remark != null ? remark : "-";
            msg = i18n.getText("wwrjSms", SysFormatter.formatDecimal(amount), remark);
            log.debug(String.format("======= SMS : Your withdrawal request amount %s has been rejected. Remark: %s ", SysFormatter.formatDecimal(amount),
                    remark));
        }
        */

        if (serverConfiguration.isProductionMode() && StringUtils.isNotBlank(memberDetail.getContactNo()) && msg != null) {
            sendSms(memberDetail.getContactNo(), msg);
        }
    }

    @Override
    public void doGenerateVerifySms(Locale locale, String phoneno) {
        I18n i18n = Application.lookupBean(I18n.class);
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.class);

        phoneno = StringUtils.remove(phoneno, " ");

//        //if user enter extra 60 / 0 infront their number
//        if (phoneno.startsWith("6060")) {
//            phoneno = "60"+phoneno.substring(4);
//        } else if (phoneno.startsWith("600")) {
//            phoneno = "60"+phoneno.substring(3);
//        }

        //create, if exist update
        //get the data
        SmsBlocker smsBlocker = smsBlockerDao.doFindSmsBlockerByPhoneNo(phoneno);

        if(smsBlocker == null){
            doSaveSmsBlocker(phoneno);
        } else {
            int retryCount = smsBlocker.getRetryCount() + 1;
            if (retryCount >= 3) {//only let them retry 3 times
                //compare date now with retryDate,

                Date retryDate = smsBlocker.getDatetimeUpdate();
                long differenceInMinute = ((new Date().getTime() - retryDate.getTime()) / (1000 * 60)% 60);// see if the date > 15 min

                if(differenceInMinute >= 15){//put 15 minute comparison here
                    smsBlocker.setRetryCount(0);
                } else {
                    long retryTime = 15 - differenceInMinute;
                    throw new ValidatorException(i18n.getText("retrySms", locale, new String[]{String.valueOf(retryTime)}));
                }
            } else {
                smsBlocker.setRetryCount(retryCount);
            }
            doUpdateSmsBlocker(smsBlocker);
        }

        String tag = cryptoProvider.isFullAmount() ? generateRandomTag() : "123456";

        SmsTag smsTag = new SmsTag(true);
        smsTag.setSmsTag(tag);
        smsTag.setContact(phoneno);
        smsTag.setTagType(Global.SmsTagType.REGISTER);
        smsTag.setStatus(Global.Status.NEW);
        smsTagDao.save(smsTag);

        log.debug(String.format("======= SMS Tag: %s, PhoneNo %s", tag, smsTag.getContact()));

        String msg = i18n.getText("verifySms", locale, new String[]{tag});
        String platformName;

        // when send sms to China, it need a 'fat' bracket, or else msg will not able to display properly.
        if (phoneno.startsWith("86")) {
            platformName = "【Whale Media】: ";
        } else {
            platformName = "[Whale Media]: ";
        }
        msg = platformName + msg;
        SmsQueue smsQueue = new SmsQueue();
        smsQueue.setBody(msg);
        smsQueue.setSmsTo(phoneno);
        smsQueue.setStatus(SmsQueue.SMS_STATUS_PENDING);
        smsQueueDao.save(smsQueue);
    }

    @Override
    public SmsTag doVerifyRegistrationVerifyCodeWithoutUpdateStatus(Locale locale, String phoneno, String verifyCode, int durationInMinutes) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MINUTE, -durationInMinutes);
        Date date = cal.getTime();

//        //if user enter extra 60 / 0 infront their number
//        if (phoneno.startsWith("6060")) {
//            phoneno = "60"+phoneno.substring(4);
//        } else if (phoneno.startsWith("600")) {
//            phoneno = "60"+phoneno.substring(3);
//        }

        SmsTag smsTag = smsTagDao.findLastNewSmsTagByContact(phoneno);

        if (smsTag == null || date.after(smsTag.getDatetimeAdd())) {
            throw new ValidatorException(i18n.getText("smsVerificationCodeInvalidOrExpired", locale));
        }

        if (!smsTag.getSmsTag().equalsIgnoreCase(verifyCode)) {
            throw new ValidatorException(i18n.getText("smsVerificationCodeInvalidOrExpired", locale));
        }

        return smsTag;
    }

    @Override
    public void updateSmsTagStatusToUsed(SmsTag smsTag) {
        smsTag.setStatus(Global.Status.USED);
        smsTag.setUsedDatatime(new Date());
        smsTagDao.update(smsTag);
    }

    private void sendSms(String mobile, String msg) {
        NexmoConnector connector = new NexmoConnector();
        connector.sendSms(mobile, msg);
    }

    private String generateRandomTag() {
        UniqueNumberGenerator generator = null;
        try {
            generator = new UniqueNumberGenerator(6, true, false, UniqueNumberGenerator.IS_NUMERIC);

            String tag = null;
            do {
                tag = generator.getNewPin();
            } while (tag.startsWith("0"));

            return tag;
        } catch (Exception ex) {
            throw new SystemErrorException(ex);
        } finally {
            if (generator != null)
                generator = null;
        }
    }


    @Override
    public void doUpdateSmsBlocker(SmsBlocker smsBlocker) {
        SmsBlocker dbSmsBlocker = doGetSmsBlocker(smsBlocker.getBlockerId());
        dbSmsBlocker.setRetryCount(smsBlocker.getRetryCount());

        smsBlockerDao.update(dbSmsBlocker);
    }

    @Override
    public void doSaveSmsBlocker(String phoneNo) {
        SmsBlocker smsBlocker = new SmsBlocker(true);
        smsBlocker.setPhoneNo(phoneNo);

        smsBlockerDao.save(smsBlocker);
    }

    @Override
    public SmsBlocker doGetSmsBlocker(String blockerId) {
        return smsBlockerDao.get(blockerId);
    }

    @Override
    public SmsBlocker doFindSmsBlockerByPhoneNumber(String phoneNo) {
        return smsBlockerDao.doFindSmsBlockerByPhoneNo(phoneNo);
    }
}
