package com.compalsolutions.compal;

public class MemberFileUploadConfiguration {
    public static final String BEAN_NAME = "memberFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;
    public static final String FOLDER_NAME = "memberFile";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String geMemberFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullMemberFileParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }

    public String getLocalFullMemberFileParentFileUrl() {
        return "http://localhost:9010" + "/file/" + FOLDER_NAME;
    }


}
