package com.compalsolutions.compal.dapp.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.dapp.vo.DappCat;
import com.compalsolutions.compal.dapp.vo.DappItem;

public interface DappItemDao extends BasicDao<DappItem, String> {
    public static final String BEAN_NAME = "dappItemDao";
}
