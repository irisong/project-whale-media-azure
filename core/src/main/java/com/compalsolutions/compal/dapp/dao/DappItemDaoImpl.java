package com.compalsolutions.compal.dapp.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.dapp.vo.DappCat;
import com.compalsolutions.compal.dapp.vo.DappItem;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(DappItemDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DappItemDaoImpl extends Jpa2Dao<DappItem, String> implements DappItemDao {

    public DappItemDaoImpl() {
        super(new DappItem(false));
    }
}
