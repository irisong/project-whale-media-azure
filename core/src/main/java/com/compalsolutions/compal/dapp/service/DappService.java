package com.compalsolutions.compal.dapp.service;

import com.compalsolutions.compal.dapp.vo.DappCat;

import java.util.List;

public interface DappService {
    public static final String BEAN_NAME = "dappService";

    public List<DappCat> findAllActiveDappCats(Boolean isHide4Test);
}
