package com.compalsolutions.compal.dapp.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "dapp_item")
@Access(AccessType.FIELD)
public class DappItem extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "item_id", unique = true, nullable = false, length = 32)
    private String itemId;

    @Column(name = "cat_id", nullable = false, length = 32)
    private String catId;

    @ToTrim
    @Column(name = "item_name", length = 100, nullable = false)
    private String itemName;

    @ToTrim
    @Column(name = "item_name_zh", length = 100, nullable = false)
    private String itemNameZh;

    @ToTrim
    @Column(name = "item_desc", length = 100)
    private String itemDesc;

    @ToTrim
    @Column(name = "item_desc_zh", length = 100)
    private String itemDescZh;

    @ToTrim
    @Column(name = "item_code", length = 20, nullable = false)
    private String itemCode;

    @ToTrim
    @Column(name = "item_seq")
    private Integer itemSeq;

    @ToTrim
    @Column(name = "icon_url", length = 255, nullable = false)
    private String iconUrl;

    @Column(name = "can_click_detail", nullable = false)
    private Boolean canClickDetail;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public DappItem() {
    }

    public DappItem(boolean defaultValue){
        if(defaultValue){
            status = Global.Status.ACTIVE;
        }
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameZh() {
        return itemNameZh;
    }

    public void setItemNameZh(String itemNameZh) {
        this.itemNameZh = itemNameZh;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public Integer getItemSeq() {
        return itemSeq;
    }

    public void setItemSeq(Integer itemSeq) {
        this.itemSeq = itemSeq;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public String getItemDescZh() {
        return itemDescZh;
    }

    public void setItemDescZh(String itemDescZh) {
        this.itemDescZh = itemDescZh;
    }

    public Boolean getCanClickDetail() {
        return canClickDetail;
    }

    public void setCanClickDetail(Boolean canClickDetail) {
        this.canClickDetail = canClickDetail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DappItem dappItem = (DappItem) o;
        return Objects.equals(itemId, dappItem.itemId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(itemId);
    }
}
