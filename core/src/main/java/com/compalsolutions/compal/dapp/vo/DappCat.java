package com.compalsolutions.compal.dapp.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "dapp_cat")
@Access(AccessType.FIELD)
public class DappCat extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String COL1 = "COL1";
    public static final String COL3 = "COL3";
    public static final String COL6 = "COL6";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "cat_id", unique = true, nullable = false, length = 32)
    private String catId;

    @ToTrim
    @Column(name = "cat_name", length = 100, nullable = false)
    private String catName;

    @ToTrim
    @Column(name = "cat_name_zh", length = 100, nullable = false)
    private String catNameZh;

    @ToTrim
    @ToUpperCase
    @Column(name = "cat_code", length = 20, nullable = false)
    private String catCode;

    @ToTrim
    @ToUpperCase
    @Column(name = "cat_type", length = 20, nullable = false)
    private String catType;

    @Column(name = "cat_seq")
    private Integer catSeq;

    @Column(name = "hide4test", nullable = false)
    private Boolean hide4Test;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "cat_id", insertable = false, updatable = false)
    @OrderBy("itemSeq")
    private List<DappItem> items = new ArrayList<>();

    public DappCat() {
    }

    public DappCat(boolean defaultValue) {
        if(defaultValue){
            status = Global.Status.ACTIVE;
        }
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatNameZh() {
        return catNameZh;
    }

    public void setCatNameZh(String catNameZh) {
        this.catNameZh = catNameZh;
    }

    public String getCatCode() {
        return catCode;
    }

    public void setCatCode(String catCode) {
        this.catCode = catCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DappItem> getItems() {
        return items;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public void setItems(List<DappItem> items) {
        this.items = items;
    }

    public Integer getCatSeq() {
        return catSeq;
    }

    public void setCatSeq(Integer catSeq) {
        this.catSeq = catSeq;
    }

    public Boolean getHide4Test() {
        return hide4Test;
    }

    public void setHide4Test(Boolean hide4Test) {
        this.hide4Test = hide4Test;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DappCat dappCat = (DappCat) o;
        return Objects.equals(catId, dappCat.catId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(catId);
    }
}
