package com.compalsolutions.compal.dapp.service;

import com.compalsolutions.compal.dapp.dao.DappCatDao;
import com.compalsolutions.compal.dapp.dao.DappItemDao;
import com.compalsolutions.compal.dapp.vo.DappCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(DappService.BEAN_NAME)
public class DappServiceImpl implements DappService {
    private DappCatDao dappCatDao;
    private DappItemDao dappItemDao;

    @Autowired
    public DappServiceImpl(DappCatDao dappCatDao, DappItemDao dappItemDao) {
        this.dappCatDao = dappCatDao;
        this.dappItemDao = dappItemDao;
    }

    @Override
    public List<DappCat> findAllActiveDappCats(Boolean isHide4Test) {
        return dappCatDao.findActiveAll(isHide4Test);
    }
}
