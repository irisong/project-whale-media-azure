package com.compalsolutions.compal.dapp.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.dapp.vo.DappCat;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(DappCatDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DappCatDaoImpl extends Jpa2Dao<DappCat, String> implements DappCatDao {

    public DappCatDaoImpl() {
        super(new DappCat(false));
    }

    @Override
    public List<DappCat> findActiveAll(Boolean isHide4Test) {
        DappCat example = new DappCat(false);

        example.setHide4Test(isHide4Test);
        example.setStatus(Global.Status.ACTIVE);
        return findByExample(example, "catSeq");
    }
}
