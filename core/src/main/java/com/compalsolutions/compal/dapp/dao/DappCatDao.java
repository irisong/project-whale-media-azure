package com.compalsolutions.compal.dapp.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.dapp.vo.DappCat;

import java.util.List;

public interface DappCatDao extends BasicDao<DappCat, String> {
    public static final String BEAN_NAME = "dappCatDao";

    public List<DappCat> findActiveAll(Boolean isHide4Test);
}
