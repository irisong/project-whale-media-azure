package com.compalsolutions.compal.security.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.security.vo.JwtToken;

import java.util.Date;
import java.util.List;

public interface JwtTokenDao extends BasicDao<JwtToken, String> {
    public static final String BEAN_NAME = "jwtTokenDao";

    public JwtToken findByJwtId(String jwtId);

    public JwtToken findJwtTokenByRefreshToken(String refreshToken);

    public void closeAllJwtTokenByDeviceAndUser(String userId);

    public void forceCloseAllJwtTokenByDeviceAndUser(String userId);

    public JwtToken findByUserId(String userId);

    public List<JwtToken> findPendingToken();
}
