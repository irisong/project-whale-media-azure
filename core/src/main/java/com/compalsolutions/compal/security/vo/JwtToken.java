package com.compalsolutions.compal.security.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_jwt_token")
@Access(AccessType.FIELD)
public class JwtToken extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String DEVICE_NAME_DEFAULT = "DEFAULT";
    public static final String DEVICE_IOS = "IOS";
    public static final String DEVICE_ANDROID = "ANDROID";
    public static final String DEVICE_POS = "POS";
    public static final String FORCE_LOGOUT = "FORCE_LOGOUT";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String Id;

    @ToTrim
    @Column(name = "token", columnDefinition = Global.ColumnDef.TEXT, nullable = false)
    private String token;

    @ToTrim
    @Column(name = "refresh_token", nullable = false, length = 100)
    private String refreshToken;

    @ToTrim
    @Column(name = "jwt_id", unique = true, nullable = false, length = 100)
    private String jwtId;

    @ToTrim
    @Column(name = "user_id", nullable = false, length = 32)
    private String userId;

    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false, nullable = true)
    private User user;

    @ToUpperCase
    @ToTrim
    @Column(name = "user_type", length = 20, nullable = false)
    private String userType;

    @ToUpperCase
    @ToTrim
    @Column(name = "device_name", length = 20, nullable = false)
    private String deviceName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "expiry_datetime", nullable = false)
    private Date expiryDatetime;

    @ToTrim
    @Column(name = "user_agent", columnDefinition = Global.ColumnDef.TEXT)
    private String userAgent;

    @ToTrim
    @Column(name = "claims_json", columnDefinition = Global.ColumnDef.TEXT, nullable = false)
    private String claimsJson;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public JwtToken() {
    }

    public JwtToken(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getJwtId() {
        return jwtId;
    }

    public void setJwtId(String jwtId) {
        this.jwtId = jwtId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Date getExpiryDatetime() {
        return expiryDatetime;
    }

    public void setExpiryDatetime(Date expiryDatetime) {
        this.expiryDatetime = expiryDatetime;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getClaimsJson() {
        return claimsJson;
    }

    public void setClaimsJson(String claimsJson) {
        this.claimsJson = claimsJson;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((Id == null) ? 0 : Id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        JwtToken other = (JwtToken) obj;
        if (Id == null) {
            if (other.Id != null)
                return false;
        } else if (!Id.equals(other.Id))
            return false;
        return true;
    }

}
