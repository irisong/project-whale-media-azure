package com.compalsolutions.compal.security.dao;

import com.compalsolutions.compal.Global;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.security.vo.JwtToken;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component(JwtTokenDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class JwtTokenDaoImpl extends Jpa2Dao<JwtToken, String> implements JwtTokenDao {
    public JwtTokenDaoImpl() {
        super(new JwtToken(false));
    }

    @Override
    public JwtToken findByJwtId(String jwtId) {
        Validate.notBlank(jwtId);

        if (StringUtils.isBlank(jwtId)) {
            throw new DataException("tokenId can not be blank at JwtTokenDaoImpl.findByTokenId()");
        }

        JwtToken example = new JwtToken(false);
        example.setJwtId(jwtId);

        return findUnique(example);
    }

    @Override
    public JwtToken findJwtTokenByRefreshToken(String refreshToken) {
        Validate.notBlank(refreshToken);

        JwtToken example = new JwtToken(false);
        example.setRefreshToken(refreshToken);

        return findUnique(example);
    }

    @Override
    public void closeAllJwtTokenByDeviceAndUser(String userId) {
        List<Object> params = new ArrayList<>();
        String hql = "update JwtToken j set j.status = ? where j.userId = ? and j.status = ? and j.deviceName in (?, ?)";

        params.add(Global.Status.CLOSED);
        params.add(userId);
        params.add(Global.Status.ACTIVE);
        params.add(JwtToken.DEVICE_ANDROID);
        params.add(JwtToken.DEVICE_IOS);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void forceCloseAllJwtTokenByDeviceAndUser(String userId) {
        List<Object> params = new ArrayList<>();
        String hql = "update JwtToken j set j.status = ? , j.datetimeUpdate = ? , j.expiryDatetime = ? where j.userId = ? and j.status = ? and j.deviceName in (?, ?)";
        Date expiryTime = Calendar.getInstance().getTime();
        expiryTime = DateUtils.addMinutes(expiryTime, 3);
        params.add(JwtToken.FORCE_LOGOUT);
        params.add(new Date());
        params.add(expiryTime);
        params.add(userId);
        params.add(Global.Status.ACTIVE);
        params.add(JwtToken.DEVICE_ANDROID);
        params.add(JwtToken.DEVICE_IOS);


        bulkUpdate(hql, params.toArray());
    }

    @Override
    public JwtToken findByUserId(String userId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + JwtToken.class + " WHERE 1=1 and status =? ";

        hql += " and a.userId=?";
        params.add(Global.Status.ACTIVE);
        params.add(userId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<JwtToken> findPendingToken() {
        List<Object> params = new ArrayList<>();

        String hql = "from a IN " + JwtToken.class + " WHERE 1=1 and status =? and expiryDatetime <= ? ";

        params.add(Global.Status.PENDING);
        params.add(new Date());

        return findQueryAsList(hql, params.toArray());
    }
}
