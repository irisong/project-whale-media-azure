package com.compalsolutions.compal.security;

import java.util.Locale;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;

import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.vo.JwtToken;

public interface TokenProvider {
    public static final String BEAN_NAME = "tokenProvider";

    public static final String CLAIM_ACCESSES = "accesses";

    public JwtClaims createJwtClaims(Locale locale, String userAgent, User user, int tokenDurationInMinutes, String deviceName);

    public String generateJwtToken(JwtClaims claims);

    public Pair<JwtClaims, JwtToken> validateJwtToken(Locale locale, String token) throws InvalidJwtException;

    public User authenticate(Locale locale, String userAgent, String username, String password, String serviceCode,
                                                          String loginKey, int tokenDurationInMinutes, String deviceName);

    Pair<JwtToken, JwtClaims> generateJwtTokenWithLoggedInUser(Locale locale, String userAgent, User user, int tokenDurationInMinutes, String deviceName, boolean forceLogin);

    public void logoutToken(String tokenId);

    public Pair<JwtToken, JwtClaims> generateJwtTokenWithRefreshToken(Locale locale, String userAgent, String refreshToken, int tokenDurationInMinutes)
            throws InvalidJwtException;

    public String getMemberIdByToken(Locale locale, String token) throws InvalidJwtException;

    public void checkIsValidUser(Locale locale, String username, String password);

    void processForceLogin(RunTaskLogger logger);
}
