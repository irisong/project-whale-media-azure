package com.compalsolutions.compal.security.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.security.dao.JwtTokenDao;
import com.compalsolutions.compal.security.vo.JwtToken;

import java.util.Date;
import java.util.List;

@Component(TokenService.BEAN_NAME)
public class TokenServiceImpl implements TokenService {
    private JwtTokenDao jwtTokenDao;

    @Autowired
    public TokenServiceImpl(JwtTokenDao jwtTokenDao) {
        this.jwtTokenDao = jwtTokenDao;
    }

    @Override
    public void saveJwtToken(JwtToken jwtToken) {
        jwtTokenDao.save(jwtToken);
    }

    @Override
    public JwtToken findJwtTokenByJwtId(String tokenId) {
        return jwtTokenDao.findByJwtId(tokenId);
    }

    @Override
    public JwtToken getJwtToken(String tokenId) {
        return jwtTokenDao.get(tokenId);
    }

    @Override
    public void updateJwtToken(JwtToken jwtToken) {
        jwtTokenDao.update(jwtToken);
    }

    @Override
    public JwtToken findJwtTokenByRefreshToken(String refreshToken) {
        return jwtTokenDao.findJwtTokenByRefreshToken(refreshToken);
    }

    @Override
    public void doCloseAllJwtTokenByDeviceAndUser(String userId) {
        jwtTokenDao.closeAllJwtTokenByDeviceAndUser(userId);
    }

    @Override
    public void doForceCloseAllJwtTokenByDeviceAndUser(String userId) {
        jwtTokenDao.forceCloseAllJwtTokenByDeviceAndUser(userId);
    }

    @Override
    public JwtToken findJwtTokenByUserId(String userId) {
        return jwtTokenDao.findByUserId(userId);
    }

    @Override
    public List<JwtToken> findPendingToken() {
        return jwtTokenDao.findPendingToken();
    }

}
