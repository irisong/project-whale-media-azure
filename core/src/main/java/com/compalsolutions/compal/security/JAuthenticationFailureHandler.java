package com.compalsolutions.compal.security;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.web.ServletUtil;

public class JAuthenticationFailureHandler extends SimpleUrlAuthenticationFailureHandler {
    private static final Log log = LogFactory.getLog(JAuthenticationFailureHandler.class);

    private String usernameParameter = SPRING_SECURITY_FORM_USERNAME_KEY;

    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {

        log.debug("login failed~~");

        SessionLogService sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);

        String username = obtainUsername(request);
        String ipAddress = ServletUtil.getRemoteAddr(request);
        sessionLogService.saveSessionLog(username, ipAddress, SessionLog.LOGIN_STATUS_FAILED, null);

        if (ServletUtil.isAjaxRequest(request)) {
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();

            out.print("{\"error\":\"" + SessionLog.LOGIN_STATUS_FAILED + "\"}");
            out.flush();
        } else {
            super.onAuthenticationFailure(request, response, exception);
        }
    }

    private String obtainUsername(HttpServletRequest request) {
        return request.getParameter(usernameParameter);
    }

    // ---------------- GETTER & SETTER (START) -------------

    public void setUsernameParameter(String usernameParameter) {
        this.usernameParameter = usernameParameter;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
