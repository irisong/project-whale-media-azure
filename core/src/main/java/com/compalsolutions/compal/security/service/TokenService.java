package com.compalsolutions.compal.security.service;

import com.compalsolutions.compal.security.vo.JwtToken;

import java.util.List;

public interface TokenService {
    public static final String BEAN_NAME = "tokenService";

    public void saveJwtToken(JwtToken jwtToken);

    public JwtToken findJwtTokenByJwtId(String tokenId);

    public JwtToken getJwtToken(String tokenId);

    public void updateJwtToken(JwtToken jwtToken);

    public JwtToken findJwtTokenByRefreshToken(String refreshToken);

    public void doCloseAllJwtTokenByDeviceAndUser(String userId);

    public void doForceCloseAllJwtTokenByDeviceAndUser(String userId);

    public JwtToken findJwtTokenByUserId(String userId);

    public List<JwtToken> findPendingToken();

}
