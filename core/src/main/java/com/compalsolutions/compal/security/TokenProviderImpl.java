package com.compalsolutions.compal.security;

import java.io.UnsupportedEncodingException;
import java.security.Key;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.omnicplus.OmnicplusClient;
import com.compalsolutions.compal.omnicplus.dto.CredentialsDto;
import com.compalsolutions.compal.omnicplus.dto.OmnicplusDto;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.service.JwtTokenCacheService;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.service.TokenService;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.thirdparty.service.ThirdPartyService;
import com.compalsolutions.compal.thirdparty.vo.ThirdPartyMemberTemp;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.UniqueNumberGenerator;

@Service(TokenProvider.BEAN_NAME)
public class TokenProviderImpl implements TokenProvider {
    private static final Log log = LogFactory.getLog(TokenProviderImpl.class);

    private String secretKey;
    private String issuer;
    private Key key;

    private UserDetailsService userDetailsService;
    private TokenService tokenService;
    private JwtTokenCacheService jwtTokenCacheService;
    private MemberService memberService;

    @Autowired
    public TokenProviderImpl(Environment env, UserDetailsService userDetailsService, TokenService tokenService, JwtTokenCacheService jwtTokenCacheService,
                             MemberService memberService) {
        secretKey = env.getProperty("jeasey.secretKey");
        issuer = env.getProperty("jeasey.issuer");

        try {
            key = new HmacKey(secretKey.getBytes("UTF-8"));
        } catch (UnsupportedEncodingException e) {
            throw new SystemErrorException("unable to create JWT Secret Key");
        }

        this.userDetailsService = userDetailsService;
        this.tokenService = tokenService;
        this.jwtTokenCacheService = jwtTokenCacheService;
        this.memberService = memberService;
    }

    @Override
    public JwtClaims createJwtClaims(Locale locale, String userAgent, User user, int tokenDurationInMinutes, String deviceName) {
        MemberService memberService = Application.lookupBean(MemberService.class);

        if (tokenDurationInMinutes <= 0) {
            throw new SystemErrorException("tokenDurationInMinutes can not less or equal to 0");
        }

        JwtClaims claims = new JwtClaims();
        claims.setIssuer(issuer);
        claims.setExpirationTimeMinutesInTheFuture(tokenDurationInMinutes);
        claims.setSubject(user.getUsername());
        claims.setGeneratedJwtId();
        // do not generate 'issuedAt'
        // claims.setIssuedAtToNow();
        claims.setClaim("userAgent", userAgent);
        claims.setClaim("id", user.getUserId());
        claims.setClaim("type", user.getUserType());
        claims.setClaim("localeCode", locale.toString());
        claims.setClaim("deviceName", deviceName);

        if (user instanceof MemberUser) {
            MemberUser memberUser = (MemberUser) user;
            Member member = memberService.getMember(memberUser.getMemberId());
            // claims.setStringClaim("username", member.getMemberDetail().getNickname());
            claims.setStringClaim("username", member.getMemberCode());
        }

        List<UserAccess> userAccesses = userDetailsService.findUserAuthorizedAccess(user.getUserId());
        if (CollectionUtil.isNotEmpty(userAccesses)) {
            List<String> accesses = userAccesses.stream().map(userAccess -> userAccess.getAccessCode()).collect(Collectors.toList());
            accesses.add("ROLE_USER");

            claims.setStringListClaim(CLAIM_ACCESSES, accesses);
        }

        return claims;
    }

    @Override
    public String generateJwtToken(JwtClaims claims) {
        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setKey(key);
        jws.setDoKeyValidation(false); // relaxes the key length requirement

        try {
            return jws.getCompactSerialization();
        } catch (Exception ex) {
            throw new SystemErrorException(ex.getMessage());
        }
    }

    @Override
    public Pair<JwtClaims, JwtToken> validateJwtToken(Locale locale, String token) throws InvalidJwtException {
        I18n i18n = Application.lookupBean(I18n.class);
        JwtConsumer jwtConsumer = new JwtConsumerBuilder() //
                .setRequireExpirationTime() //
                .setAllowedClockSkewInSeconds(30) //
                .setRequireSubject() //
                .setExpectedIssuer(issuer) //
                .setVerificationKey(key).setRelaxVerificationKeyValidation() // relaxes key length requirement
                .build();

        JwtClaims processedClaims = jwtConsumer.processToClaims(token);
        JwtToken jwtToken = null;

        try {
            jwtToken = jwtTokenCacheService.getJwtToken(processedClaims.getJwtId());

            if (jwtToken == null || !Global.Status.ACTIVE.equals(jwtToken.getStatus()) //
                    || !jwtToken.getToken().equals(token) //
                    || !jwtToken.getUserId().equals(processedClaims.getStringClaimValue("id"))) {
                throw new InvalidJwtException(i18n.getText("invalidToken", locale), Collections.emptyList(), null);
            }
        } catch (MalformedClaimException ex) {
            throw new ValidatorException(ex.getMessage());
        }

        return new MutablePair<>(processedClaims, jwtToken);
    }

    @Override
    public User authenticate(Locale locale, String userAgent, String username, String password, String serviceCode,
                                                          String loginKey, int tokenDurationInMinutes, String deviceName) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        ThirdPartyService thirdPartyService = Application.lookupBean(ThirdPartyService.class);

        boolean hasServiceCodeAndLoginKey = StringUtils.isNotBlank(serviceCode) && StringUtils.isNotBlank(loginKey);

        if (locale == null)
            locale = Application.lookupBean(Locale.class);

        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
        }

        ThirdPartyMemberTemp thirdPartyMemberTemp = null;
        if (hasServiceCodeAndLoginKey) {
            thirdPartyMemberTemp = thirdPartyService.findThirdPartyMemberTempByLoginKeyAndServiceCode(loginKey, serviceCode);

            // throw error if thirdPartyMemberTemp is not exist or being used
            // throw a general message because somebody may try to hack this system
            if (thirdPartyMemberTemp == null || StringUtils.isNotBlank(thirdPartyMemberTemp.getMemberId())) {
                throw new ValidatorException(i18n.getText("errorMessage.generalErrorMessage", locale));
            }
        }

        // check if user from omcwallet/omnicplus
        String userType = getOmnicplusUserType(username, password);
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_INVALID)) {
            throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
        }
        User user;

        // if not omc/omnic+ user, only check from local db
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_NOT_REGISTERED)) {

            user = userDetailsService.findUserByUsername(username);
            if (user == null) {
                throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
            }

            if (!userDetailsService.isEncryptedPasswordMatch(password, user.getPassword())) {
                throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
            }

            // at this point, the user considered logged-in.
            if (hasServiceCodeAndLoginKey) {
                MemberUser memberUser = (MemberUser) user;
                thirdPartyService.doProcessLoginSuccess(null, thirdPartyMemberTemp, memberUser.getMemberId());
            }
        } else {
            // valid login from omc/omnic+ user, need to store user data in local for first time
            user = userDetailsService.findUserByUsername(username);
            if (user == null) {
                memberService.doCreateSimpleMember(username, password);
                user = userDetailsService.findUserByUsername(username);
            }
        }

        return user;

    }

    @Override
    public Pair<JwtToken, JwtClaims> generateJwtTokenWithLoggedInUser(Locale locale, String userAgent, User user, int tokenDurationInMinutes, String deviceName, boolean forceLogin) {
        if (StringUtils.isBlank(deviceName))
            deviceName = JwtToken.DEVICE_NAME_DEFAULT;

        JwtClaims jwtClaims = createJwtClaims(locale, userAgent, user, tokenDurationInMinutes, deviceName);
        String token = generateJwtToken(jwtClaims);
        String refreshToken = generateRefreshToken();
        JwtToken jwtToken;

        if(forceLogin) {
            tokenService.doForceCloseAllJwtTokenByDeviceAndUser(user.getUserId());
        } else {
            // Close other token (allow only single access)
            tokenService.doCloseAllJwtTokenByDeviceAndUser(user.getUserId());
            jwtTokenCacheService.evictAll(); // clear cache
        }

        try {
            jwtToken = new JwtToken(true);
            jwtToken.setToken(token);
            jwtToken.setRefreshToken(refreshToken);
            jwtToken.setJwtId(jwtClaims.getJwtId());
            jwtToken.setUserId(user.getUserId());
            jwtToken.setUserType(user.getUserType());
            jwtToken.setExpiryDatetime(new Date(jwtClaims.getExpirationTime().getValueInMillis()));
            jwtToken.setUserAgent(userAgent);
            jwtToken.setClaimsJson(jwtClaims.toJson());
            jwtToken.setDeviceName(deviceName);

            tokenService.saveJwtToken(jwtToken);
        } catch (MalformedClaimException ex) {
            log.debug(ex.getMessage(), ex);
            throw new ValidatorException(ex.getMessage());
        }

        return new ImmutablePair<>(jwtToken, jwtClaims);
    }

    @Override
    public Pair<JwtToken, JwtClaims> generateJwtTokenWithRefreshToken(Locale locale, String userAgent, String refreshToken, int tokenDurationInMinutes)
            throws InvalidJwtException {
        if (StringUtils.isBlank(refreshToken)) {
            throw new InvalidJwtException("Invalid Token", Collections.emptyList(), null);
        }

        JwtToken jwtToken = tokenService.findJwtTokenByRefreshToken(refreshToken);
        if (jwtToken == null || !Global.Status.ACTIVE.equals(jwtToken.getStatus())) {
            throw new InvalidJwtException("Invalid Token", Collections.emptyList(), null);
        }

        validateJwtToken(locale, jwtToken.getToken());

        Pair<JwtToken, JwtClaims> pair = generateJwtTokenWithLoggedInUser(locale, userAgent, jwtToken.getUser(), tokenDurationInMinutes, jwtToken.getDeviceName(), false);

        // 'close' current token to prevent re-use
        jwtToken.setStatus(Global.Status.CLOSED);
        tokenService.updateJwtToken(jwtToken);

        return pair;
    }

    private String generateRefreshToken() {
        UniqueNumberGenerator generator = null;
        try {
            generator = new UniqueNumberGenerator(50, true, true, UniqueNumberGenerator.IS_ALPHANUM);

            String refreshToken = null;
            do {
                refreshToken = generator.getNewPin();
            } while (tokenService.findJwtTokenByRefreshToken(refreshToken) != null);

            return refreshToken;
        } catch (Exception ex) {
            throw new SystemErrorException(ex);
        }
    }

    private String getOmnicplusUserType(String username, String password) {
        OmnicplusClient omnicplusClient = new OmnicplusClient();
        try {
            CredentialsDto credentialsDto = new CredentialsDto(username, password);
            OmnicplusDto omnicplusDto = omnicplusClient.checkUserLoginCredentialsInOmnicplus(credentialsDto);

            return omnicplusDto.getUserType();
        } catch (Exception ex) {
            throw new SystemErrorException(ex);
        }
    }

    @Override
    public String getMemberIdByToken(Locale locale, String token) throws InvalidJwtException {
        Pair<JwtClaims, JwtToken> pair = validateJwtToken(locale, token);
        JwtToken jwtToken = pair.getRight();

        return ((MemberUser) jwtToken.getUser()).getMemberId();
    }

    @Override
    public void logoutToken(String tokenId) {
        JwtToken jwtToken = tokenService.getJwtToken(tokenId);
        if (jwtToken == null) {
            throw new ValidatorException("Invalid token id");
        }

        jwtToken.setStatus(Global.Status.LOGOUT);
        tokenService.updateJwtToken(jwtToken);

        jwtTokenCacheService.evictJwtToken(jwtToken.getJwtId());
    }

    @Override
    public void processForceLogin(RunTaskLogger logger) {
         TokenService tokenService = Application.lookupBean(TokenService.BEAN_NAME, TokenService.class);
         List<JwtToken> tokenList = tokenService.findPendingToken();
         JwtTokenCacheService jwtTokenCacheService = Application.lookupBean((JwtTokenCacheService.class));
         tokenList.forEach(l -> {
             jwtTokenCacheService.evictJwtToken(l.getJwtId());
             JwtToken jwtToken = tokenService.findJwtTokenByJwtId(l.getJwtId());
             jwtToken.setStatus(JwtToken.FORCE_LOGOUT);
             tokenService.updateJwtToken(jwtToken);
         });
    }

    @Override
    public void checkIsValidUser(Locale locale, String username, String password){
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        ThirdPartyService thirdPartyService = Application.lookupBean(ThirdPartyService.class);

        if (locale == null)
            locale = Application.lookupBean(Locale.class);

        if (StringUtils.isBlank(username) || StringUtils.isBlank(password)) {
            throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
        }

        // check if user from omcwallet/omnicplus
        String userType = getOmnicplusUserType(username, password);
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_INVALID)) {
            throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
        }
        User user;

        // if not omc/omnic+ user, only check from local db
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_NOT_REGISTERED)) {

            user = userDetailsService.findUserByUsername(username);
            if (user == null) {
                throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
            }

            if (!userDetailsService.isEncryptedPasswordMatch(password, user.getPassword())) {
                throw new ValidatorException(i18n.getText("errorMessage.invalid.username.password", locale));
            }
        } else {
            // valid login from omc/omnic+ user, need to store user data in local for first time
            user = userDetailsService.findUserByUsername(username);
            if (user == null) {
                memberService.doCreateSimpleMember(username, password);
                user = userDetailsService.findUserByUsername(username);
            }
        }
    }
}
