package com.compalsolutions.compal.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.service.UserMenuCacheService;
import com.compalsolutions.compal.function.user.vo.User;

public class JLogoutSuccessHandler extends SimpleUrlLogoutSuccessHandler {
    private static final Log log = LogFactory.getLog(JLogoutSuccessHandler.class);

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        UserMenuCacheService userMenuCacheService = Application.lookupBean(UserMenuCacheService.BEAN_NAME, UserMenuCacheService.class);

        if (authentication != null) {
            Object principal = authentication.getPrincipal();
            if (principal instanceof User) {
                User user = (User) principal;
                userMenuCacheService.evictUserMenu(user.getUserId());
                log.debug("evict User Menu success");
            }
        }

        super.onLogoutSuccess(request, response, authentication);

        log.debug("logout success ~");
    }
}
