package com.compalsolutions.compal.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.util.Assert;

public class JAccessDeniedHandler implements AccessDeniedHandler, InitializingBean {
    private String redirectUrl;

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
            throws IOException, ServletException {
        response.sendRedirect(redirectUrl);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Assert.notNull(redirectUrl, "redirectUrl cannot be null");
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
