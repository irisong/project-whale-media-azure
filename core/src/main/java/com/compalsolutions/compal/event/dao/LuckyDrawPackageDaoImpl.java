package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(LuckyDrawPackageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LuckyDrawPackageDaoImpl extends Jpa2Dao<LuckyDrawPackage, String> implements LuckyDrawPackageDao {
    public LuckyDrawPackageDaoImpl() {
        super(new LuckyDrawPackage(false));
    }

    @Override
    public void findLuckyDrawPackageItemForDatagrid(DatagridModel<LuckyDrawPackage> datagridModel, String packageNo, Date luckyDrawDate) {
        Validate.notBlank(packageNo);

        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setPackageNo(packageNo);
        luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);

        findForDatagrid(datagridModel, luckyDrawPackage);
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageByPackageNo(String packageNo, Date luckyDrawDate) {
        Validate.notBlank(packageNo);

        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setPackageNo(packageNo);

        if (luckyDrawDate != null) {
            luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);
        }

        return findFirst(luckyDrawPackage, "datetimeAdd DESC");
    }

    @Override
    public List<LuckyDrawPackage> getLuckyDrawPackageListByPackageNo(String packageNo, Date luckyDrawDate) {
        Validate.notBlank(packageNo);

        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setPackageNo(packageNo);

        if (luckyDrawDate != null) {
            luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);
        }

        return findByExample(luckyDrawPackage, "itemSeq ASC");
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageByMemberId(String memberId, Date luckyDrawDate) {
        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setSenderMemberId(memberId);

        if (luckyDrawDate != null) {
            luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);
        }

        return findFirst(luckyDrawPackage);
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageByItem(String memberId, String itemName, Date luckyDrawDate, String status) {
        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setSenderMemberId(memberId);
        luckyDrawPackage.setItemName(itemName);
        luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);
        luckyDrawPackage.setStatus(status);

        return findFirst(luckyDrawPackage, "itemSeq ASC");
    }

    @Override
    public int getNextLuckyDrawItemSeq(String packageNo, Date luckyDrawDate) {
        Validate.notBlank(packageNo);

        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setPackageNo(packageNo);
        luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);

        luckyDrawPackage = findFirst(luckyDrawPackage, "itemSeq DESC");
        if (luckyDrawPackage != null) {
            return luckyDrawPackage.getItemSeq() + 1;
        }

        return 1;
    }

    @Override
    public LuckyDrawPackage getNextLuckyDrawItem(String memberId, Date luckyDrawDate) {
        LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
        luckyDrawPackage.setSenderMemberId(memberId);
        luckyDrawPackage.setLuckyDrawDate(luckyDrawDate);
        luckyDrawPackage.addNotCondition("remainingQty", 0);
        luckyDrawPackage.setStatus(Global.Status.ACTIVE);

        return findFirst(luckyDrawPackage, "itemSeq ASC");
    }

    @Override
    public boolean isLuckyDrawStarted(String packageNo, Date luckyDrawDate) {
        Validate.notBlank(packageNo);

        String hql = "SELECT count(1) FROM LuckyDrawPackage a WHERE a.packageNo = ? AND a.luckyDrawDate = ? AND a.remainingQty <> a.itemQty ";
        List<Object> params = new ArrayList<>();
        params.add(packageNo);
        params.add(luckyDrawDate);

        Long result = (Long) exFindUnique(hql, params.toArray());
        if (result > 0)
            return true;

        return false;
    }

    @Override
    public void doDeactivateLuckyDrawPackage(String packageNo, Date luckyDrawDate) {
        String hql = "UPDATE LuckyDrawPackage SET status = ? WHERE packageNo = ? ";

        List<Object> params = new ArrayList<>();
        params.add(Global.Status.INACTIVE);
        params.add(packageNo);

        if (luckyDrawDate != null) {
            hql += "AND luckyDrawDate = ? ";
            params.add(luckyDrawDate);
        }

        bulkUpdate(hql, params.toArray());
    }
}
