package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawDefaultWinner;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(LuckyDrawDefaultWinnerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LuckyDrawDefaultWinnerDaoImpl extends Jpa2Dao<LuckyDrawDefaultWinner, String> implements LuckyDrawDefaultWinnerDao {
    public LuckyDrawDefaultWinnerDaoImpl() {
        super(new LuckyDrawDefaultWinner(false));
    }

    @Override
    public void findLuckyDrawDefaultWinnerByItemForDatagrid(DatagridModel<LuckyDrawDefaultWinner> datagridModel, String packageId) {
        LuckyDrawDefaultWinner luckyDrawDefaultWinner = new LuckyDrawDefaultWinner(false);
        luckyDrawDefaultWinner.setPackageId(packageId);

        findForDatagrid(datagridModel, luckyDrawDefaultWinner);
    }

    @Override
    public LuckyDrawDefaultWinner getDefaultWinnerByPackageIdBySeq(String packageId, int winSeq) {
        LuckyDrawDefaultWinner luckyDrawDefaultWinner = new LuckyDrawDefaultWinner(false);
        luckyDrawDefaultWinner.setPackageId(packageId);
        luckyDrawDefaultWinner.setWinSeq(winSeq);

        return findUnique(luckyDrawDefaultWinner);
    }

    @Override
    public boolean isDefaultWinnerSeqExceedItemQty(String packageId, int itemQty) {
        String hql = "SELECT count(1) FROM LuckyDrawDefaultWinner a WHERE a.packageId = ? AND a.winSeq > ? ";

        List<Object> params = new ArrayList<>();
        params.add(packageId);
        params.add(itemQty);

        Long result = (Long) exFindUnique(hql, params.toArray());
        if (result > 0)
            return true;

        return false;
    }

    @Override
    public List<String> getDefaultWinnerIdByPackageNo(String packageNo) {
        String hql = "SELECT a.winnerMemberId FROM LuckyDrawDefaultWinner a WHERE a.packageNo = ? ";

        List<Object> params = new ArrayList<>();
        params.add(packageNo);

        return (List<String>) exFindQueryAsList(hql,params.toArray());
    }

    @Override
    public LuckyDrawDefaultWinner getDefaultWinnerByPackageNoByWinner(String packageNo, String winnerMemberId) {
        LuckyDrawDefaultWinner luckyDrawDefaultWinner = new LuckyDrawDefaultWinner(false);
        luckyDrawDefaultWinner.setPackageNo(packageNo);
        luckyDrawDefaultWinner.setWinnerMemberId(winnerMemberId);

        return findUnique(luckyDrawDefaultWinner);
    }

    @Override
    public void doDeleteDefaultWinnerByPackageId(String packageId) {
        String hql = "DELETE LuckyDrawDefaultWinner WHERE packageId = ? ";

        List<Object> params = new ArrayList<>();
        params.add(packageId);

        bulkUpdate(hql, params.toArray());
    }
}
