package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ev_lucky_draw_package")
@Access(AccessType.FIELD)
public class LuckyDrawPackage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "package_no", nullable = false, length = 32)
    private String packageNo;

    @ToTrim
    @Column(name = "package_name", nullable = false)
    private String packageName;

    @ToTrim
    @Column(name = "package_name_cn", nullable = false)
    private String packageNameCn;

    @Column(name = "sender_member_id", nullable = false, length = 32)
    private String senderMemberId;

    @ManyToOne
    @JoinColumn(name = "sender_member_id", insertable = false, updatable = false)
    private Member senderMember;

    @Temporal(TemporalType.DATE)
    @Column(name = "lucky_draw_date", nullable = false)
    private Date luckyDrawDate;

    @Column(name = "item_seq")
    private Integer itemSeq;

    @Column(name = "item_name", nullable = false, length = 32)
    private String itemName;

    @Column(name = "item_name_cn", nullable = false, length = 32)
    private String itemNameCn;

    @Column(name = "item_qty")
    private Integer itemQty;

    @Column(name = "remaining_qty")
    private Integer remainingQty;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", nullable = false, length = 32)
    private String status;

    @Transient
    private String channelId;

    @Transient
    private Integer currentDrawQty;

    @Transient
    private String message;

    @Transient
    private Boolean luckyDrawStarted;

    public LuckyDrawPackage() {
    }

    public LuckyDrawPackage(boolean defaultValue) {
        if (defaultValue) {
            this.status = Global.Status.ACTIVE;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getPackageNameCn() {
        return packageNameCn;
    }

    public void setPackageNameCn(String packageNameCn) {
        this.packageNameCn = packageNameCn;
    }

    public String getSenderMemberId() {
        return senderMemberId;
    }

    public void setSenderMemberId(String senderMemberId) {
        this.senderMemberId = senderMemberId;
    }

    public Member getSenderMember() {
        return senderMember;
    }

    public void setSenderMember(Member senderMember) {
        this.senderMember = senderMember;
    }

    public Date getLuckyDrawDate() {
        return luckyDrawDate;
    }

    public void setLuckyDrawDate(Date luckyDrawDate) {
        this.luckyDrawDate = luckyDrawDate;
    }

    public Integer getItemSeq() {
        return itemSeq;
    }

    public void setItemSeq(Integer itemSeq) {
        this.itemSeq = itemSeq;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemNameCn() {
        return itemNameCn;
    }

    public void setItemNameCn(String itemNameCn) {
        this.itemNameCn = itemNameCn;
    }

    public Integer getItemQty() {
        return itemQty;
    }

    public void setItemQty(Integer itemQty) {
        this.itemQty = itemQty;
    }

    public Integer getRemainingQty() {
        return remainingQty;
    }

    public void setRemainingQty(Integer remainingQty) {
        this.remainingQty = remainingQty;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public Integer getCurrentDrawQty() {
        return currentDrawQty;
    }

    public void setCurrentDrawQty(Integer currentDrawQty) {
        this.currentDrawQty = currentDrawQty;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getLuckyDrawStarted() {
        return luckyDrawStarted;
    }

    public void setLuckyDrawStarted(Boolean luckyDrawStarted) {
        this.luckyDrawStarted = luckyDrawStarted;
    }
}
