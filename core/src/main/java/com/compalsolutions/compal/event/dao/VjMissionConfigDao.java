package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionConfig;

import java.util.List;

public interface VjMissionConfigDao extends BasicDao<VjMissionConfig, String> {
    public static final String BEAN_NAME = "vjMissionConfigDao";

    public List<VjMissionConfig> getMissionPeriodMinPoint();
}
