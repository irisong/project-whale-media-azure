package com.compalsolutions.compal.event.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.dao.*;
import com.compalsolutions.compal.event.vo.*;
import com.compalsolutions.compal.event.dao.VjMissionConfigDao;
import com.compalsolutions.compal.event.dao.VjMissionDao;
import com.compalsolutions.compal.event.dao.VjMissionDetailDao;
import com.compalsolutions.compal.event.dao.VjMissionSpecialDao;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionConfig;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.event.vo.VjMissionSpecial;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.telegram.BotClient;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Component(VjMissionService.BEAN_NAME)
public class VjMissionServiceImpl implements VjMissionService {
    @Autowired
    private VjMissionConfigDao vjMissionConfigDao;

    @Autowired
    private VjMissionSpecialDao vjMissionSpecialDao;

    @Autowired
    private VjMissionDao vjMissionDao;

    @Autowired
    private VjMissionDetailDao vjMissionDetailDao;

    @Autowired
    private VjMissionRankingDao vjMissionRankingDao;

    @Autowired
    private VjMissionRankingHistoryDao vjMissionRankingHistoryDao;

    private static final Object synchronizedAddMissionObject = new Object();

    @Override
    public void doAddVjMission(Locale locale, String memberId, List<VjMissionDetail> missionDetailList, String missionPeriodId,
                               List<Map<String, String>> specialMissionList) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedAddMissionObject) {
            /************************
             * VERIFICATION - START
             ************************/
            if (StringUtils.isBlank(memberId)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (StringUtils.isBlank(missionPeriodId)) {
                throw new ValidatorException(i18n.getText("mustSelectMissionPeriod", locale));
            }
            VjMissionConfig vjMissionConfig = vjMissionConfigDao.get(missionPeriodId);
            if (vjMissionConfig == null || vjMissionConfig.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                throw new ValidatorException(i18n.getText("invalidMissionPeriod", locale));
            }

            if (missionDetailList == null && specialMissionList.size() == 0) {
                throw new ValidatorException(i18n.getText("mustHaveAtLeastOneMission", locale));
            }

            VjMission vjMission = doGetLatestVjMissionByMemberId(memberId);
            if (vjMission != null) {
                if (vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)) {
                    throw new ValidatorException(i18n.getText("missionStillOngoing", locale));
                } else if (vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_PENDING)) {
                    throw new ValidatorException(i18n.getText("missionPendingApproval", locale));
                }
            }
            /************************
             * VERIFICATION - END
             ************************/

            vjMission = new VjMission(true);
            vjMission.setMemberId(memberId);
            vjMission.setConfigId(missionPeriodId);
            vjMissionDao.save(vjMission);

            if (missionDetailList != null) {
                if (missionDetailList.size() > VjMissionDetail.MISSION_MAX_COUNT) {
                    throw new ValidatorException(i18n.getText("exceedMissionMaxCount", locale));
                }

                for (VjMissionDetail normalMission : missionDetailList) {
                    if (BDUtil.g(normalMission.getPoint(), vjMissionConfig.getMaxPoint())) {
                        throw new ValidatorException(i18n.getText("missionPointExceedSystemMax", locale));
                    }

                    VjMissionDetail vjMissionDetail = new VjMissionDetail(true);
                    vjMissionDetail.setMissionId(vjMission.getMissionId());
                    vjMissionDetail.setName(normalMission.getName());
                    vjMissionDetail.setPrize(normalMission.getPrize());
                    vjMissionDetail.setPoint(normalMission.getPoint());
                    vjMissionDetailDao.save(vjMissionDetail);
                }
            }

            for (Map<String, String> specialMission: specialMissionList) {
                VjMissionSpecial vjMissionSpecial = vjMissionSpecialDao.get(specialMission.get("specialMissionId"));
                if (vjMissionSpecial == null || vjMissionSpecial.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                    throw new ValidatorException(i18n.getText("specialMissionNotFound", locale));
                }

                BigDecimal point = new BigDecimal(specialMission.get("specialMissionPoint"));
                if (BDUtil.l(point, vjMissionConfig.getSpecialMissionMinPoint())) {
                    throw new ValidatorException(i18n.getText("specialMissionPointMustGESystemMin", locale));
                }
                if (BDUtil.g(point, vjMissionConfig.getMaxPoint())) {
                    throw new ValidatorException(i18n.getText("missionPointExceedSystemMax", locale));
                }

                VjMissionDetail vjMissionDetail = new VjMissionDetail(true);
                vjMissionDetail.setMissionId(vjMission.getMissionId());
                vjMissionDetail.setName(vjMissionSpecial.getName());
                vjMissionDetail.setPrize(vjMissionSpecial.getDescription());
                vjMissionDetail.setPoint(point);
                vjMissionDetail.setSpecialMissionId(vjMissionSpecial.getSpecialMissionId());
                vjMissionDetailDao.save(vjMissionDetail);
            }

            String msg = member.getMemberCode() + " [" + member.getMemberDetail().getProfileName() + "] submitted her mission list";
            BotClient botClient = new BotClient();
            botClient.doSendReportMessage(msg);
        }
    }

    @Override
    public VjMission doGetLatestVjMissionByMemberId(String memberId) {
        VjMission vjMission = vjMissionDao.getLatestVjMissionByMemberId(memberId);
        if (vjMission != null) {
            if (vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)
                    && vjMission.getEndDatetime() != null
                    && (vjMission.getEndDatetime().compareTo(new Date()) <= 0)) {
                vjMission.setStatus(VjMission.STATUS_ENDED);
                vjMission.setEndIndicator(true);
                vjMissionDao.update(vjMission);
            }
        }

        return vjMission;
    }

    @Override
    public List<Map<String, Object>> getMissionPeriodMinPoint() {
        List<VjMissionConfig> configList = vjMissionConfigDao.getMissionPeriodMinPoint();

        List<Map<String, Object>> missionPeriodMinPoint = new ArrayList<>();
        for (VjMissionConfig config: configList) {
            Map<String, Object> map = new HashMap<>();
            map.put("id", config.getConfigId());
            map.put("value", config.getMissionPeriod());

            BigDecimal point = config.getMaxPoint() != null ? config.getMaxPoint() : BigDecimal.ZERO;
            map.put("maxPoint", point.intValue()); //return as int
            point = config.getSpecialMissionMinPoint() != null ? config.getSpecialMissionMinPoint() : BigDecimal.ZERO;
            map.put("specialMissionMinPoint", point.intValue()); //return as int

            missionPeriodMinPoint.add(map);
        }

        return missionPeriodMinPoint;
    }

    @Override
    public List<VjMissionSpecial> getSpecialMissionList() {
        return vjMissionSpecialDao.getSpecialMissionList();
    }

    @Override
    public void findMissionRankingListingByMissionId(DatagridModel<VjMissionRanking> datagridModel,  String missionId) {
         vjMissionRankingDao.findMissionRankingListingByMissionId(datagridModel, missionId);
    }

    @Override
    public List<VjMissionRanking> findMissionRankingByMissionId(String missionId) {
        return vjMissionRankingDao.findMissionRankingByMissionId(missionId);
    }

    @Override
    public List<VjMissionRankingHistory> findMissionRankingHistoryByMissionId(String missionId) {
        return vjMissionRankingHistoryDao.findMissionRankingHistoryByMissionId(missionId);
    }

    @Override
    public VjMissionRanking findPersonalRanking(String memberId, String missionId) {
        int rowCount = 1 ;

        for(VjMissionRanking vjMissionRanking : findMissionRankingByMissionId(missionId)) {
            if(memberId.equalsIgnoreCase(vjMissionRanking.getMemberId())) {
                vjMissionRanking.setRowCount(rowCount);
                return vjMissionRanking;
            }
            rowCount++;
        }
        return null;
    }

    @Override
    public Member getSpecialMissionWinner(String missionId) {
        VjMission vjMission = getMissionByMissionId(missionId);
        if (vjMission != null && vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ENDED)) {
            List<VjMissionDetail> vjMissionDetailList = vjMission.getVjMissionDetailList().stream().filter(missionDetail -> StringUtils.isNotBlank(missionDetail.getSpecialMissionId())).collect(Collectors.toList());
            if (vjMissionDetailList.size() > 0) { //has special mission
                List<VjMissionRankingHistory> rankingList = findMissionRankingHistoryByMissionId(vjMission.getMissionId());

                if (rankingList.size() > 0) {
                    VjMissionRankingHistory winnerRanking = rankingList.get(0); //get highest ranking

                    //check if highest rank point hits special mission point
                    if (BDUtil.ge(winnerRanking.getPoint(), vjMissionDetailList.get(0).getPoint())) {
                        Member winnerMember = winnerRanking.getMember();
                        if (winnerMember != null) {
                            winnerMember.setPoint(winnerRanking.getPoint());
                            return winnerMember;
                        }
                    }
                }
            }
        }

        return null;
    }

    @Override
    public VjMissionRanking doCreateMissionRankingIfNotExists(String memberId, String missionId) {
        VjMissionRanking vjMissionRanking = vjMissionRankingDao.findMissionRankingByMemberIdAndMissionId(memberId, missionId);
        if (vjMissionRanking == null) {
            vjMissionRanking = new VjMissionRanking(true);
            vjMissionRanking.setMissionId(missionId);
            vjMissionRanking.setMemberId(memberId);

            vjMissionRankingDao.save(vjMissionRanking);
        } else {
            vjMissionRankingDao.refresh(vjMissionRanking);
        }

        return vjMissionRanking;
    }

    @Override
    public void doIncreasePointForRanking(String memberId, String missionId, BigDecimal point) {
        doCreateMissionRankingIfNotExists(memberId, missionId);
        vjMissionRankingDao.doIncreasePointForRanking(memberId, missionId, point);
    }

    @Override
    public void doProcessMissionComplete() {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MessageService messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        Environment env = Application.lookupBean(Environment.class);
        String serverUrl = env.getProperty("serverUrl");
        NotificationAccount accountTo;

        for (VjMissionRanking vjMissionRanking : vjMissionRankingDao.findIncompleteMission()) {
            BigDecimal currentPoint = vjMissionRanking.getPoint();
            List<VjMissionDetail> vjMissionDetails = vjMissionDetailDao.getCompleteMission(vjMissionRanking.getMissionId(), currentPoint);

            if (vjMissionDetails.size() > vjMissionRanking.getComplete()) {
                for (int i = vjMissionRanking.getComplete(); i < vjMissionDetails.size(); i++) {
                    Member member = memberService.getMember(vjMissionRanking.getMemberId());
                    Message message = messageService.doGenerateMissionPrizeMessage(vjMissionRanking.getMemberId(), vjMissionDetails.get(i));
                    String redirectUrl = serverUrl + "/pub/webView.php?messageId=" + message.getMessageId();
                    Payload payload = new Payload();
                    payload.setType(Payload.NOTICE);
                    payload.setRedirectUrl(redirectUrl + "&languageCode=" + member.getPreferLanguage());
                    accountTo = notificationService.findNotifAccByMemberAndAccessModule(vjMissionRanking.getMemberId(), Global.AccessModule.WHALE_MEDIA);
                    notificationConfProvider.doSendMessage(account.getAccountId(), accountTo.getAccountId(), Global.AccessModule.WHALE_MEDIA,
                            i18n.getText("title.mission", new Locale(member.getPreferLanguage())),payload,null);
                }
                vjMissionRanking.setComplete(vjMissionDetails.size());
            }
            vjMissionRanking.setStatus(VjMissionRanking.CHECKED);
            vjMissionRankingDao.update(vjMissionRanking);
        }
    }

    @Override
    public void doProcessEndedMission() {
        vjMissionDao.doProcessEndMission();

        List<VjMissionRankingHistory> vjMissionRankingHistories = new ArrayList<>();
        List<VjMission> vjMissionsList = vjMissionDao.getNewEndedMission();
        for(VjMission vjMission : vjMissionsList) {

            List<VjMissionRanking> vjMissionRankingList = vjMissionRankingDao.findMissionRankingByMissionId(vjMission.getMissionId());
            for(VjMissionRanking vjMissionRanking : vjMissionRankingList) {
                VjMissionRankingHistory vjMissionRankingHistory = new VjMissionRankingHistory(false);
                vjMissionRankingHistory.setMissionId(vjMissionRanking.getMissionId());
                vjMissionRankingHistory.setMemberId(vjMissionRanking.getMemberId());
                vjMissionRankingHistory.setPoint(vjMissionRanking.getPoint());
                vjMissionRankingHistory.setStatus(vjMissionRanking.getStatus());
                vjMissionRankingHistory.setComplete(vjMissionRanking.getComplete());
                vjMissionRankingHistory.setLastPointGain(vjMissionRanking.getLastPointGain());
                vjMissionRankingHistories.add(vjMissionRankingHistory);
            }
            vjMission.setEndIndicator(false);
            vjMissionDao.update(vjMission);
            vjMissionRankingDao.deleteAll(vjMissionRankingList);

        }
        vjMissionRankingHistoryDao.saveAll(vjMissionRankingHistories);

        for(VjMission vjMission : vjMissionsList) {
            Member member = getSpecialMissionWinner(vjMission.getMissionId());
            if (member != null) {
                String msg = member.getMemberCode() + "(" + member.getMemberDetail().getProfileName() + ") have gain Rank No.1 in the mission from " + vjMission.getMember().getMemberCode()
                        + "(" + vjMission.getMember().getMemberDetail().getProfileName() + ")";

                BotClient botClient = new BotClient();
                botClient.doSendReportMessage(msg);
            }
        }
    }

    @Override
    public void doGetMissionForListing(DatagridModel<VjMission> datagridModel, String memberId, String status) {
        vjMissionDao.findMissionForDatagrid(datagridModel, memberId, status);

        for(VjMission vjMission : datagridModel.getRecords()){
            if(vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)
                    && vjMission.getEndDatetime() != null
                    && (vjMission.getEndDatetime().compareTo(new Date()) <= 0)) {
                vjMission.setStatus(VjMission.STATUS_ENDED);
                vjMission.setEndIndicator(true);
                vjMissionDao.update(vjMission);
            }
        }
    }

    @Override
    public VjMission getMissionByMissionId(String missionId) {
        return vjMissionDao.get(missionId);
    }

    @Override
    public void doApproveMission(Locale locale, String missionId) {
        I18n i18n = Application.lookupBean(I18n.class);

        VjMission vjMission = vjMissionDao.get(missionId);
        if (vjMission == null) {
            throw new ValidatorException(i18n.getText("missionNotExists", locale));
        }

        VjMissionConfig vjMissionConfig = vjMissionConfigDao.get(vjMission.getConfigId());
        if (vjMissionConfig == null) {
            throw new ValidatorException(i18n.getText("missionConfigNotExists", locale));
        }

        vjMission.setStatus(Global.Status.ACTIVE);
        vjMission.setStartDatetime(new Date());
        vjMission.setEndDatetime(DateUtil.addDate(vjMission.getStartDatetime(), Integer.parseInt(vjMissionConfig.getMissionPeriod())));
        vjMissionDao.update(vjMission);

        doProcessVjMissionNotification(vjMission);
    }

    @Override
    public void doRejectMission(Locale locale, String missionId, String remark) {
        I18n i18n = Application.lookupBean(I18n.class);

        VjMission vjMission = vjMissionDao.get(missionId);
        if (vjMission != null) {
            vjMission.setStatus(Global.Status.REJECTED);
            vjMission.setRemark(remark);
            vjMissionDao.update(vjMission);
        } else {
            throw new ValidatorException(i18n.getText("missionNotExists", locale));
        }

        doProcessVjMissionNotification(vjMission);
    }

    @Override
    public void doProcessVjMissionNotification(VjMission vjMission) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        Member member = memberService.getMember(vjMission.getMemberId());
        if (member != null) {
            // send notification to yun xin for notification pop up
            Payload payload = new Payload();
            payload.setType(Payload.VJ_MISSION_REQUEST);

            NotificationAccount frmAcct = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
            NotificationAccount toAcct = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);
            if (frmAcct != null && toAcct != null) {
                String content = i18n.getText("vjMissionRequestApproved", new Locale(member.getPreferLanguage()));
                if (vjMission.getStatus().equalsIgnoreCase(Global.Status.REJECTED)) {
                    content = i18n.getText("vjMissionRequestRejected", new Locale(member.getPreferLanguage()), vjMission.getRemark());
                }

                notificationConfProvider.doSendMessage(frmAcct.getAccountId(), toAcct.getAccountId(), Global.AccessModule.WHALE_MEDIA, content, payload, null);
            }
        }
    }

    @Override
    public void getVjMissionDetailForlisting(DatagridModel<VjMissionDetail> datagridModel, String missionId){
        vjMissionDetailDao.findVjMissionDetailForListing(datagridModel, missionId);
    }
}