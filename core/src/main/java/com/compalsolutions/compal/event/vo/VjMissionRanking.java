package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ev_vj_mission_ranking")
@Access(AccessType.FIELD)
public class VjMissionRanking extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String PENDING = Global.Status.PENDING;
    public static final String CHECKED = Global.Status.CHECKED;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "mission_id", nullable = false, length = 32)
    private String missionId;

    @ManyToOne
    @JoinColumn(name = "mission_id", insertable = false, updatable = false)
    private VjMission vjMission;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @Column(name = "point", nullable = false)
    private BigDecimal point;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "complete", nullable = false)
    private Integer complete;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_point_gain")
    private Date lastPointGain;

    @Transient
    private Integer rowCount;

    public VjMissionRanking() {
    }

    public VjMissionRanking(boolean defaultValue) {
        if (defaultValue) {
           this.status = PENDING;
           this.point = new BigDecimal(0);
           this.complete = 0;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public VjMission getVjMission() {
        return vjMission;
    }

    public void setVjMission(VjMission vjMission) {
        this.vjMission = vjMission;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getRowCount() {
        return rowCount;
    }

    public void setRowCount(Integer rowCount) {
        this.rowCount = rowCount;
    }

    public Integer getComplete() {
        return complete;
    }

    public void setComplete(Integer complete) {
        this.complete = complete;
    }

    public Date getLastPointGain() {
        return lastPointGain;
    }

    public void setLastPointGain(Date lastPointGain) {
        this.lastPointGain = lastPointGain;
    }
}
