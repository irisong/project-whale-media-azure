package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.event.vo.VjMissionConfig;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(VjMissionConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjMissionConfigDaoImpl extends Jpa2Dao<VjMissionConfig, String> implements VjMissionConfigDao {
    public VjMissionConfigDaoImpl() {
        super(new VjMissionConfig(false));
    }

    @Override
    public List<VjMissionConfig> getMissionPeriodMinPoint() {
        VjMissionConfig vjMissionConfig = new VjMissionConfig(false);
        vjMissionConfig.setStatus(Global.Status.ACTIVE);

        return findByExample(vjMissionConfig, "sequence");
    }
}
