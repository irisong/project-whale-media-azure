package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawDefaultWinner;

import java.util.List;

public interface LuckyDrawDefaultWinnerDao extends BasicDao<LuckyDrawDefaultWinner, String> {
    public static final String BEAN_NAME = "luckyDrawDefaultWinnerDao";

    public void findLuckyDrawDefaultWinnerByItemForDatagrid(DatagridModel<LuckyDrawDefaultWinner> datagridModel, String packageId);

    public LuckyDrawDefaultWinner getDefaultWinnerByPackageIdBySeq(String packageId, int winSeq);

    public boolean isDefaultWinnerSeqExceedItemQty(String packageId, int itemQty);

    public List<String> getDefaultWinnerIdByPackageNo(String packageNo);

    public LuckyDrawDefaultWinner getDefaultWinnerByPackageNoByWinner(String packageNo, String winnerMemberId);

    public void doDeleteDefaultWinnerByPackageId(String packageId);
}
