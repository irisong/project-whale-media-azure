package com.compalsolutions.compal.event.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MemberFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.event.dao.LuckyDrawDefaultWinnerDao;
import com.compalsolutions.compal.event.dao.LuckyDrawPackageDao;
import com.compalsolutions.compal.event.dao.LuckyDrawPackageSqlDao;
import com.compalsolutions.compal.event.dao.LuckyDrawWinnerDao;
import com.compalsolutions.compal.event.vo.LuckyDrawDefaultWinner;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.event.vo.LuckyDrawWinner;
import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(LuckyDrawService.BEAN_NAME)
public class LuckyDrawServiceImpl implements LuckyDrawService {
    @Autowired
    private LuckyDrawPackageDao luckyDrawPackageDao;

    @Autowired
    private LuckyDrawPackageSqlDao luckyDrawPackageSqlDao;

    @Autowired
    private LuckyDrawWinnerDao luckyDrawWinnerDao;

    @Autowired
    private LuckyDrawDefaultWinnerDao luckyDrawDefaultWinnerDao;

    @Override
    public void findLuckyDrawPackageForListing(DatagridModel<LuckyDrawPackage> datagridModel, String memberCode, Date dateFrom, Date dateTo, String status) {
        luckyDrawPackageSqlDao.findLuckyDrawPackageForDatagrid(datagridModel, memberCode, dateFrom, dateTo, status);
    }

    @Override
    public void findLuckyDrawPackageItemForListing(DatagridModel<LuckyDrawPackage> datagridModel, String packageNo, Date luckyDrawDate) {
        luckyDrawPackageDao.findLuckyDrawPackageItemForDatagrid(datagridModel, packageNo, luckyDrawDate);
    }

    @Override
    public void findLuckyDrawDefaultWinnerByItemForListing(DatagridModel<LuckyDrawDefaultWinner> datagridModel, String packageId) {
        luckyDrawDefaultWinnerDao.findLuckyDrawDefaultWinnerByItemForDatagrid(datagridModel, packageId);
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageById(String id) {
        return luckyDrawPackageDao.get(id);
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageByPackageNo(String packageNo, Date luckyDrawDate) {
        return luckyDrawPackageDao.getLuckyDrawPackageByPackageNo(packageNo, luckyDrawDate);
    }

    @Override
    public List<LuckyDrawPackage> getLuckyDrawPackageListByPackageNo(String packageNo, Date luckyDrawDate) {
        return luckyDrawPackageDao.getLuckyDrawPackageListByPackageNo(packageNo, luckyDrawDate);
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageByMemberId(String memberId, Date luckyDrawDate) {
        return luckyDrawPackageDao.getLuckyDrawPackageByMemberId(memberId, luckyDrawDate);
    }

    @Override
    public LuckyDrawPackage getLuckyDrawPackageByItem(String memberId, String itemName, Date luckyDrawDate, String status) {
        return luckyDrawPackageDao.getLuckyDrawPackageByItem(memberId, itemName, luckyDrawDate, status);
    }

    @Override
    public int getNextLuckyDrawItemSeq(String packageNo, Date luckyDrawDate) {
        return luckyDrawPackageDao.getNextLuckyDrawItemSeq(packageNo, luckyDrawDate);
    }

    @Override
    public LuckyDrawPackage getNextLuckyDrawItem(String memberId, Date luckyDrawDate) {
        return luckyDrawPackageDao.getNextLuckyDrawItem(memberId, luckyDrawDate);
    }

    @Override
    public int getLatestWinnerSeq(String packageId) {
        return luckyDrawWinnerDao.getLatestWinnerSeq(packageId);
    }

    @Override
    public LuckyDrawDefaultWinner getDefaultWinner(String id) {
        return luckyDrawDefaultWinnerDao.get(id);
    }

    @Override
    public LuckyDrawDefaultWinner getDefaultWinnerByPackageIdBySeq(String packageId, int winSeq) {
        return luckyDrawDefaultWinnerDao.getDefaultWinnerByPackageIdBySeq(packageId, winSeq);
    }

    @Override
    public boolean isDefaultWinnerSeqExceedItemQty(String packageId, int itemQty) {
        return luckyDrawDefaultWinnerDao.isDefaultWinnerSeqExceedItemQty(packageId, itemQty);
    }

    @Override
    public List<String> getDefaultWinnerIdByPackageNo(String packageNo) {
        return luckyDrawDefaultWinnerDao.getDefaultWinnerIdByPackageNo(packageNo);
    }

    @Override
    public boolean isLuckyDrawStarted(String packageNo, Date luckyDrawDate) {
        return luckyDrawPackageDao.isLuckyDrawStarted(packageNo, luckyDrawDate);
    }

    @Override
    public boolean isDefaultWinnerExist(String packageNo, String senderMemberId) {
        LuckyDrawDefaultWinner luckyDrawDefaultWinner = luckyDrawDefaultWinnerDao.getDefaultWinnerByPackageNoByWinner(packageNo, senderMemberId);
        if (luckyDrawDefaultWinner != null) {
            return true;
        }

        return false;
    }

    private static final Object synchronizedAddPackageObject = new Object();

    @Override
    public LuckyDrawPackage doSaveNewLuckyDrawPackage(Locale locale, LuckyDrawPackage luckyDrawPackage, String memberCode, String saveType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);

        synchronized (synchronizedAddPackageObject) {
            /************************
             * VERIFICATION - START
             ************************/
            if (StringUtils.isBlank(memberCode)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            Member member = memberService.findMemberByMemberCode(memberCode);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            LuckyDrawPackage luckyDrawPackageDb;
            if (saveType.equalsIgnoreCase("NEW")) {
                luckyDrawPackageDb = getNextLuckyDrawItem(member.getMemberId(), luckyDrawPackage.getLuckyDrawDate());
                if (luckyDrawPackageDb != null) {
                    throw new ValidatorException(i18n.getText("eachMemberOnlyOneLuckyDrawEachDay", locale));
                }
            }
            /************************
             * VERIFICATION - END
             ************************/

            luckyDrawPackageDb = new LuckyDrawPackage(true);

            if (saveType.equalsIgnoreCase("NEW")) {
                luckyDrawPackageDb.setPackageNo(docNoService.doGetNextLuckyDrawPackageNo());
            } else {
                luckyDrawPackageDb.setPackageNo(luckyDrawPackage.getPackageNo());
            }

            luckyDrawPackageDb.setPackageName(luckyDrawPackage.getPackageName());
            luckyDrawPackageDb.setPackageNameCn(luckyDrawPackage.getPackageNameCn());
            luckyDrawPackageDb.setSenderMemberId(member.getMemberId());
            luckyDrawPackageDb.setLuckyDrawDate(luckyDrawPackage.getLuckyDrawDate());
            luckyDrawPackageDb.setItemName(luckyDrawPackage.getItemName());
            luckyDrawPackageDb.setItemNameCn(luckyDrawPackage.getItemNameCn());
            luckyDrawPackageDb.setItemQty(luckyDrawPackage.getItemQty());
            luckyDrawPackageDb.setRemainingQty(luckyDrawPackageDb.getItemQty());
            luckyDrawPackageDb.setItemSeq(getNextLuckyDrawItemSeq(luckyDrawPackageDb.getPackageNo(), luckyDrawPackageDb.getLuckyDrawDate()));
            luckyDrawPackageDao.save(luckyDrawPackageDb);

            return luckyDrawPackageDb;
        }
    }

    private static final Object synchronizedUpdatePackageObject = new Object();

    @Override
    public void doDeactivateLuckyDrawPackage(String packageNo, Date luckyDrawDate) {
        synchronized (synchronizedUpdatePackageObject) {
            luckyDrawPackageDao.doDeactivateLuckyDrawPackage(packageNo, luckyDrawDate);
        }
    }

    @Override
    public LuckyDrawPackage doUpdateLuckyDrawPackage(Locale locale, LuckyDrawPackage luckyDrawPackage) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedUpdatePackageObject) {
            /************************
             * VERIFICATION - START
             ************************/
            LuckyDrawPackage luckyDrawPackageDb = getLuckyDrawPackageById(luckyDrawPackage.getId());
            if (luckyDrawPackageDb == null) {
                throw new ValidatorException(i18n.getText("invalidLuckyDrawPackage", locale));
            }

            boolean seqExceedQty = isDefaultWinnerSeqExceedItemQty(luckyDrawPackage.getId(), luckyDrawPackage.getItemQty());
            if (seqExceedQty == true) {
                throw new ValidatorException(i18n.getText("luckyDrawDefaultWinnerExceedItemQty", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

            luckyDrawPackageDb.setItemName(luckyDrawPackage.getItemName());
            luckyDrawPackageDb.setItemNameCn(luckyDrawPackage.getItemNameCn());
            luckyDrawPackageDb.setItemQty(luckyDrawPackage.getItemQty());
            luckyDrawPackageDb.setRemainingQty(luckyDrawPackageDb.getItemQty());
            luckyDrawPackageDao.update(luckyDrawPackageDb);

            return luckyDrawPackageDb;
        }
    }

    @Override
    public void doDeleteLuckyDrawPackageByItem(Locale locale, String packageId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedUpdatePackageObject) {
            /************************
             * VERIFICATION - START
             ************************/
            LuckyDrawPackage luckyDrawPackage = getLuckyDrawPackageById(packageId);
            if (luckyDrawPackage == null) {
                throw new ValidatorException(i18n.getText("invalidLuckyDrawPackage", locale));
            }

            List<LuckyDrawPackage> luckyDrawPackageList = getLuckyDrawPackageListByPackageNo(luckyDrawPackage.getPackageNo(), luckyDrawPackage.getLuckyDrawDate());
            if (luckyDrawPackageList.size() <= 1) {
                throw new ValidatorException(i18n.getText("luckyDrawPackageMustHaveAtLeastOneItem", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

            doDeleteAllDefaultWinnerByPackageId(packageId);
            luckyDrawPackageDao.delete(luckyDrawPackage);
        }
    }

    private static final Object synchronizedAddDefaultWinnerObject = new Object();

    @Override
    public void doSaveLuckyDrawDefaultWinner(Locale locale, LuckyDrawDefaultWinner luckyDrawDefaultWinner, String memberCode) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedAddDefaultWinnerObject) {
            /************************
             * VERIFICATION - START
             ************************/
            if (StringUtils.isBlank(memberCode)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            Member member = memberService.findMemberByMemberCode(memberCode);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (member.getMemberId().equals(luckyDrawDefaultWinner.getLuckyDrawPackage().getSenderMemberId())) {
                throw new ValidatorException(i18n.getText("notAllowVJAsDefaultWinner", locale));
            }

            boolean defaultWinnerExist = isDefaultWinnerExist(luckyDrawDefaultWinner.getPackageNo(), member.getMemberId());
            if (defaultWinnerExist == true) {
                throw new ValidatorException(i18n.getText("eachMemberCanOnlyWinLuckyDrawOnce", locale));
            }

            LuckyDrawDefaultWinner luckyDrawDefaultWinnerDb = getDefaultWinnerByPackageIdBySeq(luckyDrawDefaultWinner.getPackageId(), luckyDrawDefaultWinner.getWinSeq());
            if (luckyDrawDefaultWinnerDb != null) {
                throw new ValidatorException(i18n.getText("duplicateWinSeq", locale));
            }

            if (luckyDrawDefaultWinner.getWinSeq() == 0 || (luckyDrawDefaultWinner.getWinSeq() > luckyDrawDefaultWinner.getLuckyDrawPackage().getItemQty())) {
                throw new ValidatorException(i18n.getText("invalidSequence", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

            luckyDrawDefaultWinnerDb = new LuckyDrawDefaultWinner(true);
            luckyDrawDefaultWinnerDb.setPackageNo(luckyDrawDefaultWinner.getPackageNo());
            luckyDrawDefaultWinnerDb.setPackageId(luckyDrawDefaultWinner.getPackageId());
            luckyDrawDefaultWinnerDb.setWinnerMemberId(member.getMemberId());
            luckyDrawDefaultWinnerDb.setWinSeq(luckyDrawDefaultWinner.getWinSeq());
            luckyDrawDefaultWinnerDao.save(luckyDrawDefaultWinnerDb);
        }
    }

    private static final Object synchronizedDeleteDefaultWinnerObject = new Object();

    @Override
    public void doDeleteLuckyDrawDefaultWinner(Locale locale, String id) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedDeleteDefaultWinnerObject) {
            /************************
             * VERIFICATION - START
             ************************/
            LuckyDrawDefaultWinner luckyDrawDefaultWinner = getDefaultWinner(id);
            if (luckyDrawDefaultWinner == null) {
                throw new ValidatorException(i18n.getText("invalidLuckyDrawDefaultWinner", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

            luckyDrawDefaultWinnerDao.delete(luckyDrawDefaultWinner);
        }
    }

    @Override
    public void doDeleteAllDefaultWinnerByPackageId(String packageId) {
        luckyDrawDefaultWinnerDao.doDeleteDefaultWinnerByPackageId(packageId);
    }

    @Override
    public List<String> getLuckyDrawDefaultWinnerbySenderId(String senderMemberId) {
        List<String> luckyDrawDefaultWinner = new ArrayList<>();
        LuckyDrawPackage luckyDrawPackage = getLuckyDrawPackageByMemberId(senderMemberId, new Date());
        if (luckyDrawPackage != null) {
            luckyDrawDefaultWinner = luckyDrawDefaultWinnerDao.getDefaultWinnerIdByPackageNo(luckyDrawPackage.getPackageNo());
        }

        return luckyDrawDefaultWinner;
    }

    private static final Object synchronizedProcessLuckyDrawObject = new Object();

    @Override
    public LuckyDrawPackage doProcessLuckyDraw(Locale locale, LuckyDrawPackage luckyDrawInfo) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        LiveChatRoomProvider liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.BEAN_NAME, LiveChatRoomProvider.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.BEAN_NAME, LiveStreamService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        MemberFileUploadConfiguration memberFileUploadConfiguration = Application.lookupBean(MemberFileUploadConfiguration.class);

        synchronized (synchronizedProcessLuckyDrawObject) {
            /************************
             * VERIFICATION - START
             ************************/
            if (StringUtils.isBlank(luckyDrawInfo.getChannelId())) {
                throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
            }
            if (StringUtils.isBlank(luckyDrawInfo.getItemName())) {
                throw new ValidatorException(i18n.getText("invalidLuckyDrawItem", locale));
            }
            if (luckyDrawInfo.getCurrentDrawQty() == null) {
                throw new ValidatorException(i18n.getText("invalidQuantity", locale));
            }

            LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannel(luckyDrawInfo.getChannelId(), null, false);
            if (liveStreamChannel == null) {
                throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
            }

            LuckyDrawPackage luckyDrawPackage = getLuckyDrawPackageByItem(liveStreamChannel.getMemberId(), luckyDrawInfo.getItemName(), new Date(), Global.Status.ACTIVE);
            if (luckyDrawPackage == null) {
                throw new ValidatorException(i18n.getText("invalidLuckyDrawPackage", locale));
            }
            if (luckyDrawInfo.getCurrentDrawQty() > luckyDrawPackage.getRemainingQty()) {
                throw new ValidatorException(i18n.getText("drawQtyExceedRemainingQty", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

            for (int i = 1; i <= luckyDrawInfo.getCurrentDrawQty(); i++) {
                String winnerMemberId = null;
                String remark = "";

                int winnerSeq = getLatestWinnerSeq(luckyDrawPackage.getId());
                LuckyDrawDefaultWinner luckyDrawDefaultWinner = getDefaultWinnerByPackageIdBySeq(luckyDrawPackage.getId(), winnerSeq + 1);
                if (luckyDrawDefaultWinner != null) {
                    winnerMemberId = luckyDrawDefaultWinner.getWinnerMemberId();
                    remark = "Default Winner";
                } else {
                    // random pick from active user that no won before
                    List<String> memberList = liveStreamService.findEligibleWinnerForLuckyDraw(liveStreamChannel.getChannelId(), luckyDrawPackage.getPackageNo());
                    String[] memberIdArray = memberList.toArray(new String[memberList.size()]);
                    if (memberIdArray.length != 0) { // still have active member that dint win lucky draw
                        winnerMemberId = Global.getRandomString(memberIdArray);
                    }
                }

                if (winnerMemberId == null) {
                    luckyDrawPackage.setMessage(i18n.getText("noActiveMemberEligibleToBeWinner", locale));
                    break;
                }

                LuckyDrawWinner luckyDrawWinner = new LuckyDrawWinner(true);
                luckyDrawWinner.setWinnerMemberId(winnerMemberId);
                luckyDrawWinner.setChannelId(liveStreamChannel.getChannelId());
                luckyDrawWinner.setPackageNo(luckyDrawPackage.getPackageNo());
                luckyDrawWinner.setPackageId(luckyDrawPackage.getId());
                luckyDrawWinner.setWinSeq(winnerSeq + 1);
                luckyDrawWinner.setLuckyDrawDatetime(new Date());
                luckyDrawWinner.setRemark(remark);
                luckyDrawWinnerDao.save(luckyDrawWinner);

                luckyDrawPackage.setRemainingQty(luckyDrawPackage.getRemainingQty() - 1);
                luckyDrawPackageDao.update(luckyDrawPackage);

                // display winner in chat room
                NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(luckyDrawWinner.getWinnerMemberId(), Global.AccessModule.WHALE_MEDIA);
                if (account != null) {
                    Gson contentObject = new Gson();

                    HashMap<String, String> localizePackage = new HashMap<>();
                    localizePackage.put(Global.Language.ENGLISH, luckyDrawPackage.getPackageName() + " lucky draw");
                    localizePackage.put(Global.Language.CHINESE, luckyDrawPackage.getPackageNameCn() + "抽奖活动");

                    HashMap<String, String> localizeItem = new HashMap<>();
                    localizeItem.put(Global.Language.ENGLISH, luckyDrawPackage.getItemName());
                    localizeItem.put(Global.Language.CHINESE, luckyDrawPackage.getItemNameCn());

                    HashMap<String, Object> map = new HashMap<>();
                    map.put("luckyDrawName", localizePackage);
                    map.put("itemName", localizeItem);

                    Member winnerMember = memberService.getMember(luckyDrawWinner.getWinnerMemberId());
                    MemberDetail memberDetail = memberService.findMemberDetailByMemberId(winnerMember.getMemberId());
                    MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(winnerMember.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

                    final String parentUrl = memberFileUploadConfiguration.getFullMemberFileParentFileUrl();
                    if (memberFile != null) {
                        memberFile.setFileUrlWithParentPath(parentUrl);
                    }

                    String senderName = memberDetail.getProfileName();
                    String senderAvatarUrl = memberFile != null ? memberFile.getFileUrl() : "";
                    String contentObjectJsonString = contentObject.toJson(map);

                    liveChatRoomProvider.doSendMessageToChatRoom("REDIRECT", liveStreamChannel.getChannelId(), account.getAccountId(), senderName, senderAvatarUrl,
                            contentObjectJsonString, "LUCKY_DRAW", liveStreamChannel.getMemberId(), winnerMember, "", 0);
                }
            }

            if (luckyDrawPackage.getRemainingQty() == 0) {
                luckyDrawPackage = getNextLuckyDrawItem(luckyDrawPackage.getSenderMemberId(), luckyDrawPackage.getLuckyDrawDate());
            }

            return luckyDrawPackage;
        }
    }
}