package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.event.vo.VjMissionRankingHistory;

import java.util.List;

public interface VjMissionRankingHistoryDao extends BasicDao<VjMissionRankingHistory, String> {
    public static final String BEAN_NAME = "vjMissionRankingHistoryDao";

    public List<VjMissionRankingHistory> findMissionRankingHistoryByMissionId(String missionId);
}
