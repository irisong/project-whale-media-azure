package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.event.vo.VjMissionRanking;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component(VjMissionDetailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjMissionDetailDaoImpl extends Jpa2Dao<VjMissionDetail, String> implements VjMissionDetailDao {
    public VjMissionDetailDaoImpl() {
        super(new VjMissionDetail(false));
    }

    @Override
    public List<VjMissionDetail> getCompleteMission(String missionId, BigDecimal point) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM VjMissionDetail where missionId = ? AND specialMissionId IS NULL AND point <= ? ORDER BY POINT ASC";

        params.add(missionId);
        params.add(point);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findVjMissionDetailForListing(DatagridModel<VjMissionDetail> datagridModel, String missionId) {
        Validate.notBlank(missionId);

        VjMissionDetail vjMissionDetail = new VjMissionDetail(false);
        vjMissionDetail.setMissionId(missionId);

        findForDatagrid(datagridModel, vjMissionDetail);
    }
}
