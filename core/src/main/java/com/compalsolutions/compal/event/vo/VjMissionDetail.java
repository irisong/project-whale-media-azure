package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ev_vj_mission_detail")
@Access(AccessType.FIELD)
public class VjMissionDetail extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final int MISSION_MAX_COUNT = 10;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "detail_id", unique = true, nullable = false, length = 32)
    private String detailId;

    @Column(name = "mission_id", nullable = false, length = 32)
    private String missionId;

    @ManyToOne
    @JoinColumn(name = "mission_id", insertable = false, updatable = false)
    private VjMission vjMission;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "prize")
    private String prize;

    @Column(name = "point", nullable = false)
    private BigDecimal point;

    @Column(name = "special_mission_id", length = 32)
    private String specialMissionId;

    public VjMissionDetail() {
    }

    public VjMissionDetail(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getDetailId() {
        return detailId;
    }

    public void setDetailId(String detailId) {
        this.detailId = detailId;
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public VjMission getVjMission() {
        return vjMission;
    }

    public void setVjMission(VjMission vjMission) {
        this.vjMission = vjMission;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrize() {
        return prize;
    }

    public void setPrize(String prize) {
        this.prize = prize;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public String getSpecialMissionId() {
        return specialMissionId;
    }

    public void setSpecialMissionId(String specialMissionId) {
        this.specialMissionId = specialMissionId;
    }
}
