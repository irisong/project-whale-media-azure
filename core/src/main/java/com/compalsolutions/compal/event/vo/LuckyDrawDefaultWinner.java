package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "ev_lucky_draw_default_winner")
@Access(AccessType.FIELD)
public class LuckyDrawDefaultWinner extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "winner_member_id", nullable = false, length = 32)
    private String winnerMemberId;

    @ManyToOne
    @JoinColumn(name = "winner_member_id", insertable = false, updatable = false)
    private Member winnerMember;

    @Column(name = "package_no", nullable = false, length = 32)
    private String packageNo;

    @Column(name = "package_id", nullable = false, length = 32)
    private String packageId;

    @ManyToOne
    @JoinColumn(name = "package_id", insertable = false, updatable = false)
    private LuckyDrawPackage luckyDrawPackage;

    @Column(name = "win_seq")
    private Integer winSeq;

    public LuckyDrawDefaultWinner() {
    }

    public LuckyDrawDefaultWinner(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWinnerMemberId() {
        return winnerMemberId;
    }

    public void setWinnerMemberId(String winnerMemberId) {
        this.winnerMemberId = winnerMemberId;
    }

    public Member getWinnerMember() {
        return winnerMember;
    }

    public void setWinnerMember(Member winnerMember) {
        this.winnerMember = winnerMember;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public LuckyDrawPackage getLuckyDrawPackage() {
        return luckyDrawPackage;
    }

    public void setLuckyDrawPackage(LuckyDrawPackage luckyDrawPackage) {
        this.luckyDrawPackage = luckyDrawPackage;
    }

    public Integer getWinSeq() {
        return winSeq;
    }

    public void setWinSeq(Integer winSeq) {
        this.winSeq = winSeq;
    }
}
