package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMissionDetail;

import java.math.BigDecimal;
import java.util.List;

public interface VjMissionDetailDao extends BasicDao<VjMissionDetail, String> {
    public static final String BEAN_NAME = "vjMissionDetailDao";

    public List<VjMissionDetail> getCompleteMission(String missionId, BigDecimal point);

    public void findVjMissionDetailForListing(DatagridModel<VjMissionDetail> datagridModel, String missionId);
}
