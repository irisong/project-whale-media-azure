package com.compalsolutions.compal.event.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.*;
import com.compalsolutions.compal.member.vo.Member;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public interface VjMissionService {
    public static final String BEAN_NAME = "vjMissionService";

    public void doAddVjMission(Locale locale, String memberId, List<VjMissionDetail> missionDetailList, String missionPeriodId,
                               List<Map<String, String>> specialMissionList);

    public VjMission doGetLatestVjMissionByMemberId(String memberId);

    public List<Map<String, Object>> getMissionPeriodMinPoint();

    public List<VjMissionSpecial> getSpecialMissionList();

    public void findMissionRankingListingByMissionId(DatagridModel<VjMissionRanking> datagridModel, String missionId);

    public List<VjMissionRanking> findMissionRankingByMissionId(String missionId);

    public List<VjMissionRankingHistory> findMissionRankingHistoryByMissionId(String missionId);

    public VjMissionRanking findPersonalRanking(String memberId, String missionId) ;

    public Member getSpecialMissionWinner(String missionId);

    public VjMissionRanking doCreateMissionRankingIfNotExists(String memberId, String missionId);

    public void doIncreasePointForRanking(String memberId, String missionId, BigDecimal point);

    public void doProcessMissionComplete();

    public void doProcessEndedMission();

    public void doGetMissionForListing(DatagridModel<VjMission> datagridModel, String memberId, String status);

    public VjMission getMissionByMissionId(String missionId);

    public void doApproveMission(Locale locale, String missionId);

    public void doRejectMission(Locale locale, String missionId, String remark);

    public void doProcessVjMissionNotification(VjMission vjMission);

    public void getVjMissionDetailForlisting(DatagridModel<VjMissionDetail> datagridModel, String missionId);
}
