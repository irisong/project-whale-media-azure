package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.event.vo.LuckyDrawWinner;

public interface LuckyDrawWinnerDao extends BasicDao<LuckyDrawWinner, String> {
    public static final String BEAN_NAME = "luckyDrawWinnerDao";

    public int getLatestWinnerSeq(String packageId);
}
