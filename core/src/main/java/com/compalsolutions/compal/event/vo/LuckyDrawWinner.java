package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "ev_lucky_draw_winner")
@Access(AccessType.FIELD)
public class LuckyDrawWinner extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "winner_member_id", nullable = false, length = 32)
    private String winnerMemberId;

    @ManyToOne
    @JoinColumn(name = "winner_member_id", insertable = false, updatable = false)
    private Member winnerMember;

    @Column(name = "channel_id", nullable = false, length = 32)
    private String channelId;

    @Column(name = "package_no", nullable = false, length = 32)
    private String packageNo;

    @Column(name = "package_id", nullable = false, length = 32)
    private String packageId;

    @ManyToOne
    @JoinColumn(name = "package_id", insertable = false, updatable = false)
    private LuckyDrawPackage luckyDrawPackage;

    @Column(name = "win_seq")
    private Integer winSeq;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "win_datetime", nullable = false)
    private Date luckyDrawDatetime;

    @Column(name = "remark")
    private String remark;

    public LuckyDrawWinner() {
    }

    public LuckyDrawWinner(boolean defaultValue) {
        if (defaultValue) {
            luckyDrawDatetime = new Date();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWinnerMemberId() {
        return winnerMemberId;
    }

    public void setWinnerMemberId(String winnerMemberId) {
        this.winnerMemberId = winnerMemberId;
    }

    public Member getWinnerMember() {
        return winnerMember;
    }

    public void setWinnerMember(Member winnerMember) {
        this.winnerMember = winnerMember;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public LuckyDrawPackage getLuckyDrawPackage() {
        return luckyDrawPackage;
    }

    public void setLuckyDrawPackage(LuckyDrawPackage luckyDrawPackage) {
        this.luckyDrawPackage = luckyDrawPackage;
    }

    public Integer getWinSeq() {
        return winSeq;
    }

    public void setWinSeq(Integer winSeq) {
        this.winSeq = winSeq;
    }

    public Date getLuckyDrawDatetime() {
        return luckyDrawDatetime;
    }

    public void setLuckyDrawDatetime(Date luckyDrawDatetime) {
        this.luckyDrawDatetime = luckyDrawDatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
