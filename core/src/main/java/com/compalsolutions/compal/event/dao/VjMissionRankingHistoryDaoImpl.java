package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.event.vo.VjMissionRankingHistory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(VjMissionRankingHistoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjMissionRankingHistoryDaoImpl extends Jpa2Dao<VjMissionRankingHistory, String> implements VjMissionRankingHistoryDao {
    public VjMissionRankingHistoryDaoImpl() {
        super(new VjMissionRankingHistory(false));
    }

    @Override
    public List<VjMissionRankingHistory> findMissionRankingHistoryByMissionId(String missionId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM VjMissionRankingHistory WHERE missionId = ? ORDER BY point desc, lastPointGain asc";
        params.add(missionId);

        return findQueryAsList(hql, params.toArray());
    }
}
