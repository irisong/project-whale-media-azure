package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionRanking;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.wallet.vo.WalletBalance;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(VjMissionRankingDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjMissionRankingDaoImpl extends Jpa2Dao<VjMissionRanking, String> implements VjMissionRankingDao {
    public VjMissionRankingDaoImpl() {
        super(new VjMissionRanking(false));
    }

    @Override
    public void findMissionRankingListingByMissionId(DatagridModel<VjMissionRanking> datagridModel, String missionId) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM p IN " + VjMissionRanking.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(missionId)) {
            hql += " and p.missionId = ?";
            params.add(missionId);
        }
        hql += " order by p.point desc";

        findForDatagrid(datagridModel, "p", hql, params.toArray());
    }

    @Override
    public List<VjMissionRanking> findMissionRankingByMissionId(String missionId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM VjMissionRanking WHERE missionId = ? ORDER BY point desc , lastPointGain asc";

        params.add(missionId);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public VjMissionRanking findMissionRankingByMemberIdAndMissionId(String memberId, String missionId) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be blank for VjMissionRankingDao.findMissionRankingByMemberIdAndMissionId");
        }

        if (StringUtils.isBlank(missionId)) {
            throw new ValidatorException("ownerType can not be blank for VjMissionRankingDao.findMissionRankingByMemberIdAndMissionId");
        }

        VjMissionRanking example = new VjMissionRanking(false);
        example.setMissionId(missionId);
        example.setMemberId(memberId);

        return findFirst(example);
    }

    @Override
    public void doIncreasePointForRanking(String memberId , String missionId, BigDecimal point) {
        List<Object> params = new ArrayList<>();
        String hql = "update VjMissionRanking a set a.point = a.point + ? , a.status = ? , a.lastPointGain = ? " //
                + " where a.memberId = ? AND a.missionId = ? ";

        params.add(point);
        params.add(VjMissionRanking.PENDING);
        params.add(new Date());
        params.add(memberId);
        params.add(missionId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<VjMissionRanking> findIncompleteMission() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM VjMissionRanking WHERE status = ? ORDER BY point desc";

        params.add(VjMissionRanking.PENDING);

        return findQueryAsList(hql, params.toArray());
    }
}
