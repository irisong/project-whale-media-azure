package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionRanking;
import com.compalsolutions.compal.exception.ValidatorException;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(VjMissionDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjMissionDaoImpl extends Jpa2Dao<VjMission, String> implements VjMissionDao {
    public VjMissionDaoImpl() {
        super(new VjMission(false));
    }

    private VjMissionDao vjMissionDao;

    @Override
    public VjMission getLatestVjMissionByMemberId(String memberId) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be blank for VjMissionDao.getActiveVjMissionByMemberId");
        }

        VjMission vjMission = new VjMission(false);
        vjMission.setMemberId(memberId);

        return findFirst(vjMission, "submitDatetime desc");
    }

    @Override
    public void findMissionForDatagrid(DatagridModel<VjMission> datagridModel, String memberId, String status) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT v FROM VjMission v WHERE 1=1 ";

        if (StringUtils.isNotBlank(memberId)) {
            hql += " AND v.memberId = ? ";
            params.add(memberId);
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " AND v.status = ? ";
            params.add(status);
        }

        hql += "ORDER BY v.submitDatetime DESC";

        findForDatagrid(datagridModel, "v", hql, params.toArray());
    }

    @Override
    public List<VjMission> getNewEndedMission() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM VjMission WHERE endIndicator = 1 AND status = ? ";

        params.add(VjMission.STATUS_ENDED);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void doProcessEndMission() {
        List<Object> params = new ArrayList<>();
        String hql = "update VjMission a set a.status = ? ," //
                + " a.endIndicator = ? "
                + " where a.endDatetime < ? AND a.status = ?";

        params.add(VjMission.STATUS_ENDED);
        params.add(true);
        params.add(new Date());
        params.add(VjMission.STATUS_ACTIVE);

        bulkUpdate(hql, params.toArray());
    }
}
