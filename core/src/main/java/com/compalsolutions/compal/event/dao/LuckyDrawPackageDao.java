package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;

import java.util.Date;
import java.util.List;

public interface LuckyDrawPackageDao extends BasicDao<LuckyDrawPackage, String> {
    public static final String BEAN_NAME = "luckyDrawPackageDao";

    public void findLuckyDrawPackageItemForDatagrid(DatagridModel<LuckyDrawPackage> datagridModel, String packageNo, Date luckyDrawDate);

    public LuckyDrawPackage getLuckyDrawPackageByPackageNo(String packageNo, Date luckyDrawDate);

    public List<LuckyDrawPackage> getLuckyDrawPackageListByPackageNo(String packageNo, Date luckyDrawDate);

    public LuckyDrawPackage getLuckyDrawPackageByMemberId(String memberId, Date luckyDrawDate);

    public LuckyDrawPackage getLuckyDrawPackageByItem(String memberId, String itemName, Date luckyDrawDate, String status);

    public int getNextLuckyDrawItemSeq(String packageNo, Date luckyDrawDate);

    public LuckyDrawPackage getNextLuckyDrawItem(String memberId, Date luckyDrawDate);

    public boolean isLuckyDrawStarted(String packageNo, Date luckyDrawDate);

    public void doDeactivateLuckyDrawPackage(String packageNo, Date luckyDrawDate);
}
