package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;

import java.util.Date;

public interface LuckyDrawPackageSqlDao {
    public static final String BEAN_NAME = "luckyDrawPackageSqlDao";

    public void findLuckyDrawPackageForDatagrid(DatagridModel<LuckyDrawPackage> datagridModel, String memberCode, Date dateFrom, Date dateTo, String status);
}
