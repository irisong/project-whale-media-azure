package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMission;

import java.util.List;

public interface VjMissionDao extends BasicDao<VjMission, String> {
    public static final String BEAN_NAME = "vjMissionDao";

    public VjMission getLatestVjMissionByMemberId(String memberId);

    public void findMissionForDatagrid(DatagridModel<VjMission> datagridModel, String memberId, String status);

    public List<VjMission> getNewEndedMission();

    public void doProcessEndMission();
}
