package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(LuckyDrawPackageSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LuckyDrawPackageSqlDaoImpl extends AbstractJdbcDao implements LuckyDrawPackageSqlDao {

    @Override
    public void findLuckyDrawPackageForDatagrid(DatagridModel<LuckyDrawPackage> datagridModel, String memberCode, Date dateFrom, Date dateTo, String status) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT DISTINCT a.package_no, a.lucky_draw_date, a.sender_member_id, a.package_name, a.package_name_cn, a.status "
                + "FROM ev_lucky_draw_package a JOIN mb_member m ON (m.member_id = a.sender_member_id) WHERE 1=1 ";

        if (StringUtils.isNotBlank(memberCode)) {
            sql += "AND m.member_code like ? ";
            params.add(memberCode + "%");
        }

        if (dateFrom != null) {
            sql += "AND a.lucky_draw_date >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if ((dateTo != null)) {
            sql += "AND a.lucky_draw_date <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (StringUtils.isNotBlank(status)) {
            sql += "AND a.status = ? ";
            params.add(status);
        }

        sql += "ORDER BY a.lucky_draw_date DESC, a.package_no DESC ";

        queryForDatagrid(datagridModel, sql, new RowMapper<LuckyDrawPackage>() {
            @Override
            public LuckyDrawPackage mapRow(ResultSet rs, int rowNum) throws SQLException {
                LuckyDrawPackage luckyDrawPackage = new LuckyDrawPackage(false);
                luckyDrawPackage.setPackageNo(rs.getString("package_no"));
                luckyDrawPackage.setLuckyDrawDate(rs.getDate("lucky_draw_date"));
                luckyDrawPackage.setSenderMemberId(rs.getString("sender_member_id"));
                luckyDrawPackage.setPackageName(rs.getString("package_name"));
                luckyDrawPackage.setPackageNameCn(rs.getString("package_name_cn"));
                luckyDrawPackage.setStatus(rs.getString("status"));
                return luckyDrawPackage;
            }
        }, params.toArray());
    }
}
