package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "ev_vj_mission")
@Access(AccessType.FIELD)
public class VjMission extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_PENDING = Global.Status.PENDING;
    public static final String STATUS_ACTIVE = Global.Status.ACTIVE;
    public static final String STATUS_REJECTED = Global.Status.REJECTED;
    public static final String STATUS_ENDED = "ENDED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "mission_id", unique = true, nullable = false, length = 32)
    private String missionId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "submit_datetime", nullable = false)
    private Date submitDatetime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_datetime")
    private Date startDatetime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_datetime")
    private Date endDatetime;

    @Column(name = "config_id", nullable = false)
    private String configId;

    @ManyToOne
    @JoinColumn(name = "config_id", insertable = false, updatable = false)
    private VjMissionConfig vjMissionConfig;

    @Column(name = "status", nullable = false)
    private String status;

    @Column(name = "remark")
    private String remark;

    @Column(name = "end_ind")
    private Boolean endIndicator;

    @OneToMany
    @OrderBy("point asc")
    @JoinColumn(name = "mission_id", insertable = false, updatable = false)
    private List<VjMissionDetail> vjMissionDetailList = new ArrayList<>();

    @Transient
    private String historyName;

    @Transient
    private boolean hasWinner;

    public VjMission() {
    }

    public VjMission(boolean defaultValue) {
        if (defaultValue) {
            status = STATUS_PENDING;
            submitDatetime = new Date();
            endIndicator = false;
        }
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Date getSubmitDatetime() {
        return submitDatetime;
    }

    public void setSubmitDatetime(Date submitDatetime) {
        this.submitDatetime = submitDatetime;
    }

    public Date getStartDatetime() {
        return startDatetime;
    }

    public void setStartDatetime(Date startDatetime) {
        this.startDatetime = startDatetime;
    }

    public Date getEndDatetime() {
        return endDatetime;
    }

    public void setEndDatetime(Date endDatetime) {
        this.endDatetime = endDatetime;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public VjMissionConfig getVjMissionConfig() {
        return vjMissionConfig;
    }

    public void setVjMissionConfig(VjMissionConfig vjMissionConfig) {
        this.vjMissionConfig = vjMissionConfig;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Boolean getEndIndicator() {
        return endIndicator;
    }

    public void setEndIndicator(Boolean endIndicator) {
        this.endIndicator = endIndicator;
    }

    public List<VjMissionDetail> getVjMissionDetailList() {
        return vjMissionDetailList;
    }

    public void setVjMissionDetailList(List<VjMissionDetail> vjMissionDetailList) {
        this.vjMissionDetailList = vjMissionDetailList;
    }

    public String getHistoryName() {
        return historyName;
    }

    public void setHistoryName(String historyName) {
        this.historyName = historyName;
    }

    public boolean isHasWinner() {
        return hasWinner;
    }

    public void setHasWinner(boolean hasWinner) {
        this.hasWinner = hasWinner;
    }
}
