package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.event.vo.VjMissionSpecial;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(VjMissionSpecialDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VjMissionSpecialDaoImpl extends Jpa2Dao<VjMissionSpecial, String> implements VjMissionSpecialDao {
    public VjMissionSpecialDaoImpl() {
        super(new VjMissionSpecial(false));
    }

    @Override
    public List<VjMissionSpecial> getSpecialMissionList() {
        VjMissionSpecial vjMissionSpecial = new VjMissionSpecial(false);
        vjMissionSpecial.setStatus(Global.Status.ACTIVE);

        return findByExample(vjMissionSpecial);
    }
}
