package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMissionRanking;

import java.math.BigDecimal;
import java.util.List;

public interface VjMissionRankingDao extends BasicDao<VjMissionRanking, String> {
    public static final String BEAN_NAME = "vjMissionRankingDao";

    public void findMissionRankingListingByMissionId(DatagridModel<VjMissionRanking> datagridModel, String missionId);

    public List<VjMissionRanking> findMissionRankingByMissionId(String missionId);

    public VjMissionRanking findMissionRankingByMemberIdAndMissionId(String memberId, String missionId);

    public void doIncreasePointForRanking(String memberId , String missionId, BigDecimal point);

    public List<VjMissionRanking> findIncompleteMission();

}
