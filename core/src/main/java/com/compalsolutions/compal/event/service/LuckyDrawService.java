package com.compalsolutions.compal.event.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.LuckyDrawDefaultWinner;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface LuckyDrawService {
    public static final String BEAN_NAME = "luckyDrawService";

    public void findLuckyDrawPackageForListing(DatagridModel<LuckyDrawPackage> datagridModel, String memberCode, Date dateFrom, Date dateTo, String status);

    public void findLuckyDrawPackageItemForListing(DatagridModel<LuckyDrawPackage> datagridModel, String packageNo, Date luckyDrawDate);

    public void findLuckyDrawDefaultWinnerByItemForListing(DatagridModel<LuckyDrawDefaultWinner> datagridModel, String packageId);

    public LuckyDrawPackage getLuckyDrawPackageById(String id);

    public LuckyDrawPackage getLuckyDrawPackageByMemberId(String memberId,  Date luckyDrawDate);

    public LuckyDrawPackage getLuckyDrawPackageByPackageNo(String packageNo, Date luckyDrawDate);

    public List<LuckyDrawPackage> getLuckyDrawPackageListByPackageNo(String packageNo, Date luckyDrawDate);

    public LuckyDrawPackage getLuckyDrawPackageByItem(String memberId, String itemName, Date luckyDrawDate, String status);

    public int getNextLuckyDrawItemSeq(String packageNo, Date luckyDrawDate);

    public LuckyDrawPackage getNextLuckyDrawItem(String memberId, Date luckyDrawDate);

    public int getLatestWinnerSeq(String packageId);

    public LuckyDrawDefaultWinner getDefaultWinner(String id);

    public LuckyDrawDefaultWinner getDefaultWinnerByPackageIdBySeq(String packageId, int winSeq);

    public boolean isDefaultWinnerSeqExceedItemQty(String packageId, int itemQty);

    public List<String> getDefaultWinnerIdByPackageNo(String packageNo);

    public boolean isLuckyDrawStarted(String packageNo, Date luckyDrawDate);

    public boolean isDefaultWinnerExist(String packageNo, String senderMemberId);

    public LuckyDrawPackage doSaveNewLuckyDrawPackage(Locale locale, LuckyDrawPackage luckyDrawPackage, String memberCode, String saveType);

    public void doDeactivateLuckyDrawPackage(String packageNo, Date luckyDrawDate);

    public LuckyDrawPackage doUpdateLuckyDrawPackage(Locale locale, LuckyDrawPackage luckyDrawPackage);

    public void doDeleteLuckyDrawPackageByItem(Locale locale, String packageId);

    public void doSaveLuckyDrawDefaultWinner(Locale locale, LuckyDrawDefaultWinner luckyDrawDefaultWinner, String memberCode);

    public void doDeleteLuckyDrawDefaultWinner(Locale locale, String id);

    public LuckyDrawPackage doProcessLuckyDraw(Locale locale, LuckyDrawPackage luckyDrawInfo);

    public void doDeleteAllDefaultWinnerByPackageId(String packageId);

    public List<String> getLuckyDrawDefaultWinnerbySenderId(String senderMemberId);
}
