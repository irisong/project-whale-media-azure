package com.compalsolutions.compal.event.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "ev_vj_mission_config")
@Access(AccessType.FIELD)
public class VjMissionConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "config_id", unique = true, nullable = false, length = 32)
    private String configId;

    @Column(name = "mission_period", nullable = false)
    private String missionPeriod;

    //max point for all mission
    @Column(name = "max_point")
    private BigDecimal maxPoint;

    @Column(name = "special_mission_min_point")
    private BigDecimal specialMissionMinPoint;

    @Column(name = "sequence", nullable = false)
    private Integer sequence;

    @Column(name = "status", nullable = false)
    private String status;

    public VjMissionConfig() {
    }

    public VjMissionConfig(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public String getMissionPeriod() {
        return missionPeriod;
    }

    public void setMissionPeriod(String missionPeriod) {
        this.missionPeriod = missionPeriod;
    }

    public BigDecimal getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(BigDecimal maxPoint) {
        this.maxPoint = maxPoint;
    }

    public BigDecimal getSpecialMissionMinPoint() {
        return specialMissionMinPoint;
    }

    public void setSpecialMissionMinPoint(BigDecimal specialMissionMinPoint) {
        this.specialMissionMinPoint = specialMissionMinPoint;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
