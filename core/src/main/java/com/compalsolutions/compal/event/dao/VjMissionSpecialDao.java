package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.event.vo.VjMissionSpecial;

import java.util.List;

public interface VjMissionSpecialDao extends BasicDao<VjMissionSpecial, String> {
    public static final String BEAN_NAME = "vjMissionSpecialDao";

    public List<VjMissionSpecial> getSpecialMissionList();
}
