package com.compalsolutions.compal.event.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.event.vo.LuckyDrawWinner;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(LuckyDrawWinnerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LuckyDrawWinnerDaoImpl extends Jpa2Dao<LuckyDrawWinner, String> implements LuckyDrawWinnerDao {
    public LuckyDrawWinnerDaoImpl() {
        super(new LuckyDrawWinner(false));
    }

    @Override
    public int getLatestWinnerSeq(String packageId) {
        LuckyDrawWinner luckyDrawWinner = new LuckyDrawWinner(false);
        luckyDrawWinner.setPackageId(packageId);

        luckyDrawWinner = findFirst(luckyDrawWinner,"winSeq DESC");
        if (luckyDrawWinner != null) {
            return luckyDrawWinner.getWinSeq();
        } else {
            return 0;
        }
    }
}
