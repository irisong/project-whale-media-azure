package com.compalsolutions.compal;

public class HelpDeskFileUploadConfiguration {
    public static final String BEAN_NAME = "helpDeskFileUploadConfiguration";

    private String serverUrl;
    private String uploadPath;

    public static final String FOLDER_NAME = "helpDesk";

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getUploadPath() {
        return uploadPath;
    }

    public void setUploadPath(String uploadPath) {
        this.uploadPath = uploadPath;
    }

    public String getHelpDeskFileUploadPath() {
        return uploadPath + "/" + FOLDER_NAME;
    }

    public String getFullHelpDeskParentFileUrl() {
        return serverUrl + "/file/" + FOLDER_NAME;
    }
}
