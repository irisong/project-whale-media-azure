package com.compalsolutions.compal.provider;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;

public interface EmailProvider {
    public static final String BEAN_NAME = "mailProvider";

    public void doSendEmail(RunTaskLogger logger);
}
