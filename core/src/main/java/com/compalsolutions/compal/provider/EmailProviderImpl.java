package com.compalsolutions.compal.provider;

import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compalsolutions.compal.SysFormatter;
import com.compalsolutions.compal.function.email.EmailServerSetting;
import com.compalsolutions.compal.function.email.service.EmailService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.util.CollectionUtil;

@Service(EmailProvider.BEAN_NAME)
public class EmailProviderImpl implements EmailProvider {
    private static final Log log = LogFactory.getLog(EmailProviderImpl.class);

    @Autowired
    private EmailService emailService;

    @Autowired
    private EmailServerSetting emailServerSetting;

    public void doSendEmail(RunTaskLogger logger) {
        if (logger == null)
            logger = new RunTaskLogger();

//        MailSender mailSender = Application.lookupBean(MailSender.BEAN_NAME, MailSender.class);

        List<Emailq> emailqs = emailService.findNotProcessEmail(10, emailServerSetting.getMaxSendRetry());

        if (CollectionUtil.isEmpty(emailqs)) {
            logger.log("no email to process");
            return;
        }

        logger.log("Has email to process\n\n");

        do {
            Emailq emailq = emailqs.remove(0);

            try {
                String msg = String.format("%s > TO: %s > SUBJECT: %s > RETRY: %d", SysFormatter.formatDateTime(new Date()), emailq.getEmailTo(),
                        emailq.getTitle(), emailq.getRetry());
                logger.log(msg);

                /*
                MimeMessage message = mailSender.createMimeMessage();

                MimeMessageHelper helper = new MimeMessageHelper(message, emailq.isMultipart());
                helper.setTo(emailq.getEmailTo());

                if (StringUtils.isNotBlank(emailq.getEmailCc()))
                    helper.setBcc(emailq.getEmailBcc());

                if (StringUtils.isNotBlank(emailq.getEmailBcc()))
                    helper.setBcc(emailq.getEmailBcc());

                helper.setSubject(emailq.getTitle());
                helper.setText(emailq.getBody(), !Emailq.TYPE_TEXT.equals(emailq.getEmailType()));
                mailSender.send(message);
                 */

                final String fromEmail = "admin@omc-group.io"; //requires valid gmail id
                final String password = "Omc12345"; // correct password for gmail id
                final String toEmail = emailq.getEmailTo(); // can be any email id

                System.out.println("TLSEmail Start");
                Properties props = new Properties();
                props.put("mail.smtp.host", "smtp.gmail.com"); //SMTP Host
                props.put("mail.smtp.port", "587"); //TLS Port
                props.put("mail.smtp.auth", "true"); //enable authentication
                props.put("mail.smtp.starttls.enable", "true"); //enable STARTTLS

                //create Authenticator object to pass in Session.getInstance argument
                Authenticator auth = new Authenticator() {
                    //override the getPasswordAuthentication method
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(fromEmail, password);
                    }
                };
                Session session = Session.getInstance(props, auth);
                session.setDebug(true);

                // Create a default MimeMessage object.
                MimeMessage message = new MimeMessage(session);

                // Set From: header field of the header.
                message.setFrom(new InternetAddress(fromEmail));

                // Set To: header field of the header.
                message.addRecipient(Message.RecipientType.TO, new InternetAddress(toEmail));

                // Set Subject: header field
                message.setSubject(emailq.getTitle());

                // Now set the actual message
                message.setText(emailq.getBody());

                message.setContent(emailq.getBody(), "text/html; charset=utf-8");
                message.setSentDate(new Date());

                // Send message
                Transport.send(message);
                System.out.println("Sent message successfully....");

                emailq.setStatus(Emailq.EMAIL_STATUS_SENT);
                emailService.updateEmailq(emailq);
            } catch (Exception ex) {
                logger.log("SEND FAILED: " + ex.getMessage());
                logger.debug("SEND FAILED: " + ex.fillInStackTrace());
                log.error(ex.getMessage(), ex.fillInStackTrace());
                emailq.setRetry(emailq.getRetry() + 1);
                emailService.updateEmailq(emailq);
            }
        } while (CollectionUtil.isNotEmpty(emailqs));

        logger.log("\n\nFinish process");
    }
}
