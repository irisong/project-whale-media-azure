package com.compalsolutions.compal.init;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

public class WalletSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(WalletSetup.class);

    /*
    @Override
    public boolean execute() {
        long start = System.currentTimeMillis();
    
        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    
        WalletConfig walletConfig = walletService.getDefaultWalletConfig();
        if (walletConfig == null) {
            log.debug("walletConfig does not exists! Need to generate");
            walletService.createDefaultWalletConfig();
        } else {
            log.debug("walletConfig exists!");
        }
    
        WalletTransferConfig walletTransferConfig = walletService.getDefaultWalletTransferConfig();
        if (walletTransferConfig == null) {
            log.debug("walletTransferConfig does not exists! Need to generate");
            walletService.createDefaultWalletTransferConfig();
        } else {
            log.debug("walletTransferConfig exists!");
        }
    
        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end WalletSetup");
        return false;
    }
    */

    @Override
    public boolean execute() {
        long start = System.currentTimeMillis();

        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);

        List<WalletTypeConfig> walletTypeConfigs = Arrays.asList( //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_60, Global.WalletType.USDT, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE), //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_61, Global.WalletType.LTC, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE), //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_62, Global.WalletType.XRP, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE), //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_63, Global.WalletType.EOS, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE), //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_64, Global.WalletType.DOGE, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE), //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_65, Global.WalletType.BCH, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE), //
                new WalletTypeConfig(Global.UserType.MEMBER, Global.WalletType.WALLET_70, Global.WalletType.GGS, WalletTypeConfig.TYPE_COIN, null, null, null,
                        null, null, null, Global.Status.ACTIVE) //
        ); //

        for (WalletTypeConfig walletTypeConfig : walletTypeConfigs) {
            walletService.saveWalletTypeConfig(walletTypeConfig);
        }

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end WalletSetup");
        return false;
    }

    public static void main(String[] args) {
        new WalletSetup().execute();
    }
}
