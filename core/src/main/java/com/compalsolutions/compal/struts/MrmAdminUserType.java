package com.compalsolutions.compal.struts;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.web.HttpAdminUserType;

public class MrmAdminUserType extends HttpAdminUserType {
    private String frontEndMemberId;
    private Member frontEndMember;

    public MrmAdminUserType() {
        templateEvent = new DefaultTemplateEvent();
    }

    @Override
    public String inString() {
        return Global.UserType.HQ;
    }

    public String getFrontEndMemberId() {
        return frontEndMemberId;
    }

    public void setFrontEndMemberId(String frontEndMemberId) {
        this.frontEndMemberId = frontEndMemberId;
    }

    public Member getFrontEndMember() {
        return frontEndMember;
    }

    public void setFrontEndMember(Member frontEndMember) {
        this.frontEndMember = frontEndMember;
    }
}
