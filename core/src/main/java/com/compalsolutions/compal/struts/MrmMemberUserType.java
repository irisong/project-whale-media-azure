package com.compalsolutions.compal.struts;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.web.HttpMemberUserType;

public class MrmMemberUserType extends HttpMemberUserType {
    public MrmMemberUserType() {
        templateEvent = new DefaultTemplateEvent();
    }

    @Override
    public String inString() {
        return Global.UserType.MEMBER;
    }
}
