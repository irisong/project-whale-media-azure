package com.compalsolutions.compal.struts;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.web.HttpAgentUserType;

public class MrmAgentUserType extends HttpAgentUserType {
    public MrmAgentUserType() {
        templateEvent = new DefaultTemplateEvent();
    }

    @Override
    public String inString() {
        return Global.UserType.AGENT;
    }
}
