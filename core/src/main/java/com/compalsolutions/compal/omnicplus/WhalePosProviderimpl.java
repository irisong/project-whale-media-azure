package com.compalsolutions.compal.omnicplus;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.income.IncomeProviderImpl;
import com.compalsolutions.compal.wallet.service.WhalePosService;
import com.compalsolutions.compal.wallet.service.WhalePosServiceImpl;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service(WhalePosProvider.BEAN_NAME)
public class WhalePosProviderimpl implements WhalePosProvider {
    private static final Log log = LogFactory.getLog(IncomeProviderImpl.class);

    public WhalePosProviderimpl() {
    }

    private void logMessage(RunTaskLogger logger, String msg) {
        WebUtil.logMessage(logger, log, msg);
    }

    @Override
    public void doGetWhalePosCoinTrx(RunTaskLogger logger) {
        WhalePosService whalePosService = Application.lookupBean(WhalePosService.BEAN_NAME, WhalePosService.class);
        OmcWalletClient client = new OmcWalletClient();

        Date today = new Date();
        WebUtil.logMessage(logger, log, "------START WHALE POS COIN TRX-----");
        List<WhalePosCoinTrxDto> whalePosCoinTrxDtoList = client.getWhalePosCoinTrxFromOmcWallet(today);

        WebUtil.logMessage(logger, log, "Whale Pos Coin Total Records: " + whalePosCoinTrxDtoList.size());
        WebUtil.logMessage(logger, log, "==========================");

        int cnt = whalePosCoinTrxDtoList.size();
        WebUtil.logMessage(logger, log, "Total Records: " + cnt);

        for (WhalePosCoinTrxDto whalePosCoinTrxDto : whalePosCoinTrxDtoList) {
            WebUtil.logMessage(logger, log, "Processing Records: " + cnt);
            whalePosService.doCreateWhalePosCoinTrxRecords(whalePosCoinTrxDto);
            cnt--;
        }

        WebUtil.logMessage(logger, log, "------END WHALE POS COIN TRX-----");

        WebUtil.logMessage(logger, log, "------START PROCESS WHALE POS COIN TRX-----");

        List<WhalePosCoinTrx> whalePosCoinTrxList = whalePosService.findWhalePosCoinTrxForProcess(today, WhalePosCoinTrx.STATUS_PENDING);

        cnt = whalePosCoinTrxList.size();
        WebUtil.logMessage(logger, log, "Total Records: " + cnt);

        for (WhalePosCoinTrx trx : whalePosCoinTrxList) {
            WebUtil.logMessage(logger, log, "Processing Records: " + cnt);
            whalePosService.doConvertWhalePosCoin2WhaleCoin(trx);
            cnt--;
        }
        WebUtil.logMessage(logger, log, "------END PROCESS WHALE POS COIN TRX-----");
        whalePosCoinTrxList.clear();
        whalePosCoinTrxList = null;
        whalePosCoinTrxDtoList.clear();
        whalePosCoinTrxDtoList = null;
    }
}
