package com.compalsolutions.compal.omnicplus.dto;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

public class CredentialsDto {
    @ToTrim
    @ToUpperCase
    private String username;

    private String password;

    private String phoneno;

    private String oldPassword;

    public CredentialsDto() {
    }

    public CredentialsDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public CredentialsDto(String password, String phoneno, String oldPassword) {
        this.password = password;
        this.phoneno = phoneno;
        this.oldPassword = oldPassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }
}
