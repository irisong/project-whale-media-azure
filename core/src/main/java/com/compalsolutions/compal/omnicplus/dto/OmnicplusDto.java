package com.compalsolutions.compal.omnicplus.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class OmnicplusDto {

    private String message;
    private String userType;
    private int code;
    private String docNo;
    private List<OmcPaymentDto> paymentType;
    private String status;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<OmcPaymentDto> getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(List<OmcPaymentDto> paymentType) {
        this.paymentType = paymentType;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDocNo() {
        return docNo;
    }

    public void setDocNo(String docNo) {
        this.docNo = docNo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
