package com.compalsolutions.compal.omnicplus;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.blockchain.ObjectMapperUtil;
import com.compalsolutions.compal.exception.ExternalConnectionTimeoutException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.omnicplus.dto.CredentialsDto;
import com.compalsolutions.compal.omnicplus.dto.OmcPaymentDto;
import com.compalsolutions.compal.omnicplus.dto.OmnicplusDto;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;
import com.compalsolutions.compal.web.BaseRestUtil;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.ValidationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

public class OmnicplusClient {
    private static Log log = LogFactory.getLog(OmnicplusClient.class);

    private String serverUrl;

    public OmnicplusClient() {
        OmnicplusConfiguration config = Application.lookupBean(OmnicplusConfiguration.class);
        serverUrl = config.getServerUrl();
    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient();
    }

    public OmnicplusDto checkUserLoginCredentialsInOmnicplus(CredentialsDto credentialsDto) {
        Client client = newClient();
        try {
            WebTarget webTarget = client.target(serverUrl + "/whalemedia/authenticate");
            return getResponse(null, webTarget, credentialsDto);
        } catch (Exception e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            client.close();
        }

    }

    public OmnicplusDto checkUserInOmnicplus(CredentialsDto credentialsDto) {
        Client client = newClient();
        try {
            WebTarget webTarget = client.target(serverUrl + "/whalemedia/phonenoAvailable");
            return getResponse(null, webTarget, credentialsDto);
        } catch (Exception e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            client.close();
        }
    }

    public OmnicplusDto getUserPaymentList(CredentialsDto credentialsDto) {
        Client client = newClient();
        try {
            WebTarget webTarget = client.target(serverUrl + "/whalemedia/requestPayment");
            return getResponse(null, webTarget, credentialsDto);
        } catch (Exception e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            client.close();
        }
    }

    public OmnicplusDto payViaOmcWalletForWhaleCoinTopUp(String lang, OmcPaymentDto omcPaymentDto) {
        Client client = newClient();
        WebTarget webTarget = client.target(serverUrl + "/whalemedia/pay");
        Response response = null;
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);
        try {
            if (StringUtils.isNotBlank(lang)) {
                invocationBuilder.header("Content-Language", lang);
            }
            response = invocationBuilder.post(Entity.entity(omcPaymentDto, MediaType.APPLICATION_JSON_TYPE));

            String jsonString = response.readEntity(String.class);
            ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();
            return mapper.readValue(jsonString, OmnicplusDto.class);
        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
            client.close();
        }
    }

    public OmnicplusDto callOmnicplusResetPassword(String lang, CredentialsDto credentialsDto) {
        Client client = newClient();
        WebTarget webTarget = client.target(serverUrl + "/whalemedia/forgetPassword");
        return getResponse(lang, webTarget, credentialsDto);
    }

    public OmnicplusDto checkUserPasswordInOmnicplus(CredentialsDto credentialsDto) {
        Client client = newClient();
        WebTarget webTarget = client.target(serverUrl + "/whalemedia/changePasswordRequest");
        return getResponse(null, webTarget, credentialsDto);
    }

    public OmnicplusDto updateOmnicplusPassword(CredentialsDto credentialsDto) {
        Client client = newClient();
        WebTarget webTarget = client.target(serverUrl + "/whalemedia/changePassword");
        return getResponse(null, webTarget, credentialsDto);
    }

    public BigDecimal getOmcCoinRealtimeRate() {
        Client client = newClient();
        WebTarget webTarget = client.target(serverUrl + "/gameSpin/getExchangeRate");

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);

        Response response = null;
        try {
            response = invocationBuilder.get();
            String jsonString = response.readEntity(String.class);

            ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();
            JsonNode tree = mapper.readTree(jsonString);
            JsonNode sellRate = tree.path("sellRate");
            return new BigDecimal(sellRate.get(0).get("rate").toString());
        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
            client.close();
        }
    }

    private OmnicplusDto getResponse(String lang, WebTarget webTarget, CredentialsDto credentialsDto) {
        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);
        if (StringUtils.isNotBlank(lang)) {
            invocationBuilder.header("Content-Language", lang);
        }
        Response response = null;
        try {
            response = invocationBuilder.post(Entity.entity(credentialsDto, MediaType.APPLICATION_JSON_TYPE));

        } catch (Exception ex) {
            throw new ExternalConnectionTimeoutException("Unable to connect omnicplus");
        }

        String jsonString = response.readEntity(String.class);
        ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();

        try {
            return mapper.readValue(jsonString, OmnicplusDto.class);
        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            response.close();
        }
    }
}
