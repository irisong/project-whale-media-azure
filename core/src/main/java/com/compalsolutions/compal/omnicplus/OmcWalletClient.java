package com.compalsolutions.compal.omnicplus;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.blockchain.ObjectMapperUtil;
import com.compalsolutions.compal.exception.ExternalConnectionTimeoutException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.wallet.vo.WhalePosCoinTrx;
import com.compalsolutions.compal.web.BaseRestUtil;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class OmcWalletClient {
    private static Log log = LogFactory.getLog(OmcWalletClient.class);

    private String serverUrl;

    public OmcWalletClient() {
        OmcWalletConfiguration config = Application.lookupBean(OmcWalletConfiguration.class);
        serverUrl = config.getServerUrl();
    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient();
    }

    public List<WhalePosCoinTrxDto> getWhalePosCoinTrxFromOmcWallet(Date date) {
        Client client = newClient();

        WebTarget webTarget = client.target(serverUrl + "/whalePos/whalePosCoinTrxSummary")
                .queryParam("startDate", date)
                .queryParam("status", Global.Status.PENDING);

        Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON_TYPE);

        Response response = null;

        try {
            response = invocationBuilder.get();
            String jsonString = response.readEntity(String.class);
            // log.debug(jsonString);

            ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();

            JsonNode tree = mapper.readTree(jsonString);
            JsonParser jsonParser = mapper.treeAsTokens(tree.get("list"));
            return mapper.readValue(jsonParser, new TypeReference<List<WhalePosCoinTrxDto>>() {
            });
        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            if (response != null) {
                response.close();
            }
            client.close();
        }
    }
}
