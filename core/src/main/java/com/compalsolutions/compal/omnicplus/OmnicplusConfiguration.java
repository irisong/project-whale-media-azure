package com.compalsolutions.compal.omnicplus;

public class OmnicplusConfiguration {
    private String serverUrl;

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }
}