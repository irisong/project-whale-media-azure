package com.compalsolutions.compal.omnicplus;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;

import java.text.ParseException;

public interface WhalePosProvider {
    public static final String BEAN_NAME = "whalePosProvider";

    public void doGetWhalePosCoinTrx (RunTaskLogger logger) throws ParseException;
}
