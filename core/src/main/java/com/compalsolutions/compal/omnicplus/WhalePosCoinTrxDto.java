package com.compalsolutions.compal.omnicplus;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WhalePosCoinTrxDto {
    @JsonProperty("trxId")
    private String trxId;

    @JsonProperty("ownerId")
    private String ownerId;

    @JsonProperty("memberCode")
    private String memberCode;

    @JsonProperty("inAmt30P")
    private BigDecimal inAmt30P;

    @JsonProperty("rate")
    private BigDecimal rate;

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public BigDecimal getInAmt30P() {
        return inAmt30P;
    }

    public void setInAmt30P(BigDecimal inAmt30P) {
        this.inAmt30P = inAmt30P;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }
}
