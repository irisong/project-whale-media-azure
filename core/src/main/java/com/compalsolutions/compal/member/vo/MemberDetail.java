package com.compalsolutions.compal.member.vo;

import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "mb_member_detail")
@Access(AccessType.FIELD)
public class MemberDetail extends VoBase {
    private static final long serialVersionUID = 1L;

    public MemberDetail() {
    }

    public MemberDetail(boolean defaultValue) {
        if (defaultValue) {
            emailStatus = Global.Status.ACTIVE;
        }
    }

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_det_id", unique = true, nullable = false, length = 32)
    private String memberDetId;

    @ToTrim
    @Column(name = "profile_name", nullable = false, length = 20)
    private String profileName;

    @ToTrim
    @Column(name = "whalelive_id", unique = true, nullable = false, length = 8)
    private String whaleliveId;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", length = 10)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", length = 5)
    private String currencyCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "address", columnDefinition = "text")
    private String address;

    @ToUpperCase
    @ToTrim
    @Column(name = "address2", columnDefinition = "text")
    private String address2;

    @ToUpperCase
    @ToTrim
    @Column(name = "city", length = 200)
    private String city;

    @ToUpperCase
    @ToTrim
    @Column(name = "state", length = 200)
    private String state;

    @ToUpperCase
    @ToTrim
    @Column(name = "postcode", length = 50)
    private String postcode;

    @ToTrim
    @Column(name = "email", length = 100)
    private String email;

    @ToTrim
    @Column(name = "contact_no", length = 100)
    private String contactNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "gender", length = 10)
    private String gender;

    @Temporal(TemporalType.DATE)
    @Column(name = "dob")
    private Date dob;

    // default 'ACTIVE'
    @ToUpperCase
    @ToTrim
    @Column(name = "email_status", length = 10)
    private String emailStatus;

    @Column(name = "profile_bio", length = 500)
    private String profileBio;

    @Transient
    protected String memberId;

    @Transient
    protected String liveChannelId;


    public String getMemberDetId() {
        return memberDetId;
    }

    public void setMemberDetId(String memberDetId) {
        this.memberDetId = memberDetId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Date getDob() {
        return dob;
    }

    public void setDob(Date dob) {
        this.dob = dob;
    }

    public String getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(String emailStatus) {
        this.emailStatus = emailStatus;
    }

    public String getProfileBio() {
        return profileBio;
    }

    public void setProfileBio(String profileBio) {
        this.profileBio = profileBio;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getLiveChannelId() {
        return liveChannelId;
    }

    public void setLiveChannelId(String liveChannelId) {
        this.liveChannelId = liveChannelId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((memberDetId == null) ? 0 : memberDetId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MemberDetail other = (MemberDetail) obj;
        if (memberDetId == null) {
            if (other.memberDetId != null)
                return false;
        } else if (!memberDetId.equals(other.memberDetId))
            return false;
        return true;
    }
}
