package com.compalsolutions.compal.member.dao;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.PlanbTree;

public interface PlanbTreeSqlDao {
    public static final String BEAN_NAME = "planbTreeSqlDao";

    public List<Integer> getNoOfDownlinesGroupByLevel(String traceKey);

    public Triple<String, Integer, Integer> findUplineAndNoOfDirectDownlinesForFreeSlot(String traceKey, int topLevel, int nextDownlineLevel);

}
