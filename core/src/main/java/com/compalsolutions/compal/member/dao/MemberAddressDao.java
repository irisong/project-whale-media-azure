package com.compalsolutions.compal.member.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MemberAddress;

public interface MemberAddressDao extends BasicDao<MemberAddress, String> {
    public static final String BEAN_NAME = "MemberAddressDao";

    List<MemberAddress> findAllAddressByMemberId(String memberId);

    public MemberAddress findMemberAddress(String memberId, String addressId, boolean defaultAddress);
}
