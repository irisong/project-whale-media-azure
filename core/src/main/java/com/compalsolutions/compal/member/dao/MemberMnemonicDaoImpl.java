package com.compalsolutions.compal.member.dao;

import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.MemberMnemonic;

@Component(MemberMnemonicDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberMnemonicDaoImpl extends Jpa2Dao<MemberMnemonic, String> implements MemberMnemonicDao {
    public MemberMnemonicDaoImpl() {
        super(new MemberMnemonic(false));
    }

    @Override
    public MemberMnemonic findActiveMemberMnemonicByMemberId(String memberId) {
        Validate.notBlank(memberId);

        MemberMnemonic example = new MemberMnemonic(true);
        example.setMemberId(memberId);
        example.setStatus(Global.Status.ACTIVE);

        return findUnique(example);
    }
}
