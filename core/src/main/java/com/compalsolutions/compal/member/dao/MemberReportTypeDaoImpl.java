package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberReportType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(MemberReportTypeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberReportTypeDaoImpl extends Jpa2Dao<MemberReportType, String> implements MemberReportTypeDao {

    public MemberReportTypeDaoImpl() {
        super(new MemberReportType(false));
    }

    @Override
    public List<MemberReportType> findMemberReportTypes(String status) {
        MemberReportType example = new MemberReportType(false);
        if (StringUtils.isNotBlank(status))
            example.setStatus(status);

        return findByExample(example, "sortOrder");
    }

    @Override
    public void findMemberReportTypeForDatagrid(DatagridModel<MemberReportType> datagridModel, String status) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM a IN " + MemberReportType.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(status)) {
            hql += " AND a.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel,"a", hql,params.toArray());
    }
}
