package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import com.compalsolutions.compal.member.vo.MemberFans;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface FansIntimacyService {
    public static final String BEAN_NAME = "fansIntimacyService";

    public boolean isFirstTimeGainByTrx(String userMemberId, String channelMemberId, Date enterDate, String trxType);

    public boolean isFirstTimeGain(String userMemberId, String channelMemberId, Date enterDate, String trxType);

    public BigDecimal findTodayPointSpend(String fansMemberId, String memberId);

    public boolean findLackInteractiveDate(MemberFans memberFans);

    public void doIncreaseFanIntimacyValue(Locale locale, String userMemberId, String channelMemberId, String trxType, BigDecimal value);

    public boolean isSendGiftGain(String fansMemberId, String memberId);

    public BigDecimal findTodayIntimacyValue(MemberFans memberFans, String trxType);

    public FansIntimacyConfig getFansIntimacyConfigByLevel(int fanLevel);

    public int findTotalValueNeedForLevel(int fanLevel);

    public BigDecimal findLackInteractiveRecord(String fansMemberId, String memberId);

    public void doReduceFansIntimacyForNotActiveFans();

    public FansIntimacyConfig getFansIntimacyConfig(String id);

    public void findFansIntimacyForListing(DatagridModel<FansIntimacyConfig> datagridModel);

    public void doSaveFansIntimacyConfig(Locale locale, FansIntimacyConfig fansIntimacyConfig) ;

    public void updateFansIntimacyConfig(Locale locale, FansIntimacyConfig fansIntimacyConfig);

    public List<FansIntimacyConfig> getAllFansIntimacyConfig();

    public void doProcessFansIntimacyGain(String fansMemberId, String memberId);

    public BigDecimal getTodayIntimacy(Date lastGain, int value);

}
