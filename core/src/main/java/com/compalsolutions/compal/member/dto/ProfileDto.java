package com.compalsolutions.compal.member.dto;

import com.compalsolutions.compal.live.streaming.client.dto.Channel;

import java.io.Serializable;
import java.util.List;

public class ProfileDto implements Serializable {
    private String profilePic;
    private String memberId;
    private String profileName;
    private String whaleliveId;
    private String profileBio;
    private String yunXinAccountId;
    private int followerCount;
    private int followingCount;
    private boolean following;
    private boolean blocking;
    private boolean blockBy;
    private Channel channel;
    private List<String> actionOptions;
    private String gender;
    private String countryName;
    private String levelUrl;
    private String receivedPoint;
    private String sentPoint;

    public ProfileDto() {
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    public String getProfileBio() {
        return profileBio;
    }

    public void setProfileBio(String profileBio) {
        this.profileBio = profileBio;
    }

    public String getYunXinAccountId() {
        return yunXinAccountId;
    }

    public void setYunXinAccountId(String yunXinAccountId) {
        this.yunXinAccountId = yunXinAccountId;
    }

    public int getFollowerCount() {
        return followerCount;
    }

    public void setFollowerCount(int followerCount) {
        this.followerCount = followerCount;
    }

    public int getFollowingCount() {
        return followingCount;
    }

    public void setFollowingCount(int followingCount) {
        this.followingCount = followingCount;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public boolean isBlocking() {
        return blocking;
    }

    public void setBlocking(boolean blocking) {
        this.blocking = blocking;
    }

    public boolean isBlockBy() {
        return blockBy;
    }

    public void setBlockBy(boolean blockBy) {
        this.blockBy = blockBy;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

    public List<String> getActionOptions() {
        return actionOptions;
    }

    public void setActionOptions(List<String> actionOptions) {
        this.actionOptions = actionOptions;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getLevelUrl() {
        return levelUrl;
    }

    public void setLevelUrl(String levelUrl) {
        this.levelUrl = levelUrl;
    }


    public String getReceivedPoint() {
        return receivedPoint;
    }

    public void setReceivedPoint(String receivedPoint) {
        this.receivedPoint = receivedPoint;
    }

    public String getSentPoint() {
        return sentPoint;
    }

    public void setSentPoint(String sentPoint) {
        this.sentPoint = sentPoint;
    }
}
