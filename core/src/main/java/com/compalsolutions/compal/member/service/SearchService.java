package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.member.vo.SearchHistory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public interface SearchService {
    public static final Log log = LogFactory.getLog(SearchService.class);
    public static final String BEAN_NAME = "searchService";

    public void doCreateSearchHistory(String memberId, String keyword);

    public void doRemoveSearchHistory(String memberId, String keyword);

    public List<SearchHistory> findAllSearchHistoryByMemberId(String memberId);
}
