package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.PlacementTree;

public interface PlacementTreeDao extends BasicDao<PlacementTree, String> {
    public static final String BEAN_NAME = "placementDao";

    public PlacementTree findPlacement(String memberId, int unit);

    public PlacementTree findPlacementTreeByParentIdAndParentUnitAndParentPosition(String parentId, int parentUnit, String parentPosition);
}
