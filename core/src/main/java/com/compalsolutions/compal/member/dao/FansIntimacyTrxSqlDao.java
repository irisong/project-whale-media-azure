package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

public interface FansIntimacyTrxSqlDao {
    public static final String BEAN_NAME = "fansIntimacyTrxSqlDao";

    public List<HashMap<String, String>> findReduceIntimacyMember(Date dateFrom, Date dateTo, int minimumIntimacy);
}
