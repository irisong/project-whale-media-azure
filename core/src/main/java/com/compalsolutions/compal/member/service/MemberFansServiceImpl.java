package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.MemberFansDao;
import com.compalsolutions.compal.member.dao.MemberFansSqlDao;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.MemberFans;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(MemberFansService.BEAN_NAME)
public class MemberFansServiceImpl implements MemberFansService {
    private static final Object synchronizedAddFansObject = new Object();
    private static final Object synchronizedAddAdminObject = new Object();

    @Autowired
    private MemberFansDao memberFansDao;

    @Autowired
    private MemberFansSqlDao memberFansSqlDao;

    @Override
    public MemberFans getMemberFans(String fansId) {
        return memberFansDao.get(fansId);
    }

    @Override
    public MemberFans findMemberFans(String fansMemberId, String memberId, String role) {
        return memberFansDao.findMemberFans(fansMemberId, memberId, role);
    }

    @Override
    public void findMemberFansForListing(DatagridModel<MemberFans> datagridModel, String memberId) {
        memberFansSqlDao.findMemberFansForDatagrid(datagridModel, memberId);
    }

    @Override
    public void findMemberFansBadgeForListing(DatagridModel<MemberFans> datagridModel, String fansMemberId, String role) {
        memberFansDao.findMemberFansBadgeForDatagrid(datagridModel, fansMemberId, role);
    }

    @Override
    public boolean isMemberFans(String fansMemberId, String memberId, String role) {
        MemberFans memberFans = memberFansDao.findMemberFans(fansMemberId, memberId, role);
        if (memberFans != null) {
            return true;
        } else {
            return false;
        }
    }

    private void verifyMember(Locale locale, String memberId, String fansMemberId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        Member member = memberService.getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        if (StringUtils.isBlank(fansMemberId)) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        Member fansMember = memberService.getMember(fansMemberId);
        if (fansMember == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }
    }

    @Override
    public void doAddMemberFan(Locale locale, String memberId, String fansMemberId) {
        synchronized (synchronizedAddFansObject) {
            verifyMember(locale, memberId, fansMemberId);

            MemberFans memberFans = memberFansDao.findMemberFans(fansMemberId, memberId, "");
            if (memberFans == null) {
                memberFans = new MemberFans(true);
                memberFans.setMemberId(memberId);
                memberFans.setFansMemberId(fansMemberId);
                memberFansDao.save(memberFans);
            }
        }
    }

    @Override
    public void doAddMemberAdmin(Locale locale, String memberId, String fansMemberId, String role) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedAddAdminObject) {
            /************************
             * VERIFICATION - START
             ************************/
            verifyMember(locale, memberId, fansMemberId);

            List<MemberFans> memberFansRoleList = memberFansDao.getMemberFans(memberId, role);
            switch (role.toUpperCase()) {
                case MemberFans.ROLE_SENIOR_ADMIN:
                    if (memberFansRoleList.size() >= 3) {
                        throw new ValidatorException(i18n.getText("maximumFansSeniorAdmin", locale, 3));
                    }
                    break;
                case MemberFans.ROLE_ADMIN:
                    if (memberFansRoleList.size() >= 5) {
                        throw new ValidatorException(i18n.getText("maximumFansAdmin", locale, 5));
                    }
                    break;
                case "":
                    break;
                default:
                    throw new ValidatorException(i18n.getText("invalidRole", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

//            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(memberId, Global.AccessModule.WHALE_MEDIA);
//            LiveChatRoom liveChatRoom = liveChatService.findChatRoomByCreator(account.getAccountId());
//            if (liveChatRoom != null) {
//                LiveChatRoomClient liveChatRoomClient = new LiveChatRoomClient();
//                if (role.equals("")) {
//                    liveChatResponse = liveChatRoomClient.setMemberFansRole(liveChatRoom.getRoomId(), account.getAccountId(), targetAcct.getAccountId(),
//                            "1", String.valueOf(false), "", Global.AccessModule.WHALE_MEDIA);
//                } else {
//                    liveChatResponse = liveChatRoomClient.setMemberFansRole(liveChatRoom.getRoomId(), account.getAccountId(), targetAcct.getAccountId(),
//                            "1", String.valueOf(true), "", Global.AccessModule.WHALE_MEDIA);
//                }

            MemberFans memberFans = memberFansDao.findMemberFans(fansMemberId, memberId, "");
            if (memberFans != null) {
                if (role.equalsIgnoreCase(MemberFans.ROLE_SENIOR_ADMIN)) {
                    memberFans.setAdmin(false);
                    memberFans.setSeniorAdmin(true);
                    memberFansDao.update(memberFans);

                    doProcessFansRoleNotification(memberFans, role);
                } else if (role.equalsIgnoreCase(MemberFans.ROLE_ADMIN)) {
                    memberFans.setAdmin(true);
                    memberFans.setSeniorAdmin(false);
                    memberFansDao.update(memberFans);

                    doProcessFansRoleNotification(memberFans, role);
                } else if (role.equals("")) {
                    memberFans.setAdmin(false);
                    memberFans.setSeniorAdmin(false);
                    memberFansDao.update(memberFans);
                }
            } else {
                throw new ValidatorException(i18n.getText("memberIsNotFans", locale));
            }
//            }

            memberFansRoleList.clear();
            memberFansRoleList = null;
        }
    }

    @Override
    public int findTotalFans(String memberId){
        return memberFansDao.findTotalFans(memberId);
    }

    @Override
    public void doProcessFansRoleNotification(MemberFans memberFans, String role) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        MemberDetail memberDetail = memberService.findMemberDetailByMemberId(memberFans.getMemberId());
        Member fansMember = memberService.getMember(memberFans.getMemberId());
        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.SET_FANS_ROLE);

        NotificationAccount frmAcct = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        NotificationAccount toAcct = notificationService.findNotifAccByMemberAndAccessModule(memberFans.getFansMemberId(), Global.AccessModule.WHALE_MEDIA);
        if (frmAcct != null && toAcct != null) {
            String content;

            if (role.equalsIgnoreCase(MemberFans.ROLE_SENIOR_ADMIN)) {
                content = i18n.getText("userFansRoleSetAsSeniorAdmin", new Locale(fansMember.getPreferLanguage()), memberDetail.getProfileName());
            } else {
                content = i18n.getText("userFansRoleSetAsAdmin", new Locale(fansMember.getPreferLanguage()), memberDetail.getProfileName());
            }

            notificationConfProvider.doSendMessage(frmAcct.getAccountId(), toAcct.getAccountId(), Global.AccessModule.WHALE_MEDIA, content, payload,null);
        }
    }

    @Override
    public BigDecimal doIncreasePointSpend(MemberFans memberFans, BigDecimal value) {
        BigDecimal newPointSpend;
        if (memberFans.getPointSpend() != null) {
            newPointSpend = memberFans.getPointSpend().add(value);
        } else {
            newPointSpend = value;
        }

        return newPointSpend;
    }

    @Override
    public int doIncreaseTodayFansIntimacy(MemberFans memberFans, int value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date lastGainDate = memberFans.getLastIntimacyGain();

        boolean refreshIntimacy = lastGainDate != null && sdf.format(lastGainDate).equals(sdf.format(new Date()));
        int newIntimacy = value;
        if (memberFans.getTodayIntimacyValue() != null) {
            if (refreshIntimacy) {
                newIntimacy = memberFans.getTodayIntimacyValue() + value;
            }
        }

        return newIntimacy;
    }

    @Override
    public void doProcessWearBadge(Locale locale, String fansMemberId, String memberId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        memberFansDao.doUnwearBadge(fansMemberId);

        if (StringUtils.isNotBlank(memberId)) {
            MemberFans memberFans = memberFansDao.findMemberFans(fansMemberId, memberId,"");
            if (memberFans != null) {
                memberFans.setWearingBadge(true);
                memberFansDao.update(memberFans);
            } else {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
        }
    }

    @Override
    public MemberFans findFansWearingBadge(String fansMemberId) {
        return memberFansDao.findFansWearingBadge(fansMemberId);
    }

    /** If wearing badge level 6 & above, display wearing badge, else, display current channel fans badge **/
    @Override
    public Triple<String, String, String> getDisplayFansBadgeInfo(String memberId, String channelMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);

        MemberFans finalBadgeMemberFans;
        String fansBadgeName = null;
        String fansBadgeUrl = null;
        String fansBadgeTextColor = null;

        MemberFans currentWearingMemberFans = findFansWearingBadge(memberId);
        if (currentWearingMemberFans != null) {
            String wearingMemberId = currentWearingMemberFans.getMemberId();
            if (channelMemberId.equalsIgnoreCase(wearingMemberId)) {
                finalBadgeMemberFans = currentWearingMemberFans;
            } else {
                MemberFans wearingMemberFans = findMemberFans(memberId, wearingMemberId, ""); //other vj member fans
                if (wearingMemberFans.getFansLevel() > 6) { // fans level more than 6 only can show on other live
                    finalBadgeMemberFans = currentWearingMemberFans;
                } else {
                    finalBadgeMemberFans = findMemberFans(memberId, channelMemberId, ""); //channel vj member fans
                }
            }
        } else {
            finalBadgeMemberFans = findMemberFans(memberId, channelMemberId, ""); //channel vj member fans
        }

        if(finalBadgeMemberFans != null) {
            Member member = memberService.getMember(finalBadgeMemberFans.getMemberId());
            String fansRank = MemberFans.getFansRank(finalBadgeMemberFans.getFansLevel());
            fansBadgeUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/" + fansRank + ".png";
            fansBadgeName = member.getFansBadgeName();
            fansBadgeTextColor = MemberFans.getFansBadgeTextCode(finalBadgeMemberFans.getFansLevel());
        }

        return new MutableTriple<>(fansBadgeUrl, fansBadgeName, fansBadgeTextColor);
    }
}
