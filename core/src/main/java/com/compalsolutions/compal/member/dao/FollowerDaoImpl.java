package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.MemberRepository;
import com.compalsolutions.compal.member.vo.Follower;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(FollowerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FollowerDaoImpl extends Jpa2Dao<Follower, String> implements FollowerDao {
    @SuppressWarnings("unused")
    @Autowired
    private MemberRepository memberRepository;

    public FollowerDaoImpl() {
        super(new Follower(false));
    }

    @Override
    public Follower getFollower(String memberId, String followerId) {
        Validate.notBlank(memberId);
        Validate.notBlank(followerId);

        Follower follower = new Follower(false);
        follower.setMemberId(memberId);
        follower.setFollowerId(followerId);

        return findUnique(follower);
    }

    @Override
    public int getFollowerCount(String userMemberId) {
        String hql = "select count(m.memberId) from Follower m where m.memberId=? order by datetimeAdd desc";
        Long result = (Long) exFindUnique(hql, userMemberId);

        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public int getFollowingCount(String userMemberId) {
        String hql = "select count(m.memberId) from Follower m where m.followerId=? order by datetimeAdd desc";
        Long result = (Long) exFindUnique(hql, userMemberId);

        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<Follower> getFollowersList(String userMemberId) {
        Validate.notBlank(userMemberId);

        Follower follower = new Follower(false);
        follower.setMemberId(userMemberId);

        return findByExample(follower, "datetimeAdd desc");
    }

    @Override
    public List<Follower> getFollowingList(String userMemberId) {
        Validate.notBlank(userMemberId);

        Follower following = new Follower(false);
        following.setFollowerId(userMemberId);

        return findByExample(following, "datetimeAdd desc");
    }
}
