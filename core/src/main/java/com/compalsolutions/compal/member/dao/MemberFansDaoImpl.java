package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansIntimacyTrx;
import com.compalsolutions.compal.member.vo.MemberFans;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(MemberFansDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberFansDaoImpl extends Jpa2Dao<MemberFans, String> implements MemberFansDao {

    public MemberFansDaoImpl() {
        super(new MemberFans(false));
    }

    @Override
    public MemberFans findMemberFans(String fansMemberId, String memberId, String role) {
        Validate.notBlank(fansMemberId);
        Validate.notBlank(memberId);

        MemberFans memberFans = new MemberFans(false);
        memberFans.setFansMemberId(fansMemberId);
        memberFans.setMemberId(memberId);

        if (StringUtils.isNotBlank(role)) {
            switch (role.toUpperCase()) {
                case MemberFans.ROLE_SENIOR_ADMIN:
                    memberFans.setSeniorAdmin(true);
                    break;
                case MemberFans.ROLE_ADMIN:
                    memberFans.setAdmin(true);
                    break;
                default:
                    break;
            }
        }

        return findFirst(memberFans);
    }

    @Override
    public List<MemberFans> getMemberFans(String memberId, String role) {
        Validate.notBlank(memberId);

        MemberFans memberFans = new MemberFans(false);
        memberFans.setMemberId(memberId);

        switch (role.toUpperCase()) {
            case MemberFans.ROLE_SENIOR_ADMIN:
                memberFans.setSeniorAdmin(true);
                break;
            case MemberFans.ROLE_ADMIN:
                memberFans.setAdmin(true);
                break;
            default:
                break;
        }

        return findByExample(memberFans);
    }

    @Override
    public void findMemberFansBadgeForDatagrid(DatagridModel<MemberFans> datagridModel, String fansMemberId, String role) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT f FROM MemberFans f WHERE 1=1 ";

        if (StringUtils.isNotBlank(fansMemberId)) {
            hql += " and f.fansMemberId = ? ";
            params.add(fansMemberId);
        }

        switch (role.toUpperCase()) {
            case MemberFans.ROLE_SENIOR_ADMIN:
                hql += " and f.isSeniorAdmin = 1 ";
                break;
            case MemberFans.ROLE_ADMIN:
                hql += " and f.isAdmin = 1 ";
                break;
            default:
                break;
        }

        hql += "ORDER BY f.joinDate DESC";

        findForDatagrid(datagridModel, "f", hql, params.toArray());
    }

    @Override
    public int findTotalFans(String memberId) {
        String hql = "SELECT COUNT(m.fansMemberId) FROM MemberFans m WHERE m.memberId = '"+memberId+"' ";

        Long result = (Long) exFindFirst(hql);
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public void doUnwearBadge(String fansMemberId) {
        List<Object> params = new ArrayList<>();
        String hql = "update MemberFans c set c.wearingBadge = 0 "
                + " where c.fansMemberId = ? ";
        params.add(fansMemberId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public MemberFans findFansWearingBadge(String fansMemberId) {
        Validate.notBlank(fansMemberId);

        MemberFans memberFans = new MemberFans(false);
        memberFans.setFansMemberId(fansMemberId);
        memberFans.setWearingBadge(true);

        return findFirst(memberFans);
    }

    @Override
    public MemberFans isFirstTimeGain(String fansMemberId, String memberId, String type) {
        Validate.notBlank(fansMemberId);
        Validate.notBlank(memberId);

        MemberFans memberFans = new MemberFans(false);
        memberFans.setFansMemberId(fansMemberId);
        memberFans.setMemberId(memberId);

        if (type.equalsIgnoreCase(FansIntimacyTrx.TYPE_FIRST_TIME_ENTRY)) {
            memberFans.setFirstEntryDate(new Date());
        } else if (type.equalsIgnoreCase(FansIntimacyTrx.TYPE_CONT_FIVE_MIN_VIEW)) {
            memberFans.setContWatchingDate(new Date());
        }

        return findFirst(memberFans);
    }

    @Override
    public void doSetSendPointInd(String fansMemberId, String memberId) {
        List<Object> params = new ArrayList<>();
        String hql = "update MemberFans c set c.sendPointInd = 1 "
                + " where c.fansMemberId = ? and c.memberId = ? ";

        params.add(fansMemberId);
        params.add(memberId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public List<MemberFans> findMemberFansForGain(String fansMemberId, String memberId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM s IN " + MemberFans.class + " WHERE 1=1 ";
        hql += " AND s.sendPointInd = 1 ";

        if (StringUtils.isNotBlank(fansMemberId)) {
            hql += " AND s.fansMemberId = ? ";
            params.add(fansMemberId);
        }
        if (StringUtils.isNotBlank(memberId)) {
            hql += " AND s.memberId = ? ";
            params.add(memberId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void deleteDroppedMemberFans(Date joinDate, Date lackIntimacyDate) {
        List<Object> params = new ArrayList<>();
        String hql = "DELETE FROM MemberFans "
                   + "WHERE joinDate <= ? AND intimacyValue = 0 AND lackIntimacyDate <= ? ";

        params.add(joinDate);
        params.add(lackIntimacyDate);

        bulkUpdate(hql, params.toArray());
    }
}
