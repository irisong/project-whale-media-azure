package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import com.compalsolutions.compal.member.vo.FansIntimacyTrx;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(FansIntimacyTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FansIntimacyTrxDaoImpl extends Jpa2Dao<FansIntimacyTrx, String> implements FansIntimacyTrxDao {

    public FansIntimacyTrxDaoImpl() {
        super(new FansIntimacyTrx(false));
    }

    @Override
    public List<FansIntimacyTrx> isFirstTimeGain(String userMemberId, String channelMember, Date entryDate, String trxType) {
        String hql = " FROM s IN " + FansIntimacyTrx.class + " WHERE 1=1 ";
        hql += " AND s.fansMemberId = ?";
        hql += " AND s.memberId = ?";
        hql += " AND DATE(s.trxDatetime) = DATE(?)";
        hql += " AND s.trxType = ?";

        List<Object> params = new ArrayList<Object>();
        params.add(userMemberId);
        params.add(channelMember);
        params.add(entryDate);
        params.add(trxType);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public BigDecimal findTodayIntimacyValue(String fansMemberId, String memberId, String trxType) {
        Validate.notNull(memberId);

        String hql = "select sum(d.value) from FansIntimacyTrx d where d.fansMemberId = ? AND d.memberId = ? AND Date(d.trxDatetime) = ?";
        hql += " AND d.trxType != ? AND d.trxType != ? ";

        List<Object> params = new ArrayList<>();
        params.add(fansMemberId);
        params.add(memberId);
        params.add(new Date());
        params.add(FansIntimacyTrx.TYPE_LACK_OF_INTERACT);
        params.add(FansIntimacyTrx.TYPE_RETURN_VALUE);

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }

    @Override
    public BigDecimal findLackInteractiveRecord(String fansMemberId, String memberId){
        String hql = "select d.value from FansIntimacyTrx d where Date(d.trxDatetime) = ? AND d.trxType = ? AND d.fansMemberId = ? AND d.memberId = ? AND NOT EXISTS (select value from FansIntimacyTrx where Date(trxDatetime) = ? AND trxType = ? ) ";

        List<Object> params = new ArrayList<>();
        params.add(new Date());
        params.add(FansIntimacyTrx.TYPE_LACK_OF_INTERACT);
        params.add(fansMemberId);
        params.add(memberId);
        params.add(new Date());
        params.add(FansIntimacyTrx.TYPE_RETURN_VALUE);

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return new BigDecimal(result.toString());
        else
            return BigDecimal.ZERO;
    }
}
