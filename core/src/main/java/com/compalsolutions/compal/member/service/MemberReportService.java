package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberReport;
import com.compalsolutions.compal.member.vo.MemberReportFile;
import com.compalsolutions.compal.member.vo.MemberReportType;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface MemberReportService {
    public static final String BEAN_NAME = "memberReportService";

    public MemberReportType getMemberReportType(String reportTypeId);

    public MemberReport getMemberReport(String reportId);

    public List<MemberReportType> findMemberReportTypes(String status);

    public void doProcessMemberReport(Locale locale, String userMemberId, String reportMemberId,
                                      String reportTypeId, String description, List<MemberReportFile> uploadImageList);

    public void doProcessWarningMessage(Member warningMember,Member member ,String content , String contentCn);

    public void doAddMemberReportFile(String reportId, List<MemberReportFile> uploadImageList);

    public void findMemberReportTypeForListing(DatagridModel<MemberReportType> datagridModel, String status);

    public void saveMemberReportType(MemberReportType memberReportType);

    public void updateMemberReportType(Locale locale, MemberReportType memberReportType);

    public void findMemberReportForListing(DatagridModel<MemberReport> datagridModel, Date dateFrom, Date dateTo, String reportTypeId, String reportMemberId, Boolean isRead);

    public void updateMemberReportRead(String reportId, LoginInfo loginInfo);

    public String getMemberReportTypeByLanguage(String reportTypeId, String language);

    public void doUpdateWarningCount(MemberReport memberReport);
}
