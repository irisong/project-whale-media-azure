package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.*;
import com.compalsolutions.compal.member.redis.FansIntimacyRedisServices;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.redisson.client.RedisConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Component(FansIntimacyService.BEAN_NAME)
public class FansIntimacyServiceImpl implements FansIntimacyService {
    private static final Object synchronizedObject = new Object();
    private static final Log log = LogFactory.getLog(FansIntimacyService.class);

    @Autowired
    private FansIntimacyTrxDao fansIntimacyTrxDao;

    @Autowired
    private FansIntimacyTrxSqlDao fansIntimacyTrxSqlDao;

    @Autowired
    private FansIntimacyConfigDao fansIntimacyConfigDao;

    @Autowired
    private MemberFansDao memberFansDao;

    @Autowired
    private FansIntimacyTempTrxDao fansIntimacyTempTrxDao;

    @Override
    public boolean isFirstTimeGainByTrx(String userMemberId, String channelMemberId, Date enterDate, String trxType) {
        List<FansIntimacyTrx> fansIntimacyTrxList = fansIntimacyTrxDao.isFirstTimeGain(userMemberId, channelMemberId, enterDate, trxType);
        if (fansIntimacyTrxList.size() >= 1) {
            fansIntimacyTrxList.clear();
            fansIntimacyTrxList = null;
            return false;
        }
        return true;
    }

    @Override
    public boolean isFirstTimeGain(String userMemberId, String channelMemberId, Date enterDate, String trxType) {
        MemberFans memberFans = memberFansDao.isFirstTimeGain(userMemberId, channelMemberId, trxType);
        if (memberFans != null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean isSendGiftGain(String fansMemberId, String memberId) {
        BigDecimal todayPoint = findTodayPointSpend(fansMemberId, memberId);
        if (todayPoint.intValue() >= FansIntimacyTrx.SEND_GIFT_TASK) {
            return true;
        }
        return false;
    }

    @Override
    public BigDecimal findTodayIntimacyValue(MemberFans memberFans, String trxType) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        Date lastGainDate = memberFans.getLastIntimacyGain();

        boolean refreshIntimacy = lastGainDate != null && sdf.format(lastGainDate).equals(sdf.format(new Date()));
        if (refreshIntimacy) {
            return new BigDecimal(memberFans.getTodayIntimacyValue());
        } else {
            return new BigDecimal(0);
        }
    }

    @Override
    public BigDecimal findTodayPointSpend(String fansMemberId, String memberId) {
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.BEAN_NAME, MemberFansService.class);
        MemberFans memberFans = memberFansService.findMemberFans(fansMemberId, memberId, "");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        if (memberFans != null) {
            Date lastGainDate = memberFans.getLastPointSpend();

            boolean lastPointSpend = lastGainDate != null && sdf.format(lastGainDate).equals(sdf.format(new Date()));
            if (lastPointSpend) {
                return memberFans.getTodayPointSpend();
            } else {
                return new BigDecimal(0);
            }
        }
        return new BigDecimal(0);
    }

    @Override
    public boolean findLackInteractiveDate(MemberFans memberFans) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        boolean lackInteractive = false;
        if (memberFans != null) {
            Date lackInteractiveDate = memberFans.getLackIntimacyDate();
            lackInteractive = lackInteractiveDate != null && sdf.format(lackInteractiveDate).equals(sdf.format(new Date()));
        }
        return lackInteractive;
    }

    @Override
    public void doIncreaseFanIntimacyValue(Locale locale, String fansMemberId, String memberId, String trxType, BigDecimal value) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.BEAN_NAME, MemberFansService.class);

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/
            Member member = memberService.getMember(fansMemberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            Member channelMember = memberService.getMember(memberId);
            if (channelMember == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            MemberFans memberFans = memberFansService.findMemberFans(fansMemberId, memberId, "");
            if (memberFans == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            /***********************
             * VALIDATION - END
             ***********************/
            int todayTotalGain = findTodayIntimacyValue(memberFans, "").intValue();
            int totalValueForLevel = findTotalValueNeedForLevel(memberFans.getFansLevel() - 1); //redis
            int totalValueForLevelUp = findTotalValueNeedForLevel(memberFans.getFansLevel()); //redis
            int totalDeductTodayGain = memberFans.getIntimacyValue() - todayTotalGain;

            int updateTodayIntimacyValue = 0;

            boolean isLvlToday = totalDeductTodayGain < totalValueForLevel;
            FansIntimacyConfig fansIntimacyConfig = getFansIntimacyConfigByLevel(isLvlToday ? (memberFans.getFansLevel() == 1 ? 1 : memberFans.getFansLevel() - 1) : memberFans.getFansLevel()); //redis

            if (fansIntimacyConfig != null) {
                //check today cap value
                if (todayTotalGain < fansIntimacyConfig.getMaxPerDay()) {
                    memberFans = memberFansService.findMemberFans(fansMemberId, memberId, "");

                    if (trxType.equalsIgnoreCase(FansIntimacyTrx.TYPE_SEND_GIFT)) {
                        if(memberFans.getSendPointInd() == null || !memberFans.getSendPointInd()) {
                            memberFansDao.doSetSendPointInd(fansMemberId,memberId);
                        }
                         FansIntimacyTempTrx fansIntimacyTempTrx = new FansIntimacyTempTrx();
                         fansIntimacyTempTrx.setFansMemberId(fansMemberId);
                         fansIntimacyTempTrx.setMemberId(memberId);
                         fansIntimacyTempTrx.setTrxType(FansIntimacyTrx.TYPE_SEND_GIFT);
                         fansIntimacyTempTrx.setTrxDatetime(new Date());
                         fansIntimacyTempTrx.setPointSpend(value);
                         fansIntimacyTempTrxDao.save(fansIntimacyTempTrx);
                    } else {
                        boolean firstGain = isFirstTimeGain(fansMemberId, memberId, new Date(), trxType);

                        if (firstGain) {
                            if(memberFans.getSendPointInd() == null || !memberFans.getSendPointInd()) {
                                memberFansDao.doSetSendPointInd(fansMemberId,memberId);
                            }
                            FansIntimacyTempTrx fansIntimacyTempTrx = new FansIntimacyTempTrx();
                            fansIntimacyTempTrx.setFansMemberId(fansMemberId);
                            fansIntimacyTempTrx.setMemberId(memberId);
                            fansIntimacyTempTrx.setTrxType(trxType);
                            fansIntimacyTempTrx.setTrxDatetime(new Date());
                            fansIntimacyTempTrx.setPointSpend(value);
                            fansIntimacyTempTrxDao.save(fansIntimacyTempTrx);
                        }
                    }
                }
            }
        }
    }

    @Override
    public FansIntimacyConfig getFansIntimacyConfigByLevel(int fanLevel) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        try {
            if (redisCacheProvider.getRedisStatus()) {
                FansIntimacyRedisServices fansIntimacyRedisServices = Application.lookupBean(FansIntimacyRedisServices.class);
                return fansIntimacyRedisServices.getFansIntimacyConfigByLevel(fanLevel);
            }
            return fansIntimacyConfigDao.getFansIntimacyConfigByLevel(fanLevel);
        } catch (RedisConnectionException redisEx) {
            redisCacheProvider.setRedisStatus(false);
            return fansIntimacyConfigDao.getFansIntimacyConfigByLevel(fanLevel);
        } catch (Exception ex) {
            throw new ValidatorException(ex.getMessage());
        }
    }

    @Override
    public BigDecimal findLackInteractiveRecord(String fansMemberId, String memberId) {
        return fansIntimacyTrxDao.findLackInteractiveRecord(fansMemberId, memberId);
    }

    @Override
    public int findTotalValueNeedForLevel(int fanLevel) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        try {
            if (redisCacheProvider.getRedisStatus()) {
                FansIntimacyRedisServices fansIntimacyRedisServices = Application.lookupBean(FansIntimacyRedisServices.class);
                return fansIntimacyRedisServices.findTotalValueNeedForLevel(fanLevel);
            }
            return fansIntimacyConfigDao.findTotalValueNeedForLevel(fanLevel);
        } catch (RedisConnectionException redisEx) {
            redisCacheProvider.setRedisStatus(false);
            return fansIntimacyConfigDao.findTotalValueNeedForLevel(fanLevel);
        } catch (Exception ex) {
            throw new ValidatorException(ex.getMessage());
        }
    }

    @Override
    public void findFansIntimacyForListing(DatagridModel<FansIntimacyConfig> datagridModel) {
        fansIntimacyConfigDao.findFansIntimacyForDatagrid(datagridModel);
    }

    private static final Object synchronizedUpdateIntimacyConfigObject = new Object();

    @Override
    public void doSaveFansIntimacyConfig(Locale locale, FansIntimacyConfig fansIntimacyConfig) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        synchronized (synchronizedUpdateIntimacyConfigObject) {
            /***********************
             * VALIDATION - START
             ***********************/
            FansIntimacyConfig fansIntimacyConfigDb = fansIntimacyConfigDao.getFansIntimacyConfigByLevel(fansIntimacyConfig.getFansLevel());
            if (fansIntimacyConfigDb != null) {
                throw new ValidatorException(i18n.getText("configForThisLevelExist", locale, fansIntimacyConfig.getFansLevel()));
            }
            /***********************
             * VALIDATION - END
             ***********************/

            fansIntimacyConfigDao.save(fansIntimacyConfig);

            try {
                if (redisCacheProvider.getRedisStatus()) {
                    redisCacheProvider.addFansIntimacyConfigs(fansIntimacyConfig);
                }
            } catch (RedisConnectionException ex) {
                redisCacheProvider.setRedisStatus(false);
            }
        }
    }

    @Override
    public void updateFansIntimacyConfig(Locale locale, FansIntimacyConfig fansIntimacyConfig) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
        synchronized (synchronizedUpdateIntimacyConfigObject) {
            /***********************
             * VALIDATION - START
             ***********************/
            FansIntimacyConfig fansIntimacyConfigDb = fansIntimacyConfigDao.getFansIntimacyConfigByLevel(fansIntimacyConfig.getFansLevel());
            if (fansIntimacyConfigDb != null) {
                if (!fansIntimacyConfigDb.getId().equals(fansIntimacyConfig.getId())) {
                    throw new ValidatorException(i18n.getText("configForThisLevelExist", locale, fansIntimacyConfig.getFansLevel()));
                }
            }

            fansIntimacyConfigDb = fansIntimacyConfigDao.get(fansIntimacyConfig.getId());
            if (fansIntimacyConfigDb == null) {
                throw new ValidatorException(i18n.getText("fansIntimacyConfigNotExist", locale));
            }

            /***********************
             * VALIDATION - END
             ***********************/

            fansIntimacyConfigDb.setFansLevel(fansIntimacyConfig.getFansLevel());
            fansIntimacyConfigDb.setDeductPercent(fansIntimacyConfig.getDeductPercent());
            fansIntimacyConfigDb.setLevelUpValue(fansIntimacyConfig.getLevelUpValue());
            fansIntimacyConfigDb.setMaxPerDay(fansIntimacyConfig.getMaxPerDay());
            fansIntimacyConfigDb.setRemark(fansIntimacyConfig.getRemark());
            fansIntimacyConfigDao.update(fansIntimacyConfigDb);

            try {
                if (redisCacheProvider.getRedisStatus()) {
                    redisCacheProvider.updateFansIntimacyConfigs(fansIntimacyConfigDb);
                }
            } catch (RedisConnectionException ex) {
                redisCacheProvider.setRedisStatus(false);
            }

        }
    }

    @Override
    public FansIntimacyConfig getFansIntimacyConfig(String id) {
        return fansIntimacyConfigDao.get(id);
    }

    @Override
    public void doReduceFansIntimacyForNotActiveFans() {
        Date dateTo = DateUtil.addDate(new Date(), -1); //5 days start from ytd
        Date dateFrom = DateUtil.addDate(dateTo, -4); //5 days start from ytd

        int minimumIntimacy = 50;
        log.debug("REDUCE FANS INTIMACY: check date from " + dateFrom + " to " + dateTo);

        //remove fans badge if no gain more than minimumIntimacy (for fans badge that should be dropped on last lack intimacy date)
        memberFansDao.deleteDroppedMemberFans(DateUtil.addDate(dateFrom, -1), dateTo);

        List<HashMap<String, String>> reduceIntimacyList = fansIntimacyTrxSqlDao.findReduceIntimacyMember(dateFrom, dateTo, minimumIntimacy);
        for (HashMap<String, String> fansIntimacy : reduceIntimacyList) {
            String fansMemberId = fansIntimacy.get("fans_member_id");
            String memberId = fansIntimacy.get("member_id");
            int totalIntimacy = Integer.parseInt(fansIntimacy.get("total_intimacy"));
            int reduceIntimacy = Integer.parseInt(fansIntimacy.get("reduce_intimacy"));
            int intimacyAfterReduce = Integer.parseInt(fansIntimacy.get("intimacy_after_deduct"));

            log.debug("Reduce Intimacy Value of " + fansMemberId + " towards " + memberId + " by " + reduceIntimacy * -1);

            String extraRemark = "";
            //update fans level & fans intimacy value
            MemberFans memberFans = memberFansDao.findMemberFans(fansMemberId, memberId, "");
            memberFans.setLackIntimacyDate(new Date());
            if (memberFans.getFansLevel() == 1) {
                if (intimacyAfterReduce == 0) {
                    //delay 1 day delete record to handle: revert reduced intimacy if reach minimum on the day of intimacy reduced
                    extraRemark = " (Fans badge will be dropped on next day)";
                }

                memberFans.setIntimacyValue(intimacyAfterReduce);
                memberFansDao.update(memberFans);
            } else {
                if (intimacyAfterReduce >= findTotalValueNeedForLevel(memberFans.getFansLevel() - 1)) { //intimacy needed for this level, remain lvl
                    memberFans.setIntimacyValue(intimacyAfterReduce);
                    memberFansDao.update(memberFans);
                } else {
                    memberFans.setIntimacyValue(intimacyAfterReduce);
                    memberFans.setFansLevel(memberFans.getFansLevel() - 1);
                    memberFansDao.update(memberFans);
                }
            }

            //create new record for reduce intimacy value
            FansIntimacyTrx fansIntimacyTrx = new FansIntimacyTrx(true);
            fansIntimacyTrx.setFansMemberId(fansMemberId);
            fansIntimacyTrx.setMemberId(memberId);
            fansIntimacyTrx.setTrxType(FansIntimacyTrx.TYPE_LACK_OF_INTERACT);
            fansIntimacyTrx.setValue(BigDecimal.valueOf(reduceIntimacy));
            fansIntimacyTrx.setTrxDesc("Reduce Fans Intimacy Value " + reduceIntimacy + " due to gain less than " + minimumIntimacy + " within 5 days. Total intimacy within 5 days: " + totalIntimacy + extraRemark);
            fansIntimacyTrxDao.save(fansIntimacyTrx);
        }

        reduceIntimacyList.clear();
        reduceIntimacyList = null;
    }

    @Override
    public List<FansIntimacyConfig> getAllFansIntimacyConfig() {
        return fansIntimacyConfigDao.getAllFansIntimacyConfig();
    }

    @Override
    public void doProcessFansIntimacyGain(String fansMemberId, String memberId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        List<MemberFans> memberFansList = memberFansDao.findMemberFansForGain(fansMemberId,memberId);
        if(memberFansList != null) {
            for(MemberFans memberFans : memberFansList) {
                List<FansIntimacyTempTrx> tempTrx = fansIntimacyTempTrxDao.getRecordByFansId(memberFans.getFansMemberId(),memberFans.getMemberId());
                FansIntimacyConfig fansIntimacyConfig;

                int todayIntimacy = memberFans.getTodayIntimacyValue() == null ? 0 : memberFans.getTodayIntimacyValue();
                Date lastIntimacyGainDate = memberFans.getLastIntimacyGain();
                Date lastPointSpendDate = new Date();
                Date firstEntryDate = null;
                Date contWatchDate = null;
                int returnValue = 0;
                boolean sendGift = false;

                int todayPointSpend = 0;

                int todayTotalGain = 0 ;
                int totalIntimacyValue = memberFans.getIntimacyValue();
                int totalDeductTodayGain;
                int totalValueForLevel;
                boolean isLvlToday = false;
                int fansLevel = memberFans.getFansLevel();

                int currentIntimacyValue = memberFans.getIntimacyValue();
                BigDecimal currentPointSpend = memberFans.getPointSpend() == null ? new BigDecimal(0) : memberFans.getPointSpend();
                int currentPointValue ;
                BigDecimal newPointSpend = new BigDecimal(0);
                int newPointValue ;
                int valueGain ;
                int totalValueForLevelUp = findTotalValueNeedForLevel(memberFans.getFansLevel()); //redis

                for(FansIntimacyTempTrx trx : tempTrx) {
                    todayTotalGain = getTodayIntimacy(lastIntimacyGainDate, todayIntimacy).intValue();
                    if(todayTotalGain == 0) {
                        todayIntimacy = 0 ;
                    }

                    totalDeductTodayGain = totalIntimacyValue - todayTotalGain;
                    totalValueForLevel = findTotalValueNeedForLevel(fansLevel - 1); //redis

                    isLvlToday = isLvlToday || (totalDeductTodayGain < totalValueForLevel);
                    fansIntimacyConfig = getFansIntimacyConfigByLevel(isLvlToday ? (fansLevel == 1 ? 1 : fansLevel - 1) : fansLevel); //redis

                    if(!trx.getTrxType().equalsIgnoreCase(FansIntimacyTrx.TYPE_SEND_GIFT)) {
                        if((trx.getTrxType().equalsIgnoreCase(FansIntimacyTrx.TYPE_FIRST_TIME_ENTRY) && firstEntryDate == null)
                             || (trx.getTrxType().equalsIgnoreCase(FansIntimacyTrx.TYPE_CONT_FIVE_MIN_VIEW) && contWatchDate == null)) {
                            valueGain = trx.getPointSpend().intValue();
                            if (fansIntimacyConfig.getMaxPerDay() < (todayTotalGain + valueGain)) { //if gain value more than today cap
                                valueGain = fansIntimacyConfig.getMaxPerDay() - todayTotalGain;
                            }

                            FansIntimacyTrx fansIntimacyTrx = new FansIntimacyTrx(true);
                            fansIntimacyTrx.setFansMemberId(trx.getFansMemberId());
                            fansIntimacyTrx.setMemberId(trx.getMemberId());
                            fansIntimacyTrx.setTrxType(trx.getTrxType());
                            fansIntimacyTrx.setValue(new BigDecimal(valueGain));
                            fansIntimacyTrx.setTrxDatetime(trx.getTrxDatetime());
                            fansIntimacyTrx.setTrxDesc("Gain Fans Intimacy Value " + valueGain + " By " + trx.getTrxType());
                            fansIntimacyTrxDao.save(fansIntimacyTrx);

                            lastIntimacyGainDate = trx.getTrxDatetime();

                            totalIntimacyValue += valueGain;
                            currentIntimacyValue += valueGain;

                            if (sdf.format(lastIntimacyGainDate).equals(sdf.format(new Date()))) {
                                todayIntimacy += valueGain;
                            }

                            if (currentIntimacyValue >= totalValueForLevelUp && !isLvlToday) {
                                isLvlToday = true;
                                fansLevel += 1;
                            }
                            if (trx.getTrxType().equalsIgnoreCase(FansIntimacyTrx.TYPE_FIRST_TIME_ENTRY)) {
                                firstEntryDate = trx.getTrxDatetime();
                            } else {
                                contWatchDate = trx.getTrxDatetime();
                            }
                        }

                    } else {
                        currentPointValue = (currentPointSpend.intValue() / 1000) * 20;
                        newPointSpend = currentPointSpend.add(trx.getPointSpend());
                        newPointValue = (newPointSpend.intValue() / 1000) * 20;
                        valueGain = newPointValue - currentPointValue;
                        sendGift = true;
                        if (valueGain >= 20) {
                            if (fansIntimacyConfig.getMaxPerDay() < (todayTotalGain + valueGain)) { //if gain value more than today cap
                                valueGain = fansIntimacyConfig.getMaxPerDay() - todayTotalGain;
                            }

                            FansIntimacyTrx fansIntimacyTrx = new FansIntimacyTrx(true);
                            fansIntimacyTrx.setFansMemberId(trx.getFansMemberId());
                            fansIntimacyTrx.setMemberId(trx.getMemberId());
                            fansIntimacyTrx.setTrxType(FansIntimacyTrx.TYPE_SEND_GIFT);
                            fansIntimacyTrx.setValue(new BigDecimal(valueGain));
                            fansIntimacyTrx.setTrxDatetime(trx.getTrxDatetime());
                            fansIntimacyTrx.setTrxDesc("Gain Fans Intimacy Value " + valueGain + " By " + trx.getTrxType());
                            fansIntimacyTrxDao.save(fansIntimacyTrx);

                            lastIntimacyGainDate = trx.getTrxDatetime();

                            totalIntimacyValue += valueGain;
                            currentIntimacyValue += valueGain;

                            if (sdf.format(lastIntimacyGainDate).equals(sdf.format(new Date()))) {
                                todayIntimacy += valueGain;
                            }

                            if (currentIntimacyValue >= totalValueForLevelUp && !isLvlToday) {
                                isLvlToday = true;
                                fansLevel += 1;
                            }
                        }
                        currentPointSpend = currentPointSpend.add(trx.getPointSpend());
                        lastPointSpendDate = trx.getTrxDatetime();
                        if (sdf.format(lastPointSpendDate).equals(sdf.format(new Date()))) {
                            todayPointSpend += trx.getPointSpend().intValue();
                        }
                    }
                }
                Date oldLastPointSpendDate = memberFans.getLastPointSpend();
                if(oldLastPointSpendDate != null && sdf.format(lastPointSpendDate).equals(sdf.format(oldLastPointSpendDate))) {
                    todayPointSpend += memberFans.getTodayPointSpend().intValue();
                }

                MemberFans memberFansUpdate = memberFansDao.get(memberFans.getFansId());
                boolean lackInteractiveInd = findLackInteractiveDate(memberFans);
                if(lackInteractiveInd && todayIntimacy >= 50 ) {
                    int lackInteractive = findLackInteractiveRecord(memberFans.getFansMemberId(), memberFans.getMemberId()).intValue();
                    returnValue = Math.abs(lackInteractive);
                    if(returnValue > 0) {
                        FansIntimacyTrx fansIntimacyTrx = new FansIntimacyTrx(true);
                        fansIntimacyTrx.setFansMemberId(fansMemberId);
                        fansIntimacyTrx.setMemberId(memberId);
                        fansIntimacyTrx.setTrxType(FansIntimacyTrx.TYPE_RETURN_VALUE);
                        fansIntimacyTrx.setValue(new BigDecimal(returnValue));
                        fansIntimacyTrx.setTrxDatetime(new Date());
                        fansIntimacyTrx.setTrxDesc("Return Fans Intimacy Value " + returnValue + " By " + FansIntimacyTrx.TYPE_RETURN_VALUE);
                        fansIntimacyTrxDao.save(fansIntimacyTrx);
                        memberFansUpdate.setLackIntimacyDate(null);
                    }
                }

                memberFansUpdate.setSendPointInd(false);
                memberFansUpdate.setIntimacyValue(currentIntimacyValue + returnValue);
                memberFansUpdate.setLastIntimacyGain(lastIntimacyGainDate);
                memberFansUpdate.setFansLevel(fansLevel);
                memberFansUpdate.setTodayIntimacyValue(todayIntimacy);
                if(sendGift) {
                    memberFansUpdate.setLastPointSpend(lastPointSpendDate);
                }
                if(todayPointSpend > 0) {
                    memberFansUpdate.setTodayPointSpend(new BigDecimal(todayPointSpend));
                }
                if(newPointSpend.intValue() > 0) {
                    memberFansUpdate.setPointSpend(newPointSpend);
                }
                if(firstEntryDate != null) {
                    memberFansUpdate.setFirstEntryDate(firstEntryDate);
                }
                if(contWatchDate != null) {
                    memberFansUpdate.setContWatchingDate(contWatchDate);
                }

                memberFansDao.save(memberFansUpdate);

                fansIntimacyTempTrxDao.deleteAll(tempTrx);
            }
        }
    }

    @Override
    public BigDecimal getTodayIntimacy(Date lastGain, int value) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        boolean refreshIntimacy = lastGain != null && sdf.format(lastGain).equals(sdf.format(new Date()));
        if (refreshIntimacy) {
            return new BigDecimal(value);
        } else {
            return new BigDecimal(0);
        }
    }
}