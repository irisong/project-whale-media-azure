package com.compalsolutions.compal.member.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.SponsorTree;

public interface SponsorTreeDao extends BasicDao<SponsorTree, String> {
    public static final String BEAN_NAME = "sponsorDao";

    public void doUpdateSponsorForMember(long treeId, String oldTracekey, String newTracekey, int adjustLevel, String sponsorId);

    public void doUpdateSponsorForMemberDownline(long id, String oldTracekey, String newTracekey, int adjustLevel, String b32);

    public SponsorTree findSponsorTreeByMemberId(String memberId);

    public int getNoOfDirectDownlines(String memberId);

    public List<SponsorTree> findDirectDownlines(String memberId, int startIndex, int size);

    public int getNoOfDownlines(String memberId, String traceKey);

    public int getNoOfVipDownliens(String memberId, String traceKey);

    public void findDirectDownlineMembersForDatagrid(DatagridModel<Member> datagridModel, String uplineId);

    public SponsorTree findFirstDirectDownline(String uplineId);

    boolean isSkipVipChecking(String skipVipCheckingTracekey, String memberId);

    List<SponsorTree> findDirectDownlines(String parentMemberId);
}
