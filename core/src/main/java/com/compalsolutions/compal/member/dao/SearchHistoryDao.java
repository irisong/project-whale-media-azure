package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.SearchHistory;

import java.util.List;

public interface SearchHistoryDao extends BasicDao<SearchHistory, String> {
    public static final String BEAN_NAME = "searchHistoryDao";

    public SearchHistory findSearchHistory(String memberId, String keyword);

    public List<SearchHistory> findAllSearchHistoryByMemberId(String memberId);

}
