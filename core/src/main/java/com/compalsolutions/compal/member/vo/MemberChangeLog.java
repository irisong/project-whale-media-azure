package com.compalsolutions.compal.member.vo;

import java.util.Date;

import javax.persistence.*;

import com.compalsolutions.compal.agent.vo.Agent;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "mb_member_change_log")
@Access(AccessType.FIELD)
public class MemberChangeLog extends VoBase {
    private static final long serialVersionUID = 1L;

    public MemberChangeLog() {
    }

    public MemberChangeLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_log_id", unique = true, nullable = false, length = 32)
    private String memberLogId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @OneToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false, nullable = true)
    private Member member;

    @ToUpperCase
    @ToTrim
    @Column(name = "old_member_code", length = 50, nullable = false)
    private String oldMemberCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "new_member_code", length = 50, nullable = false)
    private String newMemberCode;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberLogId() {
        return memberLogId;
    }

    public void setMemberLogId(String memberLogId) {
        this.memberLogId = memberLogId;
    }

    public String getOldMemberCode() {
        return oldMemberCode;
    }

    public void setOldMemberCode(String oldMemberCode) {
        this.oldMemberCode = oldMemberCode;
    }

    public String getNewMemberCode() {
        return newMemberCode;
    }

    public void setNewMemberCode(String newMemberCode) {
        this.newMemberCode = newMemberCode;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
