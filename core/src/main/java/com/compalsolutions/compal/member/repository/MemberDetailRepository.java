package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.MemberDetail;

public interface MemberDetailRepository extends JpaRepository<MemberDetail, String> {
    public static final String BEAN_NAME = "memberDetailRepository";
}
