package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.List;

public interface MemberDetailDao extends BasicDao<MemberDetail, String> {
    public static final String BEAN_NAME = "memberDetailDao";

    public MemberDetail findMemberDetailsByWhaleliveId(String whaleliveId);

    public MemberDetail findMemberDetailsByContactNo(String contactNo);
}
