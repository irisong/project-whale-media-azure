package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "mb_sponsor_tree")
@Access(AccessType.FIELD)
public class SponsorTree extends BaseNode implements SolarNode {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "leader_member_id", length = 32, nullable = false)
    private String leaderMemberId;

    @Transient
    private int totalLeft;

    @Transient
    private int totalRight;

    @Transient
    private int totalCenter;

    @Transient
    private int totalDownlines;

    public SponsorTree() {
        super();
    }

    public SponsorTree(boolean defaultValue) {
        super(defaultValue);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getTotalLeft() {
        return totalLeft;
    }

    public void setTotalLeft(int totalLeft) {
        this.totalLeft = totalLeft;
    }

    public int getTotalRight() {
        return totalRight;
    }

    public void setTotalRight(int totalRight) {
        this.totalRight = totalRight;
    }

    public int getTotalCenter() {
        return totalCenter;
    }

    public void setTotalCenter(int totalCenter) {
        this.totalCenter = totalCenter;
    }

    public int getTotalDownlines() {
        return totalDownlines;
    }

    public void setTotalDownlines(int totalDownlines) {
        this.totalDownlines = totalDownlines;
    }

    public String getLeaderMemberId() {
        return leaderMemberId;
    }

    public void setLeaderMemberId(String leaderMemberId) {
        this.leaderMemberId = leaderMemberId;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SponsorTree other = (SponsorTree) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }
}
