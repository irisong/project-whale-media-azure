package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Casting;

import java.util.Date;
import java.util.Locale;

public interface CastingService {
    public static final String BEAN_NAME = "castingService";


    public Casting getCastingByWhaleliveId(String whaleLiveId);

    public Casting getCasting(String Id);
}
