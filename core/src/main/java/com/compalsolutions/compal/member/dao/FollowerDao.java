package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Follower;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.List;

public interface FollowerDao extends BasicDao<Follower, String> {
    public static final String BEAN_NAME = "followerDao";

    public Follower getFollower(String memberId, String followerId);

    public int getFollowerCount(String userMemberId);

    public int getFollowingCount(String userMemberId);

    public List<Follower> getFollowersList(String userMemberId);

    public List<Follower> getFollowingList(String userMemberId);

}
