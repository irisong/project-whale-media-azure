package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.member.dao.SearchHistoryDao;
import com.compalsolutions.compal.member.vo.SearchHistory;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component(SearchService.BEAN_NAME)
public class SearchServiceImpl implements SearchService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private SearchHistoryDao searchHistoryDao;

    @Override
    public void doCreateSearchHistory(String memberId, String keyword) {
        synchronized (synchronizedObject) {
            Validate.notBlank(memberId);
            Validate.notBlank(keyword);

            SearchHistory searchHistory = searchHistoryDao.findSearchHistory(memberId, keyword);

            if (searchHistory != null) {
                searchHistory.setDatetimeUpdate(new Date());
                searchHistoryDao.update(searchHistory);
            } else {
                searchHistory = new SearchHistory();
                searchHistory.setMemberId(memberId);
                searchHistory.setKeyword(keyword);
                searchHistoryDao.save(searchHistory);
            }
        }
    }

    @Override
    public void doRemoveSearchHistory(String memberId, String keyword) {
        if (StringUtils.isNotBlank(keyword)) {
            SearchHistory searchHistory = searchHistoryDao.findSearchHistory(memberId, keyword);

            if (searchHistory != null) {
                searchHistoryDao.delete(searchHistory);
            }
        } else {
            List<SearchHistory> searchHistoryList = searchHistoryDao.findAllSearchHistoryByMemberId(memberId);

            if (searchHistoryList.size() > 0) {
                searchHistoryDao.deleteAll(searchHistoryList);
            }
            searchHistoryList.clear();
            searchHistoryList = null;
        }
    }

    @Override
    public List<SearchHistory> findAllSearchHistoryByMemberId(String memberId) {
        return searchHistoryDao.findAllSearchHistoryByMemberId(memberId);
    }
}
