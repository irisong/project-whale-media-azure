package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MemberReportFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.member.dao.MemberReportDao;
import com.compalsolutions.compal.member.dao.MemberReportFileDao;
import com.compalsolutions.compal.member.dao.MemberReportTypeDao;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberReport;
import com.compalsolutions.compal.member.vo.MemberReportFile;
import com.compalsolutions.compal.member.vo.MemberReportType;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.telegram.BotClient;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(MemberReportService.BEAN_NAME)
public class MemberReportServiceImpl implements MemberReportService {
    private static final Log log = LogFactory.getLog(MemberReportService.class);
    //private static final Object synchronizedObject = new Object();

    @Autowired
    private MemberReportTypeDao memberReportTypeDao;

    @Autowired
    private MemberReportDao memberReportDao;

    @Autowired
    private MemberReportFileDao memberReportFileDao;

    @Override
    public MemberReportType getMemberReportType(String reportTypeId) {
        return memberReportTypeDao.get(reportTypeId);
    }

    @Override
    public MemberReport getMemberReport(String reportId) {
        return memberReportDao.get(reportId);
    }

    @Override
    public List<MemberReportType> findMemberReportTypes(String status) {
        return memberReportTypeDao.findMemberReportTypes(status);
    }

    private static final Object synchronizedMemberReportObject = new Object();

    @Override
    public void doProcessMemberReport(Locale locale, String userMemberId, String reportMemberId,
                                      String reportTypeId, String description, List<MemberReportFile> uploadImageList) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedMemberReportObject) {
            /************************
             * VERIFICATION - START
             ************************/
            if (StringUtils.isBlank(userMemberId)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            Member member = memberService.getMember(userMemberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (StringUtils.isBlank(reportMemberId)) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            Member reportMember = memberService.getMember(reportMemberId);
            if (reportMember == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (StringUtils.isBlank(reportTypeId)) {
                throw new ValidatorException(i18n.getText("invalidType", locale));
            }

            MemberReportType memberReportType = memberReportTypeDao.get(reportTypeId);

            if (memberReportType == null) {
                throw new ValidatorException(i18n.getText("invalidType", locale));
            }

            if (uploadImageList.size() < 1) {
                throw new ValidatorException(i18n.getText("pleaseUploadImage", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/

            MemberReport memberReport = new MemberReport(true);
            memberReport.setReportTypeId(reportTypeId);
            memberReport.setMemberId(member.getMemberId());
            memberReport.setMemberType(member.getUserRole());
            memberReport.setReportMemberId(reportMember.getMemberId());
            memberReport.setReportMemberType(reportMember.getUserRole());
            memberReport.setDescription(description);
            memberReport.setWarningCount(0);

            memberReportDao.save(memberReport);

            if (CollectionUtil.isNotEmpty(uploadImageList)) {
                doAddMemberReportFile(memberReport.getReportId(), uploadImageList);
            }
            String msg = member.getMemberCode() + "(" + member.getMemberDetail().getProfileName() + ") reported user " + reportMember.getMemberCode()
                    + "(" + reportMember.getMemberDetail().getProfileName()
                    + ") for reason of: [" + memberReportType.getTypeName() + "] " + description;
            BotClient botClient = new BotClient();
            botClient.doSendReportMessage(msg);
        }
    }

    @Override
    public void doProcessWarningMessage(Member warningMember, Member member ,String content, String contentCn) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MessageService messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
        Environment env = Application.lookupBean(Environment.class);

        Message message = messageService.doGenerateWarningMessage(warningMember.getMemberId(), content, contentCn);
        Message message2 = messageService.doGenerateReplyWarningMessage(member.getMemberId());
        String serverUrl = env.getProperty("serverUrl");
        String redirectUrl = serverUrl + "/pub/webView.php?messageId=" + message.getMessageId();
        String redirectUrl2 = serverUrl + "/pub/webView.php?messageId=" + message2.getMessageId();

        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.ANNOUNCEMENT);

        String language = Global.Language.ENGLISH;
        NotificationAccount frmAccount = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        NotificationAccount toAccount = notificationService.findNotifAccByMemberAndAccessModule(warningMember.getMemberId(), Global.AccessModule.WHALE_MEDIA);
        NotificationAccount replyAccount = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);

        if (frmAccount != null && toAccount != null) {
            Locale locale = Global.LOCALE_EN;
            if (warningMember.getPreferLanguage().equals(Global.Language.CHINESE)) {
                locale = Global.LOCALE_CN;
                language = Global.Language.CHINESE;

            } else if (warningMember.getPreferLanguage().equals(Global.Language.MALAY)) {
                locale = Global.LOCALE_MS;
                language = Global.Language.MALAY;
            }

            payload.setRedirectUrl(redirectUrl + "&languageCode=" + language);

            notificationConfProvider.doSendMessage(frmAccount.getAccountId(), toAccount.getAccountId(), Global.AccessModule.WHALE_MEDIA,
                    i18n.getText("title.warningMessage", locale), payload, null);
        }

        if (replyAccount != null) {
            payload = new Payload();
            payload.setType(Payload.ANNOUNCEMENT);

            Locale locale = Global.LOCALE_EN;
            if (member.getPreferLanguage().equals(Global.Language.CHINESE)) {
                    locale = Global.LOCALE_CN;
                    language = Global.Language.CHINESE;
            } else if (member.getPreferLanguage().equals(Global.Language.MALAY)) {
                locale = Global.LOCALE_MS;
                language = Global.Language.MALAY;
            }

            payload.setRedirectUrl(redirectUrl2 + "&languageCode=" + language);

            notificationConfProvider.doSendMessage(frmAccount.getAccountId(), replyAccount.getAccountId(), Global.AccessModule.WHALE_MEDIA,
                    i18n.getText("title.systemReply", locale), payload, null);

        }
    }

    @Override
    public void doAddMemberReportFile(String reportId, List<MemberReportFile> uploadImageList) {
        MemberReportFileUploadConfiguration config = Application.lookupBean(MemberReportFileUploadConfiguration.BEAN_NAME, MemberReportFileUploadConfiguration.class);
        final String uploadPath = config.getMemberReportFileUploadPath();
        Environment env = Application.lookupBean(Environment.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);


        for (MemberReportFile fileUpload : uploadImageList) {
            if (StringUtils.isNotBlank(fileUpload.getFilename())) {
                MemberReportFile memberReportFile = new MemberReportFile(true);
                memberReportFile.setReportId(reportId);
                memberReportFile.setFilename(fileUpload.getFilename());
                memberReportFile.setContentType(fileUpload.getContentType());
                memberReportFile.setFileSize(fileUpload.getFileSize());
                memberReportFile.setSortOrder(fileUpload.getSortOrder());
                memberReportFileDao.save(memberReportFile);

                if (!isProdServer) {
                    File directory = new File(uploadPath);
                    if (!directory.exists()) {
                        directory.mkdirs();
                    }
                }
                try {
                    byte[] bytes = fileUpload.getData();
                    if (isProdServer) {
                        azureBlobStorageService.uploadFileToAzureBlobStorage(memberReportFile.getRenamedFilename(), null,
                                null, memberReportFile.getContentType(), MemberReportFileUploadConfiguration.FOLDER_NAME,
                                bytes, memberReportFile.getFileSize());
                    } else {
                        Path path = Paths.get(uploadPath, memberReportFile.getRenamedFilename());
                        Files.write(path, bytes);
                    }
                } catch (IOException e) {
                    throw new SystemErrorException(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void findMemberReportTypeForListing(DatagridModel<MemberReportType> datagridModel, String status) {
        memberReportTypeDao.findMemberReportTypeForDatagrid(datagridModel, status);
    }

    @Override
    public void saveMemberReportType(MemberReportType memberReportType) {
        memberReportTypeDao.save(memberReportType);
    }

    private static final Object synchronizedUpdateMemberReportObject = new Object();

    @Override
    public void updateMemberReportType(Locale locale, MemberReportType memberReportType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        synchronized (synchronizedUpdateMemberReportObject) {
            MemberReportType dbMemberReportType = memberReportTypeDao.get(memberReportType.getReportTypeId());
            if (dbMemberReportType == null)
                throw new DataException(i18n.getText("invalidMemberReportType", locale));

            dbMemberReportType.setTypeName(memberReportType.getTypeName());
            dbMemberReportType.setTypeNameCn(memberReportType.getTypeNameCn());
            dbMemberReportType.setTypeNameMs(memberReportType.getTypeNameMs());
            dbMemberReportType.setStatus(memberReportType.getStatus());
            dbMemberReportType.setSortOrder(memberReportType.getSortOrder());
            memberReportTypeDao.update(dbMemberReportType);
        }
    }

    @Override
    public void findMemberReportForListing(DatagridModel<MemberReport> datagridModel, Date dateFrom, Date dateTo, String reportTypeId, String reportMemberId, Boolean isRead) {
        memberReportDao.findMemberReportForDatagrid(datagridModel, dateFrom, dateTo, reportTypeId, reportMemberId, isRead);
    }

    private static final Object synchronizedUpdateMemberReportReadObject = new Object();

    @Override
    public void updateMemberReportRead(String reportId, LoginInfo loginInfo) {
        synchronized (synchronizedUpdateMemberReportReadObject) {
            MemberReport memberReport = memberReportDao.get(reportId);
            if (memberReport != null) {
                memberReport.setReadStatus(true);
                memberReport.setReadDatetime(new Date());
                memberReport.setReadBy(loginInfo.getUserId());
                memberReportDao.update(memberReport);
            }
        }
    }

    @Override
    public String getMemberReportTypeByLanguage(String reportTypeId, String language) {
        MemberReportType memberReportType = memberReportTypeDao.get(reportTypeId);

        String memberReportTypeName;
        switch (language) {
            case Global.Language.CHINESE:
                memberReportTypeName = memberReportType.getTypeNameCn();
                break;
            case Global.Language.MALAY:
                memberReportTypeName = memberReportType.getTypeNameMs();
                break;
            default:
                memberReportTypeName = memberReportType.getTypeName();
                break;
        }

        return memberReportTypeName;
    }

    private static final Object synchronizedDoUpdateWarningCount = new Object();

    @Override
    public void doUpdateWarningCount(MemberReport memberReport) {
        synchronized (synchronizedDoUpdateWarningCount) {
            MemberReport dbMemberReport = memberReportDao.get(memberReport.getReportId());

            dbMemberReport.setWarningCount(memberReport.getWarningCount() + 1);
            memberReportDao.update(dbMemberReport);
        }
    }

}
