package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MemberReportFile;

public interface MemberReportFileDao extends BasicDao<MemberReportFile, String> {
    public static final String BEAN_NAME = "memberReportFileDao";
}
