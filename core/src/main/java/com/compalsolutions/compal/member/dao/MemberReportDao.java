package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberReport;

import java.util.Date;

public interface MemberReportDao extends BasicDao<MemberReport, String> {
    public static final String BEAN_NAME = "memberReportDao";

    public void findMemberReportForDatagrid(DatagridModel<MemberReport> datagridModel, Date dateFrom, Date dateTo, String reportTypeId, String reportMemberId, Boolean isRead);
}
