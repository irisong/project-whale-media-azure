package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberReportType;

import java.util.List;

public interface MemberReportTypeDao extends BasicDao<MemberReportType, String> {
    public static final String BEAN_NAME = "memberReportTypeDao";

    public List<MemberReportType> findMemberReportTypes(String status);

    public void findMemberReportTypeForDatagrid(DatagridModel<MemberReportType> datagridModel, String status);
}
