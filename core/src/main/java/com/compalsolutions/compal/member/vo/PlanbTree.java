package com.compalsolutions.compal.member.vo;

import java.util.Objects;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "mb_planb_tree")
@Access(AccessType.FIELD)
public class PlanbTree extends BaseNode implements BinaryNode {
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "tree_path", columnDefinition = "text")
    protected String treePath;

    @Transient
    private SponsorTree sponsorTree;
    public PlanbTree() {
    }

    public PlanbTree(boolean defaultValue) {
        super(defaultValue);
        if (defaultValue) {
            treeStyle = TreeConstant.TREE_STYLE_AUTO_FULL_TREE;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTreePath() {
        return treePath;
    }

    public void setTreePath(String treePath) {
        this.treePath = treePath;
    }

    @Override
    public SponsorTree getSponsorTree() {
        return sponsorTree;
    }

    @Override
    public void setSponsorTree(SponsorTree sponsorTree) {
        this.sponsorTree = sponsorTree;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        PlanbTree planbTree = (PlanbTree) o;
        return Objects.equals(id, planbTree.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String getDisplayName() {
        if (member != null) {
            return member.getMemberCode() + "-" + unit;
        }

        return null + "-" + unit;
    }

    public boolean isMaintained() {
        return false;
    }
}
