package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(FansBadgeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FansBadgeDaoImpl extends Jpa2Dao<FansBadge, String> implements FansBadgeDao {

    public FansBadgeDaoImpl() {
        super(new FansBadge(false));
    }

    @Override
    public FansBadge getLatestFansBadgeRequested(String memberId, String status) {
        Validate.notBlank(memberId);

        FansBadge fansBadge = new FansBadge(false);
        fansBadge.setMemberId(memberId);

        if (StringUtils.isNotBlank(status)) {
            fansBadge.setStatus(status);
        }

        return findFirst(fansBadge, "createDate desc");
    }

    @Override
    public FansBadge getActiveFansBadgeByName(String fansBadgeName) {
        Validate.notBlank(fansBadgeName);

        String hql = "FROM b IN " + FansBadge.class + " "
                + "WHERE b.status = ? AND lower(b.fansBadgeName) = ? ";

        List<Object> params = new ArrayList<>();
        params.add(Global.Status.ACTIVE);
        params.add(fansBadgeName.toLowerCase());

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<FansBadge> getFansBadgeWithinDatePeriod(String memberId, Date datePeriod) {
        Validate.notBlank(memberId);

        List<Object> params = new ArrayList<>();
        String hql = "FROM b IN " + FansBadge.class + " "
                   + "WHERE b.memberId = ? AND b.status <> ? AND b.createDate >= ? ";

        params.add(memberId);
        params.add(Global.Status.REJECTED);
        params.add(datePeriod);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findFansBadgesForDatagrid(DatagridModel<FansBadge> datagridModel, String memberCode, Date createDateFrom, Date createDateTo, String status) {
        List<Object> params = new ArrayList<Object>();

        String innerJoinCriteria = "";
        if (StringUtils.isNotBlank(memberCode)) {
            innerJoinCriteria += " and m.memberCode like ? ";
            params.add(memberCode + "%");
        }

        String hql = "SELECT f FROM FansBadge f INNER JOIN Member m ON m.memberId = f.memberId "+innerJoinCriteria+" WHERE 1=1 ";

        if (createDateFrom != null) {
            hql += " and f.createDate >= ? ";
            params.add(DateUtil.truncateTime(createDateFrom));
        }

        if (createDateTo != null) {
            hql += " and f.createDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(createDateTo));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and f.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "f", hql, params.toArray());
    }

    public FansBadge getFansBadgeName(String memberId){
        Validate.notBlank(memberId);

        String hql = "FROM b IN " + FansBadge.class + " "
                + "WHERE b.status = ? AND lower(b.memberId) = ? ";

        List<Object> params = new ArrayList<>();
        params.add(Global.Status.ACTIVE);
        params.add(memberId.toLowerCase());

        return findFirst(hql, params.toArray());
    }
}
