package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberChangeLog;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MemberChangeLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberChangeLogDaoImpl extends Jpa2Dao<MemberChangeLog, String> implements MemberChangeLogDao {

    public MemberChangeLogDaoImpl() {
        super(new MemberChangeLog(false));
    }

    @Override
    public MemberChangeLog findMemberLogByMemberCode(String memberId) {

        MemberChangeLog example = new MemberChangeLog(false);
        example.setMemberId(memberId);

        return findUnique(example);
    }

}