package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberFans;

import java.util.Date;
import java.util.List;

public interface MemberFansDao extends BasicDao<MemberFans, String> {
    public static final String BEAN_NAME = "memberFansDao";

    public MemberFans findMemberFans(String fansMemberId, String memberId, String role);

    public List<MemberFans> getMemberFans(String memberId, String role);

    public int findTotalFans(String memberId);

    public void findMemberFansBadgeForDatagrid(DatagridModel<MemberFans> datagridModel, String fansMemberId, String role);

    public void doUnwearBadge(String fansMemberId);

    public MemberFans findFansWearingBadge(String fansMemberId);

    public MemberFans isFirstTimeGain(String fansMemberId, String memberId, String type);

    public void doSetSendPointInd(String fansMemberId, String memberId);

    public List<MemberFans> findMemberFansForGain(String fansMemberId, String memberId);

    public void deleteDroppedMemberFans(Date joinDate, Date lackIntimacyDate);
}
