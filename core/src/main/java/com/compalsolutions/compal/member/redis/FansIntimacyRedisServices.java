package com.compalsolutions.compal.member.redis;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface FansIntimacyRedisServices {
    public static final String BEAN_NAME = "fansIntimacyRedisService";

    public int findTotalValueNeedForLevel(int level);

    public FansIntimacyConfig getFansIntimacyConfigByLevel(int level);
}
