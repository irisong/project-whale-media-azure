package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.MemberAddress;

public interface MemberAddressRepository extends JpaRepository<MemberAddress, String> {
    public static final String BEAN_NAME = "memberAddressRepository";
}
