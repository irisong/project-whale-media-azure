package com.compalsolutions.compal.member.repository;

import com.compalsolutions.compal.member.vo.Casting;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CastingRespository extends JpaRepository<Casting, String> {
    public static final String BEAN_NAME = "castingRepository";
}
