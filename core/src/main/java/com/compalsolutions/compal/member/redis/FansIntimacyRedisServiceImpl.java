package com.compalsolutions.compal.member.redis;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.redisson.api.RList;
import org.springframework.stereotype.Component;


@Component(FansIntimacyRedisServices.BEAN_NAME)
public class FansIntimacyRedisServiceImpl implements FansIntimacyRedisServices {
    private static final Object synchronizedObject = new Object();
    private static final Log log = LogFactory.getLog(FansIntimacyRedisServices.class);

    @Override
    public int findTotalValueNeedForLevel(int level) {
        try {
            RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
            RList<FansIntimacyConfig> fansIntimacyConfigsRList = redisCacheProvider.getFansIntimacyConfigsRList();
            return fansIntimacyConfigsRList.stream().filter(f -> f.getFansLevel() <= level).mapToInt(f -> f.getLevelUpValue()).sum();
        } catch (Exception ex) {
            throw ex;
        }
    }

    @Override
    public FansIntimacyConfig getFansIntimacyConfigByLevel(int level) {
        try {
            RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);
            RList<FansIntimacyConfig> fansIntimacyConfigsRList = redisCacheProvider.getFansIntimacyConfigsRList();
            return fansIntimacyConfigsRList.stream().filter(f -> f.getFansLevel() == level).findFirst().get();
        } catch (Exception ex) {
            throw ex;
        }
    }
}
