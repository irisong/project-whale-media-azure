package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "mb_fans_intimacy_config")
@Access(AccessType.FIELD)
public class FansIntimacyConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "fans_level", nullable = false)
    private Integer fansLevel;

    @Column(name = "level_up_value", length = 32, nullable = false)
    private Integer levelUpValue;

    @Column(name = "max_per_day", length = 32, nullable = false)
    private Integer maxPerDay;

    @Column(name = "deduct_percent", nullable = false)
    private Double deductPercent;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    public FansIntimacyConfig() {
    }

    public FansIntimacyConfig(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getFansLevel() {
        return fansLevel;
    }

    public void setFansLevel(Integer fansLevel) {
        this.fansLevel = fansLevel;
    }

    public Integer getLevelUpValue() {
        return levelUpValue;
    }

    public void setLevelUpValue(Integer levelUpValue) {
        this.levelUpValue = levelUpValue;
    }

    public Integer getMaxPerDay() {
        return maxPerDay;
    }

    public void setMaxPerDay(Integer maxPerDay) {
        this.maxPerDay = maxPerDay;
    }

    public Double getDeductPercent() {
        return deductPercent;
    }

    public void setDeductPercent(Double deductPercent) {
        this.deductPercent = deductPercent;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
