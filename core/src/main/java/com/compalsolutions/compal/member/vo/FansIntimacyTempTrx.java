package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "mb_fans_intimacy_temp_trx")
@Access(AccessType.FIELD)
public class FansIntimacyTempTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "fans_member_id", nullable = false, length = 32)
    private String fansMemberId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "point_spend", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal pointSpend;

    @ToTrim
    @ToUpperCase
    @Column(name = "trx_type")
    private String trxType;

    public FansIntimacyTempTrx() {
    }

    public FansIntimacyTempTrx(boolean defaultValue) {
        if (defaultValue) {
            trxDatetime = new Date();
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFansMemberId() {
        return fansMemberId;
    }

    public void setFansMemberId(String fansMemberId) {
        this.fansMemberId = fansMemberId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public BigDecimal getPointSpend() {
        return pointSpend;
    }

    public void setPointSpend(BigDecimal pointSpend) {
        this.pointSpend = pointSpend;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }
}
