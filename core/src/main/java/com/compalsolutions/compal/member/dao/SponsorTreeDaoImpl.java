package com.compalsolutions.compal.member.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.repository.SponsorTreeRepository;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.SponsorTree;

@Component(SponsorTreeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SponsorTreeDaoImpl extends Jpa2Dao<SponsorTree, String> implements SponsorTreeDao {
    @SuppressWarnings("unused")
    @Autowired
    private SponsorTreeRepository sponsorTreeRepository;

    public SponsorTreeDaoImpl() {
        super(new SponsorTree(false));
    }

    @Override
    public void doUpdateSponsorForMember(long treeId, String oldTracekey, String newTracekey, int adjustLevel, String sponsorId) {
        Validate.notBlank(oldTracekey);
        Validate.notBlank(newTracekey);
        Validate.notBlank(sponsorId);

        List<Object> params = Arrays.asList(oldTracekey, newTracekey, adjustLevel, sponsorId, treeId);

        String hql = "update SponsorTree sp set sp.traceKey=replace(sp.traceKey, ?, ?), " //
                + "sp.level = sp.level - ?, " //
                + "sp.parentId = ? " //
                + "where sp.id = ?";

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doUpdateSponsorForMemberDownline(long id, String oldTracekey, String newTracekey, int adjustLevel, String b32) {
        Validate.notBlank(oldTracekey);
        Validate.notBlank(newTracekey);
        Validate.notBlank(b32);

        List<Object> params = Arrays.asList(oldTracekey, newTracekey, adjustLevel, "%/" + b32 + "/%", id);

        String hql = "update SponsorTree sp set sp.traceKey=replace(sp.traceKey, ?, ?), " //
                + "sp.level = sp.level - ? " //
                + "where sp.traceKey like ? and sp.id != ? ";

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public SponsorTree findSponsorTreeByMemberId(String memberId) {
        if (StringUtils.isBlank(memberId))
            return null;

        SponsorTree example = new SponsorTree(false);
        example.setMemberId(memberId);
        return findFirst(example);
    }

    @Override
    public int getNoOfDirectDownlines(String memberId) {
        String hql = "select count(*) from SponsorTree sp where sp.parentId = ? and sp.status =? ";

        Long count = (Long) exFindFirst(hql, memberId, Global.Status.ACTIVE);
        if (count != null)
            return count.intValue();

        return 0;
    }

    @Override
    public List<SponsorTree> findDirectDownlines(String memberId, int startIndex, int size) {
        String hql = "from SponsorTree sp where sp.parentId = ? and sp.status = ? order by sp.member.memberCode ";
        return findBlock(hql, startIndex, size, memberId, Global.Status.ACTIVE);
    }

    @Override
    public int getNoOfDownlines(String memberId, String traceKey) {
        String hql = "select count(*) from SponsorTree sp where sp.traceKey like ? and sp.status = ? and sp.memberId != ?";

        Long count = (Long) exFindFirst(hql, traceKey + "%", Global.Status.ACTIVE, memberId);
        if (count != null)
            return count.intValue();

        return 0;
    }

    @Override
    public int getNoOfVipDownliens(String memberId, String traceKey) {
        String hql = "select count(*) from SponsorTree sp inner join sp.member mb where sp.status=? and sp.memberId!=? and sp.traceKey like ? and mb.vipRank>0";

        Long count = (Long) exFindUnique(hql, Global.Status.ACTIVE, memberId, traceKey + "/%");
        if (count != null)
            return count.intValue();

        return 0;
    }

    @Override
    public void findDirectDownlineMembersForDatagrid(DatagridModel<Member> datagridModel, String uplineId) {
        String hql = "select mb from SponsorTree sp inner join sp.member mb where sp.parentId = ? and sp.status=? ";

        List<Object> params = new ArrayList<>();
        params.add(uplineId);
        params.add(Global.Status.ACTIVE);

        exFindForDatagrid(datagridModel, "mb", hql, params.toArray());
    }

    @Override
    public SponsorTree findFirstDirectDownline(String uplineId) {
        String hql = "select sp from SponsorTree sp inner join sp.member mb where sp.parentId =? and sp.status=? and mb.activeDatetime!=null order by mb.activeDatetime asc";

        List<Object> params = new ArrayList<>();
        params.add(uplineId);
        params.add(Global.Status.ACTIVE);

        return findFirst(hql, params.toArray());
    }

    @Override
    public boolean isSkipVipChecking(String skipVipCheckingTracekey, String memberId) {
        String hql = " FROM SponsorTree sp WHERE sp.memberId = ? AND sp.traceKey LIKE ? ";

        List<Object> params = new ArrayList<>();
        params.add(memberId);
        params.add(skipVipCheckingTracekey + "%");

        SponsorTree sponsorTree = findFirst(hql, params.toArray());
        if (sponsorTree != null) {
            return true;
        }
        return false;
    }

    @Override
    public List<SponsorTree> findDirectDownlines(String parentMemberId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM m IN " + SponsorTree.class + " WHERE 1=1 ";

        if (org.apache.commons.lang.StringUtils.isNotBlank(parentMemberId)) {
            hql += " AND m.parentId = ? ";
            params.add(parentMemberId);
        }
        return findQueryAsList(hql, params.toArray());
    }
}
