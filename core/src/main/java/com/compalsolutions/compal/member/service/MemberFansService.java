package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberFans;
import org.apache.commons.lang3.tuple.Triple;

import java.math.BigDecimal;
import java.util.Locale;

public interface MemberFansService {
    public static final String BEAN_NAME = "memberFansService";

    public MemberFans getMemberFans(String fansId);

    public MemberFans findMemberFans(String fansMemberId, String memberId, String role);

    public void findMemberFansForListing(DatagridModel<MemberFans> datagridModel, String memberId);

    public void findMemberFansBadgeForListing(DatagridModel<MemberFans> datagridModel, String fansMemberId, String role);

    public boolean isMemberFans(String fansMemberId, String memberId, String role);

    public void doAddMemberFan(Locale locale, String memberId, String fansMemberId);

    public void doAddMemberAdmin(Locale locale, String memberId, String fansMemberId, String role);
    
    public int findTotalFans(String memberId);

    public void doProcessFansRoleNotification(MemberFans memberFans, String role);

    public BigDecimal doIncreasePointSpend(MemberFans memberFans, BigDecimal value);

    public int doIncreaseTodayFansIntimacy(MemberFans memberFans, int value);

    public void doProcessWearBadge(Locale locale, String fansMemberId, String memberId);

    public MemberFans findFansWearingBadge(String fansMemberId);

    public Triple<String,String,String> getDisplayFansBadgeInfo(String memberId, String channelMemberId);
}
