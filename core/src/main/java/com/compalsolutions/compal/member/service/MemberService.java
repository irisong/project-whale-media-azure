package com.compalsolutions.compal.member.service;

import java.io.File;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.MemberMnemonic;
import com.compalsolutions.compal.member.dto.ProfileDto;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.user.vo.MemberUser;
import org.apache.commons.lang3.tuple.Pair;

public interface MemberService {
    public static final String BEAN_NAME = "memberService";

    public void findMembersForListing(DatagridModel<Member> datagridModel, String agentId, String memberCode, Date joinDateFrom,
                                      Date joinDateTo, String status,String whaleLiveId);

    public void doProcessMemberLevel(String memberId, BigDecimal exp);

    public void findGuildMembersForListing(DatagridModel<Member> datagridModel, String guildId);

    public void findMembersForKYCListing(DatagridModel<Member> datagridModel, String memberCode, String kycStatus,String whaleLiveId,Date dateFrom,Date dateTo);

    public void doCreateMember(Locale locale, Member member, String password, String agentId);

    public Member getMember(String memberId);

    public Pair<Integer, String> doGetChannelTokenInfo(String memberId, String channelName);

    public int doGetMemberAgoraUid(Member member);

    public int generateAgoraUid();

    public boolean checkAgoraUidExist(int uid);

    public Member findMemberByAgoraUid(int uid);

    public String doGetMemberChannelName(Member member);

    public boolean checkChannelNameExist(String channelName);

    public void updateMember(Member member);

    public Member findMemberByMemberCode(String memberCode);

    public Member findMemberByMemberCodeOrWhaleliveId(String code);

    public MemberUser findMemberUserByMemberId(String memberId);

    public void doProcessMemberUploadFile(String memberId, String uploadType, File fileUpload, String fileUploadContentType, String fileUploadFileName);

    public void doSendNotificationEmailToAdmin(String ccAddress, String bccAddress, String title, String body);

    public int getTotalMembers();

    public boolean isMemberSecondPasswordValid(Locale locale, String memberId, String secondPassword);

//    public void doUpdateMemberProfile(String memberId, String countryCode, Date dob, String address, String address2, String city, String state,
//            String postcode, String email, String alternateEmail, String contact, String gender);

//    public void doUpdateMemberProfile(String memberId, String firstName, String lastName, String contact, String email, Date dob, String gender,
//            String identyNo, String identyType);

    public void doUpdateMember(String memberId,String fullName,String identity_no);

    public void doUpdateMemberProfile(String memberId, String currency);

    public void doUpdateMemberSetting(Member member_new);

    public void doUpdateMemberNotificationSetting(Locale locale, String memberId, String type, boolean sendNotif);

    public void doUpdateMemberKycStatus(String memberId,String kycStatus);

    public String doValidateMemberPassword(Locale locale, String memberId, String oldPassword, String newPassword);

    public void doChangeMemberPassword(Locale locale, String memberId, String oldPassword, String newPassword);

    public void doChangeMemberSecondPassword(Locale locale, String memberId, String oldPassword2, String newPassword2);

    public void doResetPassword(Locale locale, String memberId, String password);

    public void doResetPassword2(Locale locale, String memberId, String password2);

    public MemberFile findMemberFileByMemberIdAndType(String memberId, String type);

    public void doSendWelcomeLetter(Locale locale, Member member, String password);

    public MemberAddress getMemberAddressDetails(String memberId, String addressId);

    Member findActiveMemberByMemberCode(String memberCode);

    public int findNoOfMembersByRank(String rankId);

    public void doCreateSimpleMember(Locale locale, String phoneNo, String password, String verifyCode, int durationInMinutes, boolean skipSmsVerify);

    public void findSimilarMemberDetailByProfileName(DatagridModel<MemberDetail> memberDetailsPaging, String keyword, String userMemberDetailId);

    public void doRetrieveMemberPasswordByEmail(Locale locale, String memberCode);

    public void doSendRetrieveMemberPasswordLetter(Locale locale, Member member, String password);

    public void setEmailq(String email, String title, String content);

    public MemberMnemonic doCreateMnemonic(Locale locale, String memberId, String userId, String transactionPassword);

    public MemberMnemonic findActiveMemberMnemonicByMemberId(String memberId);

    public void doCheckChangePasswordValidity(Locale locale, String currentPassword, String oldPassword, String newPassword, String confirmPassword);

    public List<Member> findAllMembers();

    public void doProcessFreeWhaleCoinNewUser(String memberId);

    public void doMemberSelfResetPasswordRequest(Locale Locale, String phoneno);

    public void doMemberSelfResetPassword(Locale locale, String phoneno, String verifyCode, String newPassword, int durationInMinutes);

    public void doUpdateUserProfile(Locale locale, String memberId, String username, String email, String gender, String country, String birthday, String bio);

    public MemberDetail findMemberDetailByMemberId(String memberId);

    public MemberDetail findMemberDetailByWhaleliveId(String whaleliveId);

    public MemberDetail findMemberDetailsByContactNo(String contactNo);

    public void doUpdateMemberBankInfo(Locale locale, String memberId, MemberDetail memberDetail);

    public void doProcessMemberFiles(Locale locale, String memberId, File fileUploadBank, String fileUploadBankContentType, String fileUploadBankFileName,
                                     File fileUploadResidence, String fileUploadResidenceContentType, String fileUploadResidenceFileName, File fileUploadIc,
                                     String fileUploadIcContentType, String fileUploadIcFileName);

    public MemberFile getMemberFile(String fileId);

    public List<MemberFile> findMemberFilesByMemberId(String memberId);

    public void doProcessUploadMemberFile(Locale locale, String memberId, String uploadType, InputStream fileInputStream, String fileName, long fileSize, String contentType);

    public Member doCreateSimpleMember(String... arg);

    public Member findMemberByMemberDetId(String memberDetailId);

    public ProfileDto getProfileDetails(Locale locale, String userMemberId, String targetMemberId, String viewFrmChannelId);

    public MemberAddress findMemberAddressById(String memberAddrId);

    public List<MemberAddress> findMemberAddressByMemberId(String memberId);

    public MemberAddress findDefaultMemberAddress(String memberId);

    public MemberAddress doCreateMemberAddress (Locale locale, String memberId, String name, String phoneNo,
                                                  String countryCode, String address, String city, String postCode,
                                                  String state, String area, boolean defaultAddress, String phoneCountryCode);

    public MemberAddress doUpdateMemberAddress (Locale locale, String memberId, String memberAddrId, String name, String phoneNo,
                                                  String countryCode, String address, String city, String postCode,
                                                  String state, String area, boolean defaultAddress, String phoneCountryCode);

    public void doRemoveMemberAddress(Locale locale, String memberId, String addressId);

    public boolean checkIsDefaultAddress(String addressId);

    public void doUpdateDefaultMemberAddress(String memberId);

    public void doCreateCasting(Casting casting);

    public void doUpdateCasting(Casting casting);

    public void findCastForListing(DatagridModel<Casting> datagridModel, String whaleLiveId,Date createDateFrom, Date createDateTo);

    public void findCastForListingWithGuildId(DatagridModel<Casting> datagridModel, String guildId);

    public ProfileNameLog doCreateProfileNameLog(String memberId, String oldProfileName, String username);

    public void doSendDifferentNotificationEmailType(String signingType, String guildId, String ccAddress, String bccAddress, String title, String body);

    public void doGenerateAgoraUidForAllUser();
}
