package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberFans;

public interface MemberFansSqlDao {
    public static final String BEAN_NAME = "memberFansSqlDao";

    public void findMemberFansForDatagrid(DatagridModel<MemberFans> datagridModel, String memberId);
}
