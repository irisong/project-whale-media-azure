package com.compalsolutions.compal.member.dao;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;

public interface MemberDao extends BasicDao<Member, String> {
    public static final String BEAN_NAME = "memberDao";

    public void findMembersForDatagrid(DatagridModel<Member> datagridModel, String agentId, String memberCode, Date joinDateFrom, Date joinDateTo, String status,String whaleLiveId);

    public void findGuildMembersForDatagrid(DatagridModel<Member> datagridModel, String guildId);

    public void findMembersForKycDatagrid(DatagridModel<Member> datagridModel, String memberCode, String kycStatus,String whaleLiveId,Date dateFrom,Date dateTo);

    public int findTotalMembers();

    public Member findMemberByMemberCode(String memberCode);

    public Member findMemberByAgoraUid(int uid);

    public List<Member> findMemberWithoutAgoraUid();

    public Member findMemberByChannelName(String channelName);

    public Member findActiveMemberByMemberCode(String memberCode);

    public List<Object[]> findMemberStatisticsReport();

    public int findNoOfMembersByRank(String rankId);

    public List<Member> findAllMembers();

    public void updateTotalSales(String memberId, BigDecimal totalSales);

    public void updateVipStarRank(String memberId, int starVipRank);

    public Member findMemberByMemberDetId(String memberDetailId);

    public void doIncreaseExperience(String memberId, BigDecimal experience);

    public void doDecreaseExperience(String memberId, BigDecimal experience);

}
