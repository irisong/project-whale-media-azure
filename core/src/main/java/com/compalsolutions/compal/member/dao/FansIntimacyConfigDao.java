package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;

import java.util.List;

public interface FansIntimacyConfigDao extends BasicDao<FansIntimacyConfig, String> {
    public static final String BEAN_NAME = "fansIntimacyConfigDao";

    public FansIntimacyConfig getFansIntimacyConfigByLevel(int fanLevel);

    public int findTotalValueNeedForLevel(int level);

    public void findFansIntimacyForDatagrid(DatagridModel<FansIntimacyConfig> datagridModel);

    public List<FansIntimacyConfig> getAllFansIntimacyConfig();
}
