package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;

import javax.persistence.*;

@Entity
@Table(name = "app_search_history")
@Access(AccessType.FIELD)
public class SearchHistory extends VoBase {

    private static final long serialVersionUID = 1L;

    @Id
    @ToTrim
    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @Id
    @ToTrim
    @Column(name = "keyword", nullable = false, length = 20)
    private String keyword;

    public SearchHistory() {
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
}
