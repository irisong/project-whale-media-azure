package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "mb_member_address")
@Access(AccessType.FIELD)
public class MemberAddress extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_addr_id", unique = true, nullable = false, length = 32)
    private String memberAddrId;

    @Embedded
    private Address address;

    @ManyToOne
    @JoinColumn(name = "country_code", insertable = false, updatable = false, nullable = true)
    private Country country;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false, nullable = true)
    private Member member;

    @Column(name = "default_address")
    private Boolean defaultAddress;

    @Column(name = "phone_country_code", length = 10)
    private String phoneCountryCode;

    public MemberAddress() {
    }

    public MemberAddress(boolean defaultValue) {
        if (defaultValue) {
            defaultAddress = false;
        }
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberAddrId() {
        return memberAddrId;
    }

    public void setMemberAddrId(String memberAddrId) {
        this.memberAddrId = memberAddrId;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Boolean getDefaultAddress() {
        return defaultAddress;
    }

    public void setDefaultAddress(Boolean defaultAddress) {
        this.defaultAddress = defaultAddress;
    }

    public String getPhoneCountryCode() {
        return phoneCountryCode;
    }

    public void setPhoneCountryCode(String phoneCountryCode) {
        this.phoneCountryCode = phoneCountryCode;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((memberAddrId == null) ? 0 : memberAddrId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MemberAddress other = (MemberAddress) obj;
        if (memberAddrId == null) {
            if (other.memberAddrId != null)
                return false;
        } else if (!memberAddrId.equals(other.memberAddrId))
            return false;
        return true;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
