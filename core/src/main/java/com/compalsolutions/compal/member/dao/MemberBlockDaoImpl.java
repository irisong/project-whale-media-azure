package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberBlock;
import com.compalsolutions.compal.member.vo.MemberFans;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(MemberBlockDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberBlockDaoImpl extends Jpa2Dao<MemberBlock, String> implements MemberBlockDao {

    public MemberBlockDaoImpl() {
        super(new MemberBlock(false));
    }

    @Override
    public MemberBlock findMemberBlock(String memberId, String blockMemberId) {
        Validate.notBlank(memberId);
        Validate.notBlank(blockMemberId);

        MemberBlock memberBlock = new MemberBlock(false);
        memberBlock.setMemberId(memberId);
        memberBlock.setBlockMemberId(blockMemberId);

        return findFirst(memberBlock);
    }

    @Override
    public void findMemberBlockForDatagrid(DatagridModel<MemberBlock> datagridModel, String memberId) {
        List<Object> params = new ArrayList<Object>();
        String hql = "SELECT b FROM MemberBlock b WHERE b.memberId = ? ORDER BY b.datetimeAdd DESC ";
        params.add(memberId);

        findForDatagrid(datagridModel, "b", hql, params.toArray());
    }
}
