package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberChangeLog;

public interface MemberChangeLogDao extends BasicDao<MemberChangeLog, String>{
        public static final String BEAN_NAME = "memberChangeLogDao";

        public MemberChangeLog findMemberLogByMemberCode(String memberId);
}
