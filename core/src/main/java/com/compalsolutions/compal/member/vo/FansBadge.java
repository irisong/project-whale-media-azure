package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mb_fans_badge")
@Access(AccessType.FIELD)
public class FansBadge extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "fans_badge_id", unique = true, nullable = false, length = 32)
    private String fansBadgeId;

    @ToTrim
    @Column(name = "fans_badge_name", length = 200)
    private String fansBadgeName;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    //PENDING / ACTIVE / INACTIVE / REJECTED
    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", nullable = false)
    private Date createDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "active_datetime")
    private Date activeDatetime;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "reject_datetime")
    private Date rejectDatetime;

    @ToTrim
    @Column(name = "remark")
    private String remark;

    public FansBadge() {
    }

    public FansBadge(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.PENDING;
            createDate = new Date();
        }
    }

    public String getFansBadgeId() {
        return fansBadgeId;
    }

    public void setFansBadgeId(String fansBadgeId) {
        this.fansBadgeId = fansBadgeId;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getActiveDatetime() {
        return activeDatetime;
    }

    public void setActiveDatetime(Date activeDatetime) {
        this.activeDatetime = activeDatetime;
    }

    public Date getRejectDatetime() {
        return rejectDatetime;
    }

    public void setRejectDatetime(Date rejectDatetime) {
        this.rejectDatetime = rejectDatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
