package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.Date;
import java.util.List;

public interface MemberSqlDao {
    public static final String BEAN_NAME = "memberSqlDao";

    public List<String> findMemberNotRegisterNotificationAcc(String accessModule);

    public List<String> findAllRegisteredNotifAccMembers(boolean isCn);

    public String findSpecificAccMembers(boolean isCn, String memberId);

    public List<String> findRegisteredNotifAccMembersOfFollowers(String memberId);

    public void findSimilarMemberDetailsByProfileName(DatagridModel<MemberDetail> memberDetailsPaging, String keyword, String userMemberDetailId);

    public void findMembersForKycDatagrid(DatagridModel<Member> datagridModel, String memberCode, String kycStatus, String whaleLiveId, Date dateFrom, Date dateTo);
}
