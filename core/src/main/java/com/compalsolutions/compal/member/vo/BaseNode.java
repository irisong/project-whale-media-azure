package com.compalsolutions.compal.member.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;

@MappedSuperclass
@Access(AccessType.FIELD)
public class BaseNode extends VoBase {
    private static final long serialVersionUID = 1L;

    @Column(name = "b32", length = 32)
    protected String b32;

    @Column(name = "member_id", length = 32, nullable = false)
    protected String memberId;

    @Column(name = "unit", nullable = false)
    protected Integer unit;

    @Column(name = "tmpid", length = 32)
    protected String tempId;

    @Column(name = "tmpkey", length = 32)
    protected String tempKey;

    @Column(name = "tmpunit")
    protected Integer tempUnit;

    @Column(name = "tmppos", length = 1)
    protected String tempPosition;

    @Column(name = "tree_style")
    protected Integer treeStyle;

    @Column(name = "tree_type", length = 1)
    protected String treeType;

    @Column(name = "parent_id", length = 32)
    protected String parentId;

    @Column(name = "parent_unit")
    protected Integer parentUnit;

    @Column(name = "position", length = 1)
    protected String position;

    @Column(name = "level", nullable = false)
    protected Integer level;

    @Column(name = "tracekey", columnDefinition = "text")
    protected String traceKey;

    @Column(name = "refno", length = 32)
    protected String refno;

    @Column(name = "parse_tree", length = 1)
    protected String parseTree;

    @Column(name = "status", length = 20, nullable = false)
    protected String status;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    protected Member member;

    @ManyToOne
    @JoinColumn(name = "parent_id", insertable = false, updatable = false)
    protected Member parent;

    @Transient
    private StringBuffer stb;

    public BaseNode() {
    }

    public BaseNode(boolean defaultValue) {
        if (defaultValue) {
            unit = 0;
            tempId = TreeConstant.NO_PLACEMENT;
            tempKey = "";
            tempUnit = 0;
            treeStyle = TreeConstant.TREE_STYLE_POS_FIXED;
            treeType = TreeConstant.NODE_NORMAL;
            parentUnit = 0;
            level = 0;
            parseTree = TreeConstant.TREE_UNPARSE;
            status = Global.Status.PENDING;
        }
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Integer getUnit() {
        return unit;
    }

    public void setUnit(Integer unit) {
        this.unit = unit;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public String getTempKey() {
        return tempKey;
    }

    public void setTempKey(String tempKey) {
        this.tempKey = tempKey;
    }

    public Integer getTempUnit() {
        return tempUnit;
    }

    public void setTempUnit(Integer tempUnit) {
        this.tempUnit = tempUnit;
    }

    public String getTempPosition() {
        return tempPosition;
    }

    public void setTempPosition(String tempPosition) {
        this.tempPosition = tempPosition;
    }

    public Integer getTreeStyle() {
        return treeStyle;
    }

    public void setTreeStyle(Integer treeStyle) {
        this.treeStyle = treeStyle;
    }

    public String getTreeType() {
        return treeType;
    }

    public void setTreeType(String treeType) {
        this.treeType = treeType;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public Integer getParentUnit() {
        return parentUnit;
    }

    public void setParentUnit(Integer parentUnit) {
        this.parentUnit = parentUnit;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getTraceKey() {
        return traceKey;
    }

    public void setTraceKey(String traceKey) {
        this.traceKey = traceKey;
    }

    public String getRefno() {
        return refno;
    }

    public void setRefno(String refno) {
        this.refno = refno;
    }

    public String getParseTree() {
        return parseTree;
    }

    public void setParseTree(String parseTree) {
        this.parseTree = parseTree;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Member getParent() {
        return parent;
    }

    public void setParent(Member parent) {
        this.parent = parent;
    }

    public String getB32() {
        return b32;
    }

    public void setB32(String b32) {
        this.b32 = b32;
    }
}
