package com.compalsolutions.compal.member.service;


import com.compalsolutions.compal.member.dao.CastingDao;

import com.compalsolutions.compal.member.vo.Casting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(CastingService.BEAN_NAME)
public class CastingServiceImpl implements CastingService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private CastingDao castingDao;

    public Casting getCastingByWhaleliveId(String whaleLiveId)
    {
        return castingDao.getCastingByWhaleliveId(whaleLiveId);
    }

    public Casting getCasting(String Id)
    {
        return castingDao.get(Id);
    }

}
