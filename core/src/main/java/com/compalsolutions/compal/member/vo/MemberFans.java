package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "mb_member_fans")
@Access(AccessType.FIELD)
public class MemberFans extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String ROLE_SENIOR_ADMIN = "SENIOR_ADMIN";
    public static final String ROLE_ADMIN = "ADMIN";

    public static final Integer CONTINUE_WATCHING = 300;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "fans_id", unique = true, nullable = false, length = 32)
    private String fansId;

    @Column(name = "fans_member_id", nullable = false, length = 32)
    private String fansMemberId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @Column(name = "intimacy_value", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer intimacyValue;

    @Column(name = "fans_level")
    private Integer fansLevel;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "join_date")
    private Date joinDate;

    @Column(name = "is_admin")
    private Boolean isAdmin;

    @Column(name = "is_senior_admin")
    private Boolean isSeniorAdmin;

    @Column(name = "point_spend", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal pointSpend;

    @Column(name = "wearing_badge")
    private Boolean wearingBadge;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "last_intimacy_gain")
    private Date lastIntimacyGain;

    @Column(name = "today_intimacy_value", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer todayIntimacyValue;

    @Temporal(TemporalType.DATE)
    @Column(name = "first_entry_date")
    private Date firstEntryDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "cont_watching_date")
    private Date contWatchingDate;

    @Column(name = "today_point_spend", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal todayPointSpend;

    @Temporal(TemporalType.DATE)
    @Column(name = "last_point_spend")
    private Date lastPointSpend;

    @Temporal(TemporalType.DATE)
    @Column(name = "lack_intimacy_date")
    private Date lackIntimacyDate;

    @Column(name = "send_point_ind")
    private Boolean sendPointInd;

    @Transient
    private String profileName;

    @Transient
    private String whaleliveId;

    @Transient
    private Integer level;

    @Transient
    private String levelUrl;

    @Transient
    private String profilePicUrl;

    @Transient
    private String fansBadgeUrl;

    @Transient
    private String fansBadgeName;

    @Transient
    private String fansBadgeTextCode;

    @Transient
    private String role;

    @Transient
    private List<String> rolesOptions;

    public MemberFans() {
    }

    public MemberFans(boolean defaultValue) {
        if (defaultValue) {
            this.isAdmin = false;
            this.isSeniorAdmin = false;
            this.joinDate = new Date();
            this.intimacyValue = 0;
            this.todayIntimacyValue = 0;
            this.fansLevel = 1;
            this.pointSpend = new BigDecimal(0);
            this.todayPointSpend = new BigDecimal(0);
        }
    }

    public String getFansId() {
        return fansId;
    }

    public void setFansId(String fansId) {
        this.fansId = fansId;
    }

    public String getFansMemberId() {
        return fansMemberId;
    }

    public void setFansMemberId(String fansMemberId) {
        this.fansMemberId = fansMemberId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public Integer getFansLevel() {
        return fansLevel;
    }

    public void setFansLevel(Integer fansLevel) {
        this.fansLevel = fansLevel;
    }

    public Boolean getAdmin() {
        return isAdmin;
    }

    public void setAdmin(Boolean admin) {
        isAdmin = admin;
    }

    public Boolean getSeniorAdmin() {
        return isSeniorAdmin;
    }

    public void setSeniorAdmin(Boolean seniorAdmin) {
        isSeniorAdmin = seniorAdmin;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    public Integer getIntimacyValue() {
        return intimacyValue;
    }

    public void setIntimacyValue(Integer intimacyValue) {
        this.intimacyValue = intimacyValue;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getLevelUrl() {
        return levelUrl;
    }

    public void setLevelUrl(String levelUrl) {
        this.levelUrl = levelUrl;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getFansBadgeUrl() {
        return fansBadgeUrl;
    }

    public void setFansBadgeUrl(String fansBadgeUrl) {
        this.fansBadgeUrl = fansBadgeUrl;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }

    public void setFansBadgeTextCode(String fansBadgeTextCode) {
        this.fansBadgeTextCode = fansBadgeTextCode;
    }

    public String getFansBadgeTextCode() {
        return fansBadgeTextCode;
    }

    public BigDecimal getPointSpend() {
        return pointSpend;
    }

    public void setPointSpend(BigDecimal pointSpend) {
        this.pointSpend = pointSpend;
    }

    public Date getLastIntimacyGain() {
        return lastIntimacyGain;
    }

    public void setLastIntimacyGain(Date lastIntimacyGain) {
        this.lastIntimacyGain = lastIntimacyGain;
    }

    public Integer getTodayIntimacyValue() {
        return todayIntimacyValue;
    }

    public void setTodayIntimacyValue(Integer todayIntimacyValue) {
        this.todayIntimacyValue = todayIntimacyValue;
    }

    public static final String getFansBadgeTextCode(int level) {
        if (level <= 5) {
            return "#ffffff";
        } else if (level <= 10) {
            return "#ffffff";
        } else if (level <= 15) {
            return "#ec6bc2";
        } else if (level <= 20) {
            return "#edb045";
        } else {
            return "#edb045";
        }
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<String> getRolesOptions() {
        return rolesOptions;
    }

    public void setRolesOptions(List<String> rolesOptions) {
        this.rolesOptions = rolesOptions;
    }

    public Boolean getWearingBadge() {
        return wearingBadge;
    }

    public void setWearingBadge(Boolean wearingBadge) {
        this.wearingBadge = wearingBadge;
    }

    public Date getFirstEntryDate() {
        return firstEntryDate;
    }

    public void setFirstEntryDate(Date firstEntryDate) {
        this.firstEntryDate = firstEntryDate;
    }

    public Date getContWatchingDate() {
        return contWatchingDate;
    }

    public void setContWatchingDate(Date contWatchingDate) {
        this.contWatchingDate = contWatchingDate;
    }

    public BigDecimal getTodayPointSpend() {
        return todayPointSpend;
    }

    public void setTodayPointSpend(BigDecimal todayPointSpend) {
        this.todayPointSpend = todayPointSpend;
    }

    public Date getLastPointSpend() {
        return lastPointSpend;
    }

    public void setLastPointSpend(Date lastPointSpend) {
        this.lastPointSpend = lastPointSpend;
    }

    public Date getLackIntimacyDate() {
        return lackIntimacyDate;
    }

    public void setLackIntimacyDate(Date lackIntimacyDate) {
        this.lackIntimacyDate = lackIntimacyDate;
    }

    public Boolean getSendPointInd() {
        return sendPointInd;
    }

    public void setSendPointInd(Boolean sendPointInd) {
        this.sendPointInd = sendPointInd;
    }

    public static final String getFansRank(int level) {
        String rank = "r";
        if (level <= 5) {
            rank += "1";
        } else if (level <= 10) {
            rank += "2";
        } else if (level <= 15) {
            rank += "3";
        } else if (level <= 20) {
            rank += "4";
        } else {
            rank += "5";
        }
        return rank;
    }
}
