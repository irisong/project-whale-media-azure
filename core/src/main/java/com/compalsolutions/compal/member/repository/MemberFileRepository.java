package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.MemberFile;

public interface MemberFileRepository extends JpaRepository<MemberFile, String> {
    public static final String BEAN_NAME = "memberFileRepository";
}
