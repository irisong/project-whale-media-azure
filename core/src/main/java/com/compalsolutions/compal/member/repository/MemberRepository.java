package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.Member;

public interface MemberRepository extends JpaRepository<Member, String> {
    public static final String BEAN_NAME = "memberRepository";
}
