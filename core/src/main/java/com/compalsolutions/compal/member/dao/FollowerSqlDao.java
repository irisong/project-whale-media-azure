package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Follower;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.List;

public interface FollowerSqlDao {
    public static final String BEAN_NAME = "followerSqlDao";

    public void getFollowingListbyPaging(DatagridModel<MemberDetail> datagridModel, String userMemberId);

    public void getFollowerListbyPaging(DatagridModel<MemberDetail> datagridModel, String userMemberId);

    public List<Follower> getFollowingCount();

}
