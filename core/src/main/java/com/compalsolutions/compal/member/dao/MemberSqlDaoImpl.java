package com.compalsolutions.compal.member.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;

@Component(MemberSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberSqlDaoImpl extends AbstractJdbcDao implements MemberSqlDao {

    @Override
    public List<String> findMemberNotRegisterNotificationAcc(String accessModule) {
        List<Object> params = new ArrayList<Object>();

        String sql = "SELECT member.member_id\n" +
                "FROM mb_member member\n" +
                "LEFT JOIN app_notif_account acc ON acc.member_id = member.member_id AND acc.access_module = ? \n" +
                "WHERE member.status = ? AND (acc.member_id is null or acc.access_module is null)";
        params.add(accessModule);
        params.add(Global.Status.ACTIVE);

        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("member_id");
            }
        }, params.toArray());
    }

    @Override
    public List<String> findAllRegisteredNotifAccMembers(boolean isCn) {
        List<Object> params = new ArrayList<>();
        String criteria;
        if (isCn) {
            criteria = "= 'zh' ";
        } else {
            criteria = "!= 'zh' ";
        }

        String sql = "SELECT nt.acc_id\n" +
                "FROM app_notif_account nt\n" +
                "JOIN mb_member mb ON nt.member_id = mb.member_id " +
                "WHERE mb.prefer_language " + criteria;

        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("acc_id");
            }
        }, params.toArray());
    }

    @Override
    public String findSpecificAccMembers(boolean isCn, String memberId) {
        List<Object> params = new ArrayList<>();
        String criteria;
        if (isCn) {
            criteria = "= 'zh' ";
        } else {
            criteria = "!= 'zh' ";
        }

        String sql = "SELECT nt.acc_id\n" +
                "FROM app_notif_account nt\n" +
                "JOIN mb_member mb ON nt.member_id = mb.member_id " +
                "WHERE mb.member_id = ? AND mb.prefer_language " + criteria;

        params.add(memberId);
        List<String> results = query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("acc_id");
            }
        }, params.toArray());


        if (CollectionUtil.isEmpty(results)) {
            return null;
        }
        return results.get(0);

    }

    @Override
    public List<String> findRegisteredNotifAccMembersOfFollowers(String memberId) {
        List<Object> params = new ArrayList<>();
        String sql = "SELECT nt.acc_id " +
                "FROM app_notif_account nt " +
                "JOIN app_live_stream_follower fl ON fl.follower_id = nt.member_id " +
                "WHERE fl.member_id = ? ";
        params.add(memberId);

        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("acc_id");
            }
        }, params.toArray());
    }

    @Override
    public void findSimilarMemberDetailsByProfileName(DatagridModel<MemberDetail> memberDetailsPaging, String keyword, String userMemberDetailId) {
        List<Object> params = new ArrayList<>();

        String sql = "SELECT d.* FROM mb_member m \n"
                + "INNER JOIN mb_member_detail d ON d.member_det_id = m.member_det_id \n"
                + "WHERE d.member_det_id <> ? \n"
                + "AND NOT EXISTS ( \n"
                + "     SELECT b.member_id FROM mb_member_block b \n"
                + "     WHERE b.member_id = m.member_id \n"
                + "     AND b.block_member_id = (SELECT member_id FROM mb_member WHERE member_det_id = ?)"
                + ") ";

        params.add(userMemberDetailId);
        params.add(userMemberDetailId);

        if (StringUtils.isNotBlank(keyword)) {
            sql += " AND (UPPER(d.profile_name) like ? OR UPPER(d.whalelive_id) like ? ) ";
            params.add("%" + keyword.toUpperCase() + "%");
            params.add("%" + keyword.toUpperCase() + "%");
        }

        sql += "ORDER BY d.profile_name ";

        queryForDatagrid(memberDetailsPaging, sql, new RowMapper<MemberDetail>() {
            public MemberDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
                MemberDetail obj = new MemberDetail();
                obj.setMemberDetId(rs.getString("member_det_id"));
                obj.setProfileName(rs.getString("profile_name"));
                obj.setWhaleliveId(rs.getString("whalelive_id"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void findMembersForKycDatagrid(DatagridModel<Member> datagridModel, String memberCode, String kycStatus, String whaleLiveId, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<>();

        String sql = "select m.member_id,b.whalelive_id,m.member_code,m.full_name,m.identity_no,m.kyc_status,m.kyc_remark,f.datetime_add as kycSubmitDate "+
                "FROM mb_member m " +
                "join mb_member_detail b on m.member_det_id = b.member_det_id " ;

        if (StringUtils.isNotBlank(memberCode)) {
            sql += " and m.member_code like ? ";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(kycStatus)) {
            sql += " and m.kyc_status like ? ";
            params.add(kycStatus + "%");
        }

        if (StringUtils.isNotBlank(whaleLiveId)) {
            sql += " and b.whalelive_id like ? ";
            params.add(whaleLiveId + "%");
        }

        sql +=  " left join mb_member_file f on m.member_id = f.member_id and f.type='IC_FRONT' ";
        sql += "Where 1 = 1 ";
        if (dateFrom != null) {
            sql += " and f.datetime_add >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            sql += " and f.datetime_add <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        queryForDatagrid(datagridModel, sql, new RowMapper<Member>() {
            public Member mapRow(ResultSet rs, int rowNum) throws SQLException {
                Member obj = new Member();
                MemberDetail obj2 = new MemberDetail();
                obj.setMemberId(rs.getString("member_id"));
                obj2.setWhaleliveId(rs.getString("whalelive_id"));
                obj.setMemberDetail(obj2);
                obj.setMemberCode(rs.getString("member_code"));
                obj.setFullName(rs.getString("full_name"));
                obj.setIdentityNo(rs.getString("identity_no"));
                obj.setKycStatus(rs.getString("kyc_status"));
                obj.setKycRemark(rs.getString("kyc_remark"));
                obj.setKycSubmitDate(rs.getDate("kycSubmitDate"));
                return obj;
            }
        }, params.toArray());
    }
}
