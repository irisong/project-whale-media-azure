package com.compalsolutions.compal.member.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.member.dao.MemberSqlDao;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.SponsorTreeDao;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.SponsorTree;
import com.compalsolutions.compal.member.vo.TreeConstant;

@Component(SponsorService.BEAN_NAME)
public class SponsorServiceImpl implements SponsorService {
    private SponsorTreeDao sponsorTreeDao;
    private MemberSqlDao memberSqlDao;

    @Autowired
    public SponsorServiceImpl(SponsorTreeDao sponsorTreeDao, MemberSqlDao memberSqlDao) {
        this.sponsorTreeDao = sponsorTreeDao;
        this.memberSqlDao = memberSqlDao;
    }

    @Override
    public SponsorTree findSponsorTreeByMemberId(String memberId) {
        if (StringUtils.isBlank(memberId))
            return null;

        return sponsorTreeDao.findSponsorTreeByMemberId(memberId);
    }

    @Override
    public void saveSponsorTree(SponsorTree sponsorTree) {
        sponsorTreeDao.save(sponsorTree);
    }

    @Override
    public void doParseSponsorTree(SponsorTree sponsorTree) {
        String b32 = Long.toString(sponsorTree.getId(), 32); // get based 32 number
        sponsorTree.setB32(b32);

        SponsorTree sponsorTreeUpline = findSponsorTreeByMemberId(sponsorTree.getParentId());
        sponsorTree.setLevel(sponsorTreeUpline.getLevel() + 1);
        sponsorTree.setTraceKey(sponsorTreeUpline.getTraceKey() + "/" + b32);
        sponsorTree.setParseTree(TreeConstant.TREE_PARSED);
        sponsorTree.setLeaderMemberId(sponsorTreeUpline.getLeaderMemberId());
        sponsorTreeDao.update(sponsorTree);
    }

    @Override
    public void doChangeSponsorship(Locale locale, String memberId, String newSponsorCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);

        /************************
         * VERIFICATION - START
         ************************/

        if (Global.COMPANY_MEMBER.equalsIgnoreCase(memberId)) {
            throw new ValidatorException("This member can't change Sponsor ID");
        }

        SponsorTree sponsorTree = findSponsorTreeByMemberId(memberId);

        if (sponsorTree == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        Member member = sponsorTree.getMember();
        Member oldSponsorMember = sponsorTree.getParent();
        Member newSponsorMember = memberService.findMemberByMemberCode(newSponsorCode);

        if (newSponsorMember == null) {
            throw new ValidatorException(i18n.getText("invalidNewSponsorId", locale));
        }

        SponsorTree newSponsorSponsorTree = findSponsorTreeByMemberId(newSponsorMember.getMemberId());
        if (newSponsorSponsorTree == null) {
            throw new ValidatorException(i18n.getText("invalidNewSponsorId", locale));
        }

        // check if new sponsor is within member's sponsor tree or not.
        if (newSponsorSponsorTree.getTraceKey().startsWith(sponsorTree.getTraceKey())) {
            throw new ValidatorException(i18n.getText("youCanNotChangeSponsorToXBecauseXUnderYSponsorTree", locale, newSponsorCode, member.getMemberCode()));
        }

        if (oldSponsorMember.getMemberId().equalsIgnoreCase(newSponsorMember.getMemberId())) {
            throw new ValidatorException(i18n.getText("newSponsorIdCanNotSameWithCurrentSponsorId", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        int adjustLevel = sponsorTree.getLevel() - 1 - newSponsorSponsorTree.getLevel();
        String oldTracekey = sponsorTree.getTraceKey();
        String newTracekey = newSponsorSponsorTree.getTraceKey() + "/" + sponsorTree.getB32();

        sponsorTreeDao.doUpdateSponsorForMember(sponsorTree.getId(), oldTracekey, newTracekey, adjustLevel, newSponsorMember.getMemberId());
        sponsorTreeDao.doUpdateSponsorForMemberDownline(sponsorTree.getId(), oldTracekey, newTracekey, adjustLevel, sponsorTree.getB32());
    }

    @Override
    public boolean isSameGroup(String uplineMemberId, String downlineMemberId) {
        if (uplineMemberId.equals(downlineMemberId))
            return true;

        SponsorTree uplineTree = findSponsorTreeByMemberId(uplineMemberId);
        SponsorTree downlineTree = findSponsorTreeByMemberId(downlineMemberId);

        if (uplineTree == null || downlineTree == null)
            return false;

        if (downlineTree.getTraceKey().startsWith(uplineTree.getTraceKey()))
            return true;

        return false;
    }

    @Override
    public int getNoOfDirectDownlines(String memberId) {
        return sponsorTreeDao.getNoOfDirectDownlines(memberId);
    }

    @Override
    public int getNoOfDownlines(String memberId) {
        SponsorTree sponsorTree = sponsorTreeDao.findSponsorTreeByMemberId(memberId);

        return getNoOfDownlines(memberId, sponsorTree.getTraceKey());
    }

    private int getNoOfDownlines(String memberId, String traceKey) {
        return sponsorTreeDao.getNoOfDownlines(memberId, traceKey);
    }

    @Override
    public void updateSponsorTree(SponsorTree sponsorTree) {
        sponsorTreeDao.update(sponsorTree);
    }

    @Override
    public List<SponsorTree> findDirectDownlinesForSponsorTreeAction(String memberId, int startIndex, int size) {
        List<SponsorTree> sponsorTrees = sponsorTreeDao.findDirectDownlines(memberId, startIndex, size);

        for (SponsorTree sponsorTree : sponsorTrees) {
            sponsorTree.setTotalDownlines(getNoOfDownlines(sponsorTree.getMemberId()));
        }
        return sponsorTrees;
    }

    @Override
    public SponsorTree findSponsorTreeWithNoOfDownlines(String memberId) {
        SponsorTree sponsorTree = sponsorTreeDao.findSponsorTreeByMemberId(memberId);
        sponsorTree.setTotalDownlines(getNoOfDownlines(memberId, sponsorTree.getTraceKey()));

        return sponsorTree;
    }

    @Override
    public void findDirectDownlineMembersForListing(DatagridModel<Member> datagridModel, String memberId) {
        sponsorTreeDao.findDirectDownlineMembersForDatagrid(datagridModel, memberId);

        List<Member> records = datagridModel.getRecords();
    }

    @Override
    public Member findFirstDirectDownline(String uplineId) {
        SponsorTree sponsorTree = sponsorTreeDao.findFirstDirectDownline(uplineId);

        if (sponsorTree != null)
            return sponsorTree.getMember();

        return null;
    }

    @Override
    public int getNoOfVipDownlines(String memberId) {
        SponsorTree sponsorTree = sponsorTreeDao.findSponsorTreeByMemberId(memberId);
        return sponsorTreeDao.getNoOfVipDownliens(memberId, sponsorTree.getTraceKey());
    }
}
