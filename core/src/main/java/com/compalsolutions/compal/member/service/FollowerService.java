package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Follower;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.List;
import java.util.Locale;

public interface FollowerService {
    public static final String BEAN_NAME = "followerService";

    public void doProcessFollow(Locale locale, String action, String userMemberId, String followMemberId);

    public void doProcessFollowingNotification(String userMemberId, Member followMember);

    public void doCreateFollowing(String userMemberId, String followMemberId);

    public void doRemoveFollowing(String userMemberId, String followMemberId);

    public int getFollowerCountById(String userMemberId);

    public int getFollowingCountById(String userMemberId);

    public Follower getFollower(String userMemberId, String followMemberId);

    public List<Follower> getFollowerByMemberId(String userMemberId);

    public List<Follower> getFollowingByMemberId(String userMemberId);

    public void getFollowingList(DatagridModel<MemberDetail> datagridModel, String userMemberId);

    public void getFollowerList(DatagridModel<MemberDetail> datagridModel, String userMemberId);

//    public void doResetExperienceForExp();
}
