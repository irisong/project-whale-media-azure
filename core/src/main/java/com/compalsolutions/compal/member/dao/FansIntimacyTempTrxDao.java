package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.FansIntimacyTempTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface FansIntimacyTempTrxDao extends BasicDao<FansIntimacyTempTrx, String> {
    public static final String BEAN_NAME = "fansIntimacyTempTrxDao";

    public List<FansIntimacyTempTrx> getAllTemporaryRecord();

    public List<FansIntimacyTempTrx> getRecordByFansId(String fansMemberId, String memberId);
}
