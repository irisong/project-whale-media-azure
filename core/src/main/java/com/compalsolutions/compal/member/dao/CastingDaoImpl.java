package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.repository.CastingRespository;
import com.compalsolutions.compal.member.vo.Casting;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(CastingDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CastingDaoImpl extends Jpa2Dao<Casting, String> implements CastingDao {

    @SuppressWarnings("unused")
    @Autowired
    private CastingRespository castingRespository;

    public CastingDaoImpl() {
        super(new Casting(false));
    }

    public void findCastForDatagrid(DatagridModel<Casting> datagridModel, String whaleLiveId, Date createDateFrom, Date createDateTo){
        List<Object> params = new ArrayList<Object>();

        String hql = "select c FROM Casting c where 1=1";

        if (StringUtils.isNotBlank(whaleLiveId)) {
            hql += " and c.whaleLiveId = ? ";
            params.add(whaleLiveId);
        }
        if (createDateFrom!=null) {
            hql += " AND c.datetimeAdd >= ? ";
            params.add(createDateFrom);
        }
        if (createDateTo!=null) {
            hql += " AND c.datetimeAdd <= ? ";
            params.add(createDateTo);
        }
        findForDatagrid(datagridModel, "c", hql, params.toArray());
    }

    public void findCastForListingWithGuildId(DatagridModel<Casting> datagridModel, String guildId){
        List<Object> params = new ArrayList<Object>();

        String hql = "select c FROM Casting c where 1=1";

        if (StringUtils.isNotBlank(guildId)) {
            hql += " and c.guildId = ? ";
            params.add(guildId);
        }
        findForDatagrid(datagridModel, "c", hql, params.toArray());
    }

    @Override
    public Casting getCastingByWhaleliveId(String whaleLiveId) {
        Validate.notBlank(whaleLiveId);

        Casting casting = new Casting(false);
        casting.setWhaleLiveId(whaleLiveId);


        return findFirst(casting, "datetimeAdd desc");
    }
}
