package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.MemberMnemonic;

public interface MemberMnemonicDao extends BasicDao<MemberMnemonic, String> {
    public static final String BEAN_NAME = "memberMnemonicDao";

    public MemberMnemonic findActiveMemberMnemonicByMemberId(String memberId);
}
