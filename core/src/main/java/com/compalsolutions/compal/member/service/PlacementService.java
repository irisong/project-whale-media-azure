package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.member.vo.PlacementTree;

public interface PlacementService {
    public static final String BEAN_NAME = "placementService";

    public PlacementTree findPlacementTree(String memberId, int unit);

    public void savePlacementTree(PlacementTree placementTree);

    public void doParsePlacementTree(PlacementTree placementTree);

    public boolean isSponsorIdAndPlacementIdSameGroup(String sponsorMembeId, String placementMemberId);

    public PlacementTree findPlacementTreeByParentIdAndParentUnitAndParentPosition(String parentId, int parentUnit, String parentPosition);
}
