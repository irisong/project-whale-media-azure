package com.compalsolutions.compal.member.dao;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.MemberAddressRepository;
import com.compalsolutions.compal.member.vo.MemberAddress;

@Component(MemberAddressDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberAddressDaoImpl extends Jpa2Dao<MemberAddress, String> implements MemberAddressDao {

    @SuppressWarnings("unused")
    @Autowired
    private MemberAddressRepository memberAddressRepository;

    public MemberAddressDaoImpl() {
        super(new MemberAddress(false));
    }

    @Override
    public List<MemberAddress> findAllAddressByMemberId(String memberId) {
        MemberAddress example = new MemberAddress(false);
        example.setMemberId(memberId);
        return findByExample(example, "datetimeUpdate");
    }

    //Remark: this method only return unique record, please pass at least addressId / defaultAddress to avoid result more than 1 line
    @Override
    public MemberAddress findMemberAddress(String memberId, String addressId, boolean defaultAddress) {
        MemberAddress memberAddress = new MemberAddress();

        if (StringUtils.isNotBlank(memberId)) {
            memberAddress.setMemberId(memberId);
        }

        if (StringUtils.isNotBlank(addressId)) {
            memberAddress.setMemberAddrId(addressId);
        }

        if (defaultAddress) { //only filter when value is true
            memberAddress.setDefaultAddress(true);
        }

        return findUnique(memberAddress);
    }
}
