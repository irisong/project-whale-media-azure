package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name="app_casting")
@Access(AccessType.FIELD)
public class Casting extends VoBase {
    private static final long serialVersionUID=1L;
    public Casting() {
    }

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name="system-uuid",strategy = "uuid")
    @Column(name="cast_id",nullable = false,unique = true,length = 32)
    private String castId;

    @ToTrim
    @Column(name = "whalelive_id", length = 8, nullable = false)
    private String whaleLiveId;

    @Column(name = "experience")
    private Boolean experience;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 200)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_no", length = 100)
    private String identityNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_no", length = 50, nullable = false)
    private String phoneNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "phone_model", length = 50)
    private String phoneModel;

    @ToUpperCase
    @ToTrim
    @Column(name = "gender", length = 5, nullable = false)
    private String gender;

    @ToTrim
    @Column(name = "email", length = 100)
    private String email;

    @ToTrim
    @Column(name = "age", length = 3)
    private String age;

    @ToUpperCase
    @ToTrim
    @Column(name = "talent", length = 100)
    private String talent;


    @Column(name="url",columnDefinition= Global.ColumnDef.MEDIUM_TEXT)
    private String url;

    @ToUpperCase
    @ToTrim
    @Column(name = "tiktok_id", length = 100)
    private String tiktokId;

    @Column(name = "tiktok_fans")
    private Integer tiktokFan;

    @ToUpperCase
    @ToTrim
    @Column(name = "fb_id", length = 100)
    private String fbId;

    @ToUpperCase
    @ToTrim
    @Column(name = "ig_id", length = 100)
    private String igId;

    @Column(name = "ig_fans")
    private Integer igFan;

    @ToUpperCase
    @ToTrim
    @Column(name = "weibo_id", length = 100)
    private String weiboId;

    @Column(name = "weibo_fans")
    private Integer weiboFan;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 35, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "location", length = 100)
    private String location;

    @ToTrim
    @Column(name = "guild_id", length = 100)
    private String guildId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "test_live_date")
    private Date testLiveDate;

    @Transient
    private String guildName;

    @Transient
    private String whaleliveProfileName;


    public Casting(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getCastId() {
        return castId;
    }

    public void setCastId(String castId) {
        this.castId = castId;
    }

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTiktokId() {
        return tiktokId;
    }

    public void setTiktokId(String tiktokId) {
        this.tiktokId = tiktokId;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getIgId() {
        return igId;
    }

    public void setIgId(String igId) {
        this.igId = igId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public Integer getTiktokFan() {
        return tiktokFan;
    }

    public void setTiktokFan(Integer tiktokFan) {
        this.tiktokFan = tiktokFan;
    }

    public Integer getIgFan() {
        return igFan;
    }

    public void setIgFan(Integer igFan) {
        this.igFan = igFan;
    }

    public String getWeiboId() {
        return weiboId;
    }

    public void setWeiboId(String weiboId) {
        this.weiboId = weiboId;
    }

    public Integer getWeiboFan() {
        return weiboFan;
    }

    public void setWeiboFan(Integer weiboFan) {
        this.weiboFan = weiboFan;
    }

    public Date getTestLiveDate() {
        return testLiveDate;
    }

    public void setTestLiveDate(Date testLiveDate) {
        this.testLiveDate = testLiveDate;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getWhaleliveProfileName() {
        return whaleliveProfileName;
    }

    public void setWhaleliveProfileName(String whaleliveProfileName) {
        this.whaleliveProfileName = whaleliveProfileName;
    }

    public Boolean getExperience() {
        return experience;
    }

    public void setExperience(Boolean experience) {
        this.experience = experience;
    }
}
