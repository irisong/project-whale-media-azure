package com.compalsolutions.compal.member.dao;

import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.MemberDetailRepository;
import com.compalsolutions.compal.member.vo.MemberDetail;

@Component(MemberDetailDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberDetailDaoImpl extends Jpa2Dao<MemberDetail, String> implements MemberDetailDao {

    @SuppressWarnings("unused")
    @Autowired
    private MemberDetailRepository memberDetailRepository;

    public MemberDetailDaoImpl() {
        super(new MemberDetail(false));
    }

    @Override
    public MemberDetail findMemberDetailsByWhaleliveId(String whaleliveId) {
        Validate.notBlank(whaleliveId);

        MemberDetail example = new MemberDetail(false);
        example.setWhaleliveId(whaleliveId);

        return findUnique(example);
    }

    @Override
    public MemberDetail findMemberDetailsByContactNo(String contactNo) {
        Validate.notBlank(contactNo);

        MemberDetail example = new MemberDetail(false);
        example.setContactNo(contactNo);

        return findUnique(example);
    }
}
