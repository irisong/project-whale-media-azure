package com.compalsolutions.compal.member.vo;

import javax.persistence.*;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;

@Entity
@Table(name = "mb_member_file")
@Access(AccessType.FIELD)
public class MemberFile extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String UPLOAD_TYPE_IC = "IC";
    public static final String UPLOAD_TYPE_BANK = "BANK";
    public static final String UPLOAD_TYPE_RESIDENCE = "RESIDENCE";
    public static final String UPLOAD_TYPE_PROFILE_PIC = "PROFILE_PIC";
    public static final String UPLOAD_TYPE_IC_FRONT = "IC_FRONT";
    public static final String UPLOAD_TYPE_IC_BACK = "IC_BACK";
    public static final String UPLOAD_TYPE_PASSPORT = "PASSPORT";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "file_id", unique = true, nullable = false, length = 32)
    private String fileId;

    @ToTrim
    @Column(name = "member_id", length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false, nullable = true)
    private Member member;

    @ToTrim
    @Column(name = "type", length = 50, nullable = false)
    private String type;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data", columnDefinition = Global.ColumnDef.MEDIUM_BLOB)
    private byte[] data;

    @Column(name = "file_size", nullable = false)
    private Long fileSize;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    public MemberFile() {
    }

    public MemberFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getRenamedFilename(){
        if(StringUtils.isBlank(renamedFilename)){
            if(StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(filename)){
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length-1];
                renamedFilename = fileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public void setFileUrlWithParentPath(String parentPath) {
        fileUrl = parentPath  + "/" + getRenamedFilename();
    }

    public String getFileUrl() {
        return fileUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        MemberFile that = (MemberFile) o;

        return !(fileId != null ? !fileId.equals(that.fileId) : that.fileId != null);

    }

    @Override
    public int hashCode() {
        return fileId != null ? fileId.hashCode() : 0;
    }
}
