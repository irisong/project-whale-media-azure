package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.MemberReportFile;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MemberReportFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberReportFileDaoImpl extends Jpa2Dao<MemberReportFile, String> implements MemberReportFileDao {
    public MemberReportFileDaoImpl() {
        super(new MemberReportFile(false));
    }
}
