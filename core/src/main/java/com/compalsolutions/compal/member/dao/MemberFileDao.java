package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.MemberFile;

import java.util.List;

public interface MemberFileDao extends BasicDao<MemberFile, String> {
    public static final String BEAN_NAME = "memberFileDao";

    public MemberFile findByMemberIdAndType(String memberId, String type);

    public List<MemberFile> findMemberFilesByMemberId(String memberId);

}
