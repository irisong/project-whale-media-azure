package com.compalsolutions.compal.member.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.*;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.cache.service.JwtTokenCacheService;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.function.email.dao.EmailqDao;
import com.compalsolutions.compal.general.client.AgoraClient;
import com.compalsolutions.compal.general.dao.CountryDescDao;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.general.vo.*;
import com.compalsolutions.compal.live.chatroom.service.LiveChatService;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.live.streaming.client.dto.Channel;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.dao.*;
import com.compalsolutions.compal.member.dto.ProfileDto;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.YunxinClient;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.client.dto.YunXinResponse;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.omnicplus.OmnicplusClient;
import com.compalsolutions.compal.omnicplus.dto.CredentialsDto;
import com.compalsolutions.compal.omnicplus.dto.OmnicplusDto;
import com.compalsolutions.compal.point.dao.PointTrxSqlDao;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.security.service.TokenService;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.telegram.BotClient;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletAdjust;
import com.microsoft.azure.storage.CloudStorageAccount;
import com.microsoft.azure.storage.OperationContext;
import com.microsoft.azure.storage.StorageException;
import com.microsoft.azure.storage.blob.*;
import freemarker.template.utility.StringUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.validator.EmailValidator;
import org.apache.tika.Tika;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.email.service.EmailService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.dao.UserDetailsDao;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.member.MemberMnemonic;
import com.compalsolutions.compal.sms.service.SmsService;
import com.compalsolutions.compal.sms.vo.SmsTag;
import com.compalsolutions.compal.user.dao.MemberUserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.MailTemplateFormater;

import io.github.novacrypto.bip39.MnemonicGenerator;
import io.github.novacrypto.bip39.Words;
import io.github.novacrypto.bip39.wordlists.English;

@Component(MemberService.BEAN_NAME)
public class MemberServiceImpl implements MemberService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private MemberChangeLogDao memberChangeLogDao;

    @Autowired
    private MemberSqlDao memberSqlDao;

    @Autowired
    private MemberDetailDao memberDetailDao;

    @Autowired
    private MemberFileDao memberFileDao;

    @Autowired
    private MemberUserDao memberUserDao;

    @Autowired
    private MemberAddressDao memberAddressDao;

    @Autowired
    private MemberMnemonicDao memberMnemonicDao;

    @Autowired
    private UserDetailsDao userDetailsDao;

    @Autowired
    private EmailqDao emailqDao;

    @Autowired
    private CountryDescDao countryDescDao;

    @Autowired
    private PointTrxSqlDao pointTrxSqlDao;

    @Autowired
    private CastingDao castingDao;

    @Autowired
    private ProfileNameLogDao profileNameLogDao;

    @Override
    public void findMembersForListing(DatagridModel<Member> datagridModel, String agentId, String memberCode, Date joinDateFrom,
                                      Date joinDateTo, String status, String whaleLiveId) {
        memberDao.findMembersForDatagrid(datagridModel, agentId, memberCode, joinDateFrom, joinDateTo, status, whaleLiveId);
    }

    @Override
    public void findGuildMembersForListing(DatagridModel<Member> datagridModel, String guildId) {
        memberDao.findGuildMembersForDatagrid(datagridModel, guildId);
    }

    @Override
    public void findMembersForKYCListing(DatagridModel<Member> datagridModel, String memberCode, String kycStatus, String whaleLiveId, Date dateFrom, Date dateTo) {
        memberSqlDao.findMembersForKycDatagrid(datagridModel, memberCode, kycStatus, whaleLiveId, dateFrom, dateTo);
        MemberFileUploadConfiguration config = Application.lookupBean(MemberFileUploadConfiguration.BEAN_NAME, MemberFileUploadConfiguration.class);
        Environment env = Application.lookupBean(Environment.class);
        List<Member> memberList = datagridModel.getRecords();
        for (Member member : memberList) {
            MemberFile memberFrontIc = memberFileDao.findByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_IC_FRONT);
            MemberFile memberBackIc = memberFileDao.findByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_IC_BACK);
            if (memberFrontIc != null) {
                memberFrontIc.setFileUrlWithParentPath(env.getProperty("fileServerUrl") + "/file/memberFile");
                member.setIcFrontUrl(memberFrontIc.getFileUrl());

            }
            if (memberBackIc != null) {
                memberBackIc.setFileUrlWithParentPath(env.getProperty("fileServerUrl") + "/file/memberFile");
                member.setIcBackUrl(memberBackIc.getFileUrl());

            }

        }


    }

    @Override
    public void doCreateMember(Locale locale, Member member, String password, String agentId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);

        Agent agent = agentService.getAgent(agentId);
        if (agent == null) {
            throw new ValidatorException(i18n.getText("invalidAgent", locale));
        }

        if (memberDao.findMemberByMemberCode(member.getMemberCode()) != null) {
            throw new ValidatorException(i18n.getText("memberCodeExist", locale, member.getMemberCode()));
        }

        // save Member
        // member.setMemberName(member.getFirstName() + " " + member.getLastName());
        // member.setDefaultCurrencyCode(agent.getDefaultCurrencyCode());
        // member.setStatus(Global.STATUS_APPROVED_ACTIVE);
        // member.setRegisterDate(new Date());
        // member.setActiveDate(new Date());
        memberDao.save(member);

        // save Member User
        AgentUser agentUser = agentService.findSuperAgentUserByAgentId(agentId);
        MemberUser memberUser = new MemberUser(true);
        memberUser.setCompId(agentUser.getCompId());
        memberUser.setMemberId(member.getMemberId());
        memberUser.setUsername(member.getMemberCode());
        memberUser.setPassword(password);
        UserRole memberRole = userDetailsService.findUserRoleByRoleName(memberUser.getCompId(), Global.UserRoleGroup.MEMBER_GROUP);
        if (memberRole == null)
            userDetailsService.saveUser(memberUser, null);
        else
            userDetailsService.saveUser(memberUser, Arrays.asList(memberRole));
    }

    @Override
    public Member getMember(String memberId) {
        return memberDao.get(memberId);
    }

    @Override
    public Pair<Integer, String> doGetChannelTokenInfo(String memberId, String channelName) {
        int uid = 0;
        String token = null;

        synchronized (synchronizedObject) {
            Member member = getMember(memberId);
            if (member != null) {
                uid = doGetMemberAgoraUid(member);

                try {
                    AgoraClient agoraClient = new AgoraClient();
                    token = agoraClient.getRtcToken(channelName, uid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return new ImmutablePair<>(uid, token);
        }
    }

    @Override
    public int doGetMemberAgoraUid(Member member) {
        Integer uid = member.getAgoraUid();
        if (uid == null) {
            System.out.println("update agora uid memberId = " + member.getMemberId());
            uid = generateAgoraUid();
            member.setAgoraUid(uid);
            updateMember(member);
        }

        return uid;
    }

    @Override
    public int generateAgoraUid() {
        int uid;
        boolean duplicate;
        do {
            uid = Global.generateRandomInteger(100000000, 999999999);
            duplicate = checkAgoraUidExist(uid);
        } while (duplicate);

        return uid;
    }

    @Override
    public boolean checkAgoraUidExist(int uid) {
        Member member = memberDao.findMemberByAgoraUid(uid);
        return (member != null);
    }

    @Override
    public Member findMemberByAgoraUid(int uid) {
        return memberDao.findMemberByAgoraUid(uid);
    }

    @Override
    public String doGetMemberChannelName(Member member) {
        String channelName = member.getChannelName();
        if (StringUtils.isBlank(channelName)) {
            boolean duplicate;
            do {
                channelName = StringUtils.replace(UUID.randomUUID().toString(), "-", "");
                duplicate = checkChannelNameExist(channelName);
            } while (duplicate);

            member.setChannelName(channelName);
            updateMember(member);
        }

        return channelName;
    }

    @Override
    public boolean checkChannelNameExist(String channelName) {
        Member member = memberDao.findMemberByChannelName(channelName);
        return (member != null);
    }

    @Override
    public void updateMember(Member member) {
        memberDao.update(member);
    }

    @Override
    public Member findMemberByMemberCode(String memberCode) {
        return memberDao.findMemberByMemberCode(memberCode);
    }

    @Override
    public Member findMemberByMemberCodeOrWhaleliveId(String code) {
        Member member = findMemberByMemberCode(code);
        MemberDetail memberDetail;
        if (member != null) {
            return member;
        } else {
            memberDetail = memberDetailDao.findMemberDetailsByWhaleliveId(code);
            if (memberDetail != null) {
                member = memberDao.findMemberByMemberDetId(memberDetail.getMemberDetId());
                return member;
            } else {
                return null;
            }
        }
    }

    @Override
    public void doSendWelcomeLetter(Locale locale, Member member, String password) {
        MailTemplateFormater mailTemplateFormater = new MailTemplateFormater("simple/welcome_letter.html");
        String content = mailTemplateFormater.format(getMemberName(member), member.getMemberCode(), WebUtil.getSystemConfig().getApplicationName(), password);

        setEmailq(member.getMemberDetail().getEmail(), "welcomeLetter", content);
    }

    @Override
    public MemberUser findMemberUserByMemberId(String memberId) {
        return memberUserDao.findMemberUserByMemberId(memberId);
    }

    @Override
    public void doProcessMemberLevel(String memberId, BigDecimal exp) {
        memberDao.doIncreaseExperience(memberId, exp);
    }

    @Override
    public int getTotalMembers() {
        return memberDao.findTotalMembers();
    }

    @Override
    public boolean isMemberSecondPasswordValid(Locale locale, String memberId, String secondPassword) {
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        MemberUser memberUser = findMemberUserByMemberId(memberId);

        return userDetailsService.isEncryptedPasswordMatch(secondPassword, memberUser.getPassword2());
    }

    @Override
    public void doUpdateMember(String memberId, String fullName, String identity_no) {
        Member member = getMember(memberId);
        if (!StringUtils.isBlank(fullName)) {
            member.setFullName(fullName);
        }

        if (!StringUtils.isBlank(identity_no)) {
            member.setIdentityNo(identity_no);
        }

        memberDao.update(member);
    }

    @Override
    public void doUpdateMemberProfile(String memberId, String currency) {
        Member member = getMember(memberId);
        MemberDetail memberDetail;
        if (!StringUtils.isBlank(currency)) {
            memberDetail = member.getMemberDetail();
            memberDetail.setCurrencyCode(currency);
        } else {
            memberDetail = assignCurrencyBase(member.getMemberDetail(), member.getMemberCode());
        }
        memberDetailDao.update(memberDetail);
    }

    @Override
    public void doUpdateMemberSetting(Member member_new) {
        TokenService tokenService = Application.lookupBean((TokenService.class));
        JwtTokenCacheService jwtTokenCacheService = Application.lookupBean((JwtTokenCacheService.class));
        Member member = getMember(member_new.getMemberId());
        MemberUser memberUser = findMemberUserByMemberId(member_new.getMemberId());
        if (member != null) {
            if (member_new.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                JwtToken jwtToken = tokenService.findJwtTokenByUserId(memberUser.getUserId());
                tokenService.doCloseAllJwtTokenByDeviceAndUser(memberUser.getUserId());
                if (jwtToken != null) {
                    jwtTokenCacheService.evictJwtToken(jwtToken.getJwtId());
                }
            }

            if (member_new.getStatus().equalsIgnoreCase(Global.Status.INACTIVE) && StringUtils.isNotBlank(member_new.getRemark())) {
                member.setRemark(member_new.getRemark());
            } else if (member_new.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                member.setRemark(member.getRemark());
            } else {
                member.setRemark("");
            }

            MemberChangeLog memberChangeLog=memberChangeLogDao.findMemberLogByMemberCode(member.getMemberId());

            if(memberChangeLog==null) {
                MemberChangeLog newMemberChangeLog=new MemberChangeLog();
                newMemberChangeLog.setMemberId(member.getMemberId());
                newMemberChangeLog.setOldMemberCode(member.getMemberCode());
                newMemberChangeLog.setNewMemberCode(member_new.getMemberCode());
                memberChangeLogDao.save(newMemberChangeLog);
            }else{
                memberChangeLog.setOldMemberCode(member.getMemberCode());
                memberChangeLog.setNewMemberCode(member_new.getMemberCode());
                memberChangeLogDao.update(memberChangeLog);
            }

            member.setStatus(member_new.getStatus());
            member.setLiveStreamTypes(member_new.getLiveStreamTypes());
            member.setLiveStreamPrivate(member_new.getLiveStreamPrivate());
            member.setMemberCode(member_new.getMemberCode());
            memberDao.update(member);
        }
    }

    @Override
    public void doUpdateMemberNotificationSetting(Locale locale, String memberId, String type, boolean sendNotif) {
        I18n i18n = Application.lookupBean(I18n.class);
        Member member = getMember(memberId);

        if (member != null) {
            switch (type.toUpperCase()) {
                case Member.SEND_NOTIF_TYPE_COMMENT:
                    member.setSendNotifComment(sendNotif);
                    break;
                case Member.SEND_NOTIF_TYPE_LIKE:
                    member.setSendNotifLike(sendNotif);
                    break;
                case Member.SEND_NOTIF_TYPE_FOLLOWER:
                    member.setSendNotifFollower(sendNotif);
                    break;
                case Member.SEND_NOTIF_TYPE_FOLLOWING:
                    member.setSendNotifFollowing(sendNotif);
                    break;
                default:
                    throw new ValidatorException(i18n.getText("invalidType", locale));
            }

            memberDao.update(member);
        }
    }

    @Override
    public void doUpdateMemberKycStatus(String memberId, String KycStatus) {
        Member member = getMember(memberId);
        if (member != null) {
            MemberFile icFront = memberFileDao.findByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_IC_FRONT);
            MemberFile icBack = memberFileDao.findByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_IC_BACK);
            if (icFront != null && icBack != null) {
                member.setKycStatus(KycStatus);
                member.setKycSubmitDate(new Date());
            } else {
                throw new ValidatorException("KYC image cannot be blank");
            }
        }

        memberDao.update(member);
    }

    @Override
    public String doValidateMemberPassword(Locale locale, String memberId, String oldPassword, String newPassword) {
        I18n i18n = Application.lookupBean(I18n.class);

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        MemberUser memberUser = findMemberUserByMemberId(memberId);
        Member member = getMember(memberId);

        if (StringUtils.isBlank(oldPassword)) {
            throw new ValidatorException(i18n.getText("invalidOldPassword", locale));
        }

        if (oldPassword.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumOldPasswordLengthError", locale, 6));
        }

        if (StringUtils.isBlank(newPassword)) {
            throw new ValidatorException(i18n.getText("invalidNewPassword", locale));
        }

        if (newPassword.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumNewPasswordLengthError", locale, 6));
        }

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        OmnicplusClient omnicplusClient = new OmnicplusClient();
        CredentialsDto credentialsDto = new CredentialsDto(newPassword, member.getMemberCode(), oldPassword);

        OmnicplusDto omnicplusDto = omnicplusClient.checkUserPasswordInOmnicplus(credentialsDto);

        if (omnicplusDto.getUserType().equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_ERROR)) {
            throw new ValidatorException(omnicplusDto.getMessage());
        } else if (omnicplusDto.getUserType().equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_NOT_REGISTERED)) {
            if (!userDetailsService.isEncryptedPasswordMatch(oldPassword, memberUser.getPassword())) {
                throw new ValidatorException(i18n.getText("invalidOldPassword", locale));
            }
        }
        return omnicplusDto.getUserType();
    }

    @Override
    public void doChangeMemberPassword(Locale locale, String memberId, String oldPassword, String newPassword) {
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        MemberUser memberUser = findMemberUserByMemberId(memberId);

        String userType = doValidateMemberPassword(locale, memberId, oldPassword, newPassword);
        Member member = getMember(memberId);
        CredentialsDto credentialsDto = new CredentialsDto(newPassword, member.getMemberCode(), oldPassword);
        OmnicplusClient omnicplusClient = new OmnicplusClient();
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_VALID)) {
            OmnicplusDto omnicplusDto = omnicplusClient.updateOmnicplusPassword(credentialsDto);
        } else {
            userDetailsService.changePassword(locale, memberUser.getUserId(), oldPassword, newPassword);
        }
    }

    @Override
    public void doChangeMemberSecondPassword(Locale locale, String memberId, String oldPassword2, String newPassword2) {
        I18n i18n = Application.lookupBean(I18n.class);

        if (StringUtils.isBlank(oldPassword2)) {
            throw new ValidatorException(i18n.getText("invalidOldTransactionPassword", locale));
        }

        if (oldPassword2.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumOldTransactionPasswordLengthError", locale, 6));
        }

        if (StringUtils.isBlank(newPassword2)) {
            throw new ValidatorException(i18n.getText("invalidNewTransactionPassword", locale));
        }

        if (newPassword2.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumNewTransactionPasswordLengthError", locale, 6));
        }

        MemberUser memberUser = findMemberUserByMemberId(memberId);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        if (!userDetailsService.isEncryptedPasswordMatch(oldPassword2, memberUser.getPassword2())) {
            throw new ValidatorException(i18n.getText("incorrectOldTransactionPassword", locale));
        }

        userDetailsService.changeSecondPassword(locale, memberUser.getUserId(), oldPassword2, newPassword2);
    }

    @Override
    public void doResetPassword(Locale locale, String memberId, String password) {
        I18n i18n = Application.lookupBean(I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        Member member = getMember(memberId);

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        MemberUser memberUser = findMemberUserByMemberId(memberId);

        if (StringUtils.isBlank(password)) {
            throw new ValidatorException(i18n.getText("invalidNewPassword", locale));
        }

        if (password.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumNewPasswordLengthError", 6, locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        userDetailsService.doResetPassword(memberUser.getUserId(), password);
    }

    @Override
    public void doResetPassword2(Locale locale, String memberId, String password2) {
        I18n i18n = Application.lookupBean(I18n.class);

        /************************
         * VERIFICATION - START
         ************************/

        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        Member member = getMember(memberId);

        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        MemberUser memberUser = findMemberUserByMemberId(memberId);

        if (StringUtils.isBlank(password2)) {
            throw new ValidatorException(i18n.getText("invalidNewPassword", locale));
        }

        if (password2.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumNewPasswordLengthError", 6, locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        userDetailsService.doResetPassword2(memberUser.getUserId(), password2);
    }

    @Override
    public MemberFile findMemberFileByMemberIdAndType(String memberId, String type) {
        return memberFileDao.findByMemberIdAndType(memberId, type);
    }

    @Override
    public Member findActiveMemberByMemberCode(String memberCode) {
        return memberDao.findActiveMemberByMemberCode(memberCode);
    }

    @Override
    public int findNoOfMembersByRank(String rankId) {
        return memberDao.findNoOfMembersByRank(rankId);
    }

    @Override
    public void doCreateSimpleMember(Locale locale, String phoneNo, String password, String verifyCode, int durationInMinutes, boolean skipSmsVerify) {
        UserService userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
        SmsService smsService = Application.lookupBean(SmsService.class);
        I18n i18n = Application.lookupBean(I18n.class);

        synchronized (synchronizedObject) {
            /************************
             * VERIFICATION - START
             ************************/
            if (StringUtils.isBlank(phoneNo)) {
                throw new ValidatorException(i18n.getText("invalidPhoneno", locale));
            }

            if (phoneNo.length() < 6) {
                throw new ValidatorException(i18n.getText("minimumPhonenoLengthError", 6, locale));
            }

            if (!userService.checkUserAvailability(locale, phoneNo)) {
                throw new ValidatorException(i18n.getText("phonenoRegistered", locale, phoneNo));
            }

            if (findMemberByMemberCode(phoneNo) != null) {
                throw new ValidatorException(i18n.getText("phonenoRegistered", locale, phoneNo));
            }

            if (StringUtils.isBlank(password)) {
                throw new ValidatorException(i18n.getText("invalidPassword", locale));
            }

            if (password.length() < 6) {
                throw new ValidatorException(i18n.getText("minimumPasswordLengthError", 6, locale));
            }

            SmsTag smsTag;
            if (!skipSmsVerify) {
                smsTag = smsService.doVerifyRegistrationVerifyCodeWithoutUpdateStatus(locale, phoneNo, verifyCode, durationInMinutes);
                smsService.updateSmsTagStatusToUsed(smsTag);
            }
            /************************
             * VERIFICATION - END
             ************************/

            Member member = doCreateSimpleMember(phoneNo, password);
            member.setActiveBy(member.getMemberId());
            member.setActiveDatetime(new Date());
            memberDao.update(member);

            if (StringUtils.isNotBlank(member.getMemberDetail().getEmail())) {
                doSendWelcomeLetter(locale, member, password);
            }
        }
    }

    @Override
    public Member doCreateSimpleMember(String... parameter) { //parameter = 0:phone no, 1:password, 2:username, 3.email, 4.gender
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);

        MemberDetail memberDetail;
        int whaleliveId;

        //get random whale live id (must be unique)
        do {
            Random rand = new Random();
            whaleliveId = rand.nextInt(99999999);
            memberDetail = memberDetailDao.findMemberDetailsByWhaleliveId(String.valueOf(whaleliveId));
        } while (memberDetail != null);

        memberDetail = new MemberDetail(true);
        memberDetail.setWhaleliveId(String.valueOf(whaleliveId));
        memberDetail.setContactNo(parameter[0]); //phoneNo
        memberDetail.setProfileName(memberDetail.getWhaleliveId()); //username

//        if (setDetail) {
//            memberDetail.setProfileName(StringUtils.isNotBlank(parameter[2]) ? parameter[2] : memberDetail.getWhaleliveId()); //username
//            memberDetail.setEmail(parameter[3]); //email
//            memberDetail.setGender(parameter[4]); //gender
//        } else {
//            memberDetail.setProfileName(memberDetail.getWhaleliveId()); //username
//        }

        assignCountryCodeByCountactNo(memberDetail);
        memberDetail = assignCurrencyBase(memberDetail, memberDetail.getContactNo()); //temporary method to handle few common used country currency code
        memberDetailDao.save(memberDetail);

        String defaultLiveStreamTypes = LiveCategory.DEFAULT_CATEGORY.toUpperCase() + "|" + LiveCategory.DEFAULT_CATEGORY_2.toUpperCase();
        if (memberDetail.getContactNo().startsWith("86")) {
            defaultLiveStreamTypes = LiveCategory.DEFAULT_CHINA_CATEGORY.toUpperCase();
        }

        Integer uid = generateAgoraUid();
        Member member = new Member(true);
        member.setMemberCode(memberDetail.getContactNo()); //phoneNo
        member.setFullName("");
        member.setFirstName("");
        member.setLastName("");
        member.setIdentityNo("");
        member.setMemberDetId(memberDetail.getMemberDetId());
        member.setAgentId(Global.DEFAULT_AGENT);
        member.setStatus(Global.Status.ACTIVE);
        member.setLiveStreamTypes(defaultLiveStreamTypes);
        member.setAgoraUid(uid);
        member.setMemberDetail(memberDetail);
        memberDao.save(member);

        member.setAgoraUid(doGetMemberAgoraUid(member));

        MemberUser memberUser = new MemberUser(true);
        memberUser.setCompId(Global.DEFAULT_COMPANY);
        memberUser.setMemberId(member.getMemberId());
        memberUser.setUsername(member.getMemberCode());
        memberUser.setPassword(parameter[1]); //password
        memberUser.setStatus(Global.Status.ACTIVE);

        UserRole memberRole = userDetailsService.findUserRoleByRoleName(Global.DEFAULT_COMPANY, Global.UserRoleGroup.MEMBER_GROUP);
        if (memberRole == null)
            userDetailsService.saveUser(memberUser, null);
        else
            userDetailsService.saveUser(memberUser, Collections.singletonList(memberRole));

        YunxinClient yunxinClient = new YunxinClient();
        YunXinResponse response = yunxinClient.createUserAccount(notificationService.generateAccId(), Global.AccessModule.WHALE_MEDIA);
        if (response != null && response.getInfo() != null) {
            notificationService.doCreateNotificationAccount(member.getMemberId(), Global.AccessModule.WHALE_MEDIA, response.getInfo());
        }

//        doProcessFreePOSNotification(member.getMemberId());
        doProcessFreeWhaleCoinNewUser(member.getMemberId());

        return member;
    }

//    public void doProcessFreePOSNotification(String memberId) {
//        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
//        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
//        MessageService messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
//        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
//        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
//        Environment env = Application.lookupBean(Environment.class);
//
//        Member member = memberService.getMember(memberId);
//        Message message = messageService.doGenerateNoticeMessage(memberId,
//                i18n.getText("title.freePosForSigningWhaleMedia", new Locale(Global.Language.ENGLISH)),
//                i18n.getText("title.freePosForSigningWhaleMedia", new Locale(Global.Language.CHINESE)),
//                i18n.getText("message.freePosForSigningWhaleMedia", new Locale(Global.Language.ENGLISH)),
//                i18n.getText("message.freePosForSigningWhaleMedia", new Locale(Global.Language.CHINESE)));
//
//        String serverUrl = env.getProperty("serverUrl");
//        String redirectUrl = serverUrl + "/pub/webView.php?messageId=" + message.getMessageId();
//
//        // send notification to yun xin for notification pop up
//        Payload payload = new Payload();
//        payload.setType(Payload.NOTICE);
//        payload.setRedirectUrl(redirectUrl + "&languageCode=" + member.getPreferLanguage());
//
//        NotificationAccount frmAcct = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
//        NotificationAccount toAcct = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);
//        if (frmAcct != null && toAcct != null) {
//            notificationConfProvider.doSendMessage(frmAcct.getAccountId(), toAcct.getAccountId(), Global.AccessModule.WHALE_MEDIA,
//                    i18n.getText("title.freePosForSigningWhaleMedia", new Locale(member.getPreferLanguage())),
//                    payload, null);
//        }
//    }

    @Override
    public void doProcessFreeWhaleCoinNewUser(String memberId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        MessageService messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
        Environment env = Application.lookupBean(Environment.class);
        WalletService walletService = Application.lookupBean(WalletService.class);

        String serverUrl = env.getProperty("serverUrl");
        BigDecimal freeCoin = new BigDecimal("10");
        String trx = i18n.getText("freeWhaleCoinFirstTimeDownloadUser", new Locale(Global.Language.ENGLISH), freeCoin);
        String trxCn = i18n.getText("freeWhaleCoinFirstTimeDownloadUser", new Locale(Global.Language.CHINESE), freeCoin);

        WalletAdjust walletAdjust = new WalletAdjust(true);
        walletAdjust.setOwnerType(Global.UserType.MEMBER);
        walletAdjust.setOwnerId(memberId);
        walletAdjust.setAmount(freeCoin);
        walletAdjust.setWalletType(Global.WalletType.WALLET_80);
        walletAdjust.setAdjustType(Global.WalletAdjustType.IN);
        walletAdjust.setRemark("FREE 10 WC for first time download");
        walletService.createWalletAdjust(new Locale(Global.Language.ENGLISH), walletAdjust, trx, trxCn);

        String coinUrl = serverUrl + "/asset/image/coin/" + Global.WalletType.WC.toLowerCase() + ".png";
        Member member = memberService.getMember(memberId);
        Message message = messageService.doGenerateNoticeMessage(memberId,
                i18n.getText("title.freeWhaleCoinSigningWhaleMedia", new Locale(Global.Language.ENGLISH), freeCoin),
                i18n.getText("title.freeWhaleCoinSigningWhaleMedia", new Locale(Global.Language.CHINESE), freeCoin),
                i18n.getText("message.freeWhaleCoinForSigningWhaleMedia", new Locale(Global.Language.ENGLISH), freeCoin, coinUrl),
                i18n.getText("message.freeWhaleCoinForSigningWhaleMedia", new Locale(Global.Language.CHINESE), freeCoin, coinUrl));

        String redirectUrl = serverUrl + "/pub/webView.php?messageId=" + message.getMessageId();
        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.NOTICE);
        payload.setRedirectUrl(redirectUrl + "&languageCode=" + member.getPreferLanguage());

        NotificationAccount frmAcct = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        NotificationAccount toAcct = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);
        if (frmAcct != null && toAcct != null) {
            notificationConfProvider.doSendMessage(frmAcct.getAccountId(), toAcct.getAccountId(), Global.AccessModule.WHALE_MEDIA,
                    i18n.getText("title.freeWhaleCoinSigningWhaleMedia", new Locale(member.getPreferLanguage()), freeCoin),
                    payload, null);
        }
    }

    private void assignCountryCodeByCountactNo(MemberDetail memberDetail) {
        if (StringUtils.startsWith(memberDetail.getContactNo(), "86")) {
            memberDetail.setCountryCode("CN");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "886")) {
            memberDetail.setCountryCode("TW");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "852")) {
            memberDetail.setCountryCode("HK");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "81")) {
            memberDetail.setCountryCode("JP");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "82")) {
            memberDetail.setCountryCode("KR");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "65")) {
            memberDetail.setCountryCode("SG");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "66")) {
            memberDetail.setCountryCode("TH");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "62")) {
            memberDetail.setCountryCode("ID");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "84")) {
            memberDetail.setCountryCode("VN");
        } else if (StringUtils.startsWith(memberDetail.getContactNo(), "63")) {
            memberDetail.setCountryCode("PH");
        } else {
            memberDetail.setCountryCode("MY");
        }
    }

    private MemberDetail assignCurrencyBase(MemberDetail memberDetail, String memberCode) {
        if (StringUtils.startsWith(memberCode, "86")) {
            memberDetail.setCurrencyCode("CNY");
        } else if (StringUtils.startsWith(memberCode, "82")) {
            memberDetail.setCurrencyCode("KRW");
        } else if (StringUtils.startsWith(memberCode, "886")) {
            memberDetail.setCurrencyCode("TWD");
        } else {
            memberDetail.setCurrencyCode("MYR");
        }
        return memberDetail;
    }

    @Override
    public MemberMnemonic doCreateMnemonic(Locale locale, String memberId, String userId, String transactionPassword) {
        I18n i18n = Application.lookupBean(I18n.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        /************************
         * VERIFICATION - START
         ************************/
        if (StringUtils.isBlank(transactionPassword) || transactionPassword.length() < 6) {
            throw new ValidatorException(i18n.getText("invalidTransactionPassword", locale));
        }

        User user = userDetailsService.findUserByUserId(userId);
        if (user == null) {
            throw new ValidatorException("Invalid user");
        }

        Member member = getMember(memberId);
        if (member == null) {
            throw new ValidatorException("Invalid member");
        }

        MemberMnemonic memberMnemonic = findActiveMemberMnemonicByMemberId(member.getMemberId());
        if (memberMnemonic != null) {
            throw new ValidatorException(i18n.getText("userHasBeenSetupTransactionPassword", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        userDetailsService.doResetPassword2(userId, transactionPassword);

        memberDao.update(member);

        Pair<String, Integer> pair = generateMnenonic();
        memberMnemonic = new MemberMnemonic(true);
        memberMnemonic.setMemberId(memberId);
        memberMnemonic.setMnemonic(pair.getLeft());
        memberMnemonic.setMnemonicLength(pair.getRight());
        memberMnemonicDao.save(memberMnemonic);

        return memberMnemonic;
    }

    private Pair<String, Integer> generateMnenonic() {
        StringBuilder sb = new StringBuilder();
        byte[] entropy = new byte[Words.TWELVE.byteLength()];
        new SecureRandom().nextBytes(entropy);
        new MnemonicGenerator(English.INSTANCE).createMnemonic(entropy, sb::append);

        return new ImmutablePair<>(sb.toString(), 12);
    }

    @Override
    public MemberMnemonic findActiveMemberMnemonicByMemberId(String memberId) {
        return memberMnemonicDao.findActiveMemberMnemonicByMemberId(memberId);
    }

    @Override
    public void findSimilarMemberDetailByProfileName(DatagridModel<MemberDetail> memberDetailsPaging, String keyword, String userMemberDetailId) {
        memberSqlDao.findSimilarMemberDetailsByProfileName(memberDetailsPaging, keyword, userMemberDetailId);
    }

    @Override
    public void doRetrieveMemberPasswordByEmail(Locale locale, String memberCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        /************************
         * VERIFICATION - START
         ************************/
        if (StringUtils.isBlank(memberCode)) {
            throw new ValidatorException(i18n.getText("invalidEmailAddress", locale));
        }

        Member member = findMemberByMemberCode(memberCode);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        MemberUser memberUser = findMemberUserByMemberId(member.getMemberId());
        if (memberUser == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }
        /************************
         * VERIFICATION - END
         ************************/

        doSendRetrieveMemberPasswordLetter(locale, member, memberUser.getPassword());
    }

    @Override
    public void doSendRetrieveMemberPasswordLetter(Locale locale, Member member, String password) {
        MailTemplateFormater mailTemplateFormater = new MailTemplateFormater("simple/retrieve_password_letter.html");
        String content = mailTemplateFormater.format(getMemberName(member), member.getMemberCode(), WebUtil.getSystemConfig().getApplicationName(), password);

        setEmailq(member.getMemberDetail().getEmail(), "retrievePasswordLetter", content);
    }

    @Override
    public void setEmailq(String email, String title, String content) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        EmailService emailService = Application.lookupBean(EmailService.BEAN_NAME, EmailService.class);

        Emailq emailq = new Emailq(true);
        emailq.setTitle(i18n.getText(title));
        emailq.setBody(content);
        emailq.setEmailTo(email);
        emailService.saveEmailq(emailq);
    }

    private String getMemberName(Member member) {
        if (member.getMemberDetail().getProfileName() != null) {
            return member.getMemberDetail().getProfileName();
        }

        return member.getMemberDetail().getWhaleliveId();
    }

    public void doCheckChangePasswordValidity(Locale locale, String currentPassword, String oldPassword, String newPassword, String confirmPassword) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        if (!currentPassword.equals(oldPassword)) {
            throw new ValidatorException(i18n.getText("Invalid Current Password", locale));
        }
        if (!newPassword.equals(confirmPassword)) {
            throw new ValidatorException(i18n.getText("Confirmed Password does not matched", locale));
        }
    }

    @Override
    public List<Member> findAllMembers() {
        return memberDao.findAllMembers();
    }

    @Override
    public void doMemberSelfResetPasswordRequest(Locale locale, String phoneno) {
        I18n i18n = Application.lookupBean(I18n.class);
        SmsService smsService = Application.lookupBean(SmsService.class);
        UserService userService = Application.lookupBean(UserService.class);

        /************************
         * VERIFICATION - START
         ************************/

        phoneno = StringUtils.remove(phoneno, " ");

        if (StringUtils.isBlank(phoneno)) {
            throw new ValidatorException(i18n.getText("invalidPhoneno", locale));
        }

        if (phoneno.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumPhonenoLengthError", 6, locale));
        }

        String userType = userService.getUserType(phoneno); //check is it Omnic+ user
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_NOT_REGISTERED)) {
            if (findMemberByMemberCode(phoneno) == null) {
                throw new ValidatorException(i18n.getText("phonenoNotRegistered", locale, phoneno));
            }
        }

        /************************
         * VERIFICATION - END
         ************************/

        smsService.doGenerateVerifySms(locale, phoneno);
    }

    @Override
    public void doMemberSelfResetPassword(Locale locale, String phoneno, String verifyCode, String newPassword, int durationInMinutes) {
        I18n i18n = Application.lookupBean(I18n.class);
        SmsService smsService = Application.lookupBean(SmsService.class);
        UserService userService = Application.lookupBean(UserService.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        /************************
         * VERIFICATION - START
         ************************/
        phoneno = StringUtils.remove(phoneno, " ");

        if (StringUtils.isBlank(phoneno)) {
            throw new ValidatorException(i18n.getText("invalidPhoneno", locale));
        }

        if (phoneno.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumPhonenoLengthError", 6, locale));
        }

        String userType = userService.getUserType(phoneno); //check is it Omnic+ user, in case user reset password before login
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_NOT_REGISTERED)) {
            if (findMemberByMemberCode(phoneno) == null) {
                throw new ValidatorException(i18n.getText("phonenoNotRegistered", locale, phoneno));
            }
        }

        SmsTag smsTag = smsService.doVerifyRegistrationVerifyCodeWithoutUpdateStatus(locale, phoneno, verifyCode, durationInMinutes);

        if (StringUtils.isBlank(newPassword)) {
            throw new ValidatorException(i18n.getText("invalidNewPassword", locale, newPassword));
        }

        if (newPassword.length() < 6) {
            throw new ValidatorException(i18n.getText("minimumNewPasswordLengthError", locale, 6));
        }
        /************************
         * VERIFICATION - END
         ************************/

        smsService.updateSmsTagStatusToUsed(smsTag);

        User user = userDetailsDao.getByUsername(phoneno);
        if (user != null) { //in case user reset password before login
            String encryptedNewPassword = userDetailsService.encryptPassword(user, newPassword);
            user.setPassword(encryptedNewPassword);
            userDetailsDao.update(user);
        }

        //update omnicplus password for omnicplus user
        if (userType.equalsIgnoreCase(Global.OmnicplusUserType.USER_TYPE_VALID)) {
            String status = updateOmnicplusPassword(locale.toString(), phoneno, newPassword);

            if (!status.equals("loginPasswordResetSuccessfully")) {
                throw new ValidatorException(i18n.getText(status, locale));
            }
        }
    }

    private String updateOmnicplusPassword(String lang, String phoneno, String password) {
        OmnicplusClient omnicplusClient = new OmnicplusClient();
        try {
            CredentialsDto credentialsDto = new CredentialsDto();
            credentialsDto.setPhoneno(phoneno);
            credentialsDto.setPassword(password);

            OmnicplusDto omnicplusDto = omnicplusClient.callOmnicplusResetPassword(lang, credentialsDto);
            return omnicplusDto.getStatus();
        } catch (Exception ex) {
            throw new SystemErrorException(ex);
        }
    }

    @Override
    public void doUpdateUserProfile(Locale locale, String memberId, String username, String email, String gender, String countryCode, String birthday, String bio) {
        I18n i18n = Application.lookupBean(I18n.class);
        CountryService countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);

        /************************
         * VERIFICATION - START
         ************************/
        Member member = getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        if (StringUtils.isBlank(username)) {
            throw new ValidatorException(i18n.getText("invalidUsername", locale));
        }

        if (username.length() > 20) {
            throw new ValidatorException(i18n.getText("usernameExceedMaxLength", locale, 20));
        }

        if (!EmailValidator.getInstance().isValid(email) && StringUtils.isNotBlank(email)) {
            throw new ValidatorException(i18n.getText("invalidEmailAddress", locale));
        }

        if (StringUtils.isBlank(gender)) {
            throw new ValidatorException(i18n.getText("invalidGender", locale));
        }

        if (!gender.equalsIgnoreCase(Global.Gender.FEMALE) && !gender.equalsIgnoreCase(Global.Gender.MALE)) {
            throw new ValidatorException(i18n.getText("invalidGender", locale));
        }

        if (StringUtils.isNotBlank(countryCode)) {
            Country country = countryService.getCountry(countryCode);
            if (country == null) {
                throw new ValidatorException(i18n.getText("invalidCountry", locale));
            }
        }
        /************************
         * VERIFICATION - END
         ************************/
        MemberDetail memberDetail = member.getMemberDetail();

        if (!username.equals(memberDetail.getProfileName())) { //create log if profileName not same
            doCreateProfileNameLog(memberId, memberDetail.getProfileName(), username);
        }

        memberDetail.setProfileName(username);
        memberDetail.setEmail(email);
        memberDetail.setGender(gender);
        memberDetail.setCountryCode(countryCode);
        memberDetail.setProfileBio(bio);

        if (StringUtils.isNotBlank(birthday)) {
            memberDetail.setDob(DateUtil.getDate(birthday, "yyyy-MM-dd"));
        }

        memberDetailDao.update(memberDetail);
    }

    @Override
    public MemberDetail findMemberDetailByMemberId(String memberId) {
        Member member = memberDao.get(memberId);
        if (member != null)
            return member.getMemberDetail();
        else
            return null;
    }

    @Override
    public MemberDetail findMemberDetailByWhaleliveId(String whaleliveId) {
        return memberDetailDao.findMemberDetailsByWhaleliveId(whaleliveId);
    }

    @Override
    public MemberDetail findMemberDetailsByContactNo(String contactNo) {
        return memberDetailDao.findMemberDetailsByContactNo(contactNo);
    }

    @Override
    public void doUpdateMemberBankInfo(Locale locale, String memberId, MemberDetail memberDetail) {
//        I18n i18n = Application.lookupBean(I18n.class);
//        /************************
//         * VERIFICATION - START
//         ************************/
//        Member member = getMember(memberId);
//
//        if (member == null) {
//            throw new ValidatorException("Invalid Member");
//        }
//
//        MemberDetail memberDetailDb = member.getMemberDetail();
//        final String originalStatus = StringUtils.isBlank(memberDetailDb.getBankVerifyStatus()) ? MemberDetail.STATUS_BANK_NEW :
//                memberDetailDb.getBankVerifyStatus();
//
//        if (StringUtils.isBlank(memberDetail.getBankName())) {
//            throw new ValidatorException(i18n.getText("invalidBankName", locale));
//        }
//
//        if (StringUtils.isBlank(memberDetail.getBankBranch())) {
//            throw new ValidatorException(i18n.getText("invalidBankBranch", locale));
//        }
//
//        if (StringUtils.isBlank(memberDetail.getBankAddress())) {
//            throw new ValidatorException(i18n.getText("invalidBankAddress", locale));
//        }
//
//        if (StringUtils.isBlank(memberDetail.getBankCode())) {
//            throw new ValidatorException(i18n.getText("invalidBankCode", locale));
//        }
//
//        if (StringUtils.isBlank(memberDetail.getBankAccountNo())) {
//            throw new ValidatorException(i18n.getText("invalidBankAccountNo", locale));
//        }
//
//        if (StringUtils.isBlank(memberDetail.getBankAccountName())) {
//            throw new ValidatorException(i18n.getText("invalidBankAccountName", locale));
//        }
//
//        /************************
//         * VERIFICATION - END
//         ************************/
//
//        memberDetailDb.setBankName(memberDetail.getBankName());
//        memberDetailDb.setBankBranch(memberDetail.getBankBranch());
//        memberDetailDb.setBankAddress(memberDetail.getBankAddress());
//        memberDetailDb.setBankCode(memberDetail.getBankCode());
//        memberDetailDb.setBankAccountNo(memberDetail.getBankAccountNo());
//        memberDetailDb.setBankAccountName(memberDetail.getBankAccountName());
//
//        List<MemberFile> memberFiles = findMemberFilesByMemberId(memberId);
//        switch (originalStatus) {
//            case MemberDetail.STATUS_BANK_APPROVED:
//            case MemberDetail.STATUS_BANK_PENDING_APPROVAL:
//                // does nothing
//                break;
//            case MemberDetail.STATUS_BANK_NEW:
//            case MemberDetail.STATUS_BANK_REJECTED_FORM:
//                if (memberFiles.size() == 3) {
//                    memberDetailDb.setBankVerifyStatus(MemberDetail.STATUS_BANK_PENDING_APPROVAL);
//                    memberDetailDb.setBankVerifyRemark("");
//                }
//                break;
//        }
//
//        memberDetailDao.update(memberDetailDb);
    }

    @Override
    public void doProcessMemberFiles(Locale locale, String memberId, File fileUploadBank, String fileUploadBankContentType, String fileUploadBankFileName,
                                     File fileUploadResidence, String fileUploadResidenceContentType, String fileUploadResidenceFileName,
                                     File fileUploadIc, String fileUploadIcContentType, String fileUploadIcFileName) {
        I18n i18n = Application.lookupBean(I18n.class);
        /************************
         * VERIFICATION - START
         ************************/
        Member member = getMember(memberId);
        MemberDetail memberDetail = member.getMemberDetail();

        if (member == null) {
            throw new ValidatorException("Invalid Member");
        }

        if (fileUploadBank == null && fileUploadIc == null && fileUploadResidence == null) {
            throw new ValidatorException(i18n.getText("pleaseUploadAtLeastOneDocument", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        if (fileUploadBank != null) {
            doProcessMemberFiles_processFile(locale, memberId, fileUploadBank, fileUploadBankContentType, fileUploadBankFileName, MemberFile.UPLOAD_TYPE_BANK);
        }
        if (fileUploadResidence != null) {
            doProcessMemberFiles_processFile(locale, memberId, fileUploadResidence, fileUploadResidenceContentType, fileUploadResidenceFileName, MemberFile.UPLOAD_TYPE_RESIDENCE);
        }
        if (fileUploadIc != null) {
            doProcessMemberFiles_processFile(locale, memberId, fileUploadIc, fileUploadIcContentType, fileUploadIcFileName, MemberFile.UPLOAD_TYPE_IC);
        }

        doProcessMemberFiles_updateBankInfoStatusWhenNewMemberFileUpoaded(memberId, memberDetail);
    }

    private void doProcessMemberFiles_updateBankInfoStatusWhenNewMemberFileUpoaded(String memberId, MemberDetail memberDetail) {
//        final String originalStatus = StringUtils.isBlank(memberDetail.getBankVerifyStatus()) ? MemberDetail.STATUS_BANK_NEW :
//                memberDetail.getBankVerifyStatus();
//
//        List<MemberFile> memberFiles = findMemberFilesByMemberId(memberId);
//
//        switch (originalStatus) {
//            case MemberDetail.STATUS_BANK_NEW:
//            case MemberDetail.STATUS_BANK_PENDING_APPROVAL:
//            case MemberDetail.STATUS_BANK_REJECTED_FORM:
//            case MemberDetail.STATUS_BANK_APPROVED:
//                // does nothing
//                break;
//            case MemberDetail.STATUS_BANK_PENDING_UPLOAD:
//            case MemberDetail.STATUS_BANK_REJECTED_UPLOAD:
//                if (memberFiles.size() == 3) {
//                    memberDetail.setBankVerifyStatus(MemberDetail.STATUS_BANK_PENDING_APPROVAL);
//                    memberDetail.setBankVerifyRemark("");
//                    memberDetailDao.update(memberDetail);
//                }
//
//                break;
//        }
    }

    private void doProcessMemberFiles_processFile(Locale locale, String memberId, File fileUpload, String contentType, String fileName, String fileType) {
        FileUploadConfiguration config = Application.lookupBean(FileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);
        final String uploadPath = config.getMemberFileUploadPath();

        MemberFile memberFile = findMemberFileByMemberIdAndType(memberId, fileType);
        String originalFileName = null;
        if (memberFile != null) {
            originalFileName = memberFile.getRenamedFilename();
            // production using blobstorage in azure, only delete application file path for testing environment
            if (!isProdServer) {
                File file = new File(uploadPath, originalFileName);
                if (file.exists()) {
                    file.delete();
                }
            }
            memberFileDao.delete(memberFile);
        }
        memberFile = new MemberFile(true);
        memberFile.setMemberId(memberId);
        memberFile.setType(fileType);
        memberFile.setFilename(fileName);
        memberFile.setContentType(contentType);
        memberFile.setFileSize(fileUpload.length());
        memberFileDao.save(memberFile);

        // production using blobstorage in azure
        if (isProdServer) {
            azureBlobStorageService.uploadFileToAzureBlobStorage(memberFile.getRenamedFilename(), originalFileName,
                    fileUpload, contentType, MemberFileUploadConfiguration.FOLDER_NAME);
        } else {
            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(fileUpload.getAbsolutePath()));

                Path path = Paths.get(uploadPath, memberFile.getRenamedFilename());
                Files.write(path, bytes);
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        }
    }

    public void doProcessMemberUploadFile(String memberId, String uploadType, File fileUpload, String fileUploadContentType, String fileUploadFileName) {
        /************************
         * VERIFICATION - START
         ************************/

        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("Invalid Member");
        }

        Member member = getMember(memberId);
        if (member == null) {
            throw new ValidatorException("Invalid Member");
        }

        /************************
         * VERIFICATION - END
         ************************/

        doProcessMemberFile_processFile(memberId, fileUpload, fileUploadContentType, fileUploadFileName, uploadType);
    }

    @Override
    public void doSendNotificationEmailToAdmin(String ccAddress, String bccAddress, String title, String body) {
        final String adminEmail = "crystal@omc-group.io";

        Emailq emailq = new Emailq(true);
        emailq.setEmailTo(adminEmail);
        emailq.setEmailCc(ccAddress);
        emailq.setEmailBcc(bccAddress);
        emailq.setTitle(title);
        emailq.setBody(body);

        emailqDao.save(emailq);
    }

    private void doProcessMemberFile_processFile(String memberId, File fileUpload, String contentType, String fileName, String fileType) {
        MemberFileUploadConfiguration config = Application.lookupBean(MemberFileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        final String uploadPath = config.geMemberFileUploadPath();
        MemberFile memberFile = findMemberFileByMemberIdAndType(memberId, fileType);
        String originalFileName = null;
        if (memberFile != null) {
            originalFileName = memberFile.getRenamedFilename();
            // production using blobstorage in azure, only delete application file path for testing environment
            if (!isProdServer) {
                File file = new File(uploadPath, originalFileName);
                if (file.exists()) {
                    file.delete();
                }
            }
            memberFile.setFilename(fileName);
            memberFile.setContentType(contentType);
            memberFile.setMemberId(memberId);
            memberFile.setFileSize(fileUpload.length());
            memberFileDao.delete(memberFile);
        }

        memberFile = new MemberFile(true);
        memberFile.setMemberId(memberId);
        memberFile.setType(fileType);
        memberFile.setFilename(fileName);
        memberFile.setContentType(contentType);
        memberFile.setFileSize(fileUpload.length());
        memberFileDao.save(memberFile);

        // production using blobstorage in azure
        if (isProdServer) {
            azureBlobStorageService.uploadFileToAzureBlobStorage(memberFile.getRenamedFilename(), originalFileName, fileUpload, contentType, MemberFileUploadConfiguration.FOLDER_NAME);
        } else {
            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(fileUpload.getAbsolutePath()));

                Path path = Paths.get(uploadPath, memberFile.getRenamedFilename());
                Files.write(path, bytes);
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        }

    }

    @Override
    public MemberFile getMemberFile(String fileId) {
        return memberFileDao.get(fileId);
    }

    @Override
    public List<MemberFile> findMemberFilesByMemberId(String memberId) {
        return memberFileDao.findMemberFilesByMemberId(memberId);
    }

    @Override
    public void doProcessUploadMemberFile(Locale locale, String memberId, String uploadType, InputStream fileInputStream, String fileName, long fileSize, String contentType) {
        FileUploadConfiguration config = Application.lookupBean(FileUploadConfiguration.class);

        /************************
         * VERIFICATION - START
         ************************/

        switch (uploadType) {
            case MemberFile.UPLOAD_TYPE_BANK:
            case MemberFile.UPLOAD_TYPE_IC:
            case MemberFile.UPLOAD_TYPE_RESIDENCE:
                break;
            default:
                throw new ValidatorException("Invalid upload type");
        }

        Member member = getMember(memberId);
        if (member == null) {
            throw new ValidatorException("Invalid Member");
        }

        MemberDetail memberDetail = member.getMemberDetail();

        /************************
         * VERIFICATION - END
         ************************/

        final String uploadPath = config.getMemberFileUploadPath();

        MemberFile memberFile = findMemberFileByMemberIdAndType(memberId, uploadType);
        if (memberFile != null) {
            String originalFileName = memberFile.getRenamedFilename();
            File file = new File(uploadPath, originalFileName);
            if (file.exists()) {
                file.delete();
            }
            memberFile.setFilename(fileName);
            memberFile.setContentType(contentType);
            memberFile.setFileSize(fileSize);
            memberFileDao.delete(memberFile);
        }
        memberFile = new MemberFile(true);
        memberFile.setMemberId(memberId);
        memberFile.setType(uploadType);
        memberFile.setFilename(fileName);
        memberFile.setContentType(contentType);
        memberFile.setFileSize(fileSize);
        memberFileDao.save(memberFile);

        File directory = new File(uploadPath);
        if (!directory.exists()) {
            directory.mkdirs();
        }

        try {
            File targetFile = new File(uploadPath, memberFile.getRenamedFilename());
            FileUtils.copyInputStreamToFile(fileInputStream, targetFile);

            if ("form-data".equalsIgnoreCase(contentType) || "multipart/form-data".equalsIgnoreCase(contentType)) {
                Tika tika = new Tika();

                String betterContentType = tika.detect(targetFile);
                memberFile.setContentType(betterContentType);
                memberFileDao.update(memberFile);
            }

        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        }

        doProcessMemberFiles_updateBankInfoStatusWhenNewMemberFileUpoaded(memberId, memberDetail);
    }

    @Override
    public Member findMemberByMemberDetId(String memberDetailId) {
        return memberDao.findMemberByMemberDetId(memberDetailId);
    }

    @Override
    public ProfileDto getProfileDetails(Locale locale, String userMemberId, String targetMemberId, String viewFrmChannelId) {
        FollowerService followerService = Application.lookupBean(FollowerService.class);
        MemberBlockService memberBlockService = Application.lookupBean(MemberBlockService.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.class);
        LiveChatService liveChatService = Application.lookupBean(LiveChatService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.class);
        CountryService countryService = Application.lookupBean(CountryService.class);
        PointService pointService = Application.lookupBean(PointService.class);
        RankService rankService = Application.lookupBean(RankService.class);
        MemberFileUploadConfiguration memberFileUploadConfiguration = Application.lookupBean(MemberFileUploadConfiguration.class);
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        NotificationAccount yunXinAccount = notificationService.findNotifAccByMemberAndAccessModule(targetMemberId, Global.AccessModule.WHALE_MEDIA);
        MemberDetail memberDetail = findMemberDetailByMemberId(targetMemberId);
        if (memberDetail == null) {
            throw new ValidatorException("invalidMember", locale);
        }

        Member member = getMember(targetMemberId);
        MemberFile memberFile = findMemberFileByMemberIdAndType(targetMemberId, MemberFile.UPLOAD_TYPE_PROFILE_PIC);
        int followerListCount = followerService.getFollowerCountById(targetMemberId);
        int followingListCount = followerService.getFollowingCountById(targetMemberId);
        Follower follower = followerService.getFollower(userMemberId, targetMemberId); //whether i following this memberId

        boolean isFollowing = false;
        boolean isBlocking;
        boolean isBlockBy;
        if (follower != null) {
            isFollowing = true;
        }

        isBlocking = memberBlockService.isMemberBlocked(userMemberId, targetMemberId); //i blocking this memberId?
        isBlockBy = memberBlockService.isMemberBlocked(targetMemberId, userMemberId); //this memberId blocked me?
        final String parentUrl = memberFileUploadConfiguration.getFullMemberFileParentFileUrl();

        Channel channel = new Channel();
        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(parentUrl);
            channel.setAvatarUrl(memberFile.getFileUrl());
        }

        LiveStreamChannel liveStream = liveStreamService.getLiveStreamChannelByMemberId(targetMemberId, false);
        if (liveStream == null) {
            channel = null;
        } else {
            channel.setChannelId(liveStream.getChannelId());
            channel.setChannelName(liveStream.getChannelName());
            channel.setVjAgoraUid(memberService.doGetMemberAgoraUid(member));

            Member userMember = memberService.getMember(userMemberId);
            if (userMember != null) {
                Pair<Integer, String> tokenInfo = memberService.doGetChannelTokenInfo(userMember.getMemberId(), liveStream.getChannelName());
                channel.setUserAgoraUid(tokenInfo.getLeft());
                channel.setAgoraToken(tokenInfo.getRight());
            }

            channel.setMemberId(targetMemberId);
            channel.setWhaleLiveId(memberDetail.getWhaleliveId());
            channel.setProfileName(memberDetail.getProfileName());

            LiveChatRoom liveChatRoom = liveChatService.findChatRoomByChannelId(liveStream.getChannelId());
            if (liveChatRoom != null) {
                channel.setRoomId(liveChatRoom.getRoomId());
//                channel.setViewCount(liveStreamProvider.getViewCount(liveChatRoom.getRoomId()));
            }
        }

        List<String> actionOptions = new ArrayList<>();
        if (StringUtils.isNotBlank(viewFrmChannelId)) {
            LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannel(viewFrmChannelId, null, false);

            if (liveStreamChannel != null) {
                boolean isMute = liveChatService.isMemberMuted(locale, viewFrmChannelId, targetMemberId);
                boolean isVj = liveStreamChannel.getMemberId().equals(userMemberId);
                boolean isAdmin = memberFansService.isMemberFans(userMemberId, liveStreamChannel.getMemberId(), MemberFans.ROLE_ADMIN);
                boolean isSeniorAdmin = memberFansService.isMemberFans(userMemberId, liveStreamChannel.getMemberId(), MemberFans.ROLE_SENIOR_ADMIN);

                if ((isVj || isAdmin || isSeniorAdmin) && !targetMemberId.equals(userMemberId) && !targetMemberId.equals(liveStreamChannel.getMemberId())) { //!see own profile && !seeing vj profile
                    if (isMute) {
                        actionOptions.add("Unmute");
                    } else {
                        actionOptions.add("Mute");
                    }

                    if (isVj || isSeniorAdmin) {
                        actionOptions.add("Kick");
                    }
                }
            }
        }

        //Check Gender
        String gender = null;
        if (StringUtils.isNotBlank(memberDetail.getGender()) && memberDetail.getGender().equals(Global.Gender.MALE)) {
            gender = i18n.getText("male", locale);
        } else if (StringUtils.isNotBlank(memberDetail.getGender()) && memberDetail.getGender().equals(Global.Gender.FEMALE)) {
            gender = i18n.getText("female", locale);
        }

        //Check Country
        String countryName = null;
        if (StringUtils.isNotBlank(memberDetail.getCountryCode())) {
            CountryDesc countryDesc = countryService.findCountryDescByLocaleAndCountryCode(locale, memberDetail.getCountryCode());
            if (countryDesc != null) {
                countryName = StringUtil.capitalize(countryDesc.getCountryName()); //capitalize: first character big capital
            }
        }

        //Show BadgeView
        String levelUrl = "";
        if (member.getLevel() != null) {
            levelUrl = cryptoProvider.getServerUrl() + "/asset/image/rank/" + Member.getRankInString(rankService.getCurrentRank(member.getTotalExp())) + ".png";
        }

        //Check received point
        BigDecimal totalPoint = pointService.getTotalPointReceivedByOwnerId(member.getMemberId(), false);
        String receivedPoint = "";
        if (totalPoint != null) {
            receivedPoint = totalPoint.toString();
        }
        //Check sent point
        totalPoint = pointService.getTotalPointSentByContributorId(member.getMemberId(), false);
        String sentPoint = "";
        if (totalPoint != null) {
            sentPoint = totalPoint.toString();
        }

        ProfileDto profileDto = new ProfileDto();
        profileDto.setProfilePic(memberFile != null ? memberFile.getFileUrl() : null);
        profileDto.setMemberId(targetMemberId);
        profileDto.setProfileName(memberDetail.getProfileName());
        profileDto.setWhaleliveId(memberDetail.getWhaleliveId());
        profileDto.setProfileBio(memberDetail.getProfileBio());
        profileDto.setYunXinAccountId(yunXinAccount != null ? yunXinAccount.getAccountId() : null);
        profileDto.setFollowerCount(followerListCount);
        profileDto.setFollowingCount(followingListCount);
        profileDto.setFollowing(isFollowing);
        profileDto.setBlocking(isBlocking);
        profileDto.setBlockBy(isBlockBy);
        profileDto.setChannel(channel);
        profileDto.setActionOptions(actionOptions);
        profileDto.setGender(gender);
        profileDto.setCountryName(countryName);
        profileDto.setLevelUrl(levelUrl);
        profileDto.setReceivedPoint(receivedPoint);
        profileDto.setSentPoint(sentPoint);

        return profileDto;
    }

    @Override
    public MemberAddress findMemberAddressById(String memberAddrId) {
        return memberAddressDao.get(memberAddrId);
    }

    @Override
    public MemberAddress getMemberAddressDetails(String memberId, String addressId) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("Invalid member");
        }

        MemberAddress memberAddress = memberAddressDao.findMemberAddress(memberId, addressId, false);
        if (memberAddress != null) {
            return memberAddress;
        }

        return null;
    }

    @Override
    public List<MemberAddress> findMemberAddressByMemberId(String memberId) {
        return memberAddressDao.findAllAddressByMemberId(memberId);
    }

    @Override
    public MemberAddress findDefaultMemberAddress(String memberId) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("Invalid member");
        }

        MemberAddress memberAddress = memberAddressDao.findMemberAddress(memberId, null, true);
        if (memberAddress != null) {
            return memberAddress;
        }

        return null;
    }

    @Override
    public MemberAddress doCreateMemberAddress(Locale locale, String memberId, String name, String phoneNo, String countryCode, String address, String city, String postCode, String state, String area, boolean defaultAddress, String phoneCountryCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        /************************
         * VERIFICATION - START
         ************************/
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMemberId", locale));
        }
        Member member = memberService.getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("memberNotFound"), locale);
        }

        if (StringUtils.isBlank(name)) {
            throw new ValidatorException(i18n.getText("invalidName", locale));
        }

        if (StringUtils.isBlank(phoneNo)) {
            throw new ValidatorException(i18n.getText("invalidPhoneno", locale));
        }

        if (StringUtils.isBlank(countryCode)) {
            throw new ValidatorException(i18n.getText("invalidCountry", locale));
        }

        if (StringUtils.isBlank(address)) {
            throw new ValidatorException(i18n.getText("invalidAddress", locale));
        }

        if (StringUtils.isBlank(city)) {
            throw new ValidatorException(i18n.getText("invalidCity", locale));
        }

        if (StringUtils.isBlank(postCode)) {
            throw new ValidatorException(i18n.getText("invalidPostCode", locale));
        }

        if (StringUtils.isBlank(state)) {
            throw new ValidatorException(i18n.getText("invalidState", locale));
        }

        if (StringUtils.isBlank(phoneCountryCode)) {
            throw new ValidatorException(i18n.getText("invalidPhoneCountryCode", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/
        if (defaultAddress) {
            doUpdateDefaultMemberAddress(memberId);
        }

        Address addresses = new Address();
        addresses.setContactName(name);
        addresses.setContactNo(phoneCountryCode + phoneNo);
        addresses.setCity(city);
        addresses.setPostcode(postCode);
        addresses.setAddress(address);
        addresses.setState(state);
        addresses.setArea(area);
        addresses.setCountryCode(countryCode);

        MemberAddress memberAddress = new MemberAddress(true);
        memberAddress.setMemberId(memberId);
        memberAddress.setAddress(addresses);
        memberAddress.setPhoneCountryCode(phoneCountryCode);
        memberAddress.setDefaultAddress(defaultAddress);
        memberAddressDao.save(memberAddress);
        return memberAddress;
    }

    @Override
    public MemberAddress doUpdateMemberAddress(Locale locale, String memberId, String memberAddrId, String name, String phoneNo, String countryCode, String address, String city, String postCode, String state, String area, boolean defaultAddress, String phoneCountryCode) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        /************************
         * VERIFICATION - START
         ************************/
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMemberId", locale));
        }
        Member member = memberService.getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("memberNotFound"), locale);
        }

        if (StringUtils.isBlank(memberAddrId)) {
            throw new ValidatorException(i18n.getText("invalidAddressId", locale));
        }
        MemberAddress dbMemberAddress = memberAddressDao.findMemberAddress(memberId, memberAddrId, false);
        if (dbMemberAddress == null) {
            throw new ValidatorException(i18n.getText("addressNotFound"), locale);
        }

        if (StringUtils.isBlank(name)) {
            throw new ValidatorException(i18n.getText("invalidName", locale));
        }

        if (StringUtils.isBlank(phoneNo)) {
            throw new ValidatorException(i18n.getText("invalidPhoneno", locale));
        }

        if (StringUtils.isBlank(countryCode)) {
            throw new ValidatorException(i18n.getText("invalidCountry", locale));
        }

        if (StringUtils.isBlank(address)) {
            throw new ValidatorException(i18n.getText("invalidAddress", locale));
        }

        if (StringUtils.isBlank(city)) {
            throw new ValidatorException(i18n.getText("invalidCity", locale));
        }

        if (StringUtils.isBlank(postCode)) {
            throw new ValidatorException(i18n.getText("invalidPostCode", locale));
        }

        if (StringUtils.isBlank(state)) {
            throw new ValidatorException(i18n.getText("invalidState", locale));
        }

        if (StringUtils.isBlank(phoneCountryCode)) {
            throw new ValidatorException(i18n.getText("invalidPhoneCountryCode", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        if (defaultAddress) {
            doUpdateDefaultMemberAddress(memberId);
        } else {
            boolean isDefaultAddress = checkIsDefaultAddress(dbMemberAddress.getMemberAddrId());
            if (isDefaultAddress) {
                throw new ValidatorException(i18n.getText("cannotUnselectDefaultAddress", locale));
            }
        }

        Address dbAddress = new Address();
        dbAddress.setContactName(name);
        dbAddress.setContactNo(phoneCountryCode + phoneNo);
        dbAddress.setCountryCode(countryCode);
        dbAddress.setAddress(address);
        dbAddress.setCity(city);
        dbAddress.setPostcode(postCode);
        dbAddress.setState(state);
        dbAddress.setArea(area);

        dbMemberAddress.setAddress(dbAddress);
        dbMemberAddress.setPhoneCountryCode(phoneCountryCode);
        dbMemberAddress.setDefaultAddress(defaultAddress);
        memberAddressDao.update(dbMemberAddress);
        return dbMemberAddress;
    }

    @Override
    public void doRemoveMemberAddress(Locale locale, String memberId, String addressId) {
        I18n i18n = Application.lookupBean(I18n.class);

        if (StringUtils.isBlank(addressId)) {
            throw new ValidatorException(i18n.getText("invalidAddressId", locale));
        }

        MemberAddress memberAddress = memberAddressDao.findMemberAddress(memberId, addressId, false);
        if (memberAddress != null) {
            if (memberAddress.getDefaultAddress()) {
                throw new ValidatorException(i18n.getText("cannotDeleteDefaultAddress", locale));
            }

            memberAddressDao.delete(memberAddress);
        } else {
            throw new ValidatorException(i18n.getText("addressNotFound", locale));
        }
    }

    @Override
    public boolean checkIsDefaultAddress(String addressId) {
        MemberAddress defaultMemberAddress = memberAddressDao.findMemberAddress(null, addressId, true);
        if (defaultMemberAddress != null) {
            return true;
        }

        return false;
    }

    @Override
    public void doUpdateDefaultMemberAddress(String memberId) {
        MemberAddress memberAddress = findDefaultMemberAddress(memberId);
        if (memberAddress != null) {
            memberAddress.setDefaultAddress(false);
            memberAddressDao.update(memberAddress);
        }
    }

    @Override
    public void doCreateCasting(Casting casting) {
        castingDao.save(casting);
        String msg = "WhaleLive ID:" + casting.getWhaleLiveId() + " [" + casting.getFullName() + "] has submitted Url for casting";
        BotClient botClient = new BotClient();
        botClient.doSendReportMessage(msg);
    }

    @Override
    public void doUpdateCasting(Casting casting) {
        Casting dbCasting = castingDao.getCastingByWhaleliveId(casting.getWhaleLiveId());
        dbCasting.setWhaleLiveId(casting.getWhaleLiveId());
        dbCasting.setFullName(casting.getFullName());
        dbCasting.setIdentityNo(casting.getIdentityNo());
        dbCasting.setPhoneNo(casting.getPhoneNo());
        dbCasting.setGender(casting.getGender());
        dbCasting.setEmail(casting.getEmail());
        dbCasting.setAge(casting.getAge());
        dbCasting.setTalent(casting.getTalent());
        dbCasting.setUrl(casting.getUrl());
        dbCasting.setTiktokId(casting.getTiktokId());
        dbCasting.setFbId(casting.getFbId());
        dbCasting.setIgId(casting.getIgId());
        dbCasting.setIgFan(casting.getIgFan());
        dbCasting.setLocation(casting.getLocation());
        dbCasting.setPhoneModel(casting.getPhoneModel());
        dbCasting.setTestLiveDate(casting.getTestLiveDate());
        dbCasting.setWeiboId(casting.getWeiboId());
        dbCasting.setTiktokFan(casting.getTiktokFan());
        dbCasting.setWeiboFan(casting.getWeiboFan());
        castingDao.update(dbCasting);
    }

    public void findCastForListing(DatagridModel<Casting> datagridModel, String whaleLiveId, Date createDateFrom, Date createDateTo) {
        castingDao.findCastForDatagrid(datagridModel, whaleLiveId, createDateFrom, createDateTo);
    }

    public void findCastForListingWithGuildId(DatagridModel<Casting> datagridModel, String guildId) {
        castingDao.findCastForListingWithGuildId(datagridModel, guildId);
    }

    @Override
    public ProfileNameLog doCreateProfileNameLog(String memberId, String oldProfileName, String username) {
        ProfileNameLog profileNameLog = new ProfileNameLog();

        profileNameLog.setMemberId(memberId);
        profileNameLog.setOldProfileName(oldProfileName);
        profileNameLog.setNewProfileName(username);
        profileNameLogDao.save(profileNameLog);

        return profileNameLog;
    }

    @Override
    public void doSendDifferentNotificationEmailType(String signingType, String guildId, String ccAddress, String bccAddress, String title, String body) {
        BroadcastService broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
        AgentService agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        Environment env = Application.lookupBean(Environment.class);
        BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(guildId);
        Agent agent = agentService.getAgent(broadcastGuild.getAgentId());
        String mailTo = "";

        switch (signingType.toUpperCase()) {
            case Global.EmailType.ADJUST_RANK:
                if (StringUtils.isNotBlank(broadcastGuild.getAgentId())) {
                    mailTo = agent.getEmail();
                }
                break;
            case Global.EmailType.CASTING:
                mailTo = env.getProperty("whalemedia.support.receiver");
                break;
            case Global.EmailType.SIGNING_GUILD:
                ccAddress = env.getProperty("whalemedia.support.receiver");

                if (StringUtils.isNotBlank(broadcastGuild.getAgentId())) {
                    mailTo = agent.getEmail();
                }

                break;
        }

        if (!mailTo.equalsIgnoreCase("")) {
            Emailq emailq = new Emailq(true);
            emailq.setEmailTo(mailTo);
            emailq.setEmailCc(ccAddress);
            emailq.setEmailBcc(bccAddress);
            emailq.setTitle(title);
            emailq.setBody(body);

            emailqDao.save(emailq);
        }
    }

    @Override
    public void doGenerateAgoraUidForAllUser() {
        List<Member> memberList = memberDao.findMemberWithoutAgoraUid();
        memberList.forEach(this::doGetMemberAgoraUid);
    }
}
