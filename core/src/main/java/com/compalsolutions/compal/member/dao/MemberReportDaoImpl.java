package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberReport;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(MemberReportDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberReportDaoImpl extends Jpa2Dao<MemberReport, String> implements MemberReportDao {
    public MemberReportDaoImpl() {
        super(new MemberReport(false));
    }

    @Override
    public void findMemberReportForDatagrid(DatagridModel<MemberReport> datagridModel, Date dateFrom, Date dateTo, String reportTypeId, String reportMemberId, Boolean isRead) {
        List<Object> params = new ArrayList<>();
        String hql = "SELECT r FROM MemberReport r INNER JOIN Member m on m.memberId = r.reportMemberId WHERE 1=1 ";

        if(dateFrom != null) {
            hql += " AND r.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if(dateTo != null) {
            hql += " AND r.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if(StringUtils.isNotBlank(reportTypeId)) {
            hql += " AND r.reportTypeId = ?";
            params.add(reportTypeId);
        }

        if(StringUtils.isNotBlank(reportMemberId)) {
            hql += " AND m.memberCode = ?";
            params.add(reportMemberId);
        }

        if(isRead != null) {
            hql += " AND r.readStatus = ?";
            params.add(isRead);
        }

        findForDatagrid(datagridModel, "r", hql, params.toArray());
    }
}
