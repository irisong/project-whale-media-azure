package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.MemberBlockDao;
import com.compalsolutions.compal.member.vo.Follower;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberBlock;
import com.compalsolutions.compal.member.vo.MemberFans;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Component(MemberBlockService.BEAN_NAME)
public class MemberBlockServiceImpl implements MemberBlockService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private MemberBlockDao memberBlockDao;

    @Override
    public void findMemberBlockForListing(DatagridModel<MemberBlock> datagridModel, String memberId) {
        memberBlockDao.findMemberBlockForDatagrid(datagridModel, memberId);
    }

    @Override
    public boolean isMemberBlocked(String memberId, String blockMemberId) {
        MemberBlock memberBlock = memberBlockDao.findMemberBlock(memberId, blockMemberId);
        if (memberBlock != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void doProcessMemberBlock(Locale locale, String action, String memberId, String blockMemberId) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        if (StringUtils.isBlank(action)) {
            throw new ValidatorException(i18n.getText("invalidAction", locale));
        }

        Member member = memberService.getMember(memberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        member = memberService.getMember(blockMemberId);
        if (member == null) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        if (action.equalsIgnoreCase(MemberBlock.ACTION_BLOCK)) {
            doAddMemberBlock(memberId, blockMemberId);
        } else if (action.equalsIgnoreCase(MemberBlock.ACTION_UNBLOCK)) {
            doRemoveMemberBlock(memberId, blockMemberId);
        } else {
            throw new ValidatorException(i18n.getText("invalidAction", locale));
        }
    }


    @Override
    public void doAddMemberBlock(String memberId, String blockMemberId) {
        synchronized (synchronizedObject) {
            if (memberId.equals(blockMemberId)) {
                throw new ValidatorException("Not allow to block own account");
            }

            MemberBlock memberBlock = memberBlockDao.findMemberBlock(memberId, blockMemberId);
            if (memberBlock == null) {
                memberBlock = new MemberBlock(true);
                memberBlock.setMemberId(memberId);
                memberBlock.setBlockMemberId(blockMemberId);
                memberBlockDao.save(memberBlock);
            }
        }
    }

    @Override
    public void doRemoveMemberBlock(String memberId, String blockMemberId) {
        synchronized (synchronizedObject) {
            MemberBlock memberBlock = memberBlockDao.findMemberBlock(memberId, blockMemberId);
            if (memberBlock != null) {
                memberBlockDao.delete(memberBlock);
            }
        }
    }
}
