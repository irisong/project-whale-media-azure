package com.compalsolutions.compal.member.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.compalsolutions.compal.member.vo.PlanbTree;

public interface PlanbTreeRepository extends JpaRepository<PlanbTree, String> {
    public static final String BEAN_NAME = "planbRepository";
}
