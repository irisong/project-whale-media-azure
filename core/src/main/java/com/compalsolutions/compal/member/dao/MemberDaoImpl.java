package com.compalsolutions.compal.member.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.repository.MemberRepository;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.util.DateUtil;

@Component(MemberDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberDaoImpl extends Jpa2Dao<Member, String> implements MemberDao {
    @SuppressWarnings("unused")
    @Autowired
    private MemberRepository memberRepository;

    public MemberDaoImpl() {
        super(new Member(false));
    }

    @Override
    public void findMembersForDatagrid(DatagridModel<Member> datagridModel, String agentId, String memberCode, Date joinDateFrom, Date joinDateTo, String status,String whaleLiveId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select m FROM Member m join MemberDetail b on m.memberDetId = b.memberDetId";

        if (StringUtils.isNotBlank(agentId)) {
            hql += " and m.agentId = ? ";
            params.add(agentId);
        }

        if (StringUtils.isNotBlank(memberCode)) {
            hql += " and m.memberCode like ? ";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(whaleLiveId)) {
            hql += " and b.whaleliveId like ? ";
            params.add(whaleLiveId + "%");
        }
        
        if (StringUtils.isNotBlank(status)) {
            hql += " and b.status = ? ";
            params.add(status);
        }

        if (joinDateFrom != null) {
            hql += " and m.joinDate >= ? ";
            params.add(DateUtil.truncateTime(joinDateFrom));
        }

        if (joinDateTo != null) {
            hql += " and m.joinDate <= ? ";
            params.add(DateUtil.formatDateToEndTime(joinDateTo));
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and m.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "m", hql, params.toArray());
    }

    @Override
    public void findGuildMembersForDatagrid(DatagridModel<Member> datagridModel, String guildId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "select m FROM Member m join BroadcastGuildMember b on m.memberId = b.memberId";
        if (StringUtils.isNotBlank(guildId)) {
            hql += " and b.guildId = ? ";
            params.add(guildId);
        }
        findForDatagrid(datagridModel, "m", hql, params.toArray());
    }

    @Override
    public void findMembersForKycDatagrid(DatagridModel<Member> datagridModel, String memberCode, String kycStatus,String whaleLiveId,Date dateFrom,Date dateTo){
        List<Object> params = new ArrayList<Object>();

        String hql = "select m "+
                        "FROM Member m " +
                        "join MemberDetail b on m.memberDetId = b.memberDetId " ;

        if (StringUtils.isNotBlank(memberCode)) {
            hql += " and m.memberCode like ? ";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(kycStatus)) {
            hql += " and m.kycStatus like ? ";
            params.add(kycStatus + "%");
        }

        if (StringUtils.isNotBlank(whaleLiveId)) {
            hql += " and b.whaleliveId like ? ";
            params.add(whaleLiveId + "%");
        }

        hql +=  " left join MemberFile f on m.memberId = f.memberId and f.type='IC_FRONT' ";
        hql += "Where 1 = 1 ";
        if (dateFrom != null) {
            hql += " and f.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and f.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "m", hql, params.toArray());
    }

    @Override
    public int findTotalMembers() {
        String hql = "SELECT COUNT(m.memberId) FROM Member m";

        Long result = (Long) exFindFirst(hql);
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public Member findMemberByMemberCode(String memberCode) {
        Validate.notBlank(memberCode);

        Member example = new Member(false);
        example.setMemberCode(memberCode);

        return findUnique(example);
    }

    @Override
    public Member findMemberByAgoraUid(int uid) {
        Member example = new Member(false);
        example.setAgoraUid(uid);

        return findUnique(example);
    }

    @Override
    public List<Member> findMemberWithoutAgoraUid() {
        Member example = new Member(false);
        example.setAgoraUid(null);

        return findByExample(example);
    }
    @Override
    public Member findMemberByChannelName(String channelName) {
        Member example = new Member(false);
        example.setChannelName(channelName);

        return findUnique(example);
    }

    @Override
    public Member findActiveMemberByMemberCode(String memberCode) {
        Validate.notBlank(memberCode);

        Member example = new Member(false);
        example.setMemberCode(memberCode);
        example.setStatus(Global.Status.ACTIVE);

        return findUnique(example);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Object[]> findMemberStatisticsReport() {
        String hql = "select m.rankId, count(m.memberId) from Member m where m.rankId is not null group by m.rankId";
        return (List<Object[]>) exFindQueryAsList(hql);
    }

    @Override
    public int findNoOfMembersByRank(String rankId) {
        String hql = "select count(m.memberId) from Member m where m.rankId=? ";
        Long result = (Long) exFindUnique(hql, rankId);

        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<Member> findAllMembers() {
        return findByExample(new Member(false), "memberCode");
    }

    @Override
    public void updateTotalSales(String memberId, BigDecimal totalSales) {
        List<Object> params = new ArrayList<>();

        String hql = "update Member m set m.totalSales=? where m.memberId=? ";

        params.add(totalSales);
        params.add(memberId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void updateVipStarRank(String memberId, int starVipRank) {
        List<Object> params = new ArrayList<>();

        String hql = "update Member m set m.vipStarRank=? where m.memberId=? ";

        params.add(starVipRank);
        params.add(memberId);

        bulkUpdate(hql, params.toArray());
    }

    @Override
    public Member findMemberByMemberDetId(String memberDetailId) {
        Validate.notBlank(memberDetailId);

        Member example = new Member(false);
        example.setMemberDetId(memberDetailId);

        return findUnique(example);
    }

    @Override
    public void doIncreaseExperience(String memberId, BigDecimal experience) {
        List<Object> params = new ArrayList<>();
        String hql = "update Member a set a.totalExp = a.totalExp + ? " //
                + " where a.memberId = ?";

        params.add(experience);
        params.add(memberId);
        bulkUpdate(hql, params.toArray());
    }

    @Override
    public void doDecreaseExperience(String memberId, BigDecimal experience) {
        List<Object> params = new ArrayList<>();
        String hql = "update Member a set a.totalExp = a.totalExp - ? " //
                + " where a.memberId = ?";

        params.add(experience);
        params.add(memberId);
        bulkUpdate(hql, params.toArray());
    }
}
