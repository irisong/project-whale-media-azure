package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.FansBadgeDao;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.telegram.BotClient;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(FansBadgeService.BEAN_NAME)
public class FansBadgeServiceImpl implements FansBadgeService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private FansBadgeDao fansBadgeDao;

    @Override
    public FansBadge getLatestFansBadgeRequested(String memberId, String status) {
        return fansBadgeDao.getLatestFansBadgeRequested(memberId, status);
    }

    @Override
    public boolean doCheckAllowChgBadgeAfterActive(String memberId, int minDaysAfterActive) {
        FansBadge currentActiveBadge = getLatestFansBadgeRequested(memberId, Global.Status.ACTIVE);
        if (currentActiveBadge != null) {
            //check not allow to change within 3 days after ACTIVE
            Date allowChgBadgeDate = DateUtils.addDays(new Date(), -minDaysAfterActive);
            if (allowChgBadgeDate.compareTo(currentActiveBadge.getActiveDatetime()) <= 0) { //not allow to change within 3 days
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    @Override
    public boolean doCheckAllowChgBadgeWithinTimePeriod(String memberId, int timePeriod, int maxTimesPerTimePeriod) {
        //check 365 days maximum change 3 times
        Date datePeriod = DateUtils.addDays(new Date(), -timePeriod);
        List<FansBadge> fansBadgeList = fansBadgeDao.getFansBadgeWithinDatePeriod(memberId, datePeriod);
        if (fansBadgeList.size() >= maxTimesPerTimePeriod) {
            fansBadgeList.clear();
            fansBadgeList = null;
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void doProcessCreateFansBadge(Locale locale, String memberId, String fansBadgeName) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedObject) {
            /************************
             * VERIFICATION - START
             ************************/
            Member member = memberService.getMember(memberId);
            MemberDetail memberDetail = memberService.findMemberDetailByMemberId(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            if (StringUtils.isBlank(fansBadgeName)) {
                throw new ValidatorException(i18n.getText("invalidBadgeNickname", locale));
            }

            boolean allowChange = false;
            //already submit once and in PENDING status
            FansBadge latestPending = getLatestFansBadgeRequested(memberId, Global.Status.PENDING);
            if (latestPending == null) {
                allowChange = true;
            }

            if (allowChange) {
//                //check ACTIVE within 10 days
//                allowChange = doCheckAllowChgBadgeAfterActive(memberId, 10);
            } else {
                throw new ValidatorException(i18n.getText("nicknamePendingNotAllowSubmit", locale, 10));
            }

//            if (allowChange) {
//                //check 365 days maximum change 3 times
//                allowChange = doCheckAllowChgBadgeWithinTimePeriod(memberId, 365, 3);
//            } else {
//                throw new ValidatorException(i18n.getText("notAllowChgWithinTimePeriod", locale, 10));
//            }
//
//            if (!allowChange) {
//                throw new ValidatorException(i18n.getText("exceedMaxChgFansBadgePerPeriod", locale, 3, 365));
//            }

            if (!isFansBadgeAvailable(fansBadgeName)) {
                throw new ValidatorException(i18n.getText("fansBadgeAlreadyUsed", locale));
            }
            /************************
             * VERIFICATION - END
             ************************/


            FansBadge fansBadge = new FansBadge(true);
            fansBadge.setFansBadgeName(fansBadgeName);
            fansBadge.setMemberId(memberId);
            fansBadgeDao.save(fansBadge);

            String msg = member.getMemberCode() + " [" + memberDetail.getProfileName() + "] applied fans badge name as [" + fansBadgeName + "]";
            BotClient botClient = new BotClient();
            botClient.doSendReportMessage(msg);
        }
    }

    @Override
    public boolean isFansBadgeAvailable(String fansBadgeName) {
        FansBadge fansBadge = fansBadgeDao.getActiveFansBadgeByName(fansBadgeName);
        if (fansBadge != null) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void findFansBadgesForListing(DatagridModel<FansBadge> datagridModel, String memberCode, Date createDateFrom, Date createDateTo, String status) {
        fansBadgeDao.findFansBadgesForDatagrid(datagridModel, memberCode, createDateFrom, createDateTo, status);
    }

    @Override
    public void doApproveFansBadge(Locale locale, String fansBadgeId) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        FansBadge fansBadge = fansBadgeDao.get(fansBadgeId);
        if (fansBadge != null) {
            if (!isFansBadgeAvailable(fansBadge.getFansBadgeName())) {
                throw new ValidatorException(i18n.getText("fansBadgeAlreadyUsed", locale));
            }

            FansBadge lastActiveFansBadge = fansBadgeDao.getLatestFansBadgeRequested(fansBadge.getMemberId(), Global.Status.ACTIVE);
            if (lastActiveFansBadge != null) {
                lastActiveFansBadge.setStatus(Global.Status.INACTIVE);
                fansBadgeDao.update(lastActiveFansBadge);
            }

            fansBadge.setStatus(Global.Status.ACTIVE);
            fansBadge.setActiveDatetime(new Date());
            fansBadgeDao.update(fansBadge);
        } else {
            throw new ValidatorException(i18n.getText("fansBadgeNotExists", locale));
        }

        Member member = memberService.getMember(fansBadge.getMemberId());
        member.setFansBadgeName(fansBadge.getFansBadgeName());
        memberService.updateMember(member);
      //  doProcessAddFanWithSpecialGift(fansBadge);
        doProcessFansBadgeNotification(fansBadge);
    }

    @Override
    public void doRejectFansBadge(Locale locale, String fansBadgeId, String remark) {
        I18n i18n = Application.lookupBean(I18n.class);

        FansBadge fansBadge = fansBadgeDao.get(fansBadgeId);
        if (fansBadge != null) {
            fansBadge.setStatus(Global.Status.REJECTED);
            fansBadge.setRejectDatetime(new Date());
            fansBadge.setRemark(remark);
            fansBadgeDao.update(fansBadge);

            doProcessFansBadgeNotification(fansBadge);
        } else {
            throw new ValidatorException(i18n.getText("fansBadgeNotExists", locale));
        }
    }

    @Override
    public void doProcessAddFanWithSpecialGift(FansBadge fansBadge) {
        PointService pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
        MemberFansService memberFansService = Application.lookupBean(MemberFansService.BEAN_NAME, MemberFansService.class);

        String whaleCardId = pointService.findWhaleCard();
        List<String> memberList = pointService.findPointTrxByMemberAndSymbolId(fansBadge.getMemberId(), whaleCardId);
        for (int i = 0; i < memberList.size(); i++) {
            memberFansService.doAddMemberFan(new Locale("ENGLISH"), fansBadge.getMemberId(), memberList.get(i));
        }
        memberList.clear();
        memberList = null;
    }

    @Override
    public String getFansBadgeName(String memberId) {
        FansBadge fansBadge = fansBadgeDao.getFansBadgeName(memberId);
        if (fansBadge != null) {
            return fansBadge.getFansBadgeName();
        }

        return "";
    }

    @Override
    public FansBadge getFanBadge(String fansBadgeId) {
        return fansBadgeDao.get(fansBadgeId);
    }

    @Override
    public void doProcessFansBadgeNotification(FansBadge fansBadge) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        Member member = memberService.getMember(fansBadge.getMemberId());
        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.FANS_BADGE_REQUEST);

        NotificationAccount frmAcct = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        NotificationAccount toAcct = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);
        if (frmAcct != null && toAcct != null) {
            String content = i18n.getText("fansBadgeRequestApproved", new Locale(member.getPreferLanguage()), fansBadge.getFansBadgeName());
            if (fansBadge.getStatus().equalsIgnoreCase(Global.Status.REJECTED)) {
                content = i18n.getText("fansBadgeRequestRejected", new Locale(member.getPreferLanguage()), fansBadge.getFansBadgeName(), fansBadge.getRemark());
            }

            notificationConfProvider.doSendMessage(frmAcct.getAccountId(), toAcct.getAccountId(), Global.AccessModule.WHALE_MEDIA, content, payload, null);
        }
    }
}
