package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.ProfileNameLog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(ProfileNameLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProfileNameLogDaoImpl extends Jpa2Dao<ProfileNameLog, String> implements ProfileNameLogDao {

    public ProfileNameLogDaoImpl() {
        super(new ProfileNameLog());
    }
}
