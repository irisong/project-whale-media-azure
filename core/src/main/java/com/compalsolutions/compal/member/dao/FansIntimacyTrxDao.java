package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.FansIntimacyTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface FansIntimacyTrxDao extends BasicDao<FansIntimacyTrx, String> {
    public static final String BEAN_NAME = "fansIntimacyTrxDao";

    public List<FansIntimacyTrx> isFirstTimeGain(String userMemberId, String channelMember, Date entryDate, String trxType);

    public BigDecimal findTodayIntimacyValue(String fansMemberId, String memberId, String trxType);

    public BigDecimal findLackInteractiveRecord(String fansMemberId, String memberId);
}
