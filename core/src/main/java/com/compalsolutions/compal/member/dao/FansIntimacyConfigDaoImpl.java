package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(FansIntimacyConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FansIntimacyConfigDaoImpl extends Jpa2Dao<FansIntimacyConfig, String> implements FansIntimacyConfigDao {

    public FansIntimacyConfigDaoImpl() {
        super(new FansIntimacyConfig(false));
    }

    @Override
    public FansIntimacyConfig getFansIntimacyConfigByLevel(int fanLevel){
        FansIntimacyConfig example = new FansIntimacyConfig(false);
        example.setFansLevel(fanLevel);
        return findUnique(example);
    }

    @Override
    public int findTotalValueNeedForLevel(int level){
        String hql = "select sum(d.levelUpValue) from FansIntimacyConfig d where d.fansLevel <= ? ";
        List<Object> params = new ArrayList<>();
        params.add(level);

        Number result = (Number) exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();
        else
            return 0;
    }

    @Override
    public void findFansIntimacyForDatagrid(DatagridModel<FansIntimacyConfig> datagridModel) {
        String hql = "from f IN " + FansIntimacyConfig.class;

        findForDatagrid(datagridModel, "f", hql);
    }

    @Override
    public List<FansIntimacyConfig> getAllFansIntimacyConfig() {
        String hql = "from f IN " + FansIntimacyConfig.class;
        return findQueryAsList(hql);
    }
}
