package com.compalsolutions.compal.member.dto;

import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

public class MemberDto implements Serializable {
    private static final long serialVersionUID = 1L;

    private String memberId;
    private String memberCode;
    private BigDecimal totalSales;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public BigDecimal getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(BigDecimal totalSales) {
        this.totalSales = totalSales;
    }

    public String getMemberCodeHide() {
        return this.replaceLastCharacter(memberCode);
    }

    private String replaceLastCharacter(String memberCode) {
        if (StringUtils.isNotBlank(memberCode)) {
            int length = memberCode.length();
            if (length < 5) {
                return "******";
            }
            return memberCode.substring(0, 4) + "****" + memberCode.substring(length - 4, length);
        }
        return "******";
    }
}
