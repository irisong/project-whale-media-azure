package com.compalsolutions.compal.member.service;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.SponsorTree;

public interface SponsorService {
    public static final String BEAN_NAME = "sponsorService";

    public SponsorTree findSponsorTreeByMemberId(String memberId);

    public void saveSponsorTree(SponsorTree sponsorTree);

    public void doParseSponsorTree(SponsorTree sponsorTree);

    public void doChangeSponsorship(Locale locale, String memberId, String newSponsorCode);

    public boolean isSameGroup(String uplineMemberId, String downlineMemberId);

    public int getNoOfDirectDownlines(String memberId);

    public int getNoOfDownlines(String memberId);

    public void updateSponsorTree(SponsorTree sponsorTree);

    public List<SponsorTree> findDirectDownlinesForSponsorTreeAction(String memberId, int startIndex, int size);

    public SponsorTree findSponsorTreeWithNoOfDownlines(String memberId);

    public void findDirectDownlineMembersForListing(DatagridModel<Member> datagridModel, String memberId);

    public Member findFirstDirectDownline(String uplineId);

    public int getNoOfVipDownlines(String memberId);
}
