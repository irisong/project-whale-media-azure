package com.compalsolutions.compal.member.vo;

public class TreeConstant {
    public static final int TREE_STYLE_NOPE = -1;
    public static final int TREE_STYLE_POS_FIXED = 0;
    public static final int TREE_STYLE_PREFER_PLACE = 1;
    public static final int TREE_STYLE_AUTO_FULL_TREE = 2;
    public static final int TREE_STYLE_FAR_LEFT = 3;
    public static final int TREE_STYLE_FAR_RIGHT = 4;
    public static final int TREE_STYLE_BALANCED_LEFT_RIGHT = 5;
    // public static final int TREE_STYLE_SOLAR = 6;

    public static final String TREE_PARSED = "Y";
    public static final String TREE_UNPARSE = "N";
    public static final String TREE_PREPARSE = "P";

    public static final String NO_INTRODUCER = "NOPE";
    public static final String NO_PLACEMENT = "NOPE";
    public static final String AUTO_INTRODUCER = "AUTO";
    public static final String AUTO_PLACEMENT = "AUTO";

    public static final String TREE_TYPE_VERTICAL_VIEW = "0";
    public static final String TREE_TYPE_GENEALOGY_VIEW = "1";
    public static final String TREE_TYPE_TEXT_VIEW = "2";
    public static final String TREE_TYPE_TREE_VIEW = "3";

    public static final String topNodeId = "0000000"; // root

    public static final String NODE_ROOT = "R";
    public static final String NODE_NORMAL = "N";
    public static final String NODE_BLOCK = "B";
    public static final String NODE_DISABLE = "D";

    public static final SolarNode[] EMPTY_SOLAR = new SolarNode[0];
    public static final BinaryNode[] EMPTY_BINARY = new BinaryNode[0];

    public static String getPositionName(String pos) {
        if (pos == null)
            return "";

        if (pos.equals("L")) {
            // return MessageResource.getI18nMessage("PLACE_LEFT");
            return "L";
        } else if (pos.equals("R")) {
            // return MessageResource.getI18nMessage("PLACE_RIGHT");
            return "R";
        } else if (pos.equals("M")) {
            // return MessageResource.getI18nMessage("PLACE_MIDDLE");
            return "M";
        } else
            return "";
    }

}
