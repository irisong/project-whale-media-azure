package com.compalsolutions.compal.member.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.MemberFileRepository;
import com.compalsolutions.compal.member.vo.MemberFile;

import java.util.*;

@Component(MemberFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberFileDaoImpl extends Jpa2Dao<MemberFile, String> implements MemberFileDao {

    @SuppressWarnings("unused")
    @Autowired
    private MemberFileRepository memberFileRepository;

    public MemberFileDaoImpl() {
        super(new MemberFile(false));
    }

    @Override
    public MemberFile findByMemberIdAndType(String memberId, String type) {
        if (StringUtils.isBlank(memberId))
            return null;

        MemberFile example = new MemberFile(false);
        example.setMemberId(memberId);
        example.setType(type);
        return findFirst(example);
    }

    @Override
    public List<MemberFile> findMemberFilesByMemberId(String memberId) {
        if(StringUtils.isBlank(memberId))
            return null;

        MemberFile example = new MemberFile(false);
        example.setMemberId(memberId);

        return findByExample(example);
    }




}
