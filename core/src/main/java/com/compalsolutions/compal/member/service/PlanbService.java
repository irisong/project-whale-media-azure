package com.compalsolutions.compal.member.service;

import java.util.List;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.PlanbTree;

public interface PlanbService {
    public static final String BEAN_NAME = "planbService";

    public PlanbTree getPlanbTree(Long planbTreeId);

    public void savePlanbTree(PlanbTree planbTree);

    public void doParsePlanbTree(PlanbTree planbTree);

    public PlanbTree findPlanbTree(String memberId, int unit);

    public List<PlanbTree> findDirectDownlinesForPlanbTreeAction(String memberId, int unit);
}
