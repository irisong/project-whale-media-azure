package com.compalsolutions.compal.member.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.jdbc.SingleRowMapper;

@Component(PlanbTreeSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PlanbTreeSqlDaoImpl extends AbstractJdbcDao implements PlanbTreeSqlDao {

    @Override
    public List<Integer> getNoOfDownlinesGroupByLevel(String traceKey) {
        String sql = "select level, count(level) as cnt \n" + //
                "from mb_planb_tree where tracekey like ? and tracekey != ? \n" + //
                "group by level\n" + //
                "order by level ";

        return query(sql, new RowMapper<Integer>() {
            @Override
            public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {
                return rs.getInt("cnt");
            }
        }, traceKey + "%", traceKey);
    }

    /**
     *
     * @param traceKey
     * @param topLevel
     * @param nextDownlineLevel
     * @return Triple (parentId, parentUnit, noOfDirectDownlines
     */
    @Override
    public Triple<String, Integer, Integer> findUplineAndNoOfDirectDownlinesForFreeSlot(String traceKey, int topLevel, int nextDownlineLevel) {
        String sql = "select parent, parentunit, parent_tracekey, sum(isexist) as cnt\n" + //
                "from\n" + //
                "(\n" + //
                "    select p.parent, p.parentunit, p.parent_tracekey, case when m.unit is null then 0 else 1 end isexist\n" + //
                "    from \n" + //
                "    (\n" + //
                "        select member_id as parent, unit as parentunit, tracekey as parent_tracekey\n" + //
                "        from mb_planb_tree where tracekey like ? and level=?\n" + //
                "    ) p left join mb_planb_tree m on p.parent = m.parent_id and p.parentunit=m.parent_unit and m.level=?\n" + //
                ") downline\n" + //
                "group by parent, parentunit, parent_tracekey\n" + //
                "having sum(isexist) < 3\n" + //
                "order by parent_tracekey limit 1";

        List<Object> params = new ArrayList<>();
        params.add(traceKey + "%");
        params.add(topLevel + nextDownlineLevel - 1);
        params.add(topLevel + nextDownlineLevel);

        return queryForSingleRow(sql, new SingleRowMapper<Triple<String, Integer, Integer>>() {
            @Override
            public Triple<String, Integer, Integer> onMapRow(ResultSet resultSet, int rowNum) throws SQLException {
                return new ImmutableTriple<>(resultSet.getString("parent"), resultSet.getInt("parentunit"), resultSet.getInt("cnt"));
            }
        }, params.toArray());
    }
}
