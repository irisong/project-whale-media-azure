package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.ProfileNameLog;

public interface ProfileNameLogDao extends BasicDao<ProfileNameLog, String> {
    public static final String BEAN_NAME = "profileNameLogDao";

}
