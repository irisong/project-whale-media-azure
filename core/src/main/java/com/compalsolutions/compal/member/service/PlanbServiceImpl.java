package com.compalsolutions.compal.member.service;

import java.util.List;

import org.apache.commons.lang3.tuple.Triple;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.member.dao.PlanbTreeDao;
import com.compalsolutions.compal.member.dao.PlanbTreeSqlDao;
import com.compalsolutions.compal.member.vo.BaseNode;
import com.compalsolutions.compal.member.vo.PlanbTree;
import com.compalsolutions.compal.member.vo.TreeConstant;

@Component(PlanbService.BEAN_NAME)
public class PlanbServiceImpl implements PlanbService {
    private PlanbTreeDao planbTreeDao;

    private PlanbTreeSqlDao planbTreeSqlDao;

    // downline nodes size: 3, 9, 27 ....
    public static final int[] DOWNLINE_NODE_SIZES = new int[] { //
            (int) Math.pow(3, 1), //
            (int) Math.pow(3, 2), //
            (int) Math.pow(3, 3), //
            (int) Math.pow(3, 4), //
            (int) Math.pow(3, 5), //
            (int) Math.pow(3, 6), //
            (int) Math.pow(3, 7), //
            (int) Math.pow(3, 8), //
            (int) Math.pow(3, 9), //
            (int) Math.pow(3, 10), //
            (int) Math.pow(3, 11), //
            (int) Math.pow(3, 12), //
            (int) Math.pow(3, 13), //
            (int) Math.pow(3, 14), //
            (int) Math.pow(3, 15), //
            (int) Math.pow(3, 16), //
            (int) Math.pow(3, 17), //
            (int) Math.pow(3, 18), //
            (int) Math.pow(3, 19), //
            (int) Math.pow(3, 20), //
            (int) Math.pow(3, 21), //
            (int) Math.pow(3, 22), //
            (int) Math.pow(3, 23), //
            (int) Math.pow(3, 24), //
            (int) Math.pow(3, 25), //
            (int) Math.pow(3, 26), //
            (int) Math.pow(3, 27), //
            (int) Math.pow(3, 28), //
            (int) Math.pow(3, 29), //
            (int) Math.pow(3, 30), //
            (int) Math.pow(3, 31) //
    };

    @Autowired
    public PlanbServiceImpl(PlanbTreeDao planbTreeDao, PlanbTreeSqlDao planbTreeSqlDao) {
        this.planbTreeDao = planbTreeDao;
        this.planbTreeSqlDao = planbTreeSqlDao;
    }

    @Override
    public PlanbTree getPlanbTree(Long planbTreeId) {
        return planbTreeDao.get(planbTreeId);
    }

    @Override
    public void savePlanbTree(PlanbTree planbTree) {
        planbTreeDao.save(planbTree);
    }

    @Override
    public void doParsePlanbTree(PlanbTree planbTree) {
        String lr = null;
        String b32 = Long.toString(planbTree.getId(), 32); // get based 32 number
        planbTree.setB32(b32);

        PlanbTree upline = null;

        switch (planbTree.getTreeStyle()) {
        case TreeConstant.TREE_STYLE_AUTO_FULL_TREE:
            BaseNode freeSlot = findFreeSlotForAutoFullTree(planbTree.getTempId(), planbTree.getTempUnit(), planbTree.getTempPosition());
            upline = findPlanbTree(freeSlot.getParentId(), freeSlot.getParentUnit());
            planbTree.setPosition(freeSlot.getPosition());

            if (upline == null)
                throw new ValidatorException("upline is null for TreeConstant.TREE_STYLE_AUTO_FULL_TREE");

            break;
        default:
            throw new ValidatorException("no tree style selected for planb tree");
        }

        lr = "L".equalsIgnoreCase(planbTree.getPosition()) ? "<" : ">";
        if ("M".equalsIgnoreCase(planbTree.getPosition()))
            lr = "=";

        planbTree.setParentId(upline.getMemberId());
        planbTree.setParentUnit(upline.getUnit());
        planbTree.setLevel(upline.getLevel() + 1);
        planbTree.setTraceKey(upline.getTraceKey() + lr + b32);
        planbTree.setTreePath(upline.getTreePath() + "-" + planbTree.getPosition());
        planbTree.setParseTree("Y");

        planbTreeDao.update(planbTree);
    }

    private BaseNode findFreeSlotForAutoFullTree(String parentId, int parentUnit, String position) {
        PlanbTree topPlanbTree = findPlanbTree(parentId, parentUnit);

        if (topPlanbTree == null) {
            throw new ValidatorException("topPlanbTree is null");
        }

        // get no of downlines for each level
        List<Integer> downlinesByLevel = planbTreeSqlDao.getNoOfDownlinesGroupByLevel(topPlanbTree.getTraceKey());

        // based on no of downlines for each level, find out which level still have free slot
        int freeSlotLevel = findFreeSlotForAutoFullTree_getFreeSlotLevel(downlinesByLevel);

        // Triple<parentId, parentUnit, noOfDirectDownlines>
        Triple<String, Integer, Integer> parentTriple = planbTreeSqlDao.findUplineAndNoOfDirectDownlinesForFreeSlot(topPlanbTree.getTraceKey(),
                topPlanbTree.getLevel(), freeSlotLevel);

        PlanbTree freeSlotParent = findPlanbTree(parentTriple.getLeft(), parentTriple.getMiddle());
        if (freeSlotParent == null) {
            throw new ValidatorException("freeSlotParent is null");
        }

        List<String> childPositions = planbTreeDao.findChildNodePositionsByParentAndParentUnit(freeSlotParent.getMemberId(), freeSlotParent.getUnit());

        String pos = null;
        if (!childPositions.contains("L")) {
            pos = "L";
        } else if (!childPositions.contains("M")) {
            pos = "M";
        } else {
            pos = "R";
        }

        BaseNode node = new BaseNode();
        node.setParentId(freeSlotParent.getMemberId());
        node.setParentUnit(freeSlotParent.getUnit());
        node.setPosition(pos);
        return node;
    }

    private int findFreeSlotForAutoFullTree_getFreeSlotLevel(List<Integer> downlinesByLevel) {
        int nextLevel = -1;

        for (int index = 0; index < downlinesByLevel.size(); index++) {
            int noOfDownlines = downlinesByLevel.get(index);

            if (noOfDownlines < DOWNLINE_NODE_SIZES[index]) {
                nextLevel = index + 1;
                break;
            } else if (noOfDownlines > DOWNLINE_NODE_SIZES[index]) {
                throw new ValidatorException("downlines for level " + (index + 1) + " is larger than " + DOWNLINE_NODE_SIZES[index]);
            }
        }

        // if the last level is full
        if (nextLevel == -1) {
            nextLevel = downlinesByLevel.size() + 1;
        }
        return nextLevel;
    }

    public PlanbTree findPlanbTree(String memberId, int unit) {
        return planbTreeDao.findPlanbTree(memberId, unit);
    }

    @Override
    public List<PlanbTree> findDirectDownlinesForPlanbTreeAction(String memberId, int unit) {
        return planbTreeDao.findDirectDownlines(memberId, unit);
    }
}
