package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "mb_member_report_file")
@Access(AccessType.FIELD)
public class MemberReportFile extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "file_id", unique = true, length = 32, nullable = false)
    private String fileId;

    @Column(name = "report_id", length = 32, nullable = false)
    private String reportId;

    @ManyToOne
    @JoinColumn(name = "report_id", insertable = false, updatable = false)
    private MemberReport memberReport;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "sort_order", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer sortOrder;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    @Transient
    private byte[] data;

    public MemberReportFile() {
    }

    public MemberReportFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public MemberReportFile(byte[] data, String contentType, String filename, Long fileSize, int sortOrder) {
        this.data = data;
        this.contentType = contentType;
        this.filename = filename;
        this.fileSize = fileSize;
        this.sortOrder = sortOrder;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public MemberReport getMemberReport() {
        return memberReport;
    }

    public void setMemberReport(MemberReport memberReport) {
        this.memberReport = memberReport;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(fileId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = fileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
