package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.VipStarRank;

public interface VipStarRankDao extends BasicDao<VipStarRank, String> {
    public static final String BEAN_NAME = "vipStarRankDao";


}
