package com.compalsolutions.compal.member.dao;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.repository.PlacementTreeRepository;
import com.compalsolutions.compal.member.vo.PlacementTree;

@Component(PlacementTreeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PlacementTreeDaoImpl extends Jpa2Dao<PlacementTree, String> implements PlacementTreeDao {
    @SuppressWarnings("unused")
    @Autowired
    private PlacementTreeRepository placementTreeRepository;

    public PlacementTreeDaoImpl() {
        super(new PlacementTree(false));
    }

    @Override
    public PlacementTree findPlacement(String memberId, int unit) {
        if (StringUtils.isBlank(memberId))
            return null;

        PlacementTree example = new PlacementTree(false);
        example.setMemberId(memberId);
        example.setUnit(unit);

        return findFirst(example);
    }

    @Override
    public PlacementTree findPlacementTreeByParentIdAndParentUnitAndParentPosition(String parentId, int parentUnit, String parentPosition) {
        if (StringUtils.isBlank(parentId))
            return null;

        if (StringUtils.isBlank(parentPosition))
            return null;

        PlacementTree example = new PlacementTree(false);
        example.setParentId(parentId);
        example.setParentUnit(parentUnit);
        example.setPosition(parentPosition);

        return findFirst(example);
    }
}
