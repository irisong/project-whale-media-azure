package com.compalsolutions.compal.member.dao;

import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.member.repository.PlanbTreeRepository;
import com.compalsolutions.compal.member.vo.PlanbTree;

@Component(PlanbTreeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PlanbTreeDaoImpl extends Jpa2Dao<PlanbTree, Long> implements PlanbTreeDao {
    @SuppressWarnings("unused")
    @Autowired
    private PlanbTreeRepository planbTreeRepository;

    public PlanbTreeDaoImpl() {
        super(new PlanbTree(false));
    }

    @Override
    public PlanbTree findPlanbTree(String memberId, int unit) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be null for findPlanbTree");
        }

        PlanbTree example = new PlanbTree(false);
        example.setMemberId(memberId);
        example.setUnit(unit);

        return findUnique(example);
    }

    @Override
    public List<String> findChildNodePositionsByParentAndParentUnit(String parentId, int parentUnit) {
        String hql = "select t.position from PlanbTree as t where t.parentId=? and t.parentUnit=? order by t.position";

        @SuppressWarnings("unchecked")
        List<Object> result = (List<Object>) exFindQueryAsList(hql, parentId, parentUnit);
        return result.stream().map(obj -> (String) obj).collect(Collectors.toList());
    }

    @Override
    public List<PlanbTree> findDirectDownlines(String memberId, int unit) {
        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException("memberId can not be null for findDirectDownlines");
        }

        PlanbTree example = new PlanbTree(false);
        example.setParentId(memberId);
        example.setParentUnit(unit);
        example.setStatus(Global.Status.ACTIVE);

        return findByExample(example, "position");
    }
}
