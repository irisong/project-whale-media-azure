package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.member.vo.Follower;

import java.util.Date;
import java.util.List;

public interface FansBadgeDao extends BasicDao<FansBadge, String> {
    public static final String BEAN_NAME = "fansBadgeDao";

    public FansBadge getLatestFansBadgeRequested(String memberId, String status);

    public FansBadge getActiveFansBadgeByName(String fansBadgeName);

    public List<FansBadge> getFansBadgeWithinDatePeriod(String memberId, Date datePeriod);

    public void findFansBadgesForDatagrid(DatagridModel<FansBadge> datagridModel, String memberCode, Date createDateFrom, Date createDateTo, String status);

    public FansBadge getFansBadgeName(String memberId);
}
