package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.SearchHistory;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(SearchHistoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SearchHistoryDaoImpl extends Jpa2Dao<SearchHistory, String> implements SearchHistoryDao {

    public SearchHistoryDaoImpl() {
        super(new SearchHistory());
    }

    @Override
    public SearchHistory findSearchHistory(String memberId, String keyword) {
        SearchHistory searchHistory = new SearchHistory();
        searchHistory.setMemberId(memberId);
        searchHistory.setKeyword(keyword);

        return findUnique(searchHistory);
    }

    @Override
    public List<SearchHistory> findAllSearchHistoryByMemberId(String memberId) {
        Validate.notBlank(memberId);

        SearchHistory example = new SearchHistory();
        example.setMemberId(memberId);

        return findByExample(example, "datetimeUpdate desc");
    }
}
