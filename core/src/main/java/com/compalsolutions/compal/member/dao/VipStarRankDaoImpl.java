package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.VipStarRank;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(VipStarRankDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class VipStarRankDaoImpl extends Jpa2Dao<VipStarRank, String> implements VipStarRankDao {
    public VipStarRankDaoImpl() {
        super(new VipStarRank(false));
    }
}
