package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_live_stream_follower")
@Access(AccessType.FIELD)
public class Follower extends VoBase {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @ToTrim
    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ToTrim
    @Column(name = "follower_id", nullable = false, length = 32)
    private String followerId;

    @ToTrim
    @Column(name = "status")
    private String status;

    @Transient
    private Integer count;

    public Follower() {
    }

    public Follower(boolean defaultValue) {
        if (defaultValue) {
            this.status = Global.Status.ACTIVE;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFollowerId() {
        return followerId;
    }

    public void setFollowerId(String followerId) {
        this.followerId = followerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
