package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.FansBadge;

import java.util.Date;
import java.util.Locale;

public interface FansBadgeService {
    public static final String BEAN_NAME = "fansBadgeService";

    public FansBadge getLatestFansBadgeRequested(String memberId, String status) ;

    public boolean doCheckAllowChgBadgeAfterActive(String memberId, int minDaysAfterActive);

    public boolean doCheckAllowChgBadgeWithinTimePeriod(String memberId, int timePeriod, int maxTimesPerTimePeriod);

    public void doProcessCreateFansBadge(Locale locale, String memberId, String fansBadgeName);

    public boolean isFansBadgeAvailable(String fansBadgeName);

    public void findFansBadgesForListing(DatagridModel<FansBadge> datagridModel, String memberCode, Date createDateFrom, Date createDateTo, String status);

    public void doApproveFansBadge(Locale locale, String fansBadgeId);

    public void doRejectFansBadge(Locale locale, String fansBadgeId, String remark);

    public void doProcessFansBadgeNotification(FansBadge fansBadge);

    public void doProcessAddFanWithSpecialGift(FansBadge fansBadge);

    public String getFansBadgeName(String memberId);

    public FansBadge getFanBadge(String fansBadgeId);
}
