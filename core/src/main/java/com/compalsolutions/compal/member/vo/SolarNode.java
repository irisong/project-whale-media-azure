package com.compalsolutions.compal.member.vo;

public interface SolarNode extends NetworkTreeNode {
    public int getTotalDownlines();

    public void setTotalDownlines(int totalDownlines);
}
