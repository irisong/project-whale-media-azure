package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.member.vo.FansIntimacyTrx;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Component(FansIntimacyTrxSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FansIntimacyTrxSqlDaoImpl extends AbstractJdbcDao implements FansIntimacyTrxSqlDao {

    @Override
    public List<HashMap<String, String>> findReduceIntimacyMember(Date dateFrom, Date dateTo, int minimumIntimacy) {

        String sql =
                  // having intimacy less than 50 within 5 days
                  "SELECT f.member_id, f.fans_member_id, f.last_intimacy_gain, COALESCE(t.total_intimacy,0) AS total_intimacy, "
                + "            case when f.intimacy_value = 1 then -1 else ROUND( ((f.intimacy_value/100 * c.deduct_percent)*-1) ,0) end AS reduce_intimacy, "
                + "            case when f.intimacy_value = 1 then 0 else ROUND( (f.intimacy_value/100 * (100-c.deduct_percent)),0) end AS intimacy_after_deduct "
                + "FROM mb_member_fans f "
                + "JOIN mb_fans_intimacy_config c ON (c.fans_level = f.fans_level) "
                + "JOIN ( "
                + "        select member_id, fans_member_id, sum(COALESCE(value,0)) AS total_intimacy "
                + "        from mb_fans_intimacy_trx "
                + "        where trx_datetime >= ? and trx_datetime <= ? "
                + "    	   and trx_type <> ? "
                + "        group by member_id, fans_member_id "
                + "        having total_intimacy < ? "
                + "    ) t ON t.member_id = f.member_id AND t.fans_member_id = f.fans_member_id "
                + "WHERE f.join_date <= ? " //already became member for more than minimumIntimacy days (5days)
                + "UNION "
                  // dont have trx record within 5 days
                + "SELECT f.member_id, f.fans_member_id, f.last_intimacy_gain, 0 AS total_intimacy, "
                + "            case when f.intimacy_value = 1 then -1 else ROUND( ((f.intimacy_value/100 * c.deduct_percent)*-1) ,0) end AS reduce_intimacy, "
                + "            case when f.intimacy_value = 1 then 0 else ROUND( (f.intimacy_value/100 * (100-c.deduct_percent)),0) end AS intimacy_after_deduct "
                + "FROM mb_member_fans f "
                + "JOIN mb_fans_intimacy_config c ON (c.fans_level = f.fans_level) "
                + "WHERE f.join_date <= ? " //already became member for more than minimumIntimacy days (5days)
                + "AND (f.last_intimacy_gain is null OR f.last_intimacy_gain < ?) ";

//        String sql = "SELECT t.member_id, t.fans_member_id, t.total_intimacy, " +
//                "            case when f.intimacy_value = 1 then -1 else ROUND( ((f.intimacy_value/100 * c.deduct_percent)*-1) ,0) end AS reduce_intimacy, " +
//                "            case when f.intimacy_value = 1 then 0 else ROUND( (f.intimacy_value/100 * (100-c.deduct_percent)),0) end AS intimacy_after_deduct " +
//                "    FROM " +
//                "    ( " +
//                "        select member_id, fans_member_id, sum(COALESCE(value,0)) AS total_intimacy " +
//                "        from mb_fans_intimacy_trx " +
//                "        where trx_datetime >= ? and trx_datetime <= ? " +
//                "        group by member_id, fans_member_id " +
//                "        having total_intimacy < ? " +
//                "    ) t " +
//                "    JOIN mb_member_fans f ON (f.member_id = t.member_id AND f.fans_member_id = t.fans_member_id AND f.join_date <= ?) " +
//                "    JOIN mb_fans_intimacy_config c ON (c.fans_level = f.fans_level) ";

        List<Object> params = new ArrayList<>();
        params.add(dateFrom);
        params.add(dateTo);
        params.add(FansIntimacyTrx.TYPE_LACK_OF_INTERACT);
        params.add(minimumIntimacy);
        params.add(dateFrom);
        params.add(dateFrom);
        params.add(dateFrom);

        return query(sql, (rs, rowNum) -> {
            HashMap<String, String> hashMap = new HashMap<>();
            hashMap.put("member_id", rs.getString("member_id"));
            hashMap.put("fans_member_id", rs.getString("fans_member_id"));
            hashMap.put("total_intimacy", String.valueOf(rs.getInt("total_intimacy")));
            hashMap.put("reduce_intimacy", String.valueOf(rs.getInt("reduce_intimacy")));
            hashMap.put("intimacy_after_deduct", String.valueOf(rs.getInt("intimacy_after_deduct")));
            return hashMap;
        }, params.toArray());
    }
}
