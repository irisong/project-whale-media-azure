package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Follower;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component(FollowerSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FollowerSqlDaoImpl extends AbstractJdbcDao implements FollowerSqlDao {

    @Override
    public void getFollowingListbyPaging(DatagridModel<MemberDetail> datagridModel, String userMemberId) {
        List<Object> params = new ArrayList<>();

        String sql = "SELECT f.member_id, d.profile_name, d.whalelive_id, c.channel_id "
                + "FROM app_live_stream_follower f "
                + "INNER JOIN mb_member m ON m.member_id = f.member_id "
                + "INNER JOIN mb_member_detail d ON d.member_det_id = m.member_det_id "
                + "LEFT JOIN app_live_stream_channel c ON c.member_id = m.member_id "
                + "     AND c.status = ? "
//                    + "     AND NOT EXISTS ( "
//                    + "         SELECT b.member_id FROM mb_member_block b "
//                    + "         WHERE b.member_id = c.member_id "
//                    + "         AND b.block_member_id = ? "
//                    + "     ) "
                + "WHERE f.follower_id = ? "
                + "ORDER BY f.datetime_add DESC ";

        params.add(Global.Status.ACTIVE);
        params.add(userMemberId);
        // params.add(userMemberId);

        queryForDatagrid(datagridModel, sql, new RowMapper<MemberDetail>() {
            public MemberDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
                MemberDetail obj = new MemberDetail();
                obj.setMemberId(rs.getString("member_id"));
                obj.setProfileName(rs.getString("profile_name"));
                obj.setWhaleliveId(rs.getString("whalelive_id"));
                obj.setLiveChannelId(rs.getString("channel_id"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public void getFollowerListbyPaging(DatagridModel<MemberDetail> datagridModel, String userMemberId) {
        List<Object> params = new ArrayList<>();

        String sql = "SELECT f.follower_id, d.profile_name, d.whalelive_id, c.channel_id "
                + "FROM app_live_stream_follower f "
                + "INNER JOIN mb_member m ON m.member_id = f.follower_id "
                + "INNER JOIN mb_member_detail d ON d.member_det_id = m.member_det_id "
                + "LEFT JOIN app_live_stream_channel c ON c.member_id = m.member_id "
                + "     AND c.status = ? "
                + "WHERE f.member_id = ? "
                + "ORDER BY f.datetime_add DESC ";

        params.add(Global.Status.ACTIVE);
        params.add(userMemberId);


        queryForDatagrid(datagridModel, sql, new RowMapper<MemberDetail>() {
            public MemberDetail mapRow(ResultSet rs, int rowNum) throws SQLException {
                MemberDetail obj = new MemberDetail();
                obj.setMemberId(rs.getString("follower_id"));
                obj.setProfileName(rs.getString("profile_name"));
                obj.setWhaleliveId(rs.getString("whalelive_id"));
                obj.setLiveChannelId(rs.getString("channel_id"));
                return obj;
            }
        }, params.toArray());
    }

    @Override
    public List<Follower> getFollowingCount() {
        List<Object> params = new ArrayList<Object>();
        String sql = "select count(*) as total, follower_id from " +
                "app_live_stream_follower group by follower_id";
        List<Follower> results = query(sql, new RowMapper<Follower>() {
            public Follower mapRow(ResultSet rs, int arg1) throws SQLException {
                Integer total = rs.getInt("total");
                String followerId = rs.getString("follower_id");
                Follower follower = new Follower();
                follower.setCount(total);
                follower.setFollowerId(followerId);
                return follower;
            }
        }, params.toArray());

        if (CollectionUtil.isEmpty(results)) {
            return new ArrayList<>();
        }
        return results;
    }
}
