package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberBlock;

import java.util.Locale;

public interface MemberBlockService {
    public static final String BEAN_NAME = "memberBlockService";

    public void findMemberBlockForListing(DatagridModel<MemberBlock> datagridModel, String memberId);

    public boolean isMemberBlocked(String memberId, String blockMemberId);

    public void doProcessMemberBlock(Locale locale, String action, String memberId, String blockMemberId);

    public void doAddMemberBlock(String memberId, String blockMemberId);

    public void doRemoveMemberBlock(String memberId, String blockMemberId);
}
