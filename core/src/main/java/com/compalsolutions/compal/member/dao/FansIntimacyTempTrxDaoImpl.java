package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.member.vo.FansIntimacyTempTrx;
import com.compalsolutions.compal.member.vo.FansIntimacyTrx;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(FansIntimacyTempTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FansIntimacyTempTrxDaoImpl extends Jpa2Dao<FansIntimacyTempTrx, String> implements FansIntimacyTempTrxDao {

    public FansIntimacyTempTrxDaoImpl() {
        super(new FansIntimacyTempTrx(false));
    }

    @Override
    public List<FansIntimacyTempTrx> getAllTemporaryRecord() {
        String hql = " FROM s IN " + FansIntimacyTempTrx.class + " WHERE 1=1 ";
        List<Object> params = new ArrayList<Object>();


        return findQueryAsList(hql, params.toArray());

    }

    @Override
    public List<FansIntimacyTempTrx> getRecordByFansId(String fansMemberId, String memberId) {
        String hql = " FROM s IN " + FansIntimacyTempTrx.class + " WHERE 1=1 AND fansMemberId = ? AND memberId = ? order by trxDatetime asc ";
        List<Object> params = new ArrayList<Object>();
        params.add(fansMemberId);
        params.add(memberId);
        return findQueryAsList(hql, params.toArray());
    }



}
