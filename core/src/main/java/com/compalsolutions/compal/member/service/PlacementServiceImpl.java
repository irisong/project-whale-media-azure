package com.compalsolutions.compal.member.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.member.dao.PlacementTreeDao;
import com.compalsolutions.compal.member.vo.PlacementTree;
import com.compalsolutions.compal.member.vo.SponsorTree;
import com.compalsolutions.compal.member.vo.TreeConstant;

@Component(PlacementService.BEAN_NAME)
public class PlacementServiceImpl implements PlacementService {
    @Autowired
    private PlacementTreeDao placementTreeDao;

    @Override
    public PlacementTree findPlacementTree(String memberId, int unit) {
        return placementTreeDao.findPlacement(memberId, unit);
    }

    @Override
    public void savePlacementTree(PlacementTree placementTree) {
        placementTreeDao.save(placementTree);
    }

    @Override
    public void doParsePlacementTree(PlacementTree placementTree) {
        String lr = null;
        String b32 = Long.toString(placementTree.getId(), 32); // get based 32 number
        placementTree.setB32(b32);

        PlacementTree upline = null;
        switch (placementTree.getTreeStyle()) {
        case TreeConstant.TREE_STYLE_POS_FIXED:
            upline = findPlacementTree(placementTree.getTempId(), placementTree.getTempUnit());
            placementTree.setPosition(placementTree.getTempPosition());
            break;
        case TreeConstant.TREE_STYLE_FAR_LEFT:
        case TreeConstant.TREE_STYLE_FAR_RIGHT:
            boolean isFarLeft = placementTree.getTreeStyle() == TreeConstant.TREE_STYLE_FAR_LEFT;
            upline = doParsePlacementTree_processFarLeftRight(placementTree, isFarLeft);
            placementTree.setPosition(isFarLeft ? "L" : "R");
            break;
        default:
            throw new ValidatorException("no tree style selected for placement tree");
        }

        lr = "L".equalsIgnoreCase(placementTree.getPosition()) ? "<" : ">";
        if ("M".equalsIgnoreCase(placementTree.getPosition()))
            lr = "=";

        placementTree.setParentId(upline.getMemberId());
        placementTree.setParentUnit(upline.getUnit());
        placementTree.setLevel(upline.getLevel() + 1);
        placementTree.setTraceKey(upline.getTraceKey() + lr + b32);
        placementTree.setTreePath(upline.getTreePath() + "-" + placementTree.getPosition());
        placementTree.setParseTree("Y");
        placementTreeDao.update(placementTree);
    }

    private PlacementTree doParsePlacementTree_processFarLeftRight(PlacementTree placementTree, boolean isFarLeft) {
        SponsorService sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);

        SponsorTree sponsorTree = sponsorService.findSponsorTreeByMemberId(placementTree.getMemberId());
        PlacementTree rootNode = placementTreeDao.findPlacement(sponsorTree.getParentId(), 1);

        return doParsePlacementTree_processFarLeftRight_recursive(rootNode, isFarLeft);
    }

    private PlacementTree doParsePlacementTree_processFarLeftRight_recursive(PlacementTree rootNode, boolean isFarLeft) {
        PlacementTree node = placementTreeDao.findPlacementTreeByParentIdAndParentUnitAndParentPosition(rootNode.getMemberId(), rootNode.getUnit(),
                isFarLeft ? "L" : "R");

        if (node == null)
            return rootNode;
        else
            return doParsePlacementTree_processFarLeftRight_recursive(node, isFarLeft);
    }

    @Override
    public boolean isSponsorIdAndPlacementIdSameGroup(String sponsorMembeId, String placementMemberId) {
        PlacementTree sponsorPlacementTree = findPlacementTree(sponsorMembeId, 1);
        PlacementTree placementPlacementTree = findPlacementTree(placementMemberId, 1);

        if (sponsorPlacementTree == null || placementPlacementTree == null)
            return false;

        if (placementPlacementTree.getTraceKey().startsWith(sponsorPlacementTree.getTraceKey()))
            return true;
        return false;
    }

    @Override
    public PlacementTree findPlacementTreeByParentIdAndParentUnitAndParentPosition(String parentId, int parentUnit, String parentPosition) {
        return placementTreeDao.findPlacementTreeByParentIdAndParentUnitAndParentPosition(parentId, parentUnit, parentPosition);
    }
}
