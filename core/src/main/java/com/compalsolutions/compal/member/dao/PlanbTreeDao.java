package com.compalsolutions.compal.member.dao;

import java.util.List;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.member.vo.PlanbTree;

public interface PlanbTreeDao extends BasicDao<PlanbTree, Long> {
    public static final String BEAN_NAME = "planbDao";

    public PlanbTree findPlanbTree(String memberId, int unit);

    public List<String> findChildNodePositionsByParentAndParentUnit(String parentId, int parentUnit);

    public List<PlanbTree> findDirectDownlines(String memberId, int unit);
}
