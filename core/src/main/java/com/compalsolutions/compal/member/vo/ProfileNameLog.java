package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "lg_profile_name_log")
@Access(AccessType.FIELD)
public class ProfileNameLog extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ToTrim
    @Column(name = "old_profile_name", nullable = false, length = 20)
    private String oldProfileName;

    @ToTrim
    @Column(name = "new_profile_name", nullable = false, length = 20)
    private String newProfileName;

    public ProfileNameLog() {
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getOldProfileName() {
        return oldProfileName;
    }

    public void setOldProfileName(String oldProfileName) {
        this.oldProfileName = oldProfileName;
    }

    public String getNewProfileName() {
        return newProfileName;
    }

    public void setNewProfileName(String newProfileName) {
        this.newProfileName = newProfileName;
    }
}
