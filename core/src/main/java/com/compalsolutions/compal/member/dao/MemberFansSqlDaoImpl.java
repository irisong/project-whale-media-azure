package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberFans;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(MemberFansSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberFansSqlDaoImpl extends AbstractJdbcDao implements MemberFansSqlDao {

    @Override
    public void findMemberFansForDatagrid(DatagridModel<MemberFans> datagridModel, String memberId) {
        List<Object> params = new ArrayList<Object>();
        String sql = "SELECT f.*, CASE WHEN is_senior_admin = 1 THEN '1' WHEN is_admin = 1 THEN '2' ELSE '3' END AS sequence "
                + "FROM mb_member_fans f WHERE 1=1 ";

        if (StringUtils.isNotBlank(memberId)) {
            sql += " and f.member_id = ? ";
            params.add(memberId);
        }

        sql += "ORDER BY sequence, intimacy_value DESC ";

        queryForDatagrid(datagridModel, sql, new RowMapper<MemberFans>() {
            public MemberFans mapRow(ResultSet rs, int rowNum) throws SQLException {
                MemberFans fans = new MemberFans();
                fans.setFansMemberId(rs.getString("fans_member_id"));
                fans.setMemberId(rs.getString("member_id"));
                fans.setJoinDate(rs.getDate("join_date"));
                fans.setFansLevel(rs.getInt("fans_level"));
                fans.setIntimacyValue(rs.getInt("intimacy_value"));

                if (rs.getBoolean("is_senior_admin") == true) {
                    fans.setRole(MemberFans.ROLE_SENIOR_ADMIN);
                } else if (rs.getBoolean("is_admin") == true) {
                    fans.setRole(MemberFans.ROLE_ADMIN);
                } else {
                    fans.setRole("FANS");
                }

                return fans;
            }
        }, params.toArray());
    }
}
