package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "mb_vip_star_rank")
@Access(AccessType.FIELD)
public class VipStarRank extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "star_id", unique = true, nullable = false, length = 32)
    private String starId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @Column(name = "star_ranking", nullable = false)
    private Integer starRanking;

    @Column(name = "big_group_total_sales", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal bigGroupTotalSales;

    @Column(name = "small_group_total_sales", nullable = true, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal smallGroupTotalSales;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark_cn", columnDefinition = Global.ColumnDef.TEXT)
    private String remarkCn;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark_en", columnDefinition = Global.ColumnDef.TEXT)
    private String remarkEn;

    public VipStarRank() {
    }

    public VipStarRank(boolean defaultValue) {
        if (defaultValue) {
            bigGroupTotalSales = BigDecimal.ZERO;
            smallGroupTotalSales = BigDecimal.ZERO;
        }
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public BigDecimal getBigGroupTotalSales() {
        return bigGroupTotalSales;
    }

    public void setBigGroupTotalSales(BigDecimal bigGroupTotalSales) {
        this.bigGroupTotalSales = bigGroupTotalSales;
    }

    public BigDecimal getSmallGroupTotalSales() {
        return smallGroupTotalSales;
    }

    public void setSmallGroupTotalSales(BigDecimal smallGroupTotalSales) {
        this.smallGroupTotalSales = smallGroupTotalSales;
    }

    public String getStarId() {
        return starId;
    }

    public void setStarId(String starId) {
        this.starId = starId;
    }

    public Integer getStarRanking() {
        return starRanking;
    }

    public void setStarRanking(Integer starRanking) {
        this.starRanking = starRanking;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getRemarkCn() {
        return remarkCn;
    }

    public void setRemarkCn(String remarkCn) {
        this.remarkCn = remarkCn;
    }

    public String getRemarkEn() {
        return remarkEn;
    }

    public void setRemarkEn(String remarkEn) {
        this.remarkEn = remarkEn;
    }
}
