package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.MemberBlock;

public interface MemberBlockDao extends BasicDao<MemberBlock, String> {
    public static final String BEAN_NAME = "memberBlockDao";

    public MemberBlock findMemberBlock(String memberId, String blockMemberId);

    public void findMemberBlockForDatagrid(DatagridModel<MemberBlock> datagridModel, String memberId);
}
