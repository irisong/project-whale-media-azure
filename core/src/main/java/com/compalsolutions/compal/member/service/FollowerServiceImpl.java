package com.compalsolutions.compal.member.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.dao.*;
import com.compalsolutions.compal.member.vo.Follower;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.websocket.service.WsMessageService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Component(FollowerService.BEAN_NAME)
public class FollowerServiceImpl implements FollowerService {
    private static final Object synchronizedObject = new Object();

    @Autowired
    private FollowerDao followerDao;

    @Autowired
    private FollowerSqlDao followerSqlDao;

    @Override
    public void doProcessFollow(Locale locale, String action, String userMemberId, String followMemberId) {
        RankService rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.BEAN_NAME, LiveStreamService.class);
        WsMessageService wsMessageService = Application.lookupBean(WsMessageService.BEAN_NAME, WsMessageService.class);

        if (StringUtils.isBlank(action)) {
            throw new ValidatorException("invalidAction", locale);
        }

        if (action.equals("follow")) {
            doCreateFollowing(userMemberId, followMemberId);

            //add member experience
            Member member = memberService.getMember(followMemberId);
            MemberRankTrx memberRankTrx = rankService.findExperienceByTrxTypeByRemark(userMemberId, MemberRankTrx.FOLLOW, member.getMemberCode());
            if (memberRankTrx == null) { //check if already get exp for following this member
                Follower follower = followerDao.getFollower(followMemberId, userMemberId);
                rankService.doProcessMemberExperience(locale, userMemberId, "Follower", "", follower.getId(),
                        MemberRankTrx.FOLLOW, 0, BigDecimal.ZERO, member.getMemberCode());

                //pop up notification
                doProcessFollowingNotification(userMemberId, member);
            }

            LiveStreamChannel liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(followMemberId, Global.Status.ACTIVE);
            if (liveStreamChannel != null) {
                wsMessageService.updateFollowerCount(liveStreamChannel.getChannelId());
            }
        } else if (action.equals("unfollow")) {
            doRemoveFollowing(userMemberId, followMemberId);
        }
    }

//    @Override
//    public void doResetExperienceForExp() {
//        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
//        RankService rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
//        List<Follower> followers = followerSqlDao.getFollowingCount();
//
//        for (Follower follower : followers) {
//            Member member = memberService.getMember(follower.getFollowerId());
//            if (member != null) {
//
//                if (member.getLevel() == null) {
//                    // consist of level 0
//                    member.setLevel(0);
////                member.setLevel(1);
//                }
//                if (member.getExperience() == null) {
//                    member.setExperience(BigDecimal.ZERO);
//                }
//
//                Integer nextRank = member.getLevel() + 1;
//                // exp to up level is based next required
//                BigDecimal expToUplevel = rankService.getExperienceByRank(nextRank);
//
//                BigDecimal totalExp = member.getExperience().add(new BigDecimal(String.valueOf(follower.getCount())));
//
//                boolean canUpLevel;
//                if (!BDUtil.gZero(expToUplevel)) {
//                    canUpLevel = false;
//                } else {
//                    canUpLevel = BDUtil.ge(totalExp, expToUplevel);
//                }
//                if (canUpLevel) {
//                    // up level and reset experience
//                    BigDecimal carryForwardExp = totalExp.subtract(expToUplevel);
//                    // if user managed to up more than one level in one action
//                    expToUplevel = rankService.getExperienceByRank(nextRank + 1);
//                    while (BDUtil.ge(carryForwardExp, expToUplevel) && BDUtil.gZero(expToUplevel)) {
//                        nextRank++;
//                        carryForwardExp = carryForwardExp.subtract(expToUplevel);
//                        expToUplevel = rankService.getExperienceByRank(nextRank + 1);
//                    }
//                    member.setLevel(nextRank);
//                    member.setExperience(carryForwardExp);
//                } else {
//                    member.setExperience(totalExp);
//                }
//                memberService.updateMember(member);
//            } else {
//                System.out.println("FOLLOWER ERROR = " + follower.getFollowerId());
//            }
//        }
//
//    }

    @Override
    public void doProcessFollowingNotification(String userMemberId, Member followMember) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        MessageService messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
        Environment env = Application.lookupBean(Environment.class);

        // create message for app display
        messageService.doGenerateFollowingMessage(userMemberId, followMember.getMemberId());

        if (followMember.getSendNotifFollower()) {
            // send notification to yun xin for notification pop up
            Payload payload = new Payload();
            payload.setType(Payload.FOLLOWER);
            payload.setRedirectUrl(followMember.getMemberId());
            MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(userMemberId, MemberFile.UPLOAD_TYPE_PROFILE_PIC);
            if (memberFile != null) {
                memberFile.setFileUrlWithParentPath(env.getProperty("fileServerUrl") + "/file/memberFile");
                payload.setAvatarUrl(memberFile.getFileUrl());
            } else {
                payload.setAvatarUrl("");
            }

            NotificationAccount frmAccount = notificationService.findNotifAccByMemberAndAccessModule(userMemberId, Global.AccessModule.WHALE_MEDIA);
            NotificationAccount toAccount = notificationService.findNotifAccByMemberAndAccessModule(followMember.getMemberId(), Global.AccessModule.WHALE_MEDIA);
            if (frmAccount != null && toAccount != null) {
                MemberDetail memberDetail = memberService.findMemberDetailByMemberId(userMemberId);
                Locale locale = new Locale("en");
                if (followMember.getPreferLanguage().equals(Global.Language.CHINESE)) {
                    locale = Global.LOCALE_CN;
                } else if (followMember.getPreferLanguage().equals(Global.Language.MALAY)) {
                    locale = Global.LOCALE_MS;
                }

                notificationConfProvider.doSendMessage(frmAccount.getAccountId(), toAccount.getAccountId(), Global.AccessModule.WHALE_MEDIA,
                        i18n.getText("message.userFollowingMessage", locale, memberDetail.getProfileName()), payload, null);
            }
        }
    }

    @Override
    public void doCreateFollowing(String userMemberId, String followMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedObject) {
            if (followMemberId.equals(userMemberId)) {
                throw new ValidatorException("Not allow to follow own account");
            }

            Member member = memberService.getMember(followMemberId);
            if (member == null) {
                throw new ValidatorException("Invalid member");
            }

            Follower follower = followerDao.getFollower(followMemberId, userMemberId);
            if (follower == null) { //only insert when record not exist
                follower = new Follower(true);
                follower.setMemberId(followMemberId);
                follower.setFollowerId(userMemberId);
                followerDao.save(follower);
            }
        }
    }

    @Override
    public void doRemoveFollowing(String userMemberId, String followMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedObject) {
            Member member = memberService.getMember(followMemberId);
            if (member == null) {
                throw new ValidatorException("Invalid member");
            }

            Follower follower = followerDao.getFollower(followMemberId, userMemberId);
            if (follower != null) {
                followerDao.delete(follower);
            }
        }
    }

    @Override
    public int getFollowerCountById(String userMemberId) {
        return followerDao.getFollowerCount(userMemberId);
    }

    @Override
    public int getFollowingCountById(String userMemberId) {
        return followerDao.getFollowingCount(userMemberId);
    }

    @Override
    public Follower getFollower(String userMemberId, String followMemberId) {
        return followerDao.getFollower(followMemberId, userMemberId);
    }

    @Override
    public List<Follower> getFollowerByMemberId(String userMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        Member member = memberService.getMember(userMemberId);
        if (member == null) {
            throw new ValidatorException("Invalid member");
        }

        return followerDao.getFollowersList(userMemberId);
    }

    @Override
    public List<Follower> getFollowingByMemberId(String userMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        Member member = memberService.getMember(userMemberId);
        if (member == null) {
            throw new ValidatorException("Invalid member");
        }

        return followerDao.getFollowingList(userMemberId);
    }

    @Override
    public void getFollowingList(DatagridModel<MemberDetail> datagridModel, String userMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        Member member = memberService.getMember(userMemberId);
        if (member == null) {
            throw new ValidatorException("Invalid member");
        }

        followerSqlDao.getFollowingListbyPaging(datagridModel, userMemberId);
    }

    @Override
    public void getFollowerList(DatagridModel<MemberDetail> datagridModel, String userMemberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        Member member = memberService.getMember(userMemberId);
        if (member == null) {
            throw new ValidatorException("Invalid member");
        }

        followerSqlDao.getFollowerListbyPaging(datagridModel, userMemberId);
    }
}
