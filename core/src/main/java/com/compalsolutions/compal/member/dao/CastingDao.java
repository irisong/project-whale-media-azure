package com.compalsolutions.compal.member.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.member.vo.Casting;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.Date;


public interface CastingDao extends BasicDao<Casting, String> {
    public static final String BEAN_NAME = "castingDao";

    public void findCastForDatagrid(DatagridModel<Casting> datagridModel, String whaleLiveId, Date createDateFrom, Date createDateTo);

    public void findCastForListingWithGuildId(DatagridModel<Casting> datagridModel, String guildId);

    public Casting getCastingByWhaleliveId(String whaleLiveId);

}
