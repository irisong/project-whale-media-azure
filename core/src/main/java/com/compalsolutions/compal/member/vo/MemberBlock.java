package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "mb_member_block")
@Access(AccessType.FIELD)
public class MemberBlock extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String ACTION_BLOCK = "BLOCK";
    public static final String ACTION_UNBLOCK = "UNBLOCK";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "block_id", unique = true, nullable = false, length = 32)
    private String blockId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ManyToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private Member member;

    @Column(name = "block_member_id", nullable = false, length = 32)
    private String blockMemberId;

    @Transient
    private String profileName;

    @Transient
    private String whaleliveId;

//    @Transient
//    private Integer level;

    @Transient
    private String profilePicUrl;

//    @Transient
//    private String role;

    public MemberBlock() {
    }

    public MemberBlock(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getBlockId() {
        return blockId;
    }

    public void setBlockId(String blockId) {
        this.blockId = blockId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getBlockMemberId() {
        return blockMemberId;
    }

    public void setBlockMemberId(String blockMemberId) {
        this.blockMemberId = blockMemberId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }
}
