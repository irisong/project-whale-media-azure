package com.compalsolutions.compal.member.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.general.vo.LiveCategory;
import org.apache.commons.lang.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "mb_member")
@Access(AccessType.FIELD)
public class Member extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String KYC_STATUS_PENDING_UPLOAD = "PENDING_UPLOAD";
    public static final String KYC_STATUS_PENDING_APPROVAL = "PENDING_APPROVAL";
    public static final String KYC_STATUS_REJECT = Global.Status.REJECTED;
    public static final String KYC_STATUS_APPROVED = Global.Status.APPROVED;

    public static final String SEND_NOTIF_TYPE_COMMENT = "COMMENT";
    public static final String SEND_NOTIF_TYPE_LIKE = "LIKE";
    public static final String SEND_NOTIF_TYPE_FOLLOWER = "FOLLOWER";
    public static final String SEND_NOTIF_TYPE_FOLLOWING = "FOLLOWING";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "member_id", unique = true, nullable = false, length = 32)
    private String memberId;

    @Column(name = "member_det_id", nullable = false, length = 32)
    private String memberDetId;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "member_det_id", insertable = false, updatable = false, nullable = true)
    private MemberDetail memberDetail;

    @ToUpperCase
    @ToTrim
    @Column(name = "member_code", length = 50, nullable = false)
    private String memberCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String remark;

    @ToUpperCase
    @ToTrim
    @Column(name = "first_name", length = 200)
    private String firstName;

    @ToUpperCase
    @ToTrim
    @Column(name = "last_name", length = 200)
    private String lastName;

    @ToUpperCase
    @ToTrim
    @Column(name = "full_name", length = 200)
    private String fullName;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_type", length = 5, nullable = false)
    private String identityType;

    @ToUpperCase
    @ToTrim
    @Column(name = "identity_no", length = 100)
    private String identityNo;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "join_date", nullable = false)
    private Date joinDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "active_datetime")
    private Date activeDatetime;

    @Column(name = "active_by", length = 32)
    private String activeBy;

    // default 'en'
    @ToTrim
    @Column(name = "prefer_language", length = 10)
    private String preferLanguage;

    // default false
    @Column(name = "close_account")
    private Boolean closeAccount;

    // default false
    @Column(name = "is_block", nullable = false)
    private Boolean block;

    @Column(name = "send_notif_comment")
    private Boolean sendNotifComment;

    @Column(name = "send_notif_like")
    private Boolean sendNotifLike;

    @Column(name = "send_notif_follower")
    private Boolean sendNotifFollower;

    //People who I follow is on stream / update post
    @Column(name = "send_notif_following")
    private Boolean sendNotifFollowing;

    // default GENERAL
    @Column(name = "live_stream_types")
    private String liveStreamTypes;

    @Column(name = "live_stream_private")
    private Boolean liveStreamPrivate;

    @ToTrim
    @Column(name = "fans_badge_name", length = 20)
    private String fansBadgeName;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private List<MemberFans> memberFans = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "member_id", insertable = false, updatable = false)
    private List<MemberBlock> MemberBlock = new ArrayList<>();

    @Column(name = "experience", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal experience;

    @Column(name = "total_exp", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalExp;

    @Column(name = "level", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private Integer level;

    @ToTrim
    @Column(name = "user_role", length = 30)
    private String userRole;

    @ToUpperCase
    @ToTrim
    @Column(name = "kyc_status", length = 35, nullable = false)
    private String kycStatus;

    @Column(name = "kyc_remark", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String kycRemark;

    @Column(name = "agora_uid")
    private Integer agoraUid;

    @Column(name = "channel_name")
    private String channelName;

    @Transient
    private String liveStreamStatus;

    @Transient
    private String guildStatus;

    @Transient
    private String rankName;

    @Transient
    private String icFrontUrl;

    @Transient
    private String icBackUrl;

    @Temporal(TemporalType.TIMESTAMP)
    @Transient
    private Date kycSubmitDate;

    @Transient
    private String displayId;

    @Transient
    private String displayProfileName;

    @Transient
    private BigDecimal point;

    @Temporal(TemporalType.TIMESTAMP)
    @Transient
    private Date serveStartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Transient
    private Date serveEndDate;

    public Member() {
    }

    public Member(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
            identityType = Global.IdentityType.IDENTITY_CARD;
            preferLanguage = "en";
            closeAccount = false;
            block = false;
            sendNotifComment = true;
            sendNotifLike = true;
            sendNotifFollower = true;
            sendNotifFollowing = true;
            joinDate = new Date();
            liveStreamPrivate = false;
            userRole = Global.UserType.MEMBER;
            experience = BigDecimal.ZERO;
            level = 0;
            totalExp = BigDecimal.ZERO;
            kycStatus = KYC_STATUS_PENDING_UPLOAD;
        }
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMemberDetId() {
        return memberDetId;
    }

    public void setMemberDetId(String memberDetId) {
        this.memberDetId = memberDetId;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Member member = (Member) o;

        if (memberId != null ? !memberId.equals(member.memberId) : member.memberId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return memberId != null ? memberId.hashCode() : 0;
    }

    public String getIdentityType() {
        return identityType;
    }

    public void setIdentityType(String identityType) {
        this.identityType = identityType;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Date getActiveDatetime() {
        return activeDatetime;
    }

    public void setActiveDatetime(Date activeDatetime) {
        this.activeDatetime = activeDatetime;
    }

    public String getActiveBy() {
        return activeBy;
    }

    public void setActiveBy(String activeBy) {
        this.activeBy = activeBy;
    }

    public String getPreferLanguage() {
        return preferLanguage;
    }

    public void setPreferLanguage(String preferLanguage) {
        this.preferLanguage = preferLanguage;
    }

    public Boolean getCloseAccount() {
        return closeAccount;
    }

    public void setCloseAccount(Boolean closeAccount) {
        this.closeAccount = closeAccount;
    }

    public Boolean getBlock() {
        return block;
    }

    public void setBlock(Boolean block) {
        this.block = block;
    }

    public Boolean getSendNotifComment() {
        return sendNotifComment;
    }

    public void setSendNotifComment(Boolean sendNotifComment) {
        this.sendNotifComment = sendNotifComment;
    }

    public Boolean getSendNotifLike() {
        return sendNotifLike;
    }

    public void setSendNotifLike(Boolean sendNotifLike) {
        this.sendNotifLike = sendNotifLike;
    }

    public Boolean getSendNotifFollower() {
        return sendNotifFollower;
    }

    public void setSendNotifFollower(Boolean sendNotifFollower) {
        this.sendNotifFollower = sendNotifFollower;
    }

    public Boolean getSendNotifFollowing() {
        return sendNotifFollowing;
    }

    public void setSendNotifFollowing(Boolean sendNotifFollowing) {
        this.sendNotifFollowing = sendNotifFollowing;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public String getLiveStreamTypes() {
        return liveStreamTypes;
    }

    public void setLiveStreamTypes(String liveStreamTypes) {
        this.liveStreamTypes = liveStreamTypes;
    }

    public Boolean getLiveStreamPrivate() {
        return liveStreamPrivate;
    }

    public void setLiveStreamPrivate(Boolean liveStreamPrivate) {
        this.liveStreamPrivate = liveStreamPrivate;
    }

    public String getLiveStreamStatus() {
        return liveStreamStatus;
    }

    public void setLiveStreamStatus(String liveStreamStatus) {
        this.liveStreamStatus = liveStreamStatus;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }

    public List<MemberFans> getMemberFans() {
        return memberFans;
    }

    public void setMemberFans(List<MemberFans> memberFans) {
        this.memberFans = memberFans;
    }

    public List<com.compalsolutions.compal.member.vo.MemberBlock> getMemberBlock() {
        return MemberBlock;
    }

    public void setMemberBlock(List<com.compalsolutions.compal.member.vo.MemberBlock> memberBlock) {
        MemberBlock = memberBlock;
    }

    public BigDecimal getExperience() {
        return experience;
    }

    public void setExperience(BigDecimal experience) {
        this.experience = experience;
    }

    public String getMemberCodeHide() {
        return this.replaceLastCharacter(memberCode);
    }

    public Integer getLevel() {
        return level;
    }

    public String getUserRole() {
        return StringUtils.isNotBlank(userRole) ? userRole : Global.UserType.MEMBER;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getKycStatus() {
        return kycStatus;
    }

    public void setKycStatus(String kycStatus) {
        this.kycStatus = kycStatus;
    }

    public String getKycRemark() {
        return kycRemark;
    }

    public void setKycRemark(String kycRemark) {
        this.kycRemark = kycRemark;
    }

    public Integer getAgoraUid() {
        return agoraUid;
    }

    public void setAgoraUid(Integer agoraUid) {
        this.agoraUid = agoraUid;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public String getIcFrontUrl() {
        return icFrontUrl;
    }

    public void setIcFrontUrl(String icFrontUrl) {
        this.icFrontUrl = icFrontUrl;
    }

    public String getIcBackUrl() {
        return icBackUrl;
    }

    public void setIcBackUrl(String icBackUrl) {
        this.icBackUrl = icBackUrl;
    }

    public String getGuildStatus() {
        return guildStatus;
    }

    public void setGuildStatus(String guildStatus) {
        this.guildStatus = guildStatus;
    }

    public BigDecimal getTotalExp() {
        return totalExp;
    }

    public void setTotalExp(BigDecimal totalExp) {
        this.totalExp = totalExp;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public String getDisplayProfileName() {
        return displayProfileName;
    }

    public void setDisplayProfileName(String displayProfileName) {
        this.displayProfileName = displayProfileName;
    }

    public Date getKycSubmitDate() {
        return kycSubmitDate;
    }

    public void setKycSubmitDate(Date kycSubmitDate) {
        this.kycSubmitDate = kycSubmitDate;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public Date getServeStartDate() {
        return serveStartDate;
    }

    public void setServeStartDate(Date serveStartDate) {
        this.serveStartDate = serveStartDate;
    }

    public Date getServeEndDate() {
        return serveEndDate;
    }

    public void setServeEndDate(Date serveEndDate) {
        this.serveEndDate = serveEndDate;
    }

    private String replaceLastCharacter(String memberCode) {
        if (StringUtils.isNotBlank(memberCode)) {
            int length = memberCode.length();
            if (length < 5) {
                return "******";
            }
            return memberCode.substring(0, 4) + "****" + memberCode.substring(length - 4, length);
        }
        return "******";
    }

    public static final int getRankBasedOnLevel(int level) {
        if (level <= 5) {
            return 1;
        } else if (level <= 10) {
            return 2;
        } else if (level <= 20) {
            return 3;
        } else if (level <= 40) {
            return 4;
        } else if (level <= 60) {
            return 5;
        } else if (level <= 80) {
            return 6;
        } else if (level <= 99) {
            return 7;
        } else {
            return 8;
        }
    }

    //change to string as mobile will cache old image, need renamed for new image
    public static final String getRankInString(int level) {
        if (level <= 100) {
            return "rank" + level;
        } else {
            return "rank999";
        }
    }
}
