package com.compalsolutions.compal.member.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "mb_member_report")
@Access(AccessType.FIELD)
public class MemberReport extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static int MAX_FILE_UPLOAD = 1;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "report_id", unique = true, length = 32, nullable = false)
    private String reportId;

    @Column(name = "report_type_id", length = 32, nullable = false)
    private String reportTypeId;

    @ManyToOne
    @JoinColumn(name = "report_type_id", insertable = false, updatable = false)
    private MemberReportType memberReportType;

    @Column(name = "member_id", length = 32, nullable = false)
    private String memberId;

    @ToUpperCase
    @ToTrim
    @Column(name = "member_type", length = 10, nullable = false)
    private String memberType;

    @Column(name = "report_member_id", length = 32, nullable = false)
    private String reportMemberId;

    @ToUpperCase
    @ToTrim
    @Column(name = "report_member_type", length = 10, nullable = false)
    private String reportMemberType;

    @ToTrim
    @Column(name = "description", columnDefinition = "text")
    private String description;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "report_id", insertable = false, updatable = false)
    private List<MemberReportFile> memberReportFiles = new ArrayList<>();

    @ToTrim
    @Column(name = "read_status", nullable = false)
    private Boolean readStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "read_datetime")
    private Date readDatetime;

    @Column(name = "read_by", length = 32)
    private String readBy;

    @Transient
    private List<String> reportFileUrl;

    @Transient
    private String memberCode;

    @Column(name = "warning_count", nullable = false)
    private Integer warningCount;


    public MemberReport() {
    }

    public MemberReport(boolean defaultValue) {
        if (defaultValue) {
            readStatus = false;
        }
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(String reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    public MemberReportType getMemberReportType() {
        return memberReportType;
    }

    public void setMemberReportType(MemberReportType memberReportType) {
        this.memberReportType = memberReportType;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberType() {
        return memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    public String getReportMemberId() {
        return reportMemberId;
    }

    public void setReportMemberId(String reportMemberId) {
        this.reportMemberId = reportMemberId;
    }

    public String getReportMemberType() {
        return reportMemberType;
    }

    public void setReportMemberType(String reportMemberType) {
        this.reportMemberType = reportMemberType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<MemberReportFile> getMemberReportFiles() {
        return memberReportFiles;
    }

    public void setMemberReportFiles(List<MemberReportFile> memberReportFiles) {
        this.memberReportFiles = memberReportFiles;
    }

    public Boolean getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(Boolean readStatus) {
        this.readStatus = readStatus;
    }

    public Date getReadDatetime() {
        return readDatetime;
    }

    public void setReadDatetime(Date readDatetime) {
        this.readDatetime = readDatetime;
    }

    public String getReadBy() {
        return readBy;
    }

    public void setReadBy(String readBy) {
        this.readBy = readBy;
    }

    public List<String> getReportFileUrl() {
        return reportFileUrl;
    }

    public void setReportFileUrl(List<String> reportFileUrl) {
        this.reportFileUrl = reportFileUrl;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Integer getWarningCount() {
        return warningCount;
    }

    public void setWarningCount(Integer warningCount) {
        this.warningCount = warningCount;
    }
}
