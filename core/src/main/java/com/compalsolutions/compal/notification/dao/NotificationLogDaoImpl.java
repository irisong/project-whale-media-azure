package com.compalsolutions.compal.notification.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.notification.vo.NotificationLog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(NotificationLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NotificationLogDaoImpl extends Jpa2Dao<NotificationLog, String> implements NotificationLogDao {

    public NotificationLogDaoImpl() {
        super(new NotificationLog(false));
    }
}
