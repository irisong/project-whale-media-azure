package com.compalsolutions.compal.notification.client.dto;

public class Payload {
    public static final String WALLET_DEPOSIT = "WALLET_DEPOSIT";
    public static final String COM_WITHDRAW = "COM_WITHDRAW";
    public static final String CRYPTO_XFER = "CRYPTO_XFER";
    public static final String COM_DEPOSIT = "COM_DEPOSIT";
    public static final String FEEDBACK_REPLIED = "FEEDBACK_REPLIED";
    public static final String FOLLOWER = "FOLLOWER";
    public static final String ANNOUNCEMENT = "ANNOUNCEMENT";
    public static final String NOTICE = "NOTICE";
    public static final String LIVE_STREAM = "LIVE_STREAM";
    public static final String FANS_BADGE_REQUEST = "FANS_BADGE_REQUEST";
    public static final String SET_FANS_ROLE = "SET_FANS_ROLE";
    public static final String NEWS_UPDATE = "NEWS_UPDATE";
    public static final String VJ_MISSION_REQUEST = "VJ_MISSION_REQUEST";

    private String id;
    private String type;
    private String currency;
    private String name;
    private String redirectUrl;
    private String avatarUrl;
    private String coverUrl;
    private boolean privateLive;
    private String httpPullUrl;
    private String hlsPullUrl;
    private String rtmpPullUrl;

    public Payload() {
    }

    public Payload(String id, String type, String currency, String name) {
        this.id = id;
        this.type = type;
        this.currency = currency;
        this.name = name;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public boolean isPrivateLive() {
        return privateLive;
    }

    public void setPrivateLive(boolean privateLive) {
        this.privateLive = privateLive;
    }

    public String getHttpPullUrl() {
        return httpPullUrl;
    }

    public void setHttpPullUrl(String httpPullUrl) {
        this.httpPullUrl = httpPullUrl;
    }

    public String getHlsPullUrl() {
        return hlsPullUrl;
    }

    public void setHlsPullUrl(String hlsPullUrl) {
        this.hlsPullUrl = hlsPullUrl;
    }

    public String getRtmpPullUrl() {
        return rtmpPullUrl;
    }

    public void setRtmpPullUrl(String rtmpPullUrl) {
        this.rtmpPullUrl = rtmpPullUrl;
    }
}