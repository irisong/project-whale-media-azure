package com.compalsolutions.compal.notification.client.dto;

import java.util.List;

public class YunXinRequestBodyBatch {
    private String fromAccid;
    private List<String> toAccids;
    private String attach;
    private String pushContent;
    private String payload;
    private String toAccidsStr;

    public String getFromAccid() {
        return fromAccid;
    }

    public void setFromAccid(String fromAccid) {
        this.fromAccid = fromAccid;
    }

    public List<String> getToAccids() {
        return toAccids;
    }

    public void setToAccids(List<String> toAccids) {
        this.toAccids = toAccids;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getPushContent() {
        return pushContent;
    }

    public void setPushContent(String pushContent) {
        this.pushContent = pushContent;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getToAccidsStr() {
        return toAccidsStr;
    }

    public void setToAccidsStr(String toAccidsStr) {
        this.toAccidsStr = toAccidsStr;
    }
}
