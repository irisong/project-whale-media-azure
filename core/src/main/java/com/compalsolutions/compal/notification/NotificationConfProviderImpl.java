package com.compalsolutions.compal.notification;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.notification.client.YunxinClient;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.client.dto.YunXinRequestBody;
import com.compalsolutions.compal.notification.client.dto.YunXinRequestBodyBatch;
import com.compalsolutions.compal.notification.client.dto.YunXinResponse;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.*;

@Service(NotificationConfProvider.BEAN_NAME)
public class NotificationConfProviderImpl implements NotificationConfProvider {
    private Log log = LogFactory.getLog(NotificationConfProviderImpl.class);

    private NotificationService notificationService;
    private MemberService memberService;
    private Environment env;

    @Autowired
    public NotificationConfProviderImpl(NotificationService notificationService, MemberService memberService, Environment env) {
        this.notificationService = notificationService;
        this.memberService = memberService;
        this.env = env;
    }

    @Override
    public void doCreateMemberNotificationAccount() {
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);
        YunxinClient yunxinClient = new YunxinClient();

        try {
            // testing account, for staging and dev
            List<String> memberIds = notificationService.findMemberNotRegisterNotificationAcc(Global.AccessModule.WHALE_MEDIA);

            for (String memberId : memberIds) {
                YunXinResponse response = yunxinClient.createUserAccount(notificationService.generateAccId(), Global.AccessModule.WHALE_MEDIA);
                if (response != null && response.getInfo() != null) {
                    notificationService.doCreateNotificationAccount(memberId, Global.AccessModule.WHALE_MEDIA, response.getInfo());
                }
            }
        } catch (Exception e) {
            log.debug("error create notification account" + e.getMessage());
        }
    }

    @Override
    public YunXinResponse doSendMessage(String from, String to, String accessModule, String pushContent) {
        return doSendMessage(from, to, accessModule, pushContent, null, null);
    }

    @Override
    public YunXinResponse doSendMessage(String from, String to, String accessModule, String pushContent, Payload payload, String payloadJsonStr) {
        YunXinResponse response = null;
        SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
        SystemConfig systemConfig = systemConfigService.getDefaultSystemConfig();
        boolean notifyOffline = false;
        if (systemConfig != null && systemConfig.getNotifOffline() != null) {
            notifyOffline = systemConfig.getNotifOffline();
        }
        Gson gson = new Gson();
        YunXinRequestBody requestBody = new YunXinRequestBody();
        requestBody.setFrom(from);
        requestBody.setTo(to);
        requestBody.setAttach(accessModule);
        requestBody.setMsgtype(0);
        requestBody.setPushContent(pushContent);
        if (payload != null) {
            requestBody.setPayload(gson.toJson(payload));
        } else {
            requestBody.setPayload(payloadJsonStr);
        }

        YunxinClient yunxinClient = new YunxinClient();
        try {
            response = yunxinClient.sendMessageAttach(requestBody, accessModule, notifyOffline);
        } catch (Exception e) {
            log.debug("error send notification message" + e.getMessage());
        }

        return response;
    }

    @Override
    public YunXinResponse doSendMessageAll(String from, List<String> tos, String accessModule, String msg, String title, String lang, Payload payload) {
        I18n i18n = Application.lookupBean(I18n.class);
        List<String> ids = new ArrayList<>();
        SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
        SystemConfig systemConfig = systemConfigService.getDefaultSystemConfig();
        YunXinRequestBodyBatch requestBody = new YunXinRequestBodyBatch();
        requestBody.setFromAccid(from);
        requestBody.setAttach(accessModule);
        boolean notifyOffline = false;
        if (systemConfig != null && systemConfig.getNotifOffline() != null) {
            notifyOffline = systemConfig.getNotifOffline();
        }
        YunXinResponse response = null;
        Gson gson = new Gson();
        try {
            int record = 0;
            for (String notif : tos) {
                record++;
                ids.add(notif);

                if (payload != null) {
                    requestBody.setPayload(gson.toJson(payload));
                }

                //send message once reach 500 limits msg count / is the last msg
                if (record == 500 || tos.size() == record) {
                    YunxinClient yunxinClient = new YunxinClient();
                    requestBody.setToAccids(ids);

                    if (StringUtils.isBlank(msg)) {
                        requestBody.setPushContent(i18n.getText(title, new Locale(lang)));
                    } else {
                        requestBody.setPushContent(i18n.getText(msg, new Locale(lang), i18n.getText(title, new Locale(lang))));
                    }

                    response = yunxinClient.sendBatchMessageAttach(requestBody, accessModule, notifyOffline);
                    ids = new ArrayList<>();
                    record = 0;
                }
            }
        } catch (Exception e) {
            log.debug("error send notification message" + e.getMessage());
        }
        return response;
    }

    @Override
    public YunXinResponse doSendMessageBatch(String from, List<String> tos, String accessModule, String pushContent, Payload payload, String payloadJsonStr) {
        YunXinResponse response = null;
        SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
        SystemConfig systemConfig = systemConfigService.getDefaultSystemConfig();
        boolean notifyOffline = false;
        if (systemConfig != null && systemConfig.getNotifOffline() != null) {
            notifyOffline = systemConfig.getNotifOffline();
        }
        Gson gson = new Gson();
        YunXinRequestBodyBatch requestBody = new YunXinRequestBodyBatch();
        requestBody.setFromAccid(from);
        requestBody.setToAccids(tos);
        requestBody.setAttach(accessModule);
        requestBody.setPushContent(pushContent);
        if (payload != null) {
            requestBody.setPayload(gson.toJson(payload));
        } else {
            requestBody.setPayload(payloadJsonStr);
        }

        YunxinClient yunxinClient = new YunxinClient();
        try {
            response = yunxinClient.sendBatchMessageAttach(requestBody, accessModule, notifyOffline);

        } catch (Exception e) {
            log.debug("error send notification message" + e.getMessage());
        }
        return response;
    }

    @Override
    public void addPushNotificationMessage(String accessModule, String memberId, String id, String action, String currency, String name, String msg) {
        NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, accessModule);
        NotificationAccount accountTo = notificationService.findNotifAccByMemberAndAccessModule(memberId, accessModule);

        Payload payload = new Payload(id, action, currency, name);
        if (account != null && accountTo != null) {
            YunXinResponse yunXinResponse = doSendMessage(account.getAccountId(), accountTo.getAccountId(),
                    accessModule, msg, payload, null);
        }
    }

    @Override
    public void doProcessAnnouncementNotification(RunTaskLogger logger) {
        AnnouncementService announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 7);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        // don't disturb user during midnight
        Date timeBeginNotify = c.getTime();
        log.debug("Time to begin " + timeBeginNotify + " current :" + new Date());
        if (new Date().after(timeBeginNotify)) {
            List<Announcement> announcementList = announcementService.findPendingAnnouncementToSend(new Date());
            for (Announcement announcement : announcementList) {
                if (StringUtils.isNotBlank(announcement.getMembers())) {
                    String[] memberCodes = StringUtils.split(announcement.getMembers(), "|");
                    log.debug("memberCodes notif:" + memberCodes.length);
                    for (String memberCode : memberCodes) {
                        Member member = memberService.findMemberByMemberCode(memberCode);
                        if (member != null) {
                            announcement.setMemberId(member.getMemberId());
                        }
                        announcementService.doProcessAnnouncementNotification(announcement);
                    }
                    announcement.setStatus(Global.Status.ACTIVE);
                    announcementService.updateAnnouncement(new Locale("en"), announcement);
                } else {
                    announcementService.doProcessAnnouncementNotification(announcement);

                    announcement.setStatus(Global.Status.ACTIVE);
                    announcementService.updateAnnouncement(new Locale("en"), announcement);
                }
            }
        }
    }
}
