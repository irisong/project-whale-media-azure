package com.compalsolutions.compal.notification.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(NotificationAccDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NotificationAccDaoImpl extends Jpa2Dao<NotificationAccount, String> implements NotificationAccDao {

    public NotificationAccDaoImpl() {
        super(new NotificationAccount(false));
    }

    @Override
    public NotificationAccount findNotificationAccountByAccId(String accid) {
        Validate.notBlank(accid);

        NotificationAccount example = new NotificationAccount(false);
        example.setAccountId(accid);
        return findUnique(example);
    }

    @Override
    public NotificationAccount findNotifAccByMemberAndAccessModule(String memberId, String accessModule) {
        Validate.notBlank(memberId);
        Validate.notBlank(accessModule);

        NotificationAccount example = new NotificationAccount(false);
        example.setMemberId(memberId);
        example.setAccessModule(accessModule);
        return findUnique(example);
    }

    @Override
    public List<NotificationAccount> findAllNotifAccByAccessModule(String accessModule) {
        Validate.notBlank(accessModule);

        NotificationAccount example = new NotificationAccount(false);
        example.setAccessModule(accessModule);
        return findByExample(example);
    }
}
