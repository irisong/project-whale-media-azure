package com.compalsolutions.compal.notification.service;

import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.notification.vo.NotificationLog;

import java.util.List;
import java.util.Map;

public interface NotificationService {
    public static final String BEAN_NAME = "notificationService";

    List<String> findMemberNotRegisterNotificationAcc(String accessModule);

    public List<String> findAllRegisteredNotifAccMembers(boolean isCn);

    public String findAllRegisteredNotifAccMembers(boolean isCn, String memberId);

    public List<String> findRegisteredNotifAccMembersOfFollowers(String memberId);

    List<NotificationAccount> findAllNotifAccByAccessModule(String accessModule);

    public NotificationAccount doCreateNotificationAccount(String memberId, String accessModule, Map<String, String> retDataMap);

    NotificationLog doCreateNotificationLog(String accessModule, String actionType, String requestBody);

    void doUpdateNotificationLog(NotificationLog notificationLog);

    NotificationAccount findNotificationAccoutByAccId(String accid);

    NotificationAccount findNotifAccByMemberAndAccessModule(String memberId, String accessModule);

    String generateAccId();

}
