package com.compalsolutions.compal.notification.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.notification.vo.NotificationLog;

public interface NotificationLogDao extends BasicDao<NotificationLog, String> {
    public static final String BEAN_NAME = "notificationLogDao";

}
