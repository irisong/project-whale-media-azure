package com.compalsolutions.compal.notification.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.notification.vo.NotificationAccount;

import java.util.List;

public interface NotificationAccDao extends BasicDao<NotificationAccount, String> {
    public static final String BEAN_NAME = "notificationAccountDao";

    public NotificationAccount findNotificationAccountByAccId(String accid);

    public NotificationAccount findNotifAccByMemberAndAccessModule(String memberId, String accessModule);

    public List<NotificationAccount> findAllNotifAccByAccessModule(String accessModule);

}
