package com.compalsolutions.compal.notification.client;

import com.compalsolutions.compal.CheckSumBuilder;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.notification.client.dto.YunXinRequestBody;
import com.compalsolutions.compal.notification.client.dto.YunXinRequestBodyBatch;
import com.compalsolutions.compal.notification.client.dto.YunXinResponse;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationLog;
import com.compalsolutions.compal.web.BaseRestUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.core.env.Environment;

import javax.ws.rs.client.Client;
import java.io.IOException;
import java.util.*;

public class YunxinClient {
    private static final Log log = LogFactory.getLog(YunxinClient.class);

    private String serverUrl;
    private String appKey;
    private String appSecret;
    private String nonce;
    private String curTime;
    private String checkSum;

    public YunxinClient() {
        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("yunxin.url");
        appKey = env.getProperty("yunxin.appKey");
        appSecret = env.getProperty("yunxin.appSecret");
    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient(60000, 60000);
    }

    private void refreshhttpHeader() {
        nonce = String.valueOf((10000000 + (int) (Math.random() * ((99999999 - 10000000) + 1))));
        curTime = String.valueOf((new Date()).getTime() / 1000L);
        checkSum = CheckSumBuilder.getCheckSum(appSecret, nonce, curTime);
    }

    private void closeHttpClientConnection(CloseableHttpClient httpClient) {
        try {
            if (httpClient != null) {
                httpClient.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void closeHttpResponseConnection(CloseableHttpResponse response) {
        try {
            if (response != null) {
                response.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public YunXinResponse createUserAccount(String accId, String accessModule) {
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        refreshhttpHeader();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        YunXinResponse yunXinResponse = new YunXinResponse();
        HttpPost httpPost = generateHttpHeader("/user/create.action");
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("accid", accId));
        NotificationLog notificationLog;
        try {

            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            notificationLog = notificationService.doCreateNotificationLog(accessModule, NotificationLog.ACTION_CREATE_ACC,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));

            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);

                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                notificationLog.setResponseBody(jsonString);
                notificationService.doUpdateNotificationLog(notificationLog);
                ObjectMapper objectMapper = new ObjectMapper();
                yunXinResponse = objectMapper.readValue(jsonString, YunXinResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
//            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return yunXinResponse;
    }

    public YunXinResponse getUserToken(String accId, String channelName) {
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        refreshhttpHeader();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        YunXinResponse yunXinResponse = new YunXinResponse();
        HttpPost httpPost = generateHttpHeader("/user/getToken.action");
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("uid", accId));
        nvps.add(new BasicNameValuePair("channelName", channelName));
        nvps.add(new BasicNameValuePair("expireAt", "86400"));
        NotificationLog notificationLog;
        try {

            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            notificationLog = notificationService.doCreateNotificationLog(Global.AccessModule.WHALE_MEDIA, NotificationLog.ACTION_GET_TOKEN,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));

            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);
                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                notificationLog.setResponseBody(jsonString);
                notificationService.doUpdateNotificationLog(notificationLog);
                ObjectMapper objectMapper = new ObjectMapper();
                yunXinResponse = objectMapper.readValue(jsonString, YunXinResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return yunXinResponse;
    }

    private HttpPost generateHttpHeader(String uri) {
        HttpPost httpPost = new HttpPost(serverUrl + uri);
        httpPost.addHeader("AppKey", appKey);
        httpPost.addHeader("Nonce", nonce);
        httpPost.addHeader("CurTime", curTime);
        httpPost.addHeader("CheckSum", checkSum);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8");
        return httpPost;
    }

    public YunXinResponse sendMessageAttach(YunXinRequestBody body, String accessModule, boolean notifyOffline) {
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        SystemConfigService systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME,
                SystemConfigService.class);
        refreshhttpHeader();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        YunXinResponse yunXinResponse = new YunXinResponse();
        HttpPost httpPost = generateHttpHeader("/msg/sendAttachMsg.action");
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        Map<String, String> msgMap = new HashMap<>();
        msgMap.put("body", body.getAttach());
        Gson gson = new Gson();
        String json = gson.toJson(msgMap);
        body.setAttach(json);
        nvps.add(new BasicNameValuePair("from", body.getFrom()));
        nvps.add(new BasicNameValuePair("to", body.getTo()));
        nvps.add(new BasicNameValuePair("msgType", String.valueOf(body.getMsgtype())));
        nvps.add(new BasicNameValuePair("attach", body.getAttach()));
        nvps.add(new BasicNameValuePair("pushcontent", body.getPushContent()));
        nvps.add(new BasicNameValuePair("payload", body.getPayload()));
        log.debug("notifyOffline :" + notifyOffline);
        if (!notifyOffline) {
            nvps.add(new BasicNameValuePair("save", "1"));
        }


        NotificationLog notificationLog;
        try {

            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            notificationLog = notificationService.doCreateNotificationLog(accessModule, NotificationLog.ACTION_SEND_ATTACH_MSG,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));

            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);
                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                notificationLog.setResponseBody(jsonString);
                notificationService.doUpdateNotificationLog(notificationLog);

                ObjectMapper objectMapper = new ObjectMapper();
                yunXinResponse = objectMapper.readValue(jsonString, YunXinResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (
                IOException e) {
            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            closeHttpClientConnection(httpClient);
        }
        return yunXinResponse;
    }


    public YunXinResponse sendBatchMessageAttach(YunXinRequestBodyBatch body, String accessModule, boolean notifyOffline) {
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        refreshhttpHeader();
        CloseableHttpClient httpClient = HttpClients.createDefault();
        YunXinResponse yunXinResponse = new YunXinResponse();
        HttpPost httpPost = generateHttpHeader("/msg/sendBatchAttachMsg.action");
        List<NameValuePair> nvps = new ArrayList<>();
        Gson gson = new Gson();
        if (body.getToAccids() != null) {
            body.setToAccidsStr(gson.toJson(body.getToAccids()));
        }
        nvps.add(new BasicNameValuePair("fromAccid", body.getFromAccid()));
        nvps.add(new BasicNameValuePair("toAccids", body.getToAccidsStr()));
        nvps.add(new BasicNameValuePair("attach", body.getAttach()));
        nvps.add(new BasicNameValuePair("pushcontent", body.getPushContent()));
        nvps.add(new BasicNameValuePair("payload", body.getPayload()));
        if (!notifyOffline) {
            nvps.add(new BasicNameValuePair("save", "1"));
        }

        NotificationLog notificationLog;
        try {

            httpPost.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));

            notificationLog = notificationService.doCreateNotificationLog(accessModule, NotificationLog.ACTION_SEND_BATCH_ATTACH_MSG,
                    EntityUtils.toString(httpPost.getEntity(), "utf-8"));

            CloseableHttpResponse response = null;
            try {
                response = httpClient.execute(httpPost);
                String jsonString;

                jsonString = EntityUtils.toString(response.getEntity(), "utf-8");
                notificationLog.setResponseBody(jsonString);
                notificationService.doUpdateNotificationLog(notificationLog);

                ObjectMapper objectMapper = new ObjectMapper();
                yunXinResponse = objectMapper.readValue(jsonString, YunXinResponse.class);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                closeHttpResponseConnection(response);
            }
        } catch (IOException e) {
//            throw new SystemErrorException(e.getMessage(), e);
        } finally {
            closeHttpClientConnection(httpClient);
        }

        return yunXinResponse;
    }

    public String getServerUrl() {
        return serverUrl;
    }

    public void setServerUrl(String serverUrl) {
        this.serverUrl = serverUrl;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getNonce() {
        return nonce;
    }

    public void setNonce(String nonce) {
        this.nonce = nonce;
    }

    public String getCheckSum() {
        return checkSum;
    }

    public void setCheckSum(String checkSum) {
        this.checkSum = checkSum;
    }

    public String getCurTime() {
        return curTime;
    }

    public void setCurTime(String curTime) {
        this.curTime = curTime;
    }
}
