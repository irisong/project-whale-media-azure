package com.compalsolutions.compal.notification.service;

import com.compalsolutions.compal.member.dao.MemberSqlDao;
import com.compalsolutions.compal.notification.dao.NotificationAccDao;
import com.compalsolutions.compal.notification.dao.NotificationLogDao;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.notification.vo.NotificationLog;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component(NotificationService.BEAN_NAME)
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    private MemberSqlDao memberSqlDao;

    @Autowired
    private NotificationAccDao notificationAccDao;

    @Autowired
    private NotificationLogDao notificationLogDao;

    @Override
    public List<String> findMemberNotRegisterNotificationAcc(String accessModule) {
        return memberSqlDao.findMemberNotRegisterNotificationAcc(accessModule);
    }

    @Override
    public List<String> findAllRegisteredNotifAccMembers(boolean isCn) {
        return memberSqlDao.findAllRegisteredNotifAccMembers(isCn);
    }

    @Override
    public String findAllRegisteredNotifAccMembers(boolean isCn, String memberId) {
        return memberSqlDao.findSpecificAccMembers(isCn, memberId);
    }

    @Override
    public List<String> findRegisteredNotifAccMembersOfFollowers(String memberId) {
        return memberSqlDao.findRegisteredNotifAccMembersOfFollowers(memberId);
    }

    private static final Object synchronizedObject = new Object();

    @Override
    public NotificationAccount doCreateNotificationAccount(String memberId, String accessModule, Map<String, String> retDataMap) {
        NotificationAccount notificationAccount = new NotificationAccount(true);
        synchronized (synchronizedObject) {
            notificationAccount.setAccessModule(accessModule);
            notificationAccount.setAccountId(retDataMap.get("accid"));
            notificationAccount.setToken(retDataMap.get("token"));
            notificationAccount.setMemberId(memberId);
            notificationAccDao.save(notificationAccount);
        }
        return notificationAccount;
    }

    @Override
    public NotificationLog doCreateNotificationLog(String accessModule, String actionType, String requestBody) {
        NotificationLog notificationLog = new NotificationLog(true);
        notificationLog.setAccessModule(accessModule);
        notificationLog.setActionType(actionType);
        notificationLog.setRequestBody(requestBody);
        notificationLogDao.save(notificationLog);
        return notificationLog;
    }

    @Override
    public void doUpdateNotificationLog(NotificationLog notificationLog) {
        notificationLogDao.update(notificationLog);
    }

    @Override
    public NotificationAccount findNotificationAccoutByAccId(String accid) {
        return notificationAccDao.findNotificationAccountByAccId(accid);
    }

    @Override
    public NotificationAccount findNotifAccByMemberAndAccessModule(String memberId, String accessModule) {
        return notificationAccDao.findNotifAccByMemberAndAccessModule(memberId, accessModule);
    }

    @Override
    public List<NotificationAccount> findAllNotifAccByAccessModule(String accessModule) {
        return notificationAccDao.findAllNotifAccByAccessModule(accessModule);
    }

    @Override
    public String generateAccId() {
        String accId;
        do {
            accId = RandomStringUtils.randomAlphanumeric(32).toLowerCase();

        } while (findNotificationAccoutByAccId(accId) != null);
        return accId;
    }
}
