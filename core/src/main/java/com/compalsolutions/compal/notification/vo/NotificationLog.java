package com.compalsolutions.compal.notification.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_notification_log")
@Access(AccessType.FIELD)
public class NotificationLog extends VoBase {

    private static final long serialVersionUID = 1L;

    public static final String ACTION_GET_TOKEN = "GET_TOKEN";
    public static final String ACTION_CREATE_ACC = "CREATE_ACC";
    public static final String ACTION_UPDATE_ACC = "UPDATE_ACC";
    public static final String ACTION_SEND_ATTACH_MSG = "SEND_ATTACH_MSG";
    public static final String ACTION_SEND_BATCH_ATTACH_MSG = "SEND__BATCH_ATTACH_MSG";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @ToUpperCase
    @ToTrim
    @Column(name = "action_type", nullable = false, length = 50)
    private String actionType;

    @ToTrim
    @Column(name = "req_body", columnDefinition = Global.ColumnDef.TEXT)
    private String requestBody;

    @ToTrim
    @Column(name = "resp_body", columnDefinition = Global.ColumnDef.TEXT)
    private String responseBody;

    @ToUpperCase
    @ToTrim
    @Column(name = "access_module", length = 50)
    private String accessModule;

    @ToTrim
    @Column(name = "status", length = 32)
    private String status;


    public NotificationLog() {
    }

    public NotificationLog(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.COMPLETED;
        }
    }


    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String requestBody) {
        this.requestBody = requestBody;
    }

    public String getResponseBody() {
        return responseBody;
    }

    public void setResponseBody(String responseBody) {
        this.responseBody = responseBody;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAccessModule() {
        return accessModule;
    }

    public void setAccessModule(String accessModule) {
        this.accessModule = accessModule;
    }
}
