package com.compalsolutions.compal.notification.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_notif_account")
@Access(AccessType.FIELD)
public class NotificationAccount extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "notif_id", unique = true, nullable = false, length = 32)
    private String notifId;

    @ToTrim
    @Column(name = "acc_id", length = 100, nullable = false)
    private String accountId;

    @ToTrim
    @Column(name = "token", columnDefinition = Global.ColumnDef.TEXT, nullable = false)
    private String token;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "member_id", length = 32, nullable = false)
    private String memberId;

    @ToUpperCase
    @ToTrim
    @Column(name = "access_module", length = 50)
    private String accessModule;

    public NotificationAccount() {
    }

    public NotificationAccount(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getNotifId() {
        return notifId;
    }

    public void setNotifId(String notifId) {
        this.notifId = notifId;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAccessModule() {
        return accessModule;
    }

    public void setAccessModule(String accessModule) {
        this.accessModule = accessModule;
    }
}
