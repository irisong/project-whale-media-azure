package com.compalsolutions.compal.notification;

import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.client.dto.YunXinResponse;

import java.util.List;

public interface NotificationConfProvider {
    public static final String BEAN_NAME = "notificationConfProvider";

    void doCreateMemberNotificationAccount();

    YunXinResponse doSendMessage(String from, String to, String accessModule, String pushContent);

    YunXinResponse doSendMessage(String from, String to, String accessModule, String pushContent, Payload payload, String payloadJsonStr);

    public YunXinResponse doSendMessageAll(String from, List<String> tos, String accessModule, String msg, String title, String lang, Payload payload);

    YunXinResponse doSendMessageBatch(String from, List<String> tos, String accessModule, String pushContent, Payload payload, String payloadJsonStr);

    void addPushNotificationMessage(String accessModule, String memberId, String id, String action, String currency, String name, String msg);

    void doProcessAnnouncementNotification(RunTaskLogger logger);
}
