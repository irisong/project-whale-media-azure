package com.compalsolutions.compal;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.service.LiveCategoryService;
import com.compalsolutions.compal.general.vo.*;
import com.compalsolutions.compal.member.service.MemberReportService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberReportType;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

public class OptionBeanUtil {
    private Locale locale;
    private LanguageFrameworkService languageFrameworkService;
    private CountryService countryService;
    private BroadcastService broadcastService;

    public OptionBeanUtil(Locale locale) {
        this.locale = locale;
        languageFrameworkService = Application.lookupBean(LanguageFrameworkService.BEAN_NAME, LanguageFrameworkService.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
    }

    private String getText(String key) {
        return languageFrameworkService.getText(key, locale, null);
    }

    public OptionBean generateOptionAll() {
        return new OptionBean("", getText("all"));
    }

    public List<OptionBean> getAnnouncementStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.add(createStatusBean(Global.Status.PENDING));
        options.addAll(getAnnouncementStatus());

        return options;
    }

    public List<OptionBean> getBannerStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getBannerStatus());

        return options;
    }

    private OptionBean createStatusBean(String status) {
        return new OptionBean(status, getText(Global.Status.getI18nKey(status)));
    }

    public List<OptionBean> getAnnouncementStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));

        return options;
    }

    public List<OptionBean> getBannerStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));

        return options;
    }

    public List<OptionBean> getBroadcastRank() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(BroadcastConfig.RANK_V1, BroadcastConfig.RANK_V1));
        options.add(new OptionBean(BroadcastConfig.RANK_V2, BroadcastConfig.RANK_V2));
        options.add(new OptionBean(BroadcastConfig.RANK_A1, BroadcastConfig.RANK_A1));
        options.add(new OptionBean(BroadcastConfig.RANK_A2, BroadcastConfig.RANK_A2));
        options.add(new OptionBean(BroadcastConfig.RANK_A3, BroadcastConfig.RANK_A3));
        options.add(new OptionBean(BroadcastConfig.RANK_B1, BroadcastConfig.RANK_B1));
        options.add(new OptionBean(BroadcastConfig.RANK_B2, BroadcastConfig.RANK_B2));
        options.add(new OptionBean(BroadcastConfig.RANK_B3, BroadcastConfig.RANK_B3));
        options.add(new OptionBean(BroadcastConfig.RANK_NONE, BroadcastConfig.RANK_NONE));

        return options;
    }

    public List<OptionBean> getBroadcastRankWithAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getBroadcastRank());

        return options;
    }

    public List<OptionBean> getDocFileStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getDocFileStatus());

        return options;
    }

    public List<OptionBean> getDocFileStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));

        return options;
    }

    public List<OptionBean> getLanguagesWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getLanguages());

        return options;
    }

    public List<OptionBean> getLanguages() {
        List<OptionBean> options = new ArrayList<>();
        List<Language> languages = languageFrameworkService.findLanguages();
        for (Language language : languages) {
            options.add(new OptionBean(language.getLanguageCode(), language.getLanguageName()));
        }

        return options;
    }

    public List<OptionBean> getPublishGroups() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Global.PublishGroup.PUBLIC_GROUP, getText("public")));
//        options.add(new OptionBean(Global.PublishGroup.MEMBER_GROUP, getText("member")));
//        options.add(new OptionBean(Global.PublishGroup.AGENT_GROUP, getText("agent")));
//        options.add(new OptionBean(Global.PublishGroup.ADMIN_GROUP, getText("admin")));

        return options;
    }

    public List<OptionBean> getAnnouncementTypeWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();

        options.add(generateOptionAll());
        options.addAll(getAnnouncementType());

        return options;
    }

    public List<OptionBean> getAnnouncementType() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Global.AnnouncementType.CONTENT, getText("content")));

        return options;
    }

    public List<OptionBean> getBannerType() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Banner.OPEN_WEB_URL, getText("WEBVIEW")));
        options.add(new OptionBean(Banner.OPEN_VJ_PROFILE, getText("VJ_PROFILE")));

        return options;
    }

    public List<OptionBean> getBannerTypeWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();

        options.add(generateOptionAll());
        options.addAll(getBannerType());

        return options;
    }

    public List<OptionBean> getEmailqStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getEmailqStatus());

        return options;
    }

    public List<OptionBean> getEmailqStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(Emailq.EMAIL_STATUS_PENDING, getText("statPending")));
        options.add(new OptionBean(Emailq.EMAIL_STATUS_SENT, getText("statSent")));

        return options;
    }

    public List<OptionBean> getWalletAdjustTypes() {
        List<OptionBean> optionBeans = new ArrayList<>();
        optionBeans.add(new OptionBean(Global.WalletAdjustType.IN, getText("in")));
        optionBeans.add(new OptionBean(Global.WalletAdjustType.OUT, getText("out")));

        return optionBeans;
    }

    public List<OptionBean> getWalletAdjustTypesWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getWalletAdjustTypes());
        return options;
    }

    public List<OptionBean> getMemberStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));

        return options;
    }

    public List<OptionBean> getMemberStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getMemberStatus());
        return options;
    }

    public List<OptionBean> getYesNoOptions() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean("true", getText("yes")));
        options.add(new OptionBean("false", getText("no")));
        return options;
    }

    public List<OptionBean> getYesNoWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getYesNoOptions());
        return options;
    }

    public List<OptionBean> getStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));
        return options;
    }

    public List<OptionBean> getStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getStatus());
        return options;
    }

    public List<OptionBean> getGendersWithPleaseSelect() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean("", getText("please_select")));
        options.addAll(getGenders());

        return options;
    }

    public List<OptionBean> getGenders() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(Global.Gender.MALE, getText("male")));
        options.add(new OptionBean(Global.Gender.FEMALE, getText("female")));

        return options;
    }

    public List<OptionBean> getIdentityTypes() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(Global.IdentityType.PASSPORT, getText("passport")));
        options.add(new OptionBean(Global.IdentityType.DRIVING_LICENCE, getText("drivingLicence")));
        options.add(new OptionBean(Global.IdentityType.IDENTITY_CARD, getText("idCard")));
        options.add(new OptionBean(Global.IdentityType.OTHER, getText("other")));
        return options;
    }

    public List<OptionBean> getWalletTypeConfigStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));

        return options;
    }

    public List<OptionBean> getWalletTransferAdminFeeTypes() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(String.valueOf(true), getText("amountPerTransaction")));
        options.add(new OptionBean(String.valueOf(false), getText("percentOfTransferAmount")));

        return options;
    }

    public List<OptionBean> getWalletTransferTypeConfigStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.INACTIVE));

        return options;
    }

    public List<OptionBean> getSystemConfigImageType() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Global.SystemConfigImageType.ADMIN_DASHBOARD, getText("adminDashboard")));
        options.add(new OptionBean(Global.SystemConfigImageType.STORE_FRONT, getText("storeFront")));
        options.add(new OptionBean(Global.SystemConfigImageType.EMAIL_TEMPLATE, getText("emailTemplate")));

        return options;
    }

    public List<OptionBean> getWithdrawTypes() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(Global.WalletWithdrawType.BITCOIN, getText("bitcoin_btc")));
        options.add(new OptionBean(Global.WalletWithdrawType.ETHERCOIN, getText("ethereum_eth")));
        return options;
    }

    public List<OptionBean> getWithdrawTypesWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getWithdrawTypes());
        return options;
    }

    public List<OptionBean> getWalletTopupStatus(Locale locale) {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.PAYMENT_PENDING));
        options.add(createStatusBean(Global.Status.APPROVED));
        return options;
    }

    public List<OptionBean> getWalletTopupStatusWithOptionAll(Locale locale) {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getWalletTopupStatus(locale));
        return options;
    }

    public List<OptionBean> getWalletWithdrawStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.APPROVED));
        options.add(createStatusBean(Global.Status.PENDING));
        options.add(createStatusBean(Global.Status.REJECTED));
        options.add(createStatusBean(Global.Status.CANCELLED));
        options.add(createStatusBean(Global.Status.ERROR));
        options.add(createStatusBean(Global.Status.SCHEDULED));
        return options;
    }

    public List<OptionBean> getWalletWithdrawStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getWalletWithdrawStatus());
        return options;
    }

    public List<OptionBean> getWalletWithdrawCryptoTypesWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getWalletWithdrawCryptoTypes());
        return options;
    }

    public List<OptionBean> getWalletWithdrawCryptoTypes() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(Global.WalletType.ETH, Global.WalletType.ETH));
        options.add(new OptionBean(Global.WalletType.BTC, Global.WalletType.BTC));
        options.add(new OptionBean(Global.WalletType.USDT, Global.WalletType.USDT));
        return options;
    }

    public List<OptionBean> getWalletWithdraw2CryptoStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.APPROVED));
        options.add(createStatusBean(Global.Status.PENDING));
        options.add(createStatusBean(Global.Status.REJECTED));
        return options;
    }

    public List<OptionBean> getWalletWithdraw2CryptoStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getWalletWithdraw2CryptoStatus());
        return options;
    }

    public List<OptionBean> getSmsqStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getSmsqStatus());

        return options;
    }

    public List<OptionBean> getSmsqStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(SmsQueue.SMS_STATUS_PENDING, getText("statPending")));
        options.add(new OptionBean(SmsQueue.SMS_STATUS_SENT, getText("statSent")));

        return options;
    }

    public List<OptionBean> getWalletStatementShowDescOptions() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean("true", getText("language_english")));
        options.add(new OptionBean("false", getText("language_chinese")));

        return options;
    }

    public List<OptionBean> getIdentityUpdateStatusCodeWithOption() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Member.KYC_STATUS_APPROVED, getText("approved")));
        options.add(new OptionBean(Member.KYC_STATUS_REJECT, getText("statRejectedUpload")));

        return options;
    }

    public List<OptionBean> getIdentityStatusCodeWithOption() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Member.KYC_STATUS_PENDING_UPLOAD, getText("new")));
        options.add(new OptionBean(Member.KYC_STATUS_PENDING_APPROVAL, getText("statPendingApproval")));
        options.add(new OptionBean(Member.KYC_STATUS_REJECT, getText("statRejectedUpload")));
        options.add(new OptionBean(Member.KYC_STATUS_APPROVED, getText("approved")));

        return options;
    }

    public List<OptionBean> getIdentityStatusCodeWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();

        options.add(generateOptionAll());
        options.addAll(getIdentityStatusCodeWithOption());

        return options;
    }

    public List<OptionBean> getWalletTopupPackageStatus() {
        List<OptionBean> options = new ArrayList<>();

        options.add(new OptionBean(Global.WalletPackageStatus.DISPLAY, Global.WalletPackageStatus.DISPLAY));
        options.add(new OptionBean(Global.WalletPackageStatus.HIDE, Global.WalletPackageStatus.HIDE));

        return options;
    }

    public List<OptionBean> getPointReportType() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(PointTrx.ALL, getText("all")));
        options.add(new OptionBean(PointTrx.DAILY, getText("daily")));
        options.add(new OptionBean(PointTrx.WEEKLY, getText("weekly")));
        options.add(new OptionBean(PointTrx.MONTHLY, getText("monthly")));
        return options;
    }

    public List<OptionBean> getTrxHistoryRemark() {
        List<OptionBean> options = new ArrayList<>();
        //options.add(new OptionBean(WalletTrx.OMC_WALLET, getText(WalletTrx.OMC_WALLET)));
        options.add(new OptionBean(WalletTrx.APPLE_PAY, getText(WalletTrx.APPLE_PAY)));
        options.add(new OptionBean(WalletTrx.RAZER_PAY, getText(WalletTrx.RAZER_PAY)));
        options.add(new OptionBean(WalletTrx.GOOGLE_PAY, getText(WalletTrx.GOOGLE_PAY)));
        //options.add(new OptionBean(WalletTrx.WEBSITE_OMC_WALLET, getText(WalletTrx.WEBSITE_OMC_WALLET)));
        return options;
    }

    public List<OptionBean> getTrxHistoryRemarkWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getTrxHistoryRemark());
        return options;
    }

    public List<OptionBean> getOMCTrxHistoryMark() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(WalletTrx.OMC_WALLET, getText(WalletTrx.OMC_WALLET)));
        options.add(new OptionBean(WalletTrx.WEBSITE_OMC_WALLET, getText(WalletTrx.WEBSITE_OMC_WALLET)));
        return options;
    }

    public List<OptionBean> getOMCTrxHistoryMarkWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getOMCTrxHistoryMark());
        return options;
    }


    public List<OptionBean> getPointReportUserType() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(PointTrx.CONTRIBUTOR, getText("contributor")));
        options.add(new OptionBean(PointTrx.POPULAR, getText("popular")));
        return options;
    }

    public List<OptionBean> getHelpDeskTypesByLanguage(String language, String status) {
        HelpDeskService helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
        List<HelpDeskType> helpDeskTypes = helpDeskService.findHelpDeskTypes(status);

        List<OptionBean> options = new ArrayList<>();
        for (HelpDeskType helpDeskType : helpDeskTypes) {
            switch (language) {
                case Global.Language.CHINESE:
                    options.add(new OptionBean(helpDeskType.getTicketTypeId(), helpDeskType.getTypeNameCn()));
                    break;
                case Global.Language.MALAY:
                    options.add(new OptionBean(helpDeskType.getTicketTypeId(), helpDeskType.getTypeNameMs()));
                    break;
                default:
                    options.add(new OptionBean(helpDeskType.getTicketTypeId(), helpDeskType.getTypeName()));
                    break;
            }
        }

        return options;
    }

    public List<OptionBean> getHelpDeskStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(HelpDesk.STATUS_CLOSE));
        options.add(createStatusBean(HelpDesk.STATUS_OPEN));
        return options;
    }

    public List<OptionBean> getActiveLiveStreamCategoryForMember() {
        LiveCategoryService liveCategoryService = Application.lookupBean(LiveCategoryService.BEAN_NAME, LiveCategoryService.class);
        List<LiveCategory> liveCategoryList = liveCategoryService.doFindAllLiveCategory(false);

        return getLiveStreamCategory(liveCategoryList);
    }

    public List<OptionBean> getLiveStreamCategory(List<LiveCategory> liveCategoryList) {
        List<OptionBean> options = new ArrayList<>();
        for (LiveCategory liveCategory : liveCategoryList) {
            if (locale.equals(Global.LOCALE_CN)) {
                options.add(new OptionBean(liveCategory.getType(), liveCategory.getCategoryCn()));
            } else if (locale.equals(Global.LOCALE_MS)) {
                options.add(new OptionBean(liveCategory.getType(), liveCategory.getCategoryMs()));
            } else {
                options.add(new OptionBean(liveCategory.getType(), liveCategory.getCategory()));
            }
        }

        return options;
    }

    public List<OptionBean> getCountryDescList() {
        List<CountryDesc> countryDescList = countryService.findCountryDescsByLocale(locale);
        List<OptionBean> countryOptionList = countryDescList.stream()
                .map(countryDesc -> new OptionBean(countryDesc.getCountryCode(), countryDesc.getCountryName()))
                .collect(Collectors.toList());

        return countryOptionList;
    }


    public List<OptionBean> getFansBadgeStatus() {
        List<OptionBean> options = new ArrayList<>();
        options.add(createStatusBean(Global.Status.PENDING));
        options.add(createStatusBean(Global.Status.ACTIVE));
        options.add(createStatusBean(Global.Status.REJECTED));

        return options;
    }

    public List<OptionBean> getImageType() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(SymbolicItem.GIFT_ANIMATION, "Animation"));
        return options;
    }

    public List<OptionBean> getMemberRankWithPleaseSelect() {
        List<BroadcastConfig> broadcastConfigList = broadcastService.findAllBroadcastConfig();
        List<OptionBean> broadcastConfigOptionList = new ArrayList<>();
        broadcastConfigOptionList.add(new OptionBean("", getText("please_select")));
        broadcastConfigOptionList = broadcastConfigList.stream()
                .map(broadcastConfig -> new OptionBean(broadcastConfig.getId(), broadcastConfig.getRankName()))
                .collect(Collectors.toList());

        return broadcastConfigOptionList;
    }

    public List<OptionBean> getMemberReportTypeByLanguage(String language, String status) {
        MemberReportService memberReportService = Application.lookupBean(MemberReportService.BEAN_NAME, MemberReportService.class);
        List<MemberReportType> memberReportTypes = memberReportService.findMemberReportTypes(status);

        List<OptionBean> options = new ArrayList<>();
        for (MemberReportType memberReportType : memberReportTypes) {
            switch (language) {
                case Global.Language.CHINESE:
                    options.add(new OptionBean(memberReportType.getReportTypeId(), memberReportType.getTypeNameCn()));
                    break;
                case Global.Language.MALAY:
                    options.add(new OptionBean(memberReportType.getReportTypeId(), memberReportType.getTypeNameMs()));
                    break;
                default:
                    options.add(new OptionBean(memberReportType.getReportTypeId(), memberReportType.getTypeName()));
                    break;
            }
        }

        return options;
    }

    public List<OptionBean> getSymbolicItemFileType () {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(SymbolicItem.GIFT_IMAGE, "IMAGE"));
        options.add(new OptionBean(SymbolicItem.GIFT_ANIMATION, "ANIMATION"));
        return  options;
    }

    public List<OptionBean> getMonthWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.add(new OptionBean("01", "January"));
        options.add(new OptionBean("02", "February"));
        options.add(new OptionBean("03", "March"));
        options.add(new OptionBean("04", "April"));
        options.add(new OptionBean("05", "May"));
        options.add(new OptionBean("06", "June"));
        options.add(new OptionBean("07", "July"));
        options.add(new OptionBean("08", "August"));
        options.add(new OptionBean("09", "September"));
        options.add(new OptionBean("10", "October"));
        options.add(new OptionBean("11", "November"));
        options.add(new OptionBean("12", "December"));
        return options;
    }

    public List<OptionBean> getYearWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        int j = 2020;

        for(int i =0;i<10;i++)
        {
            String years = Integer.toString(j+i);
            options.add(new OptionBean(years, years));
        }

        return options;
    }

    public List<OptionBean> getTopupwayWithOptionAll()
    {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.add(new OptionBean("OMC wallet", "OMC wallet"));
        options.add(new OptionBean("APPLE PAY", "APPLE PAY"));
        options.add(new OptionBean("GOOGLE PAY", "GOOGLE PAY"));
        return options;
    }

    public List<OptionBean> getMerchantType() {
        List<OptionBean> options = new ArrayList<>();
        options.add(new OptionBean(Merchant.TYPE_COMPANY, "Company"));
        options.add(new OptionBean(Merchant.TYPE_PERSONAL, "Personal"));
        return options;
    }

    public List<OptionBean> getMerchantTypeWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.addAll(getMerchantType());
        return options;
    }
    public List<OptionBean> allGenderList() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.add(new OptionBean("M", "Male"));
        options.add(new OptionBean("F", "Female"));
        return options;
    }

    public List<OptionBean> getMissionStatusWithOptionAll() {
        List<OptionBean> options = new ArrayList<>();
        options.add(generateOptionAll());
        options.add(new OptionBean(VjMission.STATUS_PENDING, "Pending"));
        options.add(new OptionBean(VjMission.STATUS_ACTIVE, "Active"));
        options.add(new OptionBean(VjMission.STATUS_REJECTED, "Rejected"));
        options.add(new OptionBean(VjMission.STATUS_ENDED, "Ended"));

        return options;
    }
}
