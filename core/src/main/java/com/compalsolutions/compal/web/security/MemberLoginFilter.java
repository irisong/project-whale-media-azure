package com.compalsolutions.compal.web.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.filter.GenericFilterBean;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.web.ServletUtil;

public class MemberLoginFilter extends GenericFilterBean {
    private static final Log log = LogFactory.getLog(MemberLoginFilter.class);

    private String authenticationFailureUrl;
    private String loginProcessingUrl = "/login";

    private String userTypeParameter = "userType";

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;

            String memberLogin = req.getParameter(userTypeParameter);

            if (loginProcessingUrl.equals(req.getRequestURI())) {
                SystemConfig systemConfig = WebUtil.getSystemConfig();
                if (!systemConfig.getMemberLogin() && Global.UserType.MEMBER.equalsIgnoreCase(memberLogin)) {
                    log.debug("member login is disabled");

                    if (ServletUtil.isAjaxRequest(req)) {
                        response.setContentType("application/json");
                        PrintWriter out = response.getWriter();
                        out.print("{\"error\":\"OFFLINE\"}");
                        out.flush();
                    } else {
                        resp.sendRedirect(authenticationFailureUrl);
                    }

                    return;
                }
            }
        }

        chain.doFilter(request, response);
    }

    // ---------------- GETTER & SETTER (START) -------------

    public void setUserTypeParameter(String userTypeParameter) {
        this.userTypeParameter = userTypeParameter;
    }

    public void setAuthenticationFailureUrl(String authenticationFailureUrl) {
        this.authenticationFailureUrl = authenticationFailureUrl;
    }

    public void setLoginProcessingUrl(String loginProcessingUrl) {
        this.loginProcessingUrl = loginProcessingUrl;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
