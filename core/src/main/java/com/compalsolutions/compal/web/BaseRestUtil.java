package com.compalsolutions.compal.web;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Feature;

import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

public class BaseRestUtil {

    public static Client newJerseyClient(int connectTimeout, int readTimeout) {
        // Client client = new JerseyClientBuilder().build();
        Client client = ClientBuilder.newClient();

        Feature feature = new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO, LoggingFeature.Verbosity.PAYLOAD_ANY, 10000);
        client.register(feature);
        client.register(MultiPartFeature.class);
        client.property(ClientProperties.CONNECT_TIMEOUT, connectTimeout);
        client.property(ClientProperties.READ_TIMEOUT, readTimeout);
        return client;
    }

    public static Client newJerseyClient() {
        return newJerseyClient(30000, 30000);
    }
}
