package com.compalsolutions.compal.web.security;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.util.Assert;
import org.springframework.web.filter.GenericFilterBean;

import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.web.ServletUtil;

public class CaptchaVerifyFilter extends GenericFilterBean {
    private static final Log log = LogFactory.getLog(CaptchaVerifyFilter.class);

    private String authenticationFailureUrl;
    private String loginProcessingUrl = "/login";

    private String securityCodeParameter = "securityCode";
    private String usernameParameter = SPRING_SECURITY_FORM_USERNAME_KEY;

    private ServerConfiguration serverConfiguration;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse resp = (HttpServletResponse) response;
            HttpSession session = ((HttpServletRequest) request).getSession();

            if (loginProcessingUrl.equals(req.getRequestURI())) {
                String securityCode = request.getParameter(securityCodeParameter);
                String expectedKey = (String) session.getAttribute(FrameworkConst.CAPTCHA_KEY);
                // skip captcha checking if is development mode
                if (serverConfiguration.isProductionMode() && !StringUtils.equalsIgnoreCase(securityCode, expectedKey)) {
                    SessionLogService sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);

                    String username = obtainUsername(req);
                    sessionLogService.saveSessionLog(username, ServletUtil.getRemoteAddr(req), SessionLog.LOGIN_STATUS_FAILED_CAPTCHA, null);

                    log.debug("securityCode is invalid...");

                    if (ServletUtil.isAjaxRequest(req)) {
                        response.setContentType("application/json");
                        PrintWriter out = response.getWriter();
                        out.print("{\"error\":\"" + SessionLog.LOGIN_STATUS_FAILED_CAPTCHA + "\"}");
                        out.flush();
                    } else {
                        resp.sendRedirect(authenticationFailureUrl);
                    }
                    return;
                }
            }
        }

        chain.doFilter(request, response);
    }

    @Override
    public void afterPropertiesSet() throws ServletException {
        Assert.notNull(authenticationFailureUrl, "authenticationFailureUrl cannot be null");
        Assert.notNull(serverConfiguration, "serverConfiguration cannot be null");
        Assert.notNull(loginProcessingUrl, "loginProcessingUrl cannot be null");
    }

    private String obtainUsername(HttpServletRequest request) {
        return request.getParameter(usernameParameter);
    }

    // ---------------- GETTER & SETTER (START) -------------

    public void setAuthenticationFailureUrl(String authenticationFailureUrl) {
        this.authenticationFailureUrl = authenticationFailureUrl;
    }

    public void setServerConfiguration(ServerConfiguration serverConfiguration) {
        this.serverConfiguration = serverConfiguration;
    }

    public void setSecurityCodeParameter(String securityCodeParameter) {
        this.securityCodeParameter = securityCodeParameter;
    }

    public void setLoginProcessingUrl(String loginProcessingUrl) {
        this.loginProcessingUrl = loginProcessingUrl;
    }

    public void setUsernameParameter(String usernameParameter) {
        this.usernameParameter = usernameParameter;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
