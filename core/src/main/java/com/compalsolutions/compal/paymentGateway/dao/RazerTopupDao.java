package com.compalsolutions.compal.paymentGateway.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.paymentGateway.vo.RazerTopup;

public interface RazerTopupDao extends BasicDao<RazerTopup, String> {
    public static final String BEAN_NAME = "razerTopupDao";

    public RazerTopup findRazerTopup(String orderId);
}
