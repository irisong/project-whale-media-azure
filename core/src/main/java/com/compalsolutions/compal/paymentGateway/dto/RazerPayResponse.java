package com.compalsolutions.compal.paymentGateway.dto;

import java.util.Date;

public class RazerPayResponse {
    private String tranID;
    private String orderid;
    private String status;
    private String domain;
    private String nbcb;
    private String amount;
    private String currency;
    private String skey;
    private String paydate;
    private String appcode;
    private String error_code;
    private String error_desc;

    public String getTranID() {
        return tranID;
    }

    public void setTranID(String tranID) {
        this.tranID = tranID;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getNbcb() {
        return nbcb;
    }

    public void setNbcb(String nbcb) {
        this.nbcb = nbcb;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getAppcode() {
        return appcode;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_desc() {
        return error_desc;
    }

    public void setError_desc(String error_desc) {
        this.error_desc = error_desc;
    }

    @Override
    public String toString() {
        return "RazerPayResponse{" +
                "tranID=" + tranID +
                ", orderid='" + orderid + '\'' +
                ", status=" + status +
                ", domain='" + domain + '\'' +
                ", nbcb=" + nbcb +
                ", amount=" + amount +
                ", currency='" + currency + '\'' +
                ", skey='" + skey + '\'' +
                ", paydate=" + paydate +
                ", appcode='" + appcode + '\'' +
                ", error_code='" + error_code + '\'' +
                ", error_desc='" + error_desc + '\'' +
                '}';
    }
}
