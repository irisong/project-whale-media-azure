package com.compalsolutions.compal.paymentGateway.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.paymentGateway.vo.PaymentGatewayLog;

public interface PaymentGatewayLogDao extends BasicDao<PaymentGatewayLog, String> {
    public static final String BEAN_NAME = "paymentGatewayLogDao";
}
