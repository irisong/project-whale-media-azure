package com.compalsolutions.compal.paymentGateway.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.paymentGateway.vo.PaymentGatewayLog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(PaymentGatewayLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class PaymentGatewayLogDaoImpl extends Jpa2Dao<PaymentGatewayLog, String> implements PaymentGatewayLogDao {
    public PaymentGatewayLogDaoImpl() {
        super(new PaymentGatewayLog(false));
    }

}
