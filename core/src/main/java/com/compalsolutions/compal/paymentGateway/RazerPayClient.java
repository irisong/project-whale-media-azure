package com.compalsolutions.compal.paymentGateway;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.web.BaseRestUtil;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.Validate;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.env.Environment;

import javax.ws.rs.client.Client;
import java.math.BigDecimal;

public class RazerPayClient {
    private static Log log = LogFactory.getLog(RazerPayClient.class);

    private String serverUrl;

    private static final String merchantId = "whalemediamy";

    private static final String VERIFYKEY = "9d699eb993b69c0051b78ea97b9e2f3e";

    private static final String SECRETKEY = "b7250d851df7ff1573cc3b0cac54a793";

    private String vCode;

    public RazerPayClient() {
        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("razer.url");
        Validate.notBlank(serverUrl);

    }

    private Client newClient() {
        return BaseRestUtil.newJerseyClient(60000, 60000);
    }

    public String generateSignature(String orderId, BigDecimal amount) {
        return DigestUtils.md5Hex(amount + merchantId + orderId + VERIFYKEY);
    }

    public String generateSignature(String trxId, String orderId, String status,
                                    String amount, String currency, String payDate, String appCode) {
        String preSkey = DigestUtils.md5Hex(trxId + orderId + status + merchantId + amount + currency);
        String finalSkey = DigestUtils.md5Hex(payDate + merchantId + preSkey + appCode + SECRETKEY);
//        $key0 = md5( $tranID.$orderid.$status.$merchant.$amount.$currency );
//        $key1 = md5( $paydate.$merchant.$key0.$appcode.$vkey );

//        System.out.println(finalSkey);
        return finalSkey;
    }

}
