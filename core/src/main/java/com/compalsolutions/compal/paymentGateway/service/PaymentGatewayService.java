package com.compalsolutions.compal.paymentGateway.service;

import com.compalsolutions.compal.paymentGateway.dto.RazerPayResponse;
import com.compalsolutions.compal.paymentGateway.vo.PaymentGatewayLog;
import com.compalsolutions.compal.paymentGateway.vo.RazerTopup;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Map;

public interface PaymentGatewayService {
    public static final String BEAN_NAME = "paymentGatewayService";

    public void doSavePaymentGatewayLog(PaymentGatewayLog paymentGatewayLog);

    public void doSaveRazerPay(RazerTopup razerTopup);

    public void doTopUpWhaleCoinUsingRazerPayByWebpage(Locale locale, RazerPayResponse razerPayResponse);

    public RazerTopup doTopUpWhaleCoinUsingRazerPayByWebpage(Locale locale, String memberId,
                                                             BigDecimal whaleCoinAmount_topUp,
                                                             BigDecimal fromWalletAmt_topUp);

    public Map<Double, Double> goldCoin2Myr();

    public Map<Double, Double> goldCoin2MyrPromo();

    public Map<Double, Double> goldCoin2MyrPromoPercent();

    public Map<Double, Double> goldCoin2Omc();

    public Map<Double, Double> goldCoin2OmcPromo();

    public Map<Double, Double> goldCoin2OmcPromoPercent();
}
