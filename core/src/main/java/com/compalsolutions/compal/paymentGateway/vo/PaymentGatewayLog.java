package com.compalsolutions.compal.paymentGateway.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "lg_payment_gateway")
@Access(AccessType.FIELD)
public class PaymentGatewayLog extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String IOS_IN_APP_PURCHASE = "IOS_IN_APP_PURCHASE";
    public static final String ANDROID_IN_APP_PURCHASE = "ANDROID_IN_APP_PURCHASE";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @Column(name = "send_param", columnDefinition = "text")
    private String sendParam;

    @Column(name = "return_message", columnDefinition = "text")
    private String returnMessage;

    @Column(name = "result", columnDefinition = "text")
    private String result;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "member_id", length = 32, nullable = false)
    private String memberId;

    @ToTrim
    @Column(name = "action", length = 50, nullable = false)
    private String action;

    @ToTrim
    @Column(name = "platform", length = 50, nullable = false)
    private String platform;

    public PaymentGatewayLog() {
    }

    public PaymentGatewayLog(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getSendParam() {
        return sendParam;
    }

    public void setSendParam(String sendParam) {
        this.sendParam = sendParam;
    }

    public String getReturnMessage() {
        return returnMessage;
    }

    public void setReturnMessage(String returnMessage) {
        this.returnMessage = returnMessage;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
