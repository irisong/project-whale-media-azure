package com.compalsolutions.compal.paymentGateway.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.paymentGateway.RazerPayClient;
import com.compalsolutions.compal.paymentGateway.dao.PaymentGatewayLogDao;
import com.compalsolutions.compal.paymentGateway.dao.RazerTopupDao;
import com.compalsolutions.compal.paymentGateway.dto.RazerPayResponse;
import com.compalsolutions.compal.paymentGateway.vo.PaymentGatewayLog;
import com.compalsolutions.compal.paymentGateway.vo.RazerTopup;
import com.compalsolutions.compal.util.UniqueNumberGenerator;
import com.compalsolutions.compal.wallet.dao.WalletBalanceDao;
import com.compalsolutions.compal.wallet.dao.WalletTrxDao;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Component(PaymentGatewayService.BEAN_NAME)
public class PaymentGatewayServiceImpl implements PaymentGatewayService {
    private static final Log log = LogFactory.getLog(PaymentGatewayService.class);

    private PaymentGatewayLogDao paymentGatewayLogDao;

    private RazerTopupDao razerTopupDao;

    private WalletTrxDao walletTrxDao;

    private WalletBalanceDao walletBalanceDao;

    @Autowired
    public PaymentGatewayServiceImpl(PaymentGatewayLogDao paymentGatewayLogDao, RazerTopupDao razerTopupDao,
                                     WalletTrxDao walletTrxDao, WalletBalanceDao walletBalanceDao) {
        this.paymentGatewayLogDao = paymentGatewayLogDao;
        this.razerTopupDao = razerTopupDao;
        this.walletTrxDao = walletTrxDao;
        this.walletBalanceDao = walletBalanceDao;
    }

    @Override
    public void doSavePaymentGatewayLog(PaymentGatewayLog paymentGatewayLog) {
        paymentGatewayLogDao.save(paymentGatewayLog);
    }

    @Override
    public void doSaveRazerPay(RazerTopup razerTopup) {
        razerTopupDao.save(razerTopup);
    }


    private static final Object synchronizedRazerTopUpObject = new Object();

    public RazerTopup findRazerTopup(String orderId) {
        return razerTopupDao.findRazerTopup(orderId);
    }

    @Override
    public void doTopUpWhaleCoinUsingRazerPayByWebpage(Locale locale, RazerPayResponse razerPayResponse) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        RazerTopup razerTopup = razerTopupDao.findRazerTopup(razerPayResponse.getOrderid());

        if (razerTopup == null) {
            throw new ValidatorException("invalidWhaleCoinAmount_topUp", locale);
        }

        synchronized (synchronizedRazerTopUpObject) {
            if (razerPayResponse.getStatus().equals("11")) {
                razerTopup.setStatus(Global.Status.FAILED);
                razerTopup.setTransId(razerPayResponse.getTranID());
                razerTopup.setRemark(razerPayResponse.getError_code() + ":" + razerPayResponse.getError_desc());
                razerTopupDao.update(razerTopup);
            } else if (razerPayResponse.getStatus().equals("00")) {
                if (!razerTopup.getStatus().equalsIgnoreCase(Global.Status.COMPLETED)) {
                    RazerPayClient razerPayClient = new RazerPayClient();
                    if (BDUtil.e(razerTopup.getAmount(), new BigDecimal(razerPayResponse.getAmount()))) {
                        // check if Skey if correct, not from hacker
                        String skey = razerPayClient.generateSignature(razerPayResponse.getTranID(), razerTopup.getOrderId(),
                                razerPayResponse.getStatus(), razerPayResponse.getAmount(), razerPayResponse.getCurrency(),
                                razerPayResponse.getPaydate(), razerPayResponse.getAppcode());
                        if (skey.equals(razerPayResponse.getSkey())) {
                            BigDecimal promoAmount = BigDecimal.ZERO;
                            Double promoPercent = 0D;
                            Map<Double, Double> goldCoin2MyrPromo = goldCoin2MyrPromo();
                            Map<Double, Double> goldCoin2MyrPromoPercent = goldCoin2MyrPromoPercent();

                            for (Map.Entry<Double, Double> entry : goldCoin2MyrPromo.entrySet()) {
                                if (BDUtil.e(razerTopup.getAmountTo(), new BigDecimal("" + entry.getKey()))) {
                                    promoAmount = new BigDecimal(entry.getValue().toString());
                                    promoPercent = goldCoin2MyrPromoPercent.get(entry.getKey());
                                    break;
                                }
                            }
                            WalletTrx walletTrx = new WalletTrx(true);
                            walletTrx.setInAmt(razerTopup.getAmountTo().add(promoAmount));
                            walletTrx.setOutAmt(BigDecimal.ZERO);
                            walletTrx.setOwnerId(razerTopup.getOwnerId());
                            walletTrx.setOwnerType(Global.UserType.MEMBER);
                            walletTrx.setTrxDatetime(new Date());
                            walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_TOPUP);
                            walletTrx.setWalletRefId(razerTopup.getId());
                            walletTrx.setWalletRefno(razerTopup.getOrderId());
                            walletTrx.setWalletType(Global.WalletType.WALLET_80);
                            walletTrx.setRemark("Website-Razer Pay");

                            // using systemLocale
                            if (promoPercent > 0) {
                                walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXWithPromo", razerTopup.getAmountTo(), promoPercent, walletTrx.getWalletRefno()));
                                walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXWithPromo", Global.LOCALE_CN, razerTopup.getAmountTo(),
                                        promoPercent, walletTrx.getWalletRefno()));
                            } else {
                                walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtX", razerTopup.getAmountTo(), walletTrx.getWalletRefno()));
                                walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtX", Global.LOCALE_CN, razerTopup.getAmountTo(),
                                        walletTrx.getWalletRefno()));
                            }
                            walletTrxDao.save(walletTrx);

                            walletBalanceDao.doCreditAvailableBalance(razerTopup.getOwnerId(), walletTrx.getOwnerType(), Global.WalletType.WALLET_80,
                                    razerTopup.getAmountTo().add(promoAmount));

                            razerTopup.setPayDate(new Date());
                            razerTopup.setTransId(razerPayResponse.getTranID());
                            razerTopup.setStatus(Global.Status.COMPLETED);
                        } else {
                            razerTopup.setTransId(razerPayResponse.getTranID());
                            razerTopup.setRemark("SKEY INCORRECT");
                        }
                    } else {
                        log.debug("RAZOR TOPUP AMOUNT ERROR:" + razerTopup.getAmount() + ";" +
                                razerPayResponse.getAmount());
                        razerTopup.setTransId(razerPayResponse.getTranID());
                        razerTopup.setRemark("RAZOR TOPUP AMOUNT ERROR");
                    }
                    razerTopupDao.update(razerTopup);
                } else {
                    log.debug("RAZOR TOPUP COMPLETED BEFORE");
                }
            }
        }
    }

    private static final Object synchronizedRazerTopUpNewObject = new Object();

    @Override
    public RazerTopup doTopUpWhaleCoinUsingRazerPayByWebpage(Locale locale, String memberId,
                                                             BigDecimal whaleCoinAmount_topUp,
                                                             BigDecimal fromWalletAmt_topUp) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);
        synchronized (synchronizedRazerTopUpNewObject) {
            RazerPayClient razerPayClient = new RazerPayClient();
            Map<Double, Double> goldCoin2Myr = goldCoin2Myr();
            boolean isValid = false;
            BigDecimal myrAmount = BigDecimal.ZERO;
            for (Map.Entry<Double, Double> entry : goldCoin2Myr.entrySet()) {
                if (BDUtil.e(whaleCoinAmount_topUp, new BigDecimal("" + entry.getKey()))) {
                    isValid = true;
                    myrAmount = new BigDecimal("" + entry.getValue());
                    break;
                }
            }
            log.debug("isValid" + isValid);
            if (!isValid) {
                throw new ValidatorException(i18n.getText("invalidPackageName", locale));
            }
            if (BDUtil.leZero(myrAmount)) {
                throw new ValidatorException("invalidPackageName", locale);
            }
            RazerTopup razerTopup = new RazerTopup();
            razerTopup.setOwnerId(memberId);
            razerTopup.setCurrency("MYR");
            razerTopup.setStatus(Global.Status.PENDING);
            razerTopup.setWalletType(Global.WalletType.WALLET_80);
            razerTopup.setAmount(myrAmount);
            razerTopup.setAmountTo(whaleCoinAmount_topUp);
            razerTopup.setOrderId(generateRandomOrderId());
            String verifyCode = razerPayClient.generateSignature(razerTopup.getOrderId(),
                    razerTopup.getAmount());
            razerTopup.setVcode(verifyCode);
            doSaveRazerPay(razerTopup);

            return razerTopup;
        }
    }

    private String generateRandomOrderId() {
        UniqueNumberGenerator generator = null;
        try {
            generator = new UniqueNumberGenerator(8, true, false, UniqueNumberGenerator.IS_NUMERIC);

            String orderId = null;
            do {
                orderId = "RP" + generator.getNewPin();
            } while (findRazerTopup(orderId) != null);

            return orderId;
        } catch (Exception ex) {
            throw new SystemErrorException(ex);
        } finally {
            if (generator != null)
                generator = null;
        }
    }


    @Override
    public Map<Double, Double> goldCoin2Myr() {
        Map<Double, Double> goldCoinMap = new HashMap<>();
        goldCoinMap.put(568D, 19.90D);
        goldCoinMap.put(854D, 29.90D);
        goldCoinMap.put(1140D, 39.90D);
        goldCoinMap.put(1711D, 59.90D);
        goldCoinMap.put(2568D, 89.90D);
        goldCoinMap.put(5997D, 209.90D);
        goldCoinMap.put(11711D, 409.90D);
        goldCoinMap.put(31426D, 1099.90D);
        goldCoinMap.put(45711D, 1599.90D);
        goldCoinMap.put(59997D, 2099.90D);
        goldCoinMap.put(88568D, 3099.90D);
        goldCoinMap.put(142854D, 4999.90D);
//                goldCoin2Myr.put(171426D, 5999.9D);
        return goldCoinMap;
    }

    @Override
    public Map<Double, Double> goldCoin2MyrPromo() {
        Map<Double, Double> goldCoinMap = new HashMap<>();
        goldCoinMap.put(568D, 28D);
        goldCoinMap.put(854D, 43D);
        goldCoinMap.put(1140D, 57D);
        goldCoinMap.put(1711D, 86D);
        goldCoinMap.put(2568D, 128D);
        goldCoinMap.put(5997D, 360D);
        goldCoinMap.put(11711D, 703D);
        goldCoinMap.put(31426D, 2514D);
        goldCoinMap.put(45711D, 3657D);
        goldCoinMap.put(59997D, 5999D);
        goldCoinMap.put(88568D, 8856D);
        goldCoinMap.put(142854D, 14285D);
//                goldCoin2Myr.put(171426D, 5999.9D);
        return goldCoinMap;
    }

    @Override
    public Map<Double, Double> goldCoin2MyrPromoPercent() {
        Map<Double, Double> goldCoinMap = new HashMap<>();
        goldCoinMap.put(568D, 5D);
        goldCoinMap.put(854D, 5D);
        goldCoinMap.put(1140D, 5D);
        goldCoinMap.put(1711D, 5D);
        goldCoinMap.put(2568D, 5D);
        goldCoinMap.put(5997D, 6D);
        goldCoinMap.put(11711D, 6D);
        goldCoinMap.put(31426D, 8D);
        goldCoinMap.put(45711D, 8D);
        goldCoinMap.put(59997D, 10D);
        goldCoinMap.put(88568D, 10D);
        goldCoinMap.put(142854D, 10D);
//                goldCoin2Myr.put(171426D, 5999.9D);
        return goldCoinMap;
    }

    @Override
    public Map<Double, Double> goldCoin2Omc() {
        Map<Double, Double> goldCoin2Omc = new HashMap<>();
        //map.put(Whale Gold Coin, USD)
        goldCoin2Omc.put(117D, 1D);
        goldCoin2Omc.put(585D, 5D);
        goldCoin2Omc.put(1170D, 10D);
        goldCoin2Omc.put(2925D, 25D);
        goldCoin2Omc.put(5850D, 50D);
        goldCoin2Omc.put(11700D, 100D);
        goldCoin2Omc.put(29250D, 250D);
        goldCoin2Omc.put(58500D, 500D);
        goldCoin2Omc.put(117000D, 1000D);
        goldCoin2Omc.put(175500D, 1500D);
        return goldCoin2Omc;
    }

    @Override
    public Map<Double, Double> goldCoin2OmcPromo() {
        Map<Double, Double> goldCoin2Omc = new HashMap<>();
        //map.put(Whale Gold Coin, USD)
        goldCoin2Omc.put(117D, 6D);
        goldCoin2Omc.put(585D, 29D);
        goldCoin2Omc.put(1170D, 59D);
        goldCoin2Omc.put(2925D, 146D);
        goldCoin2Omc.put(5850D, 293D);
        goldCoin2Omc.put(11700D, 585D);
        goldCoin2Omc.put(29250D, 1463D);
        goldCoin2Omc.put(58500D, 3510D);
        goldCoin2Omc.put(117000D, 7020D);
        goldCoin2Omc.put(175500D, 10530D);
        return goldCoin2Omc;
    }


    @Override
    public Map<Double, Double> goldCoin2OmcPromoPercent() {
        Map<Double, Double> goldCoin2Omc = new HashMap<>();
        //map.put(Whale Gold Coin, USD)
        goldCoin2Omc.put(117D, 5D);
        goldCoin2Omc.put(585D, 5D);
        goldCoin2Omc.put(1170D, 5D);
        goldCoin2Omc.put(2925D, 5D);
        goldCoin2Omc.put(5850D, 5D);
        goldCoin2Omc.put(11700D, 5D);
        goldCoin2Omc.put(29250D, 5D);
        goldCoin2Omc.put(58500D, 6D);
        goldCoin2Omc.put(117000D, 6D);
        goldCoin2Omc.put(175500D, 6D);
        return goldCoin2Omc;
    }

}
