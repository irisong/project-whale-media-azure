package com.compalsolutions.compal.paymentGateway.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.paymentGateway.vo.RazerTopup;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(RazerTopupDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RazerTopupDaoImpl extends Jpa2Dao<RazerTopup, String> implements RazerTopupDao {
    public RazerTopupDaoImpl() {
        super(new RazerTopup(false));
    }

    @Override
    public RazerTopup findRazerTopup(String orderId) {
        RazerTopup example = new RazerTopup(false);
        example.setOrderId(orderId);
        return findUnique(example);
    }
}
