package com.compalsolutions.compal.paymentGateway;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.blockchain.ObjectMapperUtil;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.paymentGateway.service.PaymentGatewayService;
import com.compalsolutions.compal.paymentGateway.vo.PaymentGatewayLog;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTopupPackage;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class AndroidPaymentClient {
    public static final Log log = LogFactory.getLog(AndroidPaymentClient.class);

    private static Map<String, String> cacheToken = null;//设置静态变量，用于判断access_token是否过期

    public void androidInAppPurchase(Locale locale, String memberId, String packageName, String productId,
                                     String purchaseToken, Integer quantity) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        PaymentGatewayService paymentGatewayService = Application.lookupBean(PaymentGatewayService.BEAN_NAME, PaymentGatewayService.class);
        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMemberId", locale));
        }

        if (StringUtils.isBlank(packageName)) {
            throw new ValidatorException("invalidPackageName", locale);
        }

        if (StringUtils.isBlank(productId)) {
            throw new ValidatorException("invalidPackageId", locale);
        }

        if (StringUtils.isBlank(purchaseToken)) {
            throw new ValidatorException("invalidPurchaseToken", locale);
        }



        if (cacheToken != null) {
            Long expires_in = Long.valueOf(cacheToken.get("expires_in")); // 有效时长
            Long create_time = Long.valueOf(cacheToken.get("create_time")); // access_token的创建时间
            Long now_time = (new Date().getTime()) / 1000;
            if (now_time > (create_time + expires_in - 300)) { // 提前五分钟重新获取access_token
                cacheToken = getAccessToken();
            }
        } else {
            cacheToken = getAccessToken();
        }

        String access_token = cacheToken.get("access_token");

        Member member = memberService.getMember(memberId);

        if(member == null){
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        log.debug("-----Android In App Purchase Process --------------");
        log.debug("Package Name: " + packageName);
        log.debug("Product Id: " + productId);
        log.debug("Purchase Token: " + purchaseToken);
        log.debug("MemberCode: " + member.getMemberCode());

        WalletTopupPackage walletTopupPackage = walletService.findOneWalletTopupPackageByProdutId(productId);

        if(walletTopupPackage == null){
            throw new ValidatorException(i18n.getText("invalidTopUpPackage", locale));
        }

        PaymentGatewayLog plog = new PaymentGatewayLog();
        plog.setMemberId(member.getMemberId());
        plog.setAction(Global.WalletTrxType.LIVE_STREAM_TOPUP);
        plog.setPlatform(PaymentGatewayLog.ANDROID_IN_APP_PURCHASE);

        try {
            /**这是写这篇博客时间时的最新API，v3版本。
             * https://www.googleapis.com/androidpublisher/v3/applications/{packageName}
             * /purchases/products/{productId}/tokens/{purchaseToken}?access_token={access_token}
             *
             * GET https://www.googleapis.com/androidpublisher/v3/applications/packageName/purchases/products/productId/tokens/token?access_token={access_token}
             */

            String url = "https://www.googleapis.com/androidpublisher/v3/applications";
            StringBuffer getURL = new StringBuffer();
            getURL.append(url);
            getURL.append("/" + packageName);
            getURL.append("/purchases/products");
            getURL.append("/" + productId);
            getURL.append("/tokens/" + purchaseToken);
            getURL.append("?access_token=" + access_token);
            URL urlObtainOrder = new URL(getURL.toString());
            HttpURLConnection connectionObtainOrder = (HttpURLConnection) urlObtainOrder.openConnection();
            connectionObtainOrder.setRequestMethod("GET");
            connectionObtainOrder.setDoOutput(true);

            plog.setSendParam(getURL.toString());

            // 如果认证成功
            if (connectionObtainOrder.getResponseCode() == HttpURLConnection.HTTP_OK) {

                log.debug("Status OK");

                StringBuilder sb = new StringBuilder("");
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        connectionObtainOrder.getInputStream(), "utf-8"));
                String strLine = "";
                while ((strLine = reader.readLine()) != null) {
                    sb.append(strLine);
                }

                // 把上面取回來的資料，放進JSONObject中，以方便我們直接存取到想要的參數
                JSONObject jo = new JSONObject(sb.toString());

                log.debug("Google Play Response:" + jo);

                Integer status = jo.getInt("purchaseState");
                if (status == 0) { //验证成功

                    String orderId = jo.getString("orderId");
                    BigDecimal whaleCoinTopUpAmount = new BigDecimal(walletTopupPackage.getPackageAmount());

                    //after 1st of April 2021, they decide to deduct 20% from whale coin
                    BigDecimal finalCoinTopUpAmount = whaleCoinTopUpAmount;
                    if (walletTopupPackage.getAdjustAmount() != null && walletTopupPackage.getAdjustAmount() > 0) {
                        finalCoinTopUpAmount = new BigDecimal(String.valueOf(walletTopupPackage.getAdjustAmount())).setScale(0, RoundingMode.HALF_UP);
                    }
                    if(quantity > 0){
                        whaleCoinTopUpAmount = whaleCoinTopUpAmount.multiply(new BigDecimal(quantity)).setScale(0, RoundingMode.HALF_UP);
                        finalCoinTopUpAmount = finalCoinTopUpAmount.multiply(new BigDecimal(quantity)).setScale(0, RoundingMode.HALF_UP);

                    }


                    WalletTrx walletTrx_db = walletService.findWalletTrxByWalletRefId(orderId);

                    if (walletTrx_db == null) {
                        WalletTrx walletTrx = new WalletTrx(true);
                        walletTrx.setInAmt(finalCoinTopUpAmount);
                        walletTrx.setOutAmt(BigDecimal.ZERO);
                        walletTrx.setOwnerId(memberId);
                        walletTrx.setOwnerType(Global.UserType.MEMBER);
                        walletTrx.setTrxDatetime(new Date());
                        walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_TOPUP);
                        walletTrx.setWalletRefId(orderId);
                        walletTrx.setWalletRefno("");
                        walletTrx.setWalletType(Global.WalletType.WALLET_80);
                        walletTrx.setRemark("Google Pay");

                        // using systemLocale
                        if (walletTopupPackage.getAdjustAmount() != null && walletTopupPackage.getAdjustAmount() > 0) {
                            walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXViaXWithCharges", whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(), walletTrx.getWalletRefId(),
                                    "20"));
                            walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXViaXWithCharges", Global.LOCALE_CN, whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(),
                                    walletTrx.getWalletRefId(), "20"));
                        } else {
                            walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXViaX", whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(), walletTrx.getWalletRefId()));
                            walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXViaX",  Global.LOCALE_CN, whaleCoinTopUpAmount,  walletTopupPackage.getFiatCurrencyAmount(),
                                    walletTrx.getWalletRefId()));
                        }

                        walletService.saveWalletTrx(walletTrx);

                        walletService.doCreditAvailableWalletBalance(memberId, walletTrx.getOwnerType(), Global.WalletType.WALLET_80,
                                walletTrx.getInAmt());

                        notificationConfProvider.addPushNotificationMessage(Global.AccessModule.WHALE_MEDIA,
                                member.getMemberId(), walletTrx.getWalletRefno(), Payload.WALLET_DEPOSIT,
                                WebUtil.getWalletName(locale, Global.WalletType.WALLET_80), null,
                                i18n.getText("topupWhaleLiveCoinAmtXViaX",  Global.LOCALE_CN, walletTrx.getInAmt(),
                                        walletTopupPackage.getFiatCurrencyAmount(),
                                        walletTrx.getWalletRefId()));

                        plog.setStatus(Global.Status.COMPLETED);
                        plog.setReturnMessage(sb.toString());
                        plog.setResult("Andorid In App Purchase Verification Success");
                        paymentGatewayService.doSavePaymentGatewayLog(plog);
                    }else{
                        plog.setStatus(Global.Status.FAILED);
                        plog.setReturnMessage(sb.toString());
                        plog.setResult("Andorid In App Purchase Already Verified");
                        paymentGatewayService.doSavePaymentGatewayLog(plog);

                        throw new ValidatorException(i18n.getText("androidPurchaseHasBeenVerified",locale, purchaseToken));
                    }

                } else {
                    plog.setStatus(Global.Status.FAILED);
                    plog.setReturnMessage(sb.toString());
                    plog.setResult("Andorid In App Purchase Verification Failed");
                    paymentGatewayService.doSavePaymentGatewayLog(plog);
                    throw new ValidatorException(i18n.getText("androidPurchaseHasFailed",locale, purchaseToken));
                }
            }else{
                StringBuilder sbLines = new StringBuilder("");
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        connectionObtainOrder.getErrorStream(), "utf-8"));
                String strLine = "";
                while ((strLine = reader.readLine()) != null) {
                    sbLines.append(strLine);
                }

                ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();
                JsonNode error = mapper.readTree(sbLines.toString());
                log.debug("Android In_App Purhase Error : " + sbLines.toString());

                plog.setStatus(Global.Status.FAILED);
                plog.setReturnMessage(sbLines.toString());
                plog.setResult("Andorid In App Purchase Verification Failed");
                paymentGatewayService.doSavePaymentGatewayLog(plog);

                if (!error.get("error").isMissingNode()) {
                    String code = error.get("code").toString();
                    String message = error.get("message").toString();

                    throw new ValidatorException(i18n.getText("androidInAppVerificationFail", code, message));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            plog.setStatus(Global.Status.FAILED);
            plog.setReturnMessage(e.getMessage());
            plog.setResult("Andorid In App Purchase Verification Failed");
            paymentGatewayService.doSavePaymentGatewayLog(plog);

            throw new SystemErrorException(e.getMessage(), e);
        }
    }

    private static Map<String, String> getAccessToken() {
        final String CLIENT_ID = "36804928924-0h2enmdou7u45n3oc8aim4asl2r978lo.apps.googleusercontent.com";
        final String CLIENT_SECRET = "1ldORNz_KX7d7WViMMAU3Y56";
        final String REFRESH_TOKEN = "1//0gVqO2DrkiCcCCgYIARAAGBASNwF-L9IrUYvabtzI6fngsfCLxJ8REj5SrMuhv-7-JT9Z6rDK8m5owgzwuRxFahUSAW_yBkfREeM";
        Map<String, String> map = null;

        try {
            /**
             * https://accounts.google.com/o/oauth2/token?refresh_token={REFRESH_TOKEN}
             * &client_id={CLIENT_ID}&client_secret={CLIENT_SECRET}&grant_type=refresh_token
             */
            log.debug("------Android Access Token------");

            URL urlGetToken = new URL("https://accounts.google.com/o/oauth2/token");
            HttpURLConnection connectionGetToken = (HttpURLConnection) urlGetToken.openConnection();
            connectionGetToken.setRequestMethod("POST");
            connectionGetToken.setDoOutput(true);
            // 开始传送参数
            OutputStreamWriter writer = new OutputStreamWriter(connectionGetToken.getOutputStream());
            writer.write("refresh_token=" + REFRESH_TOKEN + "&");
            writer.write("client_id=" + CLIENT_ID + "&");
            writer.write("client_secret=" + CLIENT_SECRET + "&");
            writer.write("grant_type=refresh_token");
            writer.close();

            //若响应码为200则表示请求成功
            if (connectionGetToken.getResponseCode() == HttpURLConnection.HTTP_OK) {
                StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(
                        new InputStreamReader(connectionGetToken.getInputStream(), "utf-8"));
                String strLine = "";
                while ((strLine = reader.readLine()) != null) {
                    sb.append(strLine);
                }

                // 取得谷歌回传的信息(JSON格式)
                JSONObject jo = new JSONObject(sb.toString());
                String ACCESS_TOKEN = jo.getString("access_token");
                Integer EXPIRES_IN = jo.getInt("expires_in");
                map = new HashMap<>();
                map.put("access_token", ACCESS_TOKEN);
                map.put("expires_in", String.valueOf(EXPIRES_IN));
                // 带入access_token的创建时间，用于之后判断是否失效
                map.put("create_time", String.valueOf((new Date().getTime()) / 1000));
                log.debug("JSON Message: " + jo);
            }

            log.debug("------Android Access Token Success ------");
        } catch (MalformedURLException e) {
            log.debug("Fail to get accessToken, Error:" + e);
            e.printStackTrace();
        } catch (IOException e) {
            log.debug("Fail to get accessToken, Error:" + e);
            e.printStackTrace();
        }
        return map;
    }
}