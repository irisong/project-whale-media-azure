package com.compalsolutions.compal.paymentGateway;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.crypto.blockchain.ObjectMapperUtil;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.paymentGateway.service.PaymentGatewayService;
import com.compalsolutions.compal.paymentGateway.vo.PaymentGatewayLog;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTopupPackage;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.net.ssl.*;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Locale;

public class IosPaymentClient {
    public static final Log log = LogFactory.getLog(IosPaymentClient.class);

    public void iosInAppPurchase(Locale locale, String memberId, String receipt) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        PaymentGatewayService paymentGatewayService = Application.lookupBean(PaymentGatewayService.BEAN_NAME, PaymentGatewayService.class);
        WalletService walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        if (StringUtils.isBlank(memberId)) {
            throw new ValidatorException(i18n.getText("invalidMember", locale));
        }

        if (StringUtils.isBlank(receipt)) {
            throw new ValidatorException(i18n.getText("invalidReceiptData", locale));
        }

        Member member = memberService.getMember(memberId);

        PaymentGatewayLog plog = new PaymentGatewayLog();
        plog.setMemberId(member.getMemberId());
        plog.setAction(Global.WalletTrxType.LIVE_STREAM_TOPUP);
        plog.setPlatform(PaymentGatewayLog.IOS_IN_APP_PURCHASE);

        // 0沙盒 非0即线上
        String verifyResult = inAppPurchaseVerify(receipt, plog);

        if (StringUtils.isBlank(verifyResult)) {// 苹果服务器没有返回验证结果
            plog.setResult("Apple Server Did Not Return Any IAP Verification Result");
            plog.setStatus(Global.Status.FAILED);
            paymentGatewayService.doSavePaymentGatewayLog(plog);

            throw new ValidatorException("Apple Server Did Not Return Any IAP Verification Result");
        } else {// 苹果验证有返回结果
            ObjectMapper mapper = ObjectMapperUtil.getObjectMapper();
            try {
                JsonNode tree = mapper.readTree(verifyResult);
                log.debug("IOS内购(充值)=>苹果服务器返回验证结果:" + verifyResult);

                if (!tree.get("status").isMissingNode()) {
                    String states = tree.get("status").toString();

                    if (states.equals("0")) { // 前端所提供的收据是有效的验证成功
                        if (!tree.get("receipt").isMissingNode()) {
                            String r_receipt = tree.get("receipt").toString();

                            JSONObject jsonObject = new JSONObject(r_receipt);
                            JSONArray jsonArray = jsonObject.getJSONArray("in_app");
                            log.debug("IOS内购(充值)=>苹果服务器返回验证结果订单数量:" + jsonArray.length());

                            for (int i = jsonArray.length(); i > 0; i--) {

                                if (jsonArray.getJSONObject(i - 1).get("product_id") != null) {
                                    String productId = jsonArray.getJSONObject(i - 1).get("product_id").toString();
                                    WalletTopupPackage walletTopupPackage = walletService.findOneWalletTopupPackageByProdutId(productId);

                                    if (walletTopupPackage != null) {
                                        if (jsonArray.getJSONObject(i - 1).get("original_transaction_id") == null) {
                                            continue;
                                        } else {
                                            String original_transaction_id = jsonArray.getJSONObject(i - 1).get("original_transaction_id").toString();
                                            Double quantity = Double.valueOf(jsonArray.getJSONObject(i - 1).get("quantity").toString());

                                            BigDecimal whaleCoinTopUpAmount = new BigDecimal(String.valueOf(walletTopupPackage.getPackageAmount())).setScale(0, RoundingMode.HALF_UP);
                                            BigDecimal finalCoinTopUpAmount = whaleCoinTopUpAmount;
                                            //after 1st of April 2021, they decide to deduct 20% from whale coin
                                            if (walletTopupPackage.getAdjustAmount() != null && walletTopupPackage.getAdjustAmount() > 0) {
                                                finalCoinTopUpAmount = new BigDecimal(String.valueOf(walletTopupPackage.getAdjustAmount())).setScale(0, RoundingMode.HALF_UP);
                                            }

                                            if (quantity > 0) {
                                                finalCoinTopUpAmount = finalCoinTopUpAmount.multiply(new BigDecimal(quantity)).setScale(0, RoundingMode.HALF_UP);
                                                whaleCoinTopUpAmount = whaleCoinTopUpAmount.multiply(new BigDecimal(quantity)).setScale(0, RoundingMode.HALF_UP);

                                            }

                                            WalletTrx walletTrx_db = walletService.findWalletTrxByWalletRefId(original_transaction_id);

                                            if (walletTrx_db == null) {
                                                WalletTrx walletTrx = new WalletTrx(true);
                                                walletTrx.setInAmt(finalCoinTopUpAmount);
                                                walletTrx.setOutAmt(BigDecimal.ZERO);
                                                walletTrx.setOwnerId(memberId);
                                                walletTrx.setOwnerType(Global.UserType.MEMBER);
                                                walletTrx.setTrxDatetime(new Date());
                                                walletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_TOPUP);
                                                walletTrx.setWalletRefId(original_transaction_id);
                                                walletTrx.setWalletRefno("");
                                                walletTrx.setWalletType(Global.WalletType.WALLET_80);
                                                walletTrx.setRemark("Apple Pay");

                                                // using systemLocale
                                                if (walletTopupPackage.getAdjustAmount() != null && walletTopupPackage.getAdjustAmount() > 0) {
                                                    walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXViaXWithCharges", whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(), walletTrx.getWalletRefId(),
                                                            "20"));
                                                    walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXViaXWithCharges", Global.LOCALE_CN, whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(),
                                                            walletTrx.getWalletRefId(), "20"));
                                                } else {
                                                    walletTrx.setTrxDesc(i18n.getText("topupWhaleLiveCoinAmtXViaX", whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(), walletTrx.getWalletRefId()));
                                                    walletTrx.setTrxDescCn(i18n.getText("topupWhaleLiveCoinAmtXViaX", Global.LOCALE_CN, whaleCoinTopUpAmount, walletTopupPackage.getFiatCurrencyAmount(),
                                                            walletTrx.getWalletRefId()));
                                                }

                                                walletService.saveWalletTrx(walletTrx);

                                                walletService.doCreditAvailableWalletBalance(memberId, walletTrx.getOwnerType(), Global.WalletType.WALLET_80,
                                                        walletTrx.getInAmt());

                                                notificationConfProvider.addPushNotificationMessage(Global.AccessModule.WHALE_MEDIA,
                                                        member.getMemberId(), walletTrx.getWalletRefno(), Payload.WALLET_DEPOSIT,
                                                        WebUtil.getWalletName(locale, Global.WalletType.WALLET_80), null,
                                                        i18n.getText("topupWhaleLiveCoinAmtXViaX", Global.LOCALE_CN, walletTrx.getInAmt(),
                                                                walletTopupPackage.getFiatCurrencyAmount(),
                                                                walletTrx.getWalletRefId()));
                                            }
                                        }
                                    } else {
                                        continue;
                                    }
                                }
                            }

                            plog.setStatus(Global.Status.COMPLETED);
                            plog.setResult(Global.Status.COMPLETED);
                            paymentGatewayService.doSavePaymentGatewayLog(plog);
                        }
                    }
                }

            } catch (IOException e) {
                log.debug("--------------------Error Detail -----------------------");
                log.debug("Error: " + e.getMessage());
                throw new SystemErrorException(e.getMessage(), e);
            }
        }
    }

    private static final String url_sandbox = "https://sandbox.itunes.apple.com/verifyReceipt";
    private static final String url_verify = "https://buy.itunes.apple.com/verifyReceipt";

    /**
     * 苹果服务器验证
     *
     * @param receipt 账单
     * @return null 或返回结果 沙盒 https://sandbox.itunes.apple.com/verifyReceipt
     * @url 要验证的地址
     */
    public static String inAppPurchaseVerify(String receipt, PaymentGatewayLog plog) {
        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);
        PaymentGatewayService paymentGatewayService = Application.lookupBean(PaymentGatewayService.BEAN_NAME, PaymentGatewayService.class);

        //环境判断 线上/开发环境用不同的请求链接
        String url = "";
        String password = "";
        if (!cryptoProvider.isProductionMode()) {
            url = url_sandbox; //沙盒测试
            password = "adf45aa0ae4341b4b71de916e7840625";
        } else {
            url = url_verify; //线上测试
            password = "0259440aa17647aabdb4dd77e9feec6f";
        }
        //String url = EnvUtils.isOnline() ?url_verify : url_sandbox;

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, new TrustManager[]{new TrustAnyTrustManager()}, new java.security.SecureRandom());
            URL console = new URL(url);
            HttpsURLConnection conn = (HttpsURLConnection) console.openConnection();
            conn.setSSLSocketFactory(sc.getSocketFactory());
            conn.setHostnameVerifier(new TrustAnyHostnameVerifier());
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Proxy-Connection", "Keep-Alive");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            BufferedOutputStream hurlBufOus = new BufferedOutputStream(conn.getOutputStream());

            String str = String.format("{\"receipt-data\":\"" + receipt + "\",\"password\":\"" + password + "\"}");
            log.debug("Verification Data:" + str);
            plog.setSendParam(receipt);

            hurlBufOus.write(str.getBytes());
            hurlBufOus.flush();

            InputStream is = conn.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line = null;
            StringBuffer sb = new StringBuffer();
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
            plog.setReturnMessage(sb.toString());
            log.debug("Verification Result:" + sb.toString());

            return sb.toString();
        } catch (Exception ex) {
            System.out.println("Apple Server Verification Error");

            plog.setResult("Apple Server Verification Error");
            plog.setStatus(Global.Status.FAILED);
            paymentGatewayService.doSavePaymentGatewayLog(plog);

            ex.printStackTrace();
        }
        return null;
    }

    /**
     * 用BASE64加密
     *
     * @param str
     * @return
     */
    public static String getBASE64(String str) {
        byte[] b = str.getBytes();
        String s = null;
        if (b != null) {
            s = new sun.misc.BASE64Encoder().encode(b);
        }
        return s;

    }

    private static class TrustAnyTrustManager implements X509TrustManager {

        public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
        }

        public X509Certificate[] getAcceptedIssuers() {
            return new X509Certificate[]{};
        }
    }

    private static class TrustAnyHostnameVerifier implements HostnameVerifier {
        public boolean verify(String hostname, SSLSession session) {
            return true;
        }

    }
}