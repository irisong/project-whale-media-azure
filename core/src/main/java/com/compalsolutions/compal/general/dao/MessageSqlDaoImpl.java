package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Message;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component(MessageSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MessageSqlDaoImpl extends AbstractJdbcDao implements MessageSqlDao {

    @Override
    public void findAllMessageByReceiverIdByPaging(DatagridModel<Message> datagridModel, String memberId) {
        Validate.notBlank(memberId);

        String sql  = " SELECT * from app_message m "
                + " WHERE (m.member_id = ? OR m.member_id IS NULL) "
                + " AND coalesce(m.reference,'') NOT IN (SELECT announcement_id FROM app_message_announce_removed WHERE member_id = ? AND status = ?) "
                + " ORDER BY m.sent_date DESC ";

        List<Object> params = new ArrayList<>();
        params.add(memberId);
        params.add(memberId);
        params.add(Global.Status.DELETED);

        queryForDatagrid(datagridModel, sql, new RowMapper<Message>() {
            public Message mapRow(ResultSet rs, int rowNum) throws SQLException {
                Message message = new Message();
                message.setMessageId(rs.getString("message_id"));
                message.setMemberId(rs.getString("member_id"));
                message.setSentDate(rs.getDate("sent_date"));
                message.setMessageType(rs.getString("message_type"));
                message.setDirectToMemberId(rs.getString("direct_to_member_id"));
//                message.setImageUrl(rs.getString("image_url"));
                message.setTitle(rs.getString("title"));
                message.setTitleCn(rs.getString("title_cn"));
                message.setContent(rs.getString("content"));
                message.setContentCn(rs.getString("content_cn"));
                return message;
            }
        }, params.toArray());
    }
}
