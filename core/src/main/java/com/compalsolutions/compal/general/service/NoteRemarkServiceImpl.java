package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.dao.NoteRemarkDao;
import com.compalsolutions.compal.general.vo.NoteRemark;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component(NoteRemarkService.BEAN_NAME)
public class NoteRemarkServiceImpl implements NoteRemarkService {

    @Autowired
    private NoteRemarkDao noteRemarkDao;

    public List<NoteRemark> findMessageByMemberIdAndMessageRefId(String memberId, String messageRefId) {
        return noteRemarkDao.findMessageByMemberIdAndMessageRefId(memberId, messageRefId);
    }

    public List<NoteRemark> findMessageByMessageRefId(String messageRefId) {
        return noteRemarkDao.findMessageByMessageRefId(messageRefId);
    }

    public void doCreateNodeRemark(String remark, String memberId, String messageRefId) {
        NoteRemark noteRemark = new NoteRemark();
        noteRemark.setMessage(remark);
        noteRemark.setMemberId(memberId);
        noteRemark.setMessageRef(messageRefId);
        noteRemark.setSender(memberId);
        noteRemark.setSentDate(new Date());
        noteRemarkDao.save(noteRemark);
    }

    public void doCreateNodeRemarkByAdmin(String remark, String memberId, String messageRefId) {
        NoteRemark noteRemark = new NoteRemark();
        noteRemark.setMessage(remark);
        noteRemark.setMemberId(memberId);
        noteRemark.setMessageRef(messageRefId);
        noteRemark.setSender("ADMIN");
        noteRemark.setSentDate(new Date());
        noteRemarkDao.save(noteRemark);
    }
}
