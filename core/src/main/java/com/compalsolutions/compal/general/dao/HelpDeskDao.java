package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.HelpDesk;

import java.util.Date;
import java.util.List;

public interface HelpDeskDao extends BasicDao<HelpDesk, String> {
    public static final String BEAN_NAME = "helpDeskDao";

    public List<HelpDesk> findHelpDeskByUserId(String userId, String ticketTypeId);

    public void findHelpDeskListForDatagrid(DatagridModel<HelpDesk> datagridModel, String memberCode, String status, Date dateFrom,
                                            Date dateTo, String type, Boolean repliedByAdmin);

    public List<HelpDesk> findRepliedNonActiveTicketAfterTimePeriod(Date period);












//    public List<HelpDesk> findEnquiriesByUserId(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo, int pageNo,
//                                                int pageSize);
//
//    public int findEnquiryTotalRecord(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo);
//

//
//
//    public List<HelpDesk> findRepliedNonActiveTicketAfter72H(Date date72Hours);
//
}
