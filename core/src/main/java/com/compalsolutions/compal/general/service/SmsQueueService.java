package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.SmsQueue;

import java.util.Locale;

public interface SmsQueueService {
    public static final String BEAN_NAME = "smsQueueService";

    public void findSmsqForListing(DatagridModel<SmsQueue> datagridModel, String smsTo, String status);

    public void doSentSms();

    public void saveSmsQueue(Locale locale, SmsQueue smsQueue);
}