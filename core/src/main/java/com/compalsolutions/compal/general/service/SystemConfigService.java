package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.general.vo.SystemConfigImage;

import java.util.List;

public interface SystemConfigService {
    public static final String BEAN_NAME = "systemConfigService";

    public SystemConfig getDefaultSystemConfig();

    public void doToogleMemberRegisterForSystemConfig();

    public void doToogleMemberLoginForSystemConfig();

    public void updateApplicationName(String applicationName);

    public void updateCompanyName(String companyName);

    public void updateSupportEmail(String supportEmail);

    public void updateDefaultCountryCode(String countryCode);

    public void updateQRCodeExpiry(Integer minutes);

    public void saveOrUpdateSystemConfigImage(SystemConfigImage systemConfigImage);

    public void deleteSystemConfigImageById(String imageId);

    public SystemConfigImage findSystemConfigImageById(String imageId);

    public SystemConfigImage findSystemConfigImageByType(String imageType);

    public List<SystemConfigImage> findSystemConfigImages();
}