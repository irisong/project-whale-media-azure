package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.Province;

import java.util.List;

public interface ProvinceDao extends BasicDao<Province, String> {
    public static final String BEAN_NAME = "provinceDao";

    public List<Province> findAllActiveProvince();

    public List<Province> findProvinceByCountryCode(String countryCode);

    public Province findProvinceByProvinceCode(String provinceCode);

    public Province findProvinceByProvinceName(String provinceName);
}
