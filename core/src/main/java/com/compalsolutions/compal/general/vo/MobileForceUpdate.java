package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "app_mobile_force_update")
@Access(AccessType.FIELD)
public class MobileForceUpdate extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "force_update_id", unique = true, nullable = false, length = 32)
    private String forceUpdateId;

    @ToTrim
    @Column(name = "platform", length = 50, nullable = false)
    private String platform;

    @ToTrim
    @Column(name = "favor", length = 50, nullable = false)
    private String favor;

    @ToTrim
    @Column(name = "version_name", length = 50, nullable = false)
    private String versionName;

    @ToTrim
    @Column(name = "version_code", length = 50, nullable = false)
    private String versionCode;

    @ToTrim
    @Column(name = "force_update", nullable = false)
    private Boolean forceUpdate;

    @ToTrim
    @Column(name = "update_url", length = 200, nullable = false)
    private String updateUrl;

    public MobileForceUpdate() {
    }

    public MobileForceUpdate(boolean defaultValue) {
        if (defaultValue) {
            forceUpdate = false;
            updateUrl = "";
        }
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getFavor() {
        return favor;
    }

    public void setFavor(String favor) {
        this.favor = favor;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;
        MobileForceUpdate that = (MobileForceUpdate) o;
        return Objects.equals(forceUpdateId, that.forceUpdateId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(forceUpdateId);
    }

    public String getForceUpdateId() {
        return forceUpdateId;
    }

    public void setForceUpdateId(String forceUpdateId) {
        this.forceUpdateId = forceUpdateId;
    }
}
