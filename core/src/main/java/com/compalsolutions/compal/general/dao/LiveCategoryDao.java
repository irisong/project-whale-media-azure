package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.point.vo.Symbolic;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface LiveCategoryDao extends BasicDao<LiveCategory, String> {
    public static final String BEAN_NAME = "categoryDao";

    public List<LiveCategory> findAllActiveCategory(boolean includeVirtualCategory);

    public LiveCategory findOneCategoryByName(String category, String categoryCn, String categoryMs);

    public LiveCategory findCategoryByTypeByStatus(String type, String status);

    public void findCategoryForDatagrid(DatagridModel<LiveCategory> datagridModel, String category, String categoryCn, String categoryMs, String status);

    public String getLocalizeCategoryNameByType(Locale locale, String type);

    public void updateBannerCategorySeq(String type, int seq);
}
