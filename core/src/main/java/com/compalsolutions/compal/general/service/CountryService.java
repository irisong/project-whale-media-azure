package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.*;

import java.util.List;
import java.util.Locale;

public interface CountryService {
    public static final String BEAN_NAME = "countryService";

    public Country getCountry(String countryCode);

    public Province getProvince(String provinceCode);

    public Area getArea(String areaCode);

    public City getCity(String cityCode);

    public List<CountryDesc> findCountryDescsByLocale(Locale locale);

    public CountryDesc findCountryDescByLocaleAndCountryCode(Locale locale, String countryCode);

    public List<Province> getProvinceListByCountryCode(String countryCode);

    public List<City> getCityListByProvinceCode(String provinceCode);

    public List<Area> getAreaListByCityCode(String cityCode);

    public Province findProvinceByProvinceName(String provinceName);

    public City findCityByCityName(String cityName);

    public Area findAreaByAreaName(String areaName);

    public Province findProvinceByStateCode(String stateCode);
}
