package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.general.vo.HelpDeskType;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface HelpDeskService {
    public static final String BEAN_NAME = "helpDeskService";

    public List<HelpDeskType> findHelpDeskTypes(String status);

    public HelpDeskType getHelpDeskType(String ticketTypeId);

    public String getHelpDeskTypeNameByLanguage(String ticketTypeId, String language);

    public void findHelpDeskTypeForListing(DatagridModel<HelpDeskType> datagridModel, String status);

    public List<HelpDesk> findHelpDeskByUserId(String userId, String ticketTypeId);

    public HelpDesk getHelpDesk(String ticketId);

    public void doProcessHelpDeskTicket(Locale locale, User user, String ticketId, String subject, String ticketTypeId, String message,
                                        List<HelpDeskReplyFile> uploadImageList);

    public void doAddHelpDeskReply(String ticketId, String message, User user, List<HelpDeskReplyFile> uploadImageList);

    public void doProcessHelpDeskAdminReplyNotification(HelpDesk helpDesk);

    public int findTotalCountHelpDeskReplyByTicketId(String ticketId);

    public void saveHelpDeskType(Locale locale, HelpDeskType helpDeskType);

    public void updateHelpDeskType(Locale locale, HelpDeskType helpDeskType);

    public void findHelpDeskListForListing(DatagridModel<HelpDesk> datagridModel, String memberCode, String status, Date dateFrom,
                                           Date dateTo, String type, Boolean repliedByAdmin);

    public void updateHelpDeskStatus(String ticketId, String status, LoginInfo loginInfo);

    public HelpDeskReplyFile getHelpDeskReplyFile(String helpDeskReplyFileId);

    public void doUpdateRepliedHelpDeskToCloseAfter168H();
}
