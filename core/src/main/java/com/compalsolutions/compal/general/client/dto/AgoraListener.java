package com.compalsolutions.compal.general.client.dto;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Arrays;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class AgoraListener {
    public static final List<Integer> eventTypeList = Arrays.asList(101, 102, 103, 104, 105, 106, 107, 108, 111, 112);

    public static final int EVENT_TYPE_CREATE_CHANNEL = 101;
    public static final int EVENT_TYPE_DESTROY_CHANNEL = 102;
    public static final int EVENT_TYPE_BC_JOIN_CHANNEL = 103;
    public static final int EVENT_TYPE_BC_LEAVE_CHANNEL = 104;
    public static final int EVENT_TYPE_AUD_JOIN_CHANNEL = 105;
    public static final int EVENT_TYPE_AUD_LEAVE_CHANNEL = 106;
    public static final int EVENT_TYPE_AUD_TO_BC = 111;
    public static final int EVENT_TYPE_BC_TO_AUD = 112;

    @ToTrim
    private String sid;
    @ToTrim
    private String noticeId;
    private Integer productId;
    private Integer eventType;
    private Long notifyMs;
    private AgoraPayload payload;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public Long getNotifyMs() {
        return notifyMs;
    }

    public void setNotifyMs(Long notifyMs) {
        this.notifyMs = notifyMs;
    }

    public AgoraPayload getPayload() {
        return payload;
    }

    public void setPayload(AgoraPayload payload) {
        this.payload = payload;
    }
}
