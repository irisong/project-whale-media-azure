package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.struts.bean.OptionBean;

import java.util.List;
import java.util.Locale;

public interface LiveCategoryService {
    public static final String BEAN_NAME = "liveCategoryService";

    public void saveLiveCategory(String category , String categoryCn, String categoryMs,int seq);

    public LiveCategory getLiveCategory(String categoryId);

    public void findLiveCategoryForListing(DatagridModel<LiveCategory> datagridModel, String category, String categoryCn,String categoryMs, String status);

    public void doDeleteLiveCategory(String categoryId);

    public void doUpdateLiveCategoryStatus(String categoryId, String status);

    public LiveCategory findCategoryByTypeByStatus(String type, String status);

    public String getLocalizeCategoryNameByType(Locale locale, String type);

    public List<OptionBean> getLiveCategoryOptions(Locale locale, String memberLiveStreamType);

    public List<LiveCategory> doFindAllLiveCategory(boolean includeVirtualCategory);

    public LiveCategory doFindVirtualCategory();

    public List<LiveCategory> getLiveCategoryList(boolean isCn, boolean isMs) ;
}
