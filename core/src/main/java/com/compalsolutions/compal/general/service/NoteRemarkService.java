package com.compalsolutions.compal.general.service;
import com.compalsolutions.compal.general.vo.NoteRemark;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

public interface NoteRemarkService {
    public static final Log log = LogFactory.getLog(NoteRemarkService.class);

    public static final String BEAN_NAME = "noteRemarkService";

    public List<NoteRemark> findMessageByMemberIdAndMessageRefId(String memberId, String messageRefId);

    public List<NoteRemark> findMessageByMessageRefId(String messageRefId);

    public void doCreateNodeRemark(String remark, String memberId, String messageRefId);

    public void doCreateNodeRemarkByAdmin(String remark, String memberId, String messageRefId);

}
