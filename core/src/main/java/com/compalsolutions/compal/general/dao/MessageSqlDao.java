package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Message;

public interface MessageSqlDao {
    public static final String BEAN_NAME = "messageSqlDao";

    public void findAllMessageByReceiverIdByPaging(DatagridModel<Message> datagridModel, String memberId);

}
