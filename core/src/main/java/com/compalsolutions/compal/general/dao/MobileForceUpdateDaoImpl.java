package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.MobileForceUpdate;
import org.apache.commons.lang.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MobileForceUpdateDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MobileForceUpdateDaoImpl extends Jpa2Dao<MobileForceUpdate, String> implements MobileForceUpdateDao {
    public MobileForceUpdateDaoImpl() {
        super(new MobileForceUpdate(false));
    }

    @Override
    public MobileForceUpdate findByPlatformAndFavor(String platform, String favor) {
        Validate.notEmpty(platform);
        Validate.notEmpty(favor);

        MobileForceUpdate example = new MobileForceUpdate(false);
        example.setPlatform(platform);
        example.setFavor(favor);
        return findUnique(example);
    }
}
