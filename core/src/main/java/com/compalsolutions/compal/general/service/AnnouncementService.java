package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Announcement;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface AnnouncementService {
    public static final String BEAN_NAME = "announcementService";

    public void findAnnouncementsForListing(DatagridModel<Announcement> datagridModel, List<String> userGroups, String status, String announcementType,
                                            Date dateFrom, Date dateTo);

    public void saveAnnouncement(Locale locale, Announcement announcement);

    public Announcement getAnnouncement(String announceId);

    public void updateAnnouncement(Locale locale, Announcement announcement);

    public int getAnnouncementTotalRecordsFor(List<String> userGroups);

    public List<Announcement> findAnnouncementsForDashboard(String userGroup, int pageNo, int pageSize);

    public List<Announcement> findAnnouncementsForDashboard(String userGroup, String languageCode, String accessModule, int pageNo, int pageSize);

    public List<Announcement> findPendingAnnouncementToSend(Date untilDate);

    public void doProcessAnnouncementNotification(Announcement announcement);

    public Announcement doSaveAnnouncement(String title, String titleCn, String body, String bodyCn, String status, Date publishDate, String userGroup, String announcementType);
}
