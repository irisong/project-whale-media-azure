package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Message;

import java.util.List;

public interface MessageDao extends BasicDao<Message, String> {
    public static final String BEAN_NAME = "messageDao";

    public Message findMessageByReceiverIdAndMessageId(String receiverId, String messageId);

    public List<Message> findAllMessageByReceiverId(String receiverId);

    public Message findMessageByReceiverIdAndDirectToId(String receiverId, String directToMemberId);

    public Message findMessageByTypeAndReferenceAndMemberId(String type, String reference, String memberId);

    public List<Message> findAllMessageByType(String type);
}
