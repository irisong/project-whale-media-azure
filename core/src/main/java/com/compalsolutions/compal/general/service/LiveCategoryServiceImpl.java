package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.BannerFileUploadConfiguration;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.BannerDao;
import com.compalsolutions.compal.general.dao.BannerFileDao;
import com.compalsolutions.compal.general.dao.LiveCategoryDao;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.general.vo.BannerFile;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.struts.bean.OptionBean;
import org.apache.commons.lang3.StringUtils;
import org.redisson.client.RedisConnectionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Locale;

@Component(LiveCategoryService.BEAN_NAME)
public class LiveCategoryServiceImpl implements LiveCategoryService {
    @Autowired
    private LiveCategoryDao categoryDao;

    @Autowired
    private BannerDao bannerDao;

    @Autowired
    private BannerFileDao bannerFileDao;


    @Override
    public void saveLiveCategory(String category, String categoryCn, String categoryMs, int seq) {
        LiveCategory cat = categoryDao.findOneCategoryByName(category, categoryCn, categoryMs);
        if (cat != null) {
            throw new ValidatorException("Similar category name exists");
        } else {
            updateLiveCategorySeq("ADD", 0, seq);

            cat = new LiveCategory();
            cat.setType(category.toUpperCase());
            cat.setStatus(Global.Status.ACTIVE);
            cat.setSeq(seq);
            cat.setCategory(category);
            cat.setCategoryCn(categoryCn);
            cat.setCategoryMs(categoryMs);
            categoryDao.save(cat);
        }
    }

    private void updateLiveCategorySeq(String action, int oldSeq, int newSeq) {
        if (action.equalsIgnoreCase("UPDATE")) {
            if (oldSeq != newSeq) {
                categoryDao.updateBannerCategorySeq("OLD", oldSeq);
                categoryDao.updateBannerCategorySeq("NEW", newSeq);
            }
        } else { //ADD
            categoryDao.updateBannerCategorySeq("NEW", newSeq);
        }
    }

    @Override
    public LiveCategory getLiveCategory(String categoryId) {
        return categoryDao.get(categoryId);
    }

    @Override
    public void findLiveCategoryForListing(DatagridModel<LiveCategory> datagridModel, String category, String categoryCn, String categoryMs, String status) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        categoryDao.findCategoryForDatagrid(datagridModel, category, categoryCn, categoryMs, status);
        for (LiveCategory cat : datagridModel.getRecords()) {
            cat.setStatusDesc(i18n.getText(Global.Status.getI18nKey(cat.getStatus())));
        }
    }

    @Override
    public void doDeleteLiveCategory(String categoryId) {
        BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        LiveCategory liveCategory = categoryDao.get(categoryId);
        List<Banner> bannerList = bannerDao.findBannerByCategory(categoryId);

        if (bannerList.size() > 0) {
            for (Banner s : bannerList) {
                try {
                    String bannerId = s.getBannerId();
                    List<BannerFile> bannerFileList = bannerFileDao.findBannerFileListByBannerId(bannerId);
                    for (BannerFile bannerFile : bannerFileList) {
                        if (isProdServer) {
                            // if production, need to delete from azureblob storage
                            azureBlobStorageService.deleteFileByName(BannerFileUploadConfiguration.FOLDER_NAME,
                                    bannerFile.getRenamedFilename());
                        } else {
                            bannerFile.setFileUrlWithParentPath(config.getUploadPath());
                            Files.deleteIfExists(Paths.get(bannerFile.getFileUrl()));
                        }
                    }
                    bannerFileDao.deleteAll(bannerFileList);
                    bannerFileList.clear();
                    bannerFileList = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            bannerDao.deleteAll(bannerList);
            bannerList.clear();
            bannerList = null;
        }
        if (liveCategory != null) {
            categoryDao.delete(liveCategory);
        }
    }

    @Override
    public void doUpdateLiveCategoryStatus(String categoryId, String status) {
        LiveCategory liveCategory = categoryDao.get(categoryId);
        List<Banner> bannerList = bannerDao.findBannerByCategory(categoryId);
        if (liveCategory != null) {
            liveCategory.setStatus(status);
            if (status.equals(Global.Status.INACTIVE)) {
                bannerList.forEach(banner -> {
                    banner.setStatus(Global.Status.INACTIVE);
                });
            }
            bannerDao.saveAll(bannerList);
            categoryDao.update(liveCategory);
            bannerList.clear();
            bannerList = null;
        }
    }

    @Override
    public LiveCategory findCategoryByTypeByStatus(String type, String status) {
        return categoryDao.findCategoryByTypeByStatus(type, status);
    }

    @Override
    public String getLocalizeCategoryNameByType(Locale locale, String type) {
        return categoryDao.getLocalizeCategoryNameByType(locale, type);
    }

    @Override
    public List<OptionBean> getLiveCategoryOptions(Locale locale, String memberLiveStreamType) {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        List<LiveCategory> categoryList = new ArrayList<>();

        String[] categories = StringUtils.split(memberLiveStreamType, "|");
        for (String type : categories) {
            LiveCategory category = findCategoryByTypeByStatus(type, Global.Status.ACTIVE);
            if (category != null) {
                categoryList.add(category);
            }
        }
        categoryList.sort(Comparator.comparing(LiveCategory::getSeq));

        return optionBeanUtil.getLiveStreamCategory(categoryList);
    }

    @Override
    public List<LiveCategory> doFindAllLiveCategory(boolean includeVirtualCategory) {
        if (includeVirtualCategory) {
            doFindVirtualCategory();
        }

        return categoryDao.findAllActiveCategory(includeVirtualCategory);
    }

    @Override
    public LiveCategory doFindVirtualCategory() {
        LiveCategory liveCategory = findCategoryByTypeByStatus(LiveCategory.VIRTUAL_CATEGORY.toUpperCase(), null);
        if (liveCategory == null) {
            saveLiveCategory(LiveCategory.VIRTUAL_CATEGORY, LiveCategory.VIRTUAL_CATEGORY_CN, LiveCategory.VIRTUAL_CATEGORY_MS, 0);
            liveCategory = findCategoryByTypeByStatus(LiveCategory.VIRTUAL_CATEGORY.toUpperCase(), null);
        }

        return liveCategory;
    }

    @Override
    public List<LiveCategory> getLiveCategoryList(boolean isCn, boolean isMs) {
        RedisCacheProvider redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME,
                RedisCacheProvider.class);

        List<LiveCategory> liveCategoryList;
        String fileLanguage = isCn ? BannerFile.LANG_CN : isMs ? BannerFile.LANG_MS : BannerFile.LANG_EN;
        if(redisCacheProvider.getBannerStatus() && redisCacheProvider.getRedisStatus()) {
            try {
                liveCategoryList = redisCacheProvider.getLiveCategoryRList().get(fileLanguage);
                return liveCategoryList.size() > 0 ? liveCategoryList : null;

            } catch (RedisConnectionException redisEx) {
                redisCacheProvider.setRedisStatus(false);
                return processBanner(fileLanguage);
            }
        } else if(!redisCacheProvider.getBannerStatus() && redisCacheProvider.getRedisStatus()) {
            List<String> language = new ArrayList<>();

            language.add("EN");
            language.add("CN");
            language.add("MS");

            redisCacheProvider.getLiveCategoryRList().clear();
            for(int i = 0 ; i < language.size() ; i++) {
                liveCategoryList = processBanner(language.get(i));
                if(redisCacheProvider.getRedisStatus()){
                    redisCacheProvider.setLiveCategoryRList(language.get(i),liveCategoryList);
                }
            }
            liveCategoryList = redisCacheProvider.getLiveCategoryRList().get(fileLanguage);

            return liveCategoryList.size() > 0 ? liveCategoryList : processBanner(fileLanguage);
        } else if(!redisCacheProvider.getRedisStatus()){
            return processBanner(fileLanguage);
        }
        return null;
    }

    private List<LiveCategory> processBanner(String language) {
        BannerFileUploadConfiguration bannerFileUploadConfiguration = Application.lookupBean(BannerFileUploadConfiguration.class);
        BannerService bannerService = Application.lookupBean(BannerService.BEAN_NAME, BannerService.class);
        List<LiveCategory> liveCategoryList = doFindAllLiveCategory(true);

        String fileLanguage = language.equalsIgnoreCase("CN") ? BannerFile.LANG_CN : language.equalsIgnoreCase("MS") ? BannerFile.LANG_MS : BannerFile.LANG_EN;
        liveCategoryList.forEach(liveCategory -> {
            liveCategory.setCategory(language.equalsIgnoreCase("CN") ? liveCategory.getCategoryCn() : language.equalsIgnoreCase("MS") ? liveCategory.getCategoryMs() : liveCategory.getCategory());

            List<Banner> bannerList = bannerService.findBannerByCategory(liveCategory.getCategoryId());
            bannerList.forEach(banner -> {
                BannerFile bannerFile = bannerService.findBannerFileByBannerId(banner.getBannerId(), fileLanguage);
                banner.setTitle(language.equalsIgnoreCase("CN")  ? banner.getTitleCn() : language.equalsIgnoreCase("MS")  ? banner.getTitleMs() : banner.getTitle());
                banner.setBody(language.equalsIgnoreCase("CN")  ? banner.getBodyCn() : language.equalsIgnoreCase("MS")  ? banner.getBodyMs() : banner.getBody());
                banner.setCategoryName(language.equalsIgnoreCase("CN")  ? liveCategory.getCategoryCn() : language.equalsIgnoreCase("MS")  ? liveCategory.getCategoryMs() : liveCategory.getCategory());
                if (bannerFile != null) {
                    banner.setFileUrl(bannerFile.getFileUrlWithParentPath(bannerFileUploadConfiguration.getFullBannerParentFileUrl()));
                }
                if (banner.getTimer() == null) {
                    banner.setTimer(0);
                }
            });
            liveCategory.setBanners(bannerList);
        });
        return liveCategoryList;
    }
}
