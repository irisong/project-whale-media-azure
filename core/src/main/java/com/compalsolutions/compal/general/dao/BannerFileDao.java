package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.BannerFile;

import java.util.List;

public interface BannerFileDao extends BasicDao<BannerFile, String> {
    public static final String BEAN_NAME = "bannerFileDao";

    public BannerFile findBannerFileByBannerId(String bannerId, String language);

    public List<BannerFile> findBannerFileListByBannerId(String bannerId);
}
