package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.repository.DocFileRepository;
import com.compalsolutions.compal.general.vo.DocFile;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(DocFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class DocFileDaoImpl extends Jpa2Dao<DocFile, String> implements DocFileDao {
    @SuppressWarnings("unused")
    @Autowired
    private DocFileRepository docFileRepository;

    public DocFileDaoImpl() {
        super(new DocFile(false));
    }

    @Override
    public void findDocFilesForDatagrid(DatagridModel<DocFile> datagridModel, String languageCode, List<String> userGroups, String status) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from d IN " + DocFile.class + " WHERE 1=1 ";
        if (StringUtils.isNotBlank(languageCode)) {
            hql += " and d.languageCode = ? ";
            params.add(languageCode);
        }

        if (CollectionUtil.isNotEmpty(userGroups)) {
            for (String userGroup : userGroups) {
                hql += " and d.userGroups like ? ";
                params.add("%" + userGroup + "%");
            }
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and d.status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "d", hql, params.toArray());
    }

    @Override
    public List<DocFile> findDocFilesForDashboard(String userGroup) {
        DocFile example = new DocFile(false);
        example.addLikeCondition("userGroups", "%" + userGroup + "%");
        example.setStatus(Global.Status.ACTIVE);
        return findByExample(example, "datetimeAdd DESC");
    }
}
