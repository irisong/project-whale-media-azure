package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Message;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(MessageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MessageDaoImpl extends Jpa2Dao<Message, String> implements MessageDao {

    public MessageDaoImpl() {
        super(new Message());
    }

    public Message findMessageByReceiverIdAndMessageId(String receiverId, String messageId) {
        Message message = new Message();
        message.setMemberId(receiverId);
        message.setMessageId(messageId);

        return findUnique(message);
    }

    public List<Message> findAllMessageByReceiverId(String receiverId) {
        Message message = new Message();
        message.setMemberId(receiverId);

        return findByExample(message);
    }

    public Message findMessageByReceiverIdAndDirectToId(String receiverId, String directToMemberId) {
        Message message = new Message();
        message.setMemberId(receiverId);
        message.setDirectToMemberId(directToMemberId);
        message.setMessageType(Global.MessageType.FOLLOWING);

        return findUnique(message);
    }

    // pass memberId if announcement to be send to specific user
    public Message findMessageByTypeAndReferenceAndMemberId(String type, String reference, String memberId) {
        Message message = new Message();
        message.setReference(reference);
        message.setMessageType(type);
        if (StringUtils.isNotBlank(memberId)) {
            message.setMemberId(memberId);
        }

        return findUnique(message);
    }

    public List<Message> findAllMessageByType(String type) {
        Message message = new Message();
        message.setMessageType(type);

        return findByExample(message);
    }
}
