package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(FreqAskQuesFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FreqAskQuesFileDaoImpl extends Jpa2Dao<FreqAskQuesFile, String> implements FreqAskQuesFileDao {
    public FreqAskQuesFileDaoImpl() {
        super(new FreqAskQuesFile(false));
    }

    @Override
    public FreqAskQuesFile findFreqAskQuesFileByQuestionIdByLanguage(String questionId, String language) {
        Validate.notBlank(questionId);
        Validate.notBlank(language);

        List<Object> params = new ArrayList<>();
        String hql= " FROM FreqAskQuesFile WHERE questionId = ? AND language = ? ";
        params.add(questionId);
        params.add(language);

        return findUnique(hql, params.toArray());
    }
}
