package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.DocFile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocFileRepository extends JpaRepository<DocFile, String> {
    public static final String BEAN_NAME = "DocFileRepository";
}
