package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.Country;

import java.util.List;

public interface CountryDao extends BasicDao<Country, String> {
    public static final String BEAN_NAME = "countryDao";

    public List<Country> findAllCountriesForRegistration();

    public Country findCountryByCountryNameToUpper(String countryName);
}
