package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.repository.AnnouncementRepository;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(AnnouncementDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AnnouncementDaoImpl extends Jpa2Dao<Announcement, String> implements AnnouncementDao {
    @SuppressWarnings("unused")
    @Autowired
    private AnnouncementRepository announcementRepository;

    public AnnouncementDaoImpl() {
        super(new Announcement(false));
    }

    @Override
    public void findAnnouncementsForDatagrid(DatagridModel<Announcement> datagridModel, List<String> userGroups, String status, String announcementType,
            Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + Announcement.class + " WHERE 1=1 ";
        if (CollectionUtil.isNotEmpty(userGroups)) {
            for (String userGroup : userGroups) {
                hql += " and a.userGroups like ? ";
                params.add("%" + userGroup + "%");
            }
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (StringUtils.isNotBlank(announcementType)) {
            hql += " and a.announcementType=? ";
            params.add(announcementType);
        }

        if (dateFrom != null) {
            hql += " and a.publishDate>=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and a.publishDate<=? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public int getTotalRecordsFor(List<String> userGroups) {
        List<Object> params = new ArrayList<Object>();

        String hql = "SELECT COUNT(a) FROM a IN " + Announcement.class + " WHERE a.status=? ";
        params.add(Global.Status.ACTIVE);

        if (CollectionUtil.isNotEmpty(userGroups)) {
            for (String userGroup : userGroups) {
                hql += " and a.userGroups like ? ";
                params.add("%" + userGroup + "%");
            }
        }

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }

    @Override
    public List<Announcement> findAnnouncementsForDashboard(String userGroup, String languageCode, String accessModule, int pageNo, int pageSize) {
        List<Object> params = new ArrayList<Object>();
        String hql = "FROM a IN " + Announcement.class + " WHERE a.status=? and a.userGroups like ? ";
        params.add(Global.Status.ACTIVE);
        params.add("%" + userGroup + "%");

        if(StringUtils.isNotBlank(languageCode)){
            hql += " and a.languageCode=? ";
            params.add(languageCode);
        }

        if(StringUtils.isNotBlank(accessModule)){
            hql += " and a.accessModule=? ";
            params.add(accessModule);
        }

        hql += "order by a.publishDate DESC";

        return this.findBlock(hql, (pageNo - 1) * pageSize, pageSize, params.toArray());
    }

    @Override
    public List<Announcement> findPendingAnnouncementToSend(Date untilDate) {
        String hql = "FROM a IN " + Announcement.class + " WHERE a.status = ? AND a.publishDate <= ? ";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.PENDING);
        params.add(untilDate);

        hql += "ORDER BY a.publishDate ";

        return findQueryAsList(hql, params.toArray());
    }
}
