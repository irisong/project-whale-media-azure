package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.CountryDesc;

import java.util.List;
import java.util.Locale;

public interface CountryDescDao extends BasicDao<CountryDesc, String> {
    public static final String BEAN_NAME = "countryDescDao";

    public List<CountryDesc> findCountryDescsByLocale(Locale locale);

    public CountryDesc findCountryDescByLocaleAndCountryCode(Locale locale, String countryCode);

}
