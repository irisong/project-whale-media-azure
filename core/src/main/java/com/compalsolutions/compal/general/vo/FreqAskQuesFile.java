package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.File;
import java.util.Objects;

@Entity
@Table(name = "app_freq_ask_ques_file")
@Access(AccessType.FIELD)
public class FreqAskQuesFile extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String LANG_EN = "EN";
    public static final String LANG_CN = "CN";
    public static final String LANG_MS = "MS";
    public static final String COMBINED = "COMBINED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "question_file_id", unique = true, nullable = false, length = 32)
    private String questionFileId;

    @Column(name = "question_id", nullable = false, length = 32)
    private String questionId;

    @ToTrim
    @Column(name = "language", length = 50, nullable = false)
    private String language;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;

    @ManyToOne
    @JoinColumn(name = "question_id", insertable = false, updatable = false, nullable = true)
    private FreqAskQues freqAskQues;

    @Transient
    private String renamedFilename;

    @Transient
    private File fileUpload;

    public FreqAskQuesFile() {
    }

    public FreqAskQuesFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getQuestionFileId() {
        return questionFileId;
    }

    public void setQuestionFileId(String questionFileId) {
        this.questionFileId = questionFileId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public FreqAskQues getFreqAskQues() {
        return freqAskQues;
    }

    public void setFreqAskQues(FreqAskQues freqAskQues) {
        this.freqAskQues = freqAskQues;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(questionFileId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = questionFileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        FreqAskQuesFile that = (FreqAskQuesFile) o;

        return Objects.equals(questionFileId, that.questionFileId);

    }

    @Override
    public int hashCode() {
        return Objects.hash(questionFileId);
    }
}
