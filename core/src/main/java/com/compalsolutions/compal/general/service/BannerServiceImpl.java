package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.BannerFileUploadConfiguration;
import com.compalsolutions.compal.MemberFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.BannerDao;
import com.compalsolutions.compal.general.dao.BannerFileDao;
import com.compalsolutions.compal.general.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(BannerService.BEAN_NAME)
public class BannerServiceImpl implements BannerService {
    @Autowired
    private BannerDao bannerDao;

    @Autowired
    private BannerFileDao bannerFileDao;

    @Override
    public void findBannersForListing(DatagridModel<Banner> datagridModel, List<String> userGroups, String status, String bannerType, String category) {

        bannerDao.findBannersForDatagrid(datagridModel, userGroups, status, bannerType, category);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        for (Banner banner : datagridModel.getRecords()) {
            banner.setBannerTypeDesc(i18n.getText(banner.getBannerType()));
            setBannerFileUrl(banner);
        }
    }

    @Override
    public void setBannerFileUrl(Banner banner) {
        BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);

        for (BannerFile file : banner.getBannerFile()) {
            String fileUrl = file.getFileUrlWithParentPath(config.getFullBannerParentFileUrl());

            switch (file.getLanguage()) {
                case BannerFile.LANG_EN:
                    banner.setFileUrlEn(fileUrl);
                    break;
                case BannerFile.LANG_CN:
                    banner.setFileUrlCn(fileUrl);
                    break;
                case BannerFile.LANG_MS:
                    banner.setFileUrlMs(fileUrl);
                    break;
                case BannerFile.NON_SPECIFIED:
                    banner.setFileUrl(fileUrl);
                    break;
            }

        }

    }

    @Override
    public void saveBanner(Locale locale, Banner banner, List<BannerFile> bannerFileList) {
        BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);
        final String uploadPath = config.getBannerFileUploadPath();

        if (banner != null) {
            updateBannerSeq("ADD", 0, banner.getSeq());
            bannerDao.save(banner);
        }

        for (BannerFile bannerFile : bannerFileList) {
            doSaveBannerFile(uploadPath, banner.getBannerId(), bannerFile);
        }
    }

    private void updateBannerSeq(String action, int oldSeq, int newSeq) {
        if (action.equalsIgnoreCase("UPDATE")) {
            if (oldSeq != newSeq) {
                bannerDao.updateBannerSeq("OLD", oldSeq);
                bannerDao.updateBannerSeq("NEW", newSeq);
            }
        } else { //ADD
            bannerDao.updateBannerSeq("NEW", newSeq);
        }
    }

    @Override
    public void doSaveBannerFile(String uploadPath, String bannerId, BannerFile bannerFile) {
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        if (StringUtils.isNotBlank(bannerFile.getFilename())) {
            BannerFile bannerFiledb = new BannerFile();
            bannerFiledb.setBannerId(bannerId);
            bannerFiledb.setLanguage(bannerFile.getLanguage());
            bannerFiledb.setFilename(bannerFile.getFilename());
            bannerFiledb.setContentType(bannerFile.getContentType());
            bannerFiledb.setFileSize(bannerFile.getFileSize());
            bannerFileDao.save(bannerFiledb);

            // production using blobstorage in azure
            if (isProdServer) {
                azureBlobStorageService.uploadFileToAzureBlobStorage(bannerFiledb.getRenamedFilename(),
                        null, bannerFile.getFileUpload(), bannerFile.getContentType(), BannerFileUploadConfiguration.FOLDER_NAME);
            } else {
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                try {
                    byte[] bytes = Files.readAllBytes(Paths.get(bannerFile.getFileUpload().getAbsolutePath()));

                    Path path = Paths.get(uploadPath, bannerFiledb.getRenamedFilename());
                    Files.write(path, getScaledImage(bytes, 500, 0));
                } catch (IOException e) {
                    throw new SystemErrorException(e.getMessage(), e);
                }
            }
        }
    }

    @Override
    public void updateBanner(Locale locale, Banner banner, List<BannerFile> bannerFileList) {
        BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        final String uploadPath = config.getBannerFileUploadPath();
        Banner dbBanner = bannerDao.get(banner.getBannerId());
        if (dbBanner == null) {
            throw new DataException(i18n.getText("invalidAnnouncement", locale));
        } else {
            updateBannerSeq("UPDATE", dbBanner.getSeq(), banner.getSeq());

            dbBanner.setTitle(banner.getTitle());
            dbBanner.setTitleCn(banner.getTitleCn());
            dbBanner.setStatus(banner.getStatus());
            dbBanner.setBody(banner.getBody());
            dbBanner.setBodyCn(banner.getBodyCn());
            dbBanner.setSeq(banner.getSeq());
            dbBanner.setCategoryId(banner.getCategoryId());
            dbBanner.setRedirectUrl(banner.getRedirectUrl());
            dbBanner.setBannerType(banner.getBannerType());
            bannerDao.update(dbBanner);
        }

        for (BannerFile bannerFile : bannerFileList) {
            doSaveBannerFile(uploadPath, banner.getBannerId(), bannerFile);
        }
    }

    @Override
    public Banner getBanner(String bannerId) {
        return bannerDao.get(bannerId);
    }

    @Override
    public BannerFile findBannerFileByBannerId(String bannerId, String language) {
        BannerFile bannerFile = bannerFileDao.findBannerFileByBannerId(bannerId, language);
        if (bannerFile == null) {
            bannerFile = bannerFileDao.findBannerFileByBannerId(bannerId, BannerFile.NON_SPECIFIED); // return common file while language file no exist
        }
        return bannerFile;
    }

    @Override
    public List<Banner> findAllActiveBanner() {
        return bannerDao.findAllActiveBanner();
    }

    @Override
    public List<Banner> findBannerByCategory(String categoryId) {
        return bannerDao.findBannerByCategory(categoryId);
    }

    @Override
    public void doDeleteBanner(String bannerId) {
        try {
            Banner banner = bannerDao.get(bannerId);
            BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);
            List<BannerFile> bannerFileList = bannerFileDao.findBannerFileListByBannerId(bannerId);
            for (BannerFile bannerFile : bannerFileList) {
                bannerFile.setFileUrlWithParentPath(config.getUploadPath());
                Files.deleteIfExists(Paths.get(bannerFile.getFileUrl()));
            }
            bannerFileDao.deleteAll(bannerFileList);
            bannerFileList.clear();
            bannerDao.delete(banner);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doDeleteBannerFile(String bannerId, String language) {
        try {
            BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);
            BannerFile bannerFile = bannerFileDao.findBannerFileByBannerId(bannerId, language);
            bannerFile.setFileUrlWithParentPath(config.getUploadPath());
            Files.deleteIfExists(Paths.get(bannerFile.getFileUrl()));
            bannerFileDao.delete(bannerFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public byte[] getScaledImage(byte[] srcImg, Integer width, Integer height) {
        ByteArrayInputStream in = new ByteArrayInputStream(srcImg);
        try {
            BufferedImage img = ImageIO.read(in);
            if (height == 0) {
                height = (width * img.getHeight()) / img.getWidth();
            }
            if (width == 0) {
                width = (height * img.getWidth()) / img.getHeight();
            }
            Image scaledImage = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
            BufferedImage imageBuff = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            imageBuff.getGraphics().drawImage(scaledImage, 0, 0, new Color(0, 0, 0), null);

            ByteArrayOutputStream buffer = new ByteArrayOutputStream();

            ImageIO.write(imageBuff, "png", buffer);
            buffer.close();

            return buffer.toByteArray();
        } catch (IOException e) {
            throw new ValidatorException("Image cannot be save.");
        }
    }

    @Override
    public int getNextBannerSeq(String categoryId) {
        return bannerDao.getNextBannerSeq(categoryId);
    }

    @Override
    public int getNextBannerCategorySeq() {
        return bannerDao.getNextBannerCategorySeq();
    }
}
