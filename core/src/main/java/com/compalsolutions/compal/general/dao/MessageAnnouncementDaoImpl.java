package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.general.vo.MessageAnnouncementRemoved;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MessageAnnouncementDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MessageAnnouncementDaoImpl extends Jpa2Dao<MessageAnnouncementRemoved, String> implements MessageAnnouncementDao {

    public MessageAnnouncementDaoImpl() {
        super(new MessageAnnouncementRemoved());
    }

    @Override
    public void doDeleteAnnounceMessage(String memberId, Message message) {
        MessageAnnouncementRemoved messageAnnouncementRemoved = new MessageAnnouncementRemoved();
        messageAnnouncementRemoved.setMemberId(memberId);
        messageAnnouncementRemoved.setAnnouncementId(message.getReference());

        messageAnnouncementRemoved = findUnique(messageAnnouncementRemoved);

        if (messageAnnouncementRemoved == null) {
            messageAnnouncementRemoved = new MessageAnnouncementRemoved();
            messageAnnouncementRemoved.setMemberId(memberId);
            messageAnnouncementRemoved.setAnnouncementId(message.getReference());
            messageAnnouncementRemoved.setStatus(Global.Status.DELETED);

            save(messageAnnouncementRemoved);
        } else {
            if (!messageAnnouncementRemoved.getStatus().equals(Global.Status.DELETED)) {
                messageAnnouncementRemoved.setStatus(Global.Status.DELETED);

                update(messageAnnouncementRemoved);
            }
        }
    }
}
