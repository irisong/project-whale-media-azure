package com.compalsolutions.compal.general.service;

public interface DocNoService {
    public static final String BEAN_NAME = "docNoService";

    public String doGetNextHelpDeskTicketNo();

    public String doGetNextSalesOrderDocNo();

    public String doGetNextDeliveryOrderDocNo();

    public String doGetNextWalletAdjustDocNo();

    public String doGetNextWalletTransferDocNo();

    String doGetNextFixDepositDocNo();

    public String doGetNextWalletPaymentDocNo();

    public String doGetNextWalletWithdrawDocNo();

    public String doGetNextOmcWalletWithdrawDocNo();

    public String doGetNextWalletTopupDocNo();

    public String doGetNextWalletSwapDocNo();

    public String doGetNextWalletExchangeDocNo();

    public String doGetNextWalletExchange2CryptoDocNo();

    public String doGetNextWalletExchange2ExternalDocNo();

    public String doGetNextTradeOrderNo();

    public String doGetNextInterestAccountDepositDocNo();

    public String doGetNextInterestAccountWithdrawDocNo();

    public String doGetNextInterestAccountPayoutDocNo();

    public String doGetNextVipOrderDocNo();

    public String doGetNexPackageOrderDocNo();

    public String doGetNextOmnipayRebateDocNo();

    public String doGetNextBonusWalletAdjustDocNo();

    public String doGetNextWalletMerchantExchangeDocNo();

    public String doGetNextEquityShareSubscriptionDocNo();

    public String doGetNextAnnivEquitySharePurchaseDocNo();

    public boolean doGetHuifuPromotionFlag();

    int doGetNoOfVirtualMinerBuffer();

    boolean doGetIsEnableOmcWalletMatchingBonus();

    public String doGetNextTopUpRefId();

    public String doGetNextAudienceRefId();

    public String doGetNextOrderId();

    public String doGetNextLuckyDrawPackageNo();

    public String doGetNextMerchantOrderRefNo();

    public String doGetNextMerchantPaymentRefNo();
}
