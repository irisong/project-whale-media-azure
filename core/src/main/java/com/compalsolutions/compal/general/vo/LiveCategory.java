package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "app_live_category")
@Access(AccessType.FIELD)
public class LiveCategory extends VoBase {

    private static final long serialVersionUID = 1L;

    public final static String VIRTUAL_CATEGORY = "Trending";
    public final static String VIRTUAL_CATEGORY_CN = "热门";
    public final static String VIRTUAL_CATEGORY_MS = "Tren";

    public final static String DEFAULT_CATEGORY = "Talent";
    public final static String DEFAULT_CATEGORY_2 = "Chat";
    public final static String DEFAULT_CHINA_CATEGORY = "Other";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "category_id", unique = true, nullable = false, length = 32)
    private String categoryId;

    @ToTrim
    @Column(name = "type", length = 100)
    private String type;

    @ToTrim
    @Column(name = "category", columnDefinition = Global.ColumnDef.TEXT)
    private String category;

    @ToTrim
    @Column(name = "category_cn", columnDefinition = Global.ColumnDef.TEXT)
    private String categoryCn;

    @ToTrim
    @Column(name = "category_ms", columnDefinition = Global.ColumnDef.TEXT)
    private String categoryMs;

    @ToTrim
    @Column(name = "status", length = 20)
    private String status;

    @ToTrim
    @Column(name = "seq")
    private Integer seq;

    @Transient
    private String statusDesc;

    @Transient
    private List<Banner> banners;

    public LiveCategory() {
    }

    public LiveCategory(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }

    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryCn() {
        return categoryCn;
    }

    public void setCategoryCn(String categoryCn) {
        this.categoryCn = categoryCn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusDesc() {
        return statusDesc;
    }

    public void setStatusDesc(String statusDesc) {
        this.statusDesc = statusDesc;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getCategoryMs() {
        return categoryMs;
    }

    public void setCategoryMs(String categoryMs) {
        this.categoryMs = categoryMs;
    }

    public List<Banner> getBanners() {
        return banners;
    }

    public void setBanners(List<Banner> banners) {
        this.banners = banners;
    }
}
