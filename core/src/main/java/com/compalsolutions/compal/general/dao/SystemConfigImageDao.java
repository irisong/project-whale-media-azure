package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.SystemConfigImage;

import java.util.List;

public interface SystemConfigImageDao extends BasicDao<SystemConfigImage, String> {
    public static final String BEAN_NAME = "systemConfigImageDao";

    public SystemConfigImage findSystemConfigImageByType(String imageType);

    public List<SystemConfigImage> findSystemConfigImages();
}
