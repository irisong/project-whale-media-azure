package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.DocFile;

import java.util.List;

public interface DocFileDao extends BasicDao<DocFile, String> {
    public static final String BEAN_NAME = "docFileDao";

    public void findDocFilesForDatagrid(DatagridModel<DocFile> datagridModel, String languageCode, List<String> userGroups, String status);

    public List<DocFile> findDocFilesForDashboard(String userGroup);
}
