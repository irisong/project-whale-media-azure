package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.general.vo.Message;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Date;

public interface MessageService {
    public static final Log log = LogFactory.getLog(MessageService.class);
    public static final String BEAN_NAME = "messageService";

    public void findAllMessageByReceiverId(DatagridModel<Message> datagridModel, String receiverId);

    public void doGenerateFollowingMessage(String followerId, String memberId);

    public Message doGenerateWarningMessage(String memberId, String content, String contentCn);

    public Message doGenerateReplyWarningMessage(String memberId);

    public void doGenerateAnnouncementMessage(Announcement announcement);

    public void doUpdateAnnouncementMessage(Announcement announcement);

    public Message doGenerateNoticeMessage(String memberId, String title, String titleCn, String content, String contentCn);

    public void doDeleteMessage(String receiverId, String messageId);

    public Message findMessageByMessageId(String messageId);

    public Message doGenerateMissionPrizeMessage(String memberId, VjMissionDetail vjMissionDetail);

 }
