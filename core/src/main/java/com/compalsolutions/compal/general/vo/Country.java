package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_country")
@Access(AccessType.FIELD)
public class Country extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", unique = true, nullable = false, length = 10)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_name", nullable = false, length = 100)
    private String countryName;

    @ToUpperCase
    @ToTrim
    @Column(name = "currency_code", length = 10)
    private String currencyCode;

    @Column(name = "show_register", nullable = false)
    private Boolean showRegister;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_code", insertable = false, updatable = false, nullable = true)
    private List<CountryDesc> countryDescs = new ArrayList<CountryDesc>();

    @Column(name = "phone_country_code", length = 10)
    private String phoneCountryCode;

    public Country() {
    }

    public Country(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public Boolean getShowRegister() {
        return showRegister;
    }

    public void setShowRegister(Boolean showRegister) {
        this.showRegister = showRegister;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public Boolean isShowRegister() {
        return showRegister;
    }

    public List<CountryDesc> getCountryDescs() {
        return countryDescs;
    }

    public void setCountryDescs(List<CountryDesc> countryDescs) {
        this.countryDescs = countryDescs;
    }

    public String getPhoneCountryCode() {
        return phoneCountryCode;
    }

    public void setPhoneCountryCode(String phoneCountryCode) {
        this.phoneCountryCode = phoneCountryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Country country = (Country) o;

        if (countryCode != null ? !countryCode.equals(country.countryCode) : country.countryCode != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return countryCode != null ? countryCode.hashCode() : 0;
    }
}
