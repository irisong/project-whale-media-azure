package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_help_desk_type")
@Access(AccessType.FIELD)
public class HelpDeskType extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ticket_type_id", unique = true, nullable = false, length = 32)
    private String ticketTypeId;

    @ToTrim
    @ToUpperCase
    @Column(name = "type_name", length = 100, nullable = false)
    private String typeName;

    @ToTrim
    @ToUpperCase
    @Column(name = "type_name_cn", length = 100, nullable = false)
    private String typeNameCn;

    @ToTrim
    @ToUpperCase
    @Column(name = "type_name_ms", length = 100, nullable = false)
    private String typeNameMs;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Column(name = "sort_order", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer sortOrder;

    public HelpDeskType() {
    }

    public HelpDeskType(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
        }
    }

    public String getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(String ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeNameCn() {
        return typeNameCn;
    }

    public void setTypeNameCn(String typeNameCn) {
        this.typeNameCn = typeNameCn;
    }

    public String getTypeNameMs() {
        return typeNameMs;
    }

    public void setTypeNameMs(String typeNameMs) {
        this.typeNameMs = typeNameMs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        HelpDeskType that = (HelpDeskType) o;

        if (ticketTypeId != null ? !ticketTypeId.equals(that.ticketTypeId) : that.ticketTypeId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return ticketTypeId != null ? ticketTypeId.hashCode() : 0;
    }
}
