package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Banner;

import java.util.Date;
import java.util.List;

public interface BannerDao extends BasicDao<Banner, String> {
    public static final String BEAN_NAME = "bannerDao";

    public void findBannersForDatagrid(DatagridModel<Banner> datagridModel, List<String> userGroups, String status, String bannerType,String category);

    public List<Banner> findAllActiveBanner();

    public List<Banner> findBannerByCategory(String categoryId);

    public int getNextBannerSeq(String categoryId);

    public int getNextBannerCategorySeq();

    public void updateBannerSeq(String type, int seq);
}
