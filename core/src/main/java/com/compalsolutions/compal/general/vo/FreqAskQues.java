package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_freq_ask_ques")
@Access(AccessType.FIELD)
public class FreqAskQues extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "question_id", unique = true, nullable = false, length = 32)
    private String questionId;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "question_id", insertable = false, updatable = false, nullable = true)
    private List<FreqAskQuesFile> freqAskQuesFile = new ArrayList<>();

    @Column(name = "title", nullable = false, length = 100)
    private String title;

    @Column(name = "title_cn", length = 100)
    private String titleCn;

    @Column(name = "title_ms", length = 100)
    private String titleMs;

    @Column(name = "status", nullable = false, length = 50)
    private String status;

    @Column(name = "sort_order", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer sortOrder;

    @Transient
    private String freqAskQuesFileUrlEn;

    @Transient
    private String freqAskQuesFileUrlCn;

    @Transient
    private String freqAskQuesFileUrlMs;

    @Transient
    private String freqAskQuesFileUrlCombine;

    @Transient
    private String category;

    @Transient
    private String freqAskQuesFileUrl;

    public FreqAskQues() {
    }

    public FreqAskQues(boolean defaultValue) {
        if (defaultValue) {
            status = Global.Status.ACTIVE;
            sortOrder = 0;
        }
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<FreqAskQuesFile> getFreqAskQuesFile() {
        return freqAskQuesFile;
    }

    public void setFreqAskQuesFile(List<FreqAskQuesFile> freqAskQuesFile) {
        this.freqAskQuesFile = freqAskQuesFile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleCn() {
        return titleCn;
    }

    public void setTitleCn(String titleCn) {
        this.titleCn = titleCn;
    }

    public String getTitleMs() {
        return titleMs;
    }

    public void setTitleMs(String titleMs) {
        this.titleMs = titleMs;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getFreqAskQuesFileUrlEn() {
        return freqAskQuesFileUrlEn;
    }

    public void setFreqAskQuesFileUrlEn(String freqAskQuesFileUrlEn) {
        this.freqAskQuesFileUrlEn = freqAskQuesFileUrlEn;
    }

    public String getFreqAskQuesFileUrlCn() {
        return freqAskQuesFileUrlCn;
    }

    public void setFreqAskQuesFileUrlCn(String freqAskQuesFileUrlCn) {
        this.freqAskQuesFileUrlCn = freqAskQuesFileUrlCn;
    }

    public String getFreqAskQuesFileUrlMs() {
        return freqAskQuesFileUrlMs;
    }

    public void setFreqAskQuesFileUrlMs(String freqAskQuesFileUrlMs) {
        this.freqAskQuesFileUrlMs = freqAskQuesFileUrlMs;
    }

    public String getFreqAskQuesFileUrlCombine() {
        return freqAskQuesFileUrlCombine;
    }

    public void setFreqAskQuesFileUrlCombine(String freqAskQuesFileUrlCombine) {
        this.freqAskQuesFileUrlCombine = freqAskQuesFileUrlCombine;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getFreqAskQuesFileUrl() {
        return freqAskQuesFileUrl;
    }

    public void setFreqAskQuesFileUrl(String freqAskQuesFileUrl) {
        this.freqAskQuesFileUrl = freqAskQuesFileUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        FreqAskQues that = (FreqAskQues) o;

        if (questionId != null ? !questionId.equals(that.questionId) : that.questionId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return questionId != null ? questionId.hashCode() : 0;
    }
}
