package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import javax.persistence.*;

@Entity
@Table(name = "app_city")
@Access(AccessType.FIELD)
public class City extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "city_code", unique = true, nullable = false, length = 10)
    private String cityCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "province_code", nullable = false, length = 10)
    private String provinceCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "city_name", nullable = false, length = 100)
    private String cityName;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public City() {
    }

    public City(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        return cityCode != null ? cityCode.hashCode() : 0;
    }
}
