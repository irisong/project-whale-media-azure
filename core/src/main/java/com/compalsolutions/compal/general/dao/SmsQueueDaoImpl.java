package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.SmsQueue;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(SmsQueueDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SmsQueueDaoImpl extends Jpa2Dao<SmsQueue, String> implements SmsQueueDao {

    public SmsQueueDaoImpl() {
        super(new SmsQueue(false));
    }

    @Override
    public SmsQueue getFirstNotProcessSms() {
        List<Object> params = new ArrayList<Object>();

        String sql = "FROM q IN " + SmsQueue.class + " WHERE q.status = ?  ORDER BY q.datetimeAdd ASC ";
        params.add(SmsQueue.SMS_STATUS_PENDING);

        return findFirst(sql, params.toArray());
    }

    @Override
    public void findSmsqForDatagrid(DatagridModel<SmsQueue> datagridModel, String smsTo, String status) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + SmsQueue.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (StringUtils.isNotBlank(smsTo)) {
            hql += " and a.smsTo like ? ";
            params.add(smsTo + "%");
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }
}
