package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.Province;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(ProvinceDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ProvinceDaoImpl extends Jpa2Dao<Province, String> implements ProvinceDao {
    public ProvinceDaoImpl() {
        super(new Province(false));
    }

    @Override
    public List<Province> findAllActiveProvince() {
        Province example = new Province(false);
        example.setStatus(Global.Status.ACTIVE);
        return findByExample(example, "countryCode");
    }

    @Override
    public List<Province> findProvinceByCountryCode(String countryCode) {
        Province example = new Province(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setCountryCode(countryCode);
        return findByExample(example, "countryCode");
    }

    @Override
    public Province findProvinceByProvinceCode(String provinceCode) {
        Province province = new Province(false);
        province.setStatus(Global.Status.ACTIVE);
        province.setProvinceCode(provinceCode);
        return findUnique(province);
    }

    @Override
    public Province findProvinceByProvinceName(String provinceName) {
        Province example = new Province(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setProvinceName(provinceName);
        return findFirst(example);
    }
}