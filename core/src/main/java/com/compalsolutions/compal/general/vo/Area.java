package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import javax.persistence.*;

@Entity
@Table(name = "app_area")
@Access(AccessType.FIELD)
public class Area extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "area_code", unique = true, nullable = false, length = 10)
    private String areaCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "city_code", nullable = false, length = 10)
    private String cityCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "area_name", nullable = false, length = 100)
    private String areaName;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    public Area() {
    }

    public Area(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
