package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.GuildFileUploadConfiguration;
import com.compalsolutions.compal.HelpDeskFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.dao.HelpDeskDao;
import com.compalsolutions.compal.general.dao.HelpDeskReplyDao;
import com.compalsolutions.compal.general.dao.HelpDeskReplyFileDao;
import com.compalsolutions.compal.general.dao.HelpDeskTypeDao;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.general.vo.HelpDeskReply;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(HelpDeskService.BEAN_NAME)
public class HelpDeskServiceImpl implements HelpDeskService {
    private static Object synchronizedObject = new Object();
    private static final Log log = LogFactory.getLog(HelpDeskService.class);

    @Autowired
    private HelpDeskTypeDao helpDeskTypeDao;

    @Autowired
    private HelpDeskDao helpDeskDao;

    @Autowired
    private HelpDeskReplyDao helpDeskReplyDao;

    @Autowired
    private HelpDeskReplyFileDao helpDeskReplyFileDao;

    @Override
    public List<HelpDeskType> findHelpDeskTypes(String status) {
        return helpDeskTypeDao.findHelpDeskTypes(status);
    }

    @Override
    public HelpDeskType getHelpDeskType(String ticketTypeId) {
        return helpDeskTypeDao.get(ticketTypeId);
    }

    @Override
    public String getHelpDeskTypeNameByLanguage(String ticketTypeId, String language) {
        String helpDeskTypeName;
        HelpDeskType helpDeskType = helpDeskTypeDao.get(ticketTypeId);
        switch (language) {
            case Global.Language.CHINESE:
                helpDeskTypeName = helpDeskType.getTypeNameCn();
                break;
            case Global.Language.MALAY:
                helpDeskTypeName = helpDeskType.getTypeNameMs();
                break;
            default:
                helpDeskTypeName = helpDeskType.getTypeName();
                break;
        }

        return helpDeskTypeName;
    }

    @Override
    public void findHelpDeskTypeForListing(DatagridModel<HelpDeskType> datagridModel, String status) {
        helpDeskTypeDao.findHelpDeskTypeForDatagrid(datagridModel, status);
    }

    @Override
    public List<HelpDesk> findHelpDeskByUserId(String userId, String ticketTypeId) {
        return helpDeskDao.findHelpDeskByUserId(userId, ticketTypeId);
    }

    @Override
    public HelpDesk getHelpDesk(String ticketId) {
        return helpDeskDao.get(ticketId);
    }

    @Override
    public void doProcessHelpDeskTicket(Locale locale, User user, String ticketId, String subject, String ticketTypeId, String message,
                                        List<HelpDeskReplyFile> uploadImageList) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        DocNoService docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);

        /************************
         * VERIFICATION - START
         ************************/
        if (StringUtils.isBlank(message)) {
            throw new ValidatorException(i18n.getText("message", locale));
        }
        /************************
         * VERIFICATION - END
         ************************/

        if (StringUtils.isBlank(ticketId)) { //new feedback
            if (StringUtils.isBlank(subject)) {
                throw new ValidatorException(i18n.getText("invalidSubject", locale));
            }
            if (StringUtils.isBlank(ticketTypeId)) { //category
                throw new ValidatorException(i18n.getText("ticketTypeId", locale));
            }

            HelpDesk helpDesk = new HelpDesk(true);
            helpDesk.setTicketNo(docNoService.doGetNextHelpDeskTicketNo());
            helpDesk.setOwnerId(user.getUserId());
            helpDesk.setOwnerType(user.getUserType());
            helpDesk.setTicketTypeId(ticketTypeId);
            helpDesk.setSubject(subject);
            helpDeskDao.save(helpDesk);

            doAddHelpDeskReply(helpDesk.getTicketId(), message, user, uploadImageList);
        } else { //reply feedback
            HelpDesk helpDesk = helpDeskDao.get(ticketId);
            helpDesk.setStatus(HelpDesk.STATUS_OPEN);
            helpDesk.setCloseDatetime(null);
            helpDesk.setCloseBy(null);

            if (user.getUserType().equalsIgnoreCase(Global.UserType.HQ)) {
                helpDesk.setRepliedByAdmin(true);
                helpDesk.setAdminReplyDatetime(new Date());
            } else {
                helpDesk.setRepliedByAdmin(false);
                helpDesk.setAdminReplyDatetime(null);
            }

            helpDeskDao.save(helpDesk);

            doAddHelpDeskReply(ticketId, message, user, uploadImageList);

            if (user.getUserType().equalsIgnoreCase(Global.UserType.HQ)) {
                doProcessHelpDeskAdminReplyNotification(helpDesk);
            }
        }
    }

    @Override
    public void doAddHelpDeskReply(String ticketId, String message, User user, List<HelpDeskReplyFile> uploadImageList) {
        HelpDeskFileUploadConfiguration config = Application.lookupBean(HelpDeskFileUploadConfiguration.BEAN_NAME, HelpDeskFileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        final String uploadPath = config.getHelpDeskFileUploadPath();
        int num = findTotalCountHelpDeskReplyByTicketId(ticketId); //get max count of reply for this ticket

        HelpDeskReply helpDeskReply = new HelpDeskReply(true);
        helpDeskReply.setReplySeq(num + 1);
        helpDeskReply.setTicketId(ticketId);
        helpDeskReply.setSenderId(user.getUserId());
        helpDeskReply.setSenderType(user.getUserType());
        helpDeskReply.setMessage(message);
        helpDeskReplyDao.save(helpDeskReply);

        if (CollectionUtil.isNotEmpty(uploadImageList)) {
            for (HelpDeskReplyFile fileUpload : uploadImageList) {
                if (StringUtils.isNotBlank(fileUpload.getFilename())) {
                    HelpDeskReplyFile helpDeskReplyFile = new HelpDeskReplyFile(true);
                    helpDeskReplyFile.setReplyId(helpDeskReply.getReplyId());
                    helpDeskReplyFile.setFilename(fileUpload.getFilename());
                    helpDeskReplyFile.setContentType(fileUpload.getContentType());
                    helpDeskReplyFile.setFileSize(fileUpload.getFileSize());
                    helpDeskReplyFile.setSortOrder(fileUpload.getSortOrder());
                    helpDeskReplyFileDao.save(helpDeskReplyFile);

                    if (!isProdServer) {
                        File directory = new File(uploadPath);
                        if (!directory.exists()) {
                            directory.mkdirs();
                        }
                    }

                    try {
                        byte[] bytes = fileUpload.getData();
                        if (isProdServer) {
                            azureBlobStorageService.uploadFileToAzureBlobStorage(helpDeskReplyFile.getRenamedFilename(),
                                    null, null, helpDeskReplyFile.getContentType(), HelpDeskFileUploadConfiguration.FOLDER_NAME,
                                    bytes, helpDeskReplyFile.getFileSize());
                        } else {
                            Path path = Paths.get(uploadPath, helpDeskReplyFile.getRenamedFilename());
                            Files.write(path, bytes);
                        }
                    } catch (IOException e) {
                        throw new SystemErrorException(e.getMessage(), e);
                    }
                }

            }
        }
    }

    @Override
    public void doProcessHelpDeskAdminReplyNotification(HelpDesk helpDesk) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);

        Member member = memberService.findMemberByMemberCode(helpDesk.getOwner().getUsername());
        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.FEEDBACK_REPLIED);
        payload.setRedirectUrl(helpDesk.getTicketId());

        NotificationAccount frmAcct = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        NotificationAccount toAcct = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);
        if (frmAcct != null && toAcct != null) {
            notificationConfProvider.doSendMessage(frmAcct.getAccountId(), toAcct.getAccountId(), Global.AccessModule.WHALE_MEDIA,
                    i18n.getText("feedbackReplied", new Locale(member.getPreferLanguage()), helpDesk.getTicketNo(), helpDesk.getSubject()),
                    payload, null);
        }
    }

    @Override
    public int findTotalCountHelpDeskReplyByTicketId(String ticketId) {
        return helpDeskReplyDao.findTotalCountHelpDeskReplyByTicketId(ticketId);
    }

    @Override
    public void saveHelpDeskType(Locale locale, HelpDeskType helpDeskType) {
        helpDeskTypeDao.save(helpDeskType);
    }

    @Override
    public void updateHelpDeskType(Locale locale, HelpDeskType helpDeskType) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        HelpDeskType dbHelpDeskType = helpDeskTypeDao.get(helpDeskType.getTicketTypeId());

        if (dbHelpDeskType == null)
            throw new DataException(i18n.getText("invalidHelpDeskType", locale));

        dbHelpDeskType.setTypeName(helpDeskType.getTypeName());
        dbHelpDeskType.setTypeNameCn(helpDeskType.getTypeNameCn());
        dbHelpDeskType.setTypeNameMs(helpDeskType.getTypeNameMs());
        dbHelpDeskType.setStatus(helpDeskType.getStatus());
        dbHelpDeskType.setSortOrder(helpDeskType.getSortOrder());
        helpDeskTypeDao.update(dbHelpDeskType);
    }

    @Override
    public void findHelpDeskListForListing(DatagridModel<HelpDesk> datagridModel, String memberCode, String status, Date dateFrom,
                                           Date dateTo, String type, Boolean repliedByAdmin) {
        helpDeskDao.findHelpDeskListForDatagrid(datagridModel, memberCode, status, dateFrom, dateTo, type, repliedByAdmin);
    }

    @Override
    public void updateHelpDeskStatus(String ticketId, String status, LoginInfo loginInfo) {
        HelpDesk helpDesk = helpDeskDao.get(ticketId);
        if (helpDesk != null) {
            helpDesk.setStatus(status);

            if (status.equals(HelpDesk.STATUS_CLOSE)) {
                helpDesk.setCloseDatetime(new Date());
                helpDesk.setCloseBy(loginInfo.getUserId());
            } else { //HelpDesk.STATUS_OPEN
                helpDesk.setCloseBy(null);
                helpDesk.setCloseDatetime(null);
            }

            helpDeskDao.update(helpDesk);
        }
    }

    @Override
    public HelpDeskReplyFile getHelpDeskReplyFile(String helpDeskReplyFileId) {
        return helpDeskReplyFileDao.get(helpDeskReplyFileId);
    }

    @Override
    public void doUpdateRepliedHelpDeskToCloseAfter168H() {
        Date date168Hours = DateUtils.addHours(new Date(), -168);

        log.debug("168 Hours: " + date168Hours);
        List<HelpDesk> helpDesksRepliedButOpenList = helpDeskDao.findRepliedNonActiveTicketAfterTimePeriod(date168Hours);

        if (CollectionUtil.isNotEmpty(helpDesksRepliedButOpenList)) {
            for (HelpDesk record : helpDesksRepliedButOpenList) {
                record.setStatus(HelpDesk.STATUS_CLOSE);
                record.setCloseBy(Global.COMPANY_MEMBER);
                record.setCloseDatetime(new Date());

                helpDeskDao.update(record);
            }
        }
        helpDesksRepliedButOpenList.clear();
    }
}
