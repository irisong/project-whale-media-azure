package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "app_message")
@Access(AccessType.FIELD)
public class Message extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "message_id", unique = true, nullable = false, length = 32)
    private String messageId;

    @Column(name = "member_id", length = 32) //receiver Id [null: announcement to all member]
    private String memberId;

    @Column(name = "direct_to_member_id", length = 32)
    private String directToMemberId;

    @ToTrim
    @Column(name = "title", length = 250, nullable = false)
    private String title;

    @ToTrim
    @Column(name = "title_cn", length = 250)
    private String titleCn;

    @ToTrim
    @Column(name = "content", columnDefinition = "text")
    private String content;

    @ToTrim
    @Column(name = "content_cn", columnDefinition = "text")
    private String contentCn;

    @ToTrim
    @ToUpperCase
    @Column(name = "message_type", length = 30, nullable = false)
    private String messageType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "sent_date", nullable = false)
    private Date sentDate;

    @ToTrim
    @Column(name = "sender", length = 20, nullable = false)
    private String sender;

//    @ToTrim
//    @Column(name = "image_url", columnDefinition = "text")
//    private String imageUrl;

    @ToTrim
    @Column(name = "reference", length = 40) //announcement_id
    private String reference;

    public Message() {
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getDirectToMemberId() {
        return directToMemberId;
    }

    public void setDirectToMemberId(String directToMemberId) {
        this.directToMemberId = directToMemberId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleCn() {
        return titleCn;
    }

    public void setTitleCn(String titleCn) {
        this.titleCn = titleCn;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getContentCn() {
        return contentCn;
    }

    public void setContentCn(String contentCn) {
        this.contentCn = contentCn;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public Date getSentDate() {
        return sentDate;
    }

    public void setSentDate(Date sentDate) {
        this.sentDate = sentDate;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

//    public String getImageUrl() {
//        return imageUrl;
//    }
//
//    public void setImageUrl(String imageUrl) {
//        this.imageUrl = imageUrl;
//    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }
}
