package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.repository.AnnouncementRepository;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.general.vo.Country;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.point.vo.Symbolic;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(LiveCategoryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class LiveCategoryDaoImpl extends Jpa2Dao<LiveCategory, String> implements LiveCategoryDao {

    public LiveCategoryDaoImpl() {
        super(new LiveCategory(false));
    }

    @Override
    public List<LiveCategory> findAllActiveCategory(boolean includeVirtualCategory) {
        List<Object> params = new ArrayList<Object>();

        String hql = " FROM s IN " + LiveCategory.class + " WHERE 1=1 ";

        if (!includeVirtualCategory) {
            hql += " AND s.type <> ? ";
            params.add(LiveCategory.VIRTUAL_CATEGORY.toUpperCase());
        }

        hql += " AND s.status = ? order by seq";
        params.add(Global.Status.ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public LiveCategory findOneCategoryByName(String category, String categoryCn, String categoryMs) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM s IN " + LiveCategory.class + " WHERE 1=1 ";
        hql += " AND (s.category = ? OR s.categoryCn = ? OR s.categoryMs = ?) order by seq";
        params.add(category);
        params.add(categoryCn);
        params.add(categoryMs);
        return findFirst(hql, params.toArray());
    }

    @Override
    public LiveCategory findCategoryByTypeByStatus(String type, String status) {
        Validate.notBlank(type);

        LiveCategory liveCategory = new LiveCategory(false);
        liveCategory.setType(type);

        if (StringUtils.isNotBlank(status)) {
            liveCategory.setStatus(status);
        }

        return findUnique(liveCategory);
    }

    @Override
    public void findCategoryForDatagrid(DatagridModel<LiveCategory> datagridModel, String category, String categoryCn, String categoryMs, String status) {
        List<Object> params = new ArrayList<>();

        String hql = "from a IN " + LiveCategory.class + " WHERE 1=1 ";
        if (org.apache.commons.lang3.StringUtils.isNotBlank(category)) {
            hql += " and a.category like ? ";
            params.add("%" + category + "%");
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(categoryCn)) {
            hql += " and a.categoryCn like ? ";
            params.add("%" + categoryCn + "%");
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(categoryMs)) {
            hql += " and a.categoryMs like ? ";
            params.add("%" + categoryMs + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status = ? ";
            params.add(status);
        }
       findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public String getLocalizeCategoryNameByType(Locale locale, String type) {
        Validate.notBlank(type);

        LiveCategory liveCategory = new LiveCategory(false);
        liveCategory.setType(type);

        liveCategory = findUnique(liveCategory);
        if (locale.equals(Global.LOCALE_CN)) {
            return liveCategory.getCategoryCn();
        } else if (locale.equals(Global.LOCALE_MS)) {
            return liveCategory.getCategoryMs();
        }

        return liveCategory.getCategory();
    }

    @Override
    public void updateBannerCategorySeq(String type, int seq) {
        boolean moveSeq = false;

        LiveCategory liveCategory = new LiveCategory(false);
        liveCategory.setSeq(seq);

        List<LiveCategory> duplicateSequence = findByExample(liveCategory);
        if (type.equalsIgnoreCase("NEW")) {
            if (duplicateSequence.size() > 0) {
                moveSeq = true;
            }
        } else {
            if (duplicateSequence.size() == 1) { //no other record having same seq
                moveSeq = true;
            }
        }

        if (moveSeq) {
            String action = "+1";
            if (type.equalsIgnoreCase("OLD")) {
                action = "-1";
            }

            String hql = "UPDATE LiveCategory SET seq = seq" + action + " WHERE seq >= ? ";

            List<Object> params = new ArrayList<>();
            params.add(seq);

            bulkUpdate(hql, params.toArray());
        }
    }
}
