package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(HelpDeskDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpDeskDaoImpl extends Jpa2Dao<HelpDesk, String> implements HelpDeskDao {
    public HelpDeskDaoImpl() {
        super(new HelpDesk(false));
    }

    @Override
    public List<HelpDesk> findHelpDeskByUserId(String userId, String ticketTypeId) {
        Validate.notBlank(userId);

        List<Object> params = new ArrayList<>();
        String hql = "SELECT h FROM HelpDesk h WHERE h.ownerId = ? ";
        params.add(userId);

        if (StringUtils.isNotBlank(ticketTypeId)) {
            hql += "AND h.ticketTypeId = ? ";
            params.add(ticketTypeId);
        }

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public void findHelpDeskListForDatagrid(DatagridModel<HelpDesk> datagridModel, String memberCode, String status, Date dateFrom,
                                            Date dateTo, String type, Boolean repliedByAdmin) {
        List<Object> params = new ArrayList<>();
        String hql = "SELECT h FROM HelpDesk h JOIN h.owner o WHERE 1=1 ";

        if (StringUtils.isNotBlank(memberCode)) {
            hql += " AND o.username like ? ";
            params.add(memberCode + "%");
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " AND h.status = ? ";
            params.add(status);
        }

        if (StringUtils.isNotBlank(type)) {
            hql += " AND h.ticketTypeId = ? ";
            params.add(type);
        }

        if (dateFrom != null) {
            hql += " AND h.datetimeAdd >= ? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if ((dateTo != null)) {
            hql += " AND h.datetimeAdd <= ? ";
            params.add(DateUtil.formatDateToEndTime(dateTo));
        }

        if (repliedByAdmin != null) {
            hql += " AND h.repliedByAdmin = ? ";
            params.add(repliedByAdmin);
        }

        findForDatagrid(datagridModel, "h", hql, params.toArray());
    }

    @Override
    public List<HelpDesk> findRepliedNonActiveTicketAfterTimePeriod (Date period) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM h IN " + HelpDesk.class + " WHERE h.repliedByAdmin = ? AND h.adminReplyDatetime <= ? ";

        params.add(true);
        params.add(period);

        return findQueryAsList(hql, params.toArray());
    }












//    @Override
//    public List<HelpDesk> findEnquiriesByUserId(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo, int pageNo,
//                                                int pageSize) {
//        List<Object> params = new ArrayList<Object>();
//        String hql = "FROM h IN " + HelpDesk.class + " WHERE 1=1 ";
//        if (StringUtils.isNotBlank(userId)) {
//            hql += " and h.ownerId = ? ";
//            params.add(userId);
//        }
//
//        if (StringUtils.isNotBlank(ticketNo)) {
//            hql += " and h.ticketNo like ? ";
//            params.add(ticketNo + "%");
//        }
//
//        if (StringUtils.isNotBlank(ticketTypeId)) {
//            hql += " and h.ticketTypeId = ? ";
//            params.add(ticketTypeId);
//        }
//
//        if (StringUtils.isNotBlank(status)) {
//            hql += " and h.status = ? ";
//            params.add(status);
//        }
//
//        if (dateFrom != null) {
//            hql += " and h.datetimeAdd>=? ";
//            params.add(DateUtil.truncateTime(dateFrom));
//        }
//
//        if (dateTo != null) {
//            hql += " and h.datetimeAdd<=? ";
//            params.add(DateUtil.formatDateToEndTime(dateTo));
//        }
//
//        return this.findBlock(hql, (pageNo - 1) * pageSize, pageSize, params.toArray());
//    }
//
//    @Override
//    public int findEnquiryTotalRecord(String userId, String ticketNo, String ticketTypeId, String status, Date dateFrom, Date dateTo) {
//        List<Object> params = new ArrayList<Object>();
//
//        String hql = "SELECT COUNT(h) FROM h IN " + HelpDesk.class + " WHERE 1=1 ";
//        if (StringUtils.isNotBlank(userId)) {
//            hql += " and h.ownerId = ? ";
//            params.add(userId);
//        }
//
//        if (StringUtils.isNotBlank(ticketNo)) {
//            hql += " and h.ticketNo like ? ";
//            params.add(ticketNo + "%");
//        }
//
//        if (StringUtils.isNotBlank(ticketTypeId)) {
//            hql += " and h.ticketTypeId = ? ";
//            params.add(ticketTypeId);
//        }
//
//        if (StringUtils.isNotBlank(status)) {
//            hql += " and h.status = ? ";
//            params.add(status);
//        }
//
//        if (dateFrom != null) {
//            hql += " and h.datetimeAdd>=? ";
//            params.add(DateUtil.truncateTime(dateFrom));
//        }
//
//        if (dateTo != null) {
//            hql += " and h.datetimeAdd<=? ";
//            params.add(DateUtil.formatDateToEndTime(dateTo));
//        }
//
//        Number result = (Number) this.exFindUnique(hql, params.toArray());
//        if (result != null)
//            return result.intValue();
//
//        return 0;
//    }
//
//
//
//    @Override
//    public List<HelpDesk> findRepliedNonActiveTicketAfter72H (Date date72Hours) {
//
//        List<Object> params = new ArrayList<Object>();
//
//        String hql = "FROM h IN " + HelpDesk.class + " WHERE h.repliedByAdmin = ? AND h.adminReplyDatetime <= ? ";
//
//        params.add(true);
//        params.add(date72Hours);
//
//        return findQueryAsList(hql, params.toArray());
//    }
//
}
