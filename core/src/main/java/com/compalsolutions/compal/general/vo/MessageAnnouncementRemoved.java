package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_message_announce_removed")
@Access(AccessType.FIELD)
public class MessageAnnouncementRemoved extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "message_announce_id", unique = true, nullable = false, length = 32)
    private String messageAnnounceId;

    @ToTrim
    @Column(name = "announcement_id", nullable = false, length = 32)
    private String announcementId;

    @ToTrim
    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", nullable = false, length = 20)
    private String status;

    public MessageAnnouncementRemoved() {
    }

    public String getMessageAnnounceId() {
        return messageAnnounceId;
    }

    public void setMessageAnnounceId(String messageAnnounceId) {
        this.messageAnnounceId = messageAnnounceId;
    }

    public String getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(String announcementId) {
        this.announcementId = announcementId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
