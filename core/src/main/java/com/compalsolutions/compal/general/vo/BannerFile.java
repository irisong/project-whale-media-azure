package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.File;
import java.util.Objects;

@Entity
@Table(name = "app_banner_file")
@Access(AccessType.FIELD)
public class BannerFile extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String LANG_EN = "EN";
    public static final String LANG_CN = "CN";
    public static final String LANG_MS = "MS";
    public static final String NON_SPECIFIED = "NONE";
    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "banner_file_id", unique = true, nullable = false, length = 32)
    private String bannerFileId;

    @Column(name = "banner_id", nullable = false, length = 32)
    private String bannerId;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data", columnDefinition = Global.ColumnDef.LONG_BLOB)
    private byte[] data;

    @Column(name = "file_size", nullable = true)
    private Long fileSize;

    @Column(name = "path", columnDefinition = "text")
    private String path;

    @Column(name = "sort_order", columnDefinition = "int default 0")
    private Integer sortOrder;

    @ToTrim
    @Column(name = "language", length = 50, nullable = false)
    private String language;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    @Transient
    private String tempId;

    @Transient
    private File fileUpload;

    public BannerFile() {
    }


    public String getBannerFileId() {
        return bannerFileId;
    }

    public void setBannerFileId(String questionFileId) {
        this.bannerFileId = bannerFileId;
    }

    public String getbannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(bannerFileId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = bannerFileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setFileUrlWithParentPath(String parentPath) {
        fileUrl = parentPath + "/banner/" + getRenamedFilename();
    }
    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        BannerFile that = (BannerFile) o;

        return Objects.equals(bannerFileId, that.bannerFileId);

    }

    @Override
    public int hashCode() {
        return Objects.hash(bannerFileId);
    }
}
