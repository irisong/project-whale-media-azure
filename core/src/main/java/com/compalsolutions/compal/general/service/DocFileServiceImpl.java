package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.DocFileDao;
import com.compalsolutions.compal.general.vo.DocFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@Component(DocFileService.BEAN_NAME)
public class DocFileServiceImpl implements DocFileService {
    @Autowired
    private DocFileDao docFileDao;

    @Override
    public void saveDocFile(Locale locale, DocFile docFile) {
        docFileDao.save(docFile);
    }

    @Override
    public void findDocFilesForListing(DatagridModel<DocFile> datagridModel, String languageCode, List<String> userGroups, String status) {
        docFileDao.findDocFilesForDatagrid(datagridModel, languageCode, userGroups, status);
    }

    @Override
    public DocFile getDocFile(String docId) {
        return docFileDao.get(docId);
    }

    @Override
    public void updateDocFile(Locale locale, DocFile docFile) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        DocFile dbDocFile = docFileDao.get(docFile.getDocId());
        if (dbDocFile == null)
            throw new DataException(i18n.getText("invalidDocument", locale));

        dbDocFile.setTitle(docFile.getTitle());
        dbDocFile.setLanguageCode(docFile.getLanguageCode());
        if (docFile.getData() != null) {
            dbDocFile.setFilename(docFile.getFilename());
            dbDocFile.setContentType(docFile.getContentType());
            dbDocFile.setData(docFile.getData());
            dbDocFile.setFileSize(docFile.getFileSize());
        }
        dbDocFile.setUserGroups(docFile.getUserGroups());
        dbDocFile.setStatus(docFile.getStatus());
        docFileDao.update(dbDocFile);
    }

    @Override
    public List<DocFile> findDocFilesForDashboard(String userGroup) {
        return docFileDao.findDocFilesForDashboard(userGroup);
    }
}
