package com.compalsolutions.compal.general.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.compalsolutions.compal.Global;
import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "app_system_config")
@Access(AccessType.FIELD)
public class SystemConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "config_id", unique = true, nullable = false, length = 32)
    private String configId;

    @Column(name = "member_register", nullable = false)
    private Boolean memberRegister;

    @Column(name = "member_login", nullable = false)
    private Boolean memberLogin;

    @ToTrim
    @Column(name = "application_name", length = 250, nullable = false)
    private String applicationName;

    @ToTrim
    @Column(name = "company_name", length = 250, nullable = false)
    private String companyName;

    @ToTrim
    @Column(name = "support_email", length = 250, nullable = false)
    private String supportEmail;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", nullable = false, length = 10)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_account_no", length = 100)
    private String bankAccountNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_account_name", length = 100)
    private String bankAccountName;

    @ToUpperCase
    @ToTrim
    @Column(name = "bank_name", length = 100)
    private String bankName;

    @ToTrim
    @Column(name = "qr_code_expiry")
    private Integer qrCodeExpiry;

    @Column(name = "system_down")
    private Boolean systemDown;

    @Column(name = "notif_offline")
    private Boolean notifOffline;

//    @ToTrim
//    @Column(name = "gift_batch_version")
//    private Integer giftBatchVersion;

    @ToUpperCase
    @ToTrim
    @Column(name = "special_member", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String specialMember;

    @ToUpperCase
    @ToTrim
    @Column(name = "merchant_bank", length = 100)
    private String merchantBank;

    @ToUpperCase
    @ToTrim
    @Column(name = "merchant_account_name", length = 100)
    private String merchantAccountName;

    @ToUpperCase
    @ToTrim
    @Column(name = "merchant_account_no", length = 100)
    private String merchantAccountNo;

    public SystemConfig() {
    }

    public SystemConfig(boolean defaultValue) {
        if (defaultValue) {
            memberRegister = true;
            memberLogin = true;
            countryCode = "IG"; // INTERNATIONAL
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        SystemConfig that = (SystemConfig) o;

        return configId != null ? configId.equals(that.configId) : that.configId == null;
    }

    @Override
    public int hashCode() {
        return configId != null ? configId.hashCode() : 0;
    }

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId;
    }

    public Boolean getMemberRegister() {
        return memberRegister;
    }

    public void setMemberRegister(Boolean memberRegister) {
        this.memberRegister = memberRegister;
    }

    public Boolean getMemberLogin() {
        return memberLogin;
    }

    public void setMemberLogin(Boolean memberLogin) {
        this.memberLogin = memberLogin;
    }

    public String getApplicationName() {
        return applicationName;
    }

    public void setApplicationName(String applicationName) {
        this.applicationName = applicationName;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getSupportEmail() {
        return supportEmail;
    }

    public void setSupportEmail(String supportEmail) {
        this.supportEmail = supportEmail;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getBankAccountNo() {
        return bankAccountNo;
    }

    public void setBankAccountNo(String bankAccountNo) {
        this.bankAccountNo = bankAccountNo;
    }

    public String getBankAccountName() {
        return bankAccountName;
    }

    public void setBankAccountName(String bankAccountName) {
        this.bankAccountName = bankAccountName;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Integer getQrCodeExpiry() {
        return qrCodeExpiry;
    }

    public void setQrCodeExpiry(Integer qrCodeExpiry) {
        this.qrCodeExpiry = qrCodeExpiry;
    }

//    public Integer getGiftBatchVersion() {
//        return giftBatchVersion;
//    }
//
//    public void setGiftBatchVersion(Integer giftBatchVersion) {
//        this.giftBatchVersion = giftBatchVersion;
//    }

    public Boolean getSystemDown() {
        return systemDown;
    }

    public void setSystemDown(Boolean systemDown) {
        this.systemDown = systemDown;
    }

    public Boolean getNotifOffline() {
        return notifOffline;
    }

    public void setNotifOffline(Boolean notifOffline) {
        this.notifOffline = notifOffline;
    }

    public String getSpecialMember() {
        return specialMember;
    }

    public void setSpecialMember(String specialMember) {
        this.specialMember = specialMember;
    }

    public String getMerchantBank() {
        return merchantBank;
    }

    public void setMerchantBank(String merchantBank) {
        this.merchantBank = merchantBank;
    }

    public String getMerchantAccountName() {
        return merchantAccountName;
    }

    public void setMerchantAccountName(String merchantAccountName) {
        this.merchantAccountName = merchantAccountName;
    }

    public String getMerchantAccountNo() {
        return merchantAccountNo;
    }

    public void setMerchantAccountNo(String merchantAccountNo) {
        this.merchantAccountNo = merchantAccountNo;
    }
}