package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.FreqAskQues;

import java.util.List;

public interface FreqAskQuesDao extends BasicDao<FreqAskQues, String> {
    public static final String BEAN_NAME = "freqAskQuesDao";

    public void findFreqAskListForDatagrid(DatagridModel<FreqAskQues> datagridModel, String status);

    public List<FreqAskQues> findAllFreqAskQues(String status);








//    public List<FreqAskQues> findAllFreqAskQuesByType(String type);
//
//    public List<FreqAskQues> findAllActiveFreqAskQuesByAccessModule(String accessModule);
//
//
//    public FreqAskQues findFreqAskQuesByTitle(String title, String accessModule);
}
