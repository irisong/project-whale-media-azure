package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;

public interface HelpDeskReplyFileDao extends BasicDao<HelpDeskReplyFile, String> {
    public static final String BEAN_NAME = "helpDeskReplyFileDao";
}
