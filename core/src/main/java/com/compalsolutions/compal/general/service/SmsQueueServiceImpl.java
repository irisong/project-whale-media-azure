package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.dao.SmsQueueDao;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.sms.NexmoConnector;
import com.compalsolutions.compal.sms.service.SmsService;
import com.compalsolutions.compal.util.ExceptionUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Component(SmsQueueService.BEAN_NAME)
public class SmsQueueServiceImpl implements SmsQueueService {

    private static final Log log = LogFactory.getLog(SmsQueueServiceImpl.class);

    @Autowired
    private SmsQueueDao smsQueueDao;

    @Autowired
    private SmsService smsService;

    private static String asciiToHex(String asciiValue) {
        char[] chars = asciiValue.toCharArray();
        StringBuffer hex = new StringBuffer();
        for (int i = 0; i < chars.length; i++) {
            hex.append(String.format("%04x", (int) chars[i]));
        }
        return hex.toString();
    }

    @Override
    public void doSentSms() {
        log.debug("Checking SMS");
        SmsQueue smsQueue = smsService.getFirstNotProcessSms();

        while (smsQueue != null) {
            // if (StringUtils.isNotBlank(smsQueue.getAgentId())) {

            // Agent agent = agentDao.get(smsQueue.getAgentId());

            if (StringUtils.startsWith(smsQueue.getSmsTo(), "86")) {
                String url = "http://www.smsadmin.cn/smsmarketing/wwwroot/interface/postSendSms/";
                String uid = "albert";
                String pwd = stringMd5("albert888");
                String mobile = "";

                log.debug("China SMS API:" + url);
                log.debug("SmsTo : " + smsQueue.getSmsTo());
                mobile = StringUtils.replaceOnce(smsQueue.getSmsTo(), "86", "");

                String msg = smsQueue.getBody();

                log.debug("Mobile No:" + mobile);
                log.debug("Message:" + msg);

                HttpPost httpPost = new HttpPost(url);
                List<NameValuePair> nvps = new ArrayList<NameValuePair>();
                nvps.add(new BasicNameValuePair("uid", uid));
                nvps.add(new BasicNameValuePair("pwd", pwd));
                nvps.add(new BasicNameValuePair("mobile", mobile));
                nvps.add(new BasicNameValuePair("msg", msg));

                try {
                    httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
                    HttpClient httpclient = new DefaultHttpClient();
                    HttpResponse response = httpclient.execute(httpPost);

                    System.out.println(response.getStatusLine());

                    Header[] headers = response.getAllHeaders();
                    for (int i = 0; i < headers.length; i++)
                        System.out.println(headers[i].getName() + ":" + headers[i].getValue());
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        // System.out.println(EntityUtils.toString(entity, "UTF-8"));
                        String result = EntityUtils.toString(entity, "UTF-8");
                        log.debug("Results:" + result);
                        smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                        smsQueue.setErrMessage(result);
                        smsQueue.setGateway(url);
                        smsService.updateSmsQueue(smsQueue);
                    }
                } catch (UnknownHostException ex) {
                    ex.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                    log.debug("fail ");
                    smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                    smsQueue.setErrMessage("Fail");
                    smsQueue.setGateway(url);
                    smsService.updateSmsQueue(smsQueue);
                }
            } else if (StringUtils.startsWith(smsQueue.getSmsTo(), "82") // South Korea
                    || StringUtils.startsWith(smsQueue.getSmsTo(), "62") // Indonesia
                    || StringUtils.startsWith(smsQueue.getSmsTo(), "886") // Taiwan
                    || StringUtils.startsWith(smsQueue.getSmsTo(), "81") // Japan
                    || StringUtils.startsWith(smsQueue.getSmsTo(), "1")) { // US
                String url = "https://rest.nexmo.com/sms";
                try {
                    String mobile = smsQueue.getSmsTo();
                    String msg = smsQueue.getBody();

                    NexmoConnector nexmoConnector = new NexmoConnector();
                    nexmoConnector.sendSms(mobile,msg);

                    smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                    smsQueue.setGateway(url);

                    smsService.updateSmsQueue(smsQueue);

                } catch (Exception ex) {
                    ex.printStackTrace();
                    smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                    smsQueue.setErrMessage("Fail");
                    smsQueue.setGateway(url);
                    smsService.updateSmsQueue(smsQueue);
                }
            } else {
                // For Malaysia and other countries
                try {
                    String smsTo = smsQueue.getSmsTo();

                    log.debug("Mocean SMS TO:" + smsTo);

                    String data ="";


                    if(StringUtils.startsWith(smsQueue.getSmsTo(), "84")){
                        // For Malaysia & other countries
                        // Construct Data
                        data = URLEncoder.encode("mocean-username", "UTF-8") + "=" + URLEncoder.encode("aio2http", "UTF-8");
                        data += "&" + URLEncoder.encode("mocean-password", "UTF-8") + "=" + URLEncoder.encode("96@2gP", "UTF-8");
                    }else{
                        // For Malaysia & other countries
                        // Construct Data
                        data = URLEncoder.encode("mocean-username", "UTF-8") + "=" + URLEncoder.encode("aiohttp", "UTF-8");
                        data += "&" + URLEncoder.encode("mocean-password", "UTF-8") + "=" + URLEncoder.encode("a0hpt4", "UTF-8");
                    }

                    data += "&" + URLEncoder.encode("mocean-to", "UTF-8") + "=" + URLEncoder.encode(smsTo, "UTF-8");
                    data += "&" + URLEncoder.encode("mocean-from", "UTF-8") + "=" + URLEncoder.encode("OMC", "UTF-8");
                    String msg = smsQueue.getBody();
                    data += "&" + URLEncoder.encode("mocean-text", "UTF-8") + "=" + URLEncoder.encode(msg, "UTF-8");

                    // Send Data
                    URL url = new URL("http://183.81.161.84:13016/cgi-bin/sendsms");

                    URLConnection conn = url.openConnection();

                    conn.setDoOutput(true);
                    OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                    wr.write(data);
                    wr.flush();

                    // Get the response
                    BufferedReader resp = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                    // Display Output
                    String output = resp.readLine();
                    log.debug("Output:" + output);
                    resp.close();

                    smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                    smsQueue.setErrMessage(output);
                    smsQueue.setGateway(url.toString());

                    log.debug("Sms Gateways : " + url.toString());

                    smsService.updateSmsQueue(smsQueue);

                } catch (Exception ex) {
                    ex.printStackTrace();
                    smsQueue.setStatus(SmsQueue.SMS_STATUS_SENT);
                    smsQueue.setErrMessage(ExceptionUtil.getExceptionStacktrace(ex));
                    smsService.updateSmsQueue(smsQueue);
                }
            }
            // }
            smsQueue = smsService.getFirstNotProcessSms();
        }
        // }
        // }
    }

    public static String stringMd5(String input) {
        try {
            // 拿到一个MD5转换器（如果想要SHA1加密参数换成"SHA1"）
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            // 输入的字符串转换成字节数组
            byte[] inputByteArray = input.getBytes();
            // inputByteArray是输入字符串转换得到的字节数组
            messageDigest.update(inputByteArray);
            // 转换并返回结果，也是字节数组，包含16个元素
            byte[] resultByteArray = messageDigest.digest();
            // 字符数组转换成字符串返回
            return byteArrayToHex(resultByteArray);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    public static String byteArrayToHex(byte[] byteArray) {
        // 首先初始化一个字符数组，用来存放每个16进制字符
        char[] hexDigits = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
        // new一个字符数组，这个就是用来组成结果字符串的（解释一下：一个byte是八位二进制，也就是2位十六进制字符）
        char[] resultCharArray = new char[byteArray.length * 2];
        // 遍历字节数组，通过位运算（位运算效率高），转换成字符放到字符数组中去
        int index = 0;
        for (byte b : byteArray) {
            resultCharArray[index++] = hexDigits[b >>> 4 & 0xf];
            resultCharArray[index++] = hexDigits[b & 0xf];
        }

        // 字符数组组合成字符串返回
        return new String(resultCharArray);
    }

    @Override
    public void saveSmsQueue(Locale locale, SmsQueue smsQueue) {
        smsQueueDao.save(smsQueue);
    }

    @Override
    public void findSmsqForListing(DatagridModel<SmsQueue> datagridModel, String smsTo, String status) {
        smsQueueDao.findSmsqForDatagrid(datagridModel, smsTo, status);
    }
}
