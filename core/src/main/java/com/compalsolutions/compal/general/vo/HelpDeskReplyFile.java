package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "app_help_desk_reply_file")
@Access(AccessType.FIELD)
public class HelpDeskReplyFile extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "reply_file_id", unique = true, nullable = false, length = 32)
    private String helpDeskReplyFileId;

    @Column(name = "reply_id", nullable = false, length = 32)
    private String replyId;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "sort_order", columnDefinition = Global.ColumnDef.DEFAULT_0)
    private Integer sortOrder;

    @ManyToOne
    @JoinColumn(name = "reply_id", insertable = false, updatable = false)
    private HelpDeskReply helpDeskReply;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    @Transient
    private String tempId;

    @Transient
    private byte[] data;

    public HelpDeskReplyFile() {
    }

    public HelpDeskReplyFile(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public HelpDeskReplyFile(byte[] data, String contentType, String filename, Long fileSize, int sortOrder) {
        this.data = data;
        this.contentType = contentType;
        this.filename = filename;
        this.fileSize = fileSize;
        this.sortOrder = sortOrder;
    }

    public String getHelpDeskReplyFileId() {
        return helpDeskReplyFileId;
    }

    public void setHelpDeskReplyFileId(String helpDeskReplyFileId) {
        this.helpDeskReplyFileId = helpDeskReplyFileId;
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Integer getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

    public HelpDeskReply getHelpDeskReply() {
        return helpDeskReply;
    }

    public void setHelpDeskReply(HelpDeskReply helpDeskReply) {
        this.helpDeskReply = helpDeskReply;
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(helpDeskReplyFileId) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = helpDeskReplyFileId + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        HelpDeskReplyFile that = (HelpDeskReplyFile) o;

        return Objects.equals(helpDeskReplyFileId, that.helpDeskReplyFileId);

    }

    @Override
    public int hashCode() {
        return Objects.hash(helpDeskReplyFileId);
    }
}
