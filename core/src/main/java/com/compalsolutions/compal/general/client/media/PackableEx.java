package com.compalsolutions.compal.general.client.media;

public interface PackableEx extends Packable {
    void unmarshal(ByteBuf in);
}
