package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Announcement;

import java.util.Date;
import java.util.List;

public interface AnnouncementDao extends BasicDao<Announcement, String> {
    public static final String BEAN_NAME = "announcementDao";

    public void findAnnouncementsForDatagrid(DatagridModel<Announcement> datagridModel, List<String> userGroups, String status, String announcementType,
                                             Date dateFrom, Date dateTo);

    public int getTotalRecordsFor(List<String> userGroups);

    public List<Announcement> findAnnouncementsForDashboard(String userGroup, String languageCode, String accessModule, int pageNo, int pageSize);

    public List<Announcement> findPendingAnnouncementToSend(Date untilDate);
}
