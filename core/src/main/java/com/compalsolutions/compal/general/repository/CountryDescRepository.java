package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.CountryDesc;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryDescRepository extends JpaRepository<CountryDesc, String> {
    public static final String BEAN_NAME = "countryDescRepository";
}
