package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.FreqAskQues;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;

import java.util.List;
import java.util.Locale;

public interface FreqAskQuesService {
    public static final String BEAN_NAME = "freqAskQuesService";

    public void findFreqAskListForListing(DatagridModel<FreqAskQues> datagridModel, String status);

    public void setFaqFileUrl(FreqAskQues freqAskQues);

    public void doSaveNewFreqAskQues(Locale locale, FreqAskQues freqAskQues, List<FreqAskQuesFile> freqAskQuesFiles);

    public void doSaveFaqFile(String uploadPath, String questionId, FreqAskQuesFile freqAskQuesFile);

    public void doDeleteFaqFile(String deleteFilepath, FreqAskQuesFile freqAskQuesFiles_db);

    public FreqAskQuesFile getHelpFreqAskQuesFile(String freqAskQuesFileId);

    public FreqAskQues findFreqAskQuesByQuestionId(String questionId);

    public void doUpdateFreqAskQues(Locale locale, FreqAskQues freqAskQues, List<FreqAskQuesFile> freqAskQuesFiles);

    public FreqAskQuesFile findFreqAskQuesFileByQuestionIdAndLanguage(String questionId, String language);

    public List<FreqAskQues> findAllFreqAskQues(String status);
}
