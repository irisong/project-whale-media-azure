package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.repository.SystemConfigImageRepository;
import com.compalsolutions.compal.general.vo.SystemConfigImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(SystemConfigImageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SystemConfigImageDaoImpl extends Jpa2Dao<SystemConfigImage, String> implements SystemConfigImageDao {
    @SuppressWarnings("unused")
    @Autowired
    private SystemConfigImageRepository systemConfigImageRepository;

    public SystemConfigImageDaoImpl() {
        super(new SystemConfigImage(false));
    }

    @Override
    public SystemConfigImage findSystemConfigImageByType(String imageType) {
        SystemConfigImage systemConfigImage = new SystemConfigImage(false);
        systemConfigImage.setImageType(imageType);
        return findUnique(systemConfigImage);
    }

    @Override
    public List<SystemConfigImage> findSystemConfigImages() {
        SystemConfigImage systemConfigImage = new SystemConfigImage(false);
        return findByExample(systemConfigImage, new String[] {});
    }
}
