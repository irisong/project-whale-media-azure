package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.repository.CountryRepository;
import com.compalsolutions.compal.general.vo.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(CountryDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CountryDaoImpl extends Jpa2Dao<Country, String> implements CountryDao {
    @SuppressWarnings("unused")
    @Autowired
    private CountryRepository countryRepository;

    public CountryDaoImpl() {
        super(new Country(false));
    }

    @Override
    public List<Country> findAllCountriesForRegistration() {
        Country example = new Country(false);
        example.setShowRegister(true);
        return findByExample(example, "countryName");
    }

    @Override
    public Country findCountryByCountryNameToUpper(String countryName) {
        String hql = "FROM ct IN " + Country.class + " WHERE upper(ct.countryName)=? ";

        List<Country> results = findQueryAsList(hql, countryName);
        if (results.isEmpty())
            return null;
        else
            return results.get(0);
    }
}
