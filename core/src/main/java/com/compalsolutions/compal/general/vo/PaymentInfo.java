package com.compalsolutions.compal.general.vo;

public interface PaymentInfo {
    public String getPaymentMethod();

    public Integer getWalletType();

    public String getCrytocurrencyType();
}
