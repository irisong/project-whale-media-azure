package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.repository.SystemConfigRepository;
import com.compalsolutions.compal.general.vo.SystemConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(SystemConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class SystemConfigDaoImpl extends Jpa2Dao<SystemConfig, String> implements SystemConfigDao {
    @SuppressWarnings("unused")
    @Autowired
    private SystemConfigRepository systemConfigRepository;

    public SystemConfigDaoImpl() {
        super(new SystemConfig(false));
    }

    @Override
    public SystemConfig getDefault() {
        return findUnique(new SystemConfig(false));
    }

    @Override
    public SystemConfig findSystemConfigById(String configId) {
        SystemConfig systemConfig = new SystemConfig(false);
        systemConfig.setConfigId(configId);
        return findUnique(systemConfig);
    }
}
