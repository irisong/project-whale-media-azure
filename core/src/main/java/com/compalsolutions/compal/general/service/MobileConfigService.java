package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.vo.MobileForceUpdate;

public interface MobileConfigService {
    public static final String BEAN_NAME = "mobileConfigService";

    public MobileForceUpdate findMobileForceUpdateByPlatformAndFavor(String platform, String favor);

    public MobileForceUpdate doUpdateMobileForceUpdate(MobileForceUpdate mobileForceUpdate);
}
