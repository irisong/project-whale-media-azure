package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(HelpDeskTypeDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpDeskTypeDaoImpl extends Jpa2Dao<HelpDeskType, String> implements HelpDeskTypeDao {

    public HelpDeskTypeDaoImpl() {
        super(new HelpDeskType(false));
    }

    @Override
    public void findHelpDeskTypeForDatagrid(DatagridModel<HelpDeskType> datagridModel, String status) {
        List<Object> params = new ArrayList<>();
        String hql = "FROM h IN " + HelpDeskType.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(status)) {
            hql += " AND h.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "h", hql, params.toArray());
    }

    @Override
    public List<HelpDeskType> findHelpDeskTypes(String status) {
        HelpDeskType example = new HelpDeskType(false);

        if (StringUtils.isNotBlank(status))
            example.setStatus(status);

        return findByExample(example, "sortOrder");
    }
}
