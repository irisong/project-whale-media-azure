package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.function.system.dao.SysParamDao;
import com.compalsolutions.compal.function.system.dao.SysParamSqlDao;
import com.compalsolutions.compal.function.system.vo.SysParam;
import com.compalsolutions.compal.function.system.vo.SysParamKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;

@Component(DocNoService.BEAN_NAME)
public class DocNoServiceImpl implements DocNoService {
    @Autowired
    private SysParamDao sysParamDao;

    @Autowired
    private SysParamSqlDao sysParamSqlDao;

    // constant variable to ensure no duplication on keys
    private static final String HELP_001 = "HELP-001"; // help desk (enquiry)
    private static final String SALES_001 = "SALES-001"; // sales order
    private static final String SALES_002 = "SALES-002"; // delivery order
    private static final String SALES_003 = "SALES-003"; // vip rank order
    private static final String SALES_004 = "SALES-004"; // vip rank order
    private static final String TOPUP_001 = "TOPUP-001"; // topup whale live coin
    private static final String TRADE_001 = "SALES-TRADE_001"; // trade order
    private static final String WALLET_001 = "WALLET-001"; // wallet adjustment
    private static final String WALLET_002 = "WALLET-002"; // wallet transfer
    private static final String WALLET_003 = "WALLET-003"; // wallet withdraw
    private static final String WALLET_004 = "WALLET-004"; // wallet topup
    private static final String WALLET_005 = "WALLET-005"; // wallet swap
    private static final String WALLET_006 = "WALLET-006"; // wallet exchange from crypto to usd
    private static final String WALLET_007 = "WALLET-007"; // wallet exchange from usd to crypto
    private static final String WALLET_008 = "WALLET-008"; // omc wallet withdraw
    private static final String WALLET_009 = "WALLET-009"; // Bunus wallet adjustment
    private static final String WALLET_010 = "WALLET-010"; // wallet exchange from user x to merchant x with x to x currency
    private static final String WALLET_011 = "WALLET-011"; // Fixed deposit
    private static final String WALLET_012 = "WALLET-012"; // Wallet payment (GPT rebate)
    private static final String WALLET_013 = "WALLET-013"; // wallet exchange from/to external web (OMC)
    private static final String INTEREST_ACCOUNT_001 = "INTEREST-ACCOUNT-001"; // interest account deposit
    private static final String INTEREST_ACCOUNT_002 = "INTEREST-ACCOUNT-002"; // interest account withdraw
    private static final String INTEREST_ACCOUNT_003 = "INTEREST-ACCOUNT-003"; // interest account payout
    private static final String OMNIPAY_REBATE_001 = "OMNIPAY-001"; // Omnipay Rebate (Manual Insert)
    private static final String HUIFU_PROMOTION = "HUIFU-PROMOTION"; // Huifu Promotion
    private static final String OMCCOIN_001 = "OMCCOIN-001"; // explorer no of virtual machine
    private static final String OMCWALLET_BONUS_001 = "OMCWALLET_BONUS_001"; // explorer no of virtual machine
    private static final String OMCC_EQUITY_SHARE_SUBSCRIPTION_001 = "OMCC-EQUITY-SUB001"; // account no for omnicoin club equity share subscription
    private static final String EQUITY_PURCHASE_ANNIV_001 = "EQUITY-ANNIV-PUR001"; // Doc No for equity share purchase in anniversary promotion period
    private static final String AUDIENCE_001 = "AUDIENCE-001"; // watch live stream audience
    private static final String RAZER_PAY = "RP-WC"; // wallet exchange from/to external web (OMC)
    private static final String LUCKY_DRAW_PACKAGE_001 = "LUCKY-DRAW-PCKG-001"; // lucky draw package
    private static final String MERCHANT_ORDER_001 = "MERCHANT-ORDER-001"; // merchant order ref number
    private static final String MERCHANT_PAYMENT_001 = "MERCHANT-PAYMENT-001"; // merchant order payment ref number
//    private static final String MERCHANT_SHIPPING_001 = "MERCHANT-SHIPPING001"; // merchant order shipping ref number

    public String doGetNextHelpDeskTicketNo() {
        String desc = "Help Desk (Enquiry) ticket number. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = HELP_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("HD");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextSalesOrderDocNo() {
        String desc = "Sales Order document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = SALES_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("IN");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextDeliveryOrderDocNo() {
        String desc = "Delivery Order document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = SALES_002;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("D");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    public String doGetNextWalletAdjustDocNo() {
        String desc = "Wallet Adjustment document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WA");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    public String doGetNextWalletTransferDocNo() {
        String desc = "Wallet Transfer document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_002;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WT");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextFixDepositDocNo() {
        String desc = "Fixed deposit document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_011;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("FD");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    public String doGetNextWalletWithdrawDocNo() {
        String desc = "Wallet Withdraw document number. A human readable docno. Value=running_number";

        DecimalFormat df = new DecimalFormat("00000000");
        String key = WALLET_003;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue(null);
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletTopupDocNo() {
        String desc = "Wallet Top Up document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_004;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WT");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletSwapDocNo() {
        String desc = "Wallet Swap document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_005;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WS");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletExchangeDocNo() {
        String desc = "Wallet Swap document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_006;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WE");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletExchange2CryptoDocNo() {
        String desc = "Wallet Swap from USD to crypto document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_007;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WC");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletExchange2ExternalDocNo() {
        String desc = "Wallet Swap OMC from/to external web document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_013;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WX");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextTradeOrderNo() {
        String desc = "Trade Order document number. A human readable docno. Value=running_number";

        DecimalFormat df = new DecimalFormat("00000000");
        String key = TRADE_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue(null);
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletMerchantExchangeDocNo() {
        String desc = "Wallet Swap document number to merchant. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_010;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WME");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextInterestAccountDepositDocNo() {
        String desc = "Interest Account Deposit document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = INTEREST_ACCOUNT_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("IAD");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextInterestAccountWithdrawDocNo() {
        String desc = "Interest Account Withdraw document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = INTEREST_ACCOUNT_002;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("IAW");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextInterestAccountPayoutDocNo() {
        String desc = "Interest Account Payout document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = INTEREST_ACCOUNT_003;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("IAP");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextVipOrderDocNo() {
        String desc = "VIP Order document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = SALES_003;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("VIP");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNexPackageOrderDocNo() {
        String desc = "VIP Order document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = SALES_004;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("PACK");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextOmnipayRebateDocNo() {
        String desc = "Omnipay Rebate document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = OMNIPAY_REBATE_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("OMM");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public boolean doGetHuifuPromotionFlag() {
        String desc = "Huifu Promotion Flag";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = HUIFU_PROMOTION;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setBooleanValue(false);
            sysParamDao.save(sysParam);
        }

        return sysParam.getBooleanValue();
    }

    @Override
    public int doGetNoOfVirtualMinerBuffer() {
        String desc = "Explorer noOfVirtualMiner";
        String key = OMCCOIN_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue(null);
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        return sysParam.getNumberValue().intValue();
    }

    @Override
    public boolean doGetIsEnableOmcWalletMatchingBonus() {
        String desc = "OMCWallet Matching Bonus Flag";

        String key = OMCWALLET_BONUS_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setBooleanValue(false);
            sysParamDao.save(sysParam);
        }

        return sysParam.getBooleanValue();
    }

    @Override
    public String doGetNextOmcWalletWithdrawDocNo() {
        String desc = "OMC Wallet Withdraw document number. A human readable docno. Value=running_number";

        DecimalFormat df = new DecimalFormat("00000000");
        String key = WALLET_008;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue(null);
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return df.format(sysParam.getNumberValue());
    }

    public String doGetNextBonusWalletAdjustDocNo() {
        String desc = "Bonus Wallet Adjustment document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_009;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("BWA");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextWalletPaymentDocNo() {
        String desc = "Wallet payment document number. A human readable docno. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = WALLET_012;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("WP");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextEquityShareSubscriptionDocNo() {
        String desc = "Equity Subscription Account No. A human readable Account No. Value=6 digit running_number";

        DecimalFormat df = new DecimalFormat("000000");
        String key = OMCC_EQUITY_SHARE_SUBSCRIPTION_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_NUMBER);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextAnnivEquitySharePurchaseDocNo() {
        String desc = "Equity Purchase In Anniversary Period No. A human readable Account No. Value=6 digit running_number";

        DecimalFormat df = new DecimalFormat("000000");
        String key = EQUITY_PURCHASE_ANNIV_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_NUMBER);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("ANNIV-PUR");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextTopUpRefId() {
        String desc = "Top up whale live coin no. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = TOPUP_001;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("TU");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextAudienceRefId() {
        DecimalFormat df = new DecimalFormat("0000000");
        String desc = "Watch Live Stream Audience. Value=Prefix+running_number";

        SysParam sysParam = doGetSysParam(AUDIENCE_001, desc, SysParam.VALUE_TYPE_MIX, "AUD");
        String sysParamCode = sysParam.getId().getSysParamCode();
        double numberValue = sysParam.getNumberValue() + 1;
       // sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamSqlDao.updateSysParamByParamCode(sysParamCode, numberValue);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    private SysParam doGetSysParam(String key, String desc, String valueType, String stringValue) {
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(valueType);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue(stringValue);
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

        return sysParam;
    }

    @Override
    public String doGetNextOrderId() {
        String desc = "Top up whale live coin no. Value=Prefix+running_number";

        DecimalFormat df = new DecimalFormat("0000000");
        String key = RAZER_PAY;
        SysParam sysParam = sysParamDao.get(new SysParamKey(key, Global.DEFAULT_COMPANY));
        if (sysParam == null) {
            sysParam = new SysParam(new SysParamKey(key, Global.DEFAULT_COMPANY));
            sysParam.setSysParamDesc(desc);
            sysParam.setValueType(SysParam.VALUE_TYPE_MIX);
            sysParam.setStatus(Global.Status.ACTIVE);
            sysParam.setStringValue("RP");
            sysParam.setNumberValue(0d);
            sysParamDao.save(sysParam);
        }

//        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
//        sysParamDao.update(sysParam);
//
//        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());

        String sysParamCode = sysParam.getId().getSysParamCode();
        double numberValue = sysParam.getNumberValue() + 1;
        // sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamSqlDao.updateSysParamByParamCode(sysParamCode, numberValue);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextLuckyDrawPackageNo() {
        DecimalFormat df = new DecimalFormat("0000000");
        String desc = "Lucky draw package number. Value=Prefix+running_number";

        SysParam sysParam = doGetSysParam(LUCKY_DRAW_PACKAGE_001, desc, SysParam.VALUE_TYPE_MIX, "LDP");
        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextMerchantOrderRefNo() {
        DecimalFormat df = new DecimalFormat("0000000");
        String desc = "Merchant order referral number. Value=Prefix+running_number";

        SysParam sysParam = doGetSysParam(MERCHANT_ORDER_001, desc, SysParam.VALUE_TYPE_MIX, "ORD");
        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

    @Override
    public String doGetNextMerchantPaymentRefNo() {
        DecimalFormat df = new DecimalFormat("0000000");
        String desc = "Merchant order payment referral number. Value=Prefix+running_number";

        SysParam sysParam = doGetSysParam(MERCHANT_PAYMENT_001, desc, SysParam.VALUE_TYPE_MIX, "PYM");
        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
        sysParamDao.update(sysParam);

        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
    }

//    @Override
//    public String doGetNextMerchantShippingRefNo() {
//        DecimalFormat df = new DecimalFormat("0000000");
//        String desc = "Merchant order shipping referral number. Value=Prefix+running_number";
//
//        SysParam sysParam = doGetSysParam(MERCHANT_SHIPPING_001, desc, SysParam.VALUE_TYPE_MIX, "SHP");
//        sysParam.setNumberValue(sysParam.getNumberValue() + 1);
//        sysParamDao.update(sysParam);
//
//        return sysParam.getStringValue() + df.format(sysParam.getNumberValue());
//    }
}
