package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.Banner;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BannerRepository extends JpaRepository<Banner, String> {
    public static final String BEAN_NAME = "bannerRepository";
}
