package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.AgoraMessageLog;

import java.util.List;

public interface AgoraMessageLogDao extends BasicDao<AgoraMessageLog, String> {
    public static final String BEAN_NAME = "agoraMessageLogDao";

    public List<AgoraMessageLog> findAgoraMessageByNoticeId(String noticeId);
}
