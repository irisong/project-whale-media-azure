package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.Area;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(AreaDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AreaDaoImpl extends Jpa2Dao<Area, String> implements AreaDao {

    public AreaDaoImpl() {
        super(new Area(false));
    }


    @Override
    public List<Area> getAreaListByCityCode(String cityCode) {
        Area example = new Area(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setCityCode(cityCode);
        return findByExample(example, "cityCode");
    }

    @Override
    public Area findAreaByAreaName(String areaName) {
        Area example = new Area(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setAreaName(areaName);
        return findFirst(example);
    }
}
