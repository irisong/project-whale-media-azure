package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.Announcement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementRepository extends JpaRepository<Announcement, String> {
    public static final String BEAN_NAME = "announcementRepository";
}
