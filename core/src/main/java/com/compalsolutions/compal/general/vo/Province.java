package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import javax.persistence.*;

@Entity
@Table(name = "app_province")
@Access(AccessType.FIELD)
public class Province extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @ToUpperCase
    @ToTrim
    @Column(name = "province_code", unique = true, nullable = false, length = 10)
    private String provinceCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", nullable = false, length = 10)
    private String countryCode;

    @ToUpperCase
    @ToTrim
    @Column(name = "province_name", nullable = false, length = 100)
    private String provinceName;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToUpperCase
    @ToTrim
    @Column(name = "category", length = 20)
    private String category;

    public Province() {
    }

    public Province(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public int hashCode() {
        return countryCode != null ? countryCode.hashCode() : 0;
    }
}
