package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.AnnouncementDao;
import com.compalsolutions.compal.general.dao.MessageDao;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.notification.NotificationConfProvider;
import com.compalsolutions.compal.notification.client.dto.Payload;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(AnnouncementService.BEAN_NAME)
public class AnnouncementServiceImpl implements AnnouncementService {
    private Log log = LogFactory.getLog(AnnouncementServiceImpl.class);

    @Autowired
    private AnnouncementDao announcementDao;
    @Autowired
    private MessageDao messageDao;

    @Override
    public void findAnnouncementsForListing(DatagridModel<Announcement> datagridModel, List<String> userGroups, String status, String announcementType,
                                            Date dateFrom, Date dateTo) {
        announcementDao.findAnnouncementsForDatagrid(datagridModel, userGroups, status, announcementType, dateFrom, dateTo);
    }

    @Override
    public void updateAnnouncement(Locale locale, Announcement announcement) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        Announcement dbAnnouncement = announcementDao.get(announcement.getAnnounceId());
        if (dbAnnouncement == null)
            throw new DataException(i18n.getText("invalidAnnouncement", locale));

        dbAnnouncement.setTitle(announcement.getTitle());
        dbAnnouncement.setTitleCn(announcement.getTitleCn());
        dbAnnouncement.setUserGroups(announcement.getUserGroups());
        dbAnnouncement.setPublishDate(announcement.getPublishDate());
        dbAnnouncement.setStatus(announcement.getStatus());
        dbAnnouncement.setBody(announcement.getBody());
        dbAnnouncement.setBodyCn(announcement.getBodyCn());
        dbAnnouncement.setRedirectUrl(announcement.getRedirectUrl());
        announcementDao.update(dbAnnouncement);
    }

    @Override
    public int getAnnouncementTotalRecordsFor(List<String> userGroups) {
        return announcementDao.getTotalRecordsFor(userGroups);
    }

    @Override
    public List<Announcement> findAnnouncementsForDashboard(String userGroup, int pageNo, int pageSize) {
        return findAnnouncementsForDashboard(userGroup, null, null, pageNo, pageSize);
    }

    @Override
    public List<Announcement> findAnnouncementsForDashboard(String userGroup, String languageCode, String accessModule,
                                                            int pageNo, int pageSize) {
        return announcementDao.findAnnouncementsForDashboard(userGroup, languageCode, accessModule, pageNo, pageSize);
    }

    @Override
    public void saveAnnouncement(Locale locale, Announcement announcement) {
        announcementDao.save(announcement);
    }

    @Override
    public Announcement getAnnouncement(String announceId) {
        return announcementDao.get(announceId);
    }

    @Override
    public List<Announcement> findPendingAnnouncementToSend(Date untilDate) {
        return announcementDao.findPendingAnnouncementToSend(untilDate);
    }

    @Override
    public void doProcessAnnouncementNotification(Announcement announcement) {
        MessageService messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
        NotificationService notificationService = Application.lookupBean(NotificationService.BEAN_NAME, NotificationService.class);
        NotificationConfProvider notificationConfProvider = Application.lookupBean(NotificationConfProvider.BEAN_NAME, NotificationConfProvider.class);
        Environment env = Application.lookupBean(Environment.class);

        // create message for app display
        messageService.doGenerateAnnouncementMessage(announcement);

        Message message = messageDao.findMessageByTypeAndReferenceAndMemberId(Global.MessageType.ANNOUNCEMENT,
                announcement.getAnnounceId(), announcement.getMemberId());
        String serverUrl = env.getProperty("serverUrl");
        String redirectUrl = serverUrl + "/pub/webView.php?messageId=" + message.getMessageId();

        // send notification to yun xin for notification pop up
        Payload payload = new Payload();
        payload.setType(Payload.ANNOUNCEMENT);

        NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
        if (account != null) {
            payload.setRedirectUrl(redirectUrl + "&languageCode=en");
            List<String> acc_ids = notificationService.findAllRegisteredNotifAccMembers(false);
            log.debug("NOTIFY SIZE FOR EN : " + announcement.getMemberId());
            if (StringUtils.isNotBlank(announcement.getMemberId())) {
                acc_ids = new ArrayList<>();
                String acc_id = notificationService.findAllRegisteredNotifAccMembers(false, announcement.getMemberId());
                if (acc_id != null) {
                    acc_ids.add(acc_id);
                }
            }
            log.debug("NOTIFY SIZE FOR EN : " + acc_ids.size());

            notificationConfProvider.doSendMessageAll(account.getAccountId(), acc_ids, Global.AccessModule.WHALE_MEDIA, "", announcement.getTitle(), Global.Language.ENGLISH, payload);

            if (announcement.getTitleCn() != null) {
                payload.setRedirectUrl(redirectUrl + "&languageCode=zh");
                acc_ids = notificationService.findAllRegisteredNotifAccMembers(true);
                if (StringUtils.isNotBlank(announcement.getMemberId())) {
                    acc_ids = new ArrayList<>();
                    String acc_id = notificationService.findAllRegisteredNotifAccMembers(true, announcement.getMemberId());
                    if (acc_id != null) {
                        acc_ids.add(acc_id);
                    }
                }

                log.debug("NOTIFY SIZE FOR CN : " + acc_ids.size());
                notificationConfProvider.doSendMessageAll(account.getAccountId(), acc_ids, Global.AccessModule.WHALE_MEDIA, "", announcement.getTitleCn(), Global.Language.CHINESE, payload);
                acc_ids.clear();
            }
        }

    }

    @Override
    public Announcement doSaveAnnouncement(String title, String titleCn, String body, String bodyCn, String status, Date publishDate, String userGroup, String announcementType) {
        Announcement announcement = new Announcement(true);
        announcement.setTitle(title);
        announcement.setTitleCn(titleCn);
        announcement.setBody(body);
        announcement.setBodyCn(bodyCn);
        announcement.setStatus(status);
        announcement.setPublishDate(publishDate);
        announcement.setUserGroups(userGroup);
        announcement.setAnnouncementType(announcementType);
        announcementDao.save(announcement);

        return announcement;
    }
}
