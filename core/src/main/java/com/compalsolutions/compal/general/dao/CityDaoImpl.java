package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.City;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(CityDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CityDaoImpl extends Jpa2Dao<City, String> implements CityDao {

    public CityDaoImpl() {
        super(new City(false));
    }

    @Override
    public List<City> getCityListByProvinceCode(String provinceCode) {
        City example = new City(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setProvinceCode(provinceCode);
        return findByExample(example, "provinceCode");
    }

    @Override
    public City findCityByCityName(String cityName) {
        City example = new City(false);
        example.setStatus(Global.Status.ACTIVE);
        example.setCityName(cityName);
        return findFirst(example);
    }
}
