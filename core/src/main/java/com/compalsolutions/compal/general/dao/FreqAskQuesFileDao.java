package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;

import java.util.List;

public interface FreqAskQuesFileDao extends BasicDao<FreqAskQuesFile, String> {
    public static final String BEAN_NAME = "freqAskQuesFileDao";

    public FreqAskQuesFile findFreqAskQuesFileByQuestionIdByLanguage(String questionId, String language);
}
