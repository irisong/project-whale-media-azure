package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.NoteRemark;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(NoteRemarkDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class NoteRemarkDaoImpl extends Jpa2Dao<NoteRemark, String> implements NoteRemarkDao {

    public NoteRemarkDaoImpl() {
        super(new NoteRemark());
    }


    @Override
    public List<NoteRemark> findMessageByMemberIdAndMessageRefId(String memberId, String messageRefId) {
        NoteRemark example = new NoteRemark();
        example.setMemberId(memberId);
        example.setMessageRef(messageRefId);
        return findByExample(example, "sentDate desc");
    }

    @Override
    public List<NoteRemark> findMessageByMessageRefId(String messageRefId) {
        NoteRemark example = new NoteRemark();
        example.setMessageRef(messageRefId);
        return findByExample(example, "sentDate desc");
    }

}
