package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.general.dao.SystemConfigDao;
import com.compalsolutions.compal.general.dao.SystemConfigImageDao;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.general.vo.SystemConfigImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(SystemConfigService.BEAN_NAME)
public class SystemConfigServiceImpl implements SystemConfigService {
    @Autowired
    private SystemConfigDao systemConfigDao;

    @Autowired
    private SystemConfigImageDao systemConfigImageDao;

    @Override
    public SystemConfig getDefaultSystemConfig() {
        return systemConfigDao.getDefault();
    }

    @Override
    public void doToogleMemberRegisterForSystemConfig() {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }

        systemConfig.setMemberRegister(!systemConfig.getMemberRegister());
        systemConfigDao.update(systemConfig);
    }

    @Override
    public void doToogleMemberLoginForSystemConfig() {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }

        systemConfig.setMemberLogin(!systemConfig.getMemberLogin());
        systemConfigDao.update(systemConfig);
    }

    private SystemConfig createDefaultSystemConfig() {
        SystemConfig systemConfig = new SystemConfig(true);
        systemConfigDao.save(systemConfig);

        return systemConfig;
    }

    @Override
    public void updateApplicationName(String applicationName) {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }

        systemConfig.setApplicationName(applicationName);
        systemConfigDao.update(systemConfig);
    }

    @Override
    public void updateCompanyName(String companyName) {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }

        systemConfig.setCompanyName(companyName);
        systemConfigDao.update(systemConfig);
    }

    @Override
    public void updateSupportEmail(String supportEmail) {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }

        systemConfig.setSupportEmail(supportEmail);
        systemConfigDao.update(systemConfig);
    }

//    @Override
//    public void updateGifVersion(Integer gifVersion) {
//        SystemConfig systemConfig = getDefaultSystemConfig();
//
//        if (systemConfig == null) {
//            systemConfig = createDefaultSystemConfig();
//        }
//
//        systemConfig.setGiftBatchVersion(gifVerion);
//        systemConfigDao.update(systemConfig);
//    }

    @Override
    public void updateQRCodeExpiry(Integer minutes) {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }
        systemConfig.setQrCodeExpiry(minutes);
        systemConfigDao.update(systemConfig);
    }

    @Override
    public void updateDefaultCountryCode(String countryCode) {
        SystemConfig systemConfig = getDefaultSystemConfig();

        if (systemConfig == null) {
            systemConfig = createDefaultSystemConfig();
        }

        systemConfig.setCountryCode(countryCode);
        systemConfigDao.update(systemConfig);
    }

    @Override
    public void saveOrUpdateSystemConfigImage(SystemConfigImage systemConfigImage) {
        SystemConfigImage systemImage = systemConfigImageDao.findSystemConfigImageByType(systemConfigImage.getImageType());
        if (systemImage != null) {
            systemImage.setFilename(systemConfigImage.getFilename());
            systemImage.setFileSize(systemConfigImage.getFileSize());
            systemImage.setContentType(systemConfigImage.getContentType());
            systemImage.setData(systemConfigImage.getData());
            systemConfigImageDao.update(systemImage);
        } else {
            systemConfigImageDao.save(systemConfigImage);
        }
    }

    @Override
    public void deleteSystemConfigImageById(String imageId) {
        SystemConfigImage systemConfigImage = findSystemConfigImageById(imageId);
        if (systemConfigImage != null) {
            systemConfigImageDao.delete(systemConfigImage);
        }
    }

    @Override
    public SystemConfigImage findSystemConfigImageById(String imageId) {
        return systemConfigImageDao.get(imageId);
    }

    @Override
    public SystemConfigImage findSystemConfigImageByType(String imageType) {
        return systemConfigImageDao.findSystemConfigImageByType(imageType);
    }

    @Override
    public List<SystemConfigImage> findSystemConfigImages() {
        return systemConfigImageDao.findSystemConfigImages();
    }

}