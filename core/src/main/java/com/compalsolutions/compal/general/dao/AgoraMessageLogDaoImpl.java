package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.AgoraMessageLog;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(AgoraMessageLogDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgoraMessageLogDaoImpl extends Jpa2Dao<AgoraMessageLog, String> implements AgoraMessageLogDao {
    public AgoraMessageLogDaoImpl() {
        super(new AgoraMessageLog(false));
    }

    @Override
    public List<AgoraMessageLog> findAgoraMessageByNoticeId(String noticeId) {
        AgoraMessageLog agoraMessageLog = new AgoraMessageLog(false);
        agoraMessageLog.setNoticeId(noticeId);
        agoraMessageLog.setErrorMessage(null);

        return findByExample(agoraMessageLog, "datetimeAdd");
    }
}
