package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.SystemConfig;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemConfigRepository extends JpaRepository<SystemConfig, String> {
    public static final String BEAN_NAME = "systemConfigRepository";
}
