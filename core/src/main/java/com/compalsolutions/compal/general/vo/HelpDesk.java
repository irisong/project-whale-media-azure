package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_help_desk")
@Access(AccessType.FIELD)
public class HelpDesk extends VoBase {
    private static final long serialVersionUID = 1L;

    public final static int MAX_FILE_UPLOAD = 3;

    public final static String STATUS_OPEN = "OPEN";
    public final static String STATUS_CLOSE = "CLOSED";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "ticket_id", unique = true, nullable = false, length = 32)
    private String ticketId;

    @Column(name = "ticket_no", length = 32, nullable = false)
    private String ticketNo;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ManyToOne
    @JoinColumn(name = "owner_id", insertable = false, updatable = false, nullable = true)
    private User owner;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    @Column(name = "ticket_type_id", nullable = false, length = 32)
    private String ticketTypeId;

    @ManyToOne
    @JoinColumn(name = "ticket_type_id", insertable = false, updatable = false, nullable = true)
    private HelpDeskType helpDeskType;

    @ToUpperCase
    @ToTrim
    @Column(name = "subject", length = 150, nullable = false)
    private String subject;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "close_datetime")
    private Date closeDatetime;

    @Column(name = "close_by", length = 32)
    private String closeBy;

    @ManyToOne
    @JoinColumn(name = "close_by", insertable = false, updatable = false, nullable = true)
    private User closeByUser;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "ticket_id", insertable = false, updatable = false, nullable = true)
    private List<HelpDeskReply> helpDeskReplies = new ArrayList<HelpDeskReply>();

    @Column(name = "reply_by_admin")
    private Boolean repliedByAdmin;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "admin_reply_datetime")
    private Date adminReplyDatetime;

    public HelpDesk() {
    }

    public HelpDesk(boolean defaultValue) {
        if (defaultValue) {
            repliedByAdmin = false;
            status = HelpDesk.STATUS_OPEN;
        }
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(String ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public HelpDeskType getHelpDeskType() {
        return helpDeskType;
    }

    public void setHelpDeskType(HelpDeskType helpDeskType) {
        this.helpDeskType = helpDeskType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Date getCloseDatetime() {
        return closeDatetime;
    }

    public void setCloseDatetime(Date closeDatetime) {
        this.closeDatetime = closeDatetime;
    }

    public String getCloseBy() {
        return closeBy;
    }

    public void setCloseBy(String closeBy) {
        this.closeBy = closeBy;
    }

    public User getCloseByUser() {
        return closeByUser;
    }

    public void setCloseByUser(User closeByUser) {
        this.closeByUser = closeByUser;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<HelpDeskReply> getHelpDeskReplies() {
        return helpDeskReplies;
    }

    public void setHelpDeskReplies(List<HelpDeskReply> helpDeskReplies) {
        this.helpDeskReplies = helpDeskReplies;
    }

    public Boolean getRepliedByAdmin() {
        return repliedByAdmin;
    }

    public void setRepliedByAdmin(Boolean repliedByAdmin) {
        this.repliedByAdmin = repliedByAdmin;
    }

    public Date getAdminReplyDatetime() {
        return adminReplyDatetime;
    }

    public void setAdminReplyDatetime(Date adminReplyDatetime) {
        this.adminReplyDatetime = adminReplyDatetime;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        HelpDesk helpDesk = (HelpDesk) o;

        if (ticketId != null ? !ticketId.equals(helpDesk.ticketId) : helpDesk.ticketId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return ticketId != null ? ticketId.hashCode() : 0;
    }
}
