package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "lg_agora_message_log")
@Access(AccessType.FIELD)
public class AgoraMessageLog extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "log_id", unique = true, nullable = false, length = 32)
    private String logId;

    @ToTrim
    @Column(name = "sid", length = 50)
    private String sid;

    @ToTrim
    @Column(name = "notice_id", length = 50)
    private String noticeId;

    @Column(name = "product_id")
    private Integer productId;

    @Column(name = "event_type")
    private Integer eventType;

    @Column(name = "notify_ms")
    private Long notifyMs;

    @ToTrim
    @Column(name = "payload", columnDefinition = "text")
    private String payload;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @Column(name = "error_message", columnDefinition = "text")
    private String errorMessage;

    public AgoraMessageLog() {
    }

    public AgoraMessageLog(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getLogId() {
        return logId;
    }

    public void setLogId(String logId) {
        this.logId = logId;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getEventType() {
        return eventType;
    }

    public void setEventType(Integer eventType) {
        this.eventType = eventType;
    }

    public Long getNotifyMs() {
        return notifyMs;
    }

    public void setNotifyMs(Long notifyMs) {
        this.notifyMs = notifyMs;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
