package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.dao.MobileForceUpdateDao;
import com.compalsolutions.compal.general.vo.MobileForceUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(MobileConfigService.BEAN_NAME)
public class MobileConfigServiceImpl implements MobileConfigService {
    private MobileForceUpdateDao mobileForceUpdateDao;

    @Autowired
    public MobileConfigServiceImpl(MobileForceUpdateDao mobileForceUpdateDao) {
        this.mobileForceUpdateDao = mobileForceUpdateDao;
    }

    @Override
    public MobileForceUpdate findMobileForceUpdateByPlatformAndFavor(String platform, String favor) {
        return mobileForceUpdateDao.findByPlatformAndFavor(platform, favor);
    }

    @Override
    public MobileForceUpdate doUpdateMobileForceUpdate(MobileForceUpdate mobileForceUpdate) {
        MobileForceUpdate dbMobileForceUpdate = findMobileForceUpdateByPlatformAndFavor(mobileForceUpdate.getPlatform(), mobileForceUpdate.getFavor());

        if (dbMobileForceUpdate == null) {
            throw new ValidatorException("Invalid Platform and Favor");
        }

        /*
        if(dbMobileForceUpdate == null){
            dbMobileForceUpdate = new MobileForceUpdate(true);
            dbMobileForceUpdate.setPlatform(mobileForceUpdate.getPlatform());
            dbMobileForceUpdate.setFavor(mobileForceUpdate.getFavor());
            dbMobileForceUpdate.setForceUpdate(mobileForceUpdate.getForceUpdate());
            dbMobileForceUpdate.setVersionCode(mobileForceUpdate.getVersionCode());
            dbMobileForceUpdate.setVersionName(mobileForceUpdate.getVersionName());
            mobileForceUpdateDao.save(dbMobileForceUpdate);
        }
        */

        // do not update updateUrl;
        dbMobileForceUpdate.setForceUpdate(mobileForceUpdate.getForceUpdate());
        dbMobileForceUpdate.setVersionCode(mobileForceUpdate.getVersionCode());
        dbMobileForceUpdate.setVersionName(mobileForceUpdate.getVersionName());
        mobileForceUpdateDao.update(dbMobileForceUpdate);

        return dbMobileForceUpdate;
    }
}
