package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import freemarker.template.utility.StringUtil;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Transient;
import java.util.Locale;

@Embeddable
public class Address {
    @ToUpperCase
    @ToTrim
    @Column(name = "contact_name", length = 200)
    private String contactName;

    @ToUpperCase
    @ToTrim
    @Column(name = "contact_no", length = 100)
    private String contactNo;

    @ToUpperCase
    @ToTrim
    @Column(name = "address", columnDefinition = Global.ColumnDef.TEXT)
    private String address;

    @ToUpperCase
    @ToTrim
    @Column(name = "address2", columnDefinition = Global.ColumnDef.TEXT)
    private String address2;

    @ToUpperCase
    @ToTrim
    @Column(name = "area", length = 200)
    private String area;

    @ToUpperCase
    @ToTrim
    @Column(name = "city", length = 200)
    private String city;

    @ToUpperCase
    @ToTrim
    @Column(name = "state", length = 200)
    private String state;

    @ToUpperCase
    @ToTrim
    @Column(name = "postcode", length = 50)
    private String postcode;

    @ToUpperCase
    @ToTrim
    @Column(name = "country_code", length = 10)
    private String countryCode;

    @Transient
    private String addressId;

    @Transient
    private String longAddress;

    public String getFullAddress(Locale locale, String lineSeparator) {
        StringBuffer sb = new StringBuffer();

        // For China, Taiwan & Japan in Mandarin || Japanese
        if (StringUtils.isNotBlank(postcode)) {
            sb.append(postcode).append(lineSeparator);
        }

        if (StringUtils.isNotBlank(countryCode)) {
            CountryService countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
            CountryDesc countryDesc = countryService.findCountryDescByLocaleAndCountryCode(locale, countryCode);
            if (countryDesc != null) {
                sb.append(countryDesc.getCountryName()).append(lineSeparator);
            }
        }

        if (StringUtils.isNotBlank(state)) {
            sb.append(state).append(", ").append(lineSeparator);
        }

        if (StringUtils.isNotBlank(city)) {
            sb.append(city).append(",").append(lineSeparator);
        }

        if (StringUtils.isNotBlank(area)) {
            sb.append(area).append(",");
        }

        if (StringUtils.isNotBlank(address2)) {
            sb.append(address2).append(",").append(lineSeparator);
        }

        if (StringUtils.isNotBlank(address)) {
            sb.append(address);
        }

        return sb.toString();
    }

    public String getInternationalFullAddress(Locale locale, String lineSeparator) {
        CountryService countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        StringBuffer sb = new StringBuffer();

        if (countryCode!= null && countryCode.equalsIgnoreCase("MY")) { //Malaysia
            if (StringUtils.isNotBlank(address)) {
                sb.append(address).append(", ").append(lineSeparator);
            }

            if (StringUtils.isNotBlank(address2)) {
                sb.append(address2).append(", ");
            }

            if (StringUtils.isNotBlank(area)) {
                Area objArea = countryService.getArea(area);
                if (objArea != null) {
                    sb.append(StringUtil.capitalize(objArea.getAreaName())).append(", ").append(lineSeparator);
                } else {
                    sb.append(area).append(", ").append(lineSeparator);
                }
            }

            if (StringUtils.isNotBlank(postcode)) {
                sb.append(postcode).append(" ");
            }

            if (StringUtils.isNotBlank(city)) {
                City objCity = countryService.getCity(city);
                if (objCity != null) {
                    sb.append(StringUtil.capitalize(objCity.getCityName())).append(", ").append(lineSeparator);
                } else {
                    sb.append(city).append(", ").append(lineSeparator);
                }
            }

            if (StringUtils.isNotBlank(state)) {
                Province province = countryService.getProvince(state);
                if (province != null) {
                    sb.append(StringUtil.capitalize(province.getProvinceName())).append(", ").append(lineSeparator);
                } else {
                    sb.append(state).append(", ").append(lineSeparator);
                }
            }

            if (StringUtils.isNotBlank(countryCode)) {
                Country country = countryService.getCountry(countryCode);
                if (country != null) {
                    sb.append(StringUtil.capitalize(country.getCountryName()));
                } else {
                    sb.append(countryCode);
                }
            }
        } else {
            if (StringUtils.isNotBlank(address)) {
                sb.append(address).append(", ").append(lineSeparator);
            }

            if (StringUtils.isNotBlank(address2)) {
                sb.append(address2).append(", ").append(lineSeparator);
            }

            if (StringUtils.isNotBlank(area)) {
                Area objArea = countryService.getArea(area);
                if (objArea != null) {
                    sb.append(StringUtil.capitalize(objArea.getAreaName())).append(", ").append(lineSeparator);
                } else {
                    sb.append(area).append(", ").append(lineSeparator);
                }
            }

            if (StringUtils.isNotBlank(city)) {
                City objCity = countryService.getCity(city);
                if (objCity != null) {
                    sb.append(StringUtil.capitalize(objCity.getCityName())).append(", ").append(lineSeparator);
                } else {
                    sb.append(city).append(", ").append(lineSeparator);
                }
            }

            if (StringUtils.isNotBlank(state)) {
                Province province = countryService.getProvince(state);
                if (province != null) {
                    sb.append(StringUtil.capitalize(province.getProvinceName())).append(", ").append(lineSeparator);
                } else {
                    sb.append(state).append(", ").append(lineSeparator);
                }
            }

            if (StringUtils.isNotBlank(postcode)) {
                sb.append(postcode).append(", ").append(lineSeparator);
            }

            if (StringUtils.isNotBlank(countryCode)) {
                Country country = countryService.getCountry(countryCode);
                if (country != null) {
                    sb.append(StringUtil.capitalize(country.getCountryName()));
                } else {
                    sb.append(countryCode);
                }
            }
        }

        return sb.toString();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Address address1 = (Address) o;

        if (contactName != null ? !contactName.equals(address1.contactName) : address1.contactName != null)
            return false;
        if (contactNo != null ? !contactNo.equals(address1.contactNo) : address1.contactNo != null)
            return false;
        if (address != null ? !address.equals(address1.address) : address1.address != null)
            return false;
        if (address2 != null ? !address2.equals(address1.address2) : address1.address2 != null)
            return false;
        if (area != null ? !area.equals(address1.area) : address1.area != null)
            return false;
        if (city != null ? !city.equals(address1.city) : address1.city != null)
            return false;
        if (state != null ? !state.equals(address1.state) : address1.state != null)
            return false;
        if (postcode != null ? !postcode.equals(address1.postcode) : address1.postcode != null)
            return false;
        return countryCode != null ? countryCode.equals(address1.countryCode) : address1.countryCode == null;
    }

    @Override
    public int hashCode() {
        int result = contactName != null ? contactName.hashCode() : 0;
        result = 31 * result + (contactNo != null ? contactNo.hashCode() : 0);
        result = 31 * result + (address != null ? address.hashCode() : 0);
        result = 31 * result + (address2 != null ? address2.hashCode() : 0);
        result = 31 * result + (area != null ? area.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (postcode != null ? postcode.hashCode() : 0);
        result = 31 * result + (countryCode != null ? countryCode.hashCode() : 0);
        return result;
    }

    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }

    public String getLongAddress() {
        return longAddress;
    }

    public void setLongAddress(String longAddress) {
        this.longAddress = longAddress;
    }
}
