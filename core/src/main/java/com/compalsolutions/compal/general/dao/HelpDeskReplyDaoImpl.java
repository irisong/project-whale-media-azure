package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.HelpDeskReply;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(HelpDeskReplyDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpDeskReplyDaoImpl extends Jpa2Dao<HelpDeskReply, String> implements HelpDeskReplyDao {
    public HelpDeskReplyDaoImpl() {
        super(new HelpDeskReply(false));
    }

    @Override
    public int findTotalCountHelpDeskReplyByTicketId(String ticketId) {
        List<Object> params = new ArrayList<>();

        Validate.notBlank(ticketId);

        String hql = "SELECT COUNT(*) FROM HelpDeskReply r WHERE r.ticketId = ? ";
        params.add(ticketId);

        Number result = (Number) this.exFindUnique(hql, params.toArray());
        if (result != null)
            return result.intValue();

        return 0;
    }
}
