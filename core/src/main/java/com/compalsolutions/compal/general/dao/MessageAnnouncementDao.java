package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.general.vo.MessageAnnouncementRemoved;

public interface MessageAnnouncementDao extends BasicDao<MessageAnnouncementRemoved, String> {
    public static final String BEAN_NAME = "messageAnnouncementDao";

    public void doDeleteAnnounceMessage(String memberId, Message message);
}
