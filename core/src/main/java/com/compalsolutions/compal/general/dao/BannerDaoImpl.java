package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.repository.AnnouncementRepository;
import com.compalsolutions.compal.general.repository.BannerRepository;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.point.vo.Symbolic;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(BannerDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BannerDaoImpl extends Jpa2Dao<Banner, String> implements BannerDao {
    @SuppressWarnings("unused")
    @Autowired
    private BannerRepository bannerRepository;

    public BannerDaoImpl() {
        super(new Banner(false));
    }

    @Override
    public void findBannersForDatagrid(DatagridModel<Banner> datagridModel, List<String> userGroups, String status, String bannerType, String category) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + Banner.class + " WHERE 1=1 ";
        if (CollectionUtil.isNotEmpty(userGroups)) {
            for (String userGroup : userGroups) {
                hql += " and a.userGroups like ? ";
                params.add("%" + userGroup + "%");
            }
        }

        if (StringUtils.isNotBlank(status)) {
            hql += " and a.status=? ";
            params.add(status);
        }

        if (StringUtils.isNotBlank(category)) {
            hql += " and a.categoryId=? ";
            params.add(category);
        }

        if (StringUtils.isNotBlank(bannerType)) {
            hql += " and a.bannerType=? ";
            params.add(bannerType);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public List<Banner> findAllActiveBanner() {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM s IN " + Banner.class + " WHERE 1=1 ";
        hql += " AND s.status = 'ACTIVE' order by seq";
        // params.add(Global.Status.ACTIVE);
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<Banner> findBannerByCategory(String categoryId) {
        List<Object> params = new ArrayList<Object>();
        String hql = " FROM s IN " + Banner.class + " WHERE 1=1 ";
        hql += " AND s.categoryId = ? AND s.status = ? ORDER BY seq DESC";
        params.add(categoryId);
        params.add(Global.Status.ACTIVE);
        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public int getNextBannerSeq(String categoryId){
        String hql = "SELECT seq FROM Banner WHERE categoryId = ? ORDER BY seq DESC";

        List<Object> params = new ArrayList<>();
        params.add(categoryId);

        Integer result = (Integer) exFindFirst(hql, params.toArray());
        if (result != null)
            return result.intValue() + 1;

        return 0;
    }

    @Override
    public int getNextBannerCategorySeq() {
        String hql = "SELECT seq FROM LiveCategory ORDER BY seq DESC";

        Integer result = (Integer) exFindFirst(hql);
        if (result != null){
            return result.intValue() + 1;
        }

        return 0;
    }

    @Override
    public void updateBannerSeq(String type, int seq) {
        boolean moveSeq = false;

        Banner banner = new Banner(false);
        banner.setSeq(seq);

        List<Banner> duplicateSequence = findByExample(banner);
        if (type.equalsIgnoreCase("NEW")) {
            if (duplicateSequence.size() > 0) { //this seq already in use
                moveSeq = true;
            }
        } else {
            if (duplicateSequence.size() == 1) { //no other record having same seq
                moveSeq = true;
            }
        }

        if (moveSeq) {
            String action = "seq+1";
            if (type.equalsIgnoreCase("OLD")) {
                action = "seq-1";
            }

            String hql = "UPDATE Banner SET seq = " + action + " WHERE seq >= ?";

            List<Object> params = new ArrayList<>();
            params.add(seq);

            bulkUpdate(hql, params.toArray());
        }
    }
}
