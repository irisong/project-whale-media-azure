package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.exception.DevelopmentException;
import com.compalsolutions.compal.general.repository.CountryDescRepository;
import com.compalsolutions.compal.general.vo.CountryDesc;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@Component(CountryDescDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class CountryDescDaoImpl extends Jpa2Dao<CountryDesc, String> implements CountryDescDao {
    @SuppressWarnings("unused")
    @Autowired
    private CountryDescRepository countryDescRepository;

    public CountryDescDaoImpl() {
        super(new CountryDesc(false));
    }

    @Override
    public List<CountryDesc> findCountryDescsByLocale(Locale locale) {
        CountryDesc example = new CountryDesc(false);
        example.setLanguageCode(locale.toString());
        return findByExample(example, "countryName");
    }

    @Override
    public CountryDesc findCountryDescByLocaleAndCountryCode(Locale locale, String countryCode) {
        if (StringUtils.isBlank(countryCode)) {
            throw new DevelopmentException("country code can not be null!");
        }

        CountryDesc example = new CountryDesc(false);
        example.setLanguageCode(locale.toString());
        example.setCountryCode(countryCode);
        return findFirst(example, "countryName");
    }


}
