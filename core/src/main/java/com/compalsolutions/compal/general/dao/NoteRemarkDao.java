package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.NoteRemark;

import java.util.List;

public interface NoteRemarkDao extends BasicDao<NoteRemark, String> {
    public static final String BEAN_NAME = "noteRemarkDao";

    public List<NoteRemark> findMessageByMemberIdAndMessageRefId(String memberId, String messageRefId);

    public List<NoteRemark> findMessageByMessageRefId(String messageRefId);

}
