package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.MobileForceUpdate;

public interface MobileForceUpdateDao extends BasicDao<MobileForceUpdate, String> {
    public static final String BEAN_NAME = "mobileForceUpdateDao";

    public MobileForceUpdate findByPlatformAndFavor(String platform, String favor);
}
