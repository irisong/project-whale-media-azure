package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "app_banner")
@Access(AccessType.FIELD)
public class Banner extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String OPEN_WEB_URL = "WEBVIEW";
    public static final String OPEN_VJ_PROFILE = "VJ_PROFILE";
    public static final String OPEN_VJ_LIVE = "VJ_LIVE";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "banner_id", unique = true, nullable = false, length = 32)
    private String bannerId;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "banner_id", insertable = false, updatable = false, nullable = true)
    private List<BannerFile> bannerFile = new ArrayList<>();

    @ToTrim
    @Column(name = "title", length = 250, nullable = false)
    private String title;

    @ToTrim
    @Column(name = "titleCn", length = 250)
    private String titleCn;

    @ToTrim
    @Column(name = "titleMs", length = 250)
    private String titleMs;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @Column(name = "bodyCn", columnDefinition = "text")
    private String bodyCn;

    @ToTrim
    @Column(name = "bodyMs", columnDefinition = "text")
    private String bodyMs;

    @ToTrim
    @ToUpperCase
    @Column(name = "user_groups", length = 50, nullable = false)
    private String userGroups;


    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @ToUpperCase
    @Column(name = "banner_type", length = 20, nullable = false)
    private String bannerType;

    @ToTrim
    @Column(name = "redirect_url", columnDefinition = "text")
    private String redirectUrl;

    @ToTrim
    @Column(name = "live_category_id", length = 32)
    private String categoryId;

    @ManyToOne
    @JoinColumn(name = "live_category_id", insertable = false, updatable = false, nullable = true)
    private LiveCategory liveCategory;

    @ToTrim
    @Column(name = "seq")
    private Integer seq;

    @ToTrim
    @Column(name = "timer")
    private Integer timer;

    @Transient
    private String fileUrl;

    @Transient
    private String fileUrlEn;

    @Transient
    private String fileUrlCn;

    @Transient
    private String fileUrlMs;

    @Transient
    private String categoryName;

    @Transient
    private String bannerTypeDesc;

    public Banner() {
    }

    public Banner(boolean defaultValue) {
        if (defaultValue) {
            userGroups = Global.PublishGroup.PUBLIC_GROUP;
        }
    }

    public String getBannerId() {
        return bannerId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleCn() {
        return titleCn;
    }

    public void setTitleCn(String titleCn) {
        this.titleCn = titleCn;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBodyCn() {
        return bodyCn;
    }

    public void setBodyCn(String bodyCn) {
        this.bodyCn = bodyCn;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBannerType() {
        return bannerType;
    }

    public void setBannerType(String bannerType) {
        this.bannerType = bannerType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<BannerFile> getBannerFile() {
        return bannerFile;
    }

    public void setBannerFile(List<BannerFile> bannerFile) {
        this.bannerFile = bannerFile;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getFileUrlEn() {
        return fileUrlEn;
    }

    public void setFileUrlEn(String fileUrlEn) {
        this.fileUrlEn = fileUrlEn;
    }

    public String getFileUrlCn() {
        return fileUrlCn;
    }

    public void setFileUrlCn(String fileUrlCn) {
        this.fileUrlCn = fileUrlCn;
    }

    public String getFileUrlMs() {
        return fileUrlMs;
    }

    public void setFileUrlMs(String fileUrlMs) {
        this.fileUrlMs = fileUrlMs;
    }

    public LiveCategory getLiveCategory() {
        return liveCategory;
    }

    public void setLiveCategory(LiveCategory liveCategory) {
        this.liveCategory = liveCategory;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getTitleMs() {
        return titleMs;
    }

    public void setTitleMs(String titleMs) {
        this.titleMs = titleMs;
    }

    public String getBodyMs() {
        return bodyMs;
    }

    public void setBodyMs(String bodyMs) {
        this.bodyMs = bodyMs;
    }

    public String getBannerTypeDesc() {
        return bannerTypeDesc;
    }

    public void setBannerTypeDesc(String bannerTypeDesc) {
        this.bannerTypeDesc = bannerTypeDesc;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public Integer getTimer() {
        return timer;
    }

    public void setTimer(Integer timer) {
        this.timer = timer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Banner that = (Banner) o;

        if (bannerId != null ? !bannerId.equals(that.bannerId) : that.bannerId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return bannerId != null ? bannerId.hashCode() : 0;
    }
}
