package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.Area;

import java.util.List;

public interface AreaDao extends BasicDao<Area, String> {
    public static final String BEAN_NAME = "areaDao";


    public List<Area> getAreaListByCityCode(String cityCode);

    public Area findAreaByAreaName(String areaName);
}
