package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.SystemConfigImage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemConfigImageRepository extends JpaRepository<SystemConfigImage, String> {
    public static final String BEAN_NAME = "systemConfigImageRepository";
}
