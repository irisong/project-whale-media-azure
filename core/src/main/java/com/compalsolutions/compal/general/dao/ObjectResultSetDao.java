package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;

import java.util.List;

public interface ObjectResultSetDao extends BasicDao<Object[], String> {
    public static final String BEAN_NAME = "objectResultSetDao";

    public List<Object[]> findPosPaymentStatisticForDaily(String receiverId, Double rate, String beginDate, String endDate);

    public List<Object[]> findPosPaymentStatisticForMonthly(String receiverId, Double rate, String beginDate, String endDate);
}


