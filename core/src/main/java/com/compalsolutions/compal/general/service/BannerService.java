package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.general.vo.BannerFile;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface BannerService {
    public static final String BEAN_NAME = "bannerService";

    public void findBannersForListing(DatagridModel<Banner> datagridModel, List<String> userGroups, String status, String bannerType,String category);

    public void setBannerFileUrl(Banner banner);

    public void saveBanner(Locale locale, Banner banner, List<BannerFile> bannerFileList);

    public void doSaveBannerFile(String uploadPath, String bannerId, BannerFile bannerFile);

    public Banner getBanner(String bannerId);

    public List<Banner> findAllActiveBanner();

    public List<Banner> findBannerByCategory(String categoryId);

    public BannerFile findBannerFileByBannerId(String bannerId, String language);

    public void updateBanner(Locale locale, Banner banner, List<BannerFile> bannerFileList);

    public void doDeleteBanner(String bannerId);

    public void doDeleteBannerFile(String bannerId, String language);

    public byte[] getScaledImage(byte[] srcImg, Integer width, Integer height);

    public int getNextBannerSeq(String categoryId);

    public int getNextBannerCategorySeq();

}
