package com.compalsolutions.compal.general.repository;

import com.compalsolutions.compal.general.vo.Country;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CountryRepository extends JpaRepository<Country, String> {
    public static final String BEAN_NAME = "countryRepository";
}
