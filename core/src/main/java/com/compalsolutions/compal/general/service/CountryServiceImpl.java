package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.general.dao.*;
import com.compalsolutions.compal.general.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Locale;

@Component(CountryService.BEAN_NAME)
public class CountryServiceImpl implements CountryService {
    @Autowired
    private CountryDao countryDao;

    @Autowired
    private ProvinceDao provinceDao;

    @Autowired
    private CityDao cityDao;

    @Autowired
    private AreaDao areaDao;

    @Autowired
    private CountryDescDao countryDescDao;

    @Override
    public Country getCountry(String countryCode) {
        return countryDao.get(countryCode);
    }

    @Override
    public Province getProvince(String provinceCode) {
        return provinceDao.get(provinceCode);
    }

    @Override
    public City getCity(String cityCode) {
        return cityDao.get(cityCode);
    }

    @Override
    public Area getArea(String areaCode) {
        return areaDao.get(areaCode);
    }

    @Override
    public List<CountryDesc> findCountryDescsByLocale(Locale locale) {
        Locale countryLocale = Global.LOCALE_CN.equals(locale) ? locale : new Locale("en");
        List<CountryDesc> countryDescsList = countryDescDao.findCountryDescsByLocale(countryLocale);
        for (CountryDesc country : countryDescsList) {
            switch (country.getCountryCode()) {
                case "CN":
                    country.setSeq(0);
                    break;
                case "MY":
                    country.setSeq(1);
                    break;
                case "TW":
                    country.setSeq(2);
                    break;
                case "VN":
                    country.setSeq(3);
                    break;
                case "JP":
                    country.setSeq(4);
                    break;
                case "KR":
                    country.setSeq(5);
                    break;
                case "LA":
                    country.setSeq(6);
                    break;
                case "SG":
                    country.setSeq(7);
                    break;
                case "TH":
                    country.setSeq(8);
                    break;
                case "PH":
                    country.setSeq(9);
                    break;
                case "ID":
                    country.setSeq(10);
                    break;
                case "BT":
                    country.setSeq(11);
                    break;
                case "TP":
                    country.setSeq(12);
                    break;
                case "MO":
                    country.setSeq(13);
                    break;
                case "HK":
                    country.setSeq(14);
                    break;
                default:
                    country.setSeq(99);
            }
        }
        countryDescsList.sort((p1, p2) -> {
            if (p1.getSeq().compareTo(p2.getSeq()) == 0) {
                return p1.getCountryName().compareTo(p2.getCountryName());
            } else {
                return p1.getSeq().compareTo(p2.getSeq());
            }
        });
        return countryDescsList;
    }

    @Override
    public CountryDesc findCountryDescByLocaleAndCountryCode(Locale locale, String countryCode) {
        return countryDescDao.findCountryDescByLocaleAndCountryCode(locale, countryCode);
    }

    @Override
    public List<Province> getProvinceListByCountryCode(String countryCode) {
        return provinceDao.findProvinceByCountryCode(countryCode);
    }


    @Override
    public List<City> getCityListByProvinceCode(String provinceCode) {
        return cityDao.getCityListByProvinceCode(provinceCode);
    }

    @Override
    public List<Area> getAreaListByCityCode(String cityCode) {
        return areaDao.getAreaListByCityCode(cityCode);
    }


    @Override
    public Province findProvinceByProvinceName(String provinceName) {
        return provinceDao.findProvinceByProvinceName(provinceName);
    }

    @Override
    public City findCityByCityName(String cityName) {
        return cityDao.findCityByCityName(cityName);
    }

    @Override
    public Area findAreaByAreaName(String areaName) {
        return areaDao.findAreaByAreaName(areaName);
    }

    @Override
    public Province findProvinceByStateCode(String stateCode) {
        return provinceDao.findProvinceByProvinceCode(stateCode);
    }
}
