package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(HelpDeskReplyFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class HelpDeskReplyFileDaoImpl extends Jpa2Dao<HelpDeskReplyFile, String> implements HelpDeskReplyFileDao {
    public HelpDeskReplyFileDaoImpl() {
        super(new HelpDeskReplyFile(false));
    }
}
