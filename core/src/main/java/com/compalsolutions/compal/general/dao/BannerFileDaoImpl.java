package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.BannerFile;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(BannerFileDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BannerFileDaoImpl extends Jpa2Dao<BannerFile, String> implements BannerFileDao {
    public BannerFileDaoImpl() {
        super(new BannerFile());
    }


    @Override
    public BannerFile findBannerFileByBannerId(String bannerId, String language) {
        BannerFile example = new BannerFile();

        if (StringUtils.isNotBlank(bannerId))
            example.setBannerId(bannerId);

        if (StringUtils.isNotBlank(language))
            example.setLanguage(language);

        return findFirst(example, "sortOrder");
    }

    @Override
    public List<BannerFile> findBannerFileListByBannerId(String bannerId) {
        BannerFile example = new BannerFile();

        if (StringUtils.isNotBlank(bannerId))
            example.setBannerId(bannerId);

        return findByExample(example, "sortOrder");
    }
}
