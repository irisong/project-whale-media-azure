package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "app_system_config_image")
@Access(AccessType.FIELD)
public class SystemConfigImage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "image_id", unique = true, nullable = false, length = 32)
    private String imageId;

    @ToTrim
    @ToUpperCase
    @Column(name = "image_type", length = 10, nullable = false)
    private String imageType;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data", columnDefinition = Global.ColumnDef.MEDIUM_BLOB)
    private byte[] data;

    @Column(name = "file_size", nullable = false)
    private Long fileSize;

    public SystemConfigImage() {
    }

    public SystemConfigImage(boolean defaultValue) {
        if (defaultValue) {

        }
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        SystemConfigImage systemConfigImage = (SystemConfigImage) o;

        if (imageId != null ? !imageId.equals(systemConfigImage.imageId) : systemConfigImage.imageId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return imageId != null ? imageId.hashCode() : 0;
    }
}
