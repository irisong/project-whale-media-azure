package com.compalsolutions.compal.general.client;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.client.media.RtcTokenBuilder;
import com.compalsolutions.compal.general.client.media.RtcTokenBuilder.Role;
import com.compalsolutions.compal.general.client.media.Utils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.http.client.methods.HttpPost;
import org.springframework.core.env.Environment;

import java.util.*;

public class AgoraClient {
    private static final Log log = LogFactory.getLog(AgoraClient.class);

    private String serverUrl;
    private String appKey;
    private String appSecret;
    private String appId;
    private String appCertificate;
    private String secretKey;
    private String authorizationHeader;

    public AgoraClient() {
        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("agora.url");
        appKey = env.getProperty("agora.appKey");
        appSecret = env.getProperty("agora.appSecret");
        appId = env.getProperty("agora.appId");
        appCertificate = env.getProperty("agora.appCertificate");
        secretKey = env.getProperty("agora.secretKey");

        String credentials = appKey + ":" + appSecret;
        String base64Credentials = new String(Base64.getEncoder().encode(credentials.getBytes()));
        authorizationHeader = "Basic " + base64Credentials;
    }

    public String getRtcToken(String channelName, int uid) throws Exception {
        int expirationTimeInSeconds = 14400; //4 hours
        int timestamp = (int)(System.currentTimeMillis() / 1000 + expirationTimeInSeconds);

        RtcTokenBuilder token = new RtcTokenBuilder();
        return token.buildTokenWithUid(appId, appCertificate, channelName, uid, Role.Role_Publisher, timestamp);
    }

    public boolean checkSignature(String signature, String body) throws Exception {
        String encryptedSignature = Utils.hmacSha1(body, secretKey);
        if (signature.equals(encryptedSignature)) {
            return true;
        } else {
            System.out.println("encryptedSignature: "+encryptedSignature);
            return false;
        }
    }

    private HttpPost generateHttpHeader(String uri) {
        HttpPost httpPost = new HttpPost(serverUrl + uri);
        httpPost.addHeader("Authorization", authorizationHeader);
        httpPost.addHeader("Content-Type", "application/json");
        return httpPost;
    }
}
