package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.FreqAskQuesFileUploadConfiguration;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.FreqAskQuesDao;
import com.compalsolutions.compal.general.dao.FreqAskQuesFileDao;
import com.compalsolutions.compal.general.vo.FreqAskQues;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Locale;

@Component(FreqAskQuesService.BEAN_NAME)
public class FreqAskQuesServiceImpl implements FreqAskQuesService {
    private static Object synchronizedObject = new Object();

    @Autowired
    private FreqAskQuesDao freqAskQuesDao;

    @Autowired
    private FreqAskQuesFileDao freqAskQuesFileDao;

    @Override
    public void findFreqAskListForListing(DatagridModel<FreqAskQues> datagridModel, String status) {
        freqAskQuesDao.findFreqAskListForDatagrid(datagridModel, status);
        for (FreqAskQues freqAskQues : datagridModel.getRecords()) {
            setFaqFileUrl(freqAskQues);
        }
    }

    @Override
    public void setFaqFileUrl(FreqAskQues freqAskQues) {
        FreqAskQuesFileUploadConfiguration config = Application.lookupBean(FreqAskQuesFileUploadConfiguration.BEAN_NAME, FreqAskQuesFileUploadConfiguration.class);

        for (FreqAskQuesFile file : freqAskQues.getFreqAskQuesFile()) {
            String fileUrl = file.getFileUrlWithParentPath(config.getFullFaqParentFileUrl());

            switch (file.getLanguage()) {
                case FreqAskQuesFile.LANG_EN:
                    freqAskQues.setFreqAskQuesFileUrlEn(fileUrl);
                    break;
                case FreqAskQuesFile.LANG_CN:
                    freqAskQues.setFreqAskQuesFileUrlCn(fileUrl);
                    break;
                case FreqAskQuesFile.LANG_MS:
                    freqAskQues.setFreqAskQuesFileUrlMs(fileUrl);
                    break;
                case FreqAskQuesFile.COMBINED:
                    freqAskQues.setFreqAskQuesFileUrlCombine(fileUrl);
                    break;
            }
        }
    }

    @Override
    public void doSaveNewFreqAskQues(Locale locale, FreqAskQues freqAskQues, List<FreqAskQuesFile> freqAskQuesFiles) {
        FreqAskQuesFileUploadConfiguration config = Application.lookupBean(FreqAskQuesFileUploadConfiguration.BEAN_NAME, FreqAskQuesFileUploadConfiguration.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);

        final String uploadPath = config.getFaqFileUploadPath();

        /************************
         * VERIFICATION - START
         ************************/

        if (StringUtils.isBlank(freqAskQues.getTitle()) ||
                StringUtils.isBlank(freqAskQues.getTitleCn()) ||
                StringUtils.isBlank(freqAskQues.getTitleMs())) {
            throw new ValidatorException(i18n.getText("invalidTitle", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/
        FreqAskQues freqAskQuesdb = new FreqAskQues(true);
        freqAskQuesdb.setTitle(freqAskQues.getTitle());
        freqAskQuesdb.setTitleCn(freqAskQues.getTitleCn());
        freqAskQuesdb.setTitleMs(freqAskQues.getTitleMs());
        if (freqAskQues.getSortOrder() != null)
            freqAskQuesdb.setSortOrder(freqAskQues.getSortOrder());

        freqAskQuesDao.save(freqAskQuesdb);

        for (FreqAskQuesFile freqAskQuesFile : freqAskQuesFiles) {
            doSaveFaqFile(uploadPath, freqAskQuesdb.getQuestionId(), freqAskQuesFile);
        }
    }

    @Override
    public void doSaveFaqFile(String uploadPath, String questionId, FreqAskQuesFile freqAskQuesFile) {
        if (StringUtils.isNotBlank(freqAskQuesFile.getFilename())) {
            FreqAskQuesFile freqAskQuesFiledb = new FreqAskQuesFile(true);
            freqAskQuesFiledb.setQuestionId(questionId);
            freqAskQuesFiledb.setLanguage(freqAskQuesFile.getLanguage());
            freqAskQuesFiledb.setFilename(freqAskQuesFile.getFilename());
            freqAskQuesFiledb.setContentType(freqAskQuesFile.getContentType());
            freqAskQuesFiledb.setFileSize(freqAskQuesFile.getFileSize());
            freqAskQuesFileDao.save(freqAskQuesFiledb);

            File directory = new File(uploadPath);
            if (!directory.exists()) {
                directory.mkdirs();
            }

            try {
                byte[] bytes = Files.readAllBytes(Paths.get(freqAskQuesFile.getFileUpload().getAbsolutePath()));

                Path path = Paths.get(uploadPath, freqAskQuesFiledb.getRenamedFilename());
                Files.write(path, bytes);
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        }
    }

    @Override
    public void doDeleteFaqFile(String uploadPath, FreqAskQuesFile freqAskQuesFiles_db) {
        try {
            Files.deleteIfExists(Paths.get(freqAskQuesFiles_db.getFileUrlWithParentPath(uploadPath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        freqAskQuesFileDao.delete(freqAskQuesFiles_db);
    }

    @Override
    public FreqAskQuesFile getHelpFreqAskQuesFile(String freqAskQuesFileId) {
        return freqAskQuesFileDao.get(freqAskQuesFileId);
    }

    @Override
    public FreqAskQues findFreqAskQuesByQuestionId(String questionId) {
        return freqAskQuesDao.get(questionId);
    }

    @Override
    public void doUpdateFreqAskQues(Locale locale, FreqAskQues freqAskQues, List<FreqAskQuesFile> freqAskQuesFiles) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        FreqAskQuesFileUploadConfiguration config = Application.lookupBean(FreqAskQuesFileUploadConfiguration.BEAN_NAME, FreqAskQuesFileUploadConfiguration.class);
        final String uploadPath = config.getFaqFileUploadPath();

        /************************
         * VERIFICATION - START
         ************************/

        if (StringUtils.isBlank(freqAskQues.getTitle()) ||
                StringUtils.isBlank(freqAskQues.getTitleCn()) ||
                StringUtils.isBlank(freqAskQues.getTitleMs())) {
            throw new ValidatorException(i18n.getText("invalidTitle", locale));
        }

        /************************
         * VERIFICATION - END
         ************************/

        FreqAskQues freqAskQuesdb = findFreqAskQuesByQuestionId(freqAskQues.getQuestionId());
        if (freqAskQuesdb != null) { //update
            freqAskQuesdb.setTitle(freqAskQues.getTitle());
            freqAskQuesdb.setTitleCn(freqAskQues.getTitleCn());
            freqAskQuesdb.setTitleMs(freqAskQues.getTitleMs());
            freqAskQuesdb.setStatus(freqAskQues.getStatus());

            if (freqAskQues.getSortOrder() != null)
                freqAskQuesdb.setSortOrder(freqAskQues.getSortOrder());
            else
                freqAskQuesdb.setSortOrder(0);

            freqAskQuesDao.update(freqAskQuesdb);
        } else {
            throw new ValidatorException(i18n.getText("faqNotExists", locale));
        }

        // no need delete file before save, because already delete when user click change/delete button
        for (FreqAskQuesFile freqAskQuesFile : freqAskQuesFiles) {
            doSaveFaqFile(uploadPath, freqAskQuesdb.getQuestionId(), freqAskQuesFile);
        }
    }

    @Override
    public FreqAskQuesFile findFreqAskQuesFileByQuestionIdAndLanguage(String questionId, String language) {
        return freqAskQuesFileDao.findFreqAskQuesFileByQuestionIdByLanguage(questionId, language);
    }

    @Override
    public List<FreqAskQues> findAllFreqAskQues(String status) {
        return freqAskQuesDao.findAllFreqAskQues(status);
    }
}
