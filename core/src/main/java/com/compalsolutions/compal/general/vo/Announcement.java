package com.compalsolutions.compal.general.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "app_announce")
@Access(AccessType.FIELD)
public class Announcement extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "announce_id", unique = true, nullable = false, length = 32)
    private String announceId;

    @ToTrim
    @Column(name = "title", length = 250, nullable = false)
    private String title;

    @ToTrim
    @Column(name = "titleCn", length = 250)
    private String titleCn;

    @ToTrim
    @Column(name = "body", columnDefinition = "text")
    private String body;

    @ToTrim
    @Column(name = "bodyCn", columnDefinition = "text")
    private String bodyCn;

    @ToTrim
    @Column(name = "members", columnDefinition = "text")
    private String members;

    @ToTrim
    @ToUpperCase
    @Column(name = "user_groups", length = 50, nullable = false)
    private String userGroups;

    @Temporal(TemporalType.DATE)
    @Column(name = "publish_date", nullable = false)
    private Date publishDate;

    @ToTrim
    @ToUpperCase
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @ToTrim
    @ToUpperCase
    @Column(name = "announcement_type", length = 20, nullable = false)
    private String announcementType;

    @ToTrim
    @Column(name = "redirect_url", columnDefinition = "text")
    private String redirectUrl;

    @Transient
    private List<String[]> languageSet;

    @Transient
    private String memberId;

    public Announcement() {
    }

    public Announcement(boolean defaultValue) {
        if (defaultValue) {
            publishDate = new Date();
            userGroups = Global.PublishGroup.PUBLIC_GROUP;
        }
    }

    public String getAnnounceId() {
        return announceId;
    }

    public void setAnnounceId(String announceId) {
        this.announceId = announceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleCn() {
        return titleCn;
    }

    public void setTitleCn(String titleCn) {
        this.titleCn = titleCn;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBodyCn() {
        return bodyCn;
    }

    public void setBodyCn(String bodyCn) {
        this.bodyCn = bodyCn;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAnnouncementType() {
        return announcementType;
    }

    public void setAnnouncementType(String announcementType) {
        this.announcementType = announcementType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public List<String[]> getLanguageSet() {
        return languageSet;
    }

    public void setLanguageSet(List<String[]> languageSet) {
        this.languageSet = languageSet;
    }

    public String getMembers() {
        return members;
    }

    public void setMembers(String members) {
        this.members = members;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Announcement that = (Announcement) o;

        if (announceId != null ? !announceId.equals(that.announceId) : that.announceId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return announceId != null ? announceId.hashCode() : 0;
    }
}
