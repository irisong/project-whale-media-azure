package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.FreqAskQues;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(FreqAskQuesDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class FreqAskQuesDaoImpl extends Jpa2Dao<FreqAskQues, String> implements FreqAskQuesDao {
    public FreqAskQuesDaoImpl() {
        super(new FreqAskQues(false));
    }

    @Override
    public void findFreqAskListForDatagrid(DatagridModel<FreqAskQues> datagridModel, String status) {
        List<Object> params = new ArrayList<>();
        String hql = "SELECT f FROM FreqAskQues f WHERE 1=1 ";

        if (StringUtils.isNotBlank(status)) {
            hql += "AND f.status = ? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "f", hql, params.toArray());
    }

    @Override
    public List<FreqAskQues> findAllFreqAskQues(String status) {
        FreqAskQues example = new FreqAskQues(false);

        if (StringUtils.isNotBlank(status))
            example.setStatus(status);

        return findByExample(example, "sortOrder");
    }
}
