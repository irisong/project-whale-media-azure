package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(ObjectResultSetDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class ObjectResultSetDaoImpl extends Jpa2Dao<Object[], String> implements ObjectResultSetDao {
    public ObjectResultSetDaoImpl() {
        super(new Object[0]);
    }

    @Override
    public List<Object[]> findPosPaymentStatisticForDaily(String receiverId, Double rate, String beginDate, String endDate) {
        String hql = "select count(*) as transNo, sum(fiatAmount) * " + rate + " as totalAmount, receiverId, " +
                " trxDate , status " +
                " from PosPayment group by " +
                " receiverId, trxDate, status  having status ='COMPLETED' " +
                " AND receiverId='" + receiverId + "'and trxDate" +
                " BETWEEN '" + beginDate + "' AND '" + endDate + "' ";
        return findQueryAsList(hql);
    }

    @Override
    public List<Object[]> findPosPaymentStatisticForMonthly(String receiverId, Double rate, String beginDate, String endDate) {
        String hql = "select count(*) as transNo, sum(fiatAmount) * " + rate + " as totalAmount, receiverId, trxMonth," +
                "  status from PosPayment group by receiverId, trxMonth, status " +
                "  having status ='COMPLETED' AND receiverId='" + receiverId + "' and trxMonth" +
                "  BETWEEN '" + beginDate + "' AND '" + endDate + "' ";
        return findQueryAsList(hql);
    }
}
