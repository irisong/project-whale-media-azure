package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.HelpDeskReply;

public interface HelpDeskReplyDao extends BasicDao<HelpDeskReply, String> {
    public static final String BEAN_NAME = "helpDeskReplyDao";

    public int findTotalCountHelpDeskReplyByTicketId(String ticketId);
}
