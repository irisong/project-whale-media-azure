package com.compalsolutions.compal.general.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.dao.MessageAnnouncementDao;
import com.compalsolutions.compal.general.dao.MessageDao;
import com.compalsolutions.compal.general.dao.MessageSqlDao;
import com.compalsolutions.compal.general.vo.*;
import com.compalsolutions.compal.member.service.MemberReportService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(MessageService.BEAN_NAME)
public class MessageServiceImpl implements MessageService {

    @Autowired
    private MessageDao messageDao;

    @Autowired
    private MessageSqlDao messageSqlDao;

    @Autowired
    private MessageAnnouncementDao messageAnnouncementDao;

    @Override
    public void findAllMessageByReceiverId(DatagridModel<Message> datagridModel, String receiverId) {
        messageSqlDao.findAllMessageByReceiverIdByPaging(datagridModel, receiverId);
    }

    @Override
    public Message findMessageByMessageId(String messageId) {
        return messageDao.get(messageId);
    }

    @Override
    public void doGenerateFollowingMessage(String followerId, String memberId) {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
//        CryptoProvider cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);
        I18n i18n = Application.lookupBean(I18n.class);

        MemberDetail memberDetail = memberService.findMemberDetailByMemberId(followerId);
        if (memberDetail == null) {
            throw new ValidatorException("Invalid member");
        }

//        MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(followerId, MemberFile.UPLOAD_TYPE_PROFILE_PIC);
//        final String parentUrl = cryptoProvider.getServerUrl() + "/file/memberFile/";
//        if (memberFile != null) {
//            memberFile.setFileUrlWithParentPath(parentUrl);
//        }

        Message message = messageDao.findMessageByReceiverIdAndDirectToId(memberId, followerId);
        if (message == null) { //only insert when record not exist
            message = new Message();
            message.setMemberId(memberId);
            message.setDirectToMemberId(followerId);
            message.setTitle(i18n.getText("title.userFollowingMessage", new Locale("en")));
            message.setTitleCn(i18n.getText("title.userFollowingMessage", Global.LOCALE_CN));
            message.setContent(i18n.getText("message.userFollowingMessage", new Locale("en"), memberDetail.getProfileName()));
            message.setContentCn(i18n.getText("message.userFollowingMessage", Global.LOCALE_CN, memberDetail.getProfileName()));
            message.setMessageType(Global.MessageType.FOLLOWING);
            message.setSentDate(new Date());
            message.setSender("SYSTEM");
//            message.setImageUrl(memberFile != null ? memberFile.getFileUrl() : null);

            messageDao.save(message);
        }
    }

    @Override
    public Message doGenerateWarningMessage(String memberId, String content, String contentCn) {
        I18n i18n = Application.lookupBean(I18n.class);

        //Message message = messageDao.findMessageByTypeAndReferenceAndMemberId(Global.MessageType.WARNING, null, memberId);
        //if (message == null) { //only insert when record not exist
            Message message = new Message();
            message.setMemberId(memberId);
            message.setTitle(i18n.getText("title.warningMessage", Global.LOCALE_EN));
            message.setTitleCn(i18n.getText("title.warningMessage", Global.LOCALE_CN));
            message.setContent(i18n.getText("message.warningMessage", Global.LOCALE_EN, content));
            message.setContentCn(i18n.getText("message.warningMessage", Global.LOCALE_CN, contentCn));
            message.setMessageType(Global.MessageType.WARNING);
            message.setSentDate(new Date());
            message.setSender("SYSTEM");

            messageDao.save(message);
        //}
        return message;
    }

    @Override
    public Message doGenerateReplyWarningMessage(String memberId) {
        I18n i18n = Application.lookupBean(I18n.class);

        Message message = messageDao.findMessageByTypeAndReferenceAndMemberId(Global.MessageType.REPLY_WARNING, null, memberId);
        if (message != null) { //delete previous reply message
            messageDao.delete(message);
        }

        message = new Message();
        message.setMemberId(memberId);
        message.setTitle(i18n.getText("title.systemReply", Global.LOCALE_EN));
        message.setTitleCn(i18n.getText("title.systemReply", Global.LOCALE_CN));
        message.setContent(i18n.getText("message.systemReplyWarningMessage", Global.LOCALE_EN));
        message.setContentCn(i18n.getText("message.systemReplyWarningMessage", Global.LOCALE_CN));
        message.setMessageType(Global.MessageType.REPLY_WARNING);
        message.setSentDate(new Date());
        message.setSender("SYSTEM");

        messageDao.save(message);

        return message;
    }

    @Override
    public void doGenerateAnnouncementMessage(Announcement announcement) {
        Message message = messageDao.findMessageByTypeAndReferenceAndMemberId(Global.MessageType.ANNOUNCEMENT,
                announcement.getAnnounceId(), announcement.getMemberId());

        if (message == null) {
            message = new Message();

            message.setReference(announcement.getAnnounceId());
            message.setTitle(announcement.getTitle());
            message.setTitleCn(announcement.getTitleCn());
            message.setContent(announcement.getBody());
            message.setContentCn(announcement.getBodyCn());

            message.setMessageType(Global.MessageType.ANNOUNCEMENT);
            message.setSentDate(announcement.getPublishDate());
            message.setSender("SYSTEM");

            messageDao.save(message);
        }
    }

    @Override
    public void doUpdateAnnouncementMessage(Announcement announcement) {
        Message message = messageDao.findMessageByTypeAndReferenceAndMemberId(Global.MessageType.ANNOUNCEMENT, announcement.getAnnounceId(),
                announcement.getMemberId());

        if (message != null) {
            if (announcement.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) { //active change to inactive
                messageDao.delete(message);
            } else {
                message.setTitle(announcement.getTitle());
                message.setContent(announcement.getBody());

                message.setTitleCn(announcement.getTitleCn());
                message.setContentCn(announcement.getBodyCn());

                message.setSentDate(announcement.getPublishDate());

                messageDao.update(message);
            }
        } else { //inactive change to active
            if (announcement.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                message = new Message();

                message.setReference(announcement.getAnnounceId());
                message.setTitle(announcement.getTitle());
                message.setTitleCn(announcement.getTitleCn());
                message.setContent(announcement.getBody());
                message.setContentCn(announcement.getBodyCn());

                message.setMessageType(Global.MessageType.ANNOUNCEMENT);
                message.setSentDate(announcement.getPublishDate());
                message.setSender("SYSTEM");

                messageDao.save(message);
            }
        }
    }

    @Override
    public Message doGenerateNoticeMessage(String memberId, String title, String titleCn, String content, String contentCn) {
        Message message = messageDao.findMessageByTypeAndReferenceAndMemberId(Global.MessageType.ANNOUNCEMENT, null, memberId);
        if (message == null) { //only insert when record not exist
            message = new Message();
            message.setMemberId(memberId);
            message.setReference(memberId);
            message.setTitle(title);
            message.setTitleCn(titleCn);
            message.setContent(content);
            message.setContentCn(contentCn);
            message.setMessageType(Global.MessageType.ANNOUNCEMENT);
            message.setSentDate(new Date());
            message.setSender("SYSTEM");

            messageDao.save(message);
        }

        return message;
    }

    @Override
    public void doDeleteMessage(String receiverId, String messageId) {
        if (StringUtils.isNotBlank(messageId)) {
            Message message = messageDao.findMessageByReceiverIdAndMessageId(receiverId, messageId);

            if (message != null) {
                messageDao.delete(message);
            } else { // delete for announcement message (which member id is null)
                message = messageDao.get(messageId);
                if (message != null) {
                    messageAnnouncementDao.doDeleteAnnounceMessage(receiverId, message);
                }
            }
        } else { // delete all message for this member
            List<Message> messageList = messageDao.findAllMessageByReceiverId(receiverId);
            if (messageList.size() > 0) {
                messageDao.deleteAll(messageList);
            }

            List<Message> announcementMessageList = messageDao.findAllMessageByType(Global.MessageType.ANNOUNCEMENT);
            for (Message message : announcementMessageList) {
                messageAnnouncementDao.doDeleteAnnounceMessage(receiverId, message);
            }
        }
    }

    @Override
    public Message doGenerateMissionPrizeMessage(String memberId, VjMissionDetail vjMissionDetail) {
        I18n i18n = Application.lookupBean(I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.class);

        String vjProfileName = "";
        Member member = memberService.getMember(vjMissionDetail.getVjMission().getMemberId());
        if(member != null) {
            vjProfileName = member.getMemberDetail().getProfileName();
        }
        Message message = new Message();
        message.setMemberId(memberId);
        message.setTitle(i18n.getText("title.mission", Global.LOCALE_EN));
        message.setTitleCn(i18n.getText("title.mission", Global.LOCALE_CN));
        message.setContent(i18n.getText("message.missionCompleteMessage", Global.LOCALE_EN, vjMissionDetail.getName(), vjProfileName,vjMissionDetail.getPrize()));
        message.setContentCn(i18n.getText("message.missionCompleteMessage", Global.LOCALE_CN, vjProfileName, vjMissionDetail.getName(), vjMissionDetail.getPrize()));
        message.setMessageType(Global.MessageType.ANNOUNCEMENT);
        message.setSentDate(new Date());
        message.setSender("SYSTEM");

        messageDao.save(message);
        //}
        return message;
    }
}
