package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.SystemConfig;

public interface SystemConfigDao extends BasicDao<SystemConfig, String> {
    public static final String BEAN_NAME = "systemConfigDao";

    public SystemConfig getDefault();

    public SystemConfig findSystemConfigById(String configId);
}
