package com.compalsolutions.compal.general.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.general.vo.City;

import java.util.List;

public interface CityDao extends BasicDao<City, String> {
    public static final String BEAN_NAME = "cityDao";

    public List<City> getCityListByProvinceCode(String provinceCode);

    public City findCityByCityName(String cityName);
}
