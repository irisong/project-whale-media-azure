package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(BroadcastConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BroadcastConfigDaoImpl extends Jpa2Dao<BroadcastConfig, String> implements BroadcastConfigDao {

    public BroadcastConfigDaoImpl() {
        super(new BroadcastConfig(false));
    }

    @Override
    public void findBroadcastConfigForDatagrid(DatagridModel<BroadcastConfig> datagridModel, String rankSearch){
        String hql = "from a IN " + BroadcastConfig.class + " WHERE 1=1 ";

        List<Object> params = new ArrayList<Object>();

        if (StringUtils.isNotBlank(rankSearch)) {
            hql += " and a.rankName=? ";
            params.add(rankSearch);
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }
    @Override
    public BroadcastConfig checkBroadcast(BroadcastConfig broadcastConfig){
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + BroadcastConfig.class + " WHERE 1=1 ";


        hql += " and a.rankName=?";
        params.add(broadcastConfig.getRankName());

        return findFirst(hql, params.toArray());
    }

    @Override
    public BroadcastConfig findBroadcastConfigbyRank(String rankName){
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + BroadcastConfig.class + " WHERE 1=1 ";


        hql += " and a.rankName=?";
        params.add(rankName);

        return findFirst(hql, params.toArray());
    }

    @Override
    public List<BroadcastConfig> findAllBroadcastConfig(){
        return findByExample(new BroadcastConfig(false), "minDay");
    }

}
