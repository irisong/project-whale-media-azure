package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.Validate;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(BroadcastGuildMemberDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BroadcastGuildMemberDaoImpl extends Jpa2Dao<BroadcastGuildMember, String> implements BroadcastGuildMemberDao {

    public BroadcastGuildMemberDaoImpl() {
        super(new BroadcastGuildMember(false));
    }

    @Override
    public BroadcastGuildMember findGuildMemberApproved(String memberId) {
        Validate.notBlank(memberId);
        return findGuildMember(memberId, null, BroadcastGuildMember.STATUS_APPROVED);
    }

    @Override
    public BroadcastGuildMember findGuildMemberPending(String memberId, String guildId) {
        Validate.notBlank(memberId);
        Validate.notBlank(guildId);

        return findGuildMember(memberId, guildId, BroadcastGuildMember.STATUS_PENDING_APPROVAL);
    }

    @Override
    public BroadcastGuildMember findGuildMemberWithoutStatus(String memberId, String guildId) {
        BroadcastGuildMember example = new BroadcastGuildMember(false);
        example.setGuildId(guildId);
        example.setMemberId(memberId);
        return findUnique(example);
   }

    @Override
    public List<BroadcastGuildMember> getBroadcastGuildMembersPending(String memberId, String status) {
        BroadcastGuildMember example = new BroadcastGuildMember(false);
        example.setStatus(status);
        example.setMemberId(memberId);
        return findByExample(example);
    }

//    @Override
//    public BroadcastGuildMember findGuildMember(String memberId, String guildId) {
//        Validate.notBlank(memberId);
//        Validate.notBlank(guildId);
//
//        return findGuildMember(memberId, guildId, null);
//    }

    @Override
    public BroadcastGuildMember findGuildMember(String memberId, String guildId, String status) {
        BroadcastGuildMember example = new BroadcastGuildMember(false);

        if(StringUtils.isNotBlank(guildId))
            example.setGuildId(guildId);
        if(StringUtils.isNotBlank(memberId))
            example.setMemberId(memberId);
        if(StringUtils.isNotBlank(status))
            example.setStatus(status);

        return findUnique(example);
    }

//    @Override
//    public List<BroadcastGuildMember> findGuildMembersPendingByMember(String memberId) {
//        Validate.notBlank(memberId);
//
//        List<Object> params = new ArrayList<Object>();
//
//        String hql = " FROM s IN " + BroadcastGuildMember.class + " WHERE 1=1 AND s.status=?";
//
//        params.add(BroadcastGuildMember.STATUS_PENDING_APPROVAL);
//
//        return findQueryAsList(hql, params.toArray());
//    }

    @Override
    public void findAllBroadcastGuildMemberDataGrid(DatagridModel<BroadcastGuildMember> datagridModel, String guildId,
                                                    String status) {
        Validate.notBlank(guildId);

        List<Object> params = new ArrayList<Object>();

        String hql = " FROM s IN " + BroadcastGuildMember.class + " WHERE 1=1 AND guildId=?";

        params.add(guildId);

        if (StringUtils.isNotBlank(status)) {
            hql += " AND status=? ";
            params.add(status);
        }

        findForDatagrid(datagridModel, "s", hql, params.toArray());
    }

//    @Override
//    public int findTotalMembers(String guildId) {
//        String hql = "SELECT COUNT(m.memberId) FROM BroadcastGuildMember m WHERE m.guildId = '" + guildId + "' AND status = 'APPROVED'";
//
//        Long result = (Long) exFindFirst(hql);
//        if (result != null)
//            return result.intValue();
//
//        return 0;
//    }

}
