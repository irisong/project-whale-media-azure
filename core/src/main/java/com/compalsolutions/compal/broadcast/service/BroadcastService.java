package com.compalsolutions.compal.broadcast.service;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;

import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface BroadcastService {
    public static final String BEAN_NAME = "broadcastService";


    public void findBroadcastConfigForListing(DatagridModel<BroadcastConfig> datagridModel, String rankSearch);

    public void findGuildsForListing(DatagridModel<BroadcastGuild> datagridModel, String name);

    public boolean checkBroadcast(BroadcastConfig broadcastConfig);

    public void doSaveBroadcast(BroadcastConfig broadcastConfig);

    public BroadcastConfig getBroadcast(String id);

    public BroadcastGuild getBroadcastGuild(String id);

    public BroadcastGuildMember findGuildMemberApproved(String memberId);

    public void doUpdateGuildMember(String memberId, String guildId, String memberRank, Date dateFrom, Date dateTo, Boolean isHalfMonthStart, Boolean isHalfMonthEnd);

    public void updateBroadcast(BroadcastConfig broadcastConfig);

    public void saveBroadcastGuild(BroadcastGuild broadcastGuild, Agent agent, User user);

    public void doAddAgentBroadcastGuild(BroadcastGuild broadcastGuild, Agent agent, User user);

    public void doDeleteGuild(String id);

    public void doUpdateGuild(Locale locale, BroadcastGuild broadcastGuild);

    public List<BroadcastConfig> findAllBroadcastConfig();

    public void doAddDeleteGuildMember(String memberId, String guildId, String memberRank, boolean actionAdd,Date startDate,Date endDate,Boolean isHalfMonthStart,Boolean isHalfMonthEnd);

    public void findAllGuilds(DatagridModel<BroadcastGuild> datagridModel);

//    public List<BroadcastGuildMember> findMemberInPending(String memberId);

    public BroadcastGuildMember findMemberInPending(String memberId, String guildId);

    public String doAddDeleteGuildMemberFromAPI(String memberId, String guildId, boolean actionAdd);

    public void findAllBroadcastGuildMemberDatagrid(DatagridModel<BroadcastGuildMember> datagridModel, String guildId, String status);

    public BroadcastGuildMember getBroadcastGuildMember(BroadcastGuild broadcastGuild, String memberId, String status);

    public List<BroadcastGuildMember> getBroadcastGuildMembersApproved(BroadcastGuild broadcastGuild);

    public int findTotalMembers(BroadcastGuild broadcastGuild);

    public BroadcastGuildMember findGuildMemberWithoutStatus(String memberId,String guildId);

    public void doDeleteAllMemberPendingRequest(String memberId);

    public List<BroadcastGuildMember> getBroadcastGuildMembersPending(String memberId);

    public void doApproveMember(String memberId,String guildId,String memberRank,Date dateFrom,Date dateTo,Boolean isHalfMonthStart,Boolean isHalfMonthEnd);

    public List<String> findAllGuildMemberWithStatusApprove();

    public List<String> findGuildMemberWithStatusApproveGuildId(String guildId);

    public BroadcastGuild findBroadcastGuildByAgentId(String agentId);
}
