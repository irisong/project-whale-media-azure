package com.compalsolutions.compal.broadcast.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "bc_broadcast_guild_member")
@Access(AccessType.FIELD)
public class BroadcastGuildMember extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String STATUS_NEW = Global.Status.NEW;
    public static final String STATUS_APPROVED = Global.Status.APPROVED;
    public static final String STATUS_PENDING_APPROVAL = "PENDING_APPROVAL";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "guild_id", nullable = false, length = 32)
    private String guildId;

    @Column(name = "member_id", nullable = false, length = 32)
    private String memberId;

    @Column(name = "broadcast_config_id", length = 32)
    private String broadcastConfigId;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "joined_date")
    private Date joinedDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "served_start_date")
    private Date servedStartDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "served_end_date")
    private Date servedEndDate;

    @Column(name = "half_month_agreement_start")
    private Boolean halfMonthAgreementStart;

    @Column(name = "half_month_agreement_end")
    private Boolean halfMonthAgreementEnd;

    @ToUpperCase
    @ToTrim
    @Column(name = "status", length = 20, nullable = false)
    private String status;

    @Transient
    private String profileName;

    @Transient
    private String profileUrl;

    @Transient
    private int fans;

    public BroadcastGuildMember() {
    }

    public BroadcastGuildMember(boolean defaultValue) {
        if (defaultValue) {
            this.status = Global.Status.NEW;
            this.halfMonthAgreementStart = false;
            this.halfMonthAgreementEnd = false;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getJoinedDate() {
        return joinedDate;
    }

    public void setJoinedDate(Date joinedDate) {
        this.joinedDate = joinedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBroadcastConfigId() {
        return broadcastConfigId;
    }

    public void setBroadcastConfigId(String broadcastConfigId) {
        this.broadcastConfigId = broadcastConfigId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public int getFans() {
        return fans;
    }

    public void setFans(int fans) {
        this.fans = fans;
    }

    public Date getServedStartDate() {
        return servedStartDate;
    }

    public void setServedStartDate(Date servedStartDate) {
        this.servedStartDate = servedStartDate;
    }

    public Date getServedEndDate() {
        return servedEndDate;
    }

    public void setServedEndDate(Date servedEndDate) {
        this.servedEndDate = servedEndDate;
    }

    public Boolean getHalfMonthAgreementStart() {
        return halfMonthAgreementStart;
    }

    public void setHalfMonthAgreementStart(Boolean halfMonthAgreementStart) {
        this.halfMonthAgreementStart = halfMonthAgreementStart;
    }

    public Boolean getHalfMonthAgreementEnd() {
        return halfMonthAgreementEnd;
    }

    public void setHalfMonthAgreementEnd(Boolean halfMonthAgreementEnd) {
        this.halfMonthAgreementEnd = halfMonthAgreementEnd;
    }
}
