package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

public interface BroadcastGuildDao extends BasicDao<BroadcastGuild, String> {
    public static final String BEAN_NAME = "broadcastGuildDao";

    public void findGuildsForDatagrid(DatagridModel<BroadcastGuild> datagridModel, String name);

    public void findAllGuildsForList(DatagridModel<BroadcastGuild> datagridModel);

    public BroadcastGuild findBroadcastGuildbyDisplayId(String displayId);

    public BroadcastGuild findBroadcastGuildByAgentId(String agentId);
}
