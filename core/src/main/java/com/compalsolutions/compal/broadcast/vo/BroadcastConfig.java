package com.compalsolutions.compal.broadcast.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "bc_broadcast_config")
@Access(AccessType.FIELD)
public class BroadcastConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String RANK_V1 = "V1";
    public static final String RANK_V2 = "V2";
    public static final String RANK_A1 = "A1";
    public static final String RANK_A2 = "A2";
    public static final String RANK_A3 = "A3";
    public static final String RANK_B1 = "B1";
    public static final String RANK_B2 = "B2";
    public static final String RANK_B3 = "B3";
    public static final String RANK_NONE = "N";


    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @Column(name = "rank_name", length = 32, nullable = false) //'rank' is reserved word cannot be column name
    private String rankName;

    @Column(name = "basic_salary", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal basicSalary;

    //in minutes
    @Column(name = "min_duration")
    private Integer minDuration;

    //in days
    @Column(name = "min_day")
    private Integer minDay;

    //minimum point earned
    @Column(name = "min_point", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal minPoint;

    @Column(name = "split_percentage", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal splitPercentage;

    @Column(name = "sequence")
    private Integer sequence;

    public BroadcastConfig() {
    }

    public BroadcastConfig(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRankName() {
        return rankName;
    }

    public void setRankName(String rankName) {
        this.rankName = rankName;
    }

    public BigDecimal getBasicSalary() {
        return basicSalary;
    }

    public void setBasicSalary(BigDecimal basicSalary) {
        this.basicSalary = basicSalary;
    }

    public Integer getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(Integer minDuration) {
        this.minDuration = minDuration;
    }

    public Integer getMinDay() {
        return minDay;
    }

    public void setMinDay(Integer minDay) {
        this.minDay = minDay;
    }

    public BigDecimal getMinPoint() {
        return minPoint;
    }

    public void setMinPoint(BigDecimal minPoint) {
        this.minPoint = minPoint;
    }

    public BigDecimal getSplitPercentage() {
        return splitPercentage;
    }

    public void setSplitPercentage(BigDecimal splitPercentage) {
        this.splitPercentage = splitPercentage;
    }

    public Integer getSequence() {
        return sequence;
    }

    public void setSequence(Integer sequence) {
        this.sequence = sequence;
    }

    @Override
    public String toString() {
        return "BroadcastConfig{" +
                "id='" + id + '\'' +
                ", rankName='" + rankName + '\'' +
                ", basicSalary=" + basicSalary +
                ", minDuration=" + minDuration +
                ", minDay=" + minDay +
                ", minPoint=" + minPoint +
                ", splitPercentage=" + splitPercentage +
                '}';
    }
}
