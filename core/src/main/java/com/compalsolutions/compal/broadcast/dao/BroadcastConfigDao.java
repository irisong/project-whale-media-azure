package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.rank.vo.RankConfig;

import java.util.Date;
import java.util.List;

public interface BroadcastConfigDao extends BasicDao<BroadcastConfig, String> {
    public static final String BEAN_NAME = "broadcastConfigDao";

    public void findBroadcastConfigForDatagrid(DatagridModel<BroadcastConfig> datagridModel, String rankSearch);

    public BroadcastConfig checkBroadcast(BroadcastConfig broadcastConfig);

    public BroadcastConfig findBroadcastConfigbyRank(String rankName);

    public List<BroadcastConfig> findAllBroadcastConfig();
}
