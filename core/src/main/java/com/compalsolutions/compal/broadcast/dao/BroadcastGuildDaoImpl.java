package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component(BroadcastGuildDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BroadcastGuildDaoImpl extends Jpa2Dao<BroadcastGuild, String> implements BroadcastGuildDao {

    public BroadcastGuildDaoImpl() {
        super(new BroadcastGuild(false));
    }

    @Override
    public void findGuildsForDatagrid(DatagridModel<BroadcastGuild> datagridModel, String name) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + BroadcastGuild.class + " WHERE 1=1 ";

        if (StringUtils.isNotBlank(name)) {
            hql += " and a.name=? ";
            params.add(name);
        }
        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public void findAllGuildsForList(DatagridModel<BroadcastGuild> datagridModel) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + BroadcastGuild.class + " WHERE 1=1 ";


        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public BroadcastGuild findBroadcastGuildbyDisplayId(String displayId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + BroadcastGuild.class + " WHERE 1=1 ";


        hql += " and a.displayId=?";
        params.add(displayId);

        return findFirst(hql, params.toArray());
    }

    @Override
    public BroadcastGuild findBroadcastGuildByAgentId(String agentId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + BroadcastGuild.class + " WHERE 1=1 ";


        hql += " and a.agentId=?";
        params.add(agentId);

        return findFirst(hql, params.toArray());
    }
}
