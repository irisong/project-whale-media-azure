package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointTrx;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public interface BroadcastGuildMemberSqlDao {
    public static final String BEAN_NAME = "broadcastGuildMemberSqlDao";

    public List<String> findAllGuildMemberWithStatusApprove();

    public List<String> findGuildMemberWithStatusApproveGuildId(String guildId);
}

