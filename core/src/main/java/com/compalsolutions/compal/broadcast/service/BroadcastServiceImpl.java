package com.compalsolutions.compal.broadcast.service;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.GuildFileUploadConfiguration;
import com.compalsolutions.compal.agent.dao.AgentDao;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.broadcast.dao.*;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.DataException;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.dao.UserDetailsRoleDao;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.member.dao.MemberDao;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.telegram.BotClient;
import com.compalsolutions.compal.user.dao.AgentUserDao;
import com.compalsolutions.compal.user.dao.UserDao;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.UniqueNumberGenerator;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Component(BroadcastService.BEAN_NAME)
public class BroadcastServiceImpl implements BroadcastService {

    private static final Log log = LogFactory.getLog(BroadcastServiceImpl.class);

    @Autowired
    private BroadcastConfigDao broadcastConfigDao;

    @Autowired
    private BroadcastGuildDao broadcastGuildDao;

    @Autowired
    private MemberDao memberDao;

    @Autowired
    private AgentDao agentDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserDetailsRoleDao userDetailsRoleDao;

    @Autowired
    private AgentUserDao agentUserDao;

    @Autowired
    private BroadcastGuildMemberDao broadcastGuildMemberDao;

    @Autowired
    private BroadcastGuildMemberSqlDao broadcastGuildMemberSqlDao;

    @Override
    public void findBroadcastConfigForListing(DatagridModel<BroadcastConfig> datagridModel, String rankSearch) {
        broadcastConfigDao.findBroadcastConfigForDatagrid(datagridModel, rankSearch);

    }

    @Override
    public void findGuildsForListing(DatagridModel<BroadcastGuild> datagridModel, String name) {
        GuildFileUploadConfiguration config = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);
        broadcastGuildDao.findGuildsForDatagrid(datagridModel, name);
        for (BroadcastGuild broadcastGuild : datagridModel.getRecords()) {
            String fileUrl = broadcastGuild.getFileUrlWithParentPath(config.getFullGuildParentFileUrl());
            broadcastGuild.setFileUrl(fileUrl);
        }
    }

    @Override
    public boolean checkBroadcast(BroadcastConfig broadcastConfig) {
        BroadcastConfig bcReturn = broadcastConfigDao.checkBroadcast(broadcastConfig);
        if (bcReturn != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void doSaveBroadcast(BroadcastConfig broadcastConfig) {
        broadcastConfigDao.save(broadcastConfig);

    }

    @Override
    public BroadcastConfig getBroadcast(String id) {
        return broadcastConfigDao.get(id);
    }

    @Override
    public BroadcastGuild getBroadcastGuild(String id) {
        return broadcastGuildDao.get(id);
    }

    @Override
    public BroadcastGuildMember findGuildMemberApproved(String memberId) {
        return broadcastGuildMemberDao.findGuildMemberApproved(memberId);
    }

    @Override
    public void doUpdateGuildMember(String memberId, String guildId, String memberRank, Date dateFrom, Date dateTo, Boolean isHalfMonthStart, Boolean isHalfMonthEnd) {
        BroadcastGuildMember dbBroadcastGuildMember = broadcastGuildMemberDao.findGuildMemberWithoutStatus(memberId, guildId);

        dbBroadcastGuildMember.setBroadcastConfigId(memberRank);
        dbBroadcastGuildMember.setServedStartDate(dateFrom);
        dbBroadcastGuildMember.setServedEndDate(dateTo);
        dbBroadcastGuildMember.setHalfMonthAgreementStart(isHalfMonthStart);
        dbBroadcastGuildMember.setHalfMonthAgreementEnd(isHalfMonthEnd);

        broadcastGuildMemberDao.update(dbBroadcastGuildMember);
    }

    @Override
    public void updateBroadcast(BroadcastConfig broadcastConfig) {
        BroadcastConfig broadcastConfigDb = broadcastConfigDao.get(broadcastConfig.getId());

        broadcastConfigDb.setRankName(broadcastConfig.getRankName());
        broadcastConfigDb.setBasicSalary(broadcastConfig.getBasicSalary());
        broadcastConfigDb.setMinDay(broadcastConfig.getMinDay());
        broadcastConfigDb.setMinDuration(broadcastConfig.getMinDuration());
        broadcastConfigDb.setMinPoint(broadcastConfig.getMinPoint());
        broadcastConfigDb.setSplitPercentage(broadcastConfig.getSplitPercentage());
        broadcastConfigDb.setSequence(broadcastConfig.getSequence());
        broadcastConfigDao.update(broadcastConfigDb);
    }

    @Override
    public void saveBroadcastGuild(BroadcastGuild broadcastGuild, Agent agent, User user) {
        GuildFileUploadConfiguration config = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        final String uploadPath = config.getGuildFileUploadPath();
        //get random whale live id (must be unique)
        BroadcastGuild guild;

        UniqueNumberGenerator generator = null;
        do {
            generator = new UniqueNumberGenerator(10, true, false, UniqueNumberGenerator.IS_NUMERIC);

            guild = broadcastGuildDao.findBroadcastGuildbyDisplayId(generator.getNewPin());

        } while (guild != null);

        String displayId = generator.getNewPin();

        agent.setAgentCode(displayId);
        agentDao.save(agent);

        broadcastGuild.setAgentId(agent.getAgentId());
        broadcastGuild.setDisplayId(displayId);
        broadcastGuildDao.save(broadcastGuild);

        AgentUser agentUser = new AgentUser(true);
        agentUser.setAgentId(agent.getAgentId());
        agentUser.setSuperUser(false);
        agentUser.setUserId(user.getUserId());
        agentUser.setCompId(broadcastGuild.getId());
        agentUser.setUsername(user.getUsername());
        agentUser.setPassword(user.getPassword());
        agentUser.setPassword2(user.getPassword2());
        agentUser.setStatus(user.getStatus());
        agentUser.setUserType(user.getUserType());

        UserRole userRole = userDetailsRoleDao.findUserRoleByRoleName(Global.DEFAULT_AGENT, Global.UserRoleGroup.AGENT_GROUP);

        if (userRole != null) {
            agentUser.getUserRoles().add(userRole);
        }

        agentUserDao.save(agentUser);


        if (StringUtils.isNotBlank(broadcastGuild.getFilename())) {
            broadcastGuild.setFilename(broadcastGuild.getFilename());
            broadcastGuild.setContentType(broadcastGuild.getContentType());
            broadcastGuild.setFileSize(broadcastGuild.getFileSize());
            broadcastGuildDao.update(broadcastGuild);

            if (!isProdServer) {
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
            }
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(broadcastGuild.getFileUpload().getAbsolutePath()));

                if (isProdServer) {
                    azureBlobStorageService.uploadFileToAzureBlobStorage(broadcastGuild.getRenamedFilename(), null,
                            null, broadcastGuild.getContentType(), GuildFileUploadConfiguration.FOLDER_NAME,
                            bytes, broadcastGuild.getFileSize());
                } else {
                    Path path = Paths.get(uploadPath, broadcastGuild.getRenamedFilename());
                    Files.write(path, bytes);
                }
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        }
    }

    @Override
    public void doAddAgentBroadcastGuild(BroadcastGuild broadcastGuild, Agent agent, User user) {
        agent.setAgentCode(broadcastGuild.getDisplayId());
        agentDao.save(agent);

        broadcastGuild.setAgentId(agent.getAgentId());
        broadcastGuildDao.save(broadcastGuild);

        AgentUser agentUser = new AgentUser(true);
        agentUser.setAgentId(agent.getAgentId());
        agentUser.setSuperUser(false);
        agentUser.setUserId(user.getUserId());
        agentUser.setCompId(broadcastGuild.getId());
        agentUser.setUsername(user.getUsername());
        agentUser.setPassword(user.getPassword());
        agentUser.setPassword2(user.getPassword2());
        agentUser.setStatus(user.getStatus());
        agentUser.setUserType(user.getUserType());

        UserRole userRole = userDetailsRoleDao.findUserRoleByRoleName(Global.DEFAULT_AGENT, Global.UserRoleGroup.AGENT_GROUP);

        if (userRole != null) {
            agentUser.getUserRoles().add(userRole);
        }

        agentUserDao.save(agentUser);
    }

    @Override
    public void doDeleteGuild(String id) {
        try {
            BroadcastGuild broadcastGuild = broadcastGuildDao.get(id);
            GuildFileUploadConfiguration config = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);
            if (broadcastGuild != null) {
                broadcastGuild.setFileUrlWithParentPath(config.getUploadPath());
                Files.deleteIfExists(Paths.get(broadcastGuild.getFileUrl()));
                List<BroadcastGuildMember> broadcastGuildMembers = broadcastGuild.getGuildMembers();
                broadcastGuildMemberDao.deleteAll(broadcastGuildMembers);

                AgentUser agentUser = agentUserDao.findAgentUserByAgentId(broadcastGuild.getAgentId());
                agentUser.setStatus(Global.Status.INACTIVE);

                agentUserDao.save(agentUser);

            }
            broadcastGuildDao.delete(broadcastGuild);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doUpdateGuild(Locale locale, BroadcastGuild broadcastGuild) {
        GuildFileUploadConfiguration config = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        final String uploadPath = config.getGuildFileUploadPath();
        BroadcastGuild dbBroadcastGuild = broadcastGuildDao.get(broadcastGuild.getId());
        if (dbBroadcastGuild == null)
            throw new DataException(i18n.getText("invalidGuild", locale));

        dbBroadcastGuild.setName(broadcastGuild.getName());
        dbBroadcastGuild.setDescription(broadcastGuild.getDescription());
        dbBroadcastGuild.setStatus(broadcastGuild.getStatus());
        dbBroadcastGuild.setPresident(broadcastGuild.getPresident());

        if (StringUtils.isNotBlank(broadcastGuild.getFilename())) {
            dbBroadcastGuild.setFilename(broadcastGuild.getFilename());
            dbBroadcastGuild.setContentType(broadcastGuild.getContentType());
            dbBroadcastGuild.setFileSize(broadcastGuild.getFileSize());

            if (!isProdServer) {
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
            }
            try {
                byte[] bytes = Files.readAllBytes(Paths.get(broadcastGuild.getFileUpload().getAbsolutePath()));

                if (isProdServer) {
                    azureBlobStorageService.uploadFileToAzureBlobStorage(broadcastGuild.getRenamedFilename(), null,
                            null, broadcastGuild.getContentType(), GuildFileUploadConfiguration.FOLDER_NAME,
                            bytes, broadcastGuild.getFileSize());
                } else {
                    Path path = Paths.get(uploadPath, broadcastGuild.getRenamedFilename());
                    Files.write(path, bytes);
                }
            } catch (IOException e) {
                throw new SystemErrorException(e.getMessage(), e);
            }
        }
        broadcastGuildDao.update(dbBroadcastGuild);
    }


    @Override
    public List<BroadcastConfig> findAllBroadcastConfig() {
        return broadcastConfigDao.findAllBroadcastConfig();
    }

    @Override
    public void doAddDeleteGuildMember(String memberId, String guildId, String memberRank, boolean actionAdd, Date startDate, Date endDate, Boolean isHalfMonthStart, Boolean isHalfMonthEnd) {
        MemberService memberService = Application.lookupBean(MemberService.class);
        Member member = memberService.getMember(memberId);
        BroadcastGuild broadcastGuild = broadcastGuildDao.get(guildId);
        BroadcastConfig broadcastConfig = broadcastConfigDao.get(memberRank);
        if (member == null) {
            throw new ValidatorException("invalid member");
        }
        if (actionAdd) {
            if (broadcastConfig == null) {
                throw new ValidatorException("invalid broadcastconfig");
            }
            if (broadcastGuild == null) {
                throw new ValidatorException("invalid broadcastGuild");
            }

            BroadcastGuildMember broadcastGuildMember = new BroadcastGuildMember(true);
            broadcastGuildMember.setMemberId(memberId);
            broadcastGuildMember.setGuildId(broadcastGuild.getId());
            broadcastGuildMember.setBroadcastConfigId(broadcastConfig.getId());
            // if added by admin, no need pending for approval, direct activate
            broadcastGuildMember.setStatus(Global.Status.APPROVED);
            broadcastGuildMember.setJoinedDate(new Date());
            broadcastGuildMember.setServedStartDate(startDate);
            broadcastGuildMember.setServedEndDate(endDate);
            broadcastGuildMember.setHalfMonthAgreementStart(isHalfMonthStart);
            broadcastGuildMember.setHalfMonthAgreementEnd(isHalfMonthEnd);
            broadcastGuildMemberDao.save(broadcastGuildMember);

            doDeleteAllMemberPendingRequest(memberId);
        } else {
            BroadcastGuildMember broadcastGuildMember = findGuildMemberWithoutStatus(memberId, guildId);
            if (broadcastGuildMember != null) {
                broadcastGuildMemberDao.delete(broadcastGuildMember);
            }
        }
    }

    @Override
    public void doDeleteAllMemberPendingRequest(String memberId) {
        List<BroadcastGuildMember> broadcastGuildMemberList;
        broadcastGuildMemberList = broadcastGuildMemberDao.getBroadcastGuildMembersPending(memberId, BroadcastGuildMember.STATUS_PENDING_APPROVAL);
        if (broadcastGuildMemberList != null) {
            broadcastGuildMemberDao.deleteAll(broadcastGuildMemberList);
            broadcastGuildMemberList.clear();
        }
    }

    @Override
    public void findAllGuilds(DatagridModel<BroadcastGuild> datagridModel) {

        broadcastGuildDao.findAllGuildsForList(datagridModel);
    }


//    @Override
//    public List<BroadcastGuildMember> findMemberInPending(String memberId) {
//        return broadcastGuildMemberDao.findGuildMembersPendingByMember(memberId);
//    }

    @Override
    public BroadcastGuildMember findMemberInPending(String memberId, String guildId) {
        return broadcastGuildMemberDao.findGuildMemberPending(memberId, guildId);
    }

    @Override
    public void findAllBroadcastGuildMemberDatagrid(DatagridModel<BroadcastGuildMember> datagridModel, String guildId, String status) {
        broadcastGuildMemberDao.findAllBroadcastGuildMemberDataGrid(datagridModel, guildId, status);
    }

    @Override
    public BroadcastGuildMember getBroadcastGuildMember(BroadcastGuild broadcastGuild, String memberId, String status) {
        return broadcastGuild.getGuildMembers().stream()
                .filter(g -> (status == null || g.getStatus().equalsIgnoreCase(status))
                        && (memberId == null || memberId.equalsIgnoreCase(g.getMemberId())))
                .findAny()
                .orElse(null);
    }

    @Override
    public List<BroadcastGuildMember> getBroadcastGuildMembersApproved(BroadcastGuild broadcastGuild) {
        return broadcastGuild.getGuildMembers().stream()
                .filter(g -> g.getStatus().equalsIgnoreCase(BroadcastGuildMember.STATUS_APPROVED))
                .collect(Collectors.toList());
    }

    @Override
    public List<BroadcastGuildMember> getBroadcastGuildMembersPending(String memberId) {
        return broadcastGuildMemberDao.getBroadcastGuildMembersPending(memberId, BroadcastGuildMember.STATUS_PENDING_APPROVAL);
    }

    @Override
    public int findTotalMembers(BroadcastGuild broadcastGuild) {
        return getBroadcastGuildMembersApproved(broadcastGuild).size();
    }

    @Override
    public String doAddDeleteGuildMemberFromAPI(String memberId, String guildId, boolean actionAdd) {
        MemberService memberService = Application.lookupBean(MemberService.class);
        Member member = memberService.getMember(memberId);
        BroadcastGuild broadcastGuild = broadcastGuildDao.get(guildId);
        BroadcastConfig broadcastConfig = broadcastConfigDao.findBroadcastConfigbyRank(BroadcastConfig.RANK_NONE); //default N rank
        MemberDetail memberDetail = memberService.findMemberDetailByMemberId(memberId);
        if (member == null) {
            throw new ValidatorException("invalid member");
        }
        if (actionAdd) {
            if (broadcastConfig == null) {
                throw new ValidatorException("invalid broadcastconfig");
            }
            if (broadcastGuild == null) {
                throw new ValidatorException("invalid broadcastGuild");
            }

            BroadcastGuildMember broadcastGuildMember = new BroadcastGuildMember(true);
            broadcastGuildMember.setMemberId(memberId);
            broadcastGuildMember.setGuildId(broadcastGuild.getId());
            broadcastGuildMember.setBroadcastConfigId(broadcastConfig.getId());
            broadcastGuildMember.setStatus(BroadcastGuildMember.STATUS_PENDING_APPROVAL);
            broadcastGuildMember.setJoinedDate(null);
            broadcastGuildMemberDao.save(broadcastGuildMember);

            String msg = member.getMemberCode() + " [" + memberDetail.getProfileName() + "] has requested to join guild [" + broadcastGuild.getName() + "]";
            BotClient botClient = new BotClient();
            botClient.doSendReportMessage(msg);


        } else {
            BroadcastGuildMember broadcastGuildMemberPending = broadcastGuildMemberDao.findGuildMemberPending(memberId, guildId);
            BroadcastGuildMember broadcastGuildMemberApproved = broadcastGuildMemberDao.findGuildMemberApproved(memberId);
            if (broadcastGuildMemberPending != null) {
                broadcastGuildMemberDao.delete(broadcastGuildMemberPending);
                return "unApplyGuildSuccessfully";
            } else if (broadcastGuildMemberApproved != null) {
                broadcastGuildMemberDao.delete(broadcastGuildMemberApproved);
                return "unSigningGuildSuccessfully";
            }
            return "invalid broadcastGuild";
        }
        return "";
    }

    @Override
    public void doApproveMember(String memberId, String guildId, String memberRank, Date dateFrom, Date dateTo, Boolean isHalfMonthStart, Boolean isHalfMonthEnd) {
        BroadcastGuildMember example = broadcastGuildMemberDao.findGuildMemberWithoutStatus(memberId, guildId);
        example.setStatus(BroadcastGuildMember.STATUS_APPROVED);
        example.setBroadcastConfigId(memberRank);
        example.setJoinedDate(new Date());
        example.setServedStartDate(dateFrom);
        example.setServedEndDate(dateTo);
        example.setHalfMonthAgreementStart(isHalfMonthStart);
        example.setHalfMonthAgreementEnd(isHalfMonthEnd);
        broadcastGuildMemberDao.save(example);

        doDeleteAllMemberPendingRequest(memberId);
    }

    @Override
    public BroadcastGuildMember findGuildMemberWithoutStatus(String memberId, String guildId) {
        return broadcastGuildMemberDao.findGuildMemberWithoutStatus(memberId, guildId);
    }

    @Override
    public List<String> findAllGuildMemberWithStatusApprove() {
        return broadcastGuildMemberSqlDao.findAllGuildMemberWithStatusApprove();
    }

    @Override
    public List<String> findGuildMemberWithStatusApproveGuildId(String guildId) {
        return broadcastGuildMemberSqlDao.findGuildMemberWithStatusApproveGuildId(guildId);
    }

    @Override
    public BroadcastGuild findBroadcastGuildByAgentId(String id) {
        return broadcastGuildDao.findBroadcastGuildByAgentId(id);
    }
}
