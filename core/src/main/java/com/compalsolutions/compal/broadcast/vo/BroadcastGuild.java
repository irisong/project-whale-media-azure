package com.compalsolutions.compal.broadcast.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "bc_broadcast_guild")
@Access(AccessType.FIELD)
public class BroadcastGuild extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "id", unique = true, nullable = false, length = 32)
    private String id;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "guild_id", insertable = false, updatable = false, nullable = true)
    private List<BroadcastGuildMember> guildMembers = new ArrayList<>();

    @Column(name = "name", columnDefinition = Global.ColumnDef.TEXT)
    private String name;

    @Column(name = "president", columnDefinition = Global.ColumnDef.TEXT)
    private String president;

    @Column(name = "description", columnDefinition = Global.ColumnDef.TEXT)
    private String description;

    @ToTrim
    @Column(name = "status", length = 35, nullable = false)
    private String status;

    @ToTrim
    @Column(name = "filename", length = 100, nullable = false)
    private String filename;

    @ToTrim
    @Column(name = "display_id", length = 10)
    private String displayId;

    @ToTrim
    @Column(name = "content_type", length = 100, nullable = false)
    private String contentType;

    @Basic(fetch = FetchType.LAZY)
    @Column(name = "data", columnDefinition = Global.ColumnDef.LONG_BLOB)
    private byte[] data;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "path", columnDefinition = "text")
    private String path;

    @Column(name = "agent_id", unique = true, nullable = false, length = 32)
    private String agentId;

    @Transient
    private String fileUrl;

    @Transient
    private String renamedFilename;

    @Transient
    private String tempId;

    @Transient
    private File fileUpload;

    @Transient
    private String agentUserId;

    public BroadcastGuild() {
    }

    public BroadcastGuild(boolean defaultValue) {
        if (defaultValue) {
            this.status = Global.Status.ACTIVE;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public void setRenamedFilename(String renamedFilename) {
        this.renamedFilename = renamedFilename;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public List<BroadcastGuildMember> getGuildMembers() {
        return guildMembers;
    }

    public void setGuildMembers(List<BroadcastGuildMember> guildMembers) {
        this.guildMembers = guildMembers;
    }

    public void setFileUrlWithParentPath(String parentPath) {
        fileUrl = parentPath + "/" + getRenamedFilename();
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public String getFileUrlWithParentPath(String parentPath) {
        return parentPath + "/" + getRenamedFilename();
    }

    public String getRenamedFilename() {
        if (StringUtils.isBlank(renamedFilename)) {
            if (StringUtils.isNotBlank(id) && StringUtils.isNotBlank(filename)) {
                String[] ss = StringUtils.split(filename, ".");
                String fileExtension = ss[ss.length - 1];
                renamedFilename = id + "." + fileExtension;
            }
        }

        return renamedFilename;
    }

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president = president;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentUserId() {
        return agentUserId;
    }

    public void setAgentUserId(String agentUserId) {
        this.agentUserId = agentUserId;
    }
}
