package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.point.dao.PointTrxSqlDao;
import com.compalsolutions.compal.point.dto.PointBalanceDto;
import com.compalsolutions.compal.point.dto.PointReportDto;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component(BroadcastGuildMemberSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class BroadcastGuildMemberSqlDaoImpl extends AbstractJdbcDao implements BroadcastGuildMemberSqlDao{

    @Override
    public List<String> findAllGuildMemberWithStatusApprove() {
        List<Object> params = new ArrayList<>();
        String sql = "select distinct(member_id) memberId from bc_broadcast_guild_member where status =?";
        params.add(Global.Status.APPROVED);
        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("memberId");
            }
        }, params.toArray());
    }

    @Override
    public List<String> findGuildMemberWithStatusApproveGuildId(String guildId) {
        List<Object> params = new ArrayList<>();
        String sql = "select distinct(member_id) memberId from bc_broadcast_guild_member where status =? ";
        params.add(Global.Status.APPROVED);

        sql+=" and guild_id =? ";
        params.add(guildId);

        return query(sql, new RowMapper<String>() {
            public String mapRow(ResultSet rs, int arg1) throws SQLException {
                return rs.getString("memberId");
            }
        }, params.toArray());
    }

}


