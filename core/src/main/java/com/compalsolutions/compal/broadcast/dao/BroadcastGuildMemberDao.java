package com.compalsolutions.compal.broadcast.dao;

import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;

import java.util.List;

public interface BroadcastGuildMemberDao extends BasicDao<BroadcastGuildMember, String> {
    public static final String BEAN_NAME = "broadcastGuildMemberDao";

    public BroadcastGuildMember findGuildMemberApproved(String memberId);

    public BroadcastGuildMember findGuildMemberPending(String memberId, String guildId);

    public BroadcastGuildMember findGuildMemberWithoutStatus(String memberId, String guildId);

    public List<BroadcastGuildMember> getBroadcastGuildMembersPending(String memberId, String status);

    public BroadcastGuildMember findGuildMember(String memberId, String guildId, String status);

//    public BroadcastGuildMember findBroadcastGuildMemberByGuildId(String guildId);

//    public List<BroadcastGuildMember> findGuildMembersPendingByMember(String memberId);

    public void findAllBroadcastGuildMemberDataGrid(DatagridModel<BroadcastGuildMember> datagridModel, String guildId,
                                                    String status);

//    public int findTotalMembers(String guildId);
}
