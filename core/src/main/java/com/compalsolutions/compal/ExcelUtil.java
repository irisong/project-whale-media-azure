package com.compalsolutions.compal;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.Cell;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA. User: jason Date: Sep 25, 2008 Time: 11:02:11 AM To change this template use File |
 * Settings | File Templates.
 */
public class ExcelUtil {
	public static String readCell(HSSFCell cell) throws Exception {
		String ret = "";
		try {

			if (cell != null) {
				switch (cell.getCellType()) {
				case HSSFCell.CELL_TYPE_NUMERIC:
					ret = (cell.getNumericCellValue() + "").trim();
					break;

				case HSSFCell.CELL_TYPE_STRING:
					ret = StringUtils.trim(cell.getRichStringCellValue().getString());
					break;
				}
			}
		} catch (Exception e) {
			throw new Exception("error at cell " + cell.getCellNum());
		}
		return ret;
	}

	public static HSSFWorkbook createWorkbook() {
		return createWorkbook(1);
	}

	public static HSSFWorkbook createWorkbook(int numOfSheet) {
		HSSFWorkbook workbook = new HSSFWorkbook();
		for (int i = 0; i < numOfSheet; i++) {
			workbook.createSheet();
		}
		return workbook;
	}

	public static Date readDateCellToDate(HSSFCell cell) throws Exception {
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		return df.parse(readDateCell(cell));
	}

	public static double readDoubleCellToDouble(HSSFCell cell) throws Exception {
		try {
			return Double.parseDouble(readCell(cell));
		} catch (Exception ex) {
			throw new Exception("error at cell " + cell.getCellNum());
		}
	}

	public static String readTimeCell(HSSFCell cell) throws Exception {
		String ret = "";
		SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
		try {
			if (cell != null) {
				ret = df.format(cell.getDateCellValue());
			}
		} catch (Exception e) {
			throw new Exception("error at cell " + cell.getCellNum());
		}
		return ret;
	}

	public static String readDateCell(HSSFCell cell) throws Exception {
		String ret = "";
		SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
		try {
			if (cell != null) {
				ret = df.format(cell.getDateCellValue());
			}
		} catch (Exception e) {
			throw new Exception("error at cell " + cell.getCellNum());
		}
		return ret;
	}

	public static boolean isValidExcelContentType(String contentType) {
		return contentType.equals("application/msexcell") || contentType.equals("application/vnd.ms-excel");
	}

	public static void exportToExcel(HttpServletResponse response, HSSFWorkbook workbook, String fileName)
			throws Exception {
		response.setContentType("application/vnd.ms-excel");
		response.setHeader("Content-Disposition", "attachment; filename=" + fileName);

		ServletOutputStream servletOutputStream = response.getOutputStream();

		workbook.write(servletOutputStream);
		servletOutputStream.close();
	}

	private static void createGenaralCell(HSSFRow row, int cellIndex, String value, int cellType) {
		HSSFCell cell = row.createCell((short) cellIndex);
		cell.setCellType(cellType);
		cell.setCellValue(new HSSFRichTextString(value));
		// cell.setCellValue(value);
	}

	public static void createStringCell(HSSFRow row, int cellIndex, String value) {
		createGenaralCell(row, cellIndex, value, HSSFCell.CELL_TYPE_STRING);
	}

	public static void createStringCell(HSSFRow row, int cellIndex, String value, HSSFCellStyle hssfCellStyle) {
		createGenaralCell4StyleAndFont(row, cellIndex, value, HSSFCell.CELL_TYPE_STRING, hssfCellStyle);
	}

	private static void createGenaralCell4StyleAndFont(HSSFRow row, int cellIndex, String value, int cellType,
			HSSFCellStyle hssfCellStyle) {
		HSSFCell cell = row.createCell((short) cellIndex);
		cell.setCellType(cellType);
		cell.setCellValue(new HSSFRichTextString(value));
		cell.setCellStyle(hssfCellStyle);
		// cell.setCellValue(value);
	}

	public static void setAutoSizeColumn(HSSFSheet sheet, int numOfColumns) {
		for (int i = 0; i < numOfColumns; i++) {
			sheet.autoSizeColumn((short) i);
		}
	}
	public static void setAutoSizeColumn(HSSFSheet sheet, int from, int to) {
		for (int i = from; i <= to; i++) {
			sheet.autoSizeColumn((short) i);
		}
	}
    public static Integer getColumnIdx(String columnIdx) {
        String[] anArray = {
                "A", "B", "C", "D", "E", "F", "G", "H", "I"
                , "J", "K", "L", "M", "N", "O", "P", "Q", "R"
                , "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB"
        };

        for (int i = 0; i < anArray.length; i++) {
            if (anArray[i].equals(columnIdx))
                return i;
        }
        return null;
    }
}