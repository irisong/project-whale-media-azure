package com.compalsolutions.compal.crypto;

public interface CryptoProvider {
    public static final String BEAN_NAME = "cryptoProvider";

    public boolean isProductionMode();

    public boolean isFullAmount();

    public String getServerUrl();

    public String getFileUploadUrl();

}
