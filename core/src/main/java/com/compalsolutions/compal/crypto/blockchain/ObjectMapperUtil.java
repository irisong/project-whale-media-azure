package com.compalsolutions.compal.crypto.blockchain;

import java.io.IOException;

import com.compalsolutions.compal.exception.ValidatorException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperUtil {
    public static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return mapper;
    }

    public static String readMessage(String jsonString) {

        ObjectMapper mapper = getObjectMapper();
        try {
            JsonNode tree = mapper.readTree(jsonString);

            return tree.get("message").textValue();
        } catch (IOException e) {
            throw new ValidatorException(e.getMessage());
        }
    }
}
