package com.compalsolutions.compal.crypto;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.compalsolutions.compal.function.language.service.I18n;

@Service(CryptoProvider.BEAN_NAME)
public class CryptoProviderImpl implements CryptoProvider {
    private static final Log log = LogFactory.getLog(CryptoProviderImpl.class);

    @SuppressWarnings("unused")
    private I18n i18n;
    private boolean productionMode;
    private boolean fullAmount;
    private String serverUrl;
    private String fileUploadUrl;

    @Autowired
    public CryptoProviderImpl(Environment env, I18n i18n) {
        productionMode = env.getProperty("crypto.production", Boolean.class, true);
        fullAmount = env.getProperty("crypto.fullAmount", Boolean.class, true);
        serverUrl = env.getProperty("serverUrl");
        fileUploadUrl = env.getProperty("fileServerUrl");
        this.i18n = i18n;
    }

    @Override
    public boolean isProductionMode() {
        return productionMode;
    }

    @Override
    public boolean isFullAmount() {
        return fullAmount;
    }

    @Override
    public String getServerUrl() {
        return serverUrl;
    }

    @Override
    public String getFileUploadUrl() {
        return fileUploadUrl;
    }
}
