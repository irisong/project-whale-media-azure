package com.compalsolutions.compal.rank.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.ss.formula.functions.Rank;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component(RankConfigDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class RankConfigDaoImpl extends Jpa2Dao<RankConfig, String> implements RankConfigDao {

    public RankConfigDaoImpl() {
        super(new RankConfig(false));
    }

    @Override
    public BigDecimal findPerExperienceByRank(Integer level) {
        List<Object> params = new ArrayList<>();
        String hql = "select perExp from RankConfig r where ? BETWEEN rankFrom AND rankTo ";

        params.add(level);
        BigDecimal result = (BigDecimal) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return BigDecimal.ZERO;
    }

    @Override
    public RankConfig findRankByExperience(BigDecimal totalExp) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + RankConfig.class + " WHERE 1=1 ";


        hql += " and a.totalExp>=? order by a.rankFrom";
        params.add(totalExp);

        return findFirst(hql, params.toArray());
    }

    @Override
    public RankConfig findPreviousRankByExperience(BigDecimal totalExp) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + RankConfig.class + " WHERE 1=1 ";


        hql += " and a.totalExp<=? order by a.rankFrom desc";
        params.add(totalExp);

        return findFirst(hql, params.toArray());
    }

    @Override
    public RankConfig findHighestRank() {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + RankConfig.class + " WHERE 1=1 ";


        hql += " order by a.rankFrom desc";

        return findFirst(hql, params.toArray());
    }

    @Override
    public BigDecimal findRewardPercentByRank(Integer level) {
        List<Object> params = new ArrayList<>();
        String hql = "select rewardPercent from RankConfig r where ? BETWEEN rankFrom AND rankTo ";
        params.add(level);
        BigDecimal result = (BigDecimal) exFindUnique(hql, params.toArray());

        if (result != null)
            return result;
        return BigDecimal.ZERO;
    }

    @Override
    public void findRankConfigForDatagrid(DatagridModel<RankConfig> datagridModel, Date dateFrom, Date dateTo) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + RankConfig.class + " WHERE 1=1 ";

        if (dateFrom != null) {
            hql += " and a.startDate>=? ";
            params.add(DateUtil.truncateTime(dateFrom));
        }

        if (dateTo != null) {
            hql += " and a.endDate<=? ";
            params.add(DateUtil.truncateTime(dateTo));
        }

        findForDatagrid(datagridModel, "a", hql, params.toArray());
    }

    @Override
    public RankConfig checkRank(RankConfig rankConfig) {
        List<Object> params = new ArrayList<Object>();

        String hql = "from a IN " + RankConfig.class + " WHERE 1=1 ";


        hql += " and a.rankFrom=? and a.rankTo=?";
        params.add(rankConfig.getRankFrom());
        params.add(rankConfig.getRankTo());

        if (rankConfig.getStartDate() != null) {
            hql += " and a.startDate>=? ";
            params.add(DateUtil.truncateTime(rankConfig.getStartDate()));
        }

        if (rankConfig.getEndDate() != null) {
            hql += " and a.endDate<=? ";
            params.add(DateUtil.truncateTime(rankConfig.getEndDate()));
        }

        return findFirst(hql, params.toArray());

    }

}
