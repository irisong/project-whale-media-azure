package com.compalsolutions.compal.rank.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;

public interface MemberRankTrxDao extends BasicDao<MemberRankTrx, String> {
    public static final String BEAN_NAME = "memberRankTrxDao";

    public MemberRankTrx findExperienceByTrxTypeByRemark(String memberId, String trxType, String remark);
}
