package com.compalsolutions.compal.rank.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.dao.MemberDao;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.rank.dao.MemberRankTrxDao;
import com.compalsolutions.compal.rank.dao.RankConfigDao;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.rank.vo.RankConfig;
import jnr.ffi.annotations.In;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Component(RankService.BEAN_NAME)
public class RankServiceImpl implements RankService {

    private static final Log log = LogFactory.getLog(RankServiceImpl.class);

    @Autowired
    private MemberRankTrxDao memberRankTrxDao;

    @Autowired
    private RankConfigDao rankConfigDao;

    @Autowired
    private MemberDao memberDao;

    private static final Object synchronizedObject = new Object();

    @Override
    public BigDecimal getExperienceByRank(Integer rank) {
        return rankConfigDao.findPerExperienceByRank(rank);
    }

    @Override
    public RankConfig findRankByExperience(BigDecimal totalExp) {
        return rankConfigDao.findRankByExperience(totalExp);
    }

    @Override
    public Triple<BigDecimal, Integer, Integer> getCurrentRankDetails(BigDecimal memberExp) {
        RankConfig rankConfig = findRankByExperience(memberExp);
        RankConfig previousRankConfig = findPreviousRankByExperience(memberExp);
        BigDecimal requiredExp = BigDecimal.ZERO;
        BigDecimal nextRankExp;
        Integer nextLevel;
        if (rankConfig == null) {
            rankConfig = findHighestRank();
        }
        int currentRank = 0;
        BigDecimal totalExp = memberExp;
        if (previousRankConfig != null) {
            totalExp = totalExp.subtract(previousRankConfig.getTotalExp());
            currentRank = previousRankConfig.getRankTo();
        }
        for (int i = rankConfig.getRankFrom(); i <= rankConfig.getRankTo(); i++) {
            totalExp = totalExp.subtract(rankConfig.getPerExp());
            if (BDUtil.geZero(totalExp)) {
                currentRank = i;
            } else {
                requiredExp = totalExp.multiply(new BigDecimal("-1"));
                break;
            }
        }
        nextRankExp = getExperienceByRank(currentRank + 1);
        nextLevel = currentRank + 1;
        if (BDUtil.leZero(nextRankExp)) {
            nextLevel = currentRank;
        }
        return new MutableTriple<>(requiredExp, currentRank, nextLevel);
    }


    @Override
    public Integer getCurrentRank(BigDecimal memberExp) {
        Triple<BigDecimal, Integer, Integer> rankDetails = getCurrentRankDetails(memberExp);
        return rankDetails.getMiddle();
    }

    @Override
    public RankConfig findPreviousRankByExperience(BigDecimal totalExp) {
        return rankConfigDao.findPreviousRankByExperience(totalExp);
    }

    @Override
    public RankConfig findHighestRank() {
        return rankConfigDao.findHighestRank();
    }

    @Override
    public BigDecimal getRewardPercentByRank(Integer rank) {
        return rankConfigDao.findRewardPercentByRank(rank);
    }

    @Override
    public void doProcessMemberExperience(Locale locale, String memberId, String refType, String refno, String refId, String trxType,
                                          long duration, BigDecimal amount, String remark) {
        I18n i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        synchronized (synchronizedObject) {
            /***********************
             * VALIDATION - START
             ***********************/
            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }

            BigDecimal experienceEarned = Global.getExperienceEarnedByTrxType(trxType, duration, amount);
            if (!BDUtil.gZero(experienceEarned)) {
                return;
            }
            /***********************
             * VALIDATION - END
             ***********************/

            /************************
             * MEMBER_EXP_TRX - START
             ************************/
            MemberRankTrx memberRankTrx = new MemberRankTrx(true);
            memberRankTrx.setInExp(experienceEarned);
            memberRankTrx.setOutExp(BigDecimal.ZERO);
            memberRankTrx.setOwnerId(member.getMemberId());
            memberRankTrx.setOwnerType(member.getUserRole());
            memberRankTrx.setTrxDatetime(new Date());
            memberRankTrx.setTrxType(trxType);
            memberRankTrx.setTrxRefType(refType);
            memberRankTrx.setTrxRefno(refno);
            memberRankTrx.setTrxRefId(refId);
            memberRankTrx.setRemark(remark);
            memberRankTrxDao.save(memberRankTrx);

            memberService.doProcessMemberLevel(memberId, experienceEarned);
            /************************
             * MEMBER_EXP_TRX - END
             ************************/

            /***********************
             * MEMBER - START
             ***********************/
//            if (member.getLevel() == null) {
//                // consist of level 0
//                member.setLevel(0);
////                member.setLevel(1);
//            }
//            if (member.getExperience() == null) {
//                member.setExperience(BigDecimal.ZERO);
//            }

//            Integer nextRank = member.getLevel() + 1;
//            // exp to up level is based next required
//            BigDecimal expToUplevel = getExperienceByRank(nextRank);
//
//            BigDecimal totalExp = member.getExperience().add(experienceEarned);
//
//
//            boolean canUpLevel;
//            if (!BDUtil.gZero(expToUplevel)) {
//                canUpLevel = false;
//            } else {
//                canUpLevel = BDUtil.ge(totalExp, expToUplevel);
//            }
//            if (canUpLevel) {
//                // up level and reset experience
//                BigDecimal carryForwardExp = totalExp.subtract(expToUplevel);
//                // if user managed to up more than one level in one action
//                expToUplevel = getExperienceByRank(nextRank + 1);
//                while (BDUtil.ge(carryForwardExp, expToUplevel) && BDUtil.gZero(expToUplevel)) {
//                    nextRank++;
//                    carryForwardExp = carryForwardExp.subtract(expToUplevel);
//                    expToUplevel = getExperienceByRank(nextRank + 1);
//                }
//                member.setLevel(nextRank);
//                member.setExperience(carryForwardExp);
//            } else {
//                member.setExperience(totalExp);
//            }
//            memberDao.update(member);
            /************************
             * MEMBER - END
             ************************/
        }
    }

    @Override
    public MemberRankTrx findExperienceByTrxTypeByRemark(String memberId, String trxType, String remark) {
        return memberRankTrxDao.findExperienceByTrxTypeByRemark(memberId, trxType, remark);
    }

    @Override
    public void findRankConfigForListing(DatagridModel<RankConfig> datagridModel, Date dateFrom, Date dateTo) {
        rankConfigDao.findRankConfigForDatagrid(datagridModel, dateFrom, dateTo);
    }

    @Override
    public void doSaveRank(RankConfig rankConfig) {
        rankConfigDao.save(rankConfig);

    }

    @Override
    public boolean checkRank(RankConfig rankConfig) {
        RankConfig rcReturn = rankConfigDao.checkRank(rankConfig);
        if (rcReturn != null) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public RankConfig getRank(String rankId) {
        return rankConfigDao.get(rankId);
    }

    @Override
    public void updateRank(RankConfig rankConfig) {
        RankConfig rankConfigdb = rankConfigDao.get(rankConfig.getRankId());

        rankConfigdb.setRankFrom(rankConfig.getRankFrom());
        rankConfigdb.setRankTo(rankConfig.getRankTo());
        rankConfigdb.setPerExp(rankConfig.getPerExp());
        rankConfigdb.setRewardPercent(rankConfig.getRewardPercent());
        rankConfigdb.setStartDate(rankConfig.getStartDate());
        rankConfigdb.setEndDate(rankConfig.getEndDate());
        rankConfigdb.setRemark(rankConfig.getRemark());

        rankConfigDao.update(rankConfigdb);
    }
}
