package com.compalsolutions.compal.rank.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "rk_rank_config")
@Access(AccessType.FIELD)
public class RankConfig extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "rank_id", unique = true, nullable = false, length = 32)
    private String rankId;

    @Column(name = "rank_from", length = 32, nullable = false)
    private Integer rankFrom;

    @Column(name = "rank_to", length = 32, nullable = false)
    private Integer rankTo;

    @Column(name = "per_exp", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal perExp;

    @Column(name = "total_exp", columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal totalExp;

    @Column(name = "reward_percent", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal rewardPercent;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "start_date")
    private Date startDate;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "end_date")
    private Date endDate;

    @ToUpperCase
    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Transient
    private BigDecimal experience;

    public RankConfig() {
    }

    public RankConfig(boolean defaultValue) {
        if (defaultValue) {
            perExp = BigDecimal.ZERO;
        }
    }

    public String getRankId() {
        return rankId;
    }

    public void setRankId(String rankId) {
        this.rankId = rankId;
    }

    public Integer getRankFrom() {
        return rankFrom;
    }

    public void setRankFrom(Integer rankFrom) {
        this.rankFrom = rankFrom;
    }

    public Integer getRankTo() {
        return rankTo;
    }

    public void setRankTo(Integer rankTo) {
        this.rankTo = rankTo;
    }

    public BigDecimal getPerExp() {
        return perExp;
    }

    public void setPerExp(BigDecimal perExp) {
        this.perExp = perExp;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public BigDecimal getRewardPercent() {
        return rewardPercent;
    }

    public void setRewardPercent(BigDecimal rewardPercent) {
        this.rewardPercent = rewardPercent;
    }

    public BigDecimal getExperience() {
        return experience;
    }

    public void setExperience(BigDecimal experience) {
        this.experience = experience;
    }

    public BigDecimal getTotalExp() {
        return totalExp;
    }

    public void setTotalExp(BigDecimal totalExp) {
        this.totalExp = totalExp;
    }
}
