package com.compalsolutions.compal.rank.dao;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(MemberRankTrxDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberRankTrxDaoImpl extends Jpa2Dao<MemberRankTrx, String> implements MemberRankTrxDao {
    public MemberRankTrxDaoImpl() {
        super(new MemberRankTrx(false));
    }

    @Override
    public MemberRankTrx findExperienceByTrxTypeByRemark(String memberId, String trxType, String remark) {
        MemberRankTrx memberRankTrx = new MemberRankTrx(false);
        memberRankTrx.setOwnerId(memberId);
        memberRankTrx.setTrxType(trxType);
        memberRankTrx.setRemark(remark);

        return findUnique(memberRankTrx);
    }

}
