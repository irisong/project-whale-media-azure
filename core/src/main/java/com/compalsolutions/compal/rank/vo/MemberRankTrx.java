package com.compalsolutions.compal.rank.vo;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "rk_member_rank_trx")
@Access(AccessType.FIELD)
public class MemberRankTrx extends VoBase {
    private static final long serialVersionUID = 1L;

    public static final String FOLLOW = "FOLLOW";
    public static final String VIEW_STREAM = "STREAM";
    public static final String SPEND = "SPEND";

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "trx_id", unique = true, nullable = false, length = 32)
    private String trxId;

    @Column(name = "owner_id", length = 32, nullable = false)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10, nullable = false)
    private String ownerType;

    /**
     * Reference Type. Eg: trxRefId belongs to which table
     */
    @Column(name = "trx_ref_type")
    private String trxRefType;

    /**
     * Reference No. Eg: earning experience from follow people, spend coin
     */
    @ToTrim
    @Column(name = "trx_refno", length = 100)
    private String trxRefno;

    /**
     * Reference Id. A database table primary key. Eg: spend coin transaction id
     */
    @Column(name = "trx_ref_id", length = 32)
    private String trxRefId;

    @ToUpperCase
    @ToTrim
    @Column(name = "trx_type", length = 50, nullable = false)
    private String trxType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    @Column(name = "in_exp", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal inExp;

    @Column(name = "out_exp", nullable = false, columnDefinition = Global.ColumnDef.DECIMAL_65_20)
    private BigDecimal outExp;

    @ToTrim
    @Column(name = "remark", columnDefinition = Global.ColumnDef.TEXT)
    private String remark;

    @Transient
    private BigDecimal experience;

    public MemberRankTrx() {
    }

    public MemberRankTrx(boolean defaultValue) {
        if (defaultValue) {
            inExp = BigDecimal.ZERO;
            outExp = BigDecimal.ZERO;
        }
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        MemberRankTrx memberRankTrx = (MemberRankTrx) o;

        if (trxId != null ? !trxId.equals(memberRankTrx.trxId) : memberRankTrx.trxId != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        return trxId != null ? trxId.hashCode() : 0;
    }

    public String getTrxRefType() {
        return trxRefType;
    }

    public void setTrxRefType(String trxRefType) {
        this.trxRefType = trxRefType;
    }

    public String getTrxRefno() {
        return trxRefno;
    }

    public void setTrxRefno(String trxRefno) {
        this.trxRefno = trxRefno;
    }

    public String getTrxRefId() {
        return trxRefId;
    }

    public void setTrxRefId(String trxRefId) {
        this.trxRefId = trxRefId;
    }

    public BigDecimal getInExp() {
        return inExp;
    }

    public void setInExp(BigDecimal inExp) {
        this.inExp = inExp;
    }

    public BigDecimal getOutExp() {
        return outExp;
    }

    public void setOutExp(BigDecimal outExp) {
        this.outExp = outExp;
    }

    public BigDecimal getExperience() {
        return experience;
    }

    public void setExperience(BigDecimal experience) {
        this.experience = experience;
    }
}
