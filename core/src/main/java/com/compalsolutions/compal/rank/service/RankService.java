package com.compalsolutions.compal.rank.service;

import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.rank.vo.RankConfig;
import org.apache.commons.lang3.tuple.Triple;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public interface RankService {
    public static final String BEAN_NAME = "rankService";

    public BigDecimal getExperienceByRank(Integer rank);

    public BigDecimal getRewardPercentByRank(Integer rank);

    public RankConfig findRankByExperience(BigDecimal totalExp);

    public RankConfig findPreviousRankByExperience(BigDecimal totalExp);

    public Triple<BigDecimal, Integer, Integer> getCurrentRankDetails(BigDecimal memberExp);

    public Integer getCurrentRank(BigDecimal memberExp);

    public RankConfig findHighestRank();

    public void doProcessMemberExperience(Locale locale, String memberId, String refType, String refno, String refId, String trxType,
                                          long duration, BigDecimal amount, String remark);

    public MemberRankTrx findExperienceByTrxTypeByRemark(String memberId, String trxType, String remark);

    public void findRankConfigForListing(DatagridModel<RankConfig> datagridModel, Date dateFrom, Date dateTo);

    public void doSaveRank(RankConfig rankConfig);

    public boolean checkRank(RankConfig rankConfig);

    public RankConfig getRank(String rankId);

    public void updateRank(RankConfig rankConfig);
}
