package com.compalsolutions.compal.rank.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.rank.vo.RankConfig;

import java.math.BigDecimal;
import java.util.Date;

public interface RankConfigDao extends BasicDao<RankConfig, String> {
    public static final String BEAN_NAME = "rankConfigDao";

    public BigDecimal findPerExperienceByRank(Integer level);

    public BigDecimal findRewardPercentByRank(Integer level);

    public void findRankConfigForDatagrid(DatagridModel<RankConfig> datagridModel, Date dateFrom, Date dateTo);

    public RankConfig checkRank(RankConfig rankConfig);

    public RankConfig findRankByExperience(BigDecimal totalExp);

    public RankConfig findPreviousRankByExperience(BigDecimal totalExp);

    public RankConfig findHighestRank();
}
