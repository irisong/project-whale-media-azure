package com.compalsolutions.compal.websocket.vo;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.vo.VoBase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "ws_message")
@Access(AccessType.FIELD)
public class WsMessage extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "messsage_id", unique = true, nullable = false, length = 32)
    private String messageId;

    @Column(name = "owner_id", length = 32)
    private String ownerId;

    @ToUpperCase
    @ToTrim
    @Column(name = "owner_type", length = 10)
    private String ownerType;

    @ToUpperCase
    @ToTrim
    @Column(name = "type", length = 30, nullable = false)
    private String type;

    @Column(name = "data", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String data;

    @Column(name = "message", columnDefinition = Global.ColumnDef.MEDIUM_TEXT)
    private String messsage;

    public WsMessage() {
    }

    public WsMessage(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMesssage() {
        return messsage;
    }

    public void setMesssage(String messsage) {
        this.messsage = messsage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WsMessage wsMessage = (WsMessage) o;

        return messageId != null ? messageId.equals(wsMessage.messageId) : wsMessage.messageId == null;
    }

    @Override
    public int hashCode() {
        return messageId != null ? messageId.hashCode() : 0;
    }
}
