package com.compalsolutions.compal.websocket.vo;

import java.util.Date;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import com.compalsolutions.compal.vo.VoBase;

@Entity
@Table(name = "ws_message_broadcast")
@Access(AccessType.FIELD)
public class WsMessageBroadcast extends VoBase {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")
    @Column(name = "broadcast_id", unique = true, nullable = false, length = 32)
    private String broadcastId;

    @Column(name = "messsage_id", nullable = false, length = 32)
    private String messageId;

    @Column(name = "broadcast_code", length = 32)
    private String broadcasterCode;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "trx_datetime", nullable = false)
    private Date trxDatetime;

    public WsMessageBroadcast() {
    }

    public WsMessageBroadcast(boolean defaultValue) {
        if (defaultValue) {
        }
    }

    public String getBroadcastId() {
        return broadcastId;
    }

    public void setBroadcastId(String broadcastId) {
        this.broadcastId = broadcastId;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getBroadcasterCode() {
        return broadcasterCode;
    }

    public void setBroadcasterCode(String broadcasterCode) {
        this.broadcasterCode = broadcasterCode;
    }

    public Date getTrxDatetime() {
        return trxDatetime;
    }

    public void setTrxDatetime(Date trxDatetime) {
        this.trxDatetime = trxDatetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        WsMessageBroadcast that = (WsMessageBroadcast) o;

        return broadcastId != null ? broadcastId.equals(that.broadcastId) : that.broadcastId == null;
    }

    @Override
    public int hashCode() {
        return broadcastId != null ? broadcastId.hashCode() : 0;
    }
}
