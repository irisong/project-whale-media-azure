package com.compalsolutions.compal.websocket;

import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.web.WsLoginInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.websocket.Session;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service(WsManagerProvider.BEAN_NAME)
public class WsManagerProviderImpl implements WsManagerProvider {
    private static final Log log = LogFactory.getLog(WsManagerProviderImpl.class);

   // private static final Log log = LogFactory.getLog(WsInComingUtil.class);
    private static final String USER_ID = "USER_ID";
    private static final String LOGIN_INFO = "LOGIN_INFO";
    private static final String CHANNEL_ID = "CHANNEL_ID";

    private Map<String, Session> sessions = new HashMap<>();
    private Map<String, Map<String, Session>> channelSessions = new HashMap<>();
    private Map<String, Map<String, Session>> userSessions = new HashMap<>();
    private Map<String, Integer> fansCount = new HashMap<>();
    private Map<String, Integer> followerCount = new HashMap<>();

    @Autowired
    public WsManagerProviderImpl() {

    }

    @Override
    public Map<String, Session> getSessions() {
        return sessions;
    }

    @Override
    public void setSessions(Map<String, Session> sessions) {
        this.sessions = sessions;
    }

    @Override
    public Map<String, Map<String, Session>> getChannelSessions() {
        return channelSessions;
    }

    @Override
    public void setChannelSessions(Map<String, Map<String, Session>> channelSessions) {
        this.channelSessions = channelSessions;
    }

    @Override
    public Map<String, Map<String, Session>> getUserSessions() {
        return userSessions;
    }

    @Override
    public void setUserSessions(Map<String, Map<String, Session>> userSessions) {
        this.userSessions = userSessions;
    }

    @Override
    public Map<String, Integer> getFansCount() {
        return fansCount;
    }

    @Override
    public void setFansCount(Map<String, Integer> fansCount) {
        this.fansCount = fansCount;
    }

    @Override
    public Map<String, Integer> getFollowerCount() {
        return followerCount;
    }

    @Override
    public void setFollowerCount(Map<String, Integer> followerCount) {
        this.followerCount = followerCount;
    }

    @Override
    public void addSession(Session session) {
        sessions.put(session.getId(), session);
    }

    @Override
    public String getUserId(Session session) {
        return (String) session.getUserProperties().get(USER_ID);
    }

    @Override
    public boolean containsSession(String sessionId) {
        return sessions.containsKey(sessionId);
    }
    @Override
    public boolean containsSession(Session session) {
        return containsSession(session.getId());
    }

    @Override
    public void removeSession(Session session){

        String sessionId = session.getId();
        String userId = getUserId(session);
        String channelId = getChannelId(session);
        sessions.remove(sessionId);

        _removeFromUserSession(userId, sessionId);
        _removeFromChannelSession(channelId, sessionId);
        _removeFromFansCountSession(channelId);
        _removeFromFollowerCountSession(channelId);

        log.debug("Channel Session Size: "+channelSessions.size() + " "+sessions.size());

//        if(channelSessions.size() <= 0 && sessions.size() <= 0) {
//            log.debug("Reset session list");
//            sessions = null;
//            channelSessions = null;
//            followerCount = null;
//            fansCount = null;
//            channelSessions = new HashMap<>();
//            sessions = new HashMap<>();
//            fansCount = new HashMap<>();
//            followerCount = new HashMap<>();
//        }
    }

    @Override
    public void updateChannelSession(Session session, String channelId) {
        String oldChannelId = getChannelId(session);

        if (StringUtils.isNotBlank(oldChannelId)) {
            if (channelSessions.containsKey(oldChannelId)) {
                channelSessions.get(oldChannelId).remove(session.getId());
            }
        }
        log.debug("UpdateChannelSession: " + session.getId() + " >> channelId: " + channelId + " ");
        setChannelId(session, channelId);

        Map<String, Session> sessionMap = null;
        if (channelSessions.containsKey(channelId)) {
            sessionMap = channelSessions.get(channelId);
        } else {
            sessionMap = new HashMap<>();
            channelSessions.put(channelId, sessionMap);
        }

        sessionMap.put(session.getId(), session);
    }

    @Override
    public int getFansCountByChannelId(String channelId){
        if (fansCount.containsKey(channelId)) {
            return fansCount.get(channelId);
        } else {
            return 0;
        }
    }

    @Override
    public void setFansCountByChannelId(String channelId, int count) {
        fansCount.put(channelId, count);
    }

    @Override
    public void setFollowerCountByChannelId(String channelId, int count) {
        followerCount.put(channelId, count);
    }

    @Override
    public int getFollowerCountByChannelId(String channelId) {
        if (followerCount.containsKey(channelId)) {
            return followerCount.get(channelId);
        } else {
            return 0;
        }
    }

    @Override
    public List<Session> getSessionsByChannelId(String channelId) {
        if (channelSessions.containsKey(channelId)) {
            return new ArrayList<>(channelSessions.get(channelId).values());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void removeChannelSession(Session session) {
        String channelId = getChannelId(session);
        String sessionId = session.getId();

        _removeFromChannelSession(channelId, sessionId);
    }

    @Override
    public void removeUserSession(Session session) {
        String userId = getUserId(session);
        String sessionId = session.getId();

        _removeFromUserSession(userId, sessionId);
    }

    @Override
    public void updateUserSession(Session session, String userId, User user) {
        String oldUserId = getUserId(session);

        if (StringUtils.equals(userId, oldUserId))
            return;

        if (StringUtils.isNotBlank(oldUserId)) {
            if (userSessions.containsKey(oldUserId)) {
                userSessions.get(oldUserId).remove(session.getId());
            }
        }

        setUserId(session, userId);

        Map<String, Session> sessionMap = null;
        if (userSessions.containsKey(userId)) {
            sessionMap = userSessions.get(userId);
        } else {
            sessionMap = new HashMap<>();
            userSessions.put(userId, sessionMap);
        }

        sessionMap.put(session.getId(), session);

        WsLoginInfo loginInfo = new WsLoginInfo();
        loginInfo.setUserId(user.getUserId());
        loginInfo.setUser(user);
        loginInfo.setLocale(null);
        loginInfo.setSessionId(session.getId());

        if (user instanceof AdminUser) {
            loginInfo.setUserType(new MrmAdminUserType());
        } else if (user instanceof AgentUser) {
            loginInfo.setUserType(new MrmAgentUserType());
        } else if (user instanceof MemberUser) {
            loginInfo.setUserType(new MrmMemberUserType());
        } else
            throw new SystemErrorException("Invalid user type");
        setLoginInfo(session, loginInfo);
    }

    private void _removeFromChannelSession(String channelId, String sessionId) {
        if (StringUtils.isNotBlank(channelId) && channelSessions.containsKey(channelId)) {
            Map<String, Session> sessionMap = channelSessions.get(channelId);
            sessionMap.remove(sessionId);
            if (sessionMap.isEmpty())
                channelSessions.remove(channelId);
        }
    }

    private void _removeFromUserSession(String userId, String sessionId) {
        if (StringUtils.isNotBlank(userId) && userSessions.containsKey(userId)) {
            Map<String, Session> sessionMap = userSessions.get(userId);
            sessionMap.remove(sessionId);

            if (sessionMap.isEmpty())
                userSessions.remove(userId);
        }
    }

    private String getChannelId(Session session) {
        return (String) session.getUserProperties().get(CHANNEL_ID);
    }

    private void setChannelId(Session session, String channelId) {
        session.getUserProperties().put(CHANNEL_ID, channelId);
    }

    private void setUserId(Session session, String userId) {
        session.getUserProperties().put(USER_ID, userId);
    }

    private void setLoginInfo(Session session, LoginInfo loginInfo) {
        session.getUserProperties().put(LOGIN_INFO, loginInfo);
    }

    private void _removeFromFansCountSession(String channelId) {
        if (StringUtils.isNotBlank(channelId) && fansCount.containsKey(channelId)) {
            fansCount.remove(channelId);
        }
    }

    private void _removeFromFollowerCountSession(String channelId) {
        if (StringUtils.isNotBlank(channelId) && followerCount.containsKey(channelId)) {
            followerCount.remove(channelId);
        }
    }

}
