package com.compalsolutions.compal.websocket;

import com.compalsolutions.compal.function.user.vo.User;

import javax.websocket.Session;
import java.util.List;
import java.util.Map;

public interface WsManagerProvider {
    public static final String BEAN_NAME = "wsManagerProvider";

    public Map<String, Session> getSessions();

    public void setSessions(Map<String, Session> sessions);

    public Map<String, Map<String, Session>> getChannelSessions();

    public void setChannelSessions(Map<String, Map<String, Session>> channelSessions);

    public Map<String, Map<String, Session>> getUserSessions();

    public void setUserSessions(Map<String, Map<String, Session>> userSessions);

    public Map<String, Integer> getFansCount();

    public void setFansCount(Map<String, Integer> fansCount);

    public Map<String, Integer> getFollowerCount();

    public void setFollowerCount(Map<String, Integer> followerCount);

    public void addSession(Session session);

    public String getUserId(Session session);

    public boolean containsSession(String sessionId);

    public boolean containsSession(Session session);

    public void removeSession(Session session);

    public void updateChannelSession(Session session, String channelId);

    public int getFansCountByChannelId(String channelId);

    public void setFansCountByChannelId(String channelId, int count);

    public void setFollowerCountByChannelId(String channelId, int count);

    public int getFollowerCountByChannelId(String channelId);

    public List<Session> getSessionsByChannelId(String channelId);

    public void removeChannelSession(Session session);

    public void removeUserSession(Session session);

    public void updateUserSession(Session session, String userId, User user);

}
