package com.compalsolutions.compal.websocket.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.websocket.vo.WsMessage;
import com.compalsolutions.compal.websocket.vo.WsMessageBroadcast;

public interface WsMessageService {
    public static final String BEAN_NAME = "wsMessageService";

    public List<WsMessage> findNotProcessedMessagesSince(Date date, String broadcasterCode, int numberOfRecords);

    public void saveWsMessageBroadcast(WsMessageBroadcast wsMessageBroadcast);

    public WsMessage getWsMessage(String messageId);

    public void broadCastMessageLiveStreamData(LiveStreamDto liveStreamDto);

    public void updateOnlineMember(String channelId, String actionType);

    public void updateTotalPointAndFansCount(String channelId, BigDecimal point);

    public void updateFollowerCount(String channelId);

    public void updateBroadCasterInfo(LiveStreamChannel liveStreamChannel, String agoraUid);
}
