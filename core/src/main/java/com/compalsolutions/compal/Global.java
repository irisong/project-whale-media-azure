package com.compalsolutions.compal;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;

import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;

import com.compalsolutions.compal.exception.ValidatorException;

import static org.apache.commons.lang3.RandomUtils.nextInt;

public class Global extends FrameworkConst {

    public static class Status extends BasicStatus {
        public final static String LOGOUT = "LOGOUT";
        public final static String PAYMENT_PENDING = "PAYMENT_PENDING";
        public final static String MATCHED = "MATCHED";
        public final static String SCHEDULED = "SCHEDULED";
        public final static String PROCESSED = "PROCESSED";
        public final static String COMPLETED = "COMPLETED";
        public final static String FAILED = "FAILED";
        public final static String DOUBLE_SPEND = "DOUBLE_SPEND";
        public final static String PENDING_TRANSACTION_PASSWORD = "PENDING_TRANSACTION_PASSWORD";

        public static String getI18nKey(String status) {
            if (StringUtils.isBlank(status))
                return null;

            switch (status) {
                case ACCEPTED:
                    return "statAccepted";
                case ACTIVE:
                    return "statActive";
                case APPROVED:
                    return "statApprove";
                case CANCELLED:
                    return "statCancel";
                case CHECKED:
                    return "statChecked";
                case CLOSED:
                    return "statClosed";
                case COMPLETED:
                    return "statCompleted";
                case DELETED:
                    return "statDeleted";
                case DISABLED:
                    return "statDisabled";
                case ERROR:
                    return "statError";
                case INACTIVE:
                    return "statInactive";
                case LOGOUT:
                    return "statLogout";
                case MATCHED:
                    return "statMatch";
                case NEW:
                    return "statNew";
                case NO_ERROR:
                    return "statNoError";
                case NOT_APPLICABLE:
                    return "statNotApplicable";
                case OPEN:
                    return "statOpen";
                case PARTIAL:
                    return "statPartial";
                case PAYMENT_PENDING:
                    return "statPendingPayment";
                case PENDING:
                    return "statPending";
                case REJECTED:
                    return "statRejected";
                case SCHEDULED:
                    return "statScheduled";
                case USED:
                    return "statUsed";
                case PROCESSED:
                    return "statProcessed";
                case FAILED:
                    return "statFailed";
                case DOUBLE_SPEND:
                    return "statDoubleSpend";
            }
            return status;
        }

        public static String[][] getAllStatusWithI18nKey() {
            String[] status = new String[]{ //
                    ACCEPTED, //
                    ACTIVE, //
                    APPROVED, //
                    CANCELLED, //
                    CHECKED, //
                    CLOSED, //
                    COMPLETED, //
                    DELETED, //
                    DISABLED, //
                    ERROR, //
                    INACTIVE, //
                    LOGOUT, //
                    MATCHED, //
                    NO_ERROR, //
                    NOT_APPLICABLE, //
                    OPEN, //
                    PARTIAL, //
                    PAYMENT_PENDING, //
                    PENDING, //
                    REJECTED, //
                    SCHEDULED, //
                    USED, //
                    PROCESSED, //
                    FAILED, //
                    DOUBLE_SPEND //
            };

            String[][] statusKeys = new String[status.length][];
            for (int i = 0; i < status.length; i++) {
                statusKeys[i] = new String[]{status[i], getI18nKey(status[i])};
            }
            return statusKeys;
        }
    }

    public static class UserType extends DefaultUserType {
        public static final String HQ = "HQ";
        public static final String AGENT = "AGENT";
        public static final String MEMBER = "MEMBER";
        public static final String VJ = "VJ";
        public static final String REMOTE_AGENT = "REMOTE_AGENT";
        public static final String GM = "GM";

    }

    public static class UserRoleGroup {
        public static final String AGENT_GROUP = "AGENT_GROUP";
        public static final String MEMBER_GROUP = "MEMBER_GROUP";
    }

    public static class ColumnDef extends BasicColumnDef {
        public static final String DECIMAL_65_20 = "decimal(65,20)";
        public static final String DECIMAL_16_2_DEFAULT_0 = "decimal(16,2) default 0.00";
        public static final String DECIMAL_16_3_DEFAULT_0 = "decimal(16,3) default 0.000";
        public static final String DEFAULT_0 = "int default 0";
    }

    public static class WalletAdjustType {
        public static final String IN = "IN";
        public static final String OUT = "OUT";
    }

    public final static List<String> EXCEL_FILE_MIME_TYPES = Arrays.asList( //
            "application/vnd.ms-excel", //
            "application/msexcel", //
            "application/x-msexcel", //
            "application/x-ms-excel", //
            "application/x-excel", //
            "application/x-dos_ms_excel", //
            "application/xls", //
            "application/x-xls", //
            "application/haansoftxlsx" //
    );

    public static class ProductType {
        public static final String PRODUCT = "PRODUCT";
        public static final String REGISTER = "REGISTER";
    }

    public static class WalletTrxType {
        public static final String TOP_UP = "TOPUP";
        public static final String REGISTER = "REGISTER";
        public static final String PURCHASE = "PURCHASE";
        public static final String ADJUST = "ADJUST";
        public static final String TRANSFER = "TRANSFER";
        public static final String TRANSFER_REJECT = "TRANSFER_REJECT";
        public static final String WITHDRAW = "WITHDRAW";
        public static final String WITHDRAW_REJECT = "WITHDRAW_REJECT";
        public static final String WITHDRAW_CANCEL = "WITHDRAW_CANCEL";
        public static final String EXCHANGE_WHALEPOSCOIN = "EXCHANGE_WHALEPOSCOIN";

        public static final String DEPOSIT_INTEREST_ACCOUNT = "DEPOSIT_INTEREST_ACCOUNT";
        public static final String WITHDRAW_INTEREST_ACCOUNT = "WITHDRAW_INTEREST_ACCOUNT";
        public static final String PAYOUT_INTEREST_ACCOUNT = "PAYOUT_INTEREST_ACCOUNT";

        public static final String GHL_SWAP = "GHL_SWAP";

        public static final String DEPOSIT = "DEPOSIT";
        public static final String BANK_DEPOSIT = "BANK_DEPOSIT";
        public static final String TRADE_ORDER = "TRADE_ORDER";
        public static final String TRADE_ORDER_RETURN = "TRADE_ORDER_RETURN";
        public static final String TRADE_ORDER_CANCEL = "TRADE_ORDER_CANCEL"; // partial or fully cancel
        public static final String THIRD_PARTY_REGISTER = "THIRD_PARTY_REGISTER";
        public static final String RECEIVE = "RECEIVE";
        public static final String EXCHANGE = "EXCHANGE";
        public static final String EXCHANGE2CRYPTO = "EXCHANGE2CRYPTO";
        public static final String EXCHANGE2CRYPTO_REJECT = "EXCHANGE2CRYPTO_REJECT";
        public static final String REBATE = "REBATE";
        public static final String REFERRER = "REFERRER";
        public static final String SUPERNODE = "SUPERNODE";
        public static final String PACKAGE = "PACKAGE";
        public static final String EVENT = "EVENT";
        public static final String POSPAYMENT = "POSPAYMENT";
        public static final String INCENTIVE = "INCENTIVE";

        public static final String LIVE_STREAM_GIFT = "LIVE_STREAM_GIFT";
        public static final String LIVE_STREAM_TOPUP = "LIVE_STREAM_TOPUP";
        public static final String LIVE_STREAM_GIFT_REWARD = "LIVE_STREAM_GIFT_REWARD";
        public static final String LIVE_STREAM_GIFT_INCOME = "INCOME";
        public static final String LIVE_STREAM_BASIC_INCOME = "BASIC";
        public static final String LIVE_STREAM_WITHDRAW = "WITHDRAW";
        public static final String LIVE_STREAM_DAILY_CHECK_IN = "LIVE_STREAM_DAILY_CHECK_IN";

        public static final String MERCHANT_SPENDING = "MERCHANT_SPENDING";
    }

    public static class WalletWithdrawType {
        public static final String BITCOIN = "BITCOIN";
        public static final String ETHERCOIN = "ETHERCOIN";
    }

    public static class RoiSupernodeStatus {
        public static final String PENDING = "PENDING";
        public static final String SUCCESS = "SUCCESS";
    }

    public static class WalletTopupCryptoCurrencyType {
        public static final String BITCOIN = "BITCOIN";
    }

    // temporary configuration.
    public static class WalletWithdrawConf {
        public static final double minimumAmount = 10;
        public static final boolean adminFeeByAmount = false; // by Percentage
        public static final double adminFree = 5; // 5%
        public static final List<Integer> walletTypes = Arrays.asList(10, 20);
    }

    // temporary configuration.
    public static class WalletTopupConf {
        public static final double minimumAmount = 100;
        public static final List<Integer> walletTypes = Arrays.asList(10, 20);
    }

    public static class WalletPackageStatus {
        public static final String DISPLAY = "DISPLAY";
        public static final String HIDE = "HIDE";
    }


    /**
     * refer to http://struts.apache.org/plugins/json/
     */
    public static class JsonInclude {
        public static final String All = ".+";
        public static final String Default = "actionErrors, actionMessages, actionType, errorMessages, errors, fieldErrors, successAddNewUrl, successExitUrl, successMenuKey, successMessage, actionErrors\\[\\d+\\], actionMessages\\[\\d+\\], errorMessages\\[\\d+\\], errors\\[\\d+\\], fieldErrors\\[\\d+\\]";
        public static final String Datagrid = Default + ", rows, total";
        public static final String TreantTree = Default + ", treantObject.*";
        public static final String Datatables = Default + ", draw, recordsTotal, recordsFiltered, data";
        public static final String Rest = Default + ", message, messages, messages\\[\\d+\\]";
        public static final String WebSocket = Default + ", message, type, data, data.*";
    }

    public static class JsonExclude {
        public static final String Default = "loginUser, loginSessionId, loginInfo, locale, screenPosY, user.authorities, user.password, datagridModel, showSuccessExitButton, successExitButtonLabel, successExitNamespace, successExitAction, showSuccessAddNewButton, successAddNewButtonLabel, successAddNewNamespace, successAddNewAction, password, password2, dateFromConditions, dateToConditions, likeConditions, joinTables, notConditions, inConditions, notInConditions, .*.password, .*.password2, .*.dateFromConditions, .*.dateToConditions, .*.joinTables, .*.likeConditions, .*.notConditions, .*.inConditions, .*.notInConditions";
    }

    public static class PublishGroup {
        public static final String ADMIN_GROUP = "ADMIN";
        public static final String MEMBER_GROUP = "MEMBER";
        public static final String PUBLIC_GROUP = "PUBLIC";
        public static final String AGENT_GROUP = "AGENT";
    }

    public static class ImportOption {
        public static final String NO_OVERWRITE = "NO_OVERWRITE";
        public static final String OVERWRITE = "OVERWRITE";
        public static final String SKIP_OVERWRITE = "SKIP_OVERWRITE";

        public static final String DEFAULT_OPTION = NO_OVERWRITE;
    }

    public static class IdentityType {
        public static final String PASSPORT = "PB";
        public static final String DRIVING_LICENCE = "DL";
        public static final String IDENTITY_CARD = "IC";
        public static final String OTHER = "OT";

        public static final String DEFAULT = PASSPORT;
    }

    public static class AnnouncementType {
        public static final String CONTENT = "CONTENT";
    }

    public static class Gender {
        public static final String MALE = "M";
        public static final String FEMALE = "F";
    }

    public static class MemberFileType {
        public static final String BANK = "BANK";
        public static final String RESIDENCE = "RESIDENCE";
        public static final String PASSPORT = "PASSPORT";
    }

    public static class SmsTagType {
        public static final String LOGIN = "LOGIN";
        public static final String REGISTER = "REGISTER";
    }

    public static class CartPayment {
        private static final String CART_COOKIE = "#CART%PaYMeNT*";
        public static final Integer MAX_AGE = 60 * 60 * 24 * 30; // 30 days

        public static String getCartCookie(String memberId) {
            return CART_COOKIE + "_" + memberId;
        }
    }

    public static class PaymentMethod {
        public static final String CREDIT_DEBIT_CARD = "CREDIT_DEBIT_CARD";
        public static final String WALLET = "WALLET";
        public static final String CRYPTOCURRENCY = "CRYPTOCURRENCY";
        public static final String OMNIPAY = "OMNIPAY";
    }

    public static class CryptocurrencyPaymentType {
        public static final String BITCOIN = "BITCOIN";
    }

    public static class CreditDebitCardType {
        public static final String VISA = "VISA";
        public static final String MASTER = "MASTER";
    }

    public static class SalesOrderType {
        public static final String REGISTER = "REGISTER";
        public static final String PURCHASE = "PURCHASE";
    }

    public final static String DEFAULT_AGENT = "00000000000000000000000000000000";
    public final static String COMPANY_MEMBER = "00000000000000000000000000000000";
    public final static String IOS_TEST_USER_CODE = "1234567890";
    public final static String OMNIC_PLUS_MERCHANT_CODE = "8619999000004";
    public final static Locale LOCALE_EN = new Locale(Language.ENGLISH);
    public final static Locale LOCALE_CN = new Locale(Language.CHINESE);
    public final static Locale LOCALE_MS = new Locale(Language.MALAY);

    // a enquiry address for enquiry
    public final static String ENQUIRY_USER = "ENQUIRY_USER";

    // process user eth withdrawal
    public final static String ETH_WALLET_USER = "ETH_WALLET_USER";

    // process user deposit address to central address
    public final static String ETH_DEPOSIT_WALLET_USER = "ETH_DEPOSIT_WALLET_USER";

    // process ERC20 token withdrawal
    public final static String ERC20_WALLET_USER = "ERC20_WALLET_USER";

    // process ERC20 token withdrawal
    public final static String ERC20_WALLET_USER2 = "ERC20_WALLET_USER2";

    public static class SystemConfigImageType {
        public static final String ADMIN_DASHBOARD = "AD";
        public static final String STORE_FRONT = "SF";
        public static final String EMAIL_TEMPLATE = "ET";
    }

    public static class CryptoType {
        public static final String BASE_CRYPTO = "BASE_CRYPTO";
        public static final String ETH = "ETH";
        public static final String BTC = "BTC";
        public static final String OMC = "OMC";
    }

    public static class RankOrderStatusCode {
        public static final String PENDING = "PENDING";
        public static final String SUCCESS = "SUCCESS";
        public static final String ERROR = "ERROR";
    }

    public static class RankOrderPosStatusCode {
        public static final String PENDING = "PENDING";
        public static final String SUCCESS = "SUCCESS";
        public static final String DISQUALIFIED = "DISQUALIFIED";
        public static final String ERROR = "ERROR";
    }

    public static final int[] VipTypes = {0, 100, 1100, 2100};
    public static final int[][] VipStarTypes = {{0, 0}, {1, 3}, {2, 5}, {3, 7}, {4, 8}, {5, 9}, {6, 10}, {7, 15}};
    public static final int[] MasternodeTypes = {0, 500, 1000, 5000, 10000, 20000};

    public static class WalletType {
        public static final int WALLET_10 = 10; // ETH
        public static final int WALLET_20 = 20; // BTC
        public static final int WALLET_30 = 30; // OMC
        public static final int WALLET_40 = 40; // POINT
        public static final int WALLET_50 = 50; // USD
        public static final int WALLET_51 = 51; // MYR
        public static final int WALLET_55 = 55; // Incentive Point
        public static final int WALLET_60 = 60; // USDT
        public static final int WALLET_61 = 61; // LTC
        public static final int WALLET_62 = 62; // XRP
        public static final int WALLET_63 = 63; // EOS
        public static final int WALLET_64 = 64; // DOGE
        public static final int WALLET_65 = 65; // BCH
        public static final int WALLET_66 = 66; // OMNIC Token
        public static final int WALLET_70 = 70; // GGS
        public static final int WALLET_80 = 80; // WLC (Whale live coin)
        public static final int WALLET_81 = 81; // POINT rebate (Incentive Point from gift)
        public static final int WALLET_82 = 82; // Small Whale Coin, gift income received (during live stream)

        public static final String ETH = "ETH";
        public static final String BTC = "BTC";
        public static final String OMC = "OMC";
        public static final String POINT = "POINT";
        public static final String USD = "USD";
        public static final String INCW = "INCW";
        public static final String GGS = "GGS";

        public static final String USDT = "USDT";
        public static final String LTC = "LTC";
        public static final String XRP = "XRP";
        public static final String EOS = "EOS";
        public static final String DOGE = "DOGE";
        public static final String BCH = "BCH";
        public static final String OMNICTOKEN = "OMNICTOKEN";
        public static final String WC = "WC"; //whale live coin
        public static final String WRP = "WRP"; //whale reward/rebate point / point transfer from OMC Whale POS
        public static final String BWP = "BWP"; //baby whale point
        public static final String MYR = "MYR";
    }

    public static final double TRADE_TRANSACTION_FEE_PERCENT = 2d;
    // minimum trade amount in BTC
    public static final BigDecimal MIN_TRADE_AMOUNT_BTC = new BigDecimal("0.0005");
    public static final BigDecimal MIN_TRADE_AMOUNT_ETH = new BigDecimal("0.005");

    public static final BigDecimal DAILY_WITHDRAW_LIMIT = new BigDecimal(999999999999999999D);
    public static final String DAILY_WITHDRAW_LIMIT_CURRENCY = CryptoType.BTC;
    public static final double MAX_ETH_GAS_PRICE = 50; // max eth gas price is 50 GWei
    public static final double MAX_ETH_GAS_PRICE_INTERNAL_TRANSFER = 30; // max eth gas price is 30 GWei
    public static final BigInteger ETH_TRANSFER_GAS_LIMIT = BigInteger.valueOf(21000);

    public static class FiatSysmbol {
        public static final String USD = "USD";
        public static final String CNY = "CNY";
    }

    public static double USD2CNY = 7.5;

    /**
     * trxType that can contribute to level ranking, and experience config
     *
     * @param trxType
     * @return
     */
    public static BigDecimal getExperienceEarnedByTrxType(String trxType, long duration, BigDecimal amount) {
        switch (trxType) {
            case MemberRankTrx.FOLLOW:
                return BigDecimal.ONE;
            case MemberRankTrx.SPEND:
                return BDUtil.roundUp2dp(new BigDecimal("20").multiply(amount.divide(new BigDecimal("30"), 2, BigDecimal.ROUND_HALF_UP)));
            case MemberRankTrx.VIEW_STREAM:
                if (duration < 300) {
                    return BigDecimal.ZERO;
                }
                String durationStr = String.valueOf(duration);
                BigDecimal pointperminutes = new BigDecimal("5").divide(new BigDecimal("60"), 2, BigDecimal.ROUND_HALF_UP);
                BigDecimal minutes = new BigDecimal(durationStr).divide(new BigDecimal("60"), 0, BigDecimal.ROUND_HALF_UP);
                return BDUtil.roundUp2dp(minutes.multiply(pointperminutes));
            default:
                throw new ValidatorException("TrxType: unsupported TrxType " + trxType);
        }
    }

    /**
     * withdraw crypto from system to 3rd party wallet
     *
     * @param cryptoType
     * @return
     */
    public static Pair<BigDecimal, BigDecimal> getWithdrawalMiminumAmountAndFee(String cryptoType) {
        switch (cryptoType) {
            case CryptoType.ETH:
                return new MutablePair<>(new BigDecimal("0.02"), new BigDecimal("0.0025"));
            case CryptoType.BTC:
                return new MutablePair<>(new BigDecimal("0.001"), new BigDecimal("0.0005"));
            case WalletType.USD:
                return new MutablePair<>(new BigDecimal("100"), BigDecimal.ZERO);
            case WalletType.INCW:
                return new MutablePair<>(new BigDecimal("1"), BigDecimal.ZERO);
            case WalletType.OMC:
                return new MutablePair<>(new BigDecimal("1"), BigDecimal.ZERO);
            default:
                throw new ValidatorException("Withdrawal Minimum Amount & Fee: unsupported cryptoType " + cryptoType);
        }
    }

    public static Pair<BigDecimal, BigDecimal> getWalletTransferMiminumAmountAndFee(String cryptoType) {
        switch (cryptoType) {
            case CryptoType.ETH:
                return new MutablePair<>(new BigDecimal("0.02"), BigDecimal.ZERO);
            case CryptoType.BTC:
                return new MutablePair<>(new BigDecimal("0.001"), BigDecimal.ZERO);
            case WalletType.USD:
                return new MutablePair<>(new BigDecimal("100"), BigDecimal.ZERO);
            case WalletType.INCW:
                return new MutablePair<>(new BigDecimal("1"), BigDecimal.ZERO);
            case WalletType.OMC:
                return new MutablePair<>(new BigDecimal("1"), BigDecimal.ZERO);
            default:
                throw new ValidatorException("Wallet Transfer Minimum Amount & Fee: unsupported cryptoType " + cryptoType);
        }
    }

    /**
     * make payment by crypto (POS system)
     *
     * @param currency
     * @return
     */
    public static Pair<BigDecimal, BigDecimal> getMiminumAndMaximumPaymentInLegalCurrency(String currency) {
        switch (currency) {
            case WalletType.USD:
                return new MutablePair<>(new BigDecimal("1.00"), new BigDecimal("500000.00"));
            default:
                throw new ValidatorException("Minimum and maximum payment unsupported for currency " + currency);
        }
    }

    public static class AccessModule {
        public static final String WHALE_MEDIA = "WHALE_MEDIA";
    }

    /**
     * QR code checking (POS system: based on created date)
     */
    public static final Integer QRCodeValidityDurationInHour = 1;

    // temporary hardcode
    public static BigDecimal getWithdrawValueRate(String cryptoType) {
        if (CryptoType.BTC.equals(cryptoType)) {
            return BigDecimal.ONE;
        } else if (CryptoType.ETH.equals(cryptoType)) {
            return new BigDecimal("0.72");
        } else {
            return new BigDecimal("0.001234");
        }
    }

    public static String ensureEthAddressPrefix(String walletAddress) {
        if (StringUtils.isBlank(walletAddress)) {
            throw new ValidatorException("Global.ensureEthAddressPrefix : walletAddress is blank");
        }

        if (walletAddress.startsWith("0x"))
            return walletAddress;
        return "0x" + walletAddress;
    }

    public static String getRandomString(String[] array) {
        int rnd = new Random().nextInt(array.length);
        return array[rnd];
    }

    public static int generateRandomInteger(int min, int max) {
        return nextInt(min, max);
    }

    public static class RankOrderType {
        public static final String VIP = "VIP";
        public static final String SUPERNODE = "SUPERNODE";
    }

    public static class ProductCatCode {
        public static final String TRAVEL = "TRAVEL";
        public static final String WELLNESS = "WELLNESS";
        public static final String CARD = "CARD";
        public static final String POS = "POS";
    }

    public static class Ecommerce {
        public static final String TAOBAO = "TAOBAO";
    }

    public static class LeaderGroup {
        public static final String RAY = "f1b4067d6cf68f32016cf75566bc2cac";
        public static final String RICKY = "f1b4067d6d814d2d016d8255319c573b";
        public static final String CHRIS = "f1b4067d6e45c650016e462687bc24dd";
    }

    public static final BigDecimal GGS_PROCESSING_FEE = new BigDecimal("0.5");

    public static class Language {
        public static final String ENGLISH = "en";
        public static final String CHINESE = "zh";
        public static final String GERMAN = "de";
        public static final String FRENCH = "fr";
        public static final String ITALIAN = "it";
        public static final String JAPANESE = "ja";
        public static final String KOREAN = "ko";
        public static final String MALAY = "ms";
        public static final String THAI = "th";
        public static final String VIETNAMESE = "vi";
    }

    public static class OmnicplusUserType {
        public static final String USER_TYPE_NOT_REGISTERED = "NONE";
        public static final String USER_TYPE_INVALID = "INVALID";
        public static final String USER_TYPE_ERROR = "ERROR";
        public static final String USER_TYPE_VALID = "VALID";
    }

    public static class MessageType {
        public static final String FOLLOWING = "FOLLOWING";
        public static final String ANNOUNCEMENT = "ANNOUNCEMENT";
        public static final String WARNING = "WARNING";
        public static final String REPLY_WARNING = "REPLY_WARNING";
    }

    public static class TopUpOptionType {
        public static final String RAZER = "RAZER";
        public static final String OMC = "OMC";
    }

    public static class EmailType {
        public static final String ADJUST_RANK = "ADJUST_RANK";
        public static final String SIGNING_GUILD = "SIGNING_GUILD";
        public static final String CASTING = "CASTING";
    }
}
