package com.compalsolutions.compal.user.service;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.compalsolutions.compal.exception.InvalidCredentialsException;
import com.compalsolutions.compal.function.user.service.impl.UserDetailsServiceImpl;
import com.compalsolutions.compal.security.ExtGrantedAuthority;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

@Service(RemoteUserDetailsService.BEAN_NAME)
public class RemoteUserDetailsServiceImpl extends UserDetailsServiceImpl {
    private static final Log log = LogFactory.getLog(RemoteUserDetailsServiceImpl.class);

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
        UserDetails userDetails = super.loadUserByUsername(username);

        if (userDetails instanceof RemoteAgentUser) {
            log.debug("Remote user " + userDetails.getUsername() + " login...");
            RemoteAgentUser remoteAgentUser = (RemoteAgentUser) userDetails;

            remoteAgentUser.getAuthorities().remove(new ExtGrantedAuthority("ROLE_USER"));
            remoteAgentUser.getAuthorities().add(new ExtGrantedAuthority("ROLE_REMOTE_AGENT"));

            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

            if (!checkAllowIp(remoteAgentUser.getAllowIp(), request.getRemoteAddr())) {
                throw new InvalidCredentialsException("Invalid Credentials");
            }
        } else {
            log.debug("Web user " + userDetails.getUsername() + " login...");
        }

        return userDetails;
    }

    private boolean checkAllowIp(String allowIp, String remoteIp) {
        if (StringUtils.isBlank(allowIp))
            return true;

        String[] ips = StringUtils.split(allowIp, ",");
        for (String ip : ips) {
            ip = StringUtils.trim(ip);
            if (ip.equals(remoteIp))
                return true;
        }

        return false;
    }
}
