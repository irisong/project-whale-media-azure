package com.compalsolutions.compal.user.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.MemberUser;

@Component(MemberUserDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class MemberUserDaoImpl extends Jpa2Dao<MemberUser, String> implements MemberUserDao {
    public MemberUserDaoImpl() {
        super(new MemberUser(false));
    }

    @Override
    public MemberUser findMemberUserByMemberId(String memberId) {
        MemberUser example = new MemberUser(false);
        example.setMemberId(memberId);
        return findUnique(example);
    }
}
