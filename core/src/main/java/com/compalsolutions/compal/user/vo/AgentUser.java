package com.compalsolutions.compal.user.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.compalsolutions.compal.Global.UserType;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.function.user.vo.User;

@Entity
@Table(name = "agent_user")
public class AgentUser extends User {
    private static final long serialVersionUID = 1L;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @Column(name = "super_user", nullable = false)
    private Boolean superUser;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    public AgentUser() {
    }

    public AgentUser(String userId) {
        super(userId);
    }

    public AgentUser(boolean defaultValue) {
        super(defaultValue);
        if (defaultValue) {
            userType = UserType.AGENT;
            superUser = false;
        }
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public Boolean getSuperUser() {
        return superUser;
    }

    public void setSuperUser(Boolean superUser) {
        this.superUser = superUser;
    }
}
