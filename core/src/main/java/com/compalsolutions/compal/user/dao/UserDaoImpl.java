package com.compalsolutions.compal.user.dao;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.function.user.dao.impl.UserDetailsDaoImpl;

@Component(UserDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserDaoImpl extends UserDetailsDaoImpl implements UserDao {
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(UserDaoImpl.class);
}
