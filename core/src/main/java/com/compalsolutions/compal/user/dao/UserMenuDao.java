package com.compalsolutions.compal.user.dao;

import java.util.List;

import com.compalsolutions.compal.function.user.dao.UserMenuFrameworkDao;
import com.compalsolutions.compal.function.user.vo.UserMenu;

public interface UserMenuDao extends UserMenuFrameworkDao {
    public static final String BEAN_NAME = "userMenuDao";

    public void truncateAllMenu();

    public List<UserMenu> findAllMainUserMenu();

    public List<UserMenu> findLevel3MenusByMainMenu(Long mainMenuId);

    public List<UserMenu> findLevel3MenusByLevel2Menu(Long menuId);

    public List<UserMenu> findLevel2Menus(Long mainMenuId);
}