package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.MemberUser;

public interface MemberUserDao extends BasicDao<MemberUser, String> {
    public static final String BEAN_NAME = "memberUserDao";

    public MemberUser findMemberUserByMemberId(String memberId);
}
