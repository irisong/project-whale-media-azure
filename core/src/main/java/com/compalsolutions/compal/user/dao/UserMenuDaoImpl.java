package com.compalsolutions.compal.user.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.function.user.dao.impl.UserMenuFrameworkDaoImpl;
import com.compalsolutions.compal.function.user.vo.UserMenu;

@Component(UserMenuDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserMenuDaoImpl extends UserMenuFrameworkDaoImpl implements UserMenuDao {
    private static final Log log = LogFactory.getLog(UserMenuDaoImpl.class);

    public void truncateAllMenu() {
        log.info("truncateAllMenu");

        // stupid workaround for 'parentId DESC NULLS LAST'
        List<UserMenu> userMenus = findByExample(new UserMenu(false), "parentId DESC");
        UserMenu nullParentMenu = null;
        for (UserMenu userMenu : userMenus) {
            if (userMenu.getParentId() != null)
                delete(userMenu);
            else
                nullParentMenu = userMenu;
        }
        if (nullParentMenu != null)
            delete(nullParentMenu);
    }

    public List<UserMenu> findAllMainUserMenu() {
        String hql = "FROM menu IN " + UserMenu.class + " WHERE menu.status = ? AND menu.parentId IS NULL " + " ORDER BY menu.sortSeq";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);

        return findQueryAsList(hql, params.toArray());
    }

    public List<UserMenu> findLevel3MenusByMainMenu(Long mainMenuId) {
        String hql = "FROM menu IN " + UserMenu.class + " WHERE menu.status = ? and menu.treeLevel = ? and menu.treeConfig like ? " + " ORDER BY menu.sortSeq";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);
        params.add(3);
        params.add(mainMenuId.intValue() + "|%");

        return findQueryAsList(hql, params.toArray());
    }

    @Override
    public List<UserMenu> findLevel3MenusByLevel2Menu(Long menuId) {
        String hql = "FROM menu IN " + UserMenu.class + " WHERE menu.status = ? and menu.treeLevel = ? and menu.treeConfig like ? " + " ORDER BY menu.sortSeq";

        List<Object> params = new ArrayList<Object>();
        params.add(Global.Status.ACTIVE);
        params.add(3);
        params.add("%|" + menuId.intValue());

        return findQueryAsList(hql, params.toArray());
    }

    public List<UserMenu> findLevel2Menus(Long mainMenuId) {
        List<Object> params = new ArrayList<Object>();

        String hql = "FROM menu IN " + UserMenu.class + " WHERE menu.status = ? and menu.treeLevel = ?  and menu.treeConfig=? " //
                + " ORDER BY menu.sortSeq";

        params.add(Global.Status.ACTIVE);
        params.add(2);
        params.add("" + mainMenuId.intValue());

        return findQueryAsList(hql, params.toArray());
    }
}