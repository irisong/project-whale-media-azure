package com.compalsolutions.compal.user.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.user.vo.AgentUser;

@Component(AgentUserDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class AgentUserDaoImpl extends Jpa2Dao<AgentUser, String> implements AgentUserDao {
    public AgentUserDaoImpl() {
        super(new AgentUser(false));
    }

    @Override
    public AgentUser findSuperAgentUserByAgentId(String agentId) {
        AgentUser example = new AgentUser(false);
        example.setSuperUser(true);
        example.setAgentId(agentId);
        return findUnique(example);
    }

    @Override
    public AgentUser findAgentUserByAgentId(String agentId) {
        AgentUser example = new AgentUser(false);
        example.setAgentId(agentId);
        return findUnique(example);
    }
}
