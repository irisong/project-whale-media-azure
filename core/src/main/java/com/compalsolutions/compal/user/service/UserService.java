package com.compalsolutions.compal.user.service;

import java.util.List;
import java.util.Locale;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.function.user.vo.*;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;

public interface UserService {
    public static final String BEAN_NAME = "userService";

    public void findUserRolesForListing(DatagridModel<UserRole> datagridModel, String roleName, String roleDesc, String roleName2, String roleDesc2,
            String userType, String status);

    public void findUserRolesForAutoComplete(DatagridModel<UserRole> datagridModel, String roleName);

    public void doTruncateAllAccessAndMenu();

    public void saveAllAccessCatsIfNotExist(List<UserAccessCat> accessCats);

    public void saveAllAccessIfNotExist(List<UserAccess> userAccesses);

    public void saveAllMenusIfNotExist(List<UserMenu> userMenus);

    /**
     * This is 1st level of menu (normally is TAB)
     */
    public List<UserMenu> findAuthorizedUserMenu(String userId);

    public List<UserMenu> findAuthorizedLevel2Menu(String userId, Long mainMenuId);

    /**
     * find Default for user in this system<br/>
     * It is retrieve from sysparam code ='EXM10002'
     * 
     * @return
     */
    public List<UserRole> findDefaultUserRoles(String compId);

    public boolean checkIsDefaultUserRole(String compId, String roleId);

    public void doInitDataUpdateAdminUserPrimaryKey();

    public void updateAdminUser(AdminUser user, Long version, List<UserRole> userRoles);

    public void updateAdminUserProfile(Locale locale, AdminUser adminUser);

    public boolean checkUserAvailability(Locale locale, String phoneNo);

    public String getUserType(String phoneNo);

    public void updateUser(User user);
}
