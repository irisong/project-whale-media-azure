package com.compalsolutions.compal.user.service;

import com.compalsolutions.compal.function.user.service.UserDetailsService;

public interface RemoteUserDetailsService extends UserDetailsService {
    public static final String BEAN_NAME = "remoteUserDetailsService";
}
