package com.compalsolutions.compal.user.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Entity
@Table(name = "remote_agent_user")
public class RemoteAgentUser extends User {
    private static final long serialVersionUID = 1L;

    @Column(name = "agent_id", nullable = false, length = 32)
    private String agentId;

    @ManyToOne
    @JoinColumn(name = "agent_id", insertable = false, updatable = false, nullable = true)
    private Agent agent;

    @ToUpperCase
    @Column(name = "allowIp", length = 255)
    protected String allowIp;

    public RemoteAgentUser() {
    }

    public RemoteAgentUser(boolean defaultValue) {
        super(defaultValue);
        if (defaultValue) {
        }
    }

    public String getAllowIp() {
        return allowIp;
    }

    public void setAllowIp(String allowIp) {
        this.allowIp = allowIp;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }
}
