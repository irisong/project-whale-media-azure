package com.compalsolutions.compal.user.vo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.compalsolutions.compal.Global.UserType;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.vo.Member;

@Entity
@Table(name = "member_user")
public class MemberUser extends User {
    private static final long serialVersionUID = 1L;

    @Column(name = "member_id", unique = true, nullable = false, length = 32)
    private String memberId;

    @OneToOne
    @JoinColumn(name = "member_id", insertable = false, updatable = false, nullable = true)
    private Member member;

    public MemberUser() {
    }

    public MemberUser(String userId) {
        super(userId);
    }

    public MemberUser(boolean defaultValue) {
        super(defaultValue);
        if (defaultValue) {
            userType = UserType.MEMBER;
        }
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
