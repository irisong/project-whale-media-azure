package com.compalsolutions.compal.user.dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.aop.AppDetailsBaseAdvice;
import com.compalsolutions.compal.function.user.dao.mysql.UserDetailsSqlDaoImpl;

@Component(UserSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class UserSqlDaoImpl extends UserDetailsSqlDaoImpl implements UserSqlDao {
    protected static final String CREATE_TEMP_USER_LOCATION = "";

    public boolean isMainMenuHasNonAuthSubMenu(String userId, Long menuId) {
        String sql = "SELECT COUNT(*) FROM app_user_menu WHERE TREE_LEVEL = ? AND TREE_CONFIG LIKE ? AND STATUS = ? AND IS_AUTH_NEEDED=false ";
        List<Object> params = new ArrayList<Object>();
        params.add(3);
        params.add(menuId.intValue() + "|%");
        params.add(Global.Status.ACTIVE);

        int count = queryForInt(sql, params.toArray());

        if (count > 0)
            return true;

        return false;
    }

    public boolean isLevel2MenuHasNonAuthSubMenu(String userId, Long level2MenuId) {
        String sql = "SELECT COUNT(*) FROM app_user_menu WHERE TREE_LEVEL = ? AND TREE_CONFIG LIKE ? AND STATUS = ? AND IS_AUTH_NEEDED=false ";
        List<Object> params = new ArrayList<Object>();
        params.add(3);
        params.add("%|" + level2MenuId.intValue());
        params.add(Global.Status.ACTIVE);

        int count = queryForInt(sql, params.toArray());

        if (count > 0)
            return true;

        return false;
    }

    @Override
    public void initDataUpdateAdminUserPrimaryKey() {
        String sql = "INSERT INTO admin_user(\n" //
                + " email, fullname, user_id)\n" //
                + " VALUES ('', 'ADMIN', ?) ";
        update(sql, AppDetailsBaseAdvice.SYSTEM_USER_ID);
    }
}
