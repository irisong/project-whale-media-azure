package com.compalsolutions.compal.user.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.user.vo.AgentUser;

public interface AgentUserDao extends BasicDao<AgentUser, String> {
    public static final String BEAN_NAME = "agentUserDao";

    public AgentUser findSuperAgentUserByAgentId(String agentId);

    public AgentUser findAgentUserByAgentId(String agentId);
}
