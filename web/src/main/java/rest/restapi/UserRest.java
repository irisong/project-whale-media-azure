package rest.restapi;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.identity.service.IdentityService;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.member.vo.MemberReport;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;


import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.MemberMnemonic;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.sms.service.SmsService;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.web.RestLoginInfo;

import org.springframework.core.env.Environment;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.*;

@Path("user")
@OpenAPIDefinition(
        tags = {@Tag(name = "Basic", description = "Authenticate user and setup login credential")})
public class UserRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(UserRest.class);

    private static final int TOKEN_DURATION = 30 * 24 * 60; // in minutes (1 month)
    private static final int SMS_DURATION = 30; // in minutes

    private TokenProvider tokenProvider;
    private SessionLogService sessionLogService;
    private UserDetailsService userDetailsService;
    private MemberService memberService;
    private SmsService smsService;
    private IdentityService identityService;
    private CryptoProvider cryptoProvider;
    private NotificationService notificationService;
    private UserService userService;
    private CountryService countryService;
    private LiveStreamProvider liveStreamProvider;
    private RankService rankService;
    private BroadcastService broadcastService;
    private SystemConfigService systemConfigService;
    private I18n i18n;
    private LiveStreamService liveStreamService;
    private LiveChatRoomProvider liveChatRoomProvider;
    private boolean isProdServer;

    public UserRest() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
        sessionLogService = Application.lookupBean(SessionLogService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        memberService = Application.lookupBean(MemberService.class);
        smsService = Application.lookupBean(SmsService.class);
        identityService = Application.lookupBean(IdentityService.class);
        cryptoProvider = Application.lookupBean(CryptoProvider.class);
        notificationService = Application.lookupBean(NotificationService.class);
        userService = Application.lookupBean(UserService.class);
        countryService = Application.lookupBean(CountryService.class);
        i18n = Application.lookupBean(I18n.class);
        liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        broadcastService = Application.lookupBean(BroadcastService.class);
        rankService = Application.lookupBean(RankService.class);
        Environment env = Application.lookupBean(Environment.class);
        isProdServer = env.getProperty("server.production", Boolean.class, true);
        systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.BEAN_NAME, LiveStreamService.class);
        liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.BEAN_NAME, LiveChatRoomProvider.class);
    }

    @Tag(name = "Basic")
    @POST
    @Path("/authenticate")
    @PermitAll
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Operation(summary = "Authenticate user", description = "Get user authentication token",
            responses = {
                    @ApiResponse(responseCode = "200", description = "User credential"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized")}
    )
    public Response authenticate(
            @HeaderParam("user-agent") String userAgent,
            @RequestBody(description = "Insert user credential to obtain user token") Credentials credentials) {
        VoUtil.processProperties(credentials);

        Response.Status errorCode = Response.Status.UNAUTHORIZED;

        SessionLog sessionLog = new SessionLog(true);
        sessionLog.setLoginType(SessionLog.LOGIN_TYPE_REST);
        sessionLog.setIpAddress(RestUtil.getClientIpAddress());
        sessionLog.setUsername(credentials.getUsername());
        String memberId;
        boolean forceLogin = false;
        try {
            Locale locale = RestUtil.getUserLocale();
            if (isProdServer) {
                SystemConfig systemConfig = systemConfigService.getDefaultSystemConfig();
                if (systemConfig != null && systemConfig.getSystemDown() != null && systemConfig.getSystemDown()) {
                    throw new ValidatorException(i18n.getText("systemUnderMaintenance", locale));
                }
            }

            User user = tokenProvider.authenticate(locale, userAgent, credentials.getUsername(), credentials.getPassword(),
                    credentials.getServiceCode(), credentials.getLoginKey(), TOKEN_DURATION, credentials.getDevice());
            if (user != null) {
                MemberUser memberUser = (MemberUser) user;
                memberId = memberUser.getMemberId();
                if (credentials.getForceLogin() != null && StringUtils.isNotBlank(credentials.getForceLogin()) && credentials.getForceLogin().equalsIgnoreCase("1")) {
                    forceLogin = true;
                    LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannelByMemberId(memberId, false);
                    NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
                    if (account != null) {
                        liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", liveStreamChannel.getChannelId(), account.getAccountId(), "ADMIN", "", "", "MULTIPLE_LOGIN", "", null, "", 0);
                    }
                }

                boolean isActiveLive = liveStreamProvider.doCheckActiveLiveStream(memberId);
                if (isActiveLive && !forceLogin) {
                    errorCode = Response.Status.EXPECTATION_FAILED;
                    throw new ValidatorException(i18n.getText("errorMessage.member.onLive", locale));
                }
            }
            Pair<JwtToken, JwtClaims> pair = tokenProvider.generateJwtTokenWithLoggedInUser(locale, userAgent, user, TOKEN_DURATION, credentials.getDevice(), forceLogin);

//            Triple<JwtToken, JwtClaims, User> triple = tokenProvider.authenticate(locale, userAgent, credentials.getUsername(), credentials.getPassword(),
//                    credentials.getServiceCode(), credentials.getLoginKey(), TOKEN_DURATION, credentials.getDevice());

            sessionLog.setUserId(user.getUserId());
            sessionLogService.saveSessionLog(sessionLog);
            Member member = getMember(user.getUserId());
            NotificationAccount notificationAccount = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);

            if (member.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                throw new ValidatorException(i18n.getText("errorMessage.member.inactive", locale));
            }
            return ResponseBuilder.newInstance() //
                    .setAttribute("notifToken", notificationAccount != null ? notificationAccount.getToken() : "")
                    .setAttribute("notifAccId", notificationAccount != null ? notificationAccount.getAccountId() : "")
                    .setAttribute("token", pair.getLeft().getToken())
                    .setAttribute("refreshToken", pair.getLeft().getRefreshToken())
                    .setAttribute("claims", pair.getRight().getClaimsMap())
                    .setAttribute("status", getMemberStatus(user.getUserId()))
                    .setAttribute("language", member.getPreferLanguage())
                    .createResponse();
        } catch (Exception ex) {
            sessionLog.setLoginStatus(SessionLog.LOGIN_STATUS_FAILED);
            sessionLogService.saveSessionLog(sessionLog);
            return ResponseBuilder.build(errorCode, ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @GET
    @Path("/verifyToken")
    @PermitAll
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response verifyToken(@QueryParam("token") String token) {
        Locale locale = RestUtil.getUserLocale();
        try {
            Pair<JwtClaims, JwtToken> pair = tokenProvider.validateJwtToken(locale, token);
            Member member = getMember(pair.getRight().getUserId());

            if (member.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                throw new ValidatorException(i18n.getText("errorMessage.member.inactive", locale));
            }
            NotificationAccount notificationAccount = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);

            return ResponseBuilder.newInstance() //
                    .setAttribute("notifToken", notificationAccount != null ? notificationAccount.getToken() : "")
                    .setAttribute("notifAccId", notificationAccount != null ? notificationAccount.getAccountId() : "")
                    .setAttribute("token", pair.getRight().getToken())
                    .setAttribute("claims", pair.getLeft().getClaimsMap())
                    .setAttribute("status", getMemberStatus(pair.getRight().getUserId()))
                    .setAttribute("language", member.getPreferLanguage())
                    .createResponse();
        } catch (InvalidJwtException ex) {
            return ResponseBuilder.build(Response.Status.UNAUTHORIZED, i18n.getText("invalidToken", locale));
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.UNAUTHORIZED, ex.getMessage());
        }
    }

    private String getMemberStatus(String userId) {
        User user = userDetailsService.findUserByUserId(userId);

        if (user == null) {
            throw new ValidatorException("Invalid user");
        }

        boolean isTransactionPassword = false;
        boolean isMember = user instanceof MemberUser;
        Member member = null;
        if (!isMember) {
            throw new ValidatorException("This api for member only");
        } else {
            member = ((MemberUser) user).getMember();
//            isTransactionPassword = member.getTransactionPassword();
        }

        return isTransactionPassword ? Global.Status.ACTIVE : Global.Status.PENDING_TRANSACTION_PASSWORD;
    }

    private Member getMember(String userId) {
        User user = userDetailsService.findUserByUserId(userId);

        if (user == null) {
            throw new ValidatorException("Invalid user");
        }

        boolean isMember = user instanceof MemberUser;
        Member member = null;
        if (!isMember) {
            throw new ValidatorException("This api for member only");
        } else {
            member = memberService.getMember(((MemberUser) user).getMemberId());
        }
        return member;
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/token")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response generateNewToken(@HeaderParam("user-agent") String userAgent, Credentials credentials) {
        VoUtil.processProperties(credentials);

        try {
            Pair<JwtToken, JwtClaims> pair = tokenProvider.generateJwtTokenWithRefreshToken(RestUtil.getUserLocale(), userAgent, credentials.getRefreshToken(),
                    TOKEN_DURATION);

            Member member = getMember(pair.getLeft().getUserId());
            NotificationAccount notificationAccount = notificationService.findNotifAccByMemberAndAccessModule(member.getMemberId(), Global.AccessModule.WHALE_MEDIA);

            return ResponseBuilder.newInstance() //
                    .setAttribute("notifToken", notificationAccount != null ? notificationAccount.getToken() : "")
                    .setAttribute("notifAccId", notificationAccount != null ? notificationAccount.getAccountId() : "")
                    .setAttribute("token", pair.getLeft().getToken())
                    .setAttribute("refreshToken", pair.getLeft().getRefreshToken())
                    .setAttribute("claims", pair.getRight().getClaimsMap())
                    .setAttribute("status", getMemberStatus(pair.getLeft().getUserId()))
                    .setAttribute("language", member.getPreferLanguage())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/user")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response createUser(Credentials credentials) {
        VoUtil.processProperties(credentials);

        try {
            Locale locale = RestUtil.getUserLocale();

            memberService.doCreateSimpleMember(locale, credentials.getPhoneno(), credentials.getPassword(), credentials.getVerifyCode(),
                    SMS_DURATION, false);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("userCreatedSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/generateVerifySms")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response generateVerifySms(Credentials credentials) {
        VoUtil.processProperties(credentials);

        try {
            Locale locale = RestUtil.getUserLocale();

            smsService.doGenerateVerifySms(locale, credentials.getPhoneno());
            return ResponseBuilder.build(Response.Status.OK, i18n.getText("smsVerificationCodeGenerateSuccessful", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/verifySms")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response verifySms(Credentials credentials) {
        VoUtil.processProperties(credentials);

        try {
            Locale locale = RestUtil.getUserLocale();

            smsService.doVerifyRegistrationVerifyCodeWithoutUpdateStatus(locale, credentials.getPhoneno(), credentials.getVerifyCode(), SMS_DURATION);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("smsVerificationCodeIsValid", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @GET
    @Path("/phonenoAvailable")
    @PermitAll
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response isPhonenoAvailable(@QueryParam("phoneno") String phoneno) {
        Locale locale = RestUtil.getUserLocale();
        boolean isAvailable;

        if (StringUtils.isBlank(phoneno)) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, i18n.getText("invalidPhoneno", locale));
        }

        phoneno = StringUtils.upperCase(phoneno);
        isAvailable = userService.checkUserAvailability(locale, phoneno);

        return ResponseBuilder.newInstance().setAttribute("available", isAvailable).createResponse();
    }

    @Tag(name = "Basic")
    @GET
    @Path("/_userInformation")
    @PermitAll
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getUserInformation(@QueryParam("phoneno") String phoneno) {
        Locale locale = RestUtil.getUserLocale();

        if (StringUtils.isBlank(phoneno)) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, i18n.getText("invalidPhoneno", locale));
        }

        phoneno = StringUtils.upperCase(phoneno);

        Member member = memberService.findMemberByMemberCode(phoneno);
        boolean isRegistered = member != null;

        return ResponseBuilder.newInstance() //
                .setAttribute("registered", isRegistered) //
                .setAttribute("fullName", isRegistered ? member.getFullName() : "") //
                .setAttribute("icno", isRegistered ? member.getIdentityNo() : "") //
                .createResponse();
    }

    @Tag(name = "Authorized-User")
    @GET
    @Path("/transactionPasswordSetup")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response isTransactionPasswordSetup() {
        Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());

        return ResponseBuilder.newInstance() //
//                .setAttribute("status", member.getTransactionPassword()) //
                .createResponse();
    }

    @Tag(name = "Authorized-User")
    @POST
    @Path("/transactionPasswordSetup")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response setupTransactionPassword(Credentials credentials) {
        try {
            VoUtil.processProperties(credentials);
            Locale locale = RestUtil.getUserLocale();

            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            User user = RestUtil.getLoginUser();

            if (!WebUtil.isMember(RestUtil.getLoginInfo())) {
                throw new Exception("Invalid user");
            }

            MemberMnemonic memberMnemonic = memberService.doCreateMnemonic(locale, member.getMemberId(), user.getUserId(),
                    credentials.getTransactionPassword());

            String[] mnemonicArray = StringUtils.split(memberMnemonic.getMnemonic());

            return ResponseBuilder.newInstance() //
                    .addMessage(i18n.getText("transactionPasswordIsSetSuccessfully", locale)) //
                    .setAttribute("mnemonic", memberMnemonic.getMnemonic()) //
                    .setAttribute("length", memberMnemonic.getMnemonicLength()) //
                    .setAttribute("mnemonicArray", mnemonicArray) //
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @GET
    @Path("/emailAvailable")
    @PermitAll
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response isEmailAvailable(@QueryParam("email") String email) {
        Locale locale = RestUtil.getUserLocale();
        boolean isAvailable = false;

        if (StringUtils.isBlank(email)) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, i18n.getText("invalidEmailAddress", locale));
        }

        email = StringUtils.upperCase(email);
        isAvailable = userDetailsService.isUsernameAvailable(null, email, true);

        return ResponseBuilder.newInstance().setAttribute("available", isAvailable).createResponse();
    }

    @Tag(name = "Authorized-User")
    @POST
    @Path("/logout")
    @PermitAll
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response logout() {
        try {
            RestLoginInfo loginInfo = (RestLoginInfo) RestUtil.getLoginInfo();
            Locale locale = RestUtil.getUserLocale();
            if (loginInfo != null) {
                tokenProvider.logoutToken(loginInfo.getTokenId());
            }
            return ResponseBuilder.build(Response.Status.OK, i18n.getText("logoutSuccess", locale));
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @Tag(name = "Authorized-User")
    @POST
    @Path("/changePassword")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response changePassword(ChangePasswordBO changePasswordBO) {
        VoUtil.processProperties(changePasswordBO);
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            memberService.doChangeMemberPassword(locale, member.getMemberId(), changePasswordBO.getOldPassword(), changePasswordBO.getNewPassword());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("passwordChangedSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @Tag(name = "Authorized-User")
    @POST
    @Path("/changeTransactionPassword")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response changeTransactionPassword(ChangeTransactionPasswordBO changeTransactionPasswordBO) {
        VoUtil.processProperties(changeTransactionPasswordBO);
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            memberService.doChangeMemberSecondPassword(locale, member.getMemberId(), changeTransactionPasswordBO.getOldTransactionPassword(),
                    changeTransactionPasswordBO.getNewTransactionPassword());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("transactionPasswordChangedSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    private static final String LIST_GET_PROFILE_INCLUDE = "countryOptionList, "
            + "countryOptionList\\[\\d+\\], "
            + "countryOptionList\\[\\d+\\].key, "
            + "countryOptionList\\[\\d+\\].value, "
            + "memberId, "
            + "profilePicture, "
            + "username, "
            + "phoneNo, "
            + "email, "
            + "gender, "
            + "country, "
            + "birthday, "
            + "bio, "
            + "sendNotifComment, "
            + "sendNotifLike, "
            + "sendNotifFollower, "
            + "sendNotifFollowing, "
            + "whaleliveId,"
            + "fullName,"
            + "identity_no,"
            + "ic_front, "
            + "ic_back, "
            + "passport, "
            + "requiredExpToNextRank, "
            + "currentRank, "
            + "currentExp, "
            + "guildStatus, "
            + "kycStatus, "
            + "kycDescription.*, "
            + "nextLevel,"
            + "kycRemark, "
            + "fansBadgeExist, "
            + "rankDesc";

    @Tag(name = "Authorized-User")
    @GET
    @Path("/profile")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getUserProfile() {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            MemberDetail memberDetail = member.getMemberDetail();

            MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_PROFILE_PIC);
            MemberFile memberIC_F = memberService.findMemberFileByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_IC_FRONT);
            MemberFile memberIC_B = memberService.findMemberFileByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_IC_BACK);
            MemberFile memberPassport = memberService.findMemberFileByMemberIdAndType(memberId, MemberFile.UPLOAD_TYPE_PASSPORT);
            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberApproved(memberId);

            final String parentUrl = cryptoProvider.getFileUploadUrl() + "/file/memberFile";
            if (memberFile != null) {
                memberFile.setFileUrlWithParentPath(parentUrl);
            }

            if (memberIC_F != null) {
                memberIC_F.setFileUrlWithParentPath(parentUrl);
            }

            if (memberIC_B != null) {
                memberIC_B.setFileUrlWithParentPath(parentUrl);
            }

            if (memberPassport != null) {
                memberPassport.setFileUrlWithParentPath(parentUrl);
            }

            List<CountryDesc> countryDescList = countryService.findCountryDescsByLocale(locale);
            List<OptionBean> countryOptionList = countryDescList.stream()
                    .map(countryDesc -> new OptionBean(countryDesc.getCountryCode(), countryDesc.getCountryName()))
                    .collect(Collectors.toList());
            Integer currentRank = member.getLevel();
            BigDecimal currentExp = member.getExperience();
            BigDecimal nextRankExp = rankService.getExperienceByRank(member.getLevel() + 1);
            int nextLevel = member.getLevel() + 1;

            BigDecimal requiredExp = nextRankExp.subtract(member.getExperience());
            if (BDUtil.leZero(requiredExp)) {
                requiredExp = BigDecimal.ZERO;
            }
            // change to totalExp due to system burden if compute level , exp each time process call
            // member.level, member.experience - TO be removed
            if (member.getTotalExp() != null && BDUtil.gZero(member.getTotalExp())) {
                Triple<BigDecimal, Integer, Integer>
                        rankDetails = rankService.getCurrentRankDetails(member.getTotalExp());
                nextLevel = rankDetails.getRight();
                requiredExp = rankDetails.getLeft();
                currentRank = rankDetails.getMiddle();
                RankConfig rankConfig = rankService.findRankByExperience(member.getExperience());
                if (rankConfig != null) {
                    currentExp = rankConfig.getPerExp().subtract(requiredExp);
                }

            }

            Map<String, String> kycDescription = null;
            String kysStatus = StringUtils.isBlank(member.getKycStatus()) ? Member.KYC_STATUS_PENDING_UPLOAD : member.getKycStatus();
            if (kysStatus.equalsIgnoreCase(Member.KYC_STATUS_PENDING_UPLOAD) || kysStatus.equalsIgnoreCase(Member.KYC_STATUS_PENDING_APPROVAL)) {
                kycDescription = new HashMap<>();
                kycDescription.put(Global.Language.ENGLISH, i18n.getText("KYCApprovalPeriodDescription", Locale.ENGLISH));
                kycDescription.put(Global.Language.CHINESE, i18n.getText("KYCApprovalPeriodDescription", Locale.CHINESE));
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_GET_PROFILE_INCLUDE) //to filter countryOptionList.extra
                    .setAttribute("countryOptionList", countryOptionList)
                    .setAttribute("memberId", memberId)
                    .setAttribute("profilePicture", memberFile != null ? memberFile.getFileUrl() : null)
                    .setAttribute("ic_front", memberIC_F != null ? memberIC_F.getFileUrl() : null)
                    .setAttribute("ic_back", memberIC_B != null ? memberIC_B.getFileUrl() : null)
                    .setAttribute("passport", memberPassport != null ? memberPassport.getFileUrl() : null)
                    .setAttribute("username", memberDetail.getProfileName())
                    .setAttribute("phoneNo", memberDetail.getContactNo())
                    .setAttribute("email", memberDetail.getEmail())
                    .setAttribute("gender", memberDetail.getGender())
                    .setAttribute("country", memberDetail.getCountryCode())
                    .setAttribute("birthday", memberDetail.getDob())
                    .setAttribute("bio", memberDetail.getProfileBio())
                    .setAttribute("whaleliveId", memberDetail.getWhaleliveId())
                    .setAttribute("sendNotifComment", member.getSendNotifComment())
                    .setAttribute("sendNotifLike", member.getSendNotifLike())
                    .setAttribute("sendNotifFollower", member.getSendNotifFollower())
                    .setAttribute("sendNotifFollowing", member.getSendNotifFollowing())
                    .setAttribute("fullName", member.getFullName())
                    .setAttribute("identity_no", member.getIdentityNo())
                    .setAttribute("requiredExpToNextRank", requiredExp)
                    .setAttribute("currentRank", currentRank)
                    .setAttribute("nextLevel", nextLevel)
                    .setAttribute("currentExp", currentExp)
                    .setAttribute("kycStatus", kysStatus)
                    .setAttribute("kycDescription", kycDescription)
                    .setAttribute("kycRemark", member.getKycRemark())
                    .setAttribute("rankDesc", i18n.getText("levelDesc", locale))
                    .setAttribute("guildStatus", broadcastGuildMember != null ? BroadcastGuildMember.STATUS_APPROVED :
                            BroadcastGuildMember.STATUS_NEW)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Authorized-User")
    @POST
    @Path("/profile")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response updateProfile(ProfileVo profileVo) {
        VoUtil.processProperties(profileVo);

        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            Locale locale = RestUtil.getUserLocale();

            memberService.doUpdateUserProfile(locale, memberId, profileVo.getUsername(), profileVo.getEmail(),
                    profileVo.getGender(), profileVo.getCountry(), profileVo.getBirthday(), profileVo.getBio());

            memberService.doUpdateMember(memberId, profileVo.getFullName(), profileVo.getIdentity_no());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("updateProfileSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/forgetPassword/request")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response forgetPasswordRequest(Credentials credentials) {
        VoUtil.processProperties(credentials);
        try {
            Locale locale = RestUtil.getUserLocale();
            memberService.doMemberSelfResetPasswordRequest(locale, credentials.getPhoneno());
            return ResponseBuilder.build(Response.Status.OK, i18n.getText("smsVerificationCodeGenerateSuccessful", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/forgetPassword")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response forgetPassword(Credentials credentials) {
        VoUtil.processProperties(credentials);
        try {
            Locale locale = RestUtil.getUserLocale();
            memberService.doMemberSelfResetPassword(locale, credentials.getPhoneno(), credentials.getVerifyCode(), credentials.getPassword(), SMS_DURATION);
            return ResponseBuilder.build(Response.Status.OK, i18n.getText("loginPasswordResetSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/changePassword/request")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response changePasswordRequest(ChangePasswordBO changePasswordBO) {
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        Locale locale = RestUtil.getUserLocale();
        try {
            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            memberService.doValidateMemberPassword(locale, member.getMemberId(), changePasswordBO.getOldPassword(), changePasswordBO.getNewPassword());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("passwordChangedSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @Tag(name = "Authorized-User")
    @GET
    @Path("/language")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response setUserLanguage(@QueryParam("lang") String language) {
        try {
            switch (language) {
                case Global.Language.ENGLISH:
                case Global.Language.CHINESE:
                case Global.Language.GERMAN:
                case Global.Language.FRENCH:
                case Global.Language.ITALIAN:
                case Global.Language.JAPANESE:
                case Global.Language.KOREAN:
                case Global.Language.MALAY:
                case Global.Language.THAI:
                case Global.Language.VIETNAMESE:
                    break;
                default:
                    throw new ValidatorException("Invalid Language");
            }
            Locale locale = new Locale(language);

            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            Member member = memberService.getMember(memberId);
            member.setPreferLanguage(language);
            memberService.updateMember(member);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("languageChangedSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Authorized-User")
    @GET
    @Path("/sendNotification")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response setSendNotificationSetting(@QueryParam("type") String type, @QueryParam("sendNotif") String sendNotif) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            if (StringUtils.isBlank(sendNotif) || (!sendNotif.equals("true") && !sendNotif.equals("false"))) {
                throw new ValidatorException(i18n.getText("invalidSetting", locale));
            }

            memberService.doUpdateMemberNotificationSetting(locale, memberId, type, Boolean.parseBoolean(sendNotif));

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("settingUpdatedSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/report/type")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getReportType() {
        try {
            Locale locale = RestUtil.getUserLocale();
            List<OptionBean> reportTypes;

            OptionBeanUtil options = new OptionBeanUtil(locale);
            if (Global.LOCALE_CN.equals(locale)) {
                reportTypes = options.getMemberReportTypeByLanguage(Global.Language.CHINESE, Global.Status.ACTIVE);
            } else if (Global.LOCALE_MS.equals(locale)) {
                reportTypes = options.getMemberReportTypeByLanguage(Global.Language.MALAY, Global.Status.ACTIVE);
            } else {
                reportTypes = options.getMemberReportTypeByLanguage(Global.Language.ENGLISH, Global.Status.ACTIVE);
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("reportType", reportTypes)
                    .setAttribute("maxUploadAllow", MemberReport.MAX_FILE_UPLOAD)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }
}
