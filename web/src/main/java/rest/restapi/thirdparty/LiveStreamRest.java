package rest.restapi.thirdparty;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.BannerService;
import com.compalsolutions.compal.general.service.LiveCategoryService;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.general.vo.BannerFile;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.chatroom.service.LiveChatService;
import com.compalsolutions.compal.live.chatroom.vo.LiveChatRoom;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.client.dto.Channel;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.dto.ProfileDto;
import com.compalsolutions.compal.member.service.*;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.websocket.service.WsMessageService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.*;
import rest.vo.point.SymbolicItemBO;
import rest.vo.thirdparty.LiveStreamBO;
import websocket.LiveStreamWebsocket;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;

@Path("livestream")
public class LiveStreamRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(LiveStreamRest.class);

    private BannerFileUploadConfiguration bannerFileUploadConfiguration;
    private MemberFileUploadConfiguration memberFileUploadConfiguration;
    private GiftFileUploadConfiguration config;
    private CryptoProvider cryptoProvider;
    private LiveStreamProvider liveStreamProvider;
    private LiveChatRoomProvider liveChatRoomProvider;
    private NotificationService notificationService;
    private LiveStreamService liveStreamService;
    private LiveChatService liveChatService;
    private LiveCategoryService liveCategoryService;
    private BannerService bannerService;
    private MemberService memberService;
    private MemberFansService memberFansService;
    private MemberBlockService memberBlockService;
    private RankService rankService;
    private LuckyDrawService luckyDrawService;
    private WsMessageService wsMessageService;
    private PointService pointService;
    private FansIntimacyService fansIntimacyService;
    private VjMissionService vjMissionService;
    private I18n i18n;

    public LiveStreamRest() {
        bannerFileUploadConfiguration = Application.lookupBean(BannerFileUploadConfiguration.class);
        memberFileUploadConfiguration = Application.lookupBean(MemberFileUploadConfiguration.class);
        config = Application.lookupBean(GiftFileUploadConfiguration.class);
        cryptoProvider = Application.lookupBean(CryptoProvider.class);
        liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.class);
        notificationService = Application.lookupBean(NotificationService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        liveChatService = Application.lookupBean(LiveChatService.class);
        liveCategoryService = Application.lookupBean(LiveCategoryService.class);
        bannerService = Application.lookupBean(BannerService.class);
        memberService = Application.lookupBean(MemberService.class);
        memberFansService = Application.lookupBean(MemberFansService.class);
        memberBlockService = Application.lookupBean(MemberBlockService.class);
        rankService = Application.lookupBean(RankService.class);
        luckyDrawService = Application.lookupBean(LuckyDrawService.class);
        wsMessageService = Application.lookupBean(WsMessageService.BEAN_NAME, WsMessageService.class);
        pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
        fansIntimacyService = Application.lookupBean(FansIntimacyService.class);
        vjMissionService = Application.lookupBean(VjMissionService.class);
        i18n = Application.lookupBean(I18n.class);
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/channelType")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getMemberChannelType() {
        try {
            Locale locale = RestUtil.getUserLocale();
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String memberId = WebUtil.getLoginMemberId(loginInfo);

            List<OptionBean> categoryList = null;
            boolean privateLive = false;
            boolean fansBadgeExist = false;
            String kycStatus = Member.KYC_STATUS_PENDING_UPLOAD;
            String kycRemark = "";

            Member member = memberService.getMember(memberId);

            if (member != null) {
                categoryList = liveCategoryService.getLiveCategoryOptions(locale, member.getLiveStreamTypes());
                privateLive = member.getLiveStreamPrivate();
                fansBadgeExist = !StringUtils.isBlank(member.getFansBadgeName());
                kycStatus = StringUtils.isBlank(member.getKycStatus()) ? Member.KYC_STATUS_PENDING_UPLOAD : member.getKycStatus();
                kycRemark = member.getKycRemark();
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("categoryList", categoryList)
                    .setAttribute("privateLive", privateLive)
                    .setAttribute("kycStatus", kycStatus)
                    .setAttribute("kycRemark", kycRemark)
                    .setAttribute("fansBadgeExist", fansBadgeExist)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/verifyPrivacyCode")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response verifyPrivacyCode(LiveStreamBO liveStreamBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannel(liveStreamBO.getChannelId(), null, false);
            if (liveStreamChannel != null) {
                if (liveStreamChannel.getPrivateLive()) {
                    if (!liveStreamBO.getPrivateAccessCode().equals(liveStreamChannel.getPrivateAccessCode())) {
                        throw new ValidatorException(i18n.getText("privacyLivePasscodeInvalid", locale));
                    }
                }
            } else {
                throw new ValidatorException(i18n.getText("liveStreamChannelNotFound", locale));
            }

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("privacyLivePasscodeValid", locale));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/vcloud/deleteChannel")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response deleteVcloudLiveStreamChannel(LiveStreamBO liveStreamBO) {
        try {
            liveStreamProvider.doCloseLiveStreamChannel(liveStreamBO.getChannelId(), liveStreamBO.getView(), liveStreamBO.getDuration());

            //todo: need to move websocket to CORE level in future
            // remove channel if VJ close streaming
            LiveStreamWebsocket liveStreamWebsocket = new LiveStreamWebsocket();
            liveStreamWebsocket.removeWholeChannel(liveStreamBO.getChannelId());

            return ResponseBuilder.newInstance()
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/vcloud/channellist")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getVcloudLiveStreamChannelList(LiveStreamBO liveStreamBO) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        Locale locale = RestUtil.getUserLocale();
        try {
            log.debug("====  GetVcloudLiveStreamChannelList  ===");
            DatagridModel<LiveStreamChannel> datagridModel = new DefaultDatagridModel<>();
            datagridModel.setCurrentPage(liveStreamBO.getPageNo());
            datagridModel.setPageSize(10);
            log.debug("==== Init DONE ===");
            List<Channel> channelList = liveStreamProvider.getChannelList(locale, datagridModel, memberId, liveStreamBO.getPageNo(), liveStreamBO.getCategory());
            log.debug("====channelList ===");
            boolean isCn = Global.LOCALE_CN.equals(locale);
            boolean isMs = Global.LOCALE_MS.equals(locale);
            String fileLanguage = isCn ? BannerFile.LANG_CN : isMs ? BannerFile.LANG_MS : BannerFile.LANG_EN;
            LiveCategory liveCategory = liveCategoryService.findCategoryByTypeByStatus(liveStreamBO.getCategory(), Global.Status.ACTIVE);
            log.debug("==== liveCategory ===" + liveCategory);
            List<BannerBO> bannerBOList = new ArrayList<>();

            if (liveCategory != null) {
                liveCategory.setCategory(isCn ? liveCategory.getCategoryCn() : isMs ? liveCategory.getCategoryMs() : liveCategory.getCategory());
                List<Banner> bannerList = bannerService.findBannerByCategory(liveCategory.getCategoryId());
                log.debug("==== bannerList ===" + bannerList);
                bannerList.forEach(banner -> {
                    BannerFile bannerFile = bannerService.findBannerFileByBannerId(banner.getBannerId(), fileLanguage);
                    BannerBO bannerBO = new BannerBO();
                    if (bannerFile != null) {
                        bannerBO.setFileUrl(bannerFile.getFileUrlWithParentPath(bannerFileUploadConfiguration.getFullBannerParentFileUrl()));
                    }
                    bannerBO.setBannerType(banner.getBannerType());
                    bannerBO.setRedirectUrl(banner.getRedirectUrl());
                    bannerBO.setTimer(banner.getTimer());
                    bannerBOList.add(bannerBO);
                });
                if (bannerList != null) {
                    bannerList.clear();
                    bannerList = null;
                }
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("banners", bannerBOList)
                    .setAttribute("channels", channelList)
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    //for mobile testing
    @POST
    @Path("/chatroom/blockStream")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response blockLiveStreamSendMsg(LiveStreamBO liveStreamBO) {
        try {
            Locale locale = RestUtil.getUserLocale();

            Member member = null;
            if (StringUtils.isNotBlank(liveStreamBO.getChannelName())) {
                member = memberService.findMemberByMemberCode(liveStreamBO.getChannelName());
            }

            LiveStreamChannel liveStreamChannel;
            if (liveStreamBO.getName().equalsIgnoreCase("BLOCK")) {
                liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(member.getMemberId(), Global.Status.ACTIVE);
                liveStreamService.doBlockLiveStreamChannel(liveStreamChannel, Global.Status.DISABLED, liveStreamBO.getAnnouncement());
            } else { //inactive (unblock)
                liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(member.getMemberId(), Global.Status.DISABLED);
                liveStreamService.doBlockLiveStreamChannel(liveStreamChannel, Global.Status.INACTIVE, null);
            }

            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
            if (account != null) {
                liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", liveStreamBO.getChannelId(), account.getAccountId(), "ADMIN", "", liveStreamBO.getAnnouncement(), "BLOCK_CHANNEL", "", null, "", 0);
            }

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("Msg sent successfully", locale));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/trackWatchingTime")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getWatchingLiveTime(LiveStreamBO liveStreamBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String memberId = WebUtil.getLoginMemberId(loginInfo);

            String refNo = liveStreamService.doProcessTrackWatchLiveTime(locale, memberId, liveStreamBO.getActionType(),
                    liveStreamBO.getChannelId(), liveStreamBO.getRefNo(), liveStreamBO.isOffline(), liveStreamBO.getDuration());

            try {
                wsMessageService.updateOnlineMember(liveStreamBO.getChannelId(), liveStreamBO.getActionType()); //send websocket to update online member list
            } catch (Exception e) {
                e.printStackTrace(); //dont throw API error
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("refNo", refNo)
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());

            try {
                wsMessageService.updateOnlineMember(liveStreamBO.getChannelId(), liveStreamBO.getActionType()); //send websocket to update online member list
            } catch (Exception e) {
                e.printStackTrace();
            }

            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    //REMARK: RETURN "channel = null" INSTEAD OF "Live Stream Not Found (Error 400)" FOR CHANNEL NOT FOUND
    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/init")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getInit(@QueryParam("channelId") String channelId) {
        try {
            Locale locale = RestUtil.getUserLocale();
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String memberId = WebUtil.getLoginMemberId(loginInfo);

            Member member = memberService.getMember(memberId);
            Channel channel = liveStreamProvider.getChannelInfo(locale, member.getMemberId(), channelId, member.getMemberCode());
            Triple<String, String, String> fansBadgeInfoTriple = new MutableTriple<>(null, null, null);

            String fanRoleUrl = null;
            boolean isFans = false;
            boolean isAdmin = false;
            boolean isSeniorAdmin = false;
            boolean isBlocked = false;
            boolean isMute = false;
            boolean hasVjMission = false;
            String vjMissionMessage = null;
            if (channel != null) {
                // create pointBalance first to avoid duplication when user SPAM gift later
                pointService.doCreatePointBalanceIfNotExists(channel.getMemberId(), memberId);
                liveChatService.doRemoveMuteUser(channelId, memberId); //unmute, if prev enter being mute
                liveStreamService.doAddChannelView(channelId);
                channel.setAccumulateView(channel.getAccumulateView() + 1);

                fansBadgeInfoTriple = memberFansService.getDisplayFansBadgeInfo(memberId, channel.getMemberId());

                MemberFans memberFans = memberFansService.findMemberFans(memberId, channel.getMemberId(), "");
                if (memberFans != null) {
                    isFans = true;
                    if (memberFans.getAdmin()) {
                        isAdmin = true;
                        fanRoleUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/admin.png";
                    } else if (memberFans.getSeniorAdmin()) {
                        isSeniorAdmin = true;
                        fanRoleUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/senior_admin.png";
                    }
                }

                isBlocked = memberBlockService.isMemberBlocked(channel.getMemberId(), memberId);
                isMute = liveChatService.isMemberMuted(locale, channel.getChannelId(), memberId);

                //check vj has set mission
                VjMission vjMission = vjMissionService.doGetLatestVjMissionByMemberId(channel.getMemberId());
                if (vjMission != null && vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)) {
                    hasVjMission = true;
                    String missionList = "";
                    for (VjMissionDetail detail : vjMission.getVjMissionDetailList()) {
                        if (StringUtils.isNotBlank(missionList))
                            missionList += "\n";

                        missionList += "- " + i18n.getText("vjMissionDetailDesc", locale, detail.getName(), detail.getPoint());
                    }
                    vjMissionMessage = i18n.getText("vjMissionDesc", locale) + "\n" + missionList;
                }
            }

            String levelUrl = cryptoProvider.getServerUrl() + "/asset/image/rank/" + Member.getRankInString(rankService.getCurrentRank(member.getTotalExp())) + ".png";

            List<String> entryMsg = new ArrayList<>();
            entryMsg.add(i18n.getText("enterStreamDefaultMsg", locale));
            entryMsg.add(i18n.getText("enterStreamDefaultMsg2", locale));
            if (StringUtils.isNotBlank(vjMissionMessage)) {
                entryMsg.add(vjMissionMessage);
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("channel", channel)
                    .setAttribute("levelUrl", levelUrl)
                    .setAttribute("fansBadgeUrl", fansBadgeInfoTriple.getLeft())
                    .setAttribute("fansBadgeName", fansBadgeInfoTriple.getMiddle())
                    .setAttribute("fansBadgeTextColor", fansBadgeInfoTriple.getRight())
                    .setAttribute("fanRoleUrl", fanRoleUrl)
                    .setAttribute("memberIsFans", isFans)
                    .setAttribute("memberIsAdmin", isAdmin)
                    .setAttribute("memberIsSeniorAdmin", isSeniorAdmin)
                    .setAttribute("memberIsBlocked", isBlocked)
                    .setAttribute("memberIsMuted", isMute)
                    .setAttribute("hasVjMission", hasVjMission)
                    .setAttribute("defaultEntryMessage", entryMsg)
                    .setAttribute("defaultEntryMessageCode", "#FEEFB0")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/chatroom/mute")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response setUserMute(Channel channel) {
        try {
            Locale locale = RestUtil.getUserLocale();
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String userMemberId = WebUtil.getLoginMemberId(loginInfo);
            String targetMemberId = channel.getMemberId(); //mute which user

            //process mute/unmute
            boolean mute;
            if (StringUtils.isBlank(channel.getAction())) {
                throw new ValidatorException("invalidAction", locale);
            } else if (channel.getAction().equalsIgnoreCase("mute")) {
                mute = true;
            } else if (channel.getAction().equalsIgnoreCase("unmute")) {
                mute = false;
            } else {
                throw new ValidatorException("invalidAction", locale);
            }

            liveChatService.doProcessMuteUser(locale, channel.getChannelId(), mute, userMemberId, targetMemberId);

            //return profile detail to mobile
            ProfileDto profileDto = memberService.getProfileDetails(locale, userMemberId, targetMemberId, channel.getChannelId());

            return ResponseBuilder.newInstance()
                    .setAttribute("profilePic", profileDto.getProfilePic())
                    .setAttribute("memberId", profileDto.getMemberId())
                    .setAttribute("profileName", profileDto.getProfileName())
                    .setAttribute("whaleliveId", profileDto.getWhaleliveId())
                    .setAttribute("profileBio", profileDto.getProfileBio())
                    .setAttribute("yunXinAccountId", profileDto.getYunXinAccountId())
                    .setAttribute("followerCount", profileDto.getFollowerCount())
                    .setAttribute("followingCount", profileDto.getFollowingCount())
                    .setAttribute("following", profileDto.isFollowing())
                    .setAttribute("blocking", profileDto.isBlocking())
                    .setAttribute("blockBy", profileDto.isBlockBy())
                    .setAttribute("channel", profileDto.getChannel())
                    .setAttribute("actionOptions", profileDto.getActionOptions())
                    .setAttribute("gender", profileDto.getGender())
                    .setAttribute("countryName", profileDto.getCountryName())
                    .setAttribute("levelUrl", profileDto.getLevelUrl())
                    .setAttribute("receivedPoint", profileDto.getReceivedPoint())
                    .setAttribute("sentPoint", profileDto.getSentPoint())
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/event/luckydraw")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response processLuckyDraw(LuckyDrawPackage luckyDrawPackage) {
        try {
            Locale locale = RestUtil.getUserLocale();

            String item = null;
            int maxQty = 0;
            String message = null;
            luckyDrawPackage = luckyDrawService.doProcessLuckyDraw(locale, luckyDrawPackage);
            if (luckyDrawPackage != null) {
                item = luckyDrawPackage.getItemName();
                maxQty = luckyDrawPackage.getRemainingQty();
                message = luckyDrawPackage.getMessage();
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("item", item)
                    .setAttribute("maxQty", maxQty)
                    .setAttribute("message", message)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/fansPrivileges")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getFansPrivileges(FollowerBO followerBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member fansMember = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            String fansMemberId = fansMember.getMemberId();
            String memberId = followerBO.getMemberId();
            Member member = memberService.getMember(memberId);
            String webViewUrl = cryptoProvider.getServerUrl() + "/pub/fansBadgeInfo.php";
            boolean isCn = Global.LOCALE_CN.equals(locale);

            String whaleCardId = pointService.findWhaleCard();
            SymbolicItem symbolicItem = pointService.findSymbolicItem(whaleCardId);
            symbolicItem.setFileUrlWithParentPath(config.getFullGiftParentFileUrl());

            SymbolicItemBO item = new SymbolicItemBO();
            Map<String, String> itemName = new HashMap<>();
            itemName.put(Global.Language.CHINESE, symbolicItem.getNameCn());
            itemName.put(Global.Language.ENGLISH, symbolicItem.getName());
            item.setSeq(symbolicItem.getSeq());
            item.setPrice(BDUtil.roundUp2dp(symbolicItem.getPrice()));
            item.setName(isCn ? symbolicItem.getNameCn() : symbolicItem.getName());
            item.setNameList(itemName);
            item.setImageUrl(symbolicItem.getFileUrl());
            item.setType(symbolicItem.getType());
            item.setVersion(symbolicItem.getGifVersion());
            item.setItemTitle(i18n.getText("whaleCardTitle", locale));
            item.setItemId(symbolicItem.getId());
            item.setItemDescription(i18n.getText("whaleCardDesc", locale));
            //todo: if "whalelive_gif_animation" statement can be removed once mobile updated
            if (symbolicItem.getFilename().contains("whalelive_gif_animation")) {
                String gifFileName = symbolicItem.getFilename().substring(0, symbolicItem.getFilename().length() - 4);
                item.setAnimationUrl(gifFileName);
                item.setAnimationDownloadUrl(symbolicItem.getGifFileUrl());
            } else if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
                item.setAnimationUrl(symbolicItem.getGifFilename());
                item.setAnimationDownloadUrl(symbolicItem.getGifFileUrl());
            }

            FansDetailsBO fansDetailsBO = new FansDetailsBO();
            FansGiftBO fansGiftBO = new FansGiftBO();
            fansGiftBO.setTitle(i18n.getText("sendGiftToJoin", locale));
            fansGiftBO.setPrice(symbolicItem.getPrice().intValue() + " " + i18n.getText("whaleCoin", locale));
            fansGiftBO.setIcon(symbolicItem.getFileUrl());
            fansGiftBO.setItem(item);

            if (locale.equals(Global.LOCALE_CN)) {
                webViewUrl += "?languageCode=zh";
                fansGiftBO.setDesc(symbolicItem.getNameCn());
            } else if (locale.equals(Global.LOCALE_MS)) {
                webViewUrl += "?languageCode=ms";
                fansGiftBO.setDesc(symbolicItem.getName());
            } else {
                webViewUrl += "?languageCode=en";
                fansGiftBO.setDesc(symbolicItem.getName());
            }

            MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
            if (memberFile != null) {
                memberFile.setFileUrlWithParentPath(memberFileUploadConfiguration.getFullMemberFileParentFileUrl());
                fansDetailsBO.setProfileUrl(memberFile.getFileUrl());
            }

            fansDetailsBO.setInfoWebView(webViewUrl);
            fansDetailsBO.setFansGiftBO(fansGiftBO);
            fansDetailsBO.setProfileName(member.getMemberDetail().getProfileName());
            fansDetailsBO.setTargetPointSpent(FansIntimacyTrx.SEND_GIFT_TASK.toString());
            fansDetailsBO.setTodayPointSpent(fansIntimacyService.findTodayPointSpend(fansMemberId, memberId).intValue() + "");

            boolean fansBadgeExist = StringUtils.isNotBlank(member.getFansBadgeName()) && member.getFansBadgeName() != null;
            if (fansBadgeExist) {
                int totalFans = memberFansService.findTotalFans(memberId);
                boolean isMemberFans = memberFansService.isMemberFans(fansMemberId, memberId, "");
                fansDetailsBO.setTotalFans(totalFans);
                fansDetailsBO.setTitle(i18n.getText("legionOfFans", locale));
                fansDetailsBO.setFansBadgeName(member.getFansBadgeName());
                if (isMemberFans) {
                    fansIntimacyService.doProcessFansIntimacyGain(fansMemberId, memberId);
                    fansDetailsBO.setFans(true);
                    fansDetailsBO.setDescription(i18n.getText("increaseIntimacy", locale));
                    fansDetailsBO.setFansPrivilegesBOList(getFansPrivileges(locale, memberId, fansMemberId));
                } else {
                    fansDetailsBO.setFans(false);
                    fansDetailsBO.setDescription(i18n.getText("legionOfFansBadge", locale));
                    fansDetailsBO.setFansPrivilegesBOList(getFansPrivileges(locale, memberId, fansMemberId));
                }
            }
            return ResponseBuilder.newInstance()
                    .setAttribute("fansPrivileges", fansBadgeExist ? fansDetailsBO : null)
                    .setAttribute("fansBadgeExist", fansBadgeExist)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private List<FansPrivilegesBO> getFansPrivileges(Locale locale, String memberId, String fansMemberId) {
        boolean isFans = memberFansService.isMemberFans(fansMemberId, memberId, "");
        List<FansPrivilegesBO> fansPrivilegesBOList = new ArrayList<>();
        if (isFans) {
            MemberFans memberFans = memberFansService.findMemberFans(fansMemberId, memberId, "");
            if (memberFans.getSendPointInd() != null && memberFans.getSendPointInd()) {
                fansIntimacyService.doProcessFansIntimacyGain(memberFans.getFansMemberId(), memberFans.getMemberId());
            }
            boolean firstTimeEntry = fansIntimacyService.isFirstTimeGain(fansMemberId, memberId, new Date(), FansIntimacyTrx.TYPE_FIRST_TIME_ENTRY);
            boolean conWatching = fansIntimacyService.isFirstTimeGain(fansMemberId, memberId, new Date(), FansIntimacyTrx.TYPE_CONT_FIVE_MIN_VIEW);
            boolean sendGift = fansIntimacyService.isSendGiftGain(fansMemberId, memberId);
            FansPrivilegesBO sendGiftFansPrivilegesBO = new FansPrivilegesBO();
            FansPrivilegesBO firstEntryFansPrivilegesBO = new FansPrivilegesBO();
            FansPrivilegesBO watchTimeFansPrivilegesBO = new FansPrivilegesBO();

            firstEntryFansPrivilegesBO.setTitle(i18n.getText("firstEntryDaily", locale));
            firstEntryFansPrivilegesBO.setDescription(i18n.getText("firstEntryDailyDesc", locale));
            firstEntryFansPrivilegesBO.setComplete(!firstTimeEntry);
            firstEntryFansPrivilegesBO.setTitleColor("#ffffff");
            firstEntryFansPrivilegesBO.setDescriptionColor("#ffffff");
            firstEntryFansPrivilegesBO.setIcon(cryptoProvider.getServerUrl() + "/asset/image/memberFans/firstEntry.png");

            watchTimeFansPrivilegesBO.setTitle(i18n.getText("watchStreamDaily", locale));
            watchTimeFansPrivilegesBO.setDescription(i18n.getText("watchStreamDailyDesc", locale));
            watchTimeFansPrivilegesBO.setComplete(!conWatching);
            watchTimeFansPrivilegesBO.setTitleColor("#ffffff");
            watchTimeFansPrivilegesBO.setDescriptionColor("#ffffff");
            watchTimeFansPrivilegesBO.setIcon(cryptoProvider.getServerUrl() + "/asset/image/memberFans/watchStream.png");

            sendGiftFansPrivilegesBO.setTitle(i18n.getText("sendGift", locale));
            sendGiftFansPrivilegesBO.setDescription(i18n.getText("sendGiftDesc", locale));
            sendGiftFansPrivilegesBO.setComplete(sendGift);
            sendGiftFansPrivilegesBO.setTitleColor("#ffffff");
            sendGiftFansPrivilegesBO.setDescriptionColor("#ffffff");
            sendGiftFansPrivilegesBO.setIcon(cryptoProvider.getServerUrl() + "/asset/image/memberFans/sendGift.png");

            fansPrivilegesBOList.add(firstEntryFansPrivilegesBO);
            fansPrivilegesBOList.add(watchTimeFansPrivilegesBO);
            fansPrivilegesBOList.add(sendGiftFansPrivilegesBO);

        } else {
            FansPrivilegesBO fansPrivilegesOne = new FansPrivilegesBO();
            FansPrivilegesBO fansPrivilegesTwo = new FansPrivilegesBO();
            FansPrivilegesBO fansPrivilegesThree = new FansPrivilegesBO();

            String iconFansLegion = cryptoProvider.getServerUrl() + "/asset/image/memberFans/legionFansBadge.png";
            fansPrivilegesOne.setTitle(i18n.getText("legionOfFansBadge", locale));
            fansPrivilegesOne.setDescription(i18n.getText("legionOfFansBadgeDesc", locale));
            fansPrivilegesOne.setTitleColor("#ffffff");
            fansPrivilegesOne.setDescriptionColor("#ffffff");
            fansPrivilegesOne.setBackgroundColor("#6A18AE");
            fansPrivilegesOne.setIcon(iconFansLegion);

            String iconLeaderBoard = cryptoProvider.getServerUrl() + "/asset/image/memberFans/leaderBoard.png";
            fansPrivilegesTwo.setTitle(i18n.getText("fansIntimacyLeaderBoard", locale));
            fansPrivilegesTwo.setDescription(i18n.getText("fansIntimacyLeaderBoardDesc", locale));
            fansPrivilegesTwo.setTitleColor("#ffffff");
            fansPrivilegesTwo.setDescriptionColor("#ffffff");
            fansPrivilegesTwo.setBackgroundColor("#9224B9");
            fansPrivilegesTwo.setIcon(iconLeaderBoard);

            String iconVip = cryptoProvider.getServerUrl() + "/asset/image/memberFans/vip.png";
            fansPrivilegesThree.setTitle(i18n.getText("fansVipSeat", locale));
            fansPrivilegesThree.setDescription(i18n.getText("fansVipSeatDesc", locale));
            fansPrivilegesThree.setTitleColor("#ffffff");
            fansPrivilegesThree.setDescriptionColor("#ffffff");
            fansPrivilegesThree.setBackgroundColor("#1EC064");
            fansPrivilegesThree.setIcon(iconVip);

            fansPrivilegesBOList.add(fansPrivilegesOne);
            fansPrivilegesBOList.add(fansPrivilegesTwo);
            fansPrivilegesBOList.add(fansPrivilegesThree);
        }
        return fansPrivilegesBOList;
    }

    private static final String VJ_CONNECT_LIST = "vjConnectList, "
            + "vjConnectList\\[\\d+\\], "
            + "vjConnectList\\[\\d+\\].memberId, "
            + "vjConnectList\\[\\d+\\].profileName, "
            + "vjConnectList\\[\\d+\\].profilePicUrl, "
            + "vjConnectList\\[\\d+\\].yunXinAccountId";

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/connectList")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getConnectList(LiveStreamBO liveStreamBO) {
        try {
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String memberId = WebUtil.getLoginMemberId(loginInfo);
            final String parentUrl = cryptoProvider.getFileUploadUrl() + "/file/memberFile/";
            DatagridModel<LiveStreamChannel> datagridModel = new DefaultDatagridModel<>();
            datagridModel.setCurrentPage(liveStreamBO.getPageNo());
            datagridModel.setPageSize(liveStreamBO.getPageSize());

            liveStreamService.findLiveStreamConnectListing(datagridModel, memberId, liveStreamBO.getName());
            List<LiveConnectBO> liveConnectBOList = datagridModel.getRecords().stream()
                    .map(c -> {
                        LiveConnectBO liveConnectBO = new LiveConnectBO();
                        liveConnectBO.setMemberId(c.getMemberId());
                        liveConnectBO.setProfileName(c.getWhaleLiveId());
                        MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(c.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                        // get profile picture
                        if (memberFile != null) {
                            memberFile.setFileUrlWithParentPath(parentUrl);
                            liveConnectBO.setProfilePicUrl(memberFile.getFileUrl());
                        }
                        NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(c.getMemberId(), Global.AccessModule.WHALE_MEDIA);
                        if (account != null) {
                            liveConnectBO.setYunXinAccountId(account.getAccountId());
                        }
                        return liveConnectBO;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(VJ_CONNECT_LIST)
                    .setAttribute("vjConnectList", liveConnectBOList)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/vjConnect/reject")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response processVjConnectReject(LiveConnectBO liveConnectBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String userMemberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            liveStreamService.doProcessVjConnectReject(userMemberId, liveConnectBO.getMemberId(), liveConnectBO.getRejectDuration());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("processSuccessfully", locale));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/vjConnect/accept")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response processVjConnectAccept(LiveConnectBO liveConnectBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String userMemberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            liveStreamService.doProcessVjConnectAccept(locale, liveConnectBO.getMemberId(), userMemberId);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("processSuccessfully", locale));
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @PermitAll
    @Path("/agoraCallBack")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response processAgoraCallBack(@HeaderParam("Agora-Signature") String signature, String body) {
        try {
            String errorMsg = liveStreamService.doProcessAgoraCallBack(signature, body);
            if (StringUtils.isNotBlank(errorMsg)) { //no need return error to Agora
                throw new Exception(errorMsg);
            }

            return ResponseBuilder.build(Response.Status.OK, "Message received");
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}