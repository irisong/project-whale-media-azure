package rest.restapi;

import javax.annotation.security.PermitAll;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.glassfish.jersey.server.ResourceConfig;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.service.MobileConfigService;
import com.compalsolutions.compal.general.vo.MobileForceUpdate;

import rest.ResponseBuilder;

@Path("app")
public class AppRest extends ResourceConfig {
    private MobileConfigService mobileConfigService;

    public AppRest() {
        mobileConfigService = Application.lookupBean(MobileConfigService.class);
    }

    private static final String MOBILE_FORCE_UPDATE_INCLUDE_PROPERTIES = "platform, " //
            + "favor, " //
            + "versionName, " //
            + "versionCode, " //
            + "forceUpdate, " //
            + "updateUrl, " //
            + "datetimeUpdate"; //

    @Tag(name = "Basic")
    @GET
    @PermitAll
    @Path("/mobile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response requestMobile(@QueryParam("platform") String platform, @QueryParam("favor") String favor) {
        try {
            MobileForceUpdate mobileForceUpdate = mobileConfigService.findMobileForceUpdateByPlatformAndFavor(platform, favor);

            if (mobileForceUpdate == null) {
                throw new ValidatorException("Invalid Platform and Favor");
            }

            return ResponseBuilder.newInstance() //
                    .setRoot(mobileForceUpdate) //
                    .setIncludeProperties(MOBILE_FORCE_UPDATE_INCLUDE_PROPERTIES) //
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @Tag(name = "Basic")
    @POST
    @PermitAll
    @Path("/__mobile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response putMobile(MobileForceUpdate mobileForceUpdate) {
        try {
            MobileForceUpdate updatedMobileForceUpdate = mobileConfigService.doUpdateMobileForceUpdate(mobileForceUpdate);

            return ResponseBuilder.newInstance() //
                    .setRoot(updatedMobileForceUpdate) //
                    .setIncludeProperties(MOBILE_FORCE_UPDATE_INCLUDE_PROPERTIES) //
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
