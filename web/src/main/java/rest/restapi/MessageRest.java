package rest.restapi;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.core.env.Environment;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.MessageBO;
import rest.vo.MessageDetail;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Path("message")
public class MessageRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(MessageRest.class);
    private MessageService messageService;
    private MemberService memberService;
    private I18n i18n;
    private String serverUrl;
    private String fileServerUrl;

    public MessageRest() {
        messageService = Application.lookupBean(MessageService.class);
        memberService = Application.lookupBean(MemberService.class);
        i18n = Application.lookupBean(I18n.class);

        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("serverUrl");
        fileServerUrl= env.getProperty("fileServerUrl");
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getMessages(MessageBO messageBO) {
        try {
            final String webViewUrl = "/pub/webView.php?";
            VoUtil.processProperties(messageBO);
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String memberId = WebUtil.getLoginMemberId(loginInfo);

            DatagridModel<Message> dataPaging = initPagination(messageBO);
            messageService.findAllMessageByReceiverId(dataPaging, memberId);

            List<MessageBO> messageBOList = new ArrayList<>();
            if (dataPaging.getRecords() != null) {
                messageBOList = dataPaging.getRecords().stream().map(
                        m -> {
                            MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(m.getDirectToMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                            if (memberFile != null) {
                                memberFile.setFileUrlWithParentPath(fileServerUrl + "/file/memberFile");
                            }

                            String imageUrl = (memberFile != null ? memberFile.getFileUrl() : null);

                            MessageBO messageBO1 = new MessageBO(m.getMessageId(), m.getMemberId(), m.getSentDate(), m.getMessageType(),
                                    m.getDirectToMemberId(), imageUrl);

                            MessageDetail messageDetail = new MessageDetail("en", m.getTitle(), m.getContent(),
                                    serverUrl + webViewUrl + "languageCode=en&messageId=" + m.getMessageId());
                            messageBO1.addMessageDetails(messageDetail);

                            if (StringUtils.isNotBlank(m.getTitleCn())) {
                                messageDetail = new MessageDetail("zh", m.getTitleCn(), m.getContentCn(),
                                        serverUrl + webViewUrl + "languageCode=zh&messageId=" + m.getMessageId());
                                messageBO1.addMessageDetails(messageDetail);
                            }
                            return messageBO1;
                        })
                        .collect(Collectors.toList());
            }

            return ResponseBuilder.newInstance()
                    .setRoot(messageBOList)
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private DatagridModel<Message> initPagination(MessageBO messageBO) {
        if (messageBO.getPage() <= 0) {
            throw new ValidatorException("Invalid Page number");
        }
        if (messageBO.getPageSize() <= 0) {
            messageBO.setPageSize(10);
        }

        DatagridModel<Message> messages = new DefaultDatagridModel<>();
        messages.setCurrentPage(messageBO.getPage());
        messages.setPageSize(messageBO.getPageSize());
        return messages;
    }

    @GET
    @Path("/deleteMessage")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response deleteMessage(@QueryParam("messageId") String messageId) {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            messageService.doDeleteMessage(member.getMemberId(), messageId);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("deleteMessageSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

}
