package rest.restapi;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.util.Locale;

@Path("bankInfo")
public class BankInfoRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(BankInfoRest.class);

    private MemberService memberService;

    public BankInfoRest() {
        memberService = Application.lookupBean(MemberService.class);
    }

    @POST
    @RolesAllowed({ AP.ROLE_MEMBER })
    @Path("/uploadFile")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response uploadFile(@FormDataParam("uploadType") String uploadType,
                               @FormDataParam("fileUpload") InputStream fileInputStream,
                               @FormDataParam("fileUpload") FormDataContentDisposition fileMetaData) {
        try {
            Locale locale = RestUtil.getUserLocale();
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            String memberId = WebUtil.getLoginMemberId(loginInfo);

            uploadType = StringUtils.upperCase(uploadType);

            memberService.doProcessUploadMemberFile(locale, memberId, uploadType, fileInputStream, fileMetaData.getFileName(),
                    fileMetaData.getSize(), fileMetaData.getType());

            return ResponseBuilder.newInstance().addMessage("success").createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
