package rest.restapi.guild;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberFansService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.telegram.BotClient;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.GuildBO;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

@Path("guild")
public class GuildRest extends ResourceConfig {

    private BroadcastService broadcastService;
    private MemberService memberService;
    private MemberFansService memberFansService;
    private I18n i18n;
    private CryptoProvider cryptoProvider;
    private GuildFileUploadConfiguration configuration;

    public GuildRest() {
        broadcastService = Application.lookupBean(BroadcastService.class);
        memberService = Application.lookupBean(MemberService.class);
        memberFansService = Application.lookupBean(MemberFansService.class);
        i18n = Application.lookupBean(I18n.class);
        cryptoProvider = Application.lookupBean(CryptoProvider.class);
        configuration = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);
    }

    private static final String LIST_GUILD_LIST_INCLUDE = "data, "
            + "data\\[\\d+\\], "
            + "data\\[\\d+\\].guildId, "
            + "data\\[\\d+\\].displayId, "
            + "data\\[\\d+\\].name, "
            + "data\\[\\d+\\].description, "
            + "data\\[\\d+\\].president, "
            + "data\\[\\d+\\].memberCount,"
            + "data\\[\\d+\\].join,"
            + "data\\[\\d+\\].fileUrl, "
            + "guildInfo,"
            + "guildInfo.guildId, "
            + "guildInfo.displayId, "
            + "guildInfo.name, "
            + "guildInfo.description, "
            + "guildInfo.memberCount,"
            + "guildInfo.president,"
            + "guildInfo.join, "
            + "guildInfo.fileUrl, "
            + "guildInfo.guildMembers, "
            + "guildInfo.guildMembers\\[\\d+\\], "
            + "guildInfo.guildMembers\\[\\d+\\].fans,"
            + "guildInfo.guildMembers\\[\\d+\\].profileName,"
            + "guildInfo.guildMembers\\[\\d+\\].profileUrl,"
            + "guildInfo.guildMembers\\[\\d+\\].memberId ";

    private static final String LIST_GUILD_INCLUDE = "guildInfo,"
            + "guildInfo.guildId, "
            + "guildInfo.displayId, "
            + "guildInfo.name, "
            + "guildInfo.description, "
            + "guildInfo.president, "
            + "guildInfo.memberCount,"
            + "guildInfo.join, "
            + "guildInfo.fileUrl, "
            + "guildInfo.guildMembers, "
            + "guildInfo.guildMembers\\[\\d+\\], "
            + "guildInfo.guildMembers\\[\\d+\\].fans,"
            + "guildInfo.guildMembers\\[\\d+\\].profileName,"
            + "guildInfo.guildMembers\\[\\d+\\].profileUrl,"
            + "guildInfo.guildMembers\\[\\d+\\].memberId ";


    @POST
    @Path("/guildList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response list(GuildBO guildBO) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        Member member = memberService.getMember(memberId);

        try {
            List<GuildBO> guildBoList = new ArrayList<>();
            List<BroadcastGuild> broadcastGuildList;
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            DatagridModel<BroadcastGuild> dataPaging = initPagination(guildBO);
            broadcastService.findAllGuilds(dataPaging);
            broadcastGuildList = dataPaging.getRecords();

            broadcastGuildList.forEach(bc -> {
                GuildBO guildBo = getMemberApprovedInGuildBOs(null, locale,
                        bc.getId(), memberId);
                guildBoList.add(guildBo);
            });

            // if user already get approved by any guild, they cant request new guild
            GuildBO guild = guildBoList.stream()
                    .filter(g -> (g.getJoin().equalsIgnoreCase(BroadcastGuildMember.STATUS_APPROVED)))
                    .findAny()
                    .orElse(null);

            GuildBO guildBoResult = null;
            // return user APPROVED guild detail
            if (guild != null) {
                DatagridModel<BroadcastGuildMember> guildMemberDatagridModel = initPaginationMember(guild);
                if (StringUtils.isBlank(guild.getGuildId())) {
                    throw new ValidatorException("invalid guild Id");
                }
                //only one guild info per one request
                guildBoResult = getMemberApprovedInGuildBOs(guildMemberDatagridModel, locale, guild.getGuildId(), memberId);

            }
            if (broadcastGuildList != null) {
                broadcastGuildList.clear();
                broadcastGuildList = null;
            }
            return ResponseBuilder.newInstance()
                    .setAttribute("data", guildBoList)
                    .setAttribute("guildInfo", guildBoResult)
                    .setIncludeProperties(LIST_GUILD_LIST_INCLUDE)
                    .createResponse();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/guildInfo")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getGuildInfo(GuildBO guildBO) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        try {
            DatagridModel<BroadcastGuildMember> dataPaging = initPaginationMember(guildBO);
            if (StringUtils.isBlank(guildBO.getGuildId())) {
                throw new ValidatorException("invalid guild Id");
            }

            //only one guild info per one request
            GuildBO guildBoResult = getMemberApprovedInGuildBOs(dataPaging, locale, guildBO.getGuildId(), memberId);
            return ResponseBuilder.newInstance()
                    .setAttribute("guildInfo", guildBoResult)
                    .setIncludeProperties(LIST_GUILD_INCLUDE)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    /**
     * Share function for guildInfo and guildListing
     **/
    private GuildBO getMemberApprovedInGuildBOs(DatagridModel<BroadcastGuildMember> broadcastGuildMemberDatagridModel, Locale locale,
                                                String guildId, String memberId) {

        final String parentUrl = cryptoProvider.getFileUploadUrl() + "/file/memberFile";
        BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(guildId);

        boolean isGuildInfoUI = broadcastGuildMemberDatagridModel != null;
        if (broadcastGuild == null) {
            throw new ValidatorException(i18n.getText("invalidGuild", locale));
        }

        // Get all members in guild with status APPROVED
        if (isGuildInfoUI)
            broadcastService.findAllBroadcastGuildMemberDatagrid(broadcastGuildMemberDatagridModel, guildId, BroadcastGuildMember.STATUS_APPROVED);

        // check if login user has records under this guild
        BroadcastGuildMember selfMember = broadcastService.getBroadcastGuildMember(broadcastGuild, memberId, null);

        GuildBO guildBoResult = new GuildBO();
        List<BroadcastGuildMember> broadcastGuildMember = isGuildInfoUI ? broadcastGuildMemberDatagridModel.getRecords() : broadcastService.getBroadcastGuildMembersApproved(broadcastGuild);
        guildBoResult.setGuildId(broadcastGuild.getId());
        guildBoResult.setDisplayId(broadcastGuild.getDisplayId());
        guildBoResult.setName(broadcastGuild.getName());
        guildBoResult.setDescription(broadcastGuild.getDescription());
        guildBoResult.setPresident(broadcastGuild.getPresident());
        broadcastGuild.setFileUrlWithParentPath(configuration.getFullGuildParentFileUrl());

        guildBoResult.setFileUrl(broadcastGuild.getFileUrl());

        // if guildInfo UI with pagination purpose, cannot use size, must use totalRecords as pagination data not showing all
        guildBoResult.setMemberCount(isGuildInfoUI ? broadcastGuildMemberDatagridModel.getTotalRecords() : broadcastGuildMember.size());
        guildBoResult.setJoin(selfMember != null ? selfMember.getStatus() : BroadcastGuildMember.STATUS_NEW);

        // Retrieve all related member's info
        if (isGuildInfoUI) {
            broadcastGuildMember.forEach(guildMember -> {
                String guildMemberId = guildMember.getMemberId();
                Member memberGuild = memberService.getMember(guildMemberId);
                if (memberGuild == null) {
                    throw new ValidatorException(i18n.getText("invalidMember", locale));
                }
                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(guildMemberId, MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl);
                    guildMember.setProfileUrl(memberFile.getFileUrl());
                }
                guildMember.setFans(memberFansService.findTotalFans(memberGuild.getMemberId()));
                guildMember.setProfileName(memberGuild.getMemberDetail().getProfileName());
            });
            guildBoResult.setGuildMembers(broadcastGuildMember);
        }
        return guildBoResult;
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/signingGuild")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response signGuild(GuildBO guildBO) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        Member member = memberService.getMember(memberId);
        try {
            VoUtil.processProperties(guildBO);

            if (StringUtils.isBlank(guildBO.getGuildId())) {
                throw new ValidatorException("invalid guild");
            }

            BroadcastGuildMember approvedMember = broadcastService.findGuildMemberApproved(memberId);
            BroadcastGuildMember pendingApprovalMember = broadcastService.findMemberInPending(memberId, guildBO.getGuildId());
            BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(guildBO.getGuildId());

            if (guildBO.getAction().equals("sign")) {
                if (approvedMember != null) {
                    throw new ValidatorException(i18n.getText("memberAlreadyJoinOtherGuild", locale));
                }
                if (pendingApprovalMember != null) {
                    throw new ValidatorException(i18n.getText("memberPendingApproval", locale));
                }
                broadcastService.doAddDeleteGuildMemberFromAPI(memberId, guildBO.getGuildId(), true);

                //send notification email
                if (broadcastGuild != null) {
                    String title = ("Whale Live Notification: Signing Guild");
                    String body = ("[WHALE LIVE]" + member.getMemberCode() + ": " + member.getMemberDetail().getProfileName() + " has requested to sign guild [" + broadcastGuild.getName() + "] and pending for your approval.");
                    memberService.doSendDifferentNotificationEmailType(Global.EmailType.SIGNING_GUILD, broadcastGuild.getId(), null, null, title, body);

                }
                return ResponseBuilder.build(Response.Status.OK, i18n.getText("signingGuildSuccessfully", locale));

            } else {
                String msg = broadcastService.doAddDeleteGuildMemberFromAPI(memberId, guildBO.getGuildId(), false);
                return ResponseBuilder.build(Response.Status.OK, i18n.getText(msg, locale));
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }


    }

    private boolean compareList(String guildId, List<BroadcastGuildMember> broadcastGuildMemberList) {
        boolean flag = false;
        for (int i = 0; i < broadcastGuildMemberList.size(); i++) {
            BroadcastGuildMember broadcastGuildMember = broadcastGuildMemberList.get(i);
            if (broadcastGuildMember.getGuildId().equalsIgnoreCase(guildId)) {
                flag = true;
                break;
            }
        }
        return flag;
    }

    private DatagridModel<BroadcastGuild> initPagination(GuildBO guildBO) {

        if (guildBO.getPage() <= 0) {
            guildBO.setPage(1);
        }
        if (guildBO.getPageSize() <= 0) {
            guildBO.setPageSize(10);
        }

        DatagridModel<BroadcastGuild> memberDetailDataGrid = new DefaultDatagridModel<>();
        memberDetailDataGrid.setCurrentPage(guildBO.getPage());
        memberDetailDataGrid.setPageSize(guildBO.getPageSize());

        return memberDetailDataGrid;
    }

    private DatagridModel<BroadcastGuildMember> initPaginationMember(GuildBO guildBO) {

        if (guildBO.getPage() <= 0) {
            guildBO.setPage(1);
        }
        if (guildBO.getPageSize() <= 0) {
            guildBO.setPageSize(10);
        }

        DatagridModel<BroadcastGuildMember> memberDetailDataGrid = new DefaultDatagridModel<>();
        memberDetailDataGrid.setCurrentPage(guildBO.getPage());
        memberDetailDataGrid.setPageSize(guildBO.getPageSize());

        return memberDetailDataGrid;
    }

}
