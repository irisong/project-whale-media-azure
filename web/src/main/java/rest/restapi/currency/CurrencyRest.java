package rest.restapi.currency;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;

import rest.CryptoUtil;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.CurrencyExchangeBO;

@Path("currency")
public class CurrencyRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(CurrencyRest.class);

    private CurrencyService currencyService;
    private MemberService memberService;
    private I18n i18n;

    public CurrencyRest() {
        i18n = Application.lookupBean(I18n.class);
        currencyService = Application.lookupBean(CurrencyService.class);
        memberService = Application.lookupBean(MemberService.class);
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/exchangeRate/{currencyCodeFrom}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCurrencyExchange(@PathParam("currencyCodeFrom") String currencyCodeFrom) {
        try {
            List<CurrencyExchangeBO> currencyExchangeBOList = getCurrencyExchanges(currencyService.findLatestCurrencyExchanges()).stream()
                    .filter(curr -> curr.getCurrencyCodeFrom().equalsIgnoreCase(currencyCodeFrom))
                    .collect(Collectors.toList());
            return ResponseBuilder.newInstance()
                    .setAttribute("currencyExchangeList", currencyExchangeBOList)
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/exchangeRate")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getCurrencyExchange() {
        try {
            List<CurrencyExchangeBO> CurrencyExchangeList = getCurrencyExchanges(currencyService.findLatestCurrencyExchanges());
            return ResponseBuilder.newInstance()
                    .setAttribute("currencyExchangeList", CurrencyExchangeList)
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @POST
    @Path("/update")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response changeDefaultCurrency(@QueryParam("defaultCurrency") String defaultCurrency) {
        Locale locale = RestUtil.getUserLocale();
        try {
            updateDefaultCurrency(defaultCurrency, locale);
            return ResponseBuilder.build(Response.Status.OK, i18n.getText("successMessage.ExchangeRateAction.save", locale));
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/exchangeRateActive")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getActiveCurrencyExchange() {
        try {
            List<CurrencyExchangeBO> CurrencyExchangeList = getCurrencyExchanges(currencyService.findLatestActiveCurrencyExchanges());
            return ResponseBuilder.newInstance()
                    .setAttribute("currencyExchangeList", CurrencyExchangeList)
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    private List<CurrencyExchangeBO> getCurrencyExchanges(List<CurrencyExchange> currencyExchangeList) {
        Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
        Member latestMember = memberService.getMember(member.getMemberId());
        Pair<BigDecimal, BigDecimal> minAndMaxAmount = Global.getMiminumAndMaximumPaymentInLegalCurrency(Global.FiatSysmbol.USD);
        List<CurrencyExchangeBO> CurrencyExchangeBOList = currencyExchangeList.stream()
                .map(exchangeRate -> {
                    CurrencyExchangeBO currencyExchangeBO = new CurrencyExchangeBO();
                    currencyExchangeBO.setCurrencyCodeFrom(exchangeRate.getCurrencyCodeFrom());
                    BigDecimal bdCurrencyRate = BigDecimal.valueOf(exchangeRate.getRate());
                    currencyExchangeBO.setMinimumAmount(BDUtil.roundUp2dp(bdCurrencyRate.multiply(minAndMaxAmount.getLeft())));
                    currencyExchangeBO.setMaximumAmount(BDUtil.roundUp2dp(bdCurrencyRate.multiply(minAndMaxAmount.getRight())));
                    currencyExchangeBO.setCurrencyCodeTo(exchangeRate.getCurrencyCodeTo());
                    currencyExchangeBO.setExchangeDatetime(exchangeRate.getExchangeDatetime());
                    currencyExchangeBO.setRate(exchangeRate.getRate());
                    currencyExchangeBO.setSite(exchangeRate.getSite());
                    return currencyExchangeBO;
                }).collect(Collectors.toList());
        if (!StringUtils.isBlank(latestMember.getMemberDetail().getCurrencyCode())) {
            CurrencyExchangeBOList.forEach(c -> {
                if (c.getCurrencyCodeFrom().equalsIgnoreCase(latestMember.getMemberDetail().getCurrencyCode())) {
                    c.setSelected(true);
                    return;
                }
            });
        } else {
//            if db currencyCode is null then assign currencyCode based on phoneno
            memberService.doUpdateMemberProfile(latestMember.getMemberId(), null);
            CurrencyExchangeBOList.forEach(c -> {
                if (c.getCurrencyCodeFrom().equalsIgnoreCase(latestMember.getMemberDetail().getCurrencyCode())) {
                    c.setSelected(true);
                    return;
                }
            });
        }
        Collections.sort(CurrencyExchangeBOList, (abc1, abc2) ->
                Boolean.compare(abc2.isSelected(), abc1.isSelected()));
        return CurrencyExchangeBOList;
    }

    private void updateDefaultCurrency(String defaultCurrency, Locale locale) {
        Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
        if (StringUtils.isBlank(defaultCurrency)) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        } else {
            memberService.doUpdateMemberProfile(member.getMemberId(), defaultCurrency);
        }
    }
}
