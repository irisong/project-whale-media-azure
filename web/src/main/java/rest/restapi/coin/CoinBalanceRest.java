package rest.restapi.coin;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.omnicplus.dto.OmcPaymentDto;
import com.compalsolutions.compal.paymentGateway.AndroidPaymentClient;
import com.compalsolutions.compal.paymentGateway.IosPaymentClient;
import com.compalsolutions.compal.point.dto.PaymentDto;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.wallet.dto.TransferReceiveTrx;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.core.env.Environment;
import rest.CryptoUtil;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.SimpleOptionBean;
import rest.vo.coin.CoinBalanceBO;
import rest.vo.coin.CoinPackageBO;
import rest.vo.coin.CoinProfitBO;
import rest.vo.coin.CoinWebTopup;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Path("coin")
public class CoinBalanceRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(CoinBalanceRest.class);

    private WalletService walletService;
    private CurrencyService currencyService;
    private PointService pointService;
    private CryptoProvider cryptoProvider;
    private MemberService memberService;
    private boolean isProdServer;
    private I18n i18n;

    public CoinBalanceRest() {
        walletService = Application.lookupBean(WalletService.class);
        cryptoProvider = Application.lookupBean(CryptoProvider.class);
        i18n = Application.lookupBean(I18n.class);
        currencyService = Application.lookupBean(CurrencyService.class);
        memberService = Application.lookupBean(MemberService.class);
        pointService = Application.lookupBean(PointService.class);
        Environment env = Application.lookupBean(Environment.class);
        isProdServer = env.getProperty("server.production", Boolean.class, true);
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("payment/request/{packageAmt}/{platform}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTopUpCoinPaymentRequest(@PathParam("packageAmt") int packageAmt,
                                               @PathParam("platform") String platform) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        boolean isCn = locale.equals(Global.LOCALE_CN);

        try {
            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            boolean isIos = false;
            if (StringUtils.isNotBlank(platform)) {
                if (platform.equalsIgnoreCase("IOS")) {
                    isIos = true;
                }
            }

            WalletTopupPackage walletTopupPackage = walletService.getWalletTopupPackage(Global.WalletType.WALLET_80, packageAmt, Global.WalletPackageStatus.DISPLAY);

            if (walletTopupPackage == null)
                throw new ValidatorException("Invalid Package");

            // BigDecimal myr2UsdRate = BDUtil.roundUp2dp(new BigDecimal(currencyService.findLatestCurrencyExchange(Global.WalletType.MYR, Global.WalletType.USD).getRate()));
//            BigDecimal rate = currencyService.getWhaleCoinExchangeRate(Global.WalletType.MYR);
            // mobile topup use fixed rate
            BigDecimal priceInMyr = new BigDecimal(String.valueOf(walletTopupPackage.getFiatCurrencyAmount()));
            BigDecimal priceInUsd = new BigDecimal(String.valueOf(walletTopupPackage.getUsdPrice()));

//            List<PaymentDto> paymentDtos = pointService.doGetOMCWalletPaymentMethod(priceInUsd, isCn, member.getMemberCode());
//            if (paymentDtos == null) {
//                paymentDtos = new ArrayList<>();
//            }
            String iconAppleUrl = cryptoProvider.getServerUrl() + "/asset/image/coin/wc.png";
            String iconGoogleUrl = cryptoProvider.getServerUrl() + "/asset/whale_media/asset/payment/googlePay.png";
            List<SimpleOptionBean> coinTypes;
//            if (paymentDtos.size() > 0) {
//                coinTypes = Arrays.asList(
//                        new SimpleOptionBean(Global.WalletType.MYR, Global.WalletType.MYR),
//                        new SimpleOptionBean("CRYPTO", i18n.getText("crypto", locale))
//                );
//            } else {
                coinTypes = Arrays.asList(
                        new SimpleOptionBean(Global.WalletType.MYR, Global.WalletType.MYR));
//            }

//            List<SimpleOptionBean> mobileTopups = Arrays.asList(
//                    new SimpleOptionBean(PointTrx.GOOGLE_PAY, i18n.getText("googlePay", locale)));
//            List<SimpleOptionBean> mobileTopups;
//            if (isIos) {
//                mobileTopups = Arrays.asList(new SimpleOptionBean(PointTrx.APPLE_PAY, i18n.getText("applePay", locale)));
//            } else if (member.getMemberCode().equalsIgnoreCase("60149456773") || member.getMemberCode().equalsIgnoreCase("60163912519")){
//                mobileTopups = Arrays.asList(new SimpleOptionBean(PointTrx.GOOGLE_PAY, i18n.getText("googlePay", locale)));
//            } else {
//                mobileTopups = new ArrayList<>();
//            }

            List<SimpleOptionBean> mobileTopups = Arrays.asList(
                    new SimpleOptionBean(PointTrx.GOOGLE_PAY, i18n.getText("googlePay", locale)));
            if (isIos) {
                mobileTopups = Arrays.asList(new SimpleOptionBean(PointTrx.APPLE_PAY, i18n.getText("applePay", locale)));
            }

            for (SimpleOptionBean coin : coinTypes) {
//                if (coin.getKey().equalsIgnoreCase("CRYPTO")) {
//                    Map<String, Object> paymentDtoMap = new HashMap<>();
//                    paymentDtoMap.put("method", paymentDtos);
//                    coin.setData(paymentDtoMap);
//                } else {
                    Map<String, Object> paymentDtoMap = new HashMap<>();
                    List<PaymentDto> paymentOptions = new ArrayList<>();
                    for (SimpleOptionBean mobile : mobileTopups) {
                        PaymentDto paymentDto = new PaymentDto();
                        paymentDto.setPaymentMethod(mobile.getKey());
                        paymentDto.setPaymentMethodDesc(mobile.getValue());
                        paymentDto.setProductId(walletTopupPackage.getProductId());
                        paymentDto.setCurrencyCode(coin.getKey());
                        paymentDto.setPrice(priceInMyr);
                        if (mobile.getKey().equalsIgnoreCase(PointTrx.APPLE_PAY)) {
                            paymentDto.setUrl(iconAppleUrl);
                        } else {
                            paymentDto.setUrl(iconGoogleUrl);
                        }
                        paymentOptions.add(paymentDto);
                    }
                    paymentDtoMap.put("method", paymentOptions);
                    coin.setData(paymentDtoMap);
//                }
            }

//            if (paymentDtos.size() <= 0) {
//                throw new ValidatorException(i18n.getText("temporarySupportForOmcWalletUserOnly", locale));
//            }

            return ResponseBuilder.newInstance()
                    .setAttribute("payments", coinTypes)
                    .setAttribute("price", priceInMyr)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/payment/pay")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response payTopUpCoin(OmcPaymentDto omcPaymentDto) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        try {
            pointService.doCreateTopUpPointTrxViaOmcWallet(locale, omcPaymentDto.getCurrencyCode(),
                    omcPaymentDto.getAmount().intValue(), memberId, omcPaymentDto.getTransactionPassword());
            return ResponseBuilder.newInstance()
                    .addMessage(i18n.getText("topupSuccessfully", locale))
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("balance")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getWalletBalance() {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        String ownerType = Global.UserType.MEMBER;
        Member member = memberService.getMember(memberId);
        try {
            CoinBalanceBO bo = getWalletBalanceBO(locale, memberId, ownerType);
            CoinWebTopup coinWebTopup = new CoinWebTopup();
            coinWebTopup.setTitle(i18n.getText("topupWhaleCoinFromWeb", locale));
            if (isProdServer) {
                coinWebTopup.setUrl("https://www.whalemediamy.com/pub/whale_media/wl_recharge.php?whaleLiveId=" + member.getMemberDetail().getWhaleliveId() + "");
            } else {
                coinWebTopup.setUrl("https://staging.whalemediamy.com/pub/whale_media/wl_recharge.php?whaleLiveId=" + member.getMemberDetail().getWhaleliveId() + "");
            }
            bo.setCoinWebTopup(coinWebTopup);

            List<WalletTopupPackage> topupPackage = walletService.getWalletTopupPackages(Global.WalletPackageStatus.DISPLAY);
//            BigDecimal myr2UsdRate = BDUtil.roundUp2dp(new BigDecimal(currencyService.findLatestCurrencyExchange(Global.WalletType.MYR, Global.WalletType.USD).getRate()));
            // current rate use MYR as FIAT rate
            BigDecimal rate = currencyService.getWhaleCoinExchangeRate(Global.WalletType.MYR);

            List<CoinPackageBO> packages = topupPackage.stream()
                    .map(c -> {
//                        BigDecimal priceInMyr = BDUtil.roundUp2dp(BigDecimal.valueOf(c.getFiatCurrencyAmount()).multiply(rate)); // 1 whale coin = 0.015
                        BigDecimal priceInMyr = new BigDecimal(String.valueOf(c.getFiatCurrencyAmount()));
                        CoinPackageBO packageBO = new CoinPackageBO();
                        packageBO.setCurrencyCode(Global.WalletType.MYR);
                        packageBO.setFiatCurrency(c.getFiatCurrency());
                        packageBO.setPackageAmt(c.getPackageAmount());
                        packageBO.setExchangeRate(rate);
                        packageBO.setPrice(priceInMyr);
                        packageBO.setProductId(c.getProductId());
                        return packageBO;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            bo.setPackages(packages);
            return ResponseBuilder.newInstance().setRoot(bo).createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/history")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTransferReceive(@QueryParam("startIndex") int startIndex,
                                       @QueryParam("pageSize") Integer pageSize) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        String ownerType = Global.UserType.MEMBER;
        try {
            WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(ownerType, Global.WalletType.WC);

            final String currencyCode = walletTypeConfig.getCryptoType();

//            cryptoProvider.doCheckBlockchainBalance(memberId, currencyCode);

            pageSize = RestUtil.getDefaultPageSizeIfNull(pageSize);

            if (walletTypeConfig == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            Pair<Long, List<TransferReceiveTrx>> pair;

            pair = populateCoinHistoryTrx(memberId, ownerType, currencyCode, walletTypeConfig.getWalletType(), startIndex, pageSize, null, locale);


            WalletBalance walletBalance = walletService.findMemberWalletBalance(memberId, walletTypeConfig.getWalletType());

            if (walletBalance == null) {
                walletBalance = new WalletBalance(true);
            }

            CoinBalanceBO bo = new CoinBalanceBO();
            bo.setTotalBalance(walletBalance.getTotalBalance());
            bo.setAvailableBalance(walletBalance.getAvailableBalance());
            bo.setWalletType(walletTypeConfig.getWalletType());
            bo.setWallet(RestUtil.getWalletInfo(locale, walletBalance.getWalletType()));
            bo.setData(pair.getRight());
            bo.setTotalRecords(pair.getLeft());

            String iconUrl = cryptoProvider.getServerUrl() + "/asset/image/coin/" + currencyCode.toLowerCase() + ".png";
            bo.setIconUrl(iconUrl);

            return ResponseBuilder.newInstance() //
                    .setRoot(bo) //
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/history/profit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getWithdrawReceiveProfit(@QueryParam("startIndex") int startIndex,
                                             @QueryParam("pageSize") Integer pageSize) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        String ownerType = Global.UserType.MEMBER;
        try {
            WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(ownerType, Global.WalletType.BWP);

            final String currencyCode = walletTypeConfig.getCryptoType();

            pageSize = RestUtil.getDefaultPageSizeIfNull(pageSize);

            if (walletTypeConfig == null) {
                throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
            }

            Pair<Long, List<TransferReceiveTrx>> pair;

//            String[] trxTypes = {Global.WalletTrxType.LIVE_STREAM_GIFT_INCOME, Global.WalletTrxType.LIVE_STREAM_WITHDRAW};

            pair = populateCoinIncomeHistoryTrx(memberId, currencyCode, startIndex, pageSize, locale);

            CoinBalanceBO bo = new CoinBalanceBO();
            bo.setData(pair.getRight());
            bo.setTotalRecords(pair.getLeft());

            return ResponseBuilder.newInstance() //
                    .setRoot(bo) //
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private Pair<Long, List<TransferReceiveTrx>> populateCoinHistoryTrx(String ownerId, String ownerType, String cryptoType, int walletType, int startIndex, Integer pageSize, String[] trxTypes, Locale locale) {
        DatagridModel<WalletTrx> datagridModel = RestUtil.getDatagridModel("trxDatetime", "desc", startIndex, pageSize);
        walletService.findWalletTrxsForCryptoListing(datagridModel, ownerId, ownerType, walletType, trxTypes);

        boolean isCn = Global.LOCALE_CN.equals(locale);
        List<WalletTrx> walletTrxes = datagridModel.getRecords();
        List<TransferReceiveTrx> transferReceiveTrxes = walletTrxes.stream().map(walletTrx -> {
            TransferReceiveTrx transferReceiveTrx = new TransferReceiveTrx();
            transferReceiveTrx.setWalletTrxId(walletTrx.getWalletRefno());
            transferReceiveTrx.setTrxType(walletTrx.getTrxType());
            transferReceiveTrx.setWalletRefId(walletTrx.getWalletRefId());
            transferReceiveTrx.setCurrencyCode(cryptoType);
            transferReceiveTrx.setTrxDatetime(walletTrx.getTrxDatetime());
            if (BDUtil.gZero(walletTrx.getInAmt())) {
                transferReceiveTrx.setAmount(walletTrx.getInAmt());
            } else {
                transferReceiveTrx.setAmount(walletTrx.getOutAmt().negate());
            }

            String desc = isCn && StringUtils.isNotBlank(walletTrx.getTrxDescCn()) ? walletTrx.getTrxDescCn() : walletTrx.getTrxDesc();
            transferReceiveTrx.setDesc(desc);

            return transferReceiveTrx;
        }).collect(Collectors.toList());

        long totalRecords = datagridModel.getTotalRecords();

        return new ImmutablePair<>(totalRecords, transferReceiveTrxes);
    }

    private Pair<Long, List<TransferReceiveTrx>> populateCoinIncomeHistoryTrx(String ownerId, String cryptoType, int startIndex, Integer pageSize, Locale locale) {
//        DatagridModel<WalletTrx> datagridModel = RestUtil.getDatagridModel("trxDatetime", "desc", startIndex, pageSize);
        DatagridModel<PointReport> datagridModel = RestUtil.getDatagridModel("date", "desc", startIndex, pageSize);
//        walletService.findWalletTrxsForCryptoListing(datagridModel, ownerId, ownerType, walletType, trxTypes);
        pointService.getGiftPointIncomeHistory(datagridModel, ownerId);
        boolean isCn = Global.LOCALE_CN.equals(locale);
        List<PointReport> walletTrxes = datagridModel.getRecords();
        //put wallet trx as history
        List<TransferReceiveTrx> transferReceiveTrxes = walletTrxes.stream().map(history -> {
            TransferReceiveTrx transferReceiveTrx = new TransferReceiveTrx();
            transferReceiveTrx.setWalletTrxId("");
            transferReceiveTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_GIFT_INCOME);
            transferReceiveTrx.setWalletRefId("");
            transferReceiveTrx.setCurrencyCode(cryptoType);
            transferReceiveTrx.setTrxDatetime(history.getTrxDate());

            //TODO: TENTATIVE DESIGN STILL (MAY NEED TO FINALISE)
            if (BDUtil.gZero(history.getTotalAmt())) {
                //if this is out amount, then just set it as total but with negative
                if(BDUtil.gZero(history.getOutAmt())){
                    transferReceiveTrx.setAmount(history.getTotalAmt().multiply(new BigDecimal("-1.0")));
                } else {
                    transferReceiveTrx.setAmount(history.getTotalAmt());
                }
            } else {
                transferReceiveTrx.setAmount(history.getTotalAmt().negate());
            }
//            String desc = isCn && StringUtils.isNotBlank(walletTrx.getTrxDescCn()) ? walletTrx.getTrxDescCn() : walletTrx.getTrxDesc();
            transferReceiveTrx.setDesc(i18n.getText("monthlyIncome", locale, history.getTrxDate()));

            return transferReceiveTrx;
        }).collect(Collectors.toList());

        long totalRecords = datagridModel.getTotalRecords();

        return new ImmutablePair<>(totalRecords, transferReceiveTrxes);
    }

    private CoinBalanceBO getWalletBalanceBO(Locale locale, String memberId, String ownerType) {

        String currencyCode = Global.WalletType.WC;
        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, currencyCode);
        WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, ownerType, walletTypeConfig.getWalletType());

        CoinBalanceBO bo = new CoinBalanceBO();
        bo.setTotalBalance(walletBalance.getTotalBalance());
        bo.setAvailableBalance(walletBalance.getAvailableBalance());
        bo.setWalletType(walletTypeConfig.getWalletType());

        bo.setWallet(RestUtil.getWalletInfo(locale, walletBalance.getWalletType()));

        String iconUrl = cryptoProvider.getServerUrl() + "/asset/image/coin/" + currencyCode.toLowerCase() + ".png";
        bo.setIconUrl(iconUrl);
        // Get legal currency symbol to convert fiat balance to legal currency
        Member member = memberService.getMember(memberId);
        return bo;
    }

    private CoinProfitBO getWalletProfitBO(Locale locale, String memberId, String ownerType) {
        String currencyCode = Global.WalletType.BWP;
        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, currencyCode);

        // prepare for future they want to do withdraw function, else there is no need to have wallet balance
        WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, ownerType, walletTypeConfig.getWalletType());

        CoinProfitBO bo = new CoinProfitBO();
        bo.setTotalBalance(walletBalance.getTotalBalance());
        bo.setAvailableBalance(walletBalance.getAvailableBalance());
        bo.setWalletType(walletTypeConfig.getWalletType());
        bo.setTotalWithdraw((walletService.getWalletBalanceByTrxType(memberId, Global.UserType.MEMBER, walletTypeConfig.getWalletType(), Global.WalletTrxType.WITHDRAW)).multiply(new BigDecimal("-1")));
        bo.setTodayEarned(pointService.getTotalGiftPointWithinDate(memberId, new Date(), new Date()));
//        bo.setTodayEarned(walletService.getWalletBalanceFromDate(memberId, walletTypeConfig.getWalletType(), new Date(), Global.WalletTrxType.LIVE_STREAM_GIFT_INCOME));
        bo.setMonthlyEarned(pointService.getTotalGiftPointWithinDate(memberId, DateUtil.getFirstDayOfMonth(), new Date()));
        bo.setTotalEarned(pointService.getTotalPointReceivedByOwnerId(memberId, false));
        bo.setWallet(RestUtil.getWalletInfo(locale, walletBalance.getWalletType()));
        String iconUrl = cryptoProvider.getServerUrl() + "/asset/image/coin/" + currencyCode.toLowerCase() + ".png";
        bo.setIconUrl(iconUrl);
        bo.setRemark(i18n.getText("babyWhalePointToMYRDesc", locale));

        return bo;
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("balance/profit")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getProfitWalletBalance() {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        String ownerType = Global.UserType.MEMBER;
        try {
            Member member = memberService.getMember(memberId);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
//            if (member.getUserRole() == null || !member.getUserRole().equalsIgnoreCase(Global.UserType.VJ)) {
//                throw new ValidatorException(i18n.getText("noAccessNotVj", locale));
//            }
            CoinProfitBO bo = getWalletProfitBO(locale, memberId, ownerType);
            return ResponseBuilder.newInstance().setRoot(bo).createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/ios/inAppPurchase")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response iosInAppPurchase(OmcPaymentDto omcPaymentDto) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        String ownerType = Global.UserType.MEMBER;
        try {

            IosPaymentClient client = new IosPaymentClient();
            client.iosInAppPurchase(locale, memberId, omcPaymentDto.getReceiptData());

            return ResponseBuilder.newInstance()
                    .addMessage(i18n.getText("topupSuccessfully", locale))
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/android/inAppPurchase")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response androidInAppPurchase(OmcPaymentDto omcPaymentDto) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        String ownerType = Global.UserType.MEMBER;
        try {
            log.debug("-----Android In App Purchase --------------");
            log.debug("Package Name: " + omcPaymentDto.getPackageName());
            log.debug("Product Id: " + omcPaymentDto.getProductId());
            log.debug("Purchase Token: " + omcPaymentDto.getPurchaseToken());
            log.debug("Member ID: " + memberId);

            AndroidPaymentClient client = new AndroidPaymentClient();
            client.androidInAppPurchase(locale, memberId, omcPaymentDto.getPackageName(), omcPaymentDto.getProductId(),
                    omcPaymentDto.getPurchaseToken(), omcPaymentDto.getQuantity());

            return ResponseBuilder.newInstance()
                    .addMessage(i18n.getText("topupSuccessfully", locale))
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("walletBalance")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSingleWalletBalance() {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        String ownerType = Global.UserType.MEMBER;

        try {
            WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(ownerType, Global.WalletType.WC);
            WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, ownerType, walletTypeConfig.getWalletType());

            return ResponseBuilder.newInstance().setAttribute("availableBalance", walletBalance.getAvailableBalance()).createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
