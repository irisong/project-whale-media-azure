package rest.restapi;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.AnnouncementBO;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Path("announce")
public class AnnounceRest extends ResourceConfig {
    private AnnouncementService announcementService;
    private I18n i18n;

    public AnnounceRest(){
        announcementService = Application.lookupBean(AnnouncementService.class);
        i18n = Application.lookupBean(I18n.class);
    }

    @GET
    @RolesAllowed({ AP.ROLE_MEMBER })
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getAnnouncements(){
        try {
            Locale locale = RestUtil.getUserLocale();
            Locale systemLocale = Application.lookupBean(Locale.class);
            boolean isCn = Global.LOCALE_CN.equals(locale);

            String languageCode = isCn ? locale.getLanguage() : systemLocale.getLanguage();

            List<AnnouncementBO> announcements = announcementService.findAnnouncementsForDashboard(Global.PublishGroup.MEMBER_GROUP, languageCode, Global.AccessModule.WHALE_MEDIA, 1, 3) //
                    .stream().map(announcement -> new AnnouncementBO(announcement.getTitle(), announcement.getBody(), announcement.getPublishDate())) //
                    .collect(Collectors.toList());

            return ResponseBuilder.newInstance() //
                .setRoot(announcements) //
                .createResponse();
        } catch (Exception ex){
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @PermitAll
    @Path("/addAnnouncement")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response addAnnouncement(AnnouncementBO announcementBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            Announcement announcement = announcementService.doSaveAnnouncement(announcementBO.getTitle(), announcementBO.getTitleCn(), announcementBO.getContent(),announcementBO.getContentCn(),
                    Global.Status.ACTIVE, new Date(), Global.PublishGroup.PUBLIC_GROUP, Global.AnnouncementType.CONTENT);
            announcementService.doProcessAnnouncementNotification(announcement);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("addAnnouncementSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
