package rest.restapi.point;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberFansService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.point.dto.PointTrxResultDto;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.point.vo.Symbolic;

import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;
import com.compalsolutions.compal.wallet.vo.WalletBalance;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.restapi.MessageRest;
import rest.vo.FansBadgeJoinBO;
import rest.vo.point.PointTrxBO;
import rest.vo.point.PointTrxBodyBO;
import rest.vo.point.SymbolicBO;
import rest.vo.point.SymbolicItemBO;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Path("point")
public class PointTrxRest extends ResourceConfig {

    private static final Log log = LogFactory.getLog(MessageRest.class);
    private CryptoProvider cryptoProvider;
    private PointService pointService;
    private WalletService walletService;
    private MemberService memberService;
    private RankService rankService;
    private MemberFansService memberFansService;
    private RedisCacheProvider cacheProvider;
    private GiftFileUploadConfiguration config;
    private I18n i18n;

    public PointTrxRest() {
        cryptoProvider = Application.lookupBean(CryptoProvider.class);
        pointService = Application.lookupBean(PointService.class);
        walletService = Application.lookupBean(WalletService.class);
        memberService = Application.lookupBean(MemberService.class);
        rankService = Application.lookupBean(RankService.class);
        memberFansService = Application.lookupBean(MemberFansService.class);
        cacheProvider = Application.lookupBean(RedisCacheProvider.class);
        config = Application.lookupBean(GiftFileUploadConfiguration.BEAN_NAME, GiftFileUploadConfiguration.class);
        i18n = Application.lookupBean(I18n.class);
    }

    private static final String LIST_GIF_GIFT_INCLUDE = "data, "
            + "data\\[\\d+\\], "
            + "data\\[\\d+\\].animationUrl, "
            + "data\\[\\d+\\].version, "
            + "data\\[\\d+\\].imageId, "
            + "data\\[\\d+\\].animationDownloadUrl, "
            + "latestVersion";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("gift")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getSymbols() {
        try {
            LoginInfo loginInfo = RestUtil.getLoginInfo();
            Locale locale = loginInfo.getLocale();
            String memberId = WebUtil.getLoginMemberId(loginInfo);
            Member member = memberService.getMember(memberId);
            String serverUrl = cryptoProvider.getServerUrl();
            String webViewUrl = serverUrl + "/pub/fansBadgeInfo.php";

            if (locale.equals(Global.LOCALE_CN)) {
                webViewUrl += "?languageCode=zh";
            } else if (locale.equals(Global.LOCALE_MS)) {
                webViewUrl = "?languageCode=ms";
            } else {
                webViewUrl = "?languageCode=en";
            }
            boolean isCn = Global.LOCALE_CN.equals(locale);
            List<Symbolic> symbolics = pointService.findAllActiveSymbolicWithItems(rankService.getCurrentRank(
                    member.getTotalExp()
            ));
            String finalWebViewUrl = webViewUrl;
            List<SymbolicBO> symbolicBOS = symbolics.stream()
                    .map(s -> {
                        SymbolicBO bo = new SymbolicBO();
                        bo.setType(s.getType());
                        bo.setCategory(isCn ? s.getCategoryCn() : s.getCategory());
                        bo.setSeq(s.getSeq());
                        bo.setSymbolicId(s.getSymbolicId());

                        List<SymbolicItemBO> itemBOS = s.getSymbolicItemList().stream()
                                .map(i -> {
                                    Map<String, String> itemName = new HashMap<>();
                                    itemName.put(Global.Language.CHINESE, i.getNameCn());
                                    itemName.put(Global.Language.ENGLISH, i.getName());
                                    i.setFileUrlWithParentPath(config.getFullGiftParentFileUrl());
                                    i.setGifFileUrlWithParentPath(config.getFullGiftParentFileUrl());
                                    SymbolicItemBO item = new SymbolicItemBO();

                                    item.setSeq(i.getSeq());
                                    item.setPrice(BDUtil.roundUp2dp(i.getPrice()));
                                    item.setName(isCn ? i.getNameCn() : i.getName());
                                    item.setNameList(itemName);
                                    item.setImageUrl(i.getFileUrl());
                                    item.setType(i.getType());
                                    item.setVersion(i.getGifVersion());

                                    if (i.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
//                                        String gifFileName = i.getGifFilename().substring(0, i.getGifFilename().length() - 4);
                                        item.setAnimationUrl(i.getGifFilename());
                                        item.setAnimationDownloadUrl(i.getGifFileUrl());
                                    }
                                    boolean isWhaleCard = pointService.findSpecialRemarkSymbol(i.getId());
                                    if (isWhaleCard) {
                                        item.setItemTitle(i18n.getText("whaleCardTitle", locale));
                                        item.setItemDescription(i18n.getText("whaleCardDesc", locale));
                                        item.setItemInfo(finalWebViewUrl);
                                    }
                                    item.setItemId(i.getId());
                                    return item;
                                })
                                .filter(Objects::nonNull)
                                .collect(Collectors.toList());
                        bo.setList(itemBOS);
                        return bo;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

            WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, Global.UserType.MEMBER,
                    Global.WalletType.WALLET_80);

            if (symbolics != null) {
                symbolics.clear();
                symbolics = null;
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("gifts", symbolicBOS)
                    .setAttribute("stopSpamDuration", 500) //milisec
                    .addMessage(null)
                    .setAttribute("walletBalance", walletBalance.getAvailableBalance())
                    .createResponse();
        } catch (Exception ex) {

            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("gift")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response sendGift(SymbolicItemBO symbolicItemBO) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();
        String memberId = WebUtil.getLoginMemberId(loginInfo);

        try {
            PointTrxResultDto pointTrx = pointService.doCreatePointTrx(locale, memberId, symbolicItemBO.getSendTo(), new BigDecimal(symbolicItemBO.getQty()), symbolicItemBO.getItemId());
            WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_80);

            FansBadgeJoinBO fansBadgeJoinBO = null;
            if (pointTrx.getFansBadgeName() != null) {
                MemberFans memberFans = new MemberFans(true);
                String fansRank = MemberFans.getFansRank(memberFans.getFansLevel());
                String fansBadgeUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/" + fansRank + ".png";
                fansBadgeJoinBO = new FansBadgeJoinBO();
                fansBadgeJoinBO.setTitle(i18n.getText("joinFansMsg", locale));
                fansBadgeJoinBO.setDescription(i18n.getText("joinFansMsgDesc", locale));
                fansBadgeJoinBO.setFansLevel("1");
                fansBadgeJoinBO.setFansBadgeUrl(fansBadgeUrl);
                fansBadgeJoinBO.setFansBadgeName(pointTrx.getFansBadgeName());
                fansBadgeJoinBO.setFansBadgeTextCode(MemberFans.getFansBadgeTextCode(1));
            }

            Triple<String, String, String> fansBadgeInfoTriple = memberFansService.getDisplayFansBadgeInfo(memberId, symbolicItemBO.getSendTo());
            HashMap<String, String> chatRoomDisplayBadge = null;
            if (fansBadgeInfoTriple.getLeft() != null) {
                chatRoomDisplayBadge = new HashMap<>();
                chatRoomDisplayBadge.put("fansBadgeUrl", fansBadgeInfoTriple.getLeft());
                chatRoomDisplayBadge.put("fansBadgeName", fansBadgeInfoTriple.getMiddle());
                chatRoomDisplayBadge.put("fansBadgeTextColor", fansBadgeInfoTriple.getRight());
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("message", pointTrx.getReturnMsg())
                    .setAttribute("qtySent", pointTrx.getSentQty())
                    .setAttribute("walletBalance", walletBalance.getAvailableBalance())
                    .setAttribute("joinFans", fansBadgeJoinBO)
                    .setAttribute("chatRoomDisplayBadge", chatRoomDisplayBadge)
                    .addMessage(pointTrx.getReturnMsg())
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @GET
    @PermitAll
    @Path("gifGift/{version}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGifSymbols(@PathParam("version") int version) {
        try {
//            List<SymbolicItem> symbolicItems = pointService.findGifAnimationByVersion(version, Global.Status.ACTIVE);
            List<SymbolicItem> symbolicItems = pointService.findCacheGifAnimationByVersion(version, Global.Status.ACTIVE);
//            globalData.put("gifVersion", String.valueOf(pointService.getLatestGifVersion()));
//            int latestVersion = pointService.getLatestGifVersion();
            int latestVersion = cacheProvider.getGlobalDataInt("gifVersion");

            List<SymbolicItemBO> itemBOS = symbolicItems.stream()
                    .map(i -> {
                        i.setGifFileUrlWithParentPath(config.getFullGiftParentFileUrl());
                        SymbolicItemBO item = new SymbolicItemBO();
                        String gifFileName = i.getGifFilename();
                        item.setAnimationUrl(gifFileName);
                        item.setImageId(i.getGifId());
                        item.setAnimationDownloadUrl(i.getGifFileUrl());
                        item.setVersion(i.getGifVersion());
                        return item;
                    })
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());
            if (symbolicItems != null) {
                symbolicItems.clear();
                symbolicItems = null;
            }
            return ResponseBuilder.newInstance()
                    .setAttribute("data", itemBOS)
                    .setAttribute("latestVersion", latestVersion)
                    .setIncludeProperties(LIST_GIF_GIFT_INCLUDE)
                    .addMessage(null)
                    .createResponse();
        } catch (Exception ex) {

            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @POST
    @Path("/giftRanking")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getPopularAndContributionRanking(PointTrxBodyBO reqBody) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        boolean isOwner = true;

        try {
            if (reqBody.getIsLiveStream() == 1) {
                if (StringUtils.isBlank(reqBody.getOwnerId())) {
                    throw new ValidatorException("ownerId must not be blank if isLiveStream");
                } else if (!memberId.equalsIgnoreCase(reqBody.getOwnerId())) {
                    isOwner = false;
                }
            }

            List<PointTrxBO> pointTrxBOS = getPointTrxDataGrid(reqBody, reqBody.getOwnerId(), memberId, reqBody.getType());
            PointTrx pointTrx = pointService.findOnePointTrxForPersonal(reqBody.getOwnerId(), memberId, reqBody.getType(), reqBody.getDateRangeType(),
                    reqBody.getIsLiveStream() == 1, isOwner);
            PointTrxBO pointTrxBO = getPointTrxBO(pointTrx, reqBody.getOwnerId(), memberId, reqBody.getType());
            return ResponseBuilder.newInstance()
                    .setAttribute("data", pointTrxBOS) //
                    .setAttribute("lastRowData", pointTrxBO)
                    .addMessage(null)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @POST
    @Path("/giftRanking/personal")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getPersonalCenterPopularAndContributionRanking(PointTrxBodyBO reqBody) {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        String ownerId = memberId;
        if (StringUtils.isNotBlank(reqBody.getOwnerId())) {
            ownerId = reqBody.getOwnerId();
        }
        try {
            List<PointTrxBO> pointTrxBOS = getPointTrxDataGrid(reqBody, ownerId, memberId, reqBody.getType());
            return ResponseBuilder.newInstance()
                    .setAttribute("data", pointTrxBOS)
                    .addMessage(null)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

//    @POST
//    @Path("/testRedis")
//    @RolesAllowed({AP.ROLE_MEMBER})
//    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    public Response getSymbolicItem(PointTrxBodyBO reqBody) {
//        LoginInfo loginInfo = RestUtil.getLoginInfo();
//        try {
//            List<SymbolicItem> items = cacheProvider.getSymbolicItems();
//            return ResponseBuilder.newInstance()
//                    .setAttribute("recordsCount", items.size())
//                    .setAttribute("data", items)
//                    .addMessage(null)
//                    .createResponse();
//        } catch (Exception ex) {
//            log.error(ex.getMessage(), ex.fillInStackTrace());
//            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
//        }
//    }
//
//    @POST
//    @Path("/updateRedis")
//    @RolesAllowed({AP.ROLE_MEMBER})
//    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    public Response removeSymbolicItem(PointTrxBodyBO reqBody) {
//        try {
//            List<SymbolicItem> items = cacheProvider.getSymbolicItems();
//            SymbolicItem symbolicItem = null;
//            for (SymbolicItem item : items) {
//                if (item.getId().equalsIgnoreCase(reqBody.getId())) {
//                    symbolicItem = item;
//                    break;
//                }
//            }
//            if (symbolicItem != null) {
//                if (StringUtils.isBlank(reqBody.getAction())) {
//                    cacheProvider.addSymbolicItem(symbolicItem);
//                } else {
//                    cacheProvider.removeSymbolicItem(symbolicItem);
//                }
//            }
//            return ResponseBuilder.newInstance()
//                    .setAttribute("data", items)
//                    .addMessage(null)
//                    .createResponse();
//        } catch (Exception ex) {
//            log.error(ex.getMessage(), ex.fillInStackTrace());
//            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
//        }
//    }

    private List<PointTrxBO> getPointTrxDataGrid(PointTrxBodyBO reqBody, String ownerId, String memberId, String type) {
        List<PointTrxBO> pointTrxBOS = new ArrayList<>();
        DatagridModel<PointTrx> pointTrxs = initPagination(reqBody);
        pointService.findPointTrxForDatagrid(pointTrxs, reqBody.getType(), reqBody.getDateRangeType(), ownerId);
        List<PointTrx> pointTrxsRecords = pointTrxs.getRecords();

        if (pointTrxsRecords != null && pointTrxsRecords.size() > 0) {
            pointTrxBOS = pointTrxsRecords.stream()
                    .map(p ->
                            getPointTrxBO(p, ownerId, memberId, type)
                    ).filter(Objects::nonNull)
                    .collect(Collectors.toList());
        }
        if (pointTrxsRecords != null) {
            pointTrxsRecords.clear();
            pointTrxsRecords = null;
        }
        return pointTrxBOS;
    }

    private DatagridModel<PointTrx> initPagination(PointTrxBodyBO reqBody) {
        DatagridModel<PointTrx> pointTrx = new DefaultDatagridModel<>();
        if (reqBody.getPage() <= 0) {
            throw new ValidatorException("Invalid Page number");
        }
        pointTrx.setCurrentPage(reqBody.getPage());
        if (reqBody.getPageSize() <= 0) {
            reqBody.setPageSize(10);
        }
        pointTrx.setPageSize(reqBody.getPageSize());

        return pointTrx;
    }

    private PointTrxBO getPointTrxBO(PointTrx pointTrx, String ownerId, String memberId, String type) {
        boolean isPopular = type.equalsIgnoreCase(PointTrx.POPULAR);
        PointTrxBO pointTrxBO = new PointTrxBO();
        if (pointTrx != null) {
            pointTrxBO.setPoint(pointTrx.getPoint());
            pointTrxBO.setMemberId(pointTrx.getMemberId());
            pointTrxBO.setRowNum(pointTrx.getRowNum());
            memberId = pointTrx.getMemberId();
        } else {
            // zero point doesn't contribute to any ranking
            pointTrxBO.setPoint(BigDecimal.ZERO);
            if (StringUtils.isBlank(ownerId)) {
                ownerId = memberId;
            }
            pointTrxBO.setMemberId(isPopular ? ownerId : memberId);
            pointTrxBO.setRowNum(0);
        }
        final String parentUrl = cryptoProvider.getFileUploadUrl() + "/file/memberFile";
        Member member = memberService.getMember(pointTrxBO.getMemberId());
        if (member != null) {
            MemberDetail md = member.getMemberDetail();
            if (md != null) {
                // get user data
                pointTrxBO.setProfileName(md.getProfileName());
                pointTrxBO.setWhaleLiveId(md.getWhaleliveId());
                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(pointTrxBO.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                // get profile picture
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl);
                    pointTrxBO.setImageUrl(memberFile.getFileUrl());
                }
            }
        }
        return pointTrxBO;
    }

    @POST
    @PermitAll
    @Path("/createSymbol")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createSymbol(Symbolic symbolic) {
        try {
            pointService.doCreateSymbol(symbolic);
            return ResponseBuilder.newInstance()
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @POST
    @PermitAll
    @Path("/createSymbolItem")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response createSymbolItem(SymbolicItem symbolicItem) {
        try {
            pointService.doCreateSymbolItems(symbolicItem);
            return ResponseBuilder.newInstance()
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/rewardPoint/history")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getRewardPointHistory(@QueryParam("page") int page, @QueryParam("pageSize") Integer pageSize) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        String ownerType = Global.UserType.MEMBER;
        try {
            if (page <= 0) {
                throw new ValidatorException("Invalid Page number");
            }
            pageSize = RestUtil.getDefaultPageSizeIfNull(pageSize);

            DatagridModel<PointWalletTrx> dataPaging = new DefaultDatagridModel<>();
            dataPaging.setCurrentPage(page);
            dataPaging.setPageSize(pageSize);

            WalletTypeConfig walletTypeConfig = WebUtil.getActiveWalletTypeConfig(ownerType, Global.WalletType.WRP);

            List<Map<String, Object>> trxDetailList = walletService.findPointWalletTrxForListing(dataPaging, locale, memberId, ownerType, walletTypeConfig.getWalletType());
            WalletBalance rewardPointBalance = walletService.doCreateWalletBalanceIfNotExists(memberId, Global.UserType.MEMBER, Global.WalletType.WALLET_81);

            return ResponseBuilder.newInstance()
                    .setAttribute("transactions", trxDetailList)
                    .setAttribute("total", rewardPointBalance.getAvailableBalance())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
