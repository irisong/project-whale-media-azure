package rest.restapi.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.FreqAskQuesFileUploadConfiguration;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.FreqAskQuesService;
import com.compalsolutions.compal.general.vo.FreqAskQues;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@Path("freqAskQues")
public class FreqAskQuesRest extends ResourceConfig {

    private FreqAskQuesService freqAskQuesService;

    public FreqAskQuesRest() {
        freqAskQuesService = Application.lookupBean(FreqAskQuesService.class);
    }

    private static final String LIST_FAQ_INCLUDE = "data, "
            + "data\\[\\d+\\], "
            + "data\\[\\d+\\].category, "
            + "data\\[\\d+\\].freqAskQuesFileUrl";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response list() {
        LoginInfo loginInfo = RestUtil.getLoginInfo();
        Locale locale = loginInfo.getLocale();

        try {
            FreqAskQuesFileUploadConfiguration config = Application.lookupBean(FreqAskQuesFileUploadConfiguration.BEAN_NAME, FreqAskQuesFileUploadConfiguration.class);
            String fileUrl = config.getFullFaqParentFileUrl();

            List<FreqAskQues> freqAskQuesList = freqAskQuesService.findAllFreqAskQues(Global.Status.ACTIVE);

            freqAskQuesList.forEach(faq -> {
                String title = faq.getTitle();
                String fileLang = FreqAskQuesFile.LANG_EN;
                if (locale.equals(Global.LOCALE_CN)) {
                    title = faq.getTitleCn();
                    fileLang = FreqAskQuesFile.LANG_CN;
                } else if (locale.equals(Global.LOCALE_MS)) {
                    title = faq.getTitleMs();
                    fileLang = FreqAskQuesFile.LANG_MS;
                }
                faq.setCategory(title);

                List<FreqAskQuesFile> freqAskQuesFiles = faq.getFreqAskQuesFile();
                if (freqAskQuesFiles.size() > 0) {
                    FreqAskQuesFile faqFile = freqAskQuesService.findFreqAskQuesFileByQuestionIdAndLanguage(faq.getQuestionId(), fileLang);

                    if (faqFile != null) {
                        faq.setFreqAskQuesFileUrl(faqFile.getFileUrlWithParentPath(fileUrl));
                    } else { // if no particular language, get combined language file
                        faqFile = freqAskQuesService.findFreqAskQuesFileByQuestionIdAndLanguage(faq.getQuestionId(), FreqAskQuesFile.COMBINED);

                        if (faqFile != null)
                            faq.setFreqAskQuesFileUrl(faqFile.getFileUrlWithParentPath(fileUrl));
                    }
                }
            });

            // remove faq that have no any file
            Iterator<FreqAskQues> i = freqAskQuesList.iterator();
            while (i.hasNext()) {
                FreqAskQues s = i.next();
                if(StringUtils.isBlank(s.getFreqAskQuesFileUrl())){
                    i.remove();
                }
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("data", freqAskQuesList)
                    .setIncludeProperties(LIST_FAQ_INCLUDE)
                    .createResponse();
        } catch (Exception ex) {
            ex.printStackTrace();
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
