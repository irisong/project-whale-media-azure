package rest.restapi.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.HelpDeskFileUploadConfiguration;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.general.vo.HelpDeskReply;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.CollectionUtil;
import org.apache.commons.lang3.StringUtils;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.helpDesk.HelpDeskBO;
import rest.vo.helpDesk.HelpDeskReplyBO;
import rest.vo.helpDesk.HelpDeskReplyFileBO;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Path("helpDesk")
public class HelpDeskRest extends ResourceConfig {
    private MemberService memberService;
    private HelpDeskService helpDeskService;
    private HelpDeskFileUploadConfiguration config;
    private I18n i18n;

    public HelpDeskRest() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
        config = Application.lookupBean(HelpDeskFileUploadConfiguration.BEAN_NAME, HelpDeskFileUploadConfiguration.class);
        i18n = Application.lookupBean(I18n.BEAN_NAME, I18n.class);
    }

    @GET
    @Path("/info/ticketType")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getTicketType() {
        try {
            Locale locale = RestUtil.getUserLocale();
            List<OptionBean> helpDeskTypes;

            OptionBeanUtil options = new OptionBeanUtil(locale);
            if (Global.LOCALE_CN.equals(locale)) {
                helpDeskTypes = options.getHelpDeskTypesByLanguage(Global.Language.CHINESE, Global.Status.ACTIVE);
            } else if(Global.LOCALE_MS.equals(locale)){
                helpDeskTypes = options.getHelpDeskTypesByLanguage(Global.Language.MALAY, Global.Status.ACTIVE);
            } else {
                helpDeskTypes = options.getHelpDeskTypesByLanguage(Global.Language.ENGLISH, Global.Status.ACTIVE);
            }

            String supportTeamOperatingHrMsg = i18n.getText("supportTeamOperatingHrMsg", locale);
            return ResponseBuilder.newInstance()
                    .setAttribute("helpDeskType", helpDeskTypes)
                    .setAttribute("maxUploadAllow", HelpDesk.MAX_FILE_UPLOAD)
                    .setAttribute("supportTeamOperatingHrMsg", supportTeamOperatingHrMsg)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.build(Response.Status.BAD_REQUEST, ex.getMessage());
        }
    }

    @GET
    @Path("/info")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getHelpDeskReplyInfo(@QueryParam("ticketTypeId") String ticketTypeId, //category id
                                         @QueryParam("ticketId") String ticketId) {
        try {
            Locale locale = RestUtil.getUserLocale();
            LoginInfo loginInfo = RestUtil.getLoginInfo();

            List<HelpDesk> helpDesksList = new ArrayList<>();

            if (StringUtils.isNotBlank(ticketId)) {
                helpDesksList.add(helpDeskService.getHelpDesk(ticketId));
            } else {
                helpDesksList = helpDeskService.findHelpDeskByUserId(loginInfo.getUserId(), ticketTypeId);
            }

            List<HelpDeskBO> helpDeskBOList = new ArrayList<>();
            if (CollectionUtil.isNotEmpty(helpDesksList)) {
                helpDeskBOList = helpDesksList.stream()
                        .sorted(Comparator.comparing(HelpDesk::getDatetimeAdd).reversed())
                        .map(
                        h -> {
                            HelpDeskBO bo = new HelpDeskBO();
                            bo.setTicketId(h.getTicketId());
                            bo.setTicketNo(h.getTicketNo());
                            bo.setCreateTicketDate(h.getDatetimeAdd());
                            bo.setSubject(h.getSubject());
                            bo.setStatus(h.getStatus());

                            HelpDeskType type = helpDeskService.getHelpDeskType(h.getTicketTypeId());
                            if (Global.LOCALE_CN.equals(locale)) {
                                bo.setTicketType(type.getTypeNameCn());
                            } else if (Global.LOCALE_MS.equals(locale)) {
                                bo.setTicketType(type.getTypeNameMs());
                            } else {
                                bo.setTicketType(type.getTypeName());
                            }

                            List<HelpDeskReplyBO> helpDeskReplyBOList = new ArrayList<>();
                            List<HelpDeskReply> helpDeskReplies = h.getHelpDeskReplies();
                            if (CollectionUtil.isNotEmpty(helpDeskReplies)) {
                                helpDeskReplyBOList = helpDeskReplies.stream()
                                        .sorted(Comparator.comparing(HelpDeskReply::getReplySeq).reversed())
                                        .map(
                                        helpDeskReply -> {
                                            boolean fromAdmin = false;
                                            String username;
                                            if (helpDeskReply.getSenderType().equalsIgnoreCase(Global.UserType.HQ)) {
                                                fromAdmin = true;
                                                username = helpDeskReply.getSender().getUsername(); //ADMIN
                                            } else {
                                                MemberDetail memberDetail = memberService.findMemberDetailsByContactNo(helpDeskReply.getSender().getUsername());
                                                username = memberDetail.getProfileName();
                                            }

                                            HelpDeskReplyBO replyListBO = new HelpDeskReplyBO();
                                            replyListBO.setReplyId(helpDeskReply.getReplyId());
                                            replyListBO.setReplySeq(helpDeskReply.getReplySeq());
                                            replyListBO.setUsername(username);
                                            replyListBO.setFromAdmin(fromAdmin);
                                            replyListBO.setReplyDate(helpDeskReply.getDatetimeAdd());
                                            replyListBO.setMessage(helpDeskReply.getMessage());

                                            List<HelpDeskReplyFile> replyFiles = helpDeskReply.getHelpDeskReplyFiles();
                                            List<HelpDeskReplyFileBO> fileBOList = new ArrayList<>();
                                            if (CollectionUtil.isNotEmpty(replyFiles)) {
                                                final String fileServerUrl = config.getFullHelpDeskParentFileUrl();

                                                fileBOList = replyFiles.stream().map(
                                                        f -> {
                                                            HelpDeskReplyFileBO fileBO = new HelpDeskReplyFileBO();
                                                            fileBO.setFileName(f.getFilename());
                                                            fileBO.setFileUrl(f.getFileUrlWithParentPath(fileServerUrl));
                                                            return fileBO;
                                                        }
                                                ).collect(Collectors.toList()); //Help Desk Reply Files
                                            }

                                            replyListBO.setAttachedFile(fileBOList);

                                            return replyListBO;
                                        }).collect(Collectors.toList()); //Help Desk Replies
                            }

                            bo.setReplyMessages(helpDeskReplyBOList);
                            bo.setMaxUploadAllow(HelpDesk.MAX_FILE_UPLOAD);

                            return bo;
                        }).collect(Collectors.toList()); // Help Desk Main
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("helpDeskList", helpDeskBOList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
