package rest.restapi;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.websocket.WsManagerProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.core.env.Environment;
import rest.ResponseBuilder;
import rest.vo.WebsocketBO;
import websocket.LiveStreamWebsocket;
import websocket.WsOutGoingUtil;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("webSocket")
public class WebSocketMsgRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(WebSocketMsgRest.class);

    private boolean isProdServer;
    private LiveStreamService liveStreamService;
    private LiveStreamProvider liveStreamProvider;
    private RedisCacheProvider redisCacheProvider;
    private WsManagerProvider wsManagerProvider;

    public WebSocketMsgRest() {
        Environment env = Application.lookupBean(Environment.class);
        isProdServer = env.getProperty("server.production", Boolean.class, true);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        redisCacheProvider = Application.lookupBean(RedisCacheProvider.class);
        wsManagerProvider = Application.lookupBean(WsManagerProvider.class);

    }

    @PermitAll
    @POST
    @Path("/liveStream2")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response sendMessageToBroadcast(WebsocketBO websocketBO) {
        try {
            if (isProdServer) {
                throw new ValidatorException("Not allowed to access");
            }

            String channelId = websocketBO.getChannelId();

            LiveStreamDto liveStreamDto = new LiveStreamDto();
            liveStreamDto.setChannelId(channelId);
            liveStreamDto.setPoint(websocketBO.getPoint());
            liveStreamDto.setView(websocketBO.getView());
            liveStreamDto.setMemberList(null);

            WsOutGoingUtil.broadcastMessage_processVJPoint(liveStreamDto);
            return ResponseBuilder.newInstance()
                    .addMessage("message sent")
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @PermitAll
    @POST
    @Path("/liveStream")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response addMessageToPoint(WebsocketBO websocketBO) {
        try {
            if (isProdServer) {
                throw new ValidatorException("Not allowed to access");
            }
//            LiveStreamDto wsLiveStream = liveStreamProvider.getChannelDetail(websocketBO.getChannelId());
//            LiveStreamWebsocket liveStreamWebsocket = new LiveStreamWebsocket();
//            liveStreamWebsocket.sendLatestChannelInfo(websocketBO.getChannelId(), websocketBO.getPoint(), websocketBO.getView(), wsLiveStream.getMemberList());

            return ResponseBuilder.newInstance()
                    .addMessage("message sent")
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @PermitAll
    @POST
    @Path("/closeChannel")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response closeChannelWebsocket(WebsocketBO websocketBO) {
        try {
            if (isProdServer) {
                throw new ValidatorException("Not allowed to access");
            }

            // remove channel if VJ close streaming
            LiveStreamWebsocket liveStreamWebsocket = new LiveStreamWebsocket();
            liveStreamWebsocket.removeWholeChannel(websocketBO.getChannelId());
            return ResponseBuilder.newInstance()
                    .addMessage("channel closed")
                    .createResponse();
        } catch (Exception ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @PermitAll
    @GET
    @Path("/getSystemData")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getSystemData() {
        try {

            return ResponseBuilder.newInstance()
                    .setAttribute("Redis Status", redisCacheProvider.getRedisStatus())
                    .setAttribute("Redis Banner Status", redisCacheProvider.getBannerStatus())
                    .setAttribute("Total Websocket Sessions", wsManagerProvider.getSessions().size())
                    .setAttribute("Total Channel Sessions", wsManagerProvider.getChannelSessions().size())
                    .setAttribute("Total Audience Online", wsManagerProvider.getSessions().size() - wsManagerProvider.getChannelSessions().size())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/switchOffRedis")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getVcloudLiveStreamChannelList(WebsocketBO websocketBO) {
        try {

            if(websocketBO.getChannelId().equalsIgnoreCase("aioTestRedis")) {
                redisCacheProvider.setRedisStatus(false);
            }
            else
            {
                throw new ValidatorException("Not allowed to access");
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());

            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

}
