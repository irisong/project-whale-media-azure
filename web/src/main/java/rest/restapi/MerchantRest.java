package rest.restapi;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.*;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.MemberAddress;
import com.compalsolutions.compal.merchant.dto.MerchantOrderDto;
import com.compalsolutions.compal.merchant.service.MerchantOrderService;
import com.compalsolutions.compal.merchant.service.MerchantService;
import com.compalsolutions.compal.merchant.service.ProductService;
import com.compalsolutions.compal.merchant.service.ShippingService;
import com.compalsolutions.compal.merchant.vo.*;

import com.compalsolutions.compal.struts.bean.OptionBean;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.*;
import rest.vo.AreaBO;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;

@Path("merchant")
public class MerchantRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(MerchantRest.class);

    private I18n i18n;
    private MerchantService merchantService;
    private ProductService productService;
    private MerchantOrderService merchantOrderService;
    private ShippingService shippingService;
    private CountryService countryService;
    private MemberService memberService;

    public MerchantRest() {
        i18n = Application.lookupBean(I18n.class);
        merchantService = Application.lookupBean(MerchantService.class);
        productService = Application.lookupBean(ProductService.class);
        merchantOrderService = Application.lookupBean(MerchantOrderService.class);
        shippingService = Application.lookupBean(ShippingService.class);
        countryService = Application.lookupBean(CountryService.class);
        memberService = Application.lookupBean(MemberService.class);
    }

    private int checkPage(int page, int pageSize) {
        if (page <= 0) {
            throw new ValidatorException("Invalid Page number");
        }
        return RestUtil.getDefaultPageSizeIfNull(pageSize);
    }

    @GET
    @Path("/currency")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getMerchantCurrencyOptions() {
        try {
            List<String> currencyOptions = new ArrayList<>();
            List<MerchantCurrency> merchantCurrencyList = merchantService.getAllMerchantCurrency(Global.Status.ACTIVE);
            for (MerchantCurrency currency: merchantCurrencyList) { //if there's no product using this currency, no need return this option
                List<Product> products = productService.findProductByCurrencyCode(currency.getCurrencyCode(), null, Global.Status.ACTIVE);
                if (products.size() > 0) {
                    currencyOptions.add(currency.getCurrencyCode());
                }
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("currencyOptions", currencyOptions)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/search")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response searchProduct(ProductBO productBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            int pageSize = checkPage(productBO.getPage(), productBO.getPageSize());
            DatagridModel<Product> dataPaging = new DefaultDatagridModel<>();
            dataPaging.setCurrentPage(productBO.getPage());
            dataPaging.setPageSize(pageSize);

            productService.findProductByNameForListing(dataPaging, productBO.getKeyword(), productBO.getCurrencyCode(), Global.Status.ACTIVE);
            List<Product> productList = productService.getProductInfo(locale, dataPaging.getRecords(), productBO.getCurrencyCode());

            //save history
            productService.doCreateMerchantSearchHistory(memberId, productBO.getKeyword());

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_PRODUCT_LIST_INCLUDE)
                    .setAttribute("productList", productList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_SEARCH_HISTORY_INCLUDE = "searchHistory, "
            + "searchHistory\\[\\d+\\], "
            + "searchHistory\\[\\d+\\].keyword";
    @GET
    @Path("/searchHistory")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response findSearchHistory() {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            List<MerchantSearchHistory> merchantSearchHistoryList = productService.findAllMerchantSearchHistoryByMemberId(memberId);

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_SEARCH_HISTORY_INCLUDE)
                    .setAttribute("searchHistory", merchantSearchHistoryList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/removeSearchHistory")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response removeSearchHistory(@QueryParam("keyword") String keyword) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            productService.doRemoveMerchantSearchHistory(memberId, keyword);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("removeSearchHistorySuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_PRODUCT_CATEGORY_INCLUDE = "categories, "
            + "categories\\[\\d+\\], "
            + "categories\\[\\d+\\].categoryId, "
            + "categories\\[\\d+\\].category ";
    @GET
    @Path("/productCategoryList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getProductCategory(@QueryParam("currencyCode") String currencyCode) {
        try {
            Locale locale = RestUtil.getUserLocale();

            if (StringUtils.isNotBlank(currencyCode)) {
                MerchantCurrency merchantCurrency = merchantService.getMerchantCurrencyByCurrencyCode(currencyCode, Global.Status.ACTIVE);
                if (merchantCurrency == null) {
                    throw new ValidatorException(i18n.getText("invalidCurrencyCode",locale));
                }
            }

            List<ProductCategory> productCategoryList = productService.getProductCategoryByStatus(Global.Status.ACTIVE);
            //if there's no product using this currency, no need return this category
            Iterator<ProductCategory> i = productCategoryList.iterator();
            while (i.hasNext()) {
                ProductCategory productCategory = i.next();
                List<Product> products = productService.findProductByCurrencyCode(currencyCode, productCategory.getCategoryId(), Global.Status.ACTIVE);
                if (products.size() > 0) {
                    if (locale.equals(Global.LOCALE_CN)) {
                        productCategory.setCategory(productCategory.getCategoryCn());
                    }
                } else {
                    i.remove();
                    continue;
                }
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_PRODUCT_CATEGORY_INCLUDE)
                    .setAttribute("categories", productCategoryList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_PRODUCT_LIST_INCLUDE = "productList, "
            + "productList\\[\\d+\\], "
            + "productList\\[\\d+\\].productId, "
            + "productList\\[\\d+\\].productName, "
            + "productList\\[\\d+\\].price, "
            + "productList\\[\\d+\\].currencyCode, "
            + "productList\\[\\d+\\].fileUrl";
    @POST
    @Path("/productList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getProductList(ProductBO productBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            int pageSize = checkPage(productBO.getPage(), productBO.getPageSize());
            DatagridModel<Product> dataPaging = new DefaultDatagridModel<>();
            dataPaging.setCurrentPage(productBO.getPage());
            dataPaging.setPageSize(pageSize);

            productService.findProductForListing(dataPaging, productBO.getCategoryId(), productBO.getCurrencyCode(), Global.Status.ACTIVE);
            List<Product> productList = productService.getProductInfo(locale, dataPaging.getRecords(), productBO.getCurrencyCode());

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_PRODUCT_LIST_INCLUDE)
                    .setAttribute("productList", productList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_PRODUCT_INCLUDE = "product, "
            + "product.productId, "
            + "product.productName, "
            + "product.productDesc, "
            + "product.productUrl, "
            + "product.currencyCode, "
            + "product.price, "
            + "product.outOfStock, "
            + "product.productFiles, "
            + "product.productFiles\\[\\d+\\], "
            + "product.productFiles\\[\\d+\\].fileUrl, "
            + "product.productDetails, "
            + "product.productDetails\\[\\d+\\], "
            + "product.productDetails\\[\\d+\\].productDetId, "
            + "product.productDetails\\[\\d+\\].variant, "
            + "product.productDetails\\[\\d+\\].selectedVariant, "
            + "product.productDetails\\[\\d+\\].availableQty, "
            + "product.productDetails\\[\\d+\\].currencyCode, "
            + "product.productDetails\\[\\d+\\].price, "
            + "product.productDetails\\[\\d+\\].fileUrl, "
            + "product.errorType, "
            + "product.errorMsg";
    @GET
    @Path("/product")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getProduct(@QueryParam("productId") String productId, @QueryParam("currencyCode") String currencyCode) {
        try {
            Locale locale = RestUtil.getUserLocale();
            if (StringUtils.isBlank(productId)) {
                throw new ValidatorException(i18n.getText("invalidProductId", locale));
            }

            Product product = productService.getProductDetailInfo(locale, currencyCode, productId, null, true);
            if (StringUtils.isNotBlank(product.getErrorType())) {
                return ResponseBuilder.newInstance()
                        .setAttribute("errorType", product.getErrorType())
                        .setAttribute("errorMsg", product.getErrorMsg())
                        .createResponse();
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_PRODUCT_INCLUDE)
                    .setAttribute("product", product)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_PRODUCT_DETAIL_INCLUDE = "product, "
            + "product.productId, "
            + "product.productName, "
            + "product.currencyCode, "
            + "product.price, "
            + "product.outOfStock, "
            + "product.productDetails, "
            + "product.productDetails\\[\\d+\\], "
            + "product.productDetails\\[\\d+\\].productDetId, "
            + "product.productDetails\\[\\d+\\].variant, "
            + "product.productDetails\\[\\d+\\].selectedVariant, "
            + "product.productDetails\\[\\d+\\].availableQty, "
            + "product.productDetails\\[\\d+\\].currencyCode, "
            + "product.productDetails\\[\\d+\\].price, "
            + "product.productDetails\\[\\d+\\].fileUrl, "
            + "product.errorType, "
            + "product.errorMsg";
    @GET
    @Path("/productDetail")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getProductDetail(@QueryParam("productId") String productId, @QueryParam("productDetId") String productDetId, @QueryParam("currencyCode") String currencyCode) {
        try {
            Locale locale = RestUtil.getUserLocale();

            if (StringUtils.isBlank(productDetId)) {
                throw new ValidatorException(i18n.getText("invalidProductDetId", locale));
            }

            Product product = productService.getProductDetailInfo(locale, currencyCode, productId, productDetId, false);
            if (StringUtils.isNotBlank(product.getErrorType())) {
                return ResponseBuilder.newInstance()
                        .setAttribute("errorType", product.getErrorType())
                        .setAttribute("errorMsg", product.getErrorMsg())
                        .createResponse();
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_PRODUCT_DETAIL_INCLUDE)
                    .setAttribute("product", product)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_SHIPPING_LIST_INCLUDE = "shippingAddresses, "
            + "shippingAddresses\\[\\d+\\], "
            + "shippingAddresses\\[\\d+\\].addressId, "
            + "shippingAddresses\\[\\d+\\].name, "
            + "shippingAddresses\\[\\d+\\].phoneNo, "
            + "shippingAddresses\\[\\d+\\].address, "
            + "shippingAddresses\\[\\d+\\].defaultAddress, "
            + "shippingAddresses\\[\\d+\\].allowSelect";

    @GET
    @Path("/addressBook")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getAddressBook(@QueryParam("productId") String productId) {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            Locale locale = RestUtil.getUserLocale();

            Product product = productService.doCheckProductErrorType(locale, productId, null);
            if (StringUtils.isNotBlank(product.getErrorType())) {
                return ResponseBuilder.newInstance()
                        .setAttribute("errorType", product.getErrorType())
                        .setAttribute("errorMsg", product.getErrorMsg())
                        .createResponse();
            }
            String [] countryCodes = product.getShipCountry().split("\\|"); //separate countryCode by |

            List<AddressBO> addressBOList = new ArrayList<>();
            List<MemberAddress> memberAddressList = memberService.findMemberAddressByMemberId(memberId);
            for (MemberAddress memberAddress : memberAddressList) {
                //store shipping address list to address bo
                AddressBO addressBo = new AddressBO();
                addressBo.setAddressId(memberAddress.getMemberAddrId());
                addressBo.setDefaultAddress(memberAddress.getDefaultAddress());

                Address address = memberAddress.getAddress();
                addressBo.setName(address.getContactName());
                addressBo.setPhoneNo(address.getContactNo());
                addressBo.setAddress(address.getInternationalFullAddress(locale, ""));

                boolean allowShipping = Arrays.stream(countryCodes).anyMatch(i -> i.equalsIgnoreCase(address.getCountryCode()));
                addressBo.setAllowSelect(allowShipping);

                addressBOList.add(addressBo);
            }
    
            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_SHIPPING_LIST_INCLUDE)
                    .setAttribute("shippingAddresses", addressBOList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/shippingAddress")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getShippingAddress(@QueryParam("addressId") String memberAddrId, @QueryParam("productId") String productId) {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            Locale locale = RestUtil.getUserLocale();

            Product product = productService.doCheckProductErrorType(locale, productId, null);
            if (StringUtils.isNotBlank(product.getErrorType())) {
                return ResponseBuilder.newInstance()
                        .setAttribute("errorType", product.getErrorType())
                        .setAttribute("errorMsg", product.getErrorMsg())
                        .createResponse();
            }

            MemberAddress memberAddress = new MemberAddress(true);
            if (StringUtils.isNotBlank(memberAddrId)) { //for update shipping address
                memberAddress = memberService.getMemberAddressDetails(memberId, memberAddrId);
                if (memberAddress == null) {
                    throw new ValidatorException(i18n.getText("addressNotFound", locale));
                }

                //find stateCode
                Province province = countryService.findProvinceByProvinceName(memberAddress.getAddress().getState());
                if (province != null) {
                    if(memberAddress.getAddress().getState().equalsIgnoreCase(province.getProvinceName())) {
                        memberAddress.getAddress().setState(province.getProvinceCode());
                    }
                }

                //find cityCode
                City city = countryService.findCityByCityName(memberAddress.getAddress().getCity());
                if (city != null) {
                    if(memberAddress.getAddress().getCity().equalsIgnoreCase(city.getCityName())) {
                        memberAddress.getAddress().setCity(city.getCityCode());
                    }
                }

                if (StringUtils.isNotBlank(memberAddress.getAddress().getArea())) {
                    //find areaCode
                    Area area = countryService.findAreaByAreaName(memberAddress.getAddress().getArea());
                    if (area != null) {
                        if (memberAddress.getAddress().getArea().equalsIgnoreCase(area.getAreaName())) {
                            memberAddress.getAddress().setArea(area.getAreaCode());
                        }
                    }
                }

                //separate phoneCountryCode from contactNo
                int splitNum = memberAddress.getPhoneCountryCode().length();
                int phoneNoLength = memberAddress.getAddress().getContactNo().length();
                String newPhoneNo = memberAddress.getAddress().getContactNo().substring(splitNum, phoneNoLength);
                memberAddress.getAddress().setContactNo(newPhoneNo);
            }

            if (memberAddress.getAddress() == null) { //for create shipping address
                memberAddress.setAddress(new Address());
            }

            List<Country> countries = new ArrayList<>();
            String [] countryCodes = product.getShipCountry().split("\\|"); //separate countryCode by |
            for (String countryCode: countryCodes) {
                Country country = countryService.getCountry(countryCode);
                if (country != null) {
                    countries.add(country);
                }
            }

            //list of state, city & area return in getProvinceList to avoid late respond
            CountryBO countryBO;
            List<CountryBO> countryList = new ArrayList<>();
            for (Country country : countries) {
                countryBO = new CountryBO(country.getCountryCode(), country.getCountryName());
                countryList.add(countryBO);
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("addressId", memberAddress.getMemberAddrId())
                    .setAttribute("name", memberAddress.getAddress().getContactName())
                    .setAttribute("phoneNo", memberAddress.getAddress().getContactNo())
                    .setAttribute("countryCode", memberAddress.getAddress().getCountryCode())
                    .setAttribute("address", memberAddress.getAddress().getAddress())
                    .setAttribute("postCode", memberAddress.getAddress().getPostcode())
                    .setAttribute("city", memberAddress.getAddress().getCity())
                    .setAttribute("state", memberAddress.getAddress().getState())
                    .setAttribute("area", memberAddress.getAddress().getArea())
                    .setAttribute("defaultAddress", memberAddress.getDefaultAddress())
                    .setAttribute("countryList", countryList)
                    .setAttribute("phoneCountryCode", memberAddress.getPhoneCountryCode())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/shippingAddress/provinceList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getProvinceList(@QueryParam("countryCode") String countryCode) {
        try {
            Locale locale = RestUtil.getUserLocale();

            Country country = countryService.getCountry(countryCode);
            if (country == null) {
                throw new ValidatorException(i18n.getText("invalidCountry", locale));
            }

            //list of states, include city and area
            List<ProvinceBO> provinceBOList = new ArrayList<>();
            List<Province> provinceList = countryService.getProvinceListByCountryCode(country.getCountryCode());

            for (Province province : provinceList) {
                List<CityBO> cityBOList = new ArrayList<>();
                List<City> cityList = countryService.getCityListByProvinceCode(province.getProvinceCode());

                for (City city : cityList) {
                    List<AreaBO> areaBOList = new ArrayList<>();
                    List<Area> areaList = countryService.getAreaListByCityCode(city.getCityCode());
                    for (Area area : areaList) {
                        areaBOList.add(new AreaBO(area.getAreaCode(), area.getAreaName()));
                    }

                    cityBOList.add(new CityBO(city.getCityCode(), city.getCityName(), areaBOList));
                }

                provinceBOList.add(new ProvinceBO(province.getProvinceCode(), province.getProvinceName(), cityBOList));
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("countryCode", country.getCountryCode())
                    .setAttribute("provinceBOList", provinceBOList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/shippingAddress")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response processShippingAddress(AddressBO addressBO) {
        try{
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            Locale locale = RestUtil.getUserLocale();

            if(StringUtils.isBlank(addressBO.getAddressId())) {
                List<MemberAddress> memberAddress = memberService.findMemberAddressByMemberId(memberId);
                if (memberAddress.size() == 0) { //auto set defaultAddress for first address
                    addressBO.setDefaultAddress(true);
                }

                memberService.doCreateMemberAddress(locale, memberId, addressBO.getName(), addressBO.getPhoneNo(),
                        addressBO.getCountryCode(), addressBO.getAddress(), addressBO.getCity(),
                        addressBO.getPostCode(), addressBO.getState(), addressBO.getArea(), addressBO.isDefaultAddress(), addressBO.getPhoneCountryCode());

                return ResponseBuilder.build(Response.Status.OK, i18n.getText("addShippingAddressSuccessfully", locale));
            } else {
                memberService.doUpdateMemberAddress(locale, memberId, addressBO.getAddressId(), addressBO.getName(), addressBO.getPhoneNo(),
                        addressBO.getCountryCode(), addressBO.getAddress(), addressBO.getCity(),
                        addressBO.getPostCode(), addressBO.getState(), addressBO.getArea(), addressBO.isDefaultAddress(), addressBO.getPhoneCountryCode());

                return ResponseBuilder.build(Response.Status.OK, i18n.getText("editShippingAddressSuccessfully", locale));
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/removeShippingAddress")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response removeShippingAddress(AddressBO addressBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            memberService.doRemoveMemberAddress(locale, memberId, addressBO.getAddressId());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("removeShippingAddressSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_CHECKOUT_INCLUDE = "data, "
            + "data.address, "
            + "data.address.addressId, "
            + "data.address.contactName, "
            + "data.address.contactNo, "
            + "data.address.longAddress, "
            + "data.productDetailList, "
            + "data.productDetailList\\[\\d+\\], "
            + "data.productDetailList\\[\\d+\\].productDetId, "
            + "data.productDetailList\\[\\d+\\].productName, "
            + "data.productDetailList\\[\\d+\\].variant, "
            + "data.productDetailList\\[\\d+\\].currencyCode, "
            + "data.productDetailList\\[\\d+\\].price, "
            + "data.productDetailList\\[\\d+\\].productQty, "
            + "data.productDetailList\\[\\d+\\].fileUrl, "
            + "data.currencyCode, "
            + "data.currentBalance, "
            + "data.shippingFee, "
            + "data.uploadSlip, "
            + "data.errorType, "
            + "data.errorMsg";
    @POST
    @Path("/checkout")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response checkout(ProductBO productBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            MerchantOrderDto merchantOrderDto = merchantOrderService.getCheckoutInfo(locale, memberId, productBO.getCurrencyCode(),
                    productBO.getAddressId(), productBO.getProductId(), productBO.getProductDetId(), productBO.getOrderQty());
            if (StringUtils.isNotBlank(merchantOrderDto.getErrorType())) {
                return ResponseBuilder.newInstance()
                        .setAttribute("errorType", merchantOrderDto.getErrorType())
                        .setAttribute("errorMsg", merchantOrderDto.getErrorMsg())
                        .createResponse();
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_CHECKOUT_INCLUDE)
                    .setAttribute("data", merchantOrderDto)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_CHECKOUT_ORDER_INCLUDE = "data, "
            + "data.orderId, "
            + "data.orderRefNo, "
            + "data.orderStatus, "
//            + "data.orderDatetime, "
//            + "data.expiryDatetime, "
//            + "data.address, "
//            + "data.address.contactName, "
//            + "data.address.contactNo, "
//            + "data.address.longAddress, "
//            + "data.productDetailList, "
//            + "data.productDetailList\\[\\d+\\], "
//            + "data.productDetailList\\[\\d+\\].productDetId, "
//            + "data.productDetailList\\[\\d+\\].productName, "
//            + "data.productDetailList\\[\\d+\\].variant, "
//            + "data.productDetailList\\[\\d+\\].currencyCode, "
//            + "data.productDetailList\\[\\d+\\].price, "
//            + "data.productDetailList\\[\\d+\\].productQty, "
//            + "data.productDetailList\\[\\d+\\].fileUrl, "
            + "data.currencyCode, "
            + "data.currentBalance, "
//            + "data.orderTotal, "
//            + "data.shippingFee, "
            + "data.grandTotal, "
//            + "data.paymentBank, "
//            + "data.paymentAccountName, "
//            + "data.paymentAccountNo, "
//            + "data.uploadSlip, "
            + "data.errorType, "
            + "data.errorMsg";
    @POST
    @Path("/checkout/order")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response processOrder(ProductBO productBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            MerchantOrderDto merchantOrderDto;
            try {
                merchantOrderDto = merchantOrderService.doProcessMerchantOrder(locale, memberId, productBO.getCurrencyCode(), productBO.getAddressId(),
                        productBO.getProductId(), productBO.getProductDetId(), productBO.getOrderQty(), productBO.getGrandTotal());
            } catch (ValidatorException ve) {
                String error = ve.getMessage();

                if (error != null && error.startsWith("ERROR_")) {
                    int idx = error.indexOf("@");
                    merchantOrderDto = new MerchantOrderDto();
                    merchantOrderDto.setErrorType(error.substring(0, idx));
                    merchantOrderDto.setErrorMsg(error.substring(idx+1));
                } else {
                    throw new ValidatorException(error);
                }
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_CHECKOUT_ORDER_INCLUDE)
                    .setAttribute("data", merchantOrderDto)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_ORDER_HISTORY_INCLUDE = "data, "
            + "data\\[\\d+\\], "
            + "data\\[\\d+\\].orderId, "
            + "data\\[\\d+\\].orderRefNo, "
            + "data\\[\\d+\\].status, "
            + "data\\[\\d+\\].orderDatetime, "
            + "data\\[\\d+\\].currencyCode, "
            + "data\\[\\d+\\].grandTotal";

    @GET
    @Path("/orderHistory")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getOrderHistory(@QueryParam("page") int page, @QueryParam("pageSize") int pageSize) {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            pageSize = checkPage(page, pageSize);
            DatagridModel<MerchantOrder> dataPaging = new DefaultDatagridModel<>();
            dataPaging.setCurrentPage(page);
            dataPaging.setPageSize(pageSize);

            merchantOrderService.doGetOrderHistory(dataPaging, memberId);

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_ORDER_HISTORY_INCLUDE)
                    .setAttribute("data", dataPaging.getRecords())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_ORDER_DETAILS_INCLUDE = "data, "
            + "data.orderId, "
            + "data.orderRefNo, "
            + "data.orderStatus, "
            + "data.orderDatetime, "
            + "data.expiryDatetime, "
            + "data.remark, "
            + "data.logisticCompany, "
            + "data.trackingNo, "
            + "data.address, "
            + "data.address.contactName, "
            + "data.address.contactNo, "
            + "data.address.longAddress, "
            + "data.productDetailList, "
            + "data.productDetailList\\[\\d+\\], "
            + "data.productDetailList\\[\\d+\\].productDetId, "
            + "data.productDetailList\\[\\d+\\].productName, "
            + "data.productDetailList\\[\\d+\\].variant, "
            + "data.productDetailList\\[\\d+\\].currencyCode, "
            + "data.productDetailList\\[\\d+\\].price, "
            + "data.productDetailList\\[\\d+\\].productQty, "
            + "data.productDetailList\\[\\d+\\].fileUrl, "
            + "data.currencyCode, "
            + "data.orderTotal, "
            + "data.shippingFee, "
            + "data.grandTotal, "
            + "data.paymentBank, "
            + "data.paymentAccountName, "
            + "data.paymentAccountNo, "
            + "data.uploadSlip, "
            + "data.maxUploadFile, "
            + "data.bankinSlipUrls.*";
    @POST
    @Path("/orderDetails")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getOrderDetails(MerchantOrderDto body) {
        try {
            Locale locale = RestUtil.getUserLocale();
            MerchantOrderDto merchantOrderDto = merchantOrderService.getOrderDetails(locale, body.getOrderId());

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_ORDER_DETAILS_INCLUDE)
                    .setAttribute("data", merchantOrderDto)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }
}
