package rest.restapi.profile;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.event.vo.VjMissionRanking;
import com.compalsolutions.compal.event.vo.VjMissionSpecial;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.FansBadgeService;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.member.service.MemberFansService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.commons.lang3.tuple.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.core.env.Environment;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.*;
import rest.vo.point.LiveStreamPointBO;
import rest.vo.point.PointTrxBO;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

@Path("vj")
public class VJCenterRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(VJCenterRest.class);

    private I18n i18n;
    private String serverUrl;
    private CryptoProvider cryptoProvider;
    private MemberService memberService;
    private LiveStreamService liveStreamService;
    private FansBadgeService fansBadgeService;
    private MemberFansService memberFansService;
    private BroadcastService broadcastService;
    private FansIntimacyService fansIntimacyService;
    private RankService rankService;
    private PointService pointService;
    private VjMissionService vjMissionService;
    private GuildFileUploadConfiguration guildFileUploadConfiguration;
    private MemberFileUploadConfiguration memberFileUploadConfiguration;

    public VJCenterRest() {
        i18n = Application.lookupBean(I18n.class);
        cryptoProvider = Application.lookupBean(CryptoProvider.class);
        memberService = Application.lookupBean(MemberService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        fansBadgeService = Application.lookupBean(FansBadgeService.class);
        memberFansService = Application.lookupBean(MemberFansService.class);
        broadcastService = Application.lookupBean(BroadcastService.class);
        fansIntimacyService = Application.lookupBean(FansIntimacyService.class);
        rankService = Application.lookupBean(RankService.class);
        pointService = Application.lookupBean(PointService.class);
        vjMissionService = Application.lookupBean(VjMissionService.class);
        guildFileUploadConfiguration = Application.lookupBean(GuildFileUploadConfiguration.class);
        memberFileUploadConfiguration = Application.lookupBean(MemberFileUploadConfiguration.class);

        Environment env = Application.lookupBean(Environment.class);
        serverUrl = env.getProperty("serverUrl");
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/live/record/options")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getMemberChannelType() {
        try {
            Locale locale = RestUtil.getUserLocale();
            List<SimpleOptionBean> optionList = Arrays.asList(
                    new SimpleOptionBean(LiveStreamChannel.THIS_MONTH, i18n.getText("thisMonth", locale)),
                    new SimpleOptionBean(LiveStreamChannel.LAST_7_DAYS, i18n.getText("last7Days", locale)),
                    new SimpleOptionBean(LiveStreamChannel.LAST_14_DAYS, i18n.getText("last14Days", locale)),
                    new SimpleOptionBean(LiveStreamChannel.LAST_MONTH, i18n.getText("lastMonth", locale)),
                    new SimpleOptionBean("ALL", i18n.getText("all", locale)),
                    new SimpleOptionBean(LiveStreamChannel.CUSTOM, i18n.getText("custom", locale))
            );

            return ResponseBuilder.newInstance()
                    .setAttribute("optionList", optionList)
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/live/record")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getLiveRecord(@QueryParam("startIndex") int startIndex,
                                  @QueryParam("pageSize") Integer pageSize,
                                  @QueryParam("dateRangeType") String dateRangeType,
                                  @QueryParam("dateFrom") String customDateFrom,
                                  @QueryParam("dateTo") String customDateTo) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

        try {
            Date dateFrom = null;
            if (StringUtils.isNotBlank(customDateFrom)) {
                dateFrom = DateUtils.parseDate(customDateFrom, "yyyy-MM-dd");
            }

            Date dateTo = null;
            if (StringUtils.isNotBlank(customDateTo)) {
                dateTo = DateUtil.formatDateToEndTime(DateUtils.parseDate(customDateTo, "yyyy-MM-dd"));
            }

            DatagridModel<LiveStreamChannel> datagridModel = RestUtil.getDatagridModel("endDate", "desc", startIndex, pageSize);
            Pair<Date, Date> fromToDate = liveStreamService.getFromToDate(dateRangeType, dateFrom, dateTo);
            dateFrom = fromToDate.getLeft();
            dateTo = fromToDate.getRight();

            liveStreamService.findLiveStreamRecordsInfoForDatagrid(datagridModel, memberId, dateFrom, dateTo);

            long totalRecords = datagridModel.getTotalRecords();
            List<LiveStreamPointBO> liveStreamPointBOS = new ArrayList<>();

            for (LiveStreamChannel channel : datagridModel.getRecords()) {
                List<Pair<Date, Date>> recordList = new ArrayList<>();
                if (DateUtil.truncateTime(channel.getEndDate()).compareTo(DateUtil.truncateTime(channel.getStartDate())) != 0) { //streaming start & end date different
                    if (dateFrom != null && dateTo != null) {
                        if (channel.getEndDate().compareTo(dateTo) > 0) { //if start date between date range but end date out of date range
                            recordList.add(new MutablePair<>(channel.getStartDate(), DateUtil.formatDateToEndTime(channel.getStartDate())));
                        } else if ((channel.getStartDate().compareTo(dateFrom) < 0) && (channel.getEndDate().compareTo(dateFrom) > 0) && (channel.getEndDate().compareTo(dateTo) < 0)) { //if end date between date range but start date out of date range
                            recordList.add(new MutablePair<>(DateUtil.truncateTime(channel.getEndDate()), channel.getEndDate()));
                        } else { //separate record by date
                            recordList.add(new MutablePair<>(DateUtil.truncateTime(channel.getEndDate()), channel.getEndDate()));
                            recordList.add(new MutablePair<>(channel.getStartDate(), DateUtil.formatDateToEndTime(channel.getStartDate())));
                            totalRecords += 1;
                        }
                    } else { //separate record by date
                        recordList.add(new MutablePair<>(DateUtil.truncateTime(channel.getEndDate()), channel.getEndDate()));
                        recordList.add(new MutablePair<>(channel.getStartDate(), DateUtil.formatDateToEndTime(channel.getStartDate())));
                        totalRecords += 1;
                    }
                } else {
                    recordList.add(new MutablePair<>(channel.getStartDate(), channel.getEndDate()));
                }

                LiveStreamPointBO liveStreamPointBO;
                for (Pair<Date, Date> records : recordList) {
                    Date startDate = records.getLeft();
                    Date endDate = records.getRight();
                    long duration = (endDate.getTime() - startDate.getTime()) / 1000;

                    liveStreamPointBO = new LiveStreamPointBO();
                    liveStreamPointBO.setCid(channel.getChannelId());
                    liveStreamPointBO.setAmount(pointService.getPointByChannelByDate(channel.getChannelId(), startDate, endDate));
                    liveStreamPointBO.setStart(startDate);
                    liveStreamPointBO.setEnd(endDate);
                    liveStreamPointBO.setMaxAudience(channel.getTotalView());
                    liveStreamPointBO.setTotalAudience(channel.getTotalView());
//                    liveStreamPointBO.setTrxDatetime(channel.getDatetimeUpdate());
                    liveStreamPointBO.setTrxDatetime(startDate);
                    liveStreamPointBO.setDuration(duration);
                    liveStreamPointBO.setStatus(channel.getStatus());

                    String desc = i18n.getText("totalWhalePoint", locale, liveStreamPointBO.getAmount());
                    liveStreamPointBO.setDesc(desc);

                    liveStreamPointBOS.add(liveStreamPointBO);
                }
            }

            Triple<Long, Integer, BigDecimal> total = liveStreamService.getLiveRecordTotal(memberId, dateFrom, dateTo);

            return ResponseBuilder.newInstance()
                    .setAttribute("data", liveStreamPointBOS)
                    .setAttribute("totalRecords", totalRecords)
                    .setAttribute("totalDuration", total.getLeft())
                    .setAttribute("totalDay", total.getMiddle())
                    .setAttribute("totalPoint", total.getRight())
                    .setAttribute("dateFrom", dateFrom)
                    .setAttribute("dateTo", dateTo)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/guild")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getGuildInfo() {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        try {
            Member member = memberService.getMember(memberId);
            String fileUrl = "";
            String desc = "";
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", locale));
            }
            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberApproved(memberId);
            if (broadcastGuildMember != null && StringUtils.isNotBlank(broadcastGuildMember.getGuildId())) {
                BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(broadcastGuildMember.getGuildId());
                if (broadcastGuild != null) {
                    fileUrl = broadcastGuild.getFileUrlWithParentPath(guildFileUploadConfiguration.getFullGuildParentFileUrl());
                    desc = broadcastGuild.getDescription();
                }
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("fileUrl", fileUrl)
                    .setAttribute("desc", desc)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/fansBadge")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFanBadge() {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
        try {
            Member member = memberService.getMember(memberId);
            String badgeNickname = null;
            String statusDesc = null;
            boolean allowChange = true;

            FansBadge latestBadgeRequested = fansBadgeService.getLatestFansBadgeRequested(memberId, null);
            if (latestBadgeRequested != null) {
                if (latestBadgeRequested.getStatus().equals(Global.Status.REJECTED)) {
                    badgeNickname = member.getFansBadgeName();
                } else { //if rejected, return last active status
                    badgeNickname = latestBadgeRequested.getFansBadgeName();

                    if (latestBadgeRequested.getStatus().equals(Global.Status.PENDING)) { //already submit and in PENDING status
                        allowChange = false;
                        statusDesc = "* " + i18n.getText("statPendingApproval", locale);
                    }
                }
            }

//            //check not allow to change within 10 days after ACTIVE
//            boolean allowChange = fansBadgeService.doCheckAllowChgBadgeAfterActive(memberId, 10);
//            if (allowChange) {
//                //check 365 days maximum change 3 times
//                allowChange = fansBadgeService.doCheckAllowChgBadgeWithinTimePeriod(memberId, 365, 3);
//            }

            MemberFans memberFans = new MemberFans(true);
            String fansRank = MemberFans.getFansRank(memberFans.getFansLevel());
            String previewBadgeUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/" + fansRank + ".png";
            String badgeTextColor = MemberFans.getFansBadgeTextCode(1);

            return ResponseBuilder.newInstance()
                    .setAttribute("previewBadgeUrl", previewBadgeUrl)
                    .setAttribute("badgeTextColor", badgeTextColor)
                    .setAttribute("badgeNickname", badgeNickname)
                    .setAttribute("statusDesc", statusDesc)
                    .setAttribute("allowChange", allowChange)
                    .setAttribute("description", i18n.getText("legionOfFansDesc", locale))
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_FANS_BADGE_LIST_INCLUDE = "fansBadgeList, "
            + "fansBadgeList\\[\\d+\\], "
            + "fansBadgeList\\[\\d+\\].fansBadgeUrl, "
            + "fansBadgeList\\[\\d+\\].fansBadgeTextCode, "
            + "fansBadgeList\\[\\d+\\].fansLevel, "
            + "fansBadgeList\\[\\d+\\].fansBadgeName, "
            + "fansBadgeList\\[\\d+\\].todayFansIntimacy, "
            + "fansBadgeList\\[\\d+\\].todayMaxFansIntimacy, "
            + "fansBadgeList\\[\\d+\\].currentIntimacy, "
            + "fansBadgeList\\[\\d+\\].fansBadgeId, "
            + "fansBadgeList\\[\\d+\\].wearingBadge, "
            + "fansBadgeList\\[\\d+\\].nextLvlIntimacy ";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/fansBadgeList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFansBadgeList(@QueryParam("page") int page, @QueryParam("pageSize") int pageSize) {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            Member member = memberService.getMember(memberId);

            pageSize = checkPage(page, pageSize);
            DatagridModel<MemberFans> memberFansDatagrid = new DefaultDatagridModel<>();
            memberFansDatagrid.setCurrentPage(page);
            memberFansDatagrid.setPageSize(pageSize);

            memberFansService.findMemberFansBadgeForListing(memberFansDatagrid, member.getMemberId(), "");
            List<MemberFans> fansBadgeList = memberFansDatagrid.getRecords();
            List<FansBadgeListBO> fansBadgeListBO = new ArrayList<>();

            if (fansBadgeList != null && fansBadgeList.size() > 0) {
                fansBadgeListBO = fansBadgeList.stream()
                        .map(fans -> getFansList(fans))
                        .collect(Collectors.toList());
            }

            if (fansBadgeList != null) {
                fansBadgeList.clear();
                fansBadgeList = null;
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_FANS_BADGE_LIST_INCLUDE)
                    .setAttribute("fansBadgeList", fansBadgeListBO)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private FansBadgeListBO getFansList(MemberFans fans) {
        FansBadgeListBO fansBadgeListBO = new FansBadgeListBO();
        if (fans.getSendPointInd() != null && fans.getSendPointInd()) {
            fansIntimacyService.doProcessFansIntimacyGain(fans.getFansMemberId(), fans.getMemberId());
        }
        int todayTotalGain = fansIntimacyService.findTodayIntimacyValue(fans, "").intValue();
        int totalValueForLevel = fansIntimacyService.findTotalValueNeedForLevel(fans.getFansLevel() - 1);
        int totalDeductTodayGain = fans.getIntimacyValue() - todayTotalGain;
        boolean isLvlToday = totalDeductTodayGain < totalValueForLevel;
        String fansRank = MemberFans.getFansRank(fans.getFansLevel());
        String fansBadgeUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/" + fansRank + ".png";

        FansIntimacyConfig fansIntimacyConfigForCap = fansIntimacyService.getFansIntimacyConfigByLevel(isLvlToday ? (fans.getFansLevel() == 1 ? 1 : fans.getFansLevel() - 1) : fans.getFansLevel());
        FansIntimacyConfig fansIntimacyConfig = fansIntimacyService.getFansIntimacyConfigByLevel(fans.getFansLevel());

        Member fansOwner = memberService.getMember(fans.getMemberId());
        int nextLevelIntimacy = fansIntimacyService.findTotalValueNeedForLevel(fans.getFansLevel());
        fansBadgeListBO.setFansBadgeId(fans.getMemberId());
        fansBadgeListBO.setFansLevel(fans.getFansLevel().toString());
        fansBadgeListBO.setFansBadgeName(fansOwner.getFansBadgeName());
        fansBadgeListBO.setCurrentIntimacy(fans.getIntimacyValue().toString());
        fansBadgeListBO.setNextLvlIntimacy(fansIntimacyConfig != null ? nextLevelIntimacy + "" : "0");
        fansBadgeListBO.setTodayFansIntimacy(fansIntimacyService.findTodayIntimacyValue(fans, "").intValue() + "");
        fansBadgeListBO.setTodayMaxFansIntimacy(fansIntimacyConfigForCap != null ? fansIntimacyConfig.getMaxPerDay().toString() : "0");
        fansBadgeListBO.setFansBadgeUrl(fansBadgeUrl);
        fansBadgeListBO.setFansBadgeTextCode(MemberFans.getFansBadgeTextCode(fans.getFansLevel()));
        fansBadgeListBO.setWearingBadge(fans.getWearingBadge() == null ? false : fans.getWearingBadge());

        return fansBadgeListBO;
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/submitFansBadge")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response submitFansBadge(@QueryParam("badgeNickname") String fansBadgeName) {
        Locale locale = RestUtil.getUserLocale();
        String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

        try {
            fansBadgeService.doProcessCreateFansBadge(locale, memberId, fansBadgeName);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("submitSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_FANS_BADGE_HISTORY_INCLUDE = "fansBadgeHistory, "
            + "fansBadgeHistory\\[\\d+\\], "
            + "fansBadgeHistory\\[\\d+\\].fansBadgeId, "
            + "fansBadgeHistory\\[\\d+\\].date, "
            + "fansBadgeHistory\\[\\d+\\].description ";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/fansBadgeHistory")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFansBadgeHistory(@QueryParam("page") int page, @QueryParam("pageSize") int pageSize) {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());

            pageSize = checkPage(page, pageSize);
            DatagridModel<FansBadge> fansBadgeDatagrid = new DefaultDatagridModel<>();
            fansBadgeDatagrid.setCurrentPage(page);
            fansBadgeDatagrid.setPageSize(pageSize);

            fansBadgeService.findFansBadgesForListing(fansBadgeDatagrid, member.getMemberCode(), null, null, null);
            List<FansBadge> fansBadgeList = fansBadgeDatagrid.getRecords();

            List<FansBadgeHistoryBO> fansBadgeHistories = new ArrayList<>();
            for (FansBadge fansBadge : fansBadgeList) {
                String desc;
                desc = i18n.getText("badgeNicknamePendingApproval", fansBadge.getFansBadgeName(), locale);
                fansBadgeHistories.add(new FansBadgeHistoryBO(fansBadge.getFansBadgeId(), fansBadge.getStatus(), desc, fansBadge.getCreateDate()));

                if (Global.Status.ACTIVE.equalsIgnoreCase(fansBadge.getStatus()) ||
                        Global.Status.INACTIVE.equalsIgnoreCase(fansBadge.getStatus())) {
                    desc = i18n.getText("badgeNicknameChangedTo", fansBadge.getFansBadgeName(), locale);
                    fansBadgeHistories.add(new FansBadgeHistoryBO(fansBadge.getFansBadgeId(), fansBadge.getStatus(), desc, fansBadge.getActiveDatetime()));
                } else if (Global.Status.REJECTED.equalsIgnoreCase(fansBadge.getStatus())) {
                    desc = i18n.getText("badgeNicknameRejected", fansBadge.getFansBadgeName(), fansBadge.getRemark(), locale);
                    fansBadgeHistories.add(new FansBadgeHistoryBO(fansBadge.getFansBadgeId(), fansBadge.getStatus(), desc, fansBadge.getRejectDatetime()));
                }
            }
            Collections.reverse(fansBadgeHistories);

            fansBadgeList.clear();
            fansBadgeList = null;

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_FANS_BADGE_HISTORY_INCLUDE)
                    .setAttribute("fansBadgeHistory", fansBadgeHistories)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private int checkPage(int page, int pageSize) {
        if (page <= 0) {
            throw new com.compalsolutions.compal.exception.ValidatorException("Invalid Page number");
        }
        return RestUtil.getDefaultPageSizeIfNull(pageSize);
    }

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/fansBadgeInfo")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getFansBadgeInfo() {
        try {
            Locale locale = RestUtil.getUserLocale();
            String webViewUrl = serverUrl + "/pub/fansBadgeInfo.php";

            if (locale.equals(Global.LOCALE_CN)) {
                webViewUrl += "?languageCode=" + Global.Language.CHINESE;
            } else if (locale.equals(Global.LOCALE_MS)) {
                webViewUrl += "?languageCode=" + Global.Language.MALAY;
            } else {
                webViewUrl += "?languageCode=" + Global.Language.ENGLISH;
            }

            return ResponseBuilder.newInstance()
                    .setAttribute("redirectUrl", webViewUrl)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_MEMBER_FANS_INCLUDE = "fansList, "
            + "fansList\\[\\d+\\], "
            + "fansList\\[\\d+\\].fansMemberId, "
            + "fansList\\[\\d+\\].profileName, "
            + "fansList\\[\\d+\\].whaleliveId, "
            + "fansList\\[\\d+\\].intimacyValue, "
            + "fansList\\[\\d+\\].level, "
            + "fansList\\[\\d+\\].levelUrl, "
            + "fansList\\[\\d+\\].profilePicUrl, "
            + "fansList\\[\\d+\\].fansBadgeUrl, "
            + "fansList\\[\\d+\\].fansBadgeName, "
            + "fansList\\[\\d+\\].fansBadgeTextCode, "
            + "fansList\\[\\d+\\].role, "
            + "fansList\\[\\d+\\].rolesOptions, "
            + "fansList\\[\\d+\\].rolesOptions\\[\\d+\\].*, "
            + "fansList\\[\\d+\\].joinDate ";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/memberFansList")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response getMemberFansList(@QueryParam("page") int page, @QueryParam("pageSize") int pageSize, @QueryParam("memberId") String memberId) {
        try {
            String userMemberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            if (StringUtils.isBlank(memberId)) {
                memberId = userMemberId;
            }

            pageSize = checkPage(page, pageSize);
            DatagridModel<MemberFans> memberFansDatagrid = new DefaultDatagridModel<>();
            memberFansDatagrid.setCurrentPage(page);
            memberFansDatagrid.setPageSize(pageSize);

            memberFansService.findMemberFansForListing(memberFansDatagrid, memberId);

            List<MemberFans> resultList = memberFansDatagrid.getRecords();
            List<MemberFans> memberFansList = new ArrayList<>();
            if (resultList != null && resultList.size() > 0) {
                Member member = memberService.getMember(memberId);
                memberFansList = resultList.stream()
                        .map(fans -> getFansInfo(member, fans))
                        .collect(Collectors.toList());
            }

            if (resultList != null) {
                resultList.clear();
                resultList = null;
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_MEMBER_FANS_INCLUDE)
                    .setAttribute("fansList", memberFansList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private MemberFans getFansInfo(Member member, MemberFans fans) {
        Member fansMember = memberService.getMember(fans.getFansMemberId());
        MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(fansMember.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

        fans.setProfileName(fansMember.getMemberDetail().getProfileName());
        fans.setWhaleliveId(fansMember.getMemberDetail().getWhaleliveId());

        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(memberFileUploadConfiguration.getFullMemberFileParentFileUrl());
            fans.setProfilePicUrl(memberFile.getFileUrl());
        }

        Integer level = rankService.getCurrentRank(fansMember.getTotalExp());
        String levelUrl = cryptoProvider.getServerUrl() + "/asset/image/rank/" + Member.getRankInString(level) + ".png";
        fans.setLevel(level);
        fans.setLevelUrl(levelUrl);

        String fansRank = MemberFans.getFansRank(fans.getFansLevel());
        String fansBadgeUrl = cryptoProvider.getServerUrl() + "/asset/image/memberFans/" + fansRank + ".png";
        fans.setFansBadgeUrl(fansBadgeUrl);
        fans.setFansBadgeName(member.getFansBadgeName());
        fans.setFansBadgeTextCode(MemberFans.getFansBadgeTextCode(fans.getFansLevel()));

        List<String> rolesOptions = new ArrayList<>();
        if (fans.getRole().equalsIgnoreCase(MemberFans.ROLE_ADMIN)) {
            rolesOptions.add("Remove Admin");
        } else if (fans.getRole().equalsIgnoreCase(MemberFans.ROLE_SENIOR_ADMIN)) {
            rolesOptions.add("Remove Senior Admin");
        } else {
            rolesOptions.add("Set Admin");
            rolesOptions.add("Set Senior Admin");
        }
        fans.setRolesOptions(rolesOptions);

        return fans;
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("wearBadge")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response setFansBadge(FansBadgeListBO fansBadgeListBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());
            memberFansService.doProcessWearBadge(locale, memberId, fansBadgeListBO.getFansBadgeId());
            return ResponseBuilder.build(Response.Status.OK, i18n.getText("changeBadgeSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("setRoles")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response setRoles(MemberFans memberFans) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            memberFansService.doAddMemberAdmin(locale, memberId, memberFans.getFansMemberId(), memberFans.getRole());

            if (StringUtils.isNotBlank(memberFans.getRole())) {
                return ResponseBuilder.build(Response.Status.OK, i18n.getText("assignRoleSuccessfully", locale));
            } else {
                return ResponseBuilder.build(Response.Status.OK, i18n.getText("removeRoleSuccessfully", locale));
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_VJ_MISSION_INCLUDE = "missionMaxCount, "
            + "missionPeriodMinPoint.*, "
            + "specialMission.*, "
            + "vjMission, "
            + "vjMission.missionId, "
            + "vjMission.memberId, "
            + "vjMission.configId, "
            + "vjMission.status, "
            + "vjMission.remark, "
            + "vjMission.submitDatetime, "
            + "vjMission.startDatetime, "
            + "vjMission.endDatetime, "
            + "vjMission.vjMissionDetailList, "
            + "vjMission.vjMissionDetailList\\[\\d+\\], "
            + "vjMission.vjMissionDetailList\\[\\d+\\].detailId, "
            + "vjMission.vjMissionDetailList\\[\\d+\\].name, "
            + "vjMission.vjMissionDetailList\\[\\d+\\].prize, "
            + "vjMission.vjMissionDetailList\\[\\d+\\].point";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("vjMission")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getVjMission() {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            List<Map<String, Object>> missionPeriodMinPoint = vjMissionService.getMissionPeriodMinPoint();

            VjMission vjMission = vjMissionService.doGetLatestVjMissionByMemberId(memberId);
            if (vjMission != null && vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ENDED)) {
                vjMission = null;
            }
            if (vjMission != null) {
                vjMission.getVjMissionDetailList().forEach(detail -> {
                    if (StringUtils.isNotBlank(detail.getSpecialMissionId()))  {
                        detail.setDetailId(detail.getSpecialMissionId());
                    }
                    detail.setPoint(new BigDecimal(detail.getPoint().intValue())); //reassign to big decimal after parse int
                });
            }

            Map<String, String> specialMission = new HashMap<>();
            for (VjMissionSpecial mission: vjMissionService.getSpecialMissionList()) {
                specialMission.put("id", mission.getSpecialMissionId());
                specialMission.put("value", mission.getName());
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_VJ_MISSION_INCLUDE)
                    .setAttribute("missionMaxCount", VjMissionDetail.MISSION_MAX_COUNT)
                    .setAttribute("vjMission", vjMission)
                    .setAttribute("specialMission", specialMission)
                    .setAttribute("missionPeriodMinPoint", missionPeriodMinPoint)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("vjMission")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response addVjMission(VjMissionBO vjMissionBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            //put as list, in case future add special mission
            List<Map<String, String>> specialMissionList = new ArrayList<>();
            if (StringUtils.isNotBlank(vjMissionBO.getSpecialMissionId())) {
                Map<String, String> specialMission = new HashMap<>();
                specialMission.put("specialMissionId", vjMissionBO.getSpecialMissionId());
                specialMission.put("specialMissionPoint", String.valueOf(vjMissionBO.getSpecialMissionPoint()));
                specialMissionList.add(specialMission);
            }

            vjMissionService.doAddVjMission(locale, memberId, vjMissionBO.getVjMissionDetailList(), vjMissionBO.getMissionPeriodId(), specialMissionList);
            specialMissionList.clear();

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("processSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_VJ_MISSION_HISTORY_INCLUDE = "vjMissionList, "
            + "vjMissionList\\[\\d+\\], "
            + "vjMissionList\\[\\d+\\].missionId, "
            + "vjMissionList\\[\\d+\\].historyName, "
            + "vjMissionList\\[\\d+\\].status, "
            + "vjMissionList\\[\\d+\\].startDatetime, "
            + "vjMissionList\\[\\d+\\].endDatetime, "
            + "vjMissionList\\[\\d+\\].configId, "
            + "vjMissionList\\[\\d+\\].vjMissionDetailList, "
            + "vjMissionList\\[\\d+\\].vjMissionDetailList\\[\\d+\\], "
            + "vjMissionList\\[\\d+\\].vjMissionDetailList\\[\\d+\\].detailId, "
            + "vjMissionList\\[\\d+\\].vjMissionDetailList\\[\\d+\\].name, "
            + "vjMissionList\\[\\d+\\].vjMissionDetailList\\[\\d+\\].prize, "
            + "vjMissionList\\[\\d+\\].vjMissionDetailList\\[\\d+\\].point";

    @GET
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("vjMissionHistory")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getVjMissionHistory(@QueryParam("page") int page, @QueryParam("pageSize") int pageSize) {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            pageSize = checkPage(page, pageSize);
            DatagridModel<VjMission> datagridModel = new DefaultDatagridModel<>();
            datagridModel.setCurrentPage(page);
            datagridModel.setPageSize(pageSize);

            vjMissionService.doGetMissionForListing(datagridModel, memberId, VjMission.STATUS_ENDED);
            List<VjMission> vjMissionList = datagridModel.getRecords();
            vjMissionList.forEach(vjMission -> {
                List<VjMissionRanking> rankingList = vjMissionService.findMissionRankingByMissionId(vjMission.getMissionId());

                if (rankingList.size() > 0) {
                    VjMissionRanking winnerRanking = rankingList.get(0); //show highest ranking as winner
                    String winnerName = winnerRanking.getMember().getMemberDetail().getProfileName();
                    vjMission.setHistoryName("The Winner " + winnerName);
                } else {
                    vjMission.setHistoryName("No Winner");
                }

                vjMission.getVjMissionDetailList().forEach(detail -> {
                    if (StringUtils.isNotBlank(detail.getSpecialMissionId())) {
                        detail.setDetailId(detail.getSpecialMissionId());
                    }
                    detail.setPoint(new BigDecimal(detail.getPoint().intValue())); //reassign to big decimal after parse int
                });
            });

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_VJ_MISSION_HISTORY_INCLUDE)
                    .setAttribute("vjMissionList", vjMissionList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private static final String LIST_VJ_MISSION_RANKING_INCLUDE = "data.*, "
            + "lastRowData.*, "
            + "startDate, "
            + "endDate, "
            + "rankDesc, "
            + "vjMissionDetails, "
            + "vjMissionDetails\\[\\d+\\], "
            + "vjMissionDetails\\[\\d+\\].missionName, "
            + "vjMissionDetails\\[\\d+\\].complete, "
            + "vjMissionDetails\\[\\d+\\].targetMissionPoint, "
            + "vjMissionDetails\\[\\d+\\].currentMissionPoint";

    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("vjMissionRanking")
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response vjMissionRanking(VjMissionDetailsBO vjMissionDetailsBO) {
        try {
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            String vjMemberId = StringUtils.isNotBlank(vjMissionDetailsBO.getMemberId()) ? vjMissionDetailsBO.getMemberId() : memberId;

            List<VjMissionDetailsBO> vjMissionDetailsBOList = null;
            PointTrxBO selfData = null;
            List<PointTrxBO> pointTrxBOList = null;
            Date startDate = null;
            Date endDate = null;

            String rankDescription = i18n.getText("missionRankingDesc", locale);
            VjMission vjMission = vjMissionService.doGetLatestVjMissionByMemberId(vjMemberId);

            if(vjMission != null && vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)) {
                startDate = vjMission.getStartDatetime();
                endDate = vjMission.getEndDatetime();

                int pageSize = checkPage(vjMissionDetailsBO.getPage(), vjMissionDetailsBO.getPageSize());
                DatagridModel<VjMissionRanking> missionRankingDataGrid = new DefaultDatagridModel<>();
                missionRankingDataGrid.setCurrentPage(vjMissionDetailsBO.getPage());
                missionRankingDataGrid.setPageSize(pageSize);

                vjMissionService.findMissionRankingListingByMissionId(missionRankingDataGrid, vjMission.getMissionId());
                List<VjMissionRanking> vjMissionRanking = missionRankingDataGrid.getRecords();

                pointTrxBOList = getPointTrxBOList(vjMissionRanking, vjMissionDetailsBO.getPage(), pageSize);


                selfData = getPointTrxBO(vjMissionService.findPersonalRanking(memberId, vjMission.getMissionId()),0, 0,0, memberId);
                if(selfData != null) {
                    vjMissionDetailsBOList = getMissionDetailBO(vjMission.getVjMissionDetailList(), selfData.getPoint());
                }
                
            } else if(vjMission != null && !vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ENDED) ) {
                pointTrxBOList = new ArrayList<>();
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_VJ_MISSION_RANKING_INCLUDE)
                    .setAttribute("vjMissionDetails", vjMissionDetailsBOList)
                    .setAttribute("data", pointTrxBOList)
                    .setAttribute("lastRowData", selfData)
                    .setAttribute("startDate", startDate)
                    .setAttribute("endDate", endDate)
                    .setAttribute("rankDesc", rankDescription)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private List<VjMissionDetailsBO> getMissionDetailBO(List<VjMissionDetail> vjMissionDetails, BigDecimal point) {
        List<VjMissionDetailsBO> vjMissionDetailsBOList = new ArrayList<>();
        for(VjMissionDetail vjMissionDetail : vjMissionDetails) {

            VjMissionDetailsBO vjMissionDetailsBO = new VjMissionDetailsBO();
            vjMissionDetailsBO.setMissionName(vjMissionDetail.getName());
            vjMissionDetailsBO.setTargetMissionPoint(vjMissionDetail.getPoint().intValue());
            vjMissionDetailsBO.setCurrentMissionPoint(point.intValue());
            vjMissionDetailsBO.setComplete(point.intValue() >= vjMissionDetail.getPoint().intValue());

            vjMissionDetailsBOList.add(vjMissionDetailsBO);
        }
        return vjMissionDetailsBOList;
    }

    private List<PointTrxBO> getPointTrxBOList(List<VjMissionRanking> vjMissionRankingList, int page, int pageNo) {
        List<PointTrxBO> pointTrxBOList = new ArrayList<>();
        int rowNumber = 1;
        for(VjMissionRanking vjMissionRanking : vjMissionRankingList) {
            PointTrxBO pointTrxBO = getPointTrxBO(vjMissionRanking, page, pageNo, rowNumber, "");
            pointTrxBOList.add(pointTrxBO);
            rowNumber++;
        }
        return pointTrxBOList;
    }

    private PointTrxBO getPointTrxBO(VjMissionRanking vjMissionRanking, int page, int pageNo, int rowNumber, String memberId) {
        PointTrxBO pointTrxBO = new PointTrxBO();
        pointTrxBO.setPoint(vjMissionRanking == null ? new BigDecimal(0) : vjMissionRanking.getPoint());
        pointTrxBO.setMemberId(vjMissionRanking == null ? memberId : vjMissionRanking.getMemberId());
        pointTrxBO.setRowNum(page != 1 ? ((page-1) * pageNo) + rowNumber : rowNumber);

        if(vjMissionRanking == null) {
            pointTrxBO.setRowNum(0);
        } else if(rowNumber == 0) {
            pointTrxBO.setRowNum(vjMissionRanking.getRowCount());
        }

        final String parentUrl = cryptoProvider.getFileUploadUrl() + "/file/memberFile";
        Member member = memberService.getMember(vjMissionRanking == null ? memberId : vjMissionRanking.getMemberId());
        if (member != null) {
            MemberDetail md = member.getMemberDetail();
            if (md != null) {
                // get user data
                pointTrxBO.setProfileName(md.getProfileName());
                pointTrxBO.setWhaleLiveId(md.getWhaleliveId());
                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(pointTrxBO.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                // get profile picture
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl);
                    pointTrxBO.setImageUrl(memberFile.getFileUrl());
                }
            }
        }
        return pointTrxBO;
    }
}
