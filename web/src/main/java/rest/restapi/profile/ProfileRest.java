package rest.restapi.profile;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.live.chatroom.service.LiveChatService;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.client.dto.Channel;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.dto.ProfileDto;
import com.compalsolutions.compal.member.service.*;
import com.compalsolutions.compal.member.vo.*;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.wallet.service.DailyCheckInService;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.server.ResourceConfig;
import rest.ResponseBuilder;
import rest.RestUtil;
import rest.vo.*;
import rest.vo.thirdparty.LiveStreamBO;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.*;
import java.util.stream.Collectors;

@Path("profile")
public class ProfileRest extends ResourceConfig {
    private static final Log log = LogFactory.getLog(ProfileRest.class);

    private MemberService memberService;
    private SearchService searchService;
    private FollowerService followerService;
    private LiveStreamService liveStreamService;
    private LiveStreamProvider liveStreamProvider;
    private LiveChatService liveChatService;
    private MemberBlockService memberBlockService;
    private MemberFansService memberFansService;
    private MemberFileUploadConfiguration memberFileUploadConfiguration;
    private DailyCheckInService dailyCheckInService;
    private I18n i18n;

    public ProfileRest() {
        memberService = Application.lookupBean(MemberService.class);
        searchService = Application.lookupBean(SearchService.class);
        followerService = Application.lookupBean(FollowerService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        liveChatService = Application.lookupBean(LiveChatService.class);
        liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        memberBlockService = Application.lookupBean(MemberBlockService.class);
        memberFansService = Application.lookupBean(MemberFansService.class);
        dailyCheckInService = Application.lookupBean(DailyCheckInService.class);
        memberFileUploadConfiguration = Application.lookupBean(MemberFileUploadConfiguration.class);
        i18n = Application.lookupBean(I18n.class);
    }

    private static final String LIST_SEARCH_RESULT_INCLUDE = "searchResult, "
            + "searchResult\\[\\d+\\], "
            + "searchResult\\[\\d+\\].memberId, "
            + "searchResult\\[\\d+\\].username, "
            + "searchResult\\[\\d+\\].whaleliveId, "
            + "searchResult\\[\\d+\\].following, "
            + "searchResult\\[\\d+\\].profilePicUrl, "
            + "totalRecords";

    private static final String LIST_SEARCH_HISTORY_INCLUDE = "searchHistory, "
            + "searchHistory\\[\\d+\\], "
            + "searchHistory\\[\\d+\\].keyword";

    private static final String LIST_FOLLOWING_LIST_INCLUDE = "searchResult, "
            + "searchResult\\[\\d+\\], "
            + "searchResult\\[\\d+\\].memberId, "
            + "searchResult\\[\\d+\\].username, "
            + "searchResult\\[\\d+\\].whaleliveId, "
            + "searchResult\\[\\d+\\].onLive, "
            + "searchResult\\[\\d+\\].following, "
            + "searchResult\\[\\d+\\].profilePicUrl, "
            + "totalRecords";

    private static final String LIST_BLOCK_LIST_INCLUDE = "searchResult, "
            + "searchResult\\[\\d+\\], "
            + "searchResult\\[\\d+\\].blockMemberId, "
            + "searchResult\\[\\d+\\].profileName, "
            + "searchResult\\[\\d+\\].whaleliveId, "
            + "searchResult\\[\\d+\\].profilePicUrl, "
            + "totalRecords";

    @POST
    @Path("/search")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response searchProfile(FollowerBO followerBO) {
        try {
            VoUtil.processProperties(followerBO);

            //search
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            DatagridModel<MemberDetail> memberDetailsPaging = initPagination(followerBO);
            memberService.findSimilarMemberDetailByProfileName(memberDetailsPaging, followerBO.getKeyword(), member.getMemberDetId());

            List<MemberDetail> resultList = memberDetailsPaging.getRecords();
            List<FollowerBO> followerBoList = new ArrayList<>();

            if (resultList != null && resultList.size() > 0) {
                followerBoList = resultList.stream()
                        .map(memberDetail -> getFollowerBoForSearchResult(member.getMemberId(), memberDetail))
                        .collect(Collectors.toList());
            }

            if (resultList != null) {
                resultList.clear();
                resultList = null;
            }
            //save history
            searchService.doCreateSearchHistory(member.getMemberId(), followerBO.getKeyword());

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_SEARCH_RESULT_INCLUDE)
                    .setAttribute("searchResult", followerBoList)
                    .setAttribute("totalRecords", memberDetailsPaging.getTotalRecords())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/searchHistory")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response findSearchHistory() {
        try {
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            List<SearchHistory> searchHistoryList = searchService.findAllSearchHistoryByMemberId(member.getMemberId());

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_SEARCH_HISTORY_INCLUDE)
                    .setAttribute("searchHistory", searchHistoryList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/removeSearchHistory")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response removeSearchHistory(@QueryParam("keyword") String keyword) {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            searchService.doRemoveSearchHistory(member.getMemberId(), keyword);

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("removeSearchHistorySuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/viewProfile")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getProfile(@QueryParam("memberId") String memberId, @QueryParam("channelId") String viewFrmChannelId) {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            if (StringUtils.isBlank(memberId)) { //view own profile
                memberId = member.getMemberId();
            }

            ProfileDto profileDto = memberService.getProfileDetails(locale, member.getMemberId(), memberId, viewFrmChannelId);

            return ResponseBuilder.newInstance()
                    .setAttribute("profilePic", profileDto.getProfilePic())
                    .setAttribute("memberId", profileDto.getMemberId())
                    .setAttribute("profileName", profileDto.getProfileName())
                    .setAttribute("whaleliveId", profileDto.getWhaleliveId())
                    .setAttribute("profileBio", profileDto.getProfileBio())
                    .setAttribute("yunXinAccountId", profileDto.getYunXinAccountId())
                    .setAttribute("followerCount", profileDto.getFollowerCount())
                    .setAttribute("followingCount", profileDto.getFollowingCount())
                    .setAttribute("following", profileDto.isFollowing())
                    .setAttribute("blocking", profileDto.isBlocking())
                    .setAttribute("blockBy", profileDto.isBlockBy())
                    .setAttribute("channel", profileDto.getChannel())
                    .setAttribute("actionOptions", profileDto.getActionOptions())
                    .setAttribute("gender", profileDto.getGender())
                    .setAttribute("countryName", profileDto.getCountryName())
                    .setAttribute("levelUrl", profileDto.getLevelUrl())
                    .setAttribute("receivedPoint", profileDto.getReceivedPoint())
                    .setAttribute("sentPoint", profileDto.getSentPoint())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/follow")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response createFollow(FollowerBO followerBO) {
        try {
            VoUtil.processProperties(followerBO);
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());

            followerService.doProcessFollow(locale, followerBO.getAction(), member.getMemberId(), followerBO.getMemberId());

            if (followerBO.getAction().equals("follow")) {
                return ResponseBuilder.build(Response.Status.OK, i18n.getText("followUserSuccessfully", locale));
            } else {
                return ResponseBuilder.build(Response.Status.OK, i18n.getText("unfollowUserSuccessfully", locale));
            }
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/followingList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getFollowingList(FollowerBO followerBO) {
        try {
            VoUtil.processProperties(followerBO);
            List<FollowerBO> followerBoList = new ArrayList<>();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());

            if (StringUtils.isBlank(followerBO.getMemberId())) {
                followerBO.setMemberId(member.getMemberId());
            }

            DatagridModel<MemberDetail> dataPaging = initPagination(followerBO);
            followerService.getFollowingList(dataPaging, followerBO.getMemberId());

            List<MemberDetail> resultList = dataPaging.getRecords();
            if (resultList != null && resultList.size() > 0) {
                followerBoList = resultList.stream()
                        .map(memberDetail -> getFollowerBoForFollowingList(member.getMemberId(), memberDetail))
                        .collect(Collectors.toList());
            }

            if (resultList != null) {
                resultList.clear();
                resultList = null;
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_FOLLOWING_LIST_INCLUDE)
                    .setAttribute("searchResult", followerBoList)
                    .setAttribute("totalRecords", dataPaging.getTotalRecords())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }


    @POST
    @Path("/followerList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getFollowerList(FollowerBO followerBO) {
        try {
            VoUtil.processProperties(followerBO);
            List<FollowerBO> followerBoList = new ArrayList<>();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());

            if (StringUtils.isBlank(followerBO.getMemberId())) {
                followerBO.setMemberId(member.getMemberId());
            }

            DatagridModel<MemberDetail> dataPaging = initPagination(followerBO);
            followerService.getFollowerList(dataPaging, followerBO.getMemberId());

            List<MemberDetail> resultList = dataPaging.getRecords();
            if (resultList != null && resultList.size() > 0) {
                followerBoList = resultList.stream()
                        .map(memberDetail -> getFollowerBoForFollowerList(member.getMemberId(), memberDetail))
                        .collect(Collectors.toList());
            }

            if (resultList != null) {
                resultList.clear();
                resultList = null;
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_FOLLOWING_LIST_INCLUDE)
                    .setAttribute("searchResult", followerBoList)
                    .setAttribute("totalRecords", dataPaging.getTotalRecords())
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private DatagridModel<MemberDetail> initPagination(FollowerBO followerBO) {

        if (followerBO.getPage() <= 0) {
            throw new ValidatorException("Invalid Page number");
        }
        if (followerBO.getPageSize() <= 0) {
            followerBO.setPageSize(10);
        }

        DatagridModel<MemberDetail> memberDetailDataGrid = new DefaultDatagridModel<>();
        memberDetailDataGrid.setCurrentPage(followerBO.getPage());
        memberDetailDataGrid.setPageSize(followerBO.getPageSize());

        return memberDetailDataGrid;
    }

    private FollowerBO getFollowerBoForSearchResult(String userMemberId, MemberDetail memberDetail) {
        Member member = memberService.findMemberByMemberDetId(memberDetail.getMemberDetId());
        MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
        final String parentUrl = memberFileUploadConfiguration.getFullMemberFileParentFileUrl();

        Follower follower = followerService.getFollower(userMemberId, member.getMemberId());
        boolean isFollowing = false;
        if (follower != null) {
            isFollowing = true;
        }

        FollowerBO followerBO = new FollowerBO();
        followerBO.setMemberId(member.getMemberId());
        followerBO.setUsername(memberDetail.getProfileName());
        followerBO.setWhaleliveId(memberDetail.getWhaleliveId());
        followerBO.setFollowing(isFollowing);

        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(parentUrl);
            followerBO.setProfilePicUrl(memberFile.getFileUrl());
        }

        return followerBO;
    }

    private FollowerBO getFollowerBoForFollowingList(String userMemberId, MemberDetail memberDetail) {
        MemberFile memberFile;

        memberFile = memberService.findMemberFileByMemberIdAndType(memberDetail.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

        final String parentUrl = memberFileUploadConfiguration.getFullMemberFileParentFileUrl();
        boolean blnOnLive = StringUtils.isNotBlank(memberDetail.getLiveChannelId()) ? true : false;
        Follower follower = followerService.getFollower(userMemberId, memberDetail.getMemberId());
        boolean isFollowing = false;
        if (follower != null) {
            isFollowing = true;
        }
        FollowerBO followerBO = new FollowerBO();
        followerBO.setMemberId(memberDetail.getMemberId());
        followerBO.setUsername(memberDetail.getProfileName());
        followerBO.setWhaleliveId(memberDetail.getWhaleliveId());
        followerBO.setOnLive(blnOnLive);
        followerBO.setFollowing(isFollowing);

        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(parentUrl);
            followerBO.setProfilePicUrl(memberFile.getFileUrl());
        }

        return followerBO;
    }


    private FollowerBO getFollowerBoForFollowerList(String userMemberId, MemberDetail memberDetail) {
        MemberFile memberFile;

        memberFile = memberService.findMemberFileByMemberIdAndType(memberDetail.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

        final String parentUrl = memberFileUploadConfiguration.getFullMemberFileParentFileUrl();
        boolean blnOnLive = StringUtils.isNotBlank(memberDetail.getLiveChannelId()) ? true : false;
        Follower follower = followerService.getFollower(userMemberId, memberDetail.getMemberId());
        boolean isFollowing = false;
        if (follower != null) {
            isFollowing = true;
        }
        FollowerBO followerBO = new FollowerBO();
        followerBO.setMemberId(memberDetail.getMemberId());
        followerBO.setUsername(memberDetail.getProfileName());
        followerBO.setWhaleliveId(memberDetail.getWhaleliveId());
        followerBO.setOnLive(blnOnLive);
        followerBO.setFollowing(isFollowing);

        if (memberFile != null) {
            memberFile.setFileUrlWithParentPath(parentUrl);
            followerBO.setProfilePicUrl(memberFile.getFileUrl());
        }

        return followerBO;
    }


    @POST
    @RolesAllowed({AP.ROLE_MEMBER})
    @Path("/channellist")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response getVcloudLiveStreamChannelListByFollowing(LiveStreamBO liveStreamBO) {
        Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
        Locale locale = RestUtil.getUserLocale();
        try {
            DatagridModel<LiveStreamChannel> liveStreamChannelDatagridModel = initPagination(liveStreamBO);
            List<Channel> getFollowingUsersIsOnLiveInList = liveStreamProvider.getFollowingUsersIsOnLiveInList(locale, liveStreamChannelDatagridModel, member.getMemberId());

            return ResponseBuilder.newInstance()
                    .setAttribute("channels", getFollowingUsersIsOnLiveInList)
                    .setAttribute("success", "ok")
                    .createResponse();
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private DatagridModel<LiveStreamChannel> initPagination(LiveStreamBO liveStreamBO) {

        DatagridModel<LiveStreamChannel> liveStreamChannelDatagridModel = new DefaultDatagridModel<>();
        liveStreamChannelDatagridModel.setCurrentPage(liveStreamBO.getPageNo());
        liveStreamChannelDatagridModel.setPageSize(10);

        return liveStreamChannelDatagridModel;
    }

    @GET
    @Path("/dailyCheckIn")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getDailyCheckIn() {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());
            int checkDay = dailyCheckInService.getDailyCheckInDetails(member.getMemberId());
            boolean todayCheckIn = dailyCheckInService.getTodayCheckIn(member.getMemberId());
            List<DailyCheckInBO> dailyCheckInBOList = getDailyCheckInBoList(checkDay);

            return ResponseBuilder.newInstance()
                    .setAttribute("dailyCheckIn", dailyCheckInBOList)
                    .setAttribute("todayCheckIn", todayCheckIn)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private List<DailyCheckInBO> getDailyCheckInBoList(Integer checkDay) {
        List<DailyCheckInBO> dailyCheckInBOList = new ArrayList<>();
        boolean allCheck = checkDay % 7 == 0;
        boolean ind = checkDay > 0 ;
        checkDay = checkDay > 7 ? checkDay % 7 : checkDay;

        for (int i = 1; i <= 7; i++) {
            DailyCheckInBO dailyCheckInBO = new DailyCheckInBO();

            dailyCheckInBO.setDay(i);
            dailyCheckInBO.setCheck(false);
            if (checkDay > (i - 1)) {
                dailyCheckInBO.setCheck(true);
            }
            if (ind && allCheck) {
                dailyCheckInBO.setCheck(true);
            }
            dailyCheckInBOList.add(dailyCheckInBO);

        }
        return dailyCheckInBOList;
    }

    @POST
    @Path("/dailyCheckIn")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response doDailyCheckIn() {
        try {
            Locale locale = RestUtil.getUserLocale();
            Member member = WebUtil.getLoginMember(RestUtil.getLoginInfo());

            dailyCheckInService.doProcessDailyCheckIn(locale, member.getMemberId());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("checkInSuccessfully", locale));

        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @POST
    @Path("/block")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response createMemberBlock(FollowerBO followerBO) {
        try {
            VoUtil.processProperties(followerBO);
            Locale locale = RestUtil.getUserLocale();
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            memberBlockService.doProcessMemberBlock(locale, followerBO.getAction(), memberId, followerBO.getMemberId());

            return ResponseBuilder.build(Response.Status.OK, i18n.getText("processSuccessfully", locale));
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    @GET
    @Path("/blockList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getBlockList(@QueryParam("page") int page, @QueryParam("pageSize") int pageSize) {
        try {
            String memberId = WebUtil.getLoginMemberId(RestUtil.getLoginInfo());

            pageSize = checkPage(page, pageSize);
            DatagridModel<MemberBlock> dataPaging = new DefaultDatagridModel<>();
            dataPaging.setCurrentPage(page);
            dataPaging.setPageSize(pageSize);

            memberBlockService.findMemberBlockForListing(dataPaging, memberId);

            List<MemberBlock> resultList = dataPaging.getRecords();
            List<MemberBlock> memberBlockList = new ArrayList<>();

            if (resultList != null && resultList.size() > 0) {
                memberBlockList = resultList.stream()
                        .map(memberBlock -> {
                            Member member = memberService.getMember(memberBlock.getBlockMemberId());
                            MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);

                            memberBlock.setProfileName(member.getMemberDetail().getProfileName());
                            memberBlock.setWhaleliveId(member.getMemberDetail().getWhaleliveId());

                            if (memberFile != null) {
                                memberFile.setFileUrlWithParentPath(memberFileUploadConfiguration.getFullMemberFileParentFileUrl());
                                memberBlock.setProfilePicUrl(memberFile.getFileUrl());
                            }
                            return memberBlock;
                        })
                        .collect(Collectors.toList());
            }

            if (resultList != null) {
                resultList.clear();
                resultList = null;
            }

            return ResponseBuilder.newInstance()
                    .setIncludeProperties(LIST_BLOCK_LIST_INCLUDE)
                    .setAttribute("searchResult", memberBlockList)
                    .createResponse();
        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

    private int checkPage(int page, int pageSize) {
        if (page <= 0) {
            throw new com.compalsolutions.compal.exception.ValidatorException("Invalid Page number");
        }
        return RestUtil.getDefaultPageSizeIfNull(pageSize);
    }
}
