package rest.restapi;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.BannerFileUploadConfiguration;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.BannerService;
import com.compalsolutions.compal.general.service.LiveCategoryService;
import com.compalsolutions.compal.general.vo.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.glassfish.jersey.server.ResourceConfig;
import org.redisson.client.RedisConnectionException;
import rest.ResponseBuilder;
import rest.RestUtil;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

@Path("banner")
public class BannerRest extends ResourceConfig {
    private BannerService bannerService;
    private LiveCategoryService liveCategoryService;
    private I18n i18n;
    private BannerFileUploadConfiguration bannerFileUploadConfiguration;
    private RedisCacheProvider cacheProvider;

    private static final String LIST_BANNER_INCLUDE = "data, "
            + "data\\[\\d+\\], "
            + "data\\[\\d+\\].bannerId, "
            + "data\\[\\d+\\].title, "
            + "data\\[\\d+\\].body, "
            + "data\\[\\d+\\].status, "
            + "data\\[\\d+\\].fileUrl, "
            + "data\\[\\d+\\].categoryName, "
            + "data\\[\\d+\\].categoryId";

    private static final String LIST_CATEGORY_INCLUDE = "data, "
            + "data\\[\\d+\\], "
            + "data\\[\\d+\\].categoryId, "
            + "data\\[\\d+\\].category, "
            + "data\\[\\d+\\].type, "
            + "data\\[\\d+\\].banners, "
            + "data\\[\\d+\\].banners\\[\\d+\\], "
            + "data\\[\\d+\\].banners\\[\\d+\\].bannerType, "
            + "data\\[\\d+\\].banners\\[\\d+\\].redirectUrl, "
            + "data\\[\\d+\\].banners\\[\\d+\\].timer, "
            + "data\\[\\d+\\].banners\\[\\d+\\].fileUrl";

    public BannerRest() {
        bannerService = Application.lookupBean(BannerService.class);
        liveCategoryService = Application.lookupBean(LiveCategoryService.class);
        bannerFileUploadConfiguration = Application.lookupBean(BannerFileUploadConfiguration.class);
        i18n = Application.lookupBean(I18n.class);
        cacheProvider = Application.lookupBean(RedisCacheProvider.class);
    }
//
//    @Tag(name = "Authorized-User")
//    @GET
//    @Path("/bannerList")
//    @RolesAllowed({AP.ROLE_MEMBER})
//    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    public Response getBanners() {
//        try {
//            BannerFileUploadConfiguration config = Application.lookupBean(BannerFileUploadConfiguration.BEAN_NAME, BannerFileUploadConfiguration.class);
//            String fileUrl = config.getFullBannerParentFileUrl();
//            Locale locale = RestUtil.getUserLocale();
//            Locale systemLocale = Application.lookupBean(Locale.class);
//            boolean isCn = Global.LOCALE_CN.equals(locale);
//            boolean isMs = Global.LOCALE_MS.equals(locale);
//            List<Banner> bannerList = bannerService.findAllActiveBanner();
//
//            bannerList.forEach(banner -> {
//                BannerFile bannerFile = bannerService.findBannerFileByBannerId(banner.getBannerId(),"");
//                LiveCategory liveCategory = liveCategoryService.findCategoryName(banner.getCategoryId());
//                banner.setTitle(isCn ? banner.getTitleCn() : isMs ? banner.getTitleMs() : banner.getTitle());
//                banner.setBody(isCn ? banner.getBodyCn() : isMs ? banner.getBodyMs() : banner.getBody());
//                banner.setCategoryName(isCn ? liveCategory.getCategoryCn() : isMs ? liveCategory.getCategoryMs() : liveCategory.getCategory());
//                if (bannerFile != null) {
//                    banner.setFileUrl(bannerFile.getFileUrlWithParentPath(fileUrl));
//                }
//            });
//
//            return ResponseBuilder.newInstance() //
//                    .setAttribute("data", bannerList)
//                    .setIncludeProperties(LIST_BANNER_INCLUDE)
//                    .createResponse();
//        } catch (Exception ex) {
//            return ResponseBuilder.buildBadRequest(ex.getMessage());
//        }
//    }

    @Tag(name = "Authorized-User")
    @GET
    @Path("/liveCategoryList")
    @RolesAllowed({AP.ROLE_MEMBER})
    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
    public Response getLiveCategory() {
        try {
            Locale locale = RestUtil.getUserLocale();
            boolean isCn = Global.LOCALE_CN.equals(locale);
            boolean isMs = Global.LOCALE_MS.equals(locale);
            List<LiveCategory> liveCategoryList = liveCategoryService.getLiveCategoryList(isCn, isMs);

            return ResponseBuilder.newInstance() //
                    .setAttribute("data", liveCategoryList)
                    .setIncludeProperties(LIST_CATEGORY_INCLUDE)
                    .createResponse();

        } catch (Exception ex) {
            return ResponseBuilder.buildBadRequest(ex.getMessage());
        }
    }

//    @POST
//    @PermitAll
//    @Path("/addAnnouncement")
//    @Produces({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    @Consumes({MediaType.APPLICATION_JSON, MediaType.TEXT_PLAIN, MediaType.TEXT_HTML})
//    public Response addAnnouncement(AnnouncementBO announcementBO) {
//        try {
//            Locale locale = RestUtil.getUserLocale();
//            Announcement announcement = announcementService.doSaveAnnouncement(announcementBO.getTitle(), announcementBO.getTitleCn(), announcementBO.getContent(),announcementBO.getContentCn(),
//                    Global.Status.ACTIVE, new Date(), Global.PublishGroup.PUBLIC_GROUP, Global.AnnouncementType.CONTENT);
//            announcementService.doProcessAnnouncementNotification(announcement);
//
//            return ResponseBuilder.build(Response.Status.OK, i18n.getText("addAnnouncementSuccessfully", locale));
//        } catch (Exception ex) {
//            return ResponseBuilder.buildBadRequest(ex.getMessage());
//        }
    //  }
}
