package rest.filter;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Priority;
import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;

import com.compalsolutions.compal.aop.AppDetailsBaseAdvice;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.log.service.RestLogService;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.web.ServletUtil;

import rest.ResponseBuilder;
import rest.RestUtil;

@Provider
@Priority(Priorities.AUTHENTICATION)
public class AuthenticationFilter implements ContainerRequestFilter, ContainerResponseFilter {
    private static final Log log = LogFactory.getLog(AuthenticationFilter.class);

    @Context
    private ResourceInfo resourceInfo;

    @Context
    private HttpServletRequest servletRequest;

    public static final String AUTHORIZATION_PROPERTY = "Authorization";

    private static final String ACCESS_DENIED = "Not allowed to access this resource!";
    private static final String ACCESS_FORBIDDEN = "Access forbidden!";
    private static final String ACCESS_INVALID_TOKEN = "Token invalid. Please authenticate again!";

    private static final String START_TIME = "START_TIME";

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        long start = System.currentTimeMillis();
        requestContext.setProperty(START_TIME, start);

        Method method = resourceInfo.getResourceMethod();
        log.debug("REST [" + resourceInfo.getResourceClass().getSimpleName() + ", " + method.getName() + "]");

        Locale userLocale = RestUtil.getLocaleFromRequestContext(requestContext);
        RestUtil.setUserLocale(userLocale);

        String resourcePath = requestContext.getUriInfo().getPath();
        String resourceClassName = resourceInfo.getResourceClass().getName();
        String methodName = method.getName();
        String clientIpAddress = ServletUtil.getRemoteAddr(servletRequest);
        String userAgent = requestContext.getHeaderString("user-agent");

        RestUtil.setClientIpAddresss(clientIpAddress);

        if (!method.isAnnotationPresent(PermitAll.class)) {
            // nobody can access
            if (method.isAnnotationPresent(DenyAll.class)) {
                logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, null, null, ACCESS_FORBIDDEN);
                requestContext.abortWith(ResponseBuilder.build(Response.Status.FORBIDDEN, ACCESS_FORBIDDEN));
                return;
            }

            Pair<JwtClaims, JwtToken> pair = verifyToken(requestContext, false, userAgent, resourcePath, resourceClassName, methodName, clientIpAddress);
            if (pair == null) {
                return;
            }

            JwtClaims jwtClaims = pair.getLeft();
            JwtToken jwtToken = pair.getRight();

            // verify user access from provided roles ("admin", "user", "guest")
            if (method.isAnnotationPresent(RolesAllowed.class)) {
                // get annotated roles
                RolesAllowed rolesAnnotation = method.getAnnotation(RolesAllowed.class);
                Set<String> rolesSet = new HashSet<>(Arrays.asList(rolesAnnotation.value()));

                try {
                    List<String> accesses = jwtClaims.getStringListClaimValue(TokenProvider.CLAIM_ACCESSES);

                    // if user don't have the access right
                    if (!accesses.stream().anyMatch(access -> rolesSet.contains(access))) {
                        log.error("User does not have the rights to access this resource!");
                        logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, jwtToken.getToken(), jwtToken.getUserId(),
                                "User does not have the rights to access this resource!");
                        requestContext.abortWith(ResponseBuilder.build(Response.Status.UNAUTHORIZED, ACCESS_DENIED));
                        return;
                    }

                } catch (MalformedClaimException ex) {
                    log.debug(ex.getMessage(), ex.fillInStackTrace());
                    logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, jwtToken.getToken(), jwtToken.getUserId(),
                            ex.getMessage());
                    requestContext.abortWith(ResponseBuilder.build(Response.Status.INTERNAL_SERVER_ERROR, ex.getMessage()));
                    return;
                }
            }

            logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, jwtToken.getToken(), jwtToken.getUserId(), null);

            Application.lookupBean(AppDetailsBaseAdvice.BEAN_NAME, AppDetailsBaseAdvice.class).setCurrentUserId(jwtToken.getUserId());
            RestUtil.setLoginInfo(userLocale, jwtToken.getUser(), jwtToken.getToken(), jwtToken.getId());
        }
        // everybody can access (e.g. user/create or user/authenticate)
        else {
            Pair<JwtClaims, JwtToken> pair = verifyToken(requestContext, true, userAgent, resourcePath, resourceClassName, methodName, clientIpAddress);

            if (pair == null)
                logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, null, null, null);
            else {
                JwtToken jwtToken = pair.getRight();
                logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, jwtToken.getToken(), jwtToken.getUserId(), null);

                Application.lookupBean(AppDetailsBaseAdvice.BEAN_NAME, AppDetailsBaseAdvice.class).setCurrentUserId(jwtToken.getUserId());
                RestUtil.setLoginInfo(userLocale, jwtToken.getUser(), jwtToken.getToken(), jwtToken.getId());
            }
        }
    }

    private Pair<JwtClaims, JwtToken> verifyToken(ContainerRequestContext requestContext, boolean optionalAuthenticate, String userAgent, String resourcePath,
            String resourceClassName, String methodName, String clientIpAddress) {
        TokenProvider tokenProvider = Application.lookupBean(TokenProvider.class);
        Locale locale = Application.lookupBean(Locale.class);
        final MultivaluedMap<String, String> headers = requestContext.getHeaders();

        // get request headers to extract jwt token
        final List<String> authProperty = headers.get(AUTHORIZATION_PROPERTY);

        // block access if no authorization information is provided
        if (CollectionUtil.isEmpty(authProperty)) {
            if (!optionalAuthenticate) {
                log.warn("No token provided!");
                logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, null, null, "No token provided!");
                requestContext.abortWith(ResponseBuilder.build(Response.Status.UNAUTHORIZED, ACCESS_DENIED));
            }
            return null;
        }

        // split 'Bearer <token>'
        String[] ss = StringUtils.split(authProperty.get(0));

        if (ss.length != 2) {
            log.debug("invalid token format " + authProperty.get(0));
            logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, null, null, "invalid token format");
            requestContext.abortWith(ResponseBuilder.build(Response.Status.UNAUTHORIZED, "Invalid Token Format"));
            return null;
        }

        String jwt = ss[1];
        JwtClaims jwtClaims = null;
        JwtToken jwtToken = null;
        try {
            Pair<JwtClaims, JwtToken> pair = tokenProvider.validateJwtToken(locale, jwt);
            jwtClaims = pair.getLeft();
            jwtToken = pair.getRight();
        } catch (InvalidJwtException ex) {
            if (!optionalAuthenticate) {
                log.debug("Invalid token provided!");
                logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, null, null, "Invalid token provided!");
                requestContext.abortWith(ResponseBuilder.build(Response.Status.UNAUTHORIZED, ACCESS_INVALID_TOKEN));
            }
            return null;
        } catch (Exception ex) {
            if (!optionalAuthenticate) {
                log.debug("Invalid token provided!");
                logRestCall(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, null, null, "Invalid token provided!");
                requestContext.abortWith(ResponseBuilder.build(Response.Status.UNAUTHORIZED, ACCESS_INVALID_TOKEN));
            }
            return null;
        }

        return new MutablePair<>(jwtClaims, jwtToken);
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        String timer = "NO-TIMER";
        if (requestContext.getProperty(START_TIME) != null) {
            long start = (long) requestContext.getProperty(START_TIME);
            long end = System.currentTimeMillis();

            timer = (end - start) / 1000.0 + "sec";
        }

        Method method = resourceInfo.getResourceMethod();
        if (method != null)
            log.debug("REST-END [" + resourceInfo.getResourceClass().getSimpleName() + ", " + method.getName() + ", " + timer + "]");
        else {
            String resourcePath = requestContext.getUriInfo().getPath();
            String methodName = requestContext.getMethod();

            String clientIpAddress = ServletUtil.getRemoteAddr(servletRequest);
            String userAgent = requestContext.getHeaderString("user-agent");
            String errorMessage = responseContext.getStatus() + ": " + responseContext.getStatusInfo();

            logRestCall(userAgent, resourcePath, "", methodName, clientIpAddress, null, null, errorMessage);
            log.debug("REST-END [" + resourcePath + " > Method: " + methodName + " > Error " + errorMessage + "]");
        }
    }

    private void logRestCall(String userAgent, String resourcePath, String resourceClassName, String methodName, String clientIpAddress, String token,
            String userId, String errorMessage) {
        RestLogService restLogService = Application.lookupBean(RestLogService.BEAN_NAME, RestLogService.class);

        restLogService.saveRestLog(userAgent, resourcePath, resourceClassName, methodName, clientIpAddress, token, userId, errorMessage);
    }
}
