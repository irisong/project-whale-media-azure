package rest.filter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class ExceptionMapperFilter implements ExceptionMapper<Exception> {
    private static final Log log = LogFactory.getLog(ExceptionMapperFilter.class);

    @Override
    public Response toResponse(Exception exception) {
        log.error(exception.getMessage(), exception.getCause());
        return Response.status(500).build();
    }
}
