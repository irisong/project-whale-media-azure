package rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.Response;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.json.JSONException;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.web.JsonWriter;

public class ResponseBuilder {
    private static final Log log = LogFactory.getLog(ResponseBuilder.class);
    public static final String DEFAULT_INCLUDE_PROPERTIES = Global.JsonInclude.Rest;
    public static final String DEFAULT_EXCLUDE_PROPERTIES = "^\\.auditLog";

    private Map<String, Object> valueMap;
    private Object root;

    private String includeProperties;
    private String excludeProperties;
    private boolean excludeNullProperties;

    private List<String> errors;
    private List<String> messages;

    private ResponseBuilder() {
        valueMap = new HashMap<>();
        errors = new ArrayList<>();
        messages = new ArrayList<>();
    }

    public ResponseBuilder setRoot(Object root) {
        this.root = root;
        return this;
    }

    public ResponseBuilder setIncludeProperties(String includeProperties) {
        this.includeProperties = includeProperties;
        return this;
    }

    public ResponseBuilder setExcludeProperties(String excludeProperties) {
        this.excludeProperties = excludeProperties;
        return this;
    }

    public ResponseBuilder setExcludeNullProperties(boolean excludeNullProperties) {
        this.excludeNullProperties = excludeNullProperties;
        return this;
    }

    public ResponseBuilder setAttribute(String key, Object obj) {
        valueMap.put(key, obj);
        return this;
    }

    public ResponseBuilder addError(String errorMessage) {
        errors.add(errorMessage);
        return this;
    }

    public ResponseBuilder addMessage(String message) {
        messages.add(message);
        return this;
    }

    public boolean hasError() {
        return !errors.isEmpty();
    }

    public boolean hasMessage() {
        return !messages.isEmpty();
    }

    private void doProcess() {
        /* for REST CALL, the errors should set at HttpError.
        valueMap.put("errors", errors);
        if (hasError()) {
            valueMap.put("error", errors.get(0));
        }
        */

        valueMap.put("messages", messages);
        if (hasMessage()) {
            valueMap.put("message", messages.get(0));
        }

        if (StringUtils.isNotBlank(includeProperties)) {
            includeProperties = DEFAULT_INCLUDE_PROPERTIES + ", " + includeProperties;
        }

        /*// always have to exclude properties
        if (StringUtils.isNotBlank(excludeProperties)) {
            excludeProperties = DefaultExcludeProperties + ", " + excludeProperties;
        } else {
            excludeProperties = DefaultExcludeProperties;
        }*/

        if (StringUtils.isNotBlank(excludeProperties) && StringUtils.isBlank(includeProperties)) {
            includeProperties = DEFAULT_INCLUDE_PROPERTIES;
        }
    }

    public Response createResponse() {
        return createResponse(Response.Status.OK);
    }

    private Response createResponse(Response.Status status) {
        if (root instanceof String) {
            return Response.status(status).entity(root).build();
        }

        doProcess();
        JsonWriter jsonWriter = new JsonWriter(root != null ? root : valueMap, includeProperties, excludeProperties, excludeNullProperties);
        try {
            String jsonString = jsonWriter.write();
            return Response.status(status).entity(jsonString).build();
        } catch (JSONException ex) {
            log.debug(ex.getMessage(), ex.fillInStackTrace());
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    public static Response build(Response.Status status, String message) {
        ResponseBuilder builder = new ResponseBuilder();
        builder.addMessage(message);

        return builder.createResponse(status);
    }

    public static Response build(Response.Status status) {
        return build(status, status.toString());
    }

    public static Response build() {
        return build(Response.Status.OK);
    }

    public static Response buildBadRequest(String message) {
        return build(Response.Status.BAD_REQUEST, message);
    }

    public static ResponseBuilder newInstance() {
        return new ResponseBuilder();
    }
}
