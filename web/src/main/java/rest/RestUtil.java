package rest;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ws.rs.container.ContainerRequestContext;

import org.apache.commons.lang3.StringUtils;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.datagrid.DefaultDatagridModel;
import com.compalsolutions.compal.datagrid.DefaultSorter;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.thirdparty.service.ThirdPartyService;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;
import com.compalsolutions.compal.web.BaseRestUtil;
import com.compalsolutions.compal.web.RestLoginInfo;

import rest.vo.WalletInfo;

public class RestUtil extends BaseRestUtil{
    public static int DEFAULT_PAGE_SIZE = 20;
    private static ThreadLocal<LoginInfo> tlLoginInfo = new ThreadLocal<>();
    private static ThreadLocal<String> tlIpAddress = new ThreadLocal<>();
    private static ThreadLocal<Locale> tlLocale = new ThreadLocal<>();

    private static Map<Locale, Map<Integer, WalletInfo>> walletInfoLanguageMap = new HashMap<>();

    public static void setLoginInfo(Locale locale, User user, String token, String tokenId) {
        RestLoginInfo loginInfo = new RestLoginInfo();
        loginInfo.setUserId(user.getUserId());
        loginInfo.setUser(user);
        loginInfo.setLocale(locale);
        loginInfo.setToken(token);
        loginInfo.setTokenId(tokenId);

        if (user instanceof AdminUser) {
            loginInfo.setUserType(new MrmAdminUserType());
        } else if (user instanceof AgentUser) {
            loginInfo.setUserType(new MrmAgentUserType());
        } else if (user instanceof MemberUser) {
            loginInfo.setUserType(new MrmMemberUserType());
        } else
            throw new SystemErrorException("Invalid user type");

        tlLoginInfo.set(loginInfo);
    }

    public static LoginInfo getLoginInfo() {
        return tlLoginInfo.get();
    }

    public static User getLoginUser() {
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo != null)
            return loginInfo.getUser();
        else
            return null;
    }

    public static Locale getUserLocale() {
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo != null)
            return loginInfo.getLocale();
        else if (tlLocale.get() != null)
            return tlLocale.get();
        else
            return Application.lookupBean(Locale.class);
    }

    public static void setClientIpAddresss(String clientIpAddresss) {
        tlIpAddress.set(clientIpAddresss);
    }

    public static String getClientIpAddress() {
        return tlIpAddress.get();
    }

    public static Locale getLocaleFromRequestContext(ContainerRequestContext requestContext) {
        Locale locale = requestContext.getLanguage();
        if (locale != null)
            return locale;

        if (CollectionUtil.isNotEmpty(requestContext.getAcceptableLanguages())) {
            locale = requestContext.getAcceptableLanguages().get(0);
            if (locale != null)
                return locale;
        }

        return Application.lookupBean(Locale.class);
    }

    public static void setUserLocale(Locale userLocale) {
        tlLocale.set(userLocale);
    }

    private static Map<Integer, WalletInfo> getActiveMemberWalletInfoMap(Locale locale) {
        if (!walletInfoLanguageMap.containsKey(locale)) {
            WalletService walletService = Application.lookupBean(WalletService.class);

            List<WalletTypeConfig> walletTypeConfigs = walletService.findMemberWalletTypeConfigs();
            Map<Integer, WalletInfo> walletInfoMap = walletTypeConfigs.stream().collect(Collectors.toMap(walletTypeConfig -> walletTypeConfig.getWalletType(), //
                    walletTypeConfig -> {
                        int walletType = walletTypeConfig.getWalletType();

                        WalletInfo walletInfo = new WalletInfo();
                        walletInfo.setType(walletType);
                        walletInfo.setName(WebUtil.getWalletName(locale, walletType));
                        walletInfo.setCurrency(walletTypeConfig.getCryptoType());
                        return walletInfo;
                    }));

            walletInfoLanguageMap.put(locale, walletInfoMap);
        }

        return walletInfoLanguageMap.get(locale);
    }

    public static WalletInfo getWalletInfo(Locale locale, int walletType) {
        return getActiveMemberWalletInfoMap(locale).get(walletType);
    }

    public static void clearWalletInfos() {
        walletInfoLanguageMap.clear();
    }

    public static Integer getDefaultPageSizeIfNull(Integer pageSize) {
        if (pageSize == null)
            return DEFAULT_PAGE_SIZE;

        if (pageSize == 0)
            return DEFAULT_PAGE_SIZE;

        return pageSize;
    }

    public static <T> DatagridModel<T> getDatagridModel(String sortColumn, String sortDirection, int offset, int pageSize) {
        DatagridModel<T> datagridModel = new DefaultDatagridModel<>();
        processDatagridModel(datagridModel, sortColumn, sortDirection, offset, pageSize);
        return datagridModel;
    }

    public static <T> SqlDatagridModel<T> getSqlDatagridModel(String sortColumn, String sortDirection, int offset, int pageSize) {
        SqlDatagridModel<T> datagridModel = new SqlDatagridModel<>();
        processDatagridModel(datagridModel, sortColumn, sortDirection, offset, pageSize);
        return datagridModel;
    }

    private static <T> void processDatagridModel(DatagridModel<T> datagridModel, String sortColumn, String sortDirection, int offset, int pageSize) {
        if (StringUtils.isNotBlank(sortColumn)) {
            if (StringUtils.isBlank(sortDirection)) {
                sortDirection = "ASC";
            }

            datagridModel.getSorters().add(new DefaultSorter(sortColumn, "", sortDirection));
        }

        datagridModel.setPageSize(pageSize);
        datagridModel.setRecordOffset(offset);
        /*
        if (offset > 0 && pageSize > 0) {
            int currentPage = 1 + (offset / pageSize);
            datagridModel.setCurrentPage(currentPage);
        }
         */
    }

    public static boolean isThirdPartyAccessGranted(String serviceCode) {
        return Application.lookupBean(ThirdPartyService.class).isThirdPartyAccessGranted(serviceCode, RestUtil.getClientIpAddress());
    }
}
