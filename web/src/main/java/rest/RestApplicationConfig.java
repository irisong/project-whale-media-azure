package rest;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import rest.filter.AuthenticationFilter;
import rest.filter.ExceptionMapperFilter;

public class RestApplicationConfig extends ResourceConfig {
    public RestApplicationConfig() {
        // packages( "rest.restapi" );

        // no need. CORS handled by Spring Security Filter
        // register( CORSFilter.class );

        register(AuthenticationFilter.class);
        register(MultiPartFeature.class);
        register(ExceptionMapperFilter.class);
    }
}
