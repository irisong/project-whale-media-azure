package rest.vo;

import com.compalsolutions.compal.vo.annotation.ToTrim;

public class FollowerBO extends PageBO {

    @ToTrim
    private String memberId;

    @ToTrim
    private String followerId;

    @ToTrim
    private String whaleliveId;

    @ToTrim
    private String action;

    @ToTrim
    private String username;

    @ToTrim
    private String keyword;

    @ToTrim
    private boolean following;

    @ToTrim
    private boolean onLive;

    @ToTrim
    private String profilePicUrl;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getFollowerId() {
        return followerId;
    }

    public void setFollowerId(String followerId) {
        this.followerId = followerId;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public boolean isFollowing() {
        return following;
    }

    public void setFollowing(boolean following) {
        this.following = following;
    }

    public boolean isOnLive() {
        return onLive;
    }

    public void setOnLive(boolean onLive) {
        this.onLive = onLive;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }
}
