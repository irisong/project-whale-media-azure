package rest.vo;

public class ChangeTransactionPasswordBO {
    private String oldTransactionPassword;
    private String newTransactionPassword;

    public String getOldTransactionPassword() {
        return oldTransactionPassword;
    }

    public void setOldTransactionPassword(String oldTransactionPassword) {
        this.oldTransactionPassword = oldTransactionPassword;
    }

    public String getNewTransactionPassword() {
        return newTransactionPassword;
    }

    public void setNewTransactionPassword(String newTransactionPassword) {
        this.newTransactionPassword = newTransactionPassword;
    }
}
