package rest.vo;

public class MessageDetail {

    private String languageCode;
    private String title;
    private String content;
    private String redirectUrl;

    public MessageDetail() {
    }

    public MessageDetail(String languageCode, String title, String content, String redirectUrl) {
        this.languageCode = languageCode;
        this.title = title;
        this.content = content;
        this.redirectUrl = redirectUrl;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
}
