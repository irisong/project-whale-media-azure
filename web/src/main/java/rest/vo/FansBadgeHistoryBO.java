package rest.vo;

import java.util.Date;

public class FansBadgeHistoryBO {
    private String fansBadgeId;
    private String status;
    private String description;
    private Date date;

    public FansBadgeHistoryBO() {
    }

    public FansBadgeHistoryBO(String fansBadgeId, String status, String description, Date date) {
        this.fansBadgeId = fansBadgeId;
        this.status = status;
        this.description = description;
        this.date = date;
    }

    public String getFansBadgeId() {
        return fansBadgeId;
    }

    public void setFansBadgeId(String fansBadgeId) {
        this.fansBadgeId = fansBadgeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}
