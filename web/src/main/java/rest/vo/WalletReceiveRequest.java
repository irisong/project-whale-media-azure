package rest.vo;

public class WalletReceiveRequest {
    private WalletInfo wallet;
    private String iconUrl;
    private String depositAddress;

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public String getDepositAddress() {
        return depositAddress;
    }

    public void setDepositAddress(String depositAddress) {
        this.depositAddress = depositAddress;
    }
}
