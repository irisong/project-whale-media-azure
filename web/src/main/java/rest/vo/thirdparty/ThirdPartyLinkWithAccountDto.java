package rest.vo.thirdparty;

import java.math.BigDecimal;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

public class ThirdPartyLinkWithAccountDto {
    @ToTrim
    @ToUpperCase
    private String serviceCode;

    @ToTrim
    private String loginKey;

    @ToTrim
    private String thirdPartyUsername;

    @ToTrim
    @ToUpperCase
    private String cryptoType;
    private BigDecimal amountInUsd;

    private String hash;

    public String getServiceCode() {
        return serviceCode;
    }

    public void setServiceCode(String serviceCode) {
        this.serviceCode = serviceCode;
    }

    public String getLoginKey() {
        return loginKey;
    }

    public void setLoginKey(String loginKey) {
        this.loginKey = loginKey;
    }

    public String getThirdPartyUsername() {
        return thirdPartyUsername;
    }

    public void setThirdPartyUsername(String thirdPartyUsername) {
        this.thirdPartyUsername = thirdPartyUsername;
    }

    public String getCryptoType() {
        return cryptoType;
    }

    public void setCryptoType(String cryptoType) {
        this.cryptoType = cryptoType;
    }

    public BigDecimal getAmountInUsd() {
        return amountInUsd;
    }

    public void setAmountInUsd(BigDecimal amountInUsd) {
        this.amountInUsd = amountInUsd;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
