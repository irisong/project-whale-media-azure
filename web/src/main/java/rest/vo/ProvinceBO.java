package rest.vo;

import java.util.List;

public class ProvinceBO {
    private String provinceCode;

    private String provinceName;

    private List<CityBO> cityList;

    public ProvinceBO(String provinceCode, String provinceName) {
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
    }

    public ProvinceBO(String provinceCode, String provinceName, List<CityBO> cityList) {
        this.provinceCode = provinceCode;
        this.provinceName = provinceName;
        this.cityList = cityList;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public List<CityBO> getCityList() {
        return cityList;
    }

    public void setCityList(List<CityBO> cityList) {
        this.cityList = cityList;
    }
}
