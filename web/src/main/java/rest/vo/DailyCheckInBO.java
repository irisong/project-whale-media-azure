package rest.vo;

import java.math.BigDecimal;

public class DailyCheckInBO {

    private int day;
    private boolean check;


    public DailyCheckInBO() {
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public boolean isCheck() {
        return check;
    }

    public void setCheck(boolean check) {
        this.check = check;
    }
}
