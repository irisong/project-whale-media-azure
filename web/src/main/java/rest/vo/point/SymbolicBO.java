package rest.vo.point;

import java.util.List;

public class SymbolicBO {

    private String symbolicId;

    private String type;

    private String category;

    private int seq;

    private List<SymbolicItemBO> list;

    public String getSymbolicId() {
        return symbolicId;
    }

    public void setSymbolicId(String symbolicId) {
        this.symbolicId = symbolicId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public List<SymbolicItemBO> getList() {
        return list;
    }

    public void setList(List<SymbolicItemBO> list) {
        this.list = list;
    }
}
