package rest.vo.point;

import rest.vo.PageBO;

public class PointTrxBodyBO extends PageBO {
    private String type;
    private String dateRangeType;
    private String ownerId;
    private int isLiveStream;
    private String id;
    private String action;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDateRangeType() {
        return dateRangeType;
    }

    public void setDateRangeType(String dateRangeType) {
        this.dateRangeType = dateRangeType;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }

    public int getIsLiveStream() {
        return isLiveStream;
    }

    public void setIsLiveStream(int isLiveStream) {
        this.isLiveStream = isLiveStream;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
