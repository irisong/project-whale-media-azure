package rest.vo.point;

import java.math.BigDecimal;

public class PointTrxBO {
    private String imageUrl;
    private String whaleLiveId;
    private String profileName;
    private BigDecimal point;
    private String memberId;
    private int rowNum;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public int getRowNum() {
        return rowNum;
    }

    public void setRowNum(int rowNum) {
        this.rowNum = rowNum;
    }
}
