package rest.vo;

import java.util.List;

public class CountryBO {

    public CountryBO() {
    }

    public CountryBO(String countryCode, String countryName) {
        this.countryCode = countryCode;
        this.countryName = countryName;
    }

    public CountryBO(String countryCode, String countryName, List<ProvinceBO> provinceList) {
        this.countryCode = countryCode;
        this.countryName = countryName;
        this.provinceList = provinceList;
    }

    private String countryCode;

    private String countryName;

    private List<ProvinceBO> provinceList;

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public List<ProvinceBO> getProvinceList() {
        return provinceList;
    }

    public void setProvinceList(List<ProvinceBO> provinceList) {
        this.provinceList = provinceList;
    }
}
