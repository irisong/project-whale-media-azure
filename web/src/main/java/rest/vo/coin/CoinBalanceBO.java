package rest.vo.coin;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.wallet.dto.TransferReceiveTrx;
import rest.vo.WalletInfo;

public class CoinBalanceBO {

    private BigDecimal totalBalance;
    private BigDecimal availableBalance;
    private WalletInfo wallet;
    private Integer walletType;
    private BigDecimal priceInFiat;
    private List<TransferReceiveTrx> data = new ArrayList<>();
    private List<CoinPackageBO> packages = new ArrayList<>();
    private long totalRecords;
    private String iconUrl;
    private CoinWebTopup coinWebTopup;

    public void calculateBalanceInFiat() {
        if (priceInFiat != null && availableBalance != null) {
            BigDecimal bdBalanceInFiat = priceInFiat.multiply(availableBalance);
        }
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }


    public BigDecimal getPriceInFiat() {
        return priceInFiat;
    }

    public void setPriceInFiat(BigDecimal priceInFiat) {
        this.priceInFiat = priceInFiat;
    }

    public List<TransferReceiveTrx> getData() {
        return data;
    }

    public void setData(List<TransferReceiveTrx> data) {
        this.data = data;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<CoinPackageBO> getPackages() {
        return packages;
    }

    public void setPackages(List<CoinPackageBO> packages) {
        this.packages = packages;
    }

    public CoinWebTopup getCoinWebTopup() {
        return coinWebTopup;
    }

    public void setCoinWebTopup(CoinWebTopup coinWebTopup) {
        this.coinWebTopup = coinWebTopup;
    }
}
