package rest.vo.coin;

import rest.vo.WalletInfo;

import java.math.BigDecimal;

public class CoinProfitBO {

    private BigDecimal totalBalance;
    private BigDecimal availableBalance;
    private BigDecimal totalWithdraw;
    private WalletInfo wallet;
    private Integer walletType;
    private BigDecimal priceInFiat;
    private long totalRecords;
    private String iconUrl;
    private BigDecimal todayEarned;
    private BigDecimal monthlyEarned;
    private BigDecimal totalEarned;
    private String remark;

    public void calculateBalanceInFiat() {
        if (priceInFiat != null && availableBalance != null) {
            BigDecimal bdBalanceInFiat = priceInFiat.multiply(availableBalance);
        }
    }

    public BigDecimal getTotalBalance() {
        return totalBalance;
    }

    public void setTotalBalance(BigDecimal totalBalance) {
        this.totalBalance = totalBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(BigDecimal availableBalance) {
        this.availableBalance = availableBalance;
    }

    public BigDecimal getTotalWithdraw() {
        return totalWithdraw;
    }

    public void setTotalWithdraw(BigDecimal totalWithdraw) {
        this.totalWithdraw = totalWithdraw;
    }

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public BigDecimal getPriceInFiat() {
        return priceInFiat;
    }

    public void setPriceInFiat(BigDecimal priceInFiat) {
        this.priceInFiat = priceInFiat;
    }

    public long getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(long totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public BigDecimal getTodayEarned() {
        return todayEarned;
    }

    public void setTodayEarned(BigDecimal todayEarned) {
        this.todayEarned = todayEarned;
    }

    public BigDecimal getMonthlyEarned() {
        return monthlyEarned;
    }

    public void setMonthlyEarned(BigDecimal monthlyEarned) {
        this.monthlyEarned = monthlyEarned;
    }

    public BigDecimal getTotalEarned() {
        return totalEarned;
    }

    public void setTotalEarned(BigDecimal totalEarned) {
        this.totalEarned = totalEarned;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
