package rest.vo.coin;

import java.math.BigDecimal;

public class CoinPackageBO {
    private Integer packageAmt;
    private BigDecimal exchangeRate;
    private String currencyCode;
    private String fiatCurrency;
    private BigDecimal price;
    private String productId;

    public Integer getPackageAmt() {
        return packageAmt;
    }

    public void setPackageAmt(Integer packageAmt) {
        this.packageAmt = packageAmt;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getFiatCurrency() {
        return fiatCurrency;
    }

    public void setFiatCurrency(String fiatCurrency) {
        this.fiatCurrency = fiatCurrency;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
