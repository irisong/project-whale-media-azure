package rest.vo;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import rest.vo.point.SymbolicItemBO;

public class FansGiftBO {

    @ToTrim
    private String title;

    @ToTrim
    private String icon;

    @ToTrim
    private String desc;

    @ToTrim
    private String price;

    private SymbolicItemBO item;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public SymbolicItemBO getItem() {
        return item;
    }

    public void setItem(SymbolicItemBO item) {
        this.item = item;
    }
}
