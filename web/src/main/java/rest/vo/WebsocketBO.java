package rest.vo;

import java.math.BigDecimal;

public class WebsocketBO {

    private String channelId;
    private BigDecimal point;
    private long view;

    public WebsocketBO() {
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public BigDecimal getPoint() {
        return point;
    }

    public void setPoint(BigDecimal point) {
        this.point = point;
    }

    public long getView() {
        return view;
    }

    public void setView(long view) {
        this.view = view;
    }
}
