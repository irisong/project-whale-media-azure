package rest.vo;

public class FileBO {
    private String fileUrl;
    private String type;

    public FileBO() {
    }

    public FileBO(String fileUrl, String type) {
        this.fileUrl = fileUrl;
        this.type = type;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
