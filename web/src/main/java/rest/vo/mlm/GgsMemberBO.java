package rest.vo.mlm;

import java.util.List;

public class GgsMemberBO {
    private String headerDesc;
    private String memberId;
    private String gid;
    private String status;
    private String actionType;
    private String actionDesc;
    private String labelDesc;
    private String currentLevel;
    private List<ListInfo> menuList;
    private List<RankingListBO> rankingListBOList;
    private String rankingTitle;
    private int pageSize;
    private int page;
    private int pageCount;
    private String menu;


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getGid() {
        return gid;
    }

    public void setGid(String gid) {
        this.gid = gid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getActionType() {
        return actionType;
    }

    public void setActionType(String actionType) {
        this.actionType = actionType;
    }

    public String getActionDesc() {
        return actionDesc;
    }

    public void setActionDesc(String actionDesc) {
        this.actionDesc = actionDesc;
    }

    public String getHeaderDesc() {
        return headerDesc;
    }

    public void setHeaderDesc(String headerDesc) {
        this.headerDesc = headerDesc;
    }

    public List<ListInfo> getMenuList() {
        return menuList;
    }

    public String getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(String currentLevel) {
        this.currentLevel = currentLevel;
    }

    public void setMenuList(List<ListInfo> menuList) {
        this.menuList = menuList;
    }

    public String getLabelDesc() {
        return labelDesc;
    }

    public void setLabelDesc(String labelDesc) {
        this.labelDesc = labelDesc;
    }

    public List<RankingListBO> getRankingListBOList() {
        return rankingListBOList;
    }

    public void setRankingListBOList(List<RankingListBO> rankingListBOList) {
        this.rankingListBOList = rankingListBOList;
    }

    public String getRankingTitle() {
        return rankingTitle;
    }

    public void setRankingTitle(String rankingTitle) {
        this.rankingTitle = rankingTitle;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
