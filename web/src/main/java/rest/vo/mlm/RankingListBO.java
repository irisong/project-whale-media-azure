package rest.vo.mlm;

import java.math.BigDecimal;

public class RankingListBO {
    private String memberId;
    private String trxDateStr;
    private BigDecimal bonusAmt;
    private int rankSeq;
    private BigDecimal percentage;

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getTrxDateStr() {
        return trxDateStr;
    }

    public void setTrxDateStr(String trxDateStr) {
        this.trxDateStr = trxDateStr;
    }

    public BigDecimal getBonusAmt() {
        return bonusAmt;
    }

    public void setBonusAmt(BigDecimal bonusAmt) {
        this.bonusAmt = bonusAmt;
    }

    public int getRankSeq() {
        return rankSeq;
    }

    public void setRankSeq(int rankSeq) {
        this.rankSeq = rankSeq;
    }

    public BigDecimal getPercentage() {
        return percentage;
    }

    public void setPercentage(BigDecimal percentage) {
        this.percentage = percentage;
    }
}
