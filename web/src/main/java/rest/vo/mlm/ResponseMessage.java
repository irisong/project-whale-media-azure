package rest.vo.mlm;

import java.math.BigDecimal;

public class ResponseMessage {
    private String title;
    private String description;
    private String packages;
    private BigDecimal amountPaid;

    public ResponseMessage() {

    }

    public ResponseMessage(String title, String description, String packages, BigDecimal amountPaid) {
        this.title = title;
        this.description = description;
        this.packages = packages;
        this.amountPaid = amountPaid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPackages() {
        return packages;
    }

    public void setPackages(String packages) {
        this.packages = packages;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }
}
