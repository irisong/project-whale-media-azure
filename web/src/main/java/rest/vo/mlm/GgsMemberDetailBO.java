package rest.vo.mlm;

import java.math.BigDecimal;
import java.util.Date;

public class GgsMemberDetailBO {
    private BigDecimal rewardAmount;
    private String desc;
    private Date trxDate;

    public BigDecimal getRewardAmount() {
        return rewardAmount;
    }

    public void setRewardAmount(BigDecimal rewardAmount) {
        this.rewardAmount = rewardAmount;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Date getTrxDate() {
        return trxDate;
    }

    public void setTrxDate(Date trxDate) {
        this.trxDate = trxDate;
    }
}
