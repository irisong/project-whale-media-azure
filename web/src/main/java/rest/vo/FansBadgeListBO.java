package rest.vo;

import java.util.Date;

public class FansBadgeListBO {
    private String todayFansIntimacy;
    private String todayMaxFansIntimacy;
    private String currentIntimacy;
    private String nextLvlIntimacy;
    private String fansLevel;
    private String fansBadgeUrl;
    private String fansBadgeTextCode;
    private String fansBadgeName;
    private String fansBadgeId;
    private boolean wearingBadge;

    public FansBadgeListBO() {
    }

    public String getTodayFansIntimacy() {
        return todayFansIntimacy;
    }

    public void setTodayFansIntimacy(String todayFansIntimacy) {
        this.todayFansIntimacy = todayFansIntimacy;
    }

    public String getTodayMaxFansIntimacy() {
        return todayMaxFansIntimacy;
    }

    public void setTodayMaxFansIntimacy(String todayMaxFansIntimacy) {
        this.todayMaxFansIntimacy = todayMaxFansIntimacy;
    }

    public String getCurrentIntimacy() {
        return currentIntimacy;
    }

    public void setCurrentIntimacy(String currentIntimacy) {
        this.currentIntimacy = currentIntimacy;
    }

    public String getNextLvlIntimacy() {
        return nextLvlIntimacy;
    }

    public void setNextLvlIntimacy(String nextLvlIntimacy) {
        this.nextLvlIntimacy = nextLvlIntimacy;
    }

    public String getFansLevel() {
        return fansLevel;
    }

    public void setFansLevel(String fansLevel) {
        this.fansLevel = fansLevel;
    }

    public String getFansBadgeUrl() {
        return fansBadgeUrl;
    }

    public void setFansBadgeUrl(String fansBadgeUrl) {
        this.fansBadgeUrl = fansBadgeUrl;
    }

    public String getFansBadgeTextCode() {
        return fansBadgeTextCode;
    }

    public void setFansBadgeTextCode(String fansBadgeTextCode) {
        this.fansBadgeTextCode = fansBadgeTextCode;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }

    public String getFansBadgeId() {
        return fansBadgeId;
    }

    public void setFansBadgeId(String fansBadgeId) {
        this.fansBadgeId = fansBadgeId;
    }

    public boolean isWearingBadge() {
        return wearingBadge;
    }

    public void setWearingBadge(boolean wearingBadge) {
        this.wearingBadge = wearingBadge;
    }
}
