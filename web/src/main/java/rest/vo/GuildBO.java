package rest.vo;

import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import java.util.List;

public class GuildBO extends PageBO {
    private String guildId;
    private String displayId;
    private String name;
    private String president;
    private String description;
    private String fileUrl;
    private Long memberCount;
    private String join;
    private String action;
    private List<BroadcastGuildMember> guildMembers;

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public Long getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Long memberCount) {
        this.memberCount = memberCount;
    }

    public String getJoin() {
        return join;
    }

    public void setJoin(String join) {
        this.join = join;
    }

    public List<BroadcastGuildMember> getGuildMembers() {
        return guildMembers;
    }

    public void setGuildMembers(List<BroadcastGuildMember> guildMembers) {
        this.guildMembers = guildMembers;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public String getPresident() {
        return president;
    }

    public void setPresident(String president) {
        this.president = president;
    }
}
