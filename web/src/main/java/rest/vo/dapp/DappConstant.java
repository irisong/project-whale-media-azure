package rest.vo.dapp;

public class DappConstant {
    public static final String ITEM_OTC = "OTC";
    public static final String ITEM_MASTERNODE = "MASTERNODE";
    public static final String ITEM_EXCHANGE = "EXCHANGE";

    public static final String ITEM_VIP = "VIP";
    public static final String ITEM_EVENT3YEAR = "EVENT3YEAR";
    public static final String ITEM_TRAVEL = "TRAVEL";
    public static final String ITEM_WELLNESS = "WELLNESS";
    public static final String ITEM_CARD = "CARD";
    public static final String ITEM_MERCHANT = "MERCHANT";

    // MALLS
    public static final String ITEM_JDCOM = "JDCOM";
    public static final String ITEM_TAOBAO = "TAOBAO";
    public static final String ITEM_OMNIMALL = "OMNIMALL";
    public static final String ITEM_LAZADA = "LAZADA";

    // PROPERTY
    public static final String ITEM_DECENTRALAND = "DECENTRALAND";
    public static final String ITEM_BLOCKIMMO = "BLOCKIMMO";
    public static final String ITEM_ASTERISMA = "ASTERISMA";
    public static final String ITEM_WIBSON = "WIBSON";
    public static final String ITEM_BTU_HOTEL = "BTU_HOTEL";
    public static final String ITEM_CRAFTY = "CRAFTY";

    // SOCIAL
    public static final String ITEM_SPANK_CHAIN = "SPANK_CHAIN";
    public static final String ITEM_PRIMAS = "PRIMAS";
    public static final String ITEM_CENT = "CENT";
    public static final String ITEM_LOOM_NETWORK = "LOOM_NETWORK";
    public static final String ITEM_CROWD_HOLDING = "CROWD_HOLDING";
    public static final String ITEM_INXIGHT = "INXIGHT";

    // GAME
    public static final String ITEM_BLOCKCHAIN_CUTIES = "BLOCKCHAIN_CUTIES";
    public static final String ITEM_CHIBI_FIGHTERS = "CHIBI_FIGHTERS";
    public static final String ITEM_ETHER_KINGDOMS = "ETHER_KINGDOMS";
    public static final String ITEM_BLOCK_CITIES = "BLOCK_CITIES";
    public static final String ITEM_ETHEREMON = "ETHEREMON";
    public static final String ITEM_HYPER_DRAGONS = "HYPER_DRAGONS";

    // CHARITABLE
    public static final String ITEM_SMARTOFGIVING = "SMARTOFGIVING";
    public static final String ITEM_PINK_COIN = "PINK_COIN";
    public static final String ITEM_CHARI_KINGDOMS = "CHARI_KINGDOMS";
    public static final String ITEM_AUXILIUM = "AUXILIUM";
    public static final String ITEM_EVIMERIA = "EVIMERIA";
    public static final String ITEM_VIPSTAR_COIN = "VIPSTAR_COIN";

    // EXCHANGES
    public static final String ITEM_IDEX = "IDEX";
    public static final String ITEM_KYBER_NETWORK = "KYBER_NETWORK";
    public static final String ITEM_AIR_SWAP = "AIR_SWAP";
}
