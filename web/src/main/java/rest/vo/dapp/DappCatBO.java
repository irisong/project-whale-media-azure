package rest.vo.dapp;

import java.util.ArrayList;
import java.util.List;

public class DappCatBO {
    private String catCode;
    private String catName;
    private String catType;

    private List<DappItemBO> items = new ArrayList<>();

    public DappCatBO(String catCode, String catName, String catType) {
        this.catCode = catCode;
        this.catName = catName;
        this.catType = catType;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatCode() {
        return catCode;
    }

    public void setCatCode(String catCode) {
        this.catCode = catCode;
    }

    public List<DappItemBO> getItems() {
        return items;
    }

    public void setItems(List<DappItemBO> items) {
        this.items = items;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }
}
