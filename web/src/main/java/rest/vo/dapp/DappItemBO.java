package rest.vo.dapp;

public class DappItemBO {
    private String itemCode;
    private String itemName;
    private String itemDesc;
    private String iconUrl;
    private boolean canClickDetail;

    public DappItemBO(String itemCode, String itemName, String itemDesc, String iconUrl, boolean canClickDetail) {
        this.itemCode = itemCode;
        this.itemName = itemName;
        this.itemDesc = itemDesc;
        this.iconUrl = iconUrl;
        this.canClickDetail = canClickDetail;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public boolean isCanClickDetail() {
        return canClickDetail;
    }

    public void setCanClickDetail(boolean canClickDetail) {
        this.canClickDetail = canClickDetail;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }
}
