package rest.vo;

public class LiveConnectBO {
    private String memberId;
    private String profilePicUrl;
    private String profileName;
    private String yunXinAccountId;
    private String rejectDuration;

    public LiveConnectBO() {
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getYunXinAccountId() {
        return yunXinAccountId;
    }

    public void setYunXinAccountId(String yunXinAccountId) {
        this.yunXinAccountId = yunXinAccountId;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getRejectDuration() {
        return rejectDuration;
    }

    public void setRejectDuration(String rejectDuration) {
        this.rejectDuration = rejectDuration;
    }
}
