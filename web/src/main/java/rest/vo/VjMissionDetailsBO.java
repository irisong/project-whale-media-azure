package rest.vo;

public class VjMissionDetailsBO extends PageBO {
    private String missionName;
    private int targetMissionPoint;
    private int currentMissionPoint;
    private Boolean complete;
    private String memberId;

    public String getMissionName() {
        return missionName;
    }

    public void setMissionName(String missionName) {
        this.missionName = missionName;
    }

    public int getTargetMissionPoint() {
        return targetMissionPoint;
    }

    public void setTargetMissionPoint(int targetMissionPoint) {
        this.targetMissionPoint = targetMissionPoint;
    }

    public int getCurrentMissionPoint() {
        return currentMissionPoint;
    }

    public void setCurrentMissionPoint(int currentMissionPoint) {
        this.currentMissionPoint = currentMissionPoint;
    }

    public Boolean getComplete() {
        return complete;
    }

    public void setComplete(Boolean complete) {
        this.complete = complete;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }
}
