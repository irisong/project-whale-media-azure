package rest.vo;

import java.math.BigDecimal;

public class WalletToBo {
    private WalletInfo wallet;
    private Integer walletType;
    private BigDecimal exchangeRate;
    private boolean isCrypto;
    private String iconUrl;

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public BigDecimal getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(BigDecimal exchangeRate) {
        this.exchangeRate = exchangeRate;
    }

    public boolean isIsCrypto() {
        return isCrypto;
    }

    public void setIsCrypto(boolean isCrypto) {
        this.isCrypto = isCrypto;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
