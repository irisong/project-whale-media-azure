package rest.vo;

import com.compalsolutions.compal.event.vo.VjMissionDetail;

import java.math.BigDecimal;
import java.util.List;

public class VjMissionBO {
    private List<VjMissionDetail> vjMissionDetailList;
    private String missionPeriodId;
    private String specialMissionId;
    private BigDecimal specialMissionPoint;

    public VjMissionBO() {
    }

    public List<VjMissionDetail> getVjMissionDetailList() {
        return vjMissionDetailList;
    }

    public void setVjMissionDetailList(List<VjMissionDetail> vjMissionDetailList) {
        this.vjMissionDetailList = vjMissionDetailList;
    }

    public String getMissionPeriodId() {
        return missionPeriodId;
    }

    public void setMissionPeriodId(String missionPeriodId) {
        this.missionPeriodId = missionPeriodId;
    }

    public String getSpecialMissionId() {
        return specialMissionId;
    }

    public void setSpecialMissionId(String specialMissionId) {
        this.specialMissionId = specialMissionId;
    }

    public BigDecimal getSpecialMissionPoint() {
        return specialMissionPoint;
    }

    public void setSpecialMissionPoint(BigDecimal specialMissionPoint) {
        this.specialMissionPoint = specialMissionPoint;
    }
}
