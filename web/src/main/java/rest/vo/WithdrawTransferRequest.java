package rest.vo;

import java.math.BigDecimal;

public class WithdrawTransferRequest {
    private WalletInfo wallet;

    private BigDecimal minimumAmount;
    private BigDecimal fee;

    private BigDecimal dailyLimit;
    private BigDecimal usedLimit;

    private BigDecimal rate;

    private BigDecimal balance;

    private String dailyLimitCurrency;

    private String iconUrl;

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public BigDecimal getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(BigDecimal minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public BigDecimal getFee() {
        return fee;
    }

    public void setFee(BigDecimal fee) {
        this.fee = fee;
    }

    public BigDecimal getDailyLimit() {
        return dailyLimit;
    }

    public void setDailyLimit(BigDecimal dailyLimit) {
        this.dailyLimit = dailyLimit;
    }

    public BigDecimal getUsedLimit() {
        return usedLimit;
    }

    public void setUsedLimit(BigDecimal usedLimit) {
        this.usedLimit = usedLimit;
    }

    public String getDailyLimitCurrency() {
        return dailyLimitCurrency;
    }

    public void setDailyLimitCurrency(String dailyLimitCurrency) {
        this.dailyLimitCurrency = dailyLimitCurrency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }
}
