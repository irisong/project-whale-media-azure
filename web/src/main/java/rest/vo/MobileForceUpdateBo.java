package rest.vo;

public class MobileForceUpdateBo {
    private String platform;
    private String favor;
    private String versionName;

    /*
    version code must be integer
     */
    private String versionCode;
    private Boolean forceUpdate;
    private String updateUrl;

    public MobileForceUpdateBo() {
    }

    public MobileForceUpdateBo(String platform, String favor, String versionName, String versionCode, Boolean forceUpdate, String updateUrl) {
        this.platform = platform;
        this.favor = favor;
        this.versionName = versionName;
        this.versionCode = versionCode;
        this.forceUpdate = forceUpdate;
        this.updateUrl = updateUrl;
    }

    public String getFavor() {
        return favor;
    }

    public void setFavor(String favor) {
        this.favor = favor;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersionName() {
        return versionName;
    }

    public void setVersionName(String versionName) {
        this.versionName = versionName;
    }

    public String getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(String versionCode) {
        this.versionCode = versionCode;
    }

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public String getUpdateUrl() {
        return updateUrl;
    }

    public void setUpdateUrl(String updateUrl) {
        this.updateUrl = updateUrl;
    }
}
