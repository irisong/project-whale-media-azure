package rest.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class SimpleCryptoInfo {
    private BigDecimal priceInFiat;
    private WalletInfo wallet;
    private String iconUrl;
    private BigDecimal balance;
    private List<WalletToBo> walletTos = new ArrayList<>();

    public BigDecimal getPriceInFiat() {
        return priceInFiat;
    }

    public void setPriceInFiat(BigDecimal priceInFiat) {
        this.priceInFiat = priceInFiat;
    }

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public List<WalletToBo> getWalletTos() {
        return walletTos;
    }

    public void setWalletTos(List<WalletToBo> walletTos) {
        this.walletTos = walletTos;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }
}
