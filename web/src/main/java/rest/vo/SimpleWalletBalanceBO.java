package rest.vo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.compalsolutions.compal.wallet.dto.TransferReceiveTrx;

public class SimpleWalletBalanceBO {
    private BigDecimal balance;

    private BigDecimal totalIncome;
    private BigDecimal yesterdayEarn;
    private WalletInfo wallet;
    private Integer walletType;
    private BigDecimal realtimePrice;

    private List<TransferReceiveTrx> data = new ArrayList<>();

    public BigDecimal getTotalIncome() {
        return totalIncome;
    }

    public void setTotalIncome(BigDecimal totalIncome) {
        this.totalIncome = totalIncome;
    }

    public BigDecimal getYesterdayEarn() {
        return yesterdayEarn;
    }

    public void setYesterdayEarn(BigDecimal yesterdayEarn) {
        this.yesterdayEarn = yesterdayEarn;
    }

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getRealtimePrice() {
        return realtimePrice;
    }

    public void setRealtimePrice(BigDecimal realtimePrice) {
        this.realtimePrice = realtimePrice;
    }

    public List<TransferReceiveTrx> getData() {
        return data;
    }

    public void setData(List<TransferReceiveTrx> data) {
        this.data = data;
    }
}
