package rest.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class MessageBO extends PageBO{

    private String messageId;
    private String memberId;
    private Date publishDate;
    private String messageType;
    private String directToMemberId;
    private String imageUrl;
    private List<MessageDetail> messageDetails;

    public MessageBO() {
    }

    public MessageBO(String messageId, String memberId, Date publishDate, String messageType, String directToMemberId, String imageUrl) {
        this.messageId = messageId;
        this.memberId = memberId;
        this.publishDate = publishDate;
        this.messageType = messageType;
        this.directToMemberId = directToMemberId;
        this.imageUrl = imageUrl;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getDirectToMemberId() {
        return directToMemberId;
    }

    public void setDirectToMemberId(String directToMemberId) {
        this.directToMemberId = directToMemberId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<MessageDetail> getMessageDetails() {
        return messageDetails;
    }

    public void setMessageDetails(List<MessageDetail> messageDetails) {
        this.messageDetails = messageDetails;
    }

    public void addMessageDetails(MessageDetail messageDetail) {
        if (this.messageDetails == null) {
            this.messageDetails =  new ArrayList<>();
        }

        this.messageDetails.add(messageDetail);
    }
}
