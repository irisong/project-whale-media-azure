package rest.vo;

import java.math.BigDecimal;
import java.util.Date;

public class CurrencyExchangeBO {

    private String currencyCodeFrom;

    private String currencyCodeTo;

    private String site;

    private Date exchangeDatetime;

    private Double rate;

    private boolean selected;

    private BigDecimal minimumAmount;

    private BigDecimal maximumAmount;

    public String getCurrencyCodeFrom() {
        return currencyCodeFrom;
    }

    public void setCurrencyCodeFrom(String currencyCodeFrom) {
        this.currencyCodeFrom = currencyCodeFrom;
    }

    public String getCurrencyCodeTo() {
        return currencyCodeTo;
    }

    public void setCurrencyCodeTo(String currencyCodeTo) {
        this.currencyCodeTo = currencyCodeTo;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Date getExchangeDatetime() {
        return exchangeDatetime;
    }

    public void setExchangeDatetime(Date exchangeDatetime) {
        this.exchangeDatetime = exchangeDatetime;
    }

    public Double getRate() {
        return rate;
    }

    public void setRate(Double rate) {
        this.rate = rate;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public BigDecimal getMinimumAmount() {
        return minimumAmount;
    }

    public void setMinimumAmount(BigDecimal minimumAmount) {
        this.minimumAmount = minimumAmount;
    }

    public BigDecimal getMaximumAmount() {
        return maximumAmount;
    }

    public void setMaximumAmount(BigDecimal maximumAmount) {
        this.maximumAmount = maximumAmount;
    }
}
