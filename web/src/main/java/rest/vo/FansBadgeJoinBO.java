package rest.vo;

public class FansBadgeJoinBO {
    private String title;
    private String description;
    private String fansLevel;
    private String fansBadgeUrl;
    private String fansBadgeTextCode;
    private String fansBadgeName;


    public FansBadgeJoinBO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFansLevel() {
        return fansLevel;
    }

    public void setFansLevel(String fansLevel) {
        this.fansLevel = fansLevel;
    }

    public String getFansBadgeUrl() {
        return fansBadgeUrl;
    }

    public void setFansBadgeUrl(String fansBadgeUrl) {
        this.fansBadgeUrl = fansBadgeUrl;
    }

    public String getFansBadgeTextCode() {
        return fansBadgeTextCode;
    }

    public void setFansBadgeTextCode(String fansBadgeTextCode) {
        this.fansBadgeTextCode = fansBadgeTextCode;
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }
}
