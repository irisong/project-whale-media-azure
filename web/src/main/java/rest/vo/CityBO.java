package rest.vo;

import java.util.List;

public class CityBO {
    private String cityCode;

    private String cityName;

    private List<AreaBO> areaList;

    public CityBO(String cityCode, String cityName) {
        this.cityCode = cityCode;
        this.cityName = cityName;
    }

    public CityBO(String cityCode, String cityName, List<AreaBO> areaList) {
        this.cityCode = cityCode;
        this.cityName = cityName;
        this.areaList = areaList;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public List<AreaBO> getAreaList() {
        return areaList;
    }

    public void setAreaList(List<AreaBO> areaList) {
        this.areaList = areaList;
    }
}
