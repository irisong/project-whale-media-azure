package rest.vo;

import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import java.util.Date;

public class ProfileVo {
    @ToTrim
    private String username;

    @ToTrim
    private String phoneno;

    @ToTrim
    private String email;

    @ToTrim
    @ToUpperCase
    private String gender;

    @ToTrim
    @ToUpperCase
    private String country;

    @ToTrim
    @ToUpperCase
    private String birthday;

    @ToTrim
    private String bio;

    @ToTrim
    @ToUpperCase
    private String fullName;

    @ToTrim
    @ToUpperCase
    private String identity_no;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIdentity_no() {
        return identity_no;
    }

    public void setIdentity_no(String identity_no) {
        this.identity_no = identity_no;
    }
}
