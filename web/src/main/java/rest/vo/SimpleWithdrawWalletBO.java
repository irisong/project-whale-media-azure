package rest.vo;

import java.math.BigDecimal;

public class SimpleWithdrawWalletBO {
    private WalletInfo wallet;
    private Integer walletType;

    private BigDecimal priceInFiat;

    public WalletInfo getWallet() {
        return wallet;
    }

    public void setWallet(WalletInfo wallet) {
        this.wallet = wallet;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public BigDecimal getPriceInFiat() {
        return priceInFiat;
    }

    public void setPriceInFiat(BigDecimal priceInFiat) {
        this.priceInFiat = priceInFiat;
    }
}
