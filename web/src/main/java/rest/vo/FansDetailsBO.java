package rest.vo;

import java.util.List;

public class FansDetailsBO {
    private String title;
    private String fansBadgeName;
    private Integer totalFans;
    private String description;
    private boolean isFans;
    private String infoWebView;
    private List<FansPrivilegesBO> fansPrivilegesBOList;
    private FansGiftBO fansGiftBO;
    private String profileUrl;
    private String profileName;
    private String todayPointSpent;
    private String targetPointSpent;

    public FansDetailsBO() {
    }

    public String getFansBadgeName() {
        return fansBadgeName;
    }

    public void setFansBadgeName(String fansBadgeName) {
        this.fansBadgeName = fansBadgeName;
    }

    public Integer getTotalFans() {
        return totalFans;
    }

    public void setTotalFans(Integer totalFans) {
        this.totalFans = totalFans;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFans() {
        return isFans;
    }

    public void setFans(boolean fans) {
        isFans = fans;
    }

    public List<FansPrivilegesBO> getFansPrivilegesBOList() {
        return fansPrivilegesBOList;
    }

    public void setFansPrivilegesBOList(List<FansPrivilegesBO> fansPrivilegesBOList) {
        this.fansPrivilegesBOList = fansPrivilegesBOList;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getInfoWebView() {
        return infoWebView;
    }

    public void setInfoWebView(String infoWebView) {
        this.infoWebView = infoWebView;
    }

    public FansGiftBO getFansGiftBO() {
        return fansGiftBO;
    }

    public void setFansGiftBO(FansGiftBO fansGiftBO) {
        this.fansGiftBO = fansGiftBO;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getTodayPointSpent() {
        return todayPointSpent;
    }

    public void setTodayPointSpent(String todayPointSpent) {
        this.todayPointSpent = todayPointSpent;
    }

    public String getTargetPointSpent() {
        return targetPointSpent;
    }

    public void setTargetPointSpent(String targetPointSpent) {
        this.targetPointSpent = targetPointSpent;
    }
}
