package rest.vo;

import com.compalsolutions.compal.vo.annotation.ToLowerCase;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

public class SimpleSalesOrder {
    @ToLowerCase
    @ToTrim
    private String productId;
    private Integer quantity;

    @ToUpperCase
    @ToTrim
    private String contactName;

    @ToUpperCase
    @ToTrim
    private String contactNo;

    @ToUpperCase
    @ToTrim
    private String addressDetail;

    @ToUpperCase
    @ToTrim
    private String town;

    @ToUpperCase
    @ToTrim
    private String area;

    @ToUpperCase
    @ToTrim
    private String city;

    @ToUpperCase
    @ToTrim
    private String province;

    @ToUpperCase
    @ToTrim
    private String provinceCode;

    @ToUpperCase
    @ToTrim
    private String postcode;

    @ToUpperCase
    @ToTrim
    private String countryCode;

    private String walletType;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNo() {
        return contactNo;
    }

    public void setContactNo(String contactNo) {
        this.contactNo = contactNo;
    }

    public String getAddressDetail() {
        return addressDetail;
    }

    public void setAddressDetail(String addressDetail) {
        this.addressDetail = addressDetail;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getWalletType() {
        return walletType;
    }

    public void setWalletType(String walletType) {
        this.walletType = walletType;
    }

    public String getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(String provinceCode) {
        this.provinceCode = provinceCode;
    }
}
