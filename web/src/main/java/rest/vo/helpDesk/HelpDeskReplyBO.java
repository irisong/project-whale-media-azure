package rest.vo.helpDesk;

import java.util.Date;
import java.util.List;

public class HelpDeskReplyBO {
    private String replyId;
    private int replySeq;
    private String username;
    private Date replyDate;
    private String message;
//    private String readStatus;
    private Boolean fromAdmin;
    private List<HelpDeskReplyFileBO> attachedFile;

//    public HelpDeskReplyBO(String replyId, int replySeq, String username, Date replyDate, String message,
//                           String readStatus, List<HelpDeskReplyFileBO> attachedFile, Boolean fromAdmin) {
    public HelpDeskReplyBO(String replyId, int replySeq, String username, Date replyDate, String message, List<HelpDeskReplyFileBO> attachedFile, Boolean fromAdmin) {
        this.replyId = replyId;
        this.replySeq = replySeq;
        this.username = username;
        this.replyDate = replyDate;
        this.message = message;
//        this.readStatus = readStatus;
        this.attachedFile = attachedFile;
        this.fromAdmin = fromAdmin;
    }

    public HelpDeskReplyBO() {
    }

    public String getReplyId() {
        return replyId;
    }

    public void setReplyId(String replyId) {
        this.replyId = replyId;
    }

    public int getReplySeq() {
        return replySeq;
    }

    public void setReplySeq(int replySeq) {
        this.replySeq = replySeq;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getReplyDate() {
        return replyDate;
    }

    public void setReplyDate(Date replyDate) {
        this.replyDate = replyDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

//    public String getReadStatus() {
//        return readStatus;
//    }
//
//    public void setReadStatus(String readStatus) {
//        this.readStatus = readStatus;
//    }

    public List<HelpDeskReplyFileBO> getAttachedFile() {
        return attachedFile;
    }

    public void setAttachedFile(List<HelpDeskReplyFileBO> attachedFile) {
        this.attachedFile = attachedFile;
    }

    public Boolean getFromAdmin() {
        return fromAdmin;
    }

    public void setFromAdmin(Boolean fromAdmin) {
        this.fromAdmin = fromAdmin;
    }
}
