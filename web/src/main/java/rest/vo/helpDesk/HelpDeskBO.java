package rest.vo.helpDesk;

import java.util.Date;
import java.util.List;

public class HelpDeskBO {
    private String ticketId;
    private String ticketNo;
    private Date createTicketDate;
    private String ticketType;
    private String subject;
//    private Boolean notifyUser;
    private String status;
    private List<HelpDeskReplyBO> replyMessages;
    private int maxUploadAllow;

    public HelpDeskBO() {
    }

//    public HelpDeskBO(String ticketId, String ticketNo, Date createTicketDate, String ticketType, String subject,
//                      Boolean notifyUser, String status, List<HelpDeskReplyBO> replyMessages, int maxUploadAllow) {
    public HelpDeskBO(String ticketId, String ticketNo, Date createTicketDate, String ticketType, String subject,
                String status, List<HelpDeskReplyBO> replyMessages, int maxUploadAllow) {
        this.ticketId = ticketId;
        this.ticketNo = ticketNo;
        this.createTicketDate = createTicketDate;
        this.ticketType = ticketType;
        this.subject = subject;
//        this.notifyUser = notifyUser;
        this.status = status;
        this.replyMessages = replyMessages;
        this.maxUploadAllow = maxUploadAllow;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketNo() {
        return ticketNo;
    }

    public void setTicketNo(String ticketNo) {
        this.ticketNo = ticketNo;
    }

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

//    public Boolean getNotifyUser() {
//        return notifyUser;
//    }
//
//    public void setNotifyUser(Boolean notifyUser) {
//        this.notifyUser = notifyUser;
//    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTicketDate() {
        return createTicketDate;
    }

    public void setCreateTicketDate(Date createTicketDate) {
        this.createTicketDate = createTicketDate;
    }

    public List<HelpDeskReplyBO> getReplyMessages() {
        return replyMessages;
    }

    public void setReplyMessages(List<HelpDeskReplyBO> replyMessages) {
        this.replyMessages = replyMessages;
    }

    public int getMaxUploadAllow() {
        return maxUploadAllow;
    }

    public void setMaxUploadAllow(int maxUploadAllow) {
        this.maxUploadAllow = maxUploadAllow;
    }
}
