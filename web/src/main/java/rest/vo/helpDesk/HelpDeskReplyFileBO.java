package rest.vo.helpDesk;

public class HelpDeskReplyFileBO {
    String fileUrl;
    String fileName;

    public HelpDeskReplyFileBO(){}

    public HelpDeskReplyFileBO(String fileUrl, String fileName){
        this.fileUrl = fileUrl;
        this.fileName = fileName;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
