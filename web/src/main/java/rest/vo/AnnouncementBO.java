package rest.vo;

import java.util.Date;

public class AnnouncementBO {
    private String title;
    private String content;
    private Date publishDate;
    private String titleCn;
    private String contentCn;

    public AnnouncementBO() {
    }

    public AnnouncementBO(String title, String content, Date publishDate) {
        this.title = title;
        this.content = content;
        this.publishDate = publishDate;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public String getTitleCn() {
        return titleCn;
    }

    public void setTitleCn(String titleCn) {
        this.titleCn = titleCn;
    }

    public String getContentCn() {
        return contentCn;
    }

    public void setContentCn(String contentCn) {
        this.contentCn = contentCn;
    }
}
