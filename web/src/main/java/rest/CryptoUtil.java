package rest;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletBalance;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

import rest.vo.coin.CoinBalanceBO;

public class CryptoUtil {

    public static CoinBalanceBO getWalletBalanceBO(Locale locale, String currencyCode, String ownerId, String ownerType) {
        WalletService walletService = Application.lookupBean(WalletService.class);
        I18n i18n = Application.lookupBean(I18n.class);

        currencyCode = StringUtils.upperCase(currencyCode);

        if (StringUtils.isBlank(currencyCode)) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }

        WalletTypeConfig walletTypeConfig = walletService.findActiveWalletTypeConfigByOwnerTypeAndCryptoType(ownerType, currencyCode);
        if (walletTypeConfig == null) {
            throw new ValidatorException(i18n.getText("invalidCurrencyCode", locale));
        }

        WalletBalance walletBalance = walletService.doCreateWalletBalanceIfNotExists(ownerId, ownerType, walletTypeConfig.getWalletType());

        if (walletBalance == null) {
            walletBalance = new WalletBalance(true);
        }

        CoinBalanceBO bo = new CoinBalanceBO();
        bo.setTotalBalance(walletBalance.getTotalBalance());
        bo.setAvailableBalance(walletBalance.getAvailableBalance());
        bo.setWalletType(walletTypeConfig.getWalletType());
        bo.setWallet(RestUtil.getWalletInfo(locale, walletBalance.getWalletType()));

        return bo;
    }
}
