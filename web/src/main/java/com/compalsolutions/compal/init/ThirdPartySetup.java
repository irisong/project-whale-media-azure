package com.compalsolutions.compal.init;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.thirdparty.service.ThirdPartyService;
import com.compalsolutions.compal.thirdparty.vo.ThirdParty;

public class ThirdPartySetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(ThirdPartySetup.class);

    private ThirdPartyService thirdPartyService;

    @Override
    public boolean execute() {
        long start = System.currentTimeMillis();

        thirdPartyService = Application.lookupBean(ThirdPartyService.class);

        createThirdPartyIfNotExists("SHOP", "SHOP", "localhost", "http://localhost", "PVNv5Sgw9pdC2W2L");

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end ThirdPartySetup");

        return true;
    }

    private void createThirdPartyIfNotExists(String serviceCode, String serviceName, String serviceIp, String serviceUrl, String hashSeed) {
        ThirdParty thirdParty = thirdPartyService.findActiveThirdPartyByServiceCode(serviceCode);

        if (thirdParty != null)
            return;

        thirdParty = new ThirdParty(true);
        thirdParty.setServiceCode(serviceCode);
        thirdParty.setServiceName(serviceName);
        thirdParty.setServiceIp(serviceIp);
        thirdParty.setServiceUrl(serviceUrl);
        thirdParty.setHashSeed(hashSeed);

        thirdPartyService.saveThirdParty(thirdParty);
    }

    public static void main(String[] args) {
        new ThirdPartySetup().execute();
        System.exit(1);
    }
}
