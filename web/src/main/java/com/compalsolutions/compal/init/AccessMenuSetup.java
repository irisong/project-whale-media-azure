package com.compalsolutions.compal.init;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.MenuFrameworkService;
import com.compalsolutions.compal.user.service.UserService;

public class AccessMenuSetup implements InitAppExecutable {
    private static final Log log = LogFactory.getLog(AccessMenuSetup.class);

    @Override
    public boolean execute() {
        long start = System.currentTimeMillis();

        UserService userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
        MenuFrameworkService menuFrameworkService = (MenuFrameworkService) Application.lookupBean(MenuFrameworkService.BEAN_NAME);

        userService.doTruncateAllAccessAndMenu();
        userService.saveAllAccessCatsIfNotExist(AP.createAccessCats());
        userService.saveAllAccessIfNotExist(AP.createAccesses());
        userService.saveAllMenusIfNotExist(MP.createUserMenus());
        menuFrameworkService.doRegenerateTree();

        long end = System.currentTimeMillis();
        log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        log.debug("end AccessMenuSetup");

        return true;
    }

    public static void main(String[] args) {
        new AccessMenuSetup().execute();
    }
}
