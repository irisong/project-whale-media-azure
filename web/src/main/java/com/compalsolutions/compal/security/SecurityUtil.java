package com.compalsolutions.compal.security;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.struts.EvictUserMenuEvent;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.web.HttpLoginInfo;
import com.compalsolutions.compal.web.security.BaseSecurityUtil;

public class SecurityUtil extends BaseSecurityUtil {
    public static LoginInfo persistsSessionForSecurity(HttpSession session, User user) {
        if (session == null) {
            HttpServletRequest req = ServletActionContext.getRequest();
            session = req.getSession();
        }

        session.setAttribute(Global.LOGIN_USER, user);

        /********************************
         * PROCESS UserAccess - START
         ********************************/
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        Map<String, UserAccess> userAccesses = userDetailsService.findUserAuthorizedAccessInMap(user.getUserId());
        session.setAttribute(Global.USER_ACCESS, userAccesses);

        /********************************
         * PROCESS UserAccess - END
         ********************************/

        /********************************
         * PROCESS HttpLoginInfo - START
         ********************************/
        HttpLoginInfo loginInfo = new HttpLoginInfo();
        loginInfo.setUserId(user.getUserId());
        loginInfo.setUser(user);
        loginInfo.addUnboundEvent(new EvictUserMenuEvent());

        if (user instanceof AdminUser) {
            loginInfo.setUserType(new MrmAdminUserType());
        } else if (user instanceof AgentUser) {
            loginInfo.setUserType(new MrmAgentUserType());
        } else if (user instanceof MemberUser) {
            loginInfo.setUserType(new MrmMemberUserType());
        } else
            throw new SystemErrorException("Invalid user type");

        session.setAttribute(FrameworkConst.LOGIN_INFO, loginInfo);

        /********************************
         * PROCESS HttpLoginInfo - END
         ********************************/

        return loginInfo;
    }
}
