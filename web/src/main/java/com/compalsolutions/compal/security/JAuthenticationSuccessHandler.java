package com.compalsolutions.compal.security;

import static org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter.SPRING_SECURITY_FORM_USERNAME_KEY;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.web.ServletUtil;

public class JAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
    private static final Log log = LogFactory.getLog(JAuthenticationSuccessHandler.class);

    private String usernameParameter = SPRING_SECURITY_FORM_USERNAME_KEY;

    private Map<String, String> defaultUrls = new HashMap<>();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
            throws IOException, ServletException {
        SessionLogService sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);
        UserDetailsService userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);

        log.debug("login success~~");

        String userId = null;
        User user = null;
        Object principal = authentication.getPrincipal();
        if (principal instanceof User) {
            user = (User) principal;
            userId = user.getUserId();
        }


        String username = obtainUsername(request);
        String ipAddress = ServletUtil.getRemoteAddr(request);
        sessionLogService.saveSessionLog(username, ipAddress, SessionLog.LOGIN_STATUS_SUCCESS, userId);

        if (user != null) {
            userDetailsService.doUpdateLastLoginDatetime(user);

            SecurityUtil.persistsSessionForSecurity(request.getSession(), user);

            String userType = null;
            if (user instanceof AdminUser) {
                userType = Global.UserType.HQ;
            } else if (user instanceof AgentUser) {
                userType = Global.UserType.AGENT;
            } else if (user instanceof MemberUser) {
                userType = Global.UserType.MEMBER;
            } else
                throw new SystemErrorException("Invalid user type");

            if(user.getUserType().equalsIgnoreCase(Global.UserType.GM))
            {
                userType = Global.UserType.GM;
            }

            SecurityUtil.setLoginUrlToCookies(response, userType);

           String loginUrl = obtainLoginUrlFromCookies(request);

            if(loginUrl!=null)
            {
                if(loginUrl.contains("pub/whale_media")&&!userType.equalsIgnoreCase(Global.UserType.GM))
                {
                    userType = "INVALID_CLIENT";
                }

            }

            if (defaultUrls.containsKey(userType)) {
                setDefaultTargetUrl(defaultUrls.get(userType));
            }
        }

        if (ServletUtil.isAjaxRequest(request)) {
            response.setContentType("application/json");
            PrintWriter out = response.getWriter();

            out.print("{\"successMessage\":\"login success\"}");
            out.flush();
        } else {
            super.onAuthenticationSuccess(request, response, authentication);
        }
    }

    private String obtainLoginUrlFromCookies(HttpServletRequest request)
    {
        String loginUrl = "";
        Map<String, String> cookieMap = new HashMap<>();
        for (Cookie cookie : request.getCookies()) {
            cookieMap.put(cookie.getName(), cookie.getValue());
        }

       loginUrl = cookieMap.get(Global.LOGIN_URL_COOKIES);


        return loginUrl;
    }

    private String obtainUsername(HttpServletRequest request) {
        return request.getParameter(usernameParameter);
    }

    // ---------------- GETTER & SETTER (START) -------------

    public void setUsernameParameter(String usernameParameter) {
        this.usernameParameter = usernameParameter;
    }

    public void setDefaultUrls(Map<String, String> defaultUrls) {
        this.defaultUrls = defaultUrls;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
