package com.compalsolutions.compal.security;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts2.ServletActionContext;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.web.security.DefaultSecureAwareProvider;

public class SecurityAwareProviderImpl extends DefaultSecureAwareProvider {
    @Override
    public LoginInfo getLoginInfo() {
        LoginInfo loginInfo = super.getLoginInfo();

        if (loginInfo != null)
            return loginInfo;

        User user = getLoginUser();

        if (user == null)
            return null;

        HttpSession session = ServletActionContext.getRequest().getSession();
        return SecurityUtil.persistsSessionForSecurity(session, user);
    }

    @Override
    public void setLoginUrlToCookies(HttpServletResponse response, String userType) {
        if (response == null)
            response = ServletActionContext.getResponse();

        String url = null;
        if (Global.UserType.HQ.equalsIgnoreCase(userType)) {
            url = "/pub/login/adminLogin.php";
        } else if (Global.UserType.AGENT.equalsIgnoreCase(userType)) {
            url = "/pub/login/adminLogin.php";
        } else if (Global.UserType.MEMBER.equalsIgnoreCase(userType)) {
            url = "/pub/login/login.php";
        }   else if (Global.UserType.GM.equalsIgnoreCase(userType)) {
            url = "/pub/whale_media/wl_login.php";
        }else {
            url = "/";
        }

        Cookie cookie = new Cookie(Global.LOGIN_URL_COOKIES, url);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        response.addCookie(cookie);
    }
}
