package com.compalsolutions.compal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.compalsolutions.compal.function.user.vo.UserAccess;
import com.compalsolutions.compal.function.user.vo.UserAccessCat;

/**
 * AP - Access Policy
 */
public class AP {
    // User type roles
    public static final String ROLE_AGENT = "ACT_ROLE_AGENT";
    public static final String ROLE_MEMBER = "ACT_ROLE_MEMBER";
    public static final String ROLE_GM = "ACT_ROLE_GM";

    // MEMBER
    public static final String AGENT = "ACT_AGENT";
    public static final String MEMBER = "ACT_MEMBER";
    public static final String FANS_BADGE = "ACT_FANS_BADGE";
    public static final String GUILD = "ACT_GUILD";
    public static final String IDENTITY = "ACT_IDENTITY";
    public static final String CASTING = "ACT_CASTING";

    // WALLET
    public static final String WALLET = "ACT_WALLET";
    public static final String WALLET_ADJUST = "ACT_WALLET_ADJUST";
    public static final String WALLET_TOP_UP = "ACT_WALLET_TOP_UP";

    // REPORT
    public static final String REPORT = "ACT_REPORT";
    public static final String REPORT_POINT_LEADER_BOARD = "ACT_REPORT_POINT_LEADER_BOARD";
    public static final String REPORT_POINT_MEMBER = "ACT_REPORT_MEMBER_POINT";
    public static final String REPORT_TOPUP_PACKAGE = "ACT_REPORT_TOPUP_PACKAGE";
    public static final String REPORT_TOPUP_HISTORY = "ACT_REPORT_TOPUP_HISTORY";
    public static final String REPORT_OMC_WALLET_HISTORY = "ACT_REPORT_OMC_WALLET_HISTORY";
    public static final String REPORT_ACTIVE_USER = "ACT_REPORT_ACTIVE_USER";
    public static final String REPORT_PRIZE = "ACT_REPORT_PRIZE";
    public static final String REPORT_INCOME = "ACT_REPORT_INCOME";


    // SYSTEM CONFIGURATION
    public static final String GIFT_CONFIG = "ACT_GIFT_CONFIG";
    public static final String SYSTEM_CONFIG = "ACT_SYSTEM_CONFIG";
    public static final String WALLET_CONFIG = "ACT_WALLET_CONFIG";
    public static final String BONUS = "ACT_BONUS";
    public static final String EXCHANGE_RATE = "ACT_EXCHANGE_RATE";
    public static final String WALLET_TOP_UP_PACKAGE = "ACT_WALLET_TOP_UP_PACKAGE";
    public static final String RANK_CONFIG = "ACT_RANK_CONFIG";
    public static final String BROAD_CAST = "ACT_BROAD_CAST";
    public static final String FANS_INTIMACY_CONFIG = "ACT_FANS_INTIMACY_CONFIG";
    // USER CAT
    public static final String USER = "ACT_USER";
    public static final String USER_ROLE = "ACT_USER_ROLE";

    // EVENT
    public static final String LUCKY_DRAW = "ACT_LUCKY_DRAW";
    public static final String MISSION = "ACT_MISSION";

    // NOTICE BOARD
    public static final String ANNOUNCE = "ACT_ANNOUNCE";
    public static final String DOCFILE = "ACT_DOCFILE";
    public static final String EMAILQ = "ACT_EMAIL_QUEUE";
    public static final String HELPDESK_TYPE = "ACT_HELPDESK_TYPE";
    public static final String HELPDESK = "ACT_HELPDESK";
    public static final String SMSQ = "ACT_SMS_QUEUE";
    public static final String FAQ = "ACT_FAQ";
    public static final String BANNER = "ACT_BANNER";
    public static final String MEMBER_REPORT_TYPE = "ACT_MEMBER_REPORT_TYPE";
    public static final String MEMBER_REPORT = "ACT_MEMBER_REPORT";

    //MERCHANT
    public  static final String PRODUCT_CATEGORY = "ACT_PRODUCT_CATEGORY";
    public  static final String MERCHANT = "ACT_MERCHANT";

    // SYSTEM HEALTH
    public static final String SYSTEM_MONITOR = "ACT_SYSTEM_MONITOR";
    public static final String DATABASE_MONITOR = "ACT_DATABASE_MONITOR";
    public static final String SYSTEM_LOG = "ACT_SYSTEM_LOG";
    public static final String LOGIN_LOG = "ACT_LOGIN_LOG";
    public static final String WHO_IS_ONLINE = "ACT_WHO_IS_ONLINE";

    // BACKEND TASK
    public static final String BACKEND_TASK_RUN = "ACT_BACKEND_TASK_RUN";
    public static final String BACKEND_TASK_VIEW = "ACT_BACKEND_TASK_VIEW";

    public static class AccessCatConst {
        public static final long MEMBER = 200;
        public static final long WALLET = 400;

        public static final long SYS_CONFIG = 3000;
        public static final long USER = 3200;
        public static final long REPORT = 3300;
        public static final long NOTICE_BOARD = 3400;
        public static final long MERCHANT = 3500;
        public static final long EVENT = 3600;
        public static final long SYSTEM_HEALTH = 3700;
        public static final long BACKEND_PROCESS = 3800;
    }

    public static List<UserAccessCat> createAccessCats() {
        List<UserAccessCat> userAccessCats = new ArrayList<UserAccessCat>();

        userAccessCats.add(new UserAccessCat(AccessCatConst.MEMBER, "member.module", "member.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.WALLET, "wallet.module", "wallet.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.SYS_CONFIG, "system.configuration.module", "system.configuration.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.USER, "user.module", "user.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.EVENT, "event.module", "event.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.NOTICE_BOARD, "notice.board.module", "notice.board.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.MERCHANT, "merchant.module", "merchant.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.REPORT, "report.module", "report.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.SYSTEM_HEALTH, "system.health.module", "system.health.module.short"));
        userAccessCats.add(new UserAccessCat(AccessCatConst.BACKEND_PROCESS, "backend.process.module", "backend.process.module.short"));

        return userAccessCats;
    }

    public static List<UserAccess> createAccesses() {
        List<UserAccess> accesses = new ArrayList<>();

        accesses.addAll(createAccess4AgentUser());
        accesses.addAll(createAccess4MemberUser());

        accesses.addAll(createAccess4MemberModule());
        accesses.addAll(createAccess4WalletModule());
        accesses.addAll(createAccess4ReportModule());
        accesses.addAll(createAccess4SystemConfigModule());
        accesses.addAll(createAccess4UserModule());
        accesses.addAll(createAccess4EventModule());
        accesses.addAll(createAccess4NoticeBoardModule());
        accesses.addAll(createAccess4MerchantModule());
        accesses.addAll(createAccess4SystemHealthModule());
        accesses.addAll(createAccess4BackendProcessModule());

        return accesses;
    }

    private static List<UserAccess> createAccess4AgentUser() {
        List<UserAccess> accesses = new ArrayList<>();
        accesses.add(new UserAccess(ROLE_AGENT, ROLE_AGENT, null, null));

        return accesses;
    }

    private static List<UserAccess> createAccess4MemberUser() {
        List<UserAccess> accesses = new ArrayList<>();
        accesses.add(new UserAccess(ROLE_MEMBER, ROLE_MEMBER, null, null));

        return accesses;
    }

    private static List<UserAccess> createAccess4UserModule() {
        long module = AccessCatConst.USER;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(USER, USER, module, nextSeq(module)));
        accesses.add(new UserAccess(USER_ROLE, USER_ROLE, module, nextSeq(module)));

        return accesses;
    }

    private static List<UserAccess> createAccess4MemberModule() {
        long module = AccessCatConst.MEMBER;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(MEMBER, MEMBER, module, nextSeq(module)));
        accesses.add(new UserAccess(FANS_BADGE, FANS_BADGE, module, nextSeq(module)));
        accesses.add(new UserAccess(IDENTITY, IDENTITY, module, nextSeq(module)));
        accesses.add(new UserAccess(GUILD, GUILD, module, nextSeq(module)));
        accesses.add(new UserAccess(CASTING, CASTING, module, nextSeq(module)));
        return accesses;
    }

    private static List<UserAccess> createAccess4WalletModule() {
        long module = AccessCatConst.WALLET;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(WALLET, WALLET, module, nextSeq(module)));
        accesses.add(new UserAccess(WALLET_ADJUST, WALLET_ADJUST, module, nextSeq(module)));

        return accesses;
    }

    private static List<UserAccess> createAccess4SystemConfigModule() {
        long module = AccessCatConst.SYS_CONFIG;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(GIFT_CONFIG, GIFT_CONFIG, module, nextSeq(module)));
        accesses.add(new UserAccess(SYSTEM_CONFIG, SYSTEM_CONFIG, module, nextSeq(module)));
        accesses.add(new UserAccess(WALLET_CONFIG, WALLET_CONFIG, module, nextSeq(module)));
        accesses.add(new UserAccess(BONUS, BONUS, module, nextSeq(module)));
        accesses.add(new UserAccess(WALLET_TOP_UP_PACKAGE, WALLET_TOP_UP_PACKAGE, module, nextSeq(module)));
        accesses.add(new UserAccess(RANK_CONFIG, RANK_CONFIG, module, nextSeq(module)));
        accesses.add(new UserAccess(BROAD_CAST, BROAD_CAST, module, nextSeq(module)));
        accesses.add(new UserAccess(EXCHANGE_RATE, EXCHANGE_RATE, module, nextSeq(module)));
        accesses.add(new UserAccess(FANS_INTIMACY_CONFIG,FANS_INTIMACY_CONFIG,module,nextSeq(module)));

        return accesses;
    }

    private static List<UserAccess> createAccess4ReportModule() {
        long module = AccessCatConst.REPORT;
        List<UserAccess> accesses = new ArrayList<>();
        accesses.add(new UserAccess(REPORT_POINT_LEADER_BOARD, REPORT_POINT_LEADER_BOARD, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_POINT_MEMBER, REPORT_POINT_MEMBER, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_TOPUP_PACKAGE, REPORT_TOPUP_PACKAGE, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_TOPUP_HISTORY, REPORT_TOPUP_HISTORY, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_OMC_WALLET_HISTORY, REPORT_OMC_WALLET_HISTORY, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_ACTIVE_USER, REPORT_ACTIVE_USER, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_PRIZE, REPORT_PRIZE, module, nextSeq(module)));
        accesses.add(new UserAccess(REPORT_INCOME, REPORT_INCOME, module, nextSeq(module)));
        return accesses;
    }

    private static List<UserAccess> createAccess4EventModule() {
        long module = AccessCatConst.EVENT;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(LUCKY_DRAW, LUCKY_DRAW, module, nextSeq(module)));
        accesses.add(new UserAccess(MISSION, MISSION, module, nextSeq(module)));
        return accesses;
    }

    private static List<UserAccess> createAccess4NoticeBoardModule() {
        long module = AccessCatConst.NOTICE_BOARD;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(ANNOUNCE, ANNOUNCE, module, nextSeq(module)));
        accesses.add(new UserAccess(DOCFILE, DOCFILE, module, nextSeq(module)));
        accesses.add(new UserAccess(EMAILQ, EMAILQ, module, nextSeq(module)));
        accesses.add(new UserAccess(HELPDESK_TYPE, HELPDESK_TYPE, module, nextSeq(module)));
        accesses.add(new UserAccess(HELPDESK, HELPDESK, module, nextSeq(module)));
        accesses.add(new UserAccess(SMSQ, SMSQ, module, nextSeq(module)));
        accesses.add(new UserAccess(FAQ, FAQ, module, nextSeq(module)));
        accesses.add(new UserAccess(BANNER, BANNER, module, nextSeq(module)));
        accesses.add(new UserAccess(MEMBER_REPORT_TYPE, MEMBER_REPORT_TYPE, module, nextSeq(module)));
        accesses.add(new UserAccess(MEMBER_REPORT, MEMBER_REPORT, module, nextSeq(module)));
        return accesses;
    }

    private static List<UserAccess> createAccess4MerchantModule() {
        long module = AccessCatConst.MERCHANT;
        List<UserAccess> accesses = new ArrayList<>();

		accesses.add(new UserAccess(PRODUCT_CATEGORY, PRODUCT_CATEGORY, module, nextSeq(module)));
        accesses.add(new UserAccess(MERCHANT, MERCHANT, module, nextSeq(module)));
        return accesses;
    }

    private static List<UserAccess> createAccess4SystemHealthModule() {
        long module = AccessCatConst.SYSTEM_HEALTH;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(SYSTEM_MONITOR, SYSTEM_MONITOR, module, nextSeq(module)));
        accesses.add(new UserAccess(DATABASE_MONITOR, DATABASE_MONITOR, module, nextSeq(module)));
        accesses.add(new UserAccess(SYSTEM_LOG, SYSTEM_LOG, module, nextSeq(module)));
        accesses.add(new UserAccess(LOGIN_LOG, LOGIN_LOG, module, nextSeq(module)));
        accesses.add(new UserAccess(WHO_IS_ONLINE, WHO_IS_ONLINE, module, nextSeq(module)));

        return accesses;
    }

    private static List<UserAccess> createAccess4BackendProcessModule() {
        long module = AccessCatConst.BACKEND_PROCESS;
        List<UserAccess> accesses = new ArrayList<>();

        accesses.add(new UserAccess(BACKEND_TASK_RUN, BACKEND_TASK_RUN, module, nextSeq(module)));
        accesses.add(new UserAccess(BACKEND_TASK_VIEW, BACKEND_TASK_VIEW, module, nextSeq(module)));

        return accesses;
    }

    private static Map<Long, Long> moduleMap = new HashMap<>();

    private static long nextSeq(long module) {
        long seq = moduleMap.getOrDefault(module, module);
        seq++;
        moduleMap.put(module, seq);
        return seq;
    }

}
