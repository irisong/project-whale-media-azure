package com.compalsolutions.compal.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.general.service.FreqAskQuesService;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.MemberFile;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.tika.Tika;
import org.springframework.core.env.Environment;

import com.compalsolutions.compal.application.Application;

public class FileViewerServlet extends HttpServlet {
    private Log log = LogFactory.getLog(FileViewerServlet.class);

    public static final String MODULE_MOBILE = "mobile";
    public static final String MODULE_IDENTITY = "identity";
    public static final String MODULE_MEMBER_FILE = "memberFile";
    public static final String MODULE_GIFT_FILE = "giftFile";
    public static final String MODULE_ANNOUNCEMENT = "announce";
    public static final String MODULE_FAQ = "faq";
    public static final String MODULE_HELPDESK = "helpDesk";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uri = req.getRequestURI();

        log.debug(uri);

        String[] paths = StringUtils.split(uri, "/");
        if (paths.length < 3) {
            resp.setStatus(500);
            return;
        }

        String module = paths[1];
        String fileName = paths[2];
        String fileId = StringUtils.split(fileName, ".")[0];

        switch (module) {
            case MODULE_MOBILE:
                processMobile(req, resp, paths);
                break;
            case MODULE_IDENTITY:
                processIdentity(req, resp, paths, fileId);
                break;
            case MODULE_MEMBER_FILE:
                processMemberFile(req, resp, paths, fileId, fileName);
                break;
            case MODULE_GIFT_FILE:
                processGiftFile(req, resp, paths, fileId, fileName);
                break;
            case MODULE_ANNOUNCEMENT:
                processAnnouncementFile(req, resp, paths, fileId, fileName);
                break;
            case MODULE_FAQ:
                processFreqAskQuesFile(req, resp, paths, fileId, fileName);
                break;
            case MODULE_HELPDESK:
                processHelpDeskFile(req, resp, paths, fileId, fileName);
                break;
            default:
                processFile(req, resp, paths, fileId, fileName, "");
                break;
        }
    }

    private void processMemberFile(HttpServletRequest req, HttpServletResponse resp, String[] paths, String fileId, String fileName) throws IOException {
        MemberService memberService = Application.lookupBean(MemberService.class);
        MemberFileUploadConfiguration config = Application.lookupBean(MemberFileUploadConfiguration.class);

        String uploadPath = config.geMemberFileUploadPath();
        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }

        MemberFile memberFile = memberService.getMemberFile(fileId);
        if (memberFile == null) {
            resp.setStatus(404);
            return;
        }

        resp.setContentType(memberFile.getContentType());
        ServletOutputStream out = resp.getOutputStream();

        String filePath = uploadPath + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }

    private void processIdentity(HttpServletRequest req, HttpServletResponse resp, String[] paths, String fileId) throws IOException {
  /*      IdentityService identityService = Application.lookupBean(IdentityService.class);

        IdentityFile identityFile = identityService.getIdentityFile(fileId);

        ServletOutputStream out = resp.getOutputStream();

        resp.setContentType(identityFile.getContentType());
        out.write(identityFile.getData());
        out.flush();*/
    }

    private void processMobile(HttpServletRequest req, HttpServletResponse resp, String[] paths) throws IOException {
        Environment env = Application.lookupBean(Environment.class);
        String uploadPath = env.getProperty("mobile.download.path");

        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }

        String contentType = "";

        String fileName = paths[paths.length - 1];
        if (StringUtils.endsWith(fileName, ".apk")) {
            contentType = "application/vnd.android.package-archive";
        } else if (StringUtils.endsWith(fileName, ".jar")) {
            contentType = "application/java-archive";
        }

        ServletOutputStream out = resp.getOutputStream();

        String filePath = uploadPath;

        // exclude 'file' & 'mobile'
        for (int i = 2; i < paths.length; i++) {
            filePath += "/" + paths[i];
        }

        if (StringUtils.isBlank(contentType)) {
            Tika tika = new Tika();

            // Path path = new File(filePath).toPath();
            // contentType = Files.probeContentType(path);
            contentType = tika.detect(new File(filePath));
        }

        log.debug(contentType);

        resp.setContentType(contentType);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }

    private void processGiftFile(HttpServletRequest req, HttpServletResponse resp, String[] paths, String fileId, String fileName) throws IOException {
       GiftFileUploadConfiguration config = Application.lookupBean(GiftFileUploadConfiguration.class);

        String uploadPath = config.getGiftFileUploadPath();

        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }

        String[] fileNames = StringUtils.split(fileName, ".");

        String contentType = "";
        if (fileNames[1].equals("png")) {
            contentType = "image/png";
        } else if (fileNames[1].equals("gif")) {
            contentType = "image/gif";
        }
        resp.setContentType(contentType);
        ServletOutputStream out = resp.getOutputStream();

        String filePath = uploadPath + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }

    private void processAnnouncementFile(HttpServletRequest req, HttpServletResponse resp, String[] paths, String
            fileId, String fileName) throws IOException {
        AnnouncementFileUploadConfiguration config = Application.lookupBean(AnnouncementFileUploadConfiguration.BEAN_NAME, AnnouncementFileUploadConfiguration.class);
        String uploadPath = config.getAnnouncementFileUploadPath();

        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }
        String[] fileNames = StringUtils.split(fileName, ".");

        String contentType = "";
        if (fileNames[1].equals("jpg")) {
            contentType = "image/jpg";
        } else if (fileNames[1].equals("png")) {
            contentType = "image/png";
        } else if (fileNames[1].equals("png")) {
            contentType = "image/gif";
        } else if (fileNames[1].equals("bmp")) {
            contentType = "image/bmp";
        }

        resp.setContentType(contentType);
        ServletOutputStream out = resp.getOutputStream();

        String filePath = uploadPath + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }

    private void processFreqAskQuesFile(HttpServletRequest req, HttpServletResponse resp, String[] paths, String
            fileId, String fileName) throws IOException {
        FreqAskQuesFileUploadConfiguration config = Application.lookupBean(FreqAskQuesFileUploadConfiguration.class);
        FreqAskQuesService freqAskQuesService = Application.lookupBean(FreqAskQuesService.class);

        String uploadPath = config.getFaqFileUploadPath();

        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }

        FreqAskQuesFile freqAskQuesFile = freqAskQuesService.getHelpFreqAskQuesFile(fileId);
        if (freqAskQuesFile == null) {
            resp.setStatus(404);
            return;
        }
        resp.setContentType(freqAskQuesFile.getContentType());

        if ("application/pdf".equalsIgnoreCase(freqAskQuesFile.getContentType())) { //straight download
            resp.setHeader("content-Disposition", "attachment; filename=" + freqAskQuesFile.getFilename()); //attachment=download, filename=download file name
            resp.setContentLength(freqAskQuesFile.getFileSize().intValue());
        }

        ServletOutputStream out = resp.getOutputStream();
        String filePath = uploadPath + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }

    private void processHelpDeskFile(HttpServletRequest req, HttpServletResponse resp, String[] paths, String
            fileId, String fileName) throws IOException {
        HelpDeskFileUploadConfiguration config = Application.lookupBean(HelpDeskFileUploadConfiguration.class);
        HelpDeskService helpDeskService = Application.lookupBean(HelpDeskService.class);

        String uploadPath = config.getHelpDeskFileUploadPath();

        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }

        HelpDeskReplyFile file = helpDeskService.getHelpDeskReplyFile(fileId);
        if (file == null) {
            resp.setStatus(404);
            return;
        }

        if ("application/pdf".equalsIgnoreCase(file.getContentType())) {
            resp.setHeader("content-Disposition", "inline");
        }

        resp.setContentType(file.getContentType());

        ServletOutputStream out = resp.getOutputStream();
        String filePath = uploadPath + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }

    private void processFile(HttpServletRequest req, HttpServletResponse resp, String[] paths, String fileId, String
            fileName, String uploadPath) throws IOException {
        if (StringUtils.isBlank(uploadPath)) {
            Environment env = Application.lookupBean(Environment.class);
            uploadPath = env.getProperty("file.upload.path") + "/" + paths[1];
        }

        if (StringUtils.isBlank(uploadPath)) {
            resp.setStatus(500);
            ServletOutputStream out = resp.getOutputStream();
            out.write("environment error".getBytes());
            out.flush();
            return;
        }

        String[] fileNames = StringUtils.split(fileName, ".");
        String contentType = "";
        if (fileNames[1].equals("jpg")) {
            contentType = "image/jpg";
        } else if (fileNames[1].equals("png")) {
            contentType = "image/png";
        } else if (fileNames[1].equals("png")) {
            contentType = "image/gif";
        } else if (fileNames[1].equals("bmp")) {
            contentType = "image/bmp";
        } else if (fileNames[1].equals("pdf")) {
            contentType = "application/pdf";
        }
        resp.setContentType(contentType);

        if ("application/pdf".equalsIgnoreCase(contentType)) { //if pdf, straight download
            resp.setHeader("content-Disposition", "attachment; filename=" + fileName); //attachment=download, filename=fileId.pdf
        }

        ServletOutputStream out = resp.getOutputStream();
        String filePath = uploadPath + "/" + fileName;
        log.debug(filePath);

        byte[] bytes = Files.readAllBytes(Paths.get(filePath));
        out.write(bytes);
        out.flush();
    }
}
