package com.compalsolutions.compal;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.vo.UserMenu;

public class MP {
    @SuppressWarnings("unused")
    private static final String UNDER_CONSTRUCTION = "/pub/underConstruction.php";

    // ------------- ADMIN MENU --- START
    public static final long AD_MAIN_MOD_SYSTEM = 110000;

    /*******************************************************************************************************************
     * MODULE
     ******************************************************************************************************************/
    public static final long MOD_AD_MEMBER = 110200;
    public static final long MOD_AD_WALLET = 110400;

    public static final long MOD_AD_SYSTEM_CONFIG = 113000;
    public static final long MOD_AD_USER = 113200;
    public static final long MOD_AD_EVENT = 113300;
    public static final long MOD_AD_NOTICE_BOARD = 113400;
    public static final long MOD_AD_MERCHANT = 113500;
    public static final long MOD_AD_REPORT = 113600;
    public static final long MOD_AD_SYSTEM_HEALTH = 113700;
    public static final long MOD_AD_BACKEND_PROCESS = 113800;

    /*******************************************************************************************************************
     * MEMBER
     ******************************************************************************************************************/
    public static final long FUNC_AD_AGENT = 110201;
    public static final long FUNC_AD_MEMBER = 110202;
    public static final long FUNC_AD_FANS_BADGE = 110203;
    public static final long FUNC_AD_GUILD = 110204;
    public static final long FUNC_AD_IDENTITY = 110207;
    public static final long FUNC_AD_CASTING = 110208;
    /*******************************************************************************************************************
     * WALLET
     ******************************************************************************************************************/
    public static final long FUNC_AD_WALLET = 110401;
    public static final long FUNC_AD_WALLET_ADJUST = 110402;
    public static final long FUNC_AD_WALLET_WITHDRAW = 110403;
    public static final long FUNC_AD_WALLET_TOP_UP = 110404;
    public static final long FUNC_AD_WALLET_WITHDRAW2CRYPTO = 110405;

    /*******************************************************************************************************************
     * SYSTEM CONFIGURATION
     ******************************************************************************************************************/
    public static final long FUNC_AD_GIFT_CONFIG = 113004;
    public static final long FUNC_AD_SYSTEM_CONFIG = 113001;
    public static final long FUNC_AD_WALLET_CONFIG = 113002;
    public static final long FUNC_AD_WALLET_TOP_UP_PACKAGE = 113003;
    public static final long FUNC_AD_RANK_CONFIG = 113005;
    public static final long FUNC_AD_BROAD_CAST_CONFIG = 113006;
    public static final long FUNC_AD_EXCHANGE_RATE = 113020;
    public static final long FUNC_AD_FANS_INTIMACY_CONFIG = 113007;
    /*******************************************************************************************************************
     * USER
     ******************************************************************************************************************/
    public static final long FUNC_AD_USER = 113201;
    public static final long FUNC_AD_USER_ROLE = 113202;
    public static final long FUNC_AD_CHANGE_PASSWORD = 113203;

    /*******************************************************************************************************************
     * EVENT
     ******************************************************************************************************************/
    public static final long FUNC_AD_LUCKY_DRAW = 113301;
    public static final long FUNC_AD_MISSION = 113302;

    /*******************************************************************************************************************
     * NOTICE BOARD
     ******************************************************************************************************************/
    public static final long FUNC_AD_ANNOUNCE = 113401;
    public static final long FUNC_AD_DOCFILE = 113402;
    public static final long FUNC_AD_EMAILQ = 113403;
    public static final long FUNC_AD_HELPDESK_TYPE = 113404;
    public static final long FUNC_AD_HELPDESK = 113405;
    public static final long FUNC_AD_SMSQ = 113406;
    public static final long FUNC_AD_FAQ = 113407;
    public static final long FUNC_AD_BANNER = 113408;
    public static final long FUNC_AD_MEMBER_REPORT_TYPE = 113409;
    public static final long FUNC_AD_MEMBER_REPORT = 113410;

    /*******************************************************************************************************************
     * MERCHANT
     ******************************************************************************************************************/
    public static final long FUNC_AD_PRODUCT_CATEGORY = 1134501;
    public static final long FUNC_AD_MERCHANT = 1134502;

    /*******************************************************************************************************************
     * REPORT
     ******************************************************************************************************************/
    public static final long FUNC_AD_POINT_LEADER_BOARD = 113601;
    public static final long FUNC_AD_POINT_MEMBER_REPORT = 113602;
    public static final long FUNC_AD_TOPUP_PACKAGE_REPORT = 113603;
    public static final long FUNC_AD_TOPUP_HISTORY_REPORT = 113604;
    public static final long FUNC_AD_OMC_Wallet_HISTORY_REPORT = 113605;
    public static final long FUNC_AD_ACTIVE_USER_REPORT = 113606;
    public static final long FUNC_AD_PRIZE_REPORT = 113607;
    public static final long FUNC_AD_INCOME_REPORT = 113608;

    /*******************************************************************************************************************
     * SYSTEM
     ******************************************************************************************************************/
    public static final long FUNC_AD_SYSTEM_MONITOR = 113701;
    public static final long FUNC_AD_DATABASE_MONITOR = 113702;
    public static final long FUNC_AD_SYSTEM_LOG = 113703;
    public static final long FUNC_AD_LOGIN_LOG = 113704;
    public static final long FUNC_AD_WHO_IS_ONLINE = 113705;

    public static final long FUNC_AD_BACKEND_PROCESS_RUN = 113801;
    public static final long FUNC_AD_BACKEND_PROCESS_VIEW = 113802;

    // -------------- AGENT MENU --- START
    public static final long AG_MAIN_MOD_SYSTEM = 210000;

    /*******************************************************************************************************************
     * MODULE
     ******************************************************************************************************************/
    public static final long MOD_AG_MASTER = 210100;
    public static final long MOD_AG_WALLET = 210200;

    /*******************************************************************************************************************
     * MASTER
     ******************************************************************************************************************/
    public static final long FUNC_AG_MEMBER = 210110;
    public static final long FUNC_AG_CARD = 210120;
    public static final long FUNC_AG_CARD_TOPUP = 210130;

    /*******************************************************************************************************************
     * WALLET
     ******************************************************************************************************************/
    public static final long FUNC_AG_WALLET = 210310;

    // -------------- MEMBER MENU --- START
    public static final long MB_MAIN_MOD_SYSTEM = 310000;

    public static final long FUNC_MB_BANK_INFO = 310001;

    public static List<UserMenu> createUserMenus() {
        // admin menu
        List<UserMenu> userMenus = new ArrayList<>();
        userMenus.addAll(createAdminMainModules());
        userMenus.addAll(createAdminModules());
        userMenus.addAll(createAdminSubModules());

        // agent menu
        userMenus.addAll(createAgentMainModules());
        userMenus.addAll(createAgentModules());
        userMenus.addAll(createAgentSubModules());

        return userMenus;
    }

    private static List<UserMenu> createAdminMainModules() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel1Menu(AD_MAIN_MOD_SYSTEM, "system.module.short", "system.module.short"));

        return menus;
    }

    private static List<UserMenu> createAdminModules() {
        List<UserMenu> menus = new ArrayList<>();

        menus.add(Factory.createLevel2Menu(MOD_AD_MEMBER, "member.module.short", "member.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_WALLET, "wallet.module.short", "wallet.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_SYSTEM_CONFIG, "system.configuration.module", "system.configuration.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_USER, "user.module.short", "user.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_EVENT, "event.module.short", "event.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_NOTICE_BOARD, "notice.board.module.short", "notice.board.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_MERCHANT, "merchant.module.short", "merchant.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_REPORT, "report.module.short", "report.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_SYSTEM_HEALTH, "system.health.module.short", "system.health.module.short", AD_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AD_BACKEND_PROCESS, "backend.process.module.short", "backend.process.module.short", AD_MAIN_MOD_SYSTEM));

        return menus;
    }

    private static List<UserMenu> createAdminSubModules() {
        List<UserMenu> menus = new ArrayList<>();

        menus.addAll(createAdminMemberFunctions());
        menus.addAll(createAdminWalletFunctions());

        menus.addAll(createAdminSytemConfigFunctions());
        menus.addAll(createAdminUserFunctions());
        menus.addAll(createAdminEventFunctions());
        menus.addAll(createAdminNoticeBoardFunctions());
        menus.addAll(createAdminaMerchantFunctions());
        menus.addAll(createAdminReportFunctions());
        menus.addAll(createAdminSystemHealthFunctions());
        menus.addAll(createAdminBackendProcessFunctions());

        return menus;
    }

    private static List<UserMenu> createAdminUserFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_USER, "ACT_USER", "ACT_USER", MOD_AD_USER, "/app/user/userList.php", AP.USER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_USER_ROLE, "ACT_USER_ROLE", "ACT_USER_ROLE", MOD_AD_USER, "/app/user/userRoleList.php", AP.USER_ROLE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_CHANGE_PASSWORD, "title.changePassword", "title.changePassword", MOD_AD_USER, "/app/user/changePassword.php",
                null, null));

        return menus;
    }

    private static List<UserMenu> createAdminMemberFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_MEMBER, AP.MEMBER, AP.MEMBER, MOD_AD_MEMBER, "/app/member/memberList.php?clear=true", AP.MEMBER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_FANS_BADGE, AP.FANS_BADGE, AP.FANS_BADGE, MOD_AD_MEMBER, "/app/member/fansBadgeList.php?clear=true", AP.FANS_BADGE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_GUILD, AP.GUILD, AP.GUILD, MOD_AD_MEMBER, "/app/member/guildList.php?clear=true", AP.GUILD, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_IDENTITY, AP.IDENTITY, AP.IDENTITY, MOD_AD_MEMBER, "/app/member/identityList.php?clear=true", AP.IDENTITY, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_CASTING, AP.CASTING, AP.CASTING, MOD_AD_MEMBER, "/app/member/admin/castingList.php?clear=true", AP.CASTING, null));
        return menus;
    }

    private static List<UserMenu> createAdminWalletFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_WALLET, AP.WALLET, AP.WALLET, MOD_AD_WALLET, "/app/wallet/walletList.php", AP.WALLET, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_WALLET_ADJUST, AP.WALLET_ADJUST, AP.WALLET_ADJUST, MOD_AD_WALLET, "/app/wallet/walletAdjustList.php", AP.WALLET_ADJUST, null));
        return menus;
    }

    private static List<UserMenu> createAdminSytemConfigFunctions() {
        // 1 access right for all types of bonus

        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_SYSTEM_CONFIG, "title.systemConfiguration", "title.systemConfiguration", MOD_AD_SYSTEM_CONFIG,
                "/app/system/systemConfig.php", AP.SYSTEM_CONFIG, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_GIFT_CONFIG, "title.livestreamgiftConfiguration", "title.livestreamgiftConfiguration", MOD_AD_SYSTEM_CONFIG,
                "/app/system/liveStreamGift.php?clear=true", AP.GIFT_CONFIG, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_WALLET_CONFIG, "title.walletConfiguration", "title.walletConfiguration", MOD_AD_SYSTEM_CONFIG,
                "/app/wallet/walletConfig.php", AP.WALLET_CONFIG, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_WALLET_TOP_UP_PACKAGE, AP.WALLET_TOP_UP_PACKAGE, AP.WALLET_TOP_UP_PACKAGE, MOD_AD_SYSTEM_CONFIG,
                "/app/wallet/admin/walletTopupPackageList.php", AP.WALLET_TOP_UP_PACKAGE, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_EXCHANGE_RATE, AP.EXCHANGE_RATE, AP.EXCHANGE_RATE, MOD_AD_SYSTEM_CONFIG, "/app/master/exchangeRateList.php",
                AP.EXCHANGE_RATE, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_RANK_CONFIG, AP.RANK_CONFIG, AP.RANK_CONFIG, MOD_AD_SYSTEM_CONFIG,
                "/app/system/rankConfigList.php?clear=true", AP.RANK_CONFIG, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_BROAD_CAST_CONFIG, AP.BROAD_CAST, AP.BROAD_CAST, MOD_AD_SYSTEM_CONFIG,
                "/app/system/broadCastConfigList.php?clear=true", AP.BROAD_CAST, null));

        menus.add(Factory.createLevel3Menu(FUNC_AD_FANS_INTIMACY_CONFIG, AP.FANS_INTIMACY_CONFIG, AP.FANS_INTIMACY_CONFIG, MOD_AD_SYSTEM_CONFIG,
                "/app/system/fansIntimacyConfigList.php?clear=true", AP.FANS_INTIMACY_CONFIG, null));

        return menus;
    }

    private static List<UserMenu> createAdminEventFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_LUCKY_DRAW, AP.LUCKY_DRAW, AP.LUCKY_DRAW, MOD_AD_EVENT, "/app/event/luckyDrawPackageList.php?clear=true", AP.LUCKY_DRAW, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_MISSION, AP.MISSION, AP.MISSION, MOD_AD_EVENT, "/app/event/missionList.php?clear=true", AP.MISSION, null));

        return menus;
    }

    private static List<UserMenu> createAdminNoticeBoardFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_ANNOUNCE, AP.ANNOUNCE, AP.ANNOUNCE, MOD_AD_NOTICE_BOARD, "/app/notice/announceList.php?clear=true",
                AP.ANNOUNCE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_DOCFILE, AP.DOCFILE, AP.DOCFILE, MOD_AD_NOTICE_BOARD, "/app/notice/docfileList.php?clear=true", AP.DOCFILE,
                null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_EMAILQ, AP.EMAILQ, AP.EMAILQ, MOD_AD_NOTICE_BOARD, "/app/notice/emailqList.php?clear=true", AP.EMAILQ, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_HELPDESK_TYPE, AP.HELPDESK_TYPE, AP.HELPDESK_TYPE, MOD_AD_NOTICE_BOARD,"/app/notice/helpDeskTypeList.php?clear=true", AP.HELPDESK_TYPE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_HELPDESK, AP.HELPDESK, AP.HELPDESK, MOD_AD_NOTICE_BOARD,"/app/notice/helpDeskList.php?clear=true", AP.HELPDESK, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SMSQ, AP.SMSQ, AP.SMSQ, MOD_AD_NOTICE_BOARD, "/app/notice/smsqList.php?clear=true", AP.SMSQ, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_FAQ, AP.FAQ, AP.FAQ, MOD_AD_NOTICE_BOARD, "/app/notice/freqAskQuesList.php?clear=true", AP.FAQ, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_BANNER, AP.BANNER, AP.BANNER, MOD_AD_NOTICE_BOARD, "/app/notice/bannerList.php?clear=true", AP.BANNER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_MEMBER_REPORT_TYPE, AP.MEMBER_REPORT_TYPE, AP.MEMBER_REPORT_TYPE, MOD_AD_NOTICE_BOARD, "/app/notice/memberReportTypeList.php?clear=true", AP.MEMBER_REPORT_TYPE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_MEMBER_REPORT, AP.MEMBER_REPORT, AP.MEMBER_REPORT, MOD_AD_NOTICE_BOARD, "/app/notice/memberReportList.php?clear=true", AP.MEMBER_REPORT, null));

        return menus;
    }

    private static List<UserMenu> createAdminaMerchantFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_PRODUCT_CATEGORY, AP.PRODUCT_CATEGORY, AP.PRODUCT_CATEGORY, MOD_AD_MERCHANT, "/app/merchant/productCategoryList.php?clear=true", AP.PRODUCT_CATEGORY, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_MERCHANT, AP.MERCHANT, AP.MERCHANT, MOD_AD_MERCHANT, "/app/merchant/merchantList.php?clear=true", AP.MERCHANT, null));
        return menus;
    }

    private static List<UserMenu> createAdminReportFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_POINT_LEADER_BOARD, AP.REPORT_POINT_LEADER_BOARD, AP.REPORT_POINT_LEADER_BOARD, MOD_AD_REPORT, "/app/report/pointLeaderBoard.php?clear=true",
                AP.REPORT_POINT_LEADER_BOARD, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_POINT_MEMBER_REPORT, AP.REPORT_POINT_MEMBER, AP.REPORT_POINT_MEMBER, MOD_AD_REPORT, "/app/report/pointMemberReport.php?clear=true",
                AP.REPORT_POINT_MEMBER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_TOPUP_PACKAGE_REPORT, AP.REPORT_TOPUP_PACKAGE, AP.REPORT_TOPUP_PACKAGE, MOD_AD_REPORT, "/app/report/topupPackageReport.php?clear=true",
                AP.REPORT_POINT_MEMBER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_TOPUP_HISTORY_REPORT, AP.REPORT_TOPUP_HISTORY, AP.REPORT_TOPUP_HISTORY, MOD_AD_REPORT, "/app/report/topupHistoryReport.php?clear=true",
                AP.REPORT_TOPUP_HISTORY, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_OMC_Wallet_HISTORY_REPORT, AP.REPORT_OMC_WALLET_HISTORY, AP.REPORT_OMC_WALLET_HISTORY, MOD_AD_REPORT, "/app/report/OMCWalletHistoryReport.php?clear=true",
                AP.REPORT_OMC_WALLET_HISTORY, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_ACTIVE_USER_REPORT, AP.REPORT_ACTIVE_USER, AP.REPORT_ACTIVE_USER, MOD_AD_REPORT, "/app/report/activeUserReport.php?clear=true",
                AP.REPORT_ACTIVE_USER, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_PRIZE_REPORT, AP.REPORT_PRIZE, AP.REPORT_PRIZE, MOD_AD_REPORT, "/app/report/prizeReport.php?clear=true",
                AP.REPORT_PRIZE, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_INCOME_REPORT, AP.REPORT_INCOME, AP.REPORT_INCOME, MOD_AD_REPORT, "/app/report/incomeReport.php?clear=true",
                AP.REPORT_INCOME, null));
        return menus;
    }

    private static List<UserMenu> createAdminSystemHealthFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_SYSTEM_MONITOR, AP.SYSTEM_MONITOR, AP.SYSTEM_MONITOR, MOD_AD_SYSTEM_HEALTH, "/app/system/systemMonitor.php",
                AP.SYSTEM_MONITOR, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_DATABASE_MONITOR, AP.DATABASE_MONITOR, AP.DATABASE_MONITOR, MOD_AD_SYSTEM_HEALTH,
                "/app/system/databaseMonitor.php", AP.DATABASE_MONITOR, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_SYSTEM_LOG, AP.SYSTEM_LOG, AP.SYSTEM_LOG, MOD_AD_SYSTEM_HEALTH, "/app/system/systemLog.php", AP.SYSTEM_LOG,
                null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_LOGIN_LOG, AP.LOGIN_LOG, AP.LOGIN_LOG, MOD_AD_SYSTEM_HEALTH, "/app/system/loginLogList.php", AP.LOGIN_LOG,
                null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_WHO_IS_ONLINE, AP.WHO_IS_ONLINE, AP.WHO_IS_ONLINE, MOD_AD_SYSTEM_HEALTH, "/app/system/whoIsOnlineList.php",
                AP.WHO_IS_ONLINE, null));

        return menus;
    }

    private static List<UserMenu> createAdminBackendProcessFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AD_BACKEND_PROCESS_RUN, AP.BACKEND_TASK_RUN, AP.BACKEND_TASK_RUN, MOD_AD_BACKEND_PROCESS,
                "/app/system/backend.php", AP.BACKEND_TASK_RUN, null));
        menus.add(Factory.createLevel3Menu(FUNC_AD_BACKEND_PROCESS_VIEW, AP.BACKEND_TASK_VIEW, AP.BACKEND_TASK_VIEW, MOD_AD_BACKEND_PROCESS,
                "/app/system/backendList.php", AP.BACKEND_TASK_RUN, null));

        return menus;
    }

    private static List<UserMenu> createAgentMainModules() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel1Menu(AG_MAIN_MOD_SYSTEM, "system.module.short", "system.module.short"));

        return menus;
    }

    private static List<UserMenu> createAgentModules() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel2Menu(MOD_AG_MASTER, "master.module.short", "master.module.short", AG_MAIN_MOD_SYSTEM));
        menus.add(Factory.createLevel2Menu(MOD_AG_WALLET, "wallet.module.short", "wallet.module.short", AG_MAIN_MOD_SYSTEM));

        return menus;
    }

    private static List<UserMenu> createAgentSubModules() {
        List<UserMenu> menus = new ArrayList<>();
        menus.addAll(createAgentMasterFunctions());

        return menus;
    }

    private static List<UserMenu> createAgentMasterFunctions() {
        List<UserMenu> menus = new ArrayList<>();
        menus.add(Factory.createLevel3Menu(FUNC_AG_MEMBER, AP.MEMBER, AP.MEMBER, MOD_AD_MEMBER, "/app/member/memberList.php", AP.ROLE_AGENT, null));
        menus.add(Factory.createLevel3Menu(FUNC_AG_CARD_TOPUP, "cardTopup", "cardTopup", MOD_AD_MEMBER, "/app/card/cardTopupList.php", AP.ROLE_AGENT, null));

        return menus;
    }

    private static class Factory {
        public static UserMenu createLevel1Menu(long menuId, String menuName, String menuDesc) {
            UserMenu menu = new UserMenu(true);
            menu.setMenuId(menuId);
            menu.setSortSeq(menuId);
            menu.setTreeLevel(1);
            menu.setMenuName(menuName);
            menu.setMenuDesc(menuDesc);
            menu.setIsAuthNeeded(false);

            return menu;
        }

        public static UserMenu createLevel2Menu(long menuId, String menuName, String menuDesc, long parentId) {
            UserMenu menu = new UserMenu(true);
            menu.setMenuId(menuId);
            menu.setSortSeq(menuId);
            menu.setParentId(parentId);

            menu.setTreeLevel(2);
            menu.setMenuName(menuName);
            menu.setMenuDesc(menuDesc);
            menu.setIsAuthNeeded(false);

            return menu;
        }

        public static UserMenu createCustomMenu(long menuId, String menuName, String menuDesc, long parentId, String url, String accessCode, String acrud,
                                                int treeLevel, String menuTarget) {
            UserMenu menu = new UserMenu(true);
            menu.setMenuId(menuId);
            menu.setSortSeq(menuId);
            menu.setParentId(parentId);
            menu.setMenuName(menuName);
            menu.setMenuDesc(menuDesc);
            menu.setMenuUrl(url);
            menu.setTreeLevel(treeLevel);
            menu.setMenuTarget(menuTarget);

            if (StringUtils.isNotBlank(accessCode)) {
                menu.setAccessCode(accessCode);
                menu.setIsAuthNeeded(true);

                if (StringUtils.isNotBlank(acrud)) {
                    if (acrud.trim().length() != 5) {
                        throw new SystemErrorException(
                                "ACRUD[ADMIN, CREATE, READ, UPDATE, DELETE] length for menuId " + menuId + " is not 5. Example : YYYYY, YNYNY");
                    }
                    String admin = acrud.substring(0, 1);
                    String create = acrud.substring(1, 2);
                    String read = acrud.substring(2, 3);
                    String update = acrud.substring(3, 4);
                    String delete = acrud.substring(4);

                    // set all to false
                    menu.setAdminMode(false);
                    menu.setCreateMode(false);
                    menu.setReadMode(false);
                    menu.setUpdateMode(false);
                    menu.setDeleteMode(false);

                    if ("Y".equalsIgnoreCase(admin)) {
                        menu.setAdminMode(true);
                    }
                    if ("Y".equalsIgnoreCase(create)) {
                        menu.setCreateMode(true);
                    }
                    if ("Y".equalsIgnoreCase(read)) {
                        menu.setReadMode(true);
                    }
                    if ("Y".equalsIgnoreCase(update)) {
                        menu.setUpdateMode(true);
                    }
                    if ("Y".equalsIgnoreCase(delete)) {
                        menu.setDeleteMode(true);
                    }
                } else // if ACRUD is blank
                {
                    menu.setAdminMode(true);
                    menu.setCreateMode(true);
                    menu.setReadMode(true);
                    menu.setUpdateMode(true);
                    menu.setDeleteMode(true);
                }
            } else // if accessCode is blank
            {
                menu.setIsAuthNeeded(false);

                // set all to false
                menu.setAdminMode(false);
                menu.setCreateMode(false);
                menu.setReadMode(false);
                menu.setUpdateMode(false);
                menu.setDeleteMode(false);
            }

            return menu;
        }

        /**
         * @param menuId
         * @param menuName
         * @param menuDesc
         * @param parentId
         * @param url
         * @param accessCode
         * @param acrud      ACRUD - ADMIN, CREATE, READ, UPDATE, DELETE. if leave it as blank, default is YYYYY, mean if user
         *                   have either 1 access ACRUD, the user will able to see the menu. Other example, YNYNY, NNNN and etc
         * @return
         */
        public static UserMenu createLevel3Menu(long menuId, String menuName, String menuDesc, long parentId, String url, String accessCode, String acrud) {
            return createCustomMenu(menuId, menuName, menuDesc, parentId, url, accessCode, acrud, 3, null);
        }
    }
}
