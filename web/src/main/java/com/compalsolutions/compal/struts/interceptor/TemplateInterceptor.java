package com.compalsolutions.compal.struts.interceptor;

import java.lang.reflect.Method;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.dispatcher.HttpParameters;

import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.web.HttpLoginInfo;
import com.compalsolutions.compal.web.HttpUserTypeEvent;
import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.ActionProxy;
import com.opensymphony.xwork2.interceptor.Interceptor;
import com.opensymphony.xwork2.util.ValueStack;

public class TemplateInterceptor implements Interceptor {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(TemplateInterceptor.class);

    @Override
    public void destroy() {
    }

    @Override
    public void init() {
    }

    @Override
    public String intercept(ActionInvocation actionInvocation) throws Exception {
        if (actionInvocation.getAction() instanceof Action) {
            Action action = (Action) actionInvocation.getAction();
            // log.debug("action = " + action);

            ActionProxy actionProxy = actionInvocation.getProxy();
            // log.debug("actionName() = " + actionProxy.getActionName());
            String methodName = actionProxy.getMethod();

            // log.debug("action.getClass() = " + action.getClass());

            actionHandle(actionInvocation, action, methodName);
        }

        return actionInvocation.invoke();
    }

    private void actionHandle(ActionInvocation actionInvocation, final Action action, String methodName) {
        try {
            Method[] methods = action.getClass().getDeclaredMethods();
            for (Method method : methods) {
                if (method.getName().equals(methodName)) {
                    EnableTemplate enableTemplate = method.getAnnotation(EnableTemplate.class);
                    if (enableTemplate != null) {
                        log.debug("do template process");

                        ValueStack stack = actionInvocation.getInvocationContext().getValueStack();

                        HttpLoginInfo loginInfo = (HttpLoginInfo) actionInvocation.getInvocationContext().getSession().get(FrameworkConst.LOGIN_INFO);
                        HttpParameters requestMap = actionInvocation.getInvocationContext().getParameters();
                        if (loginInfo == null) {
                            // does nothing
                            break;
                        } else {
                            HttpUserTypeEvent templateEvent = (HttpUserTypeEvent) loginInfo.getUserType().getTemplateEvent();
                            if (templateEvent != null)
                                templateEvent.process(loginInfo, stack, requestMap, enableTemplate.menuKey());
                            else
                                log.warn("templateEvent is null");
                            break;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
