package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.income.IncomeProvider;
import com.compalsolutions.compal.omnicplus.WhalePosProvider;

public class GetWhalePosCoinTrxTask implements ScheduledRunTask {
    private WhalePosProvider whalePosProvider;

    public GetWhalePosCoinTrxTask() {
        whalePosProvider = Application.lookupBean(WhalePosProvider.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        whalePosProvider.doGetWhalePosCoinTrx(runTask.getLogger());
    }
}
