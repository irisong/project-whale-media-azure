package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;

public class ProcessEndMissionTask implements ScheduledRunTask {
    private VjMissionService vjMissionService;

    public ProcessEndMissionTask() {
        vjMissionService = Application.lookupBean(VjMissionService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        vjMissionService.doProcessEndedMission();
    }
}
