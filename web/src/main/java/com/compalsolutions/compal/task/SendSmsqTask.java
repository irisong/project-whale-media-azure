package com.compalsolutions.compal.task;

import org.quartz.DisallowConcurrentExecution;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.general.service.SmsQueueService;

@DisallowConcurrentExecution
public class SendSmsqTask implements ScheduledRunTask {

    private SmsQueueService smsQueueService;

    public SendSmsqTask() {
        smsQueueService = Application.lookupBean(SmsQueueService.BEAN_NAME, SmsQueueService.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        smsQueueService.doSentSms();
        // smsQueueService.doSentSms();
        // smsQueueService.doSentOneWaySms();
    }
}
