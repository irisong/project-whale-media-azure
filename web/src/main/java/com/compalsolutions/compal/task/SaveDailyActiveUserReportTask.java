package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.wallet.service.WalletService;

public class SaveDailyActiveUserReportTask implements ScheduledRunTask {
    private WalletService walletService;

    public SaveDailyActiveUserReportTask() {
        walletService = Application.lookupBean(WalletService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        walletService.saveDailyActiveUserReport();
    }
}
