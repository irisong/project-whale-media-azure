package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.point.service.PointService;
import org.quartz.DisallowConcurrentExecution;

@DisallowConcurrentExecution
public class ProcessGlobalGiftTask implements ScheduledRunTask {
    private PointService pointService;

    public ProcessGlobalGiftTask() {
        pointService = Application.lookupBean(PointService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        pointService.doProcessGlobalGift(runTask.getLogger());
    }
}
