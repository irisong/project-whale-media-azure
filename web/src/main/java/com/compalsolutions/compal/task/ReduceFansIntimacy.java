package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.member.service.FansIntimacyService;

public class ReduceFansIntimacy implements ScheduledRunTask {
    private FansIntimacyService fansIntimacyService;

    public ReduceFansIntimacy() {
        fansIntimacyService = Application.lookupBean(FansIntimacyService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        fansIntimacyService.doReduceFansIntimacyForNotActiveFans();
    }
}
