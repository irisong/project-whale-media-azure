package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.provider.EmailProvider;

public class SendEmailqTask implements ScheduledRunTask {
    private EmailProvider emailProvider;

    public SendEmailqTask() {
        emailProvider = Application.lookupBean(EmailProvider.BEAN_NAME, EmailProvider.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        emailProvider.doSendEmail(task.getLogger());
    }
}
