package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.notification.NotificationConfProvider;

public class PushAnnouncementNotificationTask implements ScheduledRunTask {
    private NotificationConfProvider notificationConfProvider;

    public PushAnnouncementNotificationTask() {
        notificationConfProvider = Application.lookupBean(NotificationConfProvider.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        notificationConfProvider.doProcessAnnouncementNotification(runTask.getLogger());
    }
}
