package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.service.TokenService;

public class ProcessForceLoginTask implements ScheduledRunTask {
    private TokenProvider tokenProvider;

    public ProcessForceLoginTask() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        tokenProvider.processForceLogin(runTask.getLogger());
    }
}
