package com.compalsolutions.compal.task;

import java.util.Date;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.websocket.WebSocketConfiguration;
import com.compalsolutions.compal.websocket.WsMessageProvider;

public class WebSocketBroadcastTask implements ScheduledRunTask {
    private WsMessageProvider wsMessageProvider;

    public WebSocketBroadcastTask() {
        wsMessageProvider = Application.lookupBean(WsMessageProvider.class);
    }

    @Override
    public void preScheduled(RunTask task) {
    }

    @Override
    public void preParameter(RunTask task) throws Exception {
    }

    @Override
    public void process(RunTask task) throws Exception {
        WebSocketConfiguration config = Application.lookupBean(WebSocketConfiguration.class);

        if (!config.isEnable()) {
            task.getLogger().log("websocket is disabled");
            return;
        }

        Date dateFrom = DateUtil.addMinute(new Date(), -5);

        wsMessageProvider.doProcessMessageSince(dateFrom, config.getServerName());
    }
}
