package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.income.IncomeProvider;

public class ProcessIncomeVJStreamTask implements ScheduledRunTask {
    private IncomeProvider incomeProvider;

    public ProcessIncomeVJStreamTask() {
        incomeProvider = Application.lookupBean(IncomeProvider.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        incomeProvider.doProcessVjIncome(runTask.getLogger());
    }
}
