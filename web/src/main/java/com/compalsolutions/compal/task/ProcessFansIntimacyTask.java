package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.security.TokenProvider;

public class ProcessFansIntimacyTask implements ScheduledRunTask {
    private FansIntimacyService fansIntimacyService;

    public ProcessFansIntimacyTask() {
        fansIntimacyService = Application.lookupBean(FansIntimacyService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        fansIntimacyService.doProcessFansIntimacyGain("","");
    }
}
