package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.general.service.HelpDeskService;

public class UpdateRepliedHelpDeskTicketTask implements ScheduledRunTask {
    private HelpDeskService helpDeskService;

    public UpdateRepliedHelpDeskTicketTask() {
        helpDeskService = Application.lookupBean(HelpDeskService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        helpDeskService.doUpdateRepliedHelpDeskToCloseAfter168H();
    }
}
