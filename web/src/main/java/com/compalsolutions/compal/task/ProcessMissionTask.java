package com.compalsolutions.compal.task;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.function.schedule.ScheduledRunTask;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.point.service.PointService;
import org.quartz.DisallowConcurrentExecution;

public class ProcessMissionTask implements ScheduledRunTask {
    private VjMissionService vjMissionService;

    public ProcessMissionTask() {
        vjMissionService = Application.lookupBean(VjMissionService.class);
    }

    @Override
    public void preScheduled(RunTask runTask) {
    }

    @Override
    public void preParameter(RunTask runTask) throws Exception {
    }

    @Override
    public void process(RunTask runTask) throws Exception {
        vjMissionService.doProcessMissionComplete();
    }
}
