package com.compalsolutions.compal;

import com.compalsolutions.compal.identity.IdentityConfiguration;
import com.compalsolutions.compal.omnicplus.OmcWalletConfiguration;
import com.compalsolutions.compal.omnicplus.OmnicplusConfiguration;
import com.compalsolutions.compal.sms.NexmoConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.apache.commons.lang3.Validate;

import com.compalsolutions.compal.function.config.BaseProjectAppConfig;
import com.compalsolutions.compal.function.config.BasicAppConfig;
import com.compalsolutions.compal.function.config.DataSourceAppConfig;
import com.compalsolutions.compal.function.config.JpaAppConfig;
import com.compalsolutions.compal.security.SecurityAwareProviderImpl;
import com.compalsolutions.compal.security.SecurityUtil;
import com.compalsolutions.compal.wallet.WalletInfoCacheProvider;
import com.compalsolutions.compal.wallet.WalletInfoCacheUtil;
import com.compalsolutions.compal.web.security.SecureAwareProvider;
import com.compalsolutions.compal.websocket.WebSocketConfiguration;

import rest.RestUtil;

@Configuration
@Import({BasicAppConfig.class, DataSourceAppConfig.class, JpaAppConfig.class, MiscAppConfig.class})
@ImportResource("classpath:com/compalsolutions/compal/tx-config.xml")
@PropertySource(value = {"classpath:com/compalsolutions/compal/appConfig.properties", "classpath:com/compalsolutions/compal/global.properties"})
@EnableTransactionManagement
@ComponentScan(basePackages = {"com.compalsolutions.compal"})
public class ProjectAppConfig extends BaseProjectAppConfig {
    @Autowired
    private Environment env;

    // custom implementation for SecureAwareProvider.
    @Bean
    public SecureAwareProvider secureAwareProvider() {
        SecureAwareProvider provider = new SecurityAwareProviderImpl();

        SecurityUtil.setSecureAwareProvider(provider);
        return provider;
    }

    @Bean
    public WalletInfoCacheProvider walletInfoCacheProvider() {
        WalletInfoCacheProvider provider = () -> {
            WebUtil.clearWalletTypes();
            RestUtil.clearWalletInfos();
        };

        WalletInfoCacheUtil.setWalletInfoCacheProvider(provider);
        return provider;
    }

    @Bean
    public WebSocketConfiguration webSocketConfiguration() {
        WebSocketConfiguration config = new WebSocketConfiguration();

        config.setEnable(env.getProperty("websocket.enable", Boolean.class, false));
        config.setServerName(env.getProperty("websocket.serverName"));

        return config;
    }

    @Bean
    public NexmoConfig nexmoConfig() {
        NexmoConfig config = new NexmoConfig();
        config.setApiKey(env.getProperty("nexmoConfig.apiKey"));
        config.setApiSecret(env.getProperty("nexmoConfig.apiSecret"));

        Validate.notBlank(config.getApiKey());
        Validate.notBlank(config.getApiSecret());
        return config;
    }

    @Bean
    public FileUploadConfiguration fileUploadConfiguration() {
        FileUploadConfiguration config = new FileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));
        config.setAzureBlobStorageConnection(env.getProperty("storageConnectionString"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    @Bean
    public IdentityConfiguration identityConfiguration() {
        IdentityConfiguration config = new IdentityConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));

        Validate.notBlank(config.getServerUrl());
        return config;
    }

    @Bean
    public OmnicplusConfiguration omnicplusConfiguration() {
        OmnicplusConfiguration config = new OmnicplusConfiguration();
        config.setServerUrl(env.getProperty("omnicplus.url"));

        Validate.notBlank(config.getServerUrl());
        return config;
    }

    @Bean
    public OmcWalletConfiguration omcWalletConfiguration() {
        OmcWalletConfiguration config = new OmcWalletConfiguration();
        config.setServerUrl(env.getProperty("omc.serverUrl"));

        Validate.notBlank(config.getServerUrl());
        return config;
    }

    @Bean
    public MemberFileUploadConfiguration memberFileUploadConfiguration() {
        MemberFileUploadConfiguration config = new MemberFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));
        Validate.notBlank(config.getServerUrl());
        return config;
    }

    @Bean
    public GiftFileUploadConfiguration giftFileUploadConfiguration() {
        GiftFileUploadConfiguration config = new GiftFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));
        Validate.notBlank(config.getServerUrl());
        return config;
    }

    @Bean
    public AnnouncementFileUploadConfiguration announcementFileUploadConfiguration() {
        AnnouncementFileUploadConfiguration config = new AnnouncementFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    @Bean
    public FreqAskQuesFileUploadConfiguration freqAskQuesFileUploadConfiguration() {
        FreqAskQuesFileUploadConfiguration config = new FreqAskQuesFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    @Bean
    public HelpDeskFileUploadConfiguration helpDeskFileUploadConfiguration() {
        HelpDeskFileUploadConfiguration config = new HelpDeskFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    @Bean
    public BannerFileUploadConfiguration bannerFileUploadConfiguration() {
        BannerFileUploadConfiguration config = new BannerFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    @Bean
    public GuildFileUploadConfiguration guildFileUploadConfiguration() {
        GuildFileUploadConfiguration config = new GuildFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    @Bean
    public MemberReportFileUploadConfiguration memberReportFileUploadConfiguration() {
        MemberReportFileUploadConfiguration config = new MemberReportFileUploadConfiguration();
        config.setServerUrl(env.getProperty("fileServerUrl"));
        config.setUploadPath(env.getProperty("file.upload.path"));

        Validate.notBlank(config.getServerUrl());
        Validate.notBlank(config.getUploadPath());
        return config;
    }

    /**
     * Redis configuration start
     **/

    //this is for default configuration
//    @Bean
//    JedisConnectionFactory jedisConnectionFactory() {
//        return new JedisConnectionFactory();
//    }
//    @Bean
//    public JedisConnectionFactory jedisConnectionFactory() {
//        RedisStandaloneConfiguration redisStandaloneConfiguration =
//                new RedisStandaloneConfiguration(env.getProperty("redis.url"), Integer.valueOf(env.getProperty("redis.port")));
////        redisStandaloneConfiguration.setPassword(RedisPassword.of("yourRedisPasswordIfAny"));
//        return new JedisConnectionFactory(redisStandaloneConfiguration);
//    }
//
//    @Bean
//    public RedisTemplate<String, Object> redisTemplate() {
//        RedisTemplate<String, Object> template = new RedisTemplate<>();
//        RedisStandaloneConfiguration redisStandaloneConfiguration =
//                new RedisStandaloneConfiguration(env.getProperty("redis.url"), Integer.valueOf(env.getProperty("redis.port")));
//        template.setConnectionFactory(new JedisConnectionFactory(redisStandaloneConfiguration));
//        return template;
//    }

    /**Redis configuration end**/
}
