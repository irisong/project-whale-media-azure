package com.compalsolutions.compal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class MiscAppConfig {
    @SuppressWarnings("unused")
    @Autowired
    private Environment env;
}
