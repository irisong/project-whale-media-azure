package com.compalsolutions.compal.websocket.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.AbstractJdbcDao;
import com.compalsolutions.compal.websocket.vo.WsMessage;

@Component(WsMessageSqlDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WsMessageSqlDaoImpl extends AbstractJdbcDao implements WsMessageSqlDao {
    @Override
    public List<WsMessage> findNotProcessedMessagesSince(Date date, String broadcasterCode, int numberOfRecords) {
        List<Object> params = new ArrayList<>();
        params.add(broadcasterCode);
        params.add(date);

        String sql = "select m.* from ws_message m left join ws_message_broadcast mb on m.messsage_id=mb.messsage_id and mb.broadcast_code=?\n" + //
                "where mb.broadcast_code is null and m.datetime_add >= ?\n" + //
                "order by m.datetime_add asc";

        sql = convertToPaginationSql(sql, numberOfRecords);

        return query(sql, (rs, rowNum) -> {
            WsMessage wsMessage = new WsMessage(false);
            wsMessage.setMessageId(rs.getString("messsage_id"));
            wsMessage.setOwnerId(rs.getString("owner_id"));
            wsMessage.setOwnerType(rs.getString("owner_type"));
            wsMessage.setType(rs.getString("type"));
            wsMessage.setData(rs.getString("data"));
            wsMessage.setMesssage(rs.getString("message"));

            return wsMessage;
        }, params.toArray());
    }
}
