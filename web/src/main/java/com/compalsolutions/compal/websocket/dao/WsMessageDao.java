package com.compalsolutions.compal.websocket.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.websocket.vo.WsMessage;

public interface WsMessageDao extends BasicDao<WsMessage, String> {
    public static final String BEAN_NAME = "wsMessageDao";
}
