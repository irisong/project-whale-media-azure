package com.compalsolutions.compal.websocket.dao;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.websocket.vo.WsMessage;

public interface WsMessageSqlDao {
    public static final String BEAN_NAME = "wsMessageSqlDao";

    public List<WsMessage> findNotProcessedMessagesSince(Date date, String broadcasterCode, int numberOfRecords);
}
