package com.compalsolutions.compal.websocket;

import java.util.Date;

public interface WsMessageProvider {
    public static final String BEAN_NAME = "wsMessageProvider";

    public void doProcessMessageSince(Date date, String broadcasterCode);
}
