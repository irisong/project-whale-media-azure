package com.compalsolutions.compal.websocket;

public class WebSocketConfiguration {
    private boolean enable;
    private String serverName;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }
}
