package com.compalsolutions.compal.websocket.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.websocket.vo.WsMessage;

@Component(WsMessageDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WsMessageDaoImpl extends Jpa2Dao<WsMessage, String> implements WsMessageDao {
    public WsMessageDaoImpl() {
        super(new WsMessage(false));
    }
}
