package com.compalsolutions.compal.websocket.dao;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.dao.Jpa2Dao;
import com.compalsolutions.compal.websocket.vo.WsMessageBroadcast;

@Component(WsMessageBroadcastDao.BEAN_NAME)
@Scope(BeanDefinition.SCOPE_PROTOTYPE)
public class WsMessageBroadcastDaoImpl extends Jpa2Dao<WsMessageBroadcast, String> implements WsMessageBroadcastDao {
    public WsMessageBroadcastDaoImpl() {
        super(new WsMessageBroadcast(false));
    }
}
