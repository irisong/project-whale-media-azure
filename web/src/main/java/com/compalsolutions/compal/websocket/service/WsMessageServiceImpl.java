package com.compalsolutions.compal.websocket.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamActiveUserDto;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamBroadCasterDto;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.websocket.WsManagerProvider;
import com.google.gson.Gson;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.compalsolutions.compal.websocket.dao.WsMessageBroadcastDao;
import com.compalsolutions.compal.websocket.dao.WsMessageDao;
import com.compalsolutions.compal.websocket.dao.WsMessageSqlDao;
import com.compalsolutions.compal.websocket.vo.WsMessage;
import com.compalsolutions.compal.websocket.vo.WsMessageBroadcast;
import websocket.WsManager;
import websocket.WsOutGoingUtil;

@Component(WsMessageService.BEAN_NAME)
public class WsMessageServiceImpl implements WsMessageService {
    @SuppressWarnings("unused")
    private Log log = LogFactory.getLog(WsMessageService.class);

    private WsMessageDao wsMessageDao;
    private WsMessageBroadcastDao wsMessageBroadcastDao;
    private WsMessageSqlDao wsMessageSqlDao;

    @Autowired
    public WsMessageServiceImpl(WsMessageDao wsMessageDao, WsMessageBroadcastDao wsMessageBroadcastDao, WsMessageSqlDao wsMessageSqlDao) {
        this.wsMessageDao = wsMessageDao;
        this.wsMessageBroadcastDao = wsMessageBroadcastDao;
        this.wsMessageSqlDao = wsMessageSqlDao;
    }

    @Override
    public List<WsMessage> findNotProcessedMessagesSince(Date date, String broadcasterCode, int numberOfRecords) {
        return wsMessageSqlDao.findNotProcessedMessagesSince(date, broadcasterCode, numberOfRecords);
    }

    @Override
    public void saveWsMessageBroadcast(WsMessageBroadcast wsMessageBroadcast) {
        wsMessageBroadcastDao.save(wsMessageBroadcast);
    }

    @Override
    public WsMessage getWsMessage(String messageId) {
        return wsMessageDao.get(messageId);
    }

    @Override
    public void broadCastMessageLiveStreamData(LiveStreamDto liveStreamDto) {
        WsOutGoingUtil.broadcastMessage_processVJPoint(liveStreamDto);
    }

    @Override
    public void updateOnlineMember(String channelId, String actionType) {
        LiveStreamProvider liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        LiveStreamService liveStreamService = Application.lookupBean(LiveStreamService.class);

        LiveStreamBroadCasterDto broadCasterDto = null;
        LiveStreamDto wsLiveStream = liveStreamProvider.getChannelDetail(channelId);

        LiveStreamDto liveStreamDto = new LiveStreamDto();
        liveStreamDto.setChannelId(channelId);
        liveStreamDto.setMemberList(wsLiveStream.getMemberList());

        if(actionType.equalsIgnoreCase(LiveStreamAudience.ENTER_CHANNEL)) {
            LiveStreamChannel liveStreamChannel = liveStreamService.getLiveStreamChannelByChannelId(channelId);
            if(liveStreamChannel.getLinkUserAgoraUid() != null) {
                broadCasterDto = processBroadCaster(liveStreamChannel.getLinkUserAgoraUid());
            }
            liveStreamDto.setView(wsLiveStream.getView());
        }

        liveStreamDto.setBroadCaster(broadCasterDto);
        broadCastMessageLiveStreamData(liveStreamDto);
    }

    @Override
    public void updateTotalPointAndFansCount(String channelId, BigDecimal point) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        int fans = wsManagerProvider.getFansCountByChannelId(channelId) + 1;
        wsManagerProvider.setFansCountByChannelId(channelId, fans);

        LiveStreamDto liveStreamDto = new LiveStreamDto();
        liveStreamDto.setChannelId(channelId);
        liveStreamDto.setPoint(point);
        liveStreamDto.setFans(fans);

        broadCastMessageLiveStreamData(liveStreamDto);
    }

    @Override
    public void updateFollowerCount(String channelId) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        int follower = wsManagerProvider.getFollowerCountByChannelId(channelId) + 1;
        wsManagerProvider.setFollowerCountByChannelId(channelId, follower);

        LiveStreamDto liveStreamDto = new LiveStreamDto();
        liveStreamDto.setChannelId(channelId);
        liveStreamDto.setFollower(follower);

        broadCastMessageLiveStreamData(liveStreamDto);
    }

    @Override
    public void updateBroadCasterInfo(LiveStreamChannel liveStreamChannel, String agoraUid) {
        LiveStreamDto liveStreamDto = new LiveStreamDto();
        LiveStreamBroadCasterDto broadCasterDto ;

        if (agoraUid != null) {
            broadCasterDto = processBroadCaster(agoraUid);
        } else {
            broadCasterDto = null;
        }

//        System.out.println("Show me agora ID : "+broadCasterDto == null ? gg : broadCasterDto.getAgoraUuid());
        liveStreamDto.setChannelId(liveStreamChannel.getChannelId());
        liveStreamDto.setBroadCaster(broadCasterDto);
        broadCastMessageLiveStreamData(liveStreamDto);
    }

    private LiveStreamBroadCasterDto processBroadCaster(String agoraUid) {
        Environment env = Application.lookupBean(Environment.class);
        MemberService memberService = Application.lookupBean(MemberService.class);
        LiveStreamBroadCasterDto broadCasterDto = new LiveStreamBroadCasterDto();
        if(agoraUid != null) {
            Member member = memberService.findMemberByAgoraUid(Integer.parseInt(agoraUid));
            if(member != null) {
                final String parentUrl = env.getProperty("fileServerUrl") + "/file/memberFile";

                MemberFile memberFile = memberService.findMemberFileByMemberIdAndType(member.getMemberId(), MemberFile.UPLOAD_TYPE_PROFILE_PIC);
                broadCasterDto.setMemberProfileUrl("");
                if (memberFile != null) {
                    memberFile.setFileUrlWithParentPath(parentUrl);
                    broadCasterDto.setMemberProfileUrl(memberFile.getFileUrl());
                }
                broadCasterDto.setMemberId(member.getMemberId());
                broadCasterDto.setProfileName(member.getMemberDetail().getProfileName());
                broadCasterDto.setAgoraUuid(member.getAgoraUid());
            }
            return broadCasterDto;
        } else {
            return null;
        }
    }
}
