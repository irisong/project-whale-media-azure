package com.compalsolutions.compal.websocket.dao;

import com.compalsolutions.compal.dao.BasicDao;
import com.compalsolutions.compal.websocket.vo.WsMessageBroadcast;

public interface WsMessageBroadcastDao extends BasicDao<WsMessageBroadcast, String> {
    public static final String BEAN_NAME = "wsMessageBroadcastDao";
}
