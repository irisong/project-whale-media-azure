package com.compalsolutions.compal.websocket;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.websocket.service.WsMessageService;
import com.compalsolutions.compal.websocket.vo.WsMessage;
import com.compalsolutions.compal.websocket.vo.WsMessageBroadcast;

import websocket.WsOutGoingUtil;

@Service(WsMessageProvider.BEAN_NAME)
public class WsMessageProviderImpl implements WsMessageProvider {
    private WsMessageService wsMessageService;

    @Autowired
    public WsMessageProviderImpl(WsMessageService wsMessageService) {
        this.wsMessageService = wsMessageService;
    }

    @Override
    public void doProcessMessageSince(Date date, String broadcasterCode) {
        final int numberOfRecords = 10;

        List<WsMessage> wsMessages = wsMessageService.findNotProcessedMessagesSince(date, broadcasterCode, numberOfRecords);

        while (CollectionUtil.isNotEmpty(wsMessages)) {
            for (WsMessage wsMessage : wsMessages) {
                WsOutGoingUtil.broadcastMessage(wsMessage);

                WsMessageBroadcast wsMessageBroadcast = new WsMessageBroadcast(true);
                wsMessageBroadcast.setMessageId(wsMessage.getMessageId());
                wsMessageBroadcast.setBroadcasterCode(broadcasterCode);
                wsMessageBroadcast.setTrxDatetime(new Date());
                wsMessageService.saveWsMessageBroadcast(wsMessageBroadcast);
            }

            wsMessages = wsMessageService.findNotProcessedMessagesSince(date, broadcasterCode, numberOfRecords);
        }
    }
}
