import static com.compalsolutions.compal.WebUtil.getSystemConfig;

import java.util.List;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.general.vo.Address;
import com.compalsolutions.compal.general.vo.PaymentInfo;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.opensymphony.xwork2.ActionContext;

/**
 * AU : ApplicationUtil This is the class which can called by struts tag.
 */
public class AU {
    public static List<Language> getLanguages() {
        return WebUtil.getLanguages();
    }

    public static boolean enableMemberLogin() {
        SystemConfig systemConfig = getSystemConfig();
        if (systemConfig != null)
            return systemConfig.getMemberLogin();
        else
            return false;
    }

    public static boolean enableMemberRegister() {
        SystemConfig systemConfig = getSystemConfig();
        if (systemConfig != null)
            return systemConfig.getMemberRegister();
        else
            return false;
    }

    public static String getWalletName(int walletType) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getWalletName(locale, walletType);
    }

    public static String getWalletCurrency(int walletType) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getWalletCurrency(locale, walletType);
    }

    public static String getWalletNameWithCurrency(int walletType) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getWalletNameWithCurrency(locale, walletType);
    }

    public static List<Integer> getActiveMemberWalletTypes() {
        return WebUtil.getActiveMemberWalletTypes();
    }

    public static String stringReplace(String text, String searchString, String replacement) {
        return StringUtils.replace(text, searchString, replacement);
    }

    public static String getStatusDesc(String status) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getStatusDesc(locale, status);
    }

    public static String getPaymentInfoLabel(PaymentInfo paymentInfo) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getPaymentInfoLabel(locale, paymentInfo);
    }

    public static String getFullAddress(Address address, String lineSeparator) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getFullAddress(locale, address, lineSeparator);
    }

    public static String getInternationalFullAddress(Address address, String lineSeparator) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getInternationalFullAddress(locale, address, lineSeparator);
    }

    public static String getApplicationName() {
        SystemConfig systemConfig = getSystemConfig();
        if (systemConfig != null)
            return systemConfig.getApplicationName();
        else
            return null;
    }

    public static String getCompanyName() {
        SystemConfig systemConfig = getSystemConfig();
        if (systemConfig != null)
            return systemConfig.getCompanyName();
        else
            return null;
    }

    public static String getSupportEmail() {
        SystemConfig systemConfig = getSystemConfig();
        if (systemConfig != null)
            return systemConfig.getSupportEmail();
        else
            return null;
    }

    public static String getSystemConfigImageType(String imageType) {
        return WebUtil.getSystemConfigImageType(imageType);
    }

    public static String getCountryNameByCountryCode(String countryCode) {
        Locale locale = ActionContext.getContext().getLocale();
        return WebUtil.getCountryNameByCountryCode(locale, countryCode);
    }
}
