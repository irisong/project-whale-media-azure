package websocket.serializer;

import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;
import java.util.TimeZone;

public class VJLiveStreamDetailSerializer implements JsonSerializer<LiveStreamDto> {

    @Override
    public JsonElement serialize(LiveStreamDto vjPointDto, Type type, JsonSerializationContext jsonSerializationContext) {
        DateFormat iso8601Format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.US);
        iso8601Format.setTimeZone(TimeZone.getTimeZone("UTC"));

        JsonObject result = new JsonObject();
        Gson gson = new Gson();
        String memberJsonString = gson.toJson(vjPointDto.getMemberList());
        result.add("channelId", new JsonPrimitive(vjPointDto.getChannelId()));
        result.add("point", new JsonPrimitive(vjPointDto.getPoint()));
        result.add("view", new JsonPrimitive(vjPointDto.getView()));
        result.add("memberList", new JsonPrimitive(memberJsonString));

        return result;
    }

}
