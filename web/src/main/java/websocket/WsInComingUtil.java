package websocket;

import java.io.IOException;
import java.util.Locale;

import javax.websocket.Session;

import com.compalsolutions.compal.websocket.WsManagerProvider;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import websocket.dto.WsMessageDto;

public class WsInComingUtil {
    private static final Log log = LogFactory.getLog(WsInComingUtil.class);

    public static void processMessage(Session session, String jsonString) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        String userId = wsManagerProvider.getUserId(session);
        log.debug("Session: " + session.getId() + " >> userId: " + userId + " >> " + jsonString);

        // does nothing
        if (!isJSONValid(jsonString))
            return;

        WsMessageDto message;

        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        try {
            message = mapper.readValue(jsonString, WsMessageDto.class);
        } catch (IOException e) {
            throw new ValidatorException(e.getMessage(), e);
        }

        switch (message.getType()) {
            case WsConst.MessageType.CONNECTED:
                // does nothing
                break;
            case WsConst.MessageType.JWT_TOKEN:
                processTokenMessage(session, message);
                break;
            case WsConst.MessageType.LOGOUT:
                processLogoutMessage(session, message);
                break;
            case WsConst.MessageType.VJ_CHANNEL:
                processChannelMessage(session, message);
                break;
            case WsConst.MessageType.VJ_CHANNEL_REMOVE:
                processRemoveChannelIdMessage(session);
                break;
        }
    }

    private static boolean isJSONValid(String jsonInString) {
        try {
            final ObjectMapper mapper = new ObjectMapper();
            mapper.readTree(jsonInString);
            return true;
        } catch (IOException e) {
            return false;
        }
    }

    private static void processTokenMessage(Session session, WsMessageDto message) {
        String tokenString = (String) message.getData();

        TokenProvider tokenProvider = Application.lookupBean(TokenProvider.class);
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        Locale locale = Application.lookupBean(Locale.class);

        try {
            Pair<JwtClaims, JwtToken> pair = tokenProvider.validateJwtToken(locale, tokenString);

            JwtToken jwtToken = pair.getRight();

            wsManagerProvider.updateUserSession(session, jwtToken.getUserId(), jwtToken.getUser());
        } catch (InvalidJwtException ex) {
            ex.printStackTrace();
        }
    }

    private static void processLogoutMessage(Session session, WsMessageDto message) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        wsManagerProvider.removeUserSession(session);
    }

    private static void processChannelMessage(Session session, WsMessageDto message) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        String channelId = (String) message.getData();
        if (StringUtils.isBlank(channelId))
            return;

        wsManagerProvider.updateChannelSession(session, channelId);
    }

    private static void processRemoveChannelIdMessage(Session session) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        wsManagerProvider.removeChannelSession(session);
    }
}
