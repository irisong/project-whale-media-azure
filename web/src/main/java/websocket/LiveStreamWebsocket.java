package websocket;

import com.compalsolutions.compal.live.streaming.dto.LiveStreamActiveUserDto;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamAudience;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.util.ExceptionUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import rest.vo.GuildBO;
import websocket.serializer.VJLiveStreamDetailSerializer;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

@ApplicationScoped
@ServerEndpoint(value = "/wsLiveStream", configurator = DefaultConfigurator.class)
public class LiveStreamWebsocket {
    private static final Log log = LogFactory.getLog(LiveStreamWebsocket.class);

    private static HashMap<String, Session> userSessions = new HashMap<>();
    private static HashMap<String, List<Session>> channels = new HashMap<>();


    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        try {
            session.getUserProperties().put(WsConst.IP_ADDRESS, config.getUserProperties().get(WsConst.IP_ADDRESS));

            log.debug("Remote Address:" + config.getUserProperties().get(WsConst.IP_ADDRESS));
            log.debug("Connect: " + session.getUserProperties().get(WsConst.IP_ADDRESS));

            session.setMaxIdleTimeout(5 * 60000);
            session.getBasicRemote().sendText("Connection Established");

            addUserSession(session);

        } catch (IOException ex) {
            log.error(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        try {
            log.debug("Message: " + message);

            if (isJSONValid(message)) {
                JSONObject jsonObject = new JSONObject(message);
                String channelId = jsonObject.getString("channelId");

                log.debug("ChannelId: " + channelId);

                if (userSessions.containsKey(session.getId())) {
                    removeUserSession(session);
                    session.getUserProperties().put(WsConst.WEBSOCKET_CHANNEL_ID, channelId);
                    addUserSession(session);
                }
                addChannelSession(session, channelId);

            } else {
                log.debug("[onMesage JSON Object Error: ] " + message);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    @OnClose
    public void onClose(Session session) {
        try {
            processConnectionLost(session);
        } catch (Exception ex) {
//            log.debug(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
//        log.debug("Session Id: " + session.getId());
//        log.error("[Webscoket Error Session Id: ]" + session.getId() + ExceptionUtil.getExceptionStacktrace(error));

        try {
            processConnectionLost(session);
        } catch (Exception ex) {
//            log.debug(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    private void processConnectionLost(Session session) {
//        log.debug("[processConnectionLost]Session ID:" + session.getId());

        try {
            if (LiveStreamWebsocket.userSessions.containsKey(session.getId())) {
                removeUserSession(session);
            } else {
                log.debug("[processConnectionLost] Not Exist Session ID:" + session.getId());
            }

            log.debug("Channel Id: " + session.getUserProperties().get(WsConst.WEBSOCKET_CHANNEL_ID));
            String channelId = (String) session.getUserProperties().get(WsConst.WEBSOCKET_CHANNEL_ID);

            removeChannelSession(channelId, session);

        } catch (Exception ex) {
            log.error(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    public void sendLatestChannelInfo(String channelId, BigDecimal point, long view, List<LiveStreamActiveUserDto> memberList) {
        // Line Chart
        if (channels != null) {
            if (channels.containsKey(channelId)) {
                List<Session> sessionLists = channels.get(channelId);
                log.debug("[ Session List Size : " + sessionLists.size() + " ]");

                for (Session session : sessionLists) {
                    if (session.isOpen()) {
                        log.debug("[ Session Id: " + session.getId() + " ]");
                        log.debug("[ IP Address: " + session.getUserProperties().get(WsConst.IP_ADDRESS) + " ]");


                        LiveStreamDto liveStreamDto = new LiveStreamDto();
                        liveStreamDto.setChannelId(channelId);
                        liveStreamDto.setPoint(point);
                        liveStreamDto.setView(view);
                        liveStreamDto.setMemberList(memberList);

                        Gson gson = new GsonBuilder().registerTypeAdapter(LiveStreamDto.class, new VJLiveStreamDetailSerializer()).create();
                        String text = gson.toJson(liveStreamDto);

                        session.getAsyncRemote().sendText(text);

                    } else {
//                        log.debug("[Session Id Not Open Connection : " + session.getId() + " ]");

                        removeUserSession(session);
                        removeChannelSession(channelId, session);
                    }
                }
            } else {
                log.debug("ChannelId Not Exist");
            }
        } else {
            log.debug("Channel Id Map is empty");
        }
    }


    public boolean isJSONValid(String message) {
        try {
            new JSONObject(message);
        } catch (JSONException ex) {
            try {
                new JSONArray(message);
            } catch (JSONException ex1) {
                return false;
            }
        }

        return true;
    }

    private void addUserSession(Session session) {
        userSessions.put(session.getId(), session);
    }

    private void removeUserSession(Session session) {
        userSessions.remove(session.getId());
    }

    public void removeWholeChannel(String channelId) {
        if (channels.containsKey(channelId)) {
            channels.remove(channelId);
        } else {
            log.debug("[Error: Line Channel Id Not Exist]");
        }
    }


    private void removeChannelSession(String channelId, Session session) {
        if (channels.containsKey(channelId)) {
            List<Session> sessionList = channels.get(channelId);
            sessionList.remove(session);
        } else {
//            log.debug("[Error: Line Channel Id Not Exist]");
        }
    }

    private void addChannelSession(Session session, String channelId) {
        if (channels.containsKey(channelId)) {
            List<Session> sessionList = channels.get(channelId);
            sessionList.add(session);
        } else {
            List<Session> sessionList = new ArrayList<Session>();
            sessionList.add(session);
            channels.put(channelId, sessionList);
        }
    }

    /**
     * Random Generate Number
     *
     * @param min
     * @param max
     * @return
     */
    public static int generateRandomInteger(int min, int max) {
        int randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
        return randomNum;
    }

}
