package websocket;

public class WsConst {
    public static final String IP_ADDRESS = "IP_ADDRESS";

    public static final String WEBSOCKET_CHANNEL_ID = "channel_id";

    public class MessageType {
        public static final String VJ_CHANNEL = "VJ_CHANNEL";
        public static final String VJ_CHANNEL_REMOVE = "VJ_CHANNEL_REMOVE";
        public static final String CONNECTED = "CONNECTED";
        public static final String JWT_TOKEN = "JWT_TOKEN";
        public static final String LOGOUT = "LOGOUT";
    }

}
