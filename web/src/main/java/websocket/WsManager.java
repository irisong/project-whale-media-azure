package websocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.websocket.Session;

import com.compalsolutions.compal.live.streaming.dto.LiveStreamActiveUserDto;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamBroadCasterDto;
import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import org.apache.commons.lang3.StringUtils;

import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.MrmAdminUserType;
import com.compalsolutions.compal.struts.MrmAgentUserType;
import com.compalsolutions.compal.struts.MrmMemberUserType;
import com.compalsolutions.compal.user.vo.AdminUser;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.web.WsLoginInfo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class WsManager {
    private static WsManager ourInstance = new WsManager();
    private static final Log log = LogFactory.getLog(WsInComingUtil.class);
    private static final String USER_ID = "USER_ID";
    private static final String LOGIN_INFO = "LOGIN_INFO";
    private static final String CHANNEL_ID = "CHANNEL_ID";

    private Map<String, Session> sessions = new HashMap<>();
    private Map<String, Map<String, Session>> channelSessions = new HashMap<>();
    private Map<String, Map<String, Session>> userSessions = new HashMap<>();
    private Map<String, List<LiveStreamActiveUserDto>> memberListSessions = new HashMap<>();
    private Map<String, Integer> fansCount = new HashMap<>();
    private Map<String, Integer> followerCount = new HashMap<>();
    private Map<String, LiveStreamBroadCasterDto> broadCasters = new HashMap<>();

    private WsManager() {

    }

    public static WsManager getInstance() {
        return ourInstance;
    }

    public void addSession(Session session) {
        sessions.put(session.getId(), session);
    }

    public boolean containsSession(String sessionId) {
        return sessions.containsKey(sessionId);
    }

    public boolean containsSession(Session session) {
        return containsSession(session.getId());
    }

    public Map<String, Session> getSessions() {
        return sessions;
    }

    public void setSessions(Map<String, Session> sessions) {
        this.sessions = sessions;
    }

    public void removeSession(Session session) {
        String sessionId = session.getId();
        String userId = getUserId(session);
        String channelId = getChannelId(session);
        sessions.remove(sessionId);

        _removeFromUserSession(userId, sessionId);
        _removeFromChannelSession(channelId, sessionId);
        _removeFromMemberListSession(channelId);
        _removeFromFansCountSession(channelId);
        _removeFromFollowerCountSession(channelId);
        _removeFromBroadCasterSession(channelId);

        log.debug("Channel Session Size: "+channelSessions.size() + " "+followerCount.size() + " "+memberListSessions.size() + " "+broadCasters.size());
    }

    public void removeUserSession(Session session) {
        String userId = getUserId(session);
        String sessionId = session.getId();

        _removeFromUserSession(userId, sessionId);
    }

    private void _removeFromUserSession(String userId, String sessionId) {
        if (StringUtils.isNotBlank(userId) && userSessions.containsKey(userId)) {
            Map<String, Session> sessionMap = userSessions.get(userId);
            sessionMap.remove(sessionId);

            if (sessionMap.isEmpty())
                userSessions.remove(userId);
        }
    }

    public void removeChannelSession(Session session) {
        String channelId = getChannelId(session);
        String sessionId = session.getId();

        _removeFromChannelSession(channelId, sessionId);
    }

    private void _removeFromChannelSession(String channelId, String sessionId) {
        if (StringUtils.isNotBlank(channelId) && channelSessions.containsKey(channelId)) {
            Map<String, Session> sessionMap = channelSessions.get(channelId);
            sessionMap.remove(sessionId);
            if (sessionMap.isEmpty())
                channelSessions.remove(channelId);
        }
    }

    private void _removeFromMemberListSession(String channelId) {
        if (StringUtils.isNotBlank(channelId) && memberListSessions.containsKey(channelId)) {
            memberListSessions.remove(channelId);
        }
    }

    private void _removeFromBroadCasterSession(String channelId) {
        if (StringUtils.isNotBlank(channelId) && broadCasters.containsKey(channelId)) {
            broadCasters.remove(channelId);
        }
    }

    private void _removeFromFansCountSession(String channelId) {
        if (StringUtils.isNotBlank(channelId) && fansCount.containsKey(channelId)) {
            fansCount.remove(channelId);
        }
    }

    private void _removeFromFollowerCountSession(String channelId) {
        if (StringUtils.isNotBlank(channelId) && followerCount.containsKey(channelId)) {
            followerCount.remove(channelId);
        }
    }

    public void updateUserSession(Session session, String userId, User user) {
        String oldUserId = getUserId(session);

        if (StringUtils.equals(userId, oldUserId))
            return;

        if (StringUtils.isNotBlank(oldUserId)) {
            if (userSessions.containsKey(oldUserId)) {
                userSessions.get(oldUserId).remove(session.getId());
            }
        }

        setUserId(session, userId);

        Map<String, Session> sessionMap = null;
        if (userSessions.containsKey(userId)) {
            sessionMap = userSessions.get(userId);
        } else {
            sessionMap = new HashMap<>();
            userSessions.put(userId, sessionMap);
        }

        sessionMap.put(session.getId(), session);

        WsLoginInfo loginInfo = new WsLoginInfo();
        loginInfo.setUserId(user.getUserId());
        loginInfo.setUser(user);
        loginInfo.setLocale(null);
        loginInfo.setSessionId(session.getId());

        if (user instanceof AdminUser) {
            loginInfo.setUserType(new MrmAdminUserType());
        } else if (user instanceof AgentUser) {
            loginInfo.setUserType(new MrmAgentUserType());
        } else if (user instanceof MemberUser) {
            loginInfo.setUserType(new MrmMemberUserType());
        } else
            throw new SystemErrorException("Invalid user type");
        setLoginInfo(session, loginInfo);
    }

    public void updateMemberListSession(String channelId, LiveStreamDto liveStreamDto){
        getMemberListByChannelId(channelId);
    }

    public void updateChannelSession(Session session, String channelId) {
        String oldChannelId = getChannelId(session);

        if (StringUtils.isNotBlank(oldChannelId)) {
            if (channelSessions.containsKey(oldChannelId)) {
                channelSessions.get(oldChannelId).remove(session.getId());
            }
        }
        log.debug("UpdateChannelSession: " + session.getId() + " >> channelId: " + channelId + " ");
        setChannelId(session, channelId);

        Map<String, Session> sessionMap = null;
        if (channelSessions.containsKey(channelId)) {
            sessionMap = channelSessions.get(channelId);
        } else {
            sessionMap = new HashMap<>();
            channelSessions.put(channelId, sessionMap);
        }

        sessionMap.put(session.getId(), session);
    }

    // ---------------- GETTER & SETTER (START) -------------

    String getUserId(Session session) {
        return (String) session.getUserProperties().get(USER_ID);
    }

    private void setUserId(Session session, String userId) {
        session.getUserProperties().put(USER_ID, userId);
    }

    String getChannelId(Session session) {
        return (String) session.getUserProperties().get(CHANNEL_ID);
    }

    private void setChannelId(Session session, String channelId) {
        session.getUserProperties().put(CHANNEL_ID, channelId);
    }

    public List<Session> getSessionsByUserId(String userId) {
        if (userSessions.containsKey(userId)) {
            return new ArrayList<>(userSessions.get(userId).values());
        } else {
            return new ArrayList<>();
        }
    }
    public void setMemberListByChannelId(String channelId, List<LiveStreamActiveUserDto> liveStreamActiveUserDto) {
        memberListSessions.put(channelId, liveStreamActiveUserDto);
    }

    public List<LiveStreamActiveUserDto> getMemberListByChannelId(String channelId) {
        if (memberListSessions.containsKey(channelId)) {
            return memberListSessions.get(channelId);
        } else {
            return null;
        }
    }

    public void setFansCountByChannelId(String channelId, int count) {
        fansCount.put(channelId, count);
    }

    public int getFansCountByChannelId(String channelId){
        if (fansCount.containsKey(channelId)) {
            return fansCount.get(channelId);
        } else {
            return 0;
        }
    }

    public void setFollowerCountByChannelId(String channelId, int count) {
        followerCount.put(channelId, count);
    }

    public int getFollowerCountByChannelId(String channelId) {
        if (followerCount.containsKey(channelId)) {
            return followerCount.get(channelId);
        } else {
            return 0;
        }
    }

    private void setLoginInfo(Session session, LoginInfo loginInfo) {
        session.getUserProperties().put(LOGIN_INFO, loginInfo);
    }

    public List<Session> getSessionsByChannelId(String channelId) {
        if (channelSessions.containsKey(channelId)) {
            return new ArrayList<>(channelSessions.get(channelId).values());
        } else {
            return new ArrayList<>();
        }
    }

    public List<String> getAllChannelIds() {
        return new ArrayList<>(channelSessions.keySet());
    }

    public LiveStreamBroadCasterDto getBroadCasterListByChannelId(String channelId) {
        if (broadCasters.containsKey(channelId)) {
            return broadCasters.get(channelId);
        } else {
            return null;
        }
    }

    public void setBroadCasters(String channelId, LiveStreamBroadCasterDto liveStreamBroadCasterDto) {
        broadCasters.put(channelId, liveStreamBroadCasterDto);
    }

    // ---------------- GETTER & SETTER (END) -------------
}
