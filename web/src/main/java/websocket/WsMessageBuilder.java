package websocket;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.json.JSONException;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.web.JsonWriter;

public class WsMessageBuilder {
    public static final String DEFAULT_INCLUDE_PROPERTIES = Global.JsonInclude.WebSocket;

    private Map<String, Object> valueMap;
    private Object root;

    private String includeProperties;
    private String excludeProperties;
    private boolean excludeNullProperties;

    private List<String> messages;

    private WsMessageBuilder() {
        valueMap = new HashMap<>();
        messages = new ArrayList<>();
    }

    public WsMessageBuilder setRoot(Object root) {
        this.root = root;
        return this;
    }

    public WsMessageBuilder setAttribute(String key, Object obj) {
        valueMap.put(key, obj);
        return this;
    }

    public WsMessageBuilder setIncludeProperties(String includeProperties) {
        this.includeProperties = includeProperties;
        return this;
    }

    public WsMessageBuilder setExcludeProperties(String excludeProperties) {
        this.excludeProperties = excludeProperties;
        return this;
    }

    public static WsMessageBuilder newInstance() {
        return new WsMessageBuilder();
    }

    private void doProcess() {
        if (CollectionUtil.isNotEmpty(messages)) {
            valueMap.put("message", messages.get(0));
        }

        if (StringUtils.isNotBlank(includeProperties)) {
            includeProperties = DEFAULT_INCLUDE_PROPERTIES + ", " + includeProperties;
        }

        if (StringUtils.isNotBlank(excludeProperties) && StringUtils.isBlank(includeProperties)) {
            includeProperties = DEFAULT_INCLUDE_PROPERTIES;
        }
    }

    public String createMessage() {
        doProcess();

        JsonWriter jsonWriter = new JsonWriter(root != null ? root : valueMap, includeProperties, excludeProperties, excludeNullProperties);
        try {
            return jsonWriter.write();
        } catch (JSONException ex) {
            // log.debug(ex.getMessage(), ex.fillInStackTrace());
            throw new SystemErrorException(ex.getMessage(), ex);
        }
    }
}
