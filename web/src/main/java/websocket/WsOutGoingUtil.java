package websocket;

import java.util.Arrays;
import java.util.List;

import javax.websocket.Session;

import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import com.compalsolutions.compal.util.ExceptionUtil;
import com.compalsolutions.compal.websocket.WsManagerProvider;
import com.compalsolutions.compal.websocket.vo.WsMessage;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import websocket.dto.WsMessageDto;

public class WsOutGoingUtil {
    private static final Log log = LogFactory.getLog(WsOutGoingUtil.class);

    private static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return mapper;
    }

    public static void sendConnected(Session session) {
        WsMessageDto message = new WsMessageDto();
        message.setType(WsConst.MessageType.CONNECTED);
        message.setMessage("Connection Established");

        String msg = WsMessageBuilder.newInstance().setRoot(message).createMessage();
        session.getAsyncRemote().sendText(msg);
    }

    /***enable this function if want to handle broadcast data in wsMessage
     * *
     */
    public static void broadcastMessage(WsMessage wsMessage) {
        switch (wsMessage.getType()) {
            case WsConst.MessageType.VJ_CHANNEL:
//                broadcastMessage_processVJChannelPoint(wsMessage);
                break;
            default:
                throw new UnsupportedOperationException("Message type " + wsMessage.getType() + " is not supported ");
        }
    }

    //    private static void broadcastMessage_processVJChannelPoint(WsMessage wsMessage) {
//        String channelId;
//        BigDecimal point;
//        Long view;
//        List<String> updatedParam;
//        List<LiveStreamActiveUserDto> memberList;
//
//        String jsonString = wsMessage.getData();
//
//        ObjectMapper mapper = getObjectMapper();
//        LiveStreamDto wsLiveStreamDto = null;
//        try {
//            wsLiveStreamDto = mapper.readValue(jsonString, LiveStreamDto.class);
//        } catch (IOException e) {
//            throw new ValidatorException(e.getMessage());
//        }
//
//        channelId = wsLiveStreamDto.getChannelId();
//        point = wsLiveStreamDto.getPoint();
//        view = wsLiveStreamDto.getView();
//        updatedParam = wsLiveStreamDto.getUpdatedParam();
//
//        broadcastMessage_processVJPoint(channelId, point, view);
//    }

    public static void broadcastMessage_processVJPoint(LiveStreamDto liveStreamDto) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        try {
            List<Session> sessions = wsManagerProvider.getSessionsByChannelId(liveStreamDto.getChannelId());
            log.debug(sessions.size() + " Sessions Sent to Channel ID " + liveStreamDto.getChannelId());
            for (Session session : sessions) {

                String msg = WsMessageBuilder.newInstance()
                        .setAttribute("data", liveStreamDto)
                        .setAttribute("type", WsConst.MessageType.VJ_CHANNEL)
                        .createMessage();

                synchronized (session) {
                    session.getBasicRemote().sendText(msg);
                 //   session.getAsyncRemote().sendText(msg);
                }

            }
        }catch (Exception ex) {
            log.error(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    private static List<String> getUserIdsByOwner(String ownerId, String ownerType) {
        MemberService memberService = Application.lookupBean(MemberService.class);

        switch (ownerType) {
            case Global.UserType.MEMBER:
                MemberUser memberUser = memberService.findMemberUserByMemberId(ownerId);
                return Arrays.asList(memberUser.getUserId());
            default:
                throw new ValidatorException("OwnerType " + ownerType + " is not supported");
        }
    }
}
