package websocket;

import javax.enterprise.context.ApplicationScoped;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.websocket.WsManagerProvider;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.compalsolutions.compal.util.ExceptionUtil;

@ApplicationScoped
@ServerEndpoint(value = "/default", configurator = DefaultConfigurator.class)
public class DefaultWebSocket {
    private static final Log log = LogFactory.getLog(DefaultWebSocket.class);
    private static final long MAX_IDLE_TIMEOUT = 5 * 60000;

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {

        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        try {
            session.getUserProperties().put(WsConst.IP_ADDRESS, config.getUserProperties().get(WsConst.IP_ADDRESS));

         //   log.debug("Connect: " + session.getUserProperties().get(WsConst.IP_ADDRESS));

            session.setMaxTextMessageBufferSize(512 * 1024);
            session.setMaxBinaryMessageBufferSize(512 * 1024);
            session.setMaxIdleTimeout(MAX_IDLE_TIMEOUT);
            WsOutGoingUtil.sendConnected(session);

            wsManagerProvider.addSession(session);

            log.debug("Session Added  Session ID: " + session.getId() + " Total Sessions: "+wsManagerProvider.getSessions().size());

        } catch (Exception ex) {
            log.error(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        try {
            WsInComingUtil.processMessage(session, message);
        } catch (Exception ex) {
            log.debug(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    @OnClose
    public void onClose(Session session) {
        log.debug("Web Socket onClose");
        try {
            processConnectionLost(session);
        } catch (Exception ex) {
            log.debug(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        log.debug("Web Socket onError :" + error.getMessage());
//        log.debug("onError >> Session Id: " + session.getId());
//        log.error("[Webscoket Error Session Id: ]" + session.getId() + " " + error.getMessage());
        try {
            processConnectionLost(session);
        } catch (Exception ex) {
            log.debug(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }

    private void processConnectionLost(Session session) {
        WsManagerProvider wsManagerProvider = Application.lookupBean(WsManagerProvider.BEAN_NAME, WsManagerProvider.class);
        try {
            if (wsManagerProvider.containsSession(session)) {
                wsManagerProvider.removeSession(session);
                log.debug("[processConnectionLost] Session ID Removed " + session.getId());
            } else {
                log.debug("[processConnectionLost] Not Exist Session ID:" + session.getId());
            }
            session.close();
            log.debug("Total Sessions Left: "+wsManagerProvider.getSessions().size());
        } catch (Exception ex) {
            log.error(ExceptionUtil.getExceptionStacktrace(ex));
        }
    }
}
