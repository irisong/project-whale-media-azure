package struts.remote;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.opensymphony.xwork2.ModelDriven;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class HelloWorldAction extends BaseAction implements ModelDriven<User> {
    private static final long serialVersionUID = 1L;

    private UserDetailsService userDetailsService;

    private User user = new User(true);

    public HelloWorldAction() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    @Action("/helloWorld")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this, user);
        try {
            if (StringUtils.isBlank(user.getUsername()))
                throw new ValidatorException("Username is invalid");

            user = userDetailsService.findUserByUsername(user.getUsername());
            if (user == null) {
                throw new ValidatorException(getText("errorMessage.invalid.username.password"));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Override
    public User getModel() {
        return user;
    }
}
