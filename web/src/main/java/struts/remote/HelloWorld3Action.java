package struts.remote;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", HelloWorld3Action.JSON_INCLUDE_PROPERTIES }) })
public class HelloWorld3Action extends BaseAction {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", " //
            + "user\\.username, "//
            + "user\\.member\\.memberId";

    private UserDetailsService userDetailsService;

    private User user = new User(true);

    @ToUpperCase
    @ToTrim
    private String username;

    public HelloWorld3Action() {
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
    }

    @Action("/helloWorld3")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        try {
            if (StringUtils.isBlank(username))
                throw new ValidatorException("Username is invalid");

            user = userDetailsService.findUserByUsername(username);
            if (user == null) {
                throw new ValidatorException(getText("errorMessage.invalid.username.password"));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
