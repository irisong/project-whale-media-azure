package struts.remote.bo;

import com.compalsolutions.compal.function.user.vo.User;

public class HelloWorldResult {
    private User user;
    private String message;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
