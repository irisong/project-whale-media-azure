package struts.member;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "memberSecondPasswordRequired"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT_EXTERNAL, location = "${url}") })
public class SecurityPasswordRequiredAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String transactionPassword;
    private String url;

    @Action(value = "/securityPasswordRequired")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) //
    })
    @Override
    public String execute() throws Exception {
        if (isSubmitData()) {
            try {
                boolean isSuccess = WebUtil.isMemberSecondPasswordValid(getLocale(), getLoginInfo(), transactionPassword);
                if (isSuccess) {
                    setSecondPasswordLoggedIn(true);
                    url = (String) session.remove(Global.CURRENT_URL);
                    return SUCCESS;
                } else {
                    setSecondPasswordLoggedIn(false);
                    addActionError(getText("login_password_is_not_valid"));
                }
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        return INPUT;
    }

    public String getUrl() {
        return url;
    }

    public String getTransactionPassword() {
        return transactionPassword;
    }

    public void setTransactionPassword(String transactionPassword) {
        this.transactionPassword = transactionPassword;
    }
}
