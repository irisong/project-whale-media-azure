package struts.member;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = "input", location = "memberMessage") //
})
public class SuccessMessageAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    @Action("/successMessage")
    @Override
    @EnableTemplate
    public String execute() throws Exception {
        return INPUT;
    }

    public void setSuccessMenuKey(Long successMenuKey) {
        this.successMenuKey = successMenuKey;
    }
}
