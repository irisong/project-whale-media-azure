package struts.member;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.jquery.validate.CheckValidate;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.DISPATCHER, location = "/jsp/misc/checkValid.jsp"), //
        @Result(name = BaseAction.INPUT, type = BaseAction.ResultType.DISPATCHER, location = "/jsp/misc/checkValid.jsp") //
})
public class VerifyTransactionPasswordAction extends BaseSecureAction implements CheckValidate {
    private static final long serialVersionUID = 1L;

    private String transactionPassword;
    private boolean valid = false;

    @Action(value = "/verifyTransactionPassword")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, createMode = true, readMode = true, updateMode = true, deleteMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) //
    })
    @Override
    public String execute() throws Exception {
        valid = WebUtil.isMemberSecondPasswordValid(getLocale(), getLoginInfo(), transactionPassword);

        return SUCCESS;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    public String getTransactionPassword() {
        return transactionPassword;
    }

    public void setTransactionPassword(String transactionPassword) {
        this.transactionPassword = transactionPassword;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }
}
