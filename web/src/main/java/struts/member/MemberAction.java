package struts.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "member")
})
public class MemberAction extends BaseSecureAction {
    @Action("/member")
    @EnableTemplate
    @Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String member() {
        return INPUT;
    }
}
