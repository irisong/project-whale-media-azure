package struts.member;

import java.util.Date;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "summary") })
public class SummaryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;
    private WalletService walletService;

    private Member member = new Member(true);
    private Date lastLogin;
    private double totalCp1;
    private double totalCp2;
    private double totalCp3;
    private double totalRt;

    public SummaryAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @Accesses(access = { @Access(accessCode = AP.MEMBER, readMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) //
    })
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        String memberId = WebUtil.getLoginMemberId(loginInfo);
        member = memberService.getMember(memberId);
        lastLogin = loginInfo.getUser().getLastLoginDatetime();

        totalCp1 = walletService.getWalletBalance(memberId, Global.UserType.MEMBER, 10).doubleValue();
        totalCp2 = walletService.getWalletBalance(memberId, Global.UserType.MEMBER, 20).doubleValue();
        totalCp3 = walletService.getWalletBalance(memberId, Global.UserType.MEMBER, 30).doubleValue();

        return INPUT;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    @TypeConversion(converter = FrameworkConst.STRUTS_DATETIME_CONVERTER)
    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    @TypeConversion(converter = FrameworkConst.STRUTS_DECIMAL_CONVERTER)
    public double getTotalCp1() {
        return totalCp1;
    }

    public void setTotalCp1(double totalCp1) {
        this.totalCp1 = totalCp1;
    }

    @TypeConversion(converter = FrameworkConst.STRUTS_DECIMAL_CONVERTER)
    public double getTotalCp2() {
        return totalCp2;
    }

    public void setTotalCp2(double totalCp2) {
        this.totalCp2 = totalCp2;
    }

    @TypeConversion(converter = FrameworkConst.STRUTS_DECIMAL_CONVERTER)
    public double getTotalCp3() {
        return totalCp3;
    }

    public void setTotalCp3(double totalCp3) {
        this.totalCp3 = totalCp3;
    }

    @TypeConversion(converter = FrameworkConst.STRUTS_DECIMAL_CONVERTER)
    public double getTotalRt() {
        return totalRt;
    }

    public void setTotalRt(double totalRt) {
        this.totalRt = totalRt;
    }
}
