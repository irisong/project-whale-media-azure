package struts.member;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.FrameworkConst;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.DevelopmentException;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.member.vo.BaseNode;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.TreeConstant;
import com.compalsolutions.compal.security.SecurityUtil;
import com.compalsolutions.compal.security.UserDetails;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.SecureAware;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.opensymphony.xwork2.conversion.annotations.TypeConversion;

import struts.pub.SelfRegisterAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "memberRegistration"), //
        @Result(name = BaseAction.PAGE2, location = "memberRegistration2"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "successMessage", "namespace", "/member",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }) //
})
public class MemberRegistrationAction extends SelfRegisterAction implements SecureAware {
    private static final long serialVersionUID = 1L;

    private WalletService walletService;

    private double totalCp1;
    private Long pid;
    @ToTrim
    private String productCode;
    private boolean privateInvestmentAgreement;

    @ToTrim
    private String placementType;
    @ToTrim
    @ToUpperCase
    private String position1;
    @ToTrim
    @ToUpperCase
    private String position2;
    @ToTrim
    @ToUpperCase
    private String placementDistId;
    @ToTrim
    @ToUpperCase
    private String placementDistName;

    private List<OptionBean> placementTypes = new ArrayList<>();
    private List<OptionBean> autoPositionTypes = new ArrayList<>();
    private List<OptionBean> manualPositionTypes = new ArrayList<>();

    public MemberRegistrationAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @Action("/memberRegistration")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, createMode = true, readMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) //
    })
    @Override
    public String execute() throws Exception {
        String memberId = WebUtil.getLoginMemberId(getLoginInfo());

        // bonusPackageGroups = bonusService.findBonusPackagesGroupByPackageName(true);
        totalCp1 = walletService.getWalletBalance(memberId, Global.UserType.MEMBER, 10).doubleValue();

        return INPUT;
    }

    @Action("/memberRegistration2")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, createMode = true, readMode = true, adminMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) //
    })
    public String memberRegistration2() throws Exception {
        /****************************************
         * VALIDATION START
         ****************************************/
        if (pid == null) {
            addActionError(getText("invalid_join_package"));
            return execute();
        }


        /****************************************
         * VALIDATION END
         ****************************************/

        // initialise form value
        initForm();

        placementTypes.add(new OptionBean("1", getText("auto")));
        placementTypes.add(new OptionBean("0", getText("manual")));

        autoPositionTypes.add(new OptionBean("L", getText("auto_left")));
        autoPositionTypes.add(new OptionBean("R", getText("auto_right")));

        manualPositionTypes.add(new OptionBean("L", getText("left")));
        manualPositionTypes.add(new OptionBean("R", getText("right")));

        if (!isSubmitData()) {
            placementType = "1";
            position1 = "L";
            position2 = "L";
        } else {
            try {
                String memberId = WebUtil.getLoginMemberId(getLoginInfo());
                VoUtil.toTrimUpperCaseProperties(this, member, memberDetail);
                BaseNode placement = new BaseNode(false);

                // if auto placement
                if ("1".equals(placementType)) {
                    placement.setParentId(TreeConstant.AUTO_PLACEMENT);
                    placement.setPosition(position1);
                    placement.setParentUnit(1);
                } else {
                    Member placementMember = memberService.findMemberByMemberCode(placementDistId);
                    if (placementMember != null)
                        placement.setParentId(placementMember.getMemberId());
                    placement.setPosition(position2);
                    placement.setParentUnit(1);
                }

                member.setMemberCode(username);
                successMessage = getText("member_registered_successfully", new String[] { member.getMemberCode() });

                return SUCCESS;
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        return PAGE2;
    }

    /****************************************
     * IMPLEMENTING SecureAware - start
     ****************************************/

    @Override
    public User getLoginUser() {
        UserDetails details = SecurityUtil.getLoginUser();
        if (details instanceof User)
            return (User) details;
        else if (details != null)
            throw new DevelopmentException("The loginUser object is exists but not instanceof " + User.class.getName());
        else
            return null;
    }

    @Override
    public LoginInfo getLoginInfo() {
        return SecurityUtil.getLoginInfo();
    }

    /****************************************
     * IMPLEMENTING SecureAware - end
     ****************************************/

    @TypeConversion(converter = FrameworkConst.STRUTS_DECIMAL_CONVERTER)
    public double getTotalCp1() {
        return totalCp1;
    }

    public void setTotalCp1(double totalCp1) {
        this.totalCp1 = totalCp1;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public boolean isPrivateInvestmentAgreement() {
        return privateInvestmentAgreement;
    }

    public void setPrivateInvestmentAgreement(boolean privateInvestmentAgreement) {
        this.privateInvestmentAgreement = privateInvestmentAgreement;
    }

    public List<OptionBean> getPlacementTypes() {
        return placementTypes;
    }

    public void setPlacementTypes(List<OptionBean> placementTypes) {
        this.placementTypes = placementTypes;
    }

    public String getPlacementType() {
        return placementType;
    }

    public void setPlacementType(String placementType) {
        this.placementType = placementType;
    }

    public String getPlacementDistId() {
        return placementDistId;
    }

    public void setPlacementDistId(String placementDistId) {
        this.placementDistId = placementDistId;
    }

    public List<OptionBean> getAutoPositionTypes() {
        return autoPositionTypes;
    }

    public void setAutoPositionTypes(List<OptionBean> autoPositionTypes) {
        this.autoPositionTypes = autoPositionTypes;
    }

    public String getPosition1() {
        return position1;
    }

    public void setPosition1(String position1) {
        this.position1 = position1;
    }

    public String getPlacementDistName() {
        return placementDistName;
    }

    public void setPlacementDistName(String placementDistName) {
        this.placementDistName = placementDistName;
    }

    public List<OptionBean> getManualPositionTypes() {
        return manualPositionTypes;
    }

    public void setManualPositionTypes(List<OptionBean> manualPositionTypes) {
        this.manualPositionTypes = manualPositionTypes;
    }

    public String getPosition2() {
        return position2;
    }

    public void setPosition2(String position2) {
        this.position2 = position2;
    }

}
