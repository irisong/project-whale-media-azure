package struts.app.wallet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletExchange;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "walletWithdraw2CryptoList"), //
        @Result(name = BaseAction.INPUT, location = "walletWithdraw2CryptoList"), //
        @Result(name = BaseAction.SHOW, location = "walletWithdraw2CryptoView"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", Global.JsonInclude.Default }) //
})
public class WalletWithdraw2CryptoAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_WALLET_WITHDRAW2CRYPTO_LIST = "walletWithdraw2CryptoList";

    private MemberService memberService;
    private WalletService walletService;

    private WalletExchange walletExchange = new WalletExchange(true);
    private List<OptionBean> allStatusList = new ArrayList<>();

    @ToTrim
    @ToUpperCase
    private String memberCode;

    @ToTrim
    @ToUpperCase
    private String docno;

    private Date dateFrom;
    private Date dateTo;

    private Date processDateFrom;
    private Date processDateTo;

    private boolean clear;

    private String exchangeIds;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String remark;

    public WalletWithdraw2CryptoAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allStatusList.addAll(optionBeanUtil.getWalletWithdraw2CryptoStatusWithOptionAll());
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_WITHDRAW2CRYPTO })
    @Action(value = "/walletWithdraw2CryptoList")
    @Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        init();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_WALLET_WITHDRAW2CRYPTO_LIST);
        } else if (session.containsKey(SESSION_WALLET_WITHDRAW2CRYPTO_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_WALLET_WITHDRAW2CRYPTO_LIST);
        } else {
            session.put(SESSION_WALLET_WITHDRAW2CRYPTO_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("memberCode", memberCode);
            objectMap.put("docno", docno);
            objectMap.put("dateFrom", dateFrom);
            objectMap.put("dateTo", dateTo);
            objectMap.put("processDateFrom", processDateFrom);
            objectMap.put("processDateTo", processDateTo);
            objectMap.put("status", status);
        } else {
            memberCode = (String) objectMap.get("memberCode");
            docno = (String) objectMap.get("docno");
            dateFrom = (Date) objectMap.get("dateFrom");
            dateTo = (Date) objectMap.get("dateTo");
            processDateFrom = (Date) objectMap.get("processDateFrom");
            processDateTo = (Date) objectMap.get("processDateTo");
            status = (String) objectMap.get("status");
        }

        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_WITHDRAW2CRYPTO })
    @Action(value = "/walletWithdraw2CryptoView")
    @Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String view() {
        try {
            walletExchange = walletService.getWalletExchangeWithOwner(walletExchange.getExchangeId());
            if (walletExchange == null) {
                throw new Exception(getText("invalidWalletWithdrawal"));
            }

            if (WebUtil.isMember(getLoginInfo())) {
                String memberId = WebUtil.getLoginMemberId(getLoginInfo());
                if (!walletExchange.getOwnerId().equalsIgnoreCase(memberId)) {
                    throw new Exception(getText("invalidWalletWithdrawal"));
                }
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return SHOW;
    }

    @Action(value = "/walletWithdraw2CryptoApproveReject")
    @Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String approveReject() {
        try {
            walletService.doApproveRejectWalletWithdraw2Crypto(getLocale(), walletExchange.getExchangeId(), status, remark, getLoginInfo().getUserId());
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/walletWithdraw2CryptoBulkApproveReject")
    @Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String bulkApproveReject() {
        try {
            String[] ss = StringUtils.split(exchangeIds, ",");
            for (int i = 0; i < ss.length; i++) {
                ss[i] = StringUtils.trim(ss[i]);
            }

            walletService.doBulkApproveRejectWalletWithdraw2Crypto(getLocale(), Arrays.asList(ss), status, remark, getLoginInfo().getUserId());
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public WalletExchange getWalletExchange() {
        return walletExchange;
    }

    public void setWalletExchange(WalletExchange walletExchange) {
        this.walletExchange = walletExchange;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getProcessDateFrom() {
        return processDateFrom;
    }

    public void setProcessDateFrom(Date processDateFrom) {
        this.processDateFrom = processDateFrom;
    }

    public Date getProcessDateTo() {
        return processDateTo;
    }

    public void setProcessDateTo(Date processDateTo) {
        this.processDateTo = processDateTo;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getExchangeIds() {
        return exchangeIds;
    }

    public void setExchangeIds(String exchangeIds) {
        this.exchangeIds = exchangeIds;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
