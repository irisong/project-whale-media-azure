package struts.app.wallet;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.wallet.vo.WalletBalance;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "walletList"), //
})
public class WalletAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;
    protected WalletService walletService;

    protected Member member = new Member(true);
    private MemberDetail memberDetail = new MemberDetail(true);

    private List<OptionBean> walletTypes = new ArrayList<>();
    private List<Integer> activeWalletTypes = new ArrayList<>();
    private List<BigDecimal> walletBalances = new ArrayList<>();
    private List<OptionBean> showDescOptions = new ArrayList<>();
    private List<OptionBean> showRemarkOptions = new ArrayList<>();

    private Date dateFrom;
    private Date dateTo;
    protected Integer walletType;

    @ToTrim
    @ToUpperCase
    private Boolean showDescEn;
    private Boolean showRemark;

    @ToTrim
    @ToUpperCase
    private String memberCode;
    private boolean memberExist = false;

    public WalletAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());

        activeWalletTypes = WebUtil.getActiveMemberWalletTypes();

        for (Integer type : activeWalletTypes) {
            walletTypes.add(new OptionBean(String.valueOf(type), WebUtil.getWalletCurrencyWithName(getLocale(), type)));
        }

        showDescOptions = optionBeanUtil.getWalletStatementShowDescOptions();
        showRemarkOptions.add(new OptionBean("true", getText("yes")));
        showRemarkOptions.add(new OptionBean("false", getText("no")));
    }

    private void populateMember() {
        LoginInfo loginInfo = getLoginInfo();

        if (WebUtil.isAdmin(loginInfo)) {
            if (StringUtils.isNotBlank(memberCode)) {
                member = memberService.findMemberByMemberCode(memberCode);

                if (member == null) {
                    addActionError(getText("invalidMember"));
                } else {
                    memberDetail = member.getMemberDetail();
                    memberExist = true;
                }
            }
        } else if (WebUtil.isMember(loginInfo)) {
            member = WebUtil.getLoginMember(loginInfo);
            memberExist = true;
        }
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_WALLET})
    @Action(value = "/walletList")
    @Access(accessCode = AP.WALLET, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);
        populateMember();

        if(showDescEn == null)
            showDescEn = true;

        if(showRemark == null)
            showRemark = false;

        if (memberExist) {
            walletBalances = activeWalletTypes.stream().map( //
                    walletType -> {
                        // walletService.getMemberWalletBalance(member.getMemberId(), walletType)
                        WalletBalance walletBalance = walletService.findMemberWalletBalance(member.getMemberId(), walletType);
                        return walletBalance != null ? walletBalance.getAvailableBalance() : BigDecimal.ZERO;
                    }
            ).collect(Collectors.toList());
        }
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public boolean isMemberExist() {
        return memberExist;
    }

    public void setMemberExist(boolean memberExist) {
        this.memberExist = memberExist;
    }

    public List<OptionBean> getWalletTypes() {
        return walletTypes;
    }

    public void setWalletTypes(List<OptionBean> walletTypes) {
        this.walletTypes = walletTypes;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    public List<BigDecimal> getWalletBalances() {
        return walletBalances;
    }

    public void setWalletBalances(List<BigDecimal> walletBalances) {
        this.walletBalances = walletBalances;
    }

    public Boolean getShowDescEn() {
        return showDescEn;
    }

    public void setShowDescEn(Boolean showDescEn) {
        this.showDescEn = showDescEn;
    }

    public Boolean getShowRemark() {
        return showRemark;
    }

    public void setShowRemark(Boolean showRemark) {
        this.showRemark = showRemark;
    }

    public List<OptionBean> getShowDescOptions() {
        return showDescOptions;
    }

    public void setShowDescOptions(List<OptionBean> showDescOptions) {
        this.showDescOptions = showDescOptions;
    }

    public List<OptionBean> getShowRemarkOptions() {
        return showRemarkOptions;
    }

    public void setShowRemarkOptions(List<OptionBean> showRemarkOptions) {
        this.showRemarkOptions = showRemarkOptions;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
