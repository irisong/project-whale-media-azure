package struts.app.wallet;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletAdjust;

import java.util.Date;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                WalletAdjustListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletAdjustListDatagridAction extends BaseDatagridAction<WalletAdjust> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", "
            + "rows\\[\\d+\\]\\.adjustId, "
            + "rows\\[\\d+\\]\\.docno, "
            + "rows\\[\\d+\\]\\.ownerId, "
            + "rows\\[\\d+\\]\\.ownerType, "
            + "rows\\[\\d+\\]\\.walletType, "
            + "rows\\[\\d+\\]\\.adjustType, "
            + "rows\\[\\d+\\]\\.amount, "
            + "rows\\[\\d+\\]\\.trxDatetime, "
            + "rows\\[\\d+\\]\\.ownerName,"
            + "rows\\[\\d+\\]\\.ownerUsername,"
            + "rows\\[\\d+\\]\\.remark";

    private WalletService walletService;

    @ToUpperCase
    @ToTrim
    private String docno;

    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String adjustType;

    private Integer walletType;

    private Date dateFrom;

    private Date dateTo;


    public WalletAdjustListDatagridAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @Action(value = "/walletAdjustListDatagrid")
    @Access(accessCode = AP.WALLET_ADJUST, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        walletService.findMemberWalletAdjustsForListing(getDatagridModel(), docno, memberCode, adjustType, walletType, dateFrom, dateTo);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getAdjustType() {
        return adjustType;
    }

    public void setAdjustType(String adjustType) {
        this.adjustType = adjustType;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
