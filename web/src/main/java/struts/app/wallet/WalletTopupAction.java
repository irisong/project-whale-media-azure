package struts.app.wallet;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTopup;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "walletTopupList"), //
        @Result(name = BaseAction.LIST, location = "walletTopupList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "appMessage", "namespace", "/app", "successMessage",
                "${successMessage}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class WalletTopupAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_WALLET_TOPUP_LIST = "walletTopupList";

    protected WalletService walletService;

    protected WalletTopup walletTopup = new WalletTopup(true);

    protected List<Integer> walletTypes = new ArrayList<>();
    private List<OptionBean> allStatusList = new ArrayList<>();
    private List<OptionBean> allWalletTypes = new ArrayList<>();

    @ToTrim
    @ToUpperCase
    private String memberCode;

    @ToTrim
    @ToUpperCase
    private String docno;

    private Date dateFrom;
    private Date dateTo;
    private Integer walletType;
    @ToTrim
    @ToUpperCase
    private String status;

    private boolean clear;

    public WalletTopupAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_WALLET_TOP_UP)
    @Access(accessCode = AP.MEMBER, createMode = true, adminMode = true)
    @Action("/walletTopupList")
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());

        walletTypes = Global.WalletTopupConf.walletTypes;

        allWalletTypes.add(optionBeanUtil.generateOptionAll());
        for (Integer walletType : walletTypes) {
            allWalletTypes.add(new OptionBean(String.valueOf(walletType), WebUtil.getWalletCurrencyWithName(getLocale(), walletType)));
        }

        allStatusList = optionBeanUtil.getWalletTopupStatusWithOptionAll(getLocale());

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_WALLET_TOPUP_LIST);
        } else if (session.containsKey(SESSION_WALLET_TOPUP_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_WALLET_TOPUP_LIST);
        } else {
            session.put(SESSION_WALLET_TOPUP_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("memberCode", memberCode);
            objectMap.put("docno", docno);
            objectMap.put("dateFrom", dateFrom);
            objectMap.put("dateTo", dateTo);
            objectMap.put("walletType", walletType);
            objectMap.put("status", status);
        } else {
            memberCode = (String) objectMap.get("memberCode");
            docno = (String) objectMap.get("docno");
            dateFrom = (Date) objectMap.get("dateFrom");
            dateTo = (Date) objectMap.get("dateTo");
            walletType = (Integer) objectMap.get("walletType");
            status = (String) objectMap.get("status");
        }

        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public WalletTopup getWalletTopup() {
        return walletTopup;
    }

    public void setWalletTopup(WalletTopup walletTopup) {
        this.walletTopup = walletTopup;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<OptionBean> getAllWalletTypes() {
        return allWalletTypes;
    }

    public void setAllWalletTypes(List<OptionBean> allWalletTypes) {
        this.allWalletTypes = allWalletTypes;
    }

    public List<Integer> getWalletTypes() {
        return walletTypes;
    }

    public void setWalletTypes(List<Integer> walletTypes) {
        this.walletTypes = walletTypes;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
