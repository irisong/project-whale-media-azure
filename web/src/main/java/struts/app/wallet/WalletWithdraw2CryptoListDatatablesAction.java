package struts.app.wallet;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletExchange;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                WalletWithdraw2CryptoListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletWithdraw2CryptoListDatatablesAction extends BaseSecureDatatablesAction<WalletExchange> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.exchangeId, " //
            + "data\\[\\d+\\]\\.trxDatetime, " //
            + "data\\[\\d+\\]\\.processDatetime, " //
            + "data\\[\\d+\\]\\.docno, " //
            + "data\\[\\d+\\]\\.ownerCode, " //
            + "data\\[\\d+\\]\\.ownerName, " //
            + "data\\[\\d+\\]\\.withdrawType, " //
            + "data\\[\\d+\\]\\.withdrawTypeDesc, " //
            + "data\\[\\d+\\]\\.amount, " //
            + "data\\[\\d+\\]\\.rate, " //
            + "data\\[\\d+\\]\\.adminFee, " //
            + "data\\[\\d+\\]\\.amountTo, " //
            + "data\\[\\d+\\]\\.totalAmount, " //
            + "data\\[\\d+\\]\\.cryptoTypeFrom, " //
            + "data\\[\\d+\\]\\.cryptoTypeTo, " //
            + "data\\[\\d+\\]\\.totalAmount, " //
            + "data\\[\\d+\\]\\.adminFee, " //
            + "data\\[\\d+\\]\\.statusDesc, " //
            + "data\\[\\d+\\]\\.status ";

    private WalletService walletService;

    @ToTrim
    @ToUpperCase
    private String memberCode;

    @ToTrim
    @ToUpperCase
    private String docno;

    private Date dateFrom;
    private Date dateTo;

    private Date processDateFrom;
    private Date processDateTo;

    @ToTrim
    @ToUpperCase
    private String withdrawType;

    @ToTrim
    @ToUpperCase
    private String status;

    public WalletWithdraw2CryptoListDatatablesAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);

        // implicitly set datagrid to SqlDatagridModel
        setDatagridModel(new SqlDatagridModel<>(new ORWrapper(new WalletExchange(), "ww")));
    }

    @Action(value = "/walletWithdraw2CryptoListDatatables")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        walletService.findWalletExchanges2CryptoForListing(getLocale(), getDatagridModel(), memberCode, docno, dateFrom, dateTo, processDateFrom, processDateTo,
                status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getProcessDateFrom() {
        return processDateFrom;
    }

    public void setProcessDateFrom(Date processDateFrom) {
        this.processDateFrom = processDateFrom;
    }

    public Date getProcessDateTo() {
        return processDateTo;
    }

    public void setProcessDateTo(Date processDateTo) {
        this.processDateTo = processDateTo;
    }

    public String getWithdrawType() {
        return withdrawType;
    }

    public void setWithdrawType(String withdrawType) {
        this.withdrawType = withdrawType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
