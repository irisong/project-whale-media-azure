package struts.app.wallet;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTopupPackage;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "walletTopupPackageList"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", Global.JsonInclude.Default })
})
public class WalletTopupPackageAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private WalletService walletService;

    private WalletTopupPackage walletTopupPackage = new WalletTopupPackage(true);

    private List<OptionBean> walletTypes = new ArrayList<>();
    private List<OptionBean> allWalletTypes = new ArrayList<>();
    private List<OptionBean> allStatus = new ArrayList<>();

    public WalletTopupPackageAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_TOP_UP_PACKAGE })
    @Action(value = "/walletTopupPackageList")
    @Access(accessCode = AP.WALLET_TOP_UP_PACKAGE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        return LIST;
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());

        walletTypes = walletService.findWalletTypeForWalletAdjustment(getLocale());
        allWalletTypes.add(optionBeanUtil.generateOptionAll());
        allWalletTypes.addAll(walletTypes);

        allStatus.add(optionBeanUtil.generateOptionAll());
        allStatus.addAll(optionBeanUtil.getWalletTopupPackageStatus());
    }

    @Action("/walletTopupPackageSave")
    @Access(accessCode = AP.WALLET_TOP_UP_PACKAGE, createMode = true, adminMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(walletTopupPackage);

            walletTopupPackage.setFiatCurrency(Global.WalletType.USD);
            walletTopupPackage.setFiatCurrencyAmount(Double.parseDouble(String.valueOf(walletTopupPackage.getPackageAmount()))); // Currently 1 USD = 1 Whale Live Coin
            walletService.doSaveWalletTopupPackage(getLocale(), walletTopupPackage);

            successMessage = getText("successMessage.WalletTopupPackage.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getWalletTypes() {
        return walletTypes;
    }

    public void setWalletTypes(List<OptionBean> walletTypes) {
        this.walletTypes = walletTypes;
    }

    public List<OptionBean> getAllWalletTypes() {
        return allWalletTypes;
    }

    public void setAllWalletTypes(List<OptionBean> allWalletTypes) {
        this.allWalletTypes = allWalletTypes;
    }

    public List<OptionBean> getAllStatus() {
        return allStatus;
    }

    public void setAllStatus(List<OptionBean> allStatus) {
        this.allStatus = allStatus;
    }

    public WalletTopupPackage getWalletTopupPackage() {
        return walletTopupPackage;
    }

    public void setWalletTopupPackage(WalletTopupPackage walletTopupPackage) {
        this.walletTopupPackage = walletTopupPackage;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
