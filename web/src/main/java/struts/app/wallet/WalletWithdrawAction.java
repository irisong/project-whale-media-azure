package struts.app.wallet;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletWithdraw;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "walletWithdrawList"), //
        @Result(name = BaseAction.INPUT, location = "walletWithdrawList"), //
        @Result(name = BaseAction.SHOW, location = "walletWithdrawView"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", Global.JsonInclude.Default }) //
})
public class WalletWithdrawAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_WALLET_WITHDRAW_LIST = "walletWithdrawList";

    protected MemberService memberService;
    protected WalletService walletService;

    protected WalletWithdraw walletWithdraw = new WalletWithdraw(true);

    private List<OptionBean> allWithdrawTypes = new ArrayList<>();
    private List<OptionBean> allCryptoTypes = new ArrayList<>();
    private List<OptionBean> allStatusList = new ArrayList<>();

    @ToTrim
    @ToUpperCase
    private String memberCode;

    @ToTrim
    @ToUpperCase
    private String docno;

    private Date dateFrom;
    private Date dateTo;

    private Date processDateFrom;
    private Date processDateTo;

    @ToTrim
    @ToUpperCase
    private String withdrawType;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String cryptoType;

    @ToTrim
    @ToUpperCase
    private String remark;

    @ToTrim
    private String crytoAddressLabel;

    private boolean crytocurrency;

    private boolean clear;

    private String withdrawIds;

    public WalletWithdrawAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allWithdrawTypes.addAll(optionBeanUtil.getWithdrawTypesWithOptionAll());
        allStatusList.addAll(optionBeanUtil.getWalletWithdrawStatusWithOptionAll());
        allCryptoTypes.addAll(optionBeanUtil.getWalletWithdrawCryptoTypesWithOptionAll());
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_WITHDRAW })
    @Action(value = "/walletWithdrawList")
    @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        init();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_WALLET_WITHDRAW_LIST);
        } else if (session.containsKey(SESSION_WALLET_WITHDRAW_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_WALLET_WITHDRAW_LIST);
        } else {
            session.put(SESSION_WALLET_WITHDRAW_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("memberCode", memberCode);
            objectMap.put("docno", docno);
            objectMap.put("dateFrom", dateFrom);
            objectMap.put("dateTo", dateTo);
            objectMap.put("processDateFrom", processDateFrom);
            objectMap.put("processDateTo", processDateTo);
            objectMap.put("withdrawType", withdrawType);
            objectMap.put("status", status);
            objectMap.put("cryptoType", cryptoType);
        } else {
            memberCode = (String) objectMap.get("memberCode");
            docno = (String) objectMap.get("docno");
            dateFrom = (Date) objectMap.get("dateFrom");
            dateTo = (Date) objectMap.get("dateTo");
            processDateFrom = (Date) objectMap.get("processDateFrom");
            processDateTo = (Date) objectMap.get("processDateTo");
            withdrawType = (String) objectMap.get("withdrawType");
            status = (String) objectMap.get("status");
            cryptoType = (String) objectMap.get("cryptoType");
        }

        return LIST;
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_WITHDRAW })
    @Action(value = "/walletWithdrawView")
    @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String view() {
        try {
            walletWithdraw = walletService.getWalletWithdraw(walletWithdraw.getWithdrawId());

            if (walletWithdraw == null) {
                throw new Exception(getText("invalidWalletWithdrawal"));
            }

            if (WebUtil.isMember(getLoginInfo())) {
                String memberId = WebUtil.getLoginMemberId(getLoginInfo());
                if (!walletWithdraw.getOwnerId().equalsIgnoreCase(memberId)) {
                    throw new Exception(getText("invalidWalletWithdrawal"));
                }
            }

            crytocurrency = true;
            crytoAddressLabel = getText("coinAddress");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return SHOW;
    }

    @Action(value = "/walletWithdrawApproveReject")
    @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String approveReject() {
        try {
            walletService.doApproveRejectWalletWithdraw(getLocale(), walletWithdraw.getWithdrawId(), status, remark, getLoginInfo().getUserId());
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/walletWithdrawBulkApproveReject")
    @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String bulkApproveReject() {
        try {
            String[] ss = StringUtils.split(withdrawIds, ",");
            for (int i = 0; i < ss.length; i++) {
                ss[i] = StringUtils.trim(ss[i]);
            }

            walletService.doBulkApproveRejectWalletWithdraw(getLocale(), Arrays.asList(ss), status, remark, getLoginInfo().getUserId());
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getAllWithdrawTypes() {
        return allWithdrawTypes;
    }

    public void setAllWithdrawTypes(List<OptionBean> allWithdrawTypes) {
        this.allWithdrawTypes = allWithdrawTypes;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getProcessDateFrom() {
        return processDateFrom;
    }

    public void setProcessDateFrom(Date processDateFrom) {
        this.processDateFrom = processDateFrom;
    }

    public Date getProcessDateTo() {
        return processDateTo;
    }

    public void setProcessDateTo(Date processDateTo) {
        this.processDateTo = processDateTo;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getWithdrawType() {
        return withdrawType;
    }

    public void setWithdrawType(String withdrawType) {
        this.withdrawType = withdrawType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WalletWithdraw getWalletWithdraw() {
        return walletWithdraw;
    }

    public void setWalletWithdraw(WalletWithdraw walletWithdraw) {
        this.walletWithdraw = walletWithdraw;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getCrytoAddressLabel() {
        return crytoAddressLabel;
    }

    public void setCrytoAddressLabel(String crytoAddressLabel) {
        this.crytoAddressLabel = crytoAddressLabel;
    }

    public boolean isCrytocurrency() {
        return crytocurrency;
    }

    public void setCrytocurrency(boolean crytocurrency) {
        this.crytocurrency = crytocurrency;
    }

    public String getWithdrawIds() {
        return withdrawIds;
    }

    public void setWithdrawIds(String withdrawIds) {
        this.withdrawIds = withdrawIds;
    }

    public List<OptionBean> getAllCryptoTypes() {
        return allCryptoTypes;
    }

    public void setAllCryptoTypes(List<OptionBean> allCryptoTypes) {
        this.allCryptoTypes = allCryptoTypes;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
