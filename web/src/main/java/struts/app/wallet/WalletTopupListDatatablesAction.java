package struts.app.wallet;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTopup;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                WalletTopupListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletTopupListDatatablesAction extends BaseSecureDatatablesAction<WalletTopup> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.topupId, "//
            + "data\\[\\d+\\]\\.docno, " //
            + "data\\[\\d+\\]\\.ownerId, " //
            + "data\\[\\d+\\]\\.ownerType, " //
            + "data\\[\\d+\\]\\.walletType, " //
            + "data\\[\\d+\\]\\.amount, " //
            + "data\\[\\d+\\]\\.trxDatetime, " //
            + "data\\[\\d+\\]\\.status, " //
            + "data\\[\\d+\\]\\.crytocurrencyType, " //
            + "data\\[\\d+\\]\\.crytocurrencyAmount, " //
            + "data\\[\\d+\\]\\.crytocurrencyRate, " //
            + "data\\[\\d+\\]\\.ownerName," //
            + "data\\[\\d+\\]\\.ownerCode";

    private WalletService walletService;

    @ToTrim
    @ToUpperCase
    private String memberCode;

    @ToTrim
    @ToUpperCase
    private String docno;

    private Date dateFrom;
    private Date dateTo;
    private Integer walletType;
    @ToTrim
    @ToUpperCase
    private String status;

    public WalletTopupListDatatablesAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);

        // implicitly set datagrid to SqlDatagridModel
        setDatagridModel(new SqlDatagridModel<>(new ORWrapper(new WalletTopup(), "wt")));
    }

    @Action(value = "/walletTopupListDatatables")
    @Accesses(access = { @Access(accessCode = AP.WALLET_TOP_UP, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        walletService.findWalletTopupsForListing(getDatagridModel(), memberCode, docno, dateFrom, dateTo, walletType, status);

        System.out.println(getDatagridModel().getTotalRecords());
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getDocno() {
        return docno;
    }

    public void setDocno(String docno) {
        this.docno = docno;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
