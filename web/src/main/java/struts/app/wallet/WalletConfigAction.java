package struts.app.wallet;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletConfig;
import com.compalsolutions.compal.wallet.vo.WalletTransferConfig;
import com.compalsolutions.compal.wallet.vo.WalletTransferTypeConfig;
import com.compalsolutions.compal.wallet.vo.WalletTypeConfig;

@Results(value = { //
        @Result(name = BaseAction.SHOW, location = "walletConfigView"), @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default}), //
        @Result(name = WalletConfigAction.JSON_GET_WALLET_INFO, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                WalletConfigAction.JSON_GET_WALLET_INFO_INCLUDE_PROPERTIES}), //
        @Result(name = WalletConfigAction.JSON_GET_WALLET_TRANSFER_TYPE_CONFIG_INFO, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                WalletConfigAction.JSON_GET_WALLET_TRANSFER_TYPE_CONFIG_INFO_INCLUDE_PROPERTIES}), //
})
public class WalletConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String JSON_GET_WALLET_INFO = "json_wallet_info";
    public static final String JSON_GET_WALLET_INFO_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", " //
            + "walletCurrency, "//
            + "walletName, " //
            + "walletType, " + "status";

    public static final String JSON_GET_WALLET_TRANSFER_TYPE_CONFIG_INFO = "json_wallet_transfer_type_config_info";
    public static final String JSON_GET_WALLET_TRANSFER_TYPE_CONFIG_INFO_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", " //
            + "walletTransferTypeConfig\\.transferSelf, "//
            + "walletTransferTypeConfig\\.walletTypeFrom, "//
            + "walletTransferTypeConfig\\.walletTypeTo, "//
            + "walletTransferTypeConfig\\.status ";

    private WalletService walletService;

    private WalletConfig walletConfig = new WalletConfig(true);
    private WalletTransferConfig walletTransferConfig = new WalletTransferConfig(true);
    private WalletTransferTypeConfig walletTransferTypeConfig = new WalletTransferTypeConfig(true);

    private List<WalletTypeConfig> walletTypeConfigs = new ArrayList<>();
    private List<OptionBean> walletTypeStatusList = new ArrayList<>();
    private List<OptionBean> walletTransferTypeStatusList = new ArrayList<>();
    private List<OptionBean> adminFeeTypeList = new ArrayList<>();
    private List<OptionBean> walletTransferTypeList = new ArrayList<>();
    private List<OptionBean> walletTypeList = new ArrayList<>();
    private List<WalletTransferTypeConfig> walletTransferTypeConfigs = new ArrayList<>();

    private boolean blockchainProcess;
    // sorted map
    private Map<String, Boolean> blockchainProcessMap = new TreeMap<>();

    // for edit wallet type...
    private String walletCurrency;
    private String walletName;
    private int walletType;

    private String transferTypeId;

    @ToUpperCase
    @ToTrim
    private String status;

    public WalletConfigAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_WALLET_CONFIG})
    @Action("/walletConfig")
    @Accesses(access = {@Access(accessCode = AP.WALLET_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)})
    @Override
    public String execute() throws Exception {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());

        walletConfig = walletService.getDefaultWalletConfig();
        walletTransferConfig = walletService.getDefaultWalletTransferConfig();
        walletTypeConfigs = walletService.findMemberWalletTypeConfigs();
        walletTypeStatusList = optionBeanUtil.getWalletTypeConfigStatus();
        adminFeeTypeList = optionBeanUtil.getWalletTransferAdminFeeTypes();
        walletTransferTypeConfigs = walletService.findAllWalletTransferTypesConfigs();

        walletTransferTypeList.add(new OptionBean(String.valueOf(true), getText("selfTransfer")));
        walletTransferTypeList.add(new OptionBean(String.valueOf(false), getText("transferToMember")));
        walletTransferTypeStatusList = optionBeanUtil.getWalletTransferTypeConfigStatus();

        for (WalletTypeConfig walletTypeConfig : walletTypeConfigs) {
            walletTypeList.add(new OptionBean(String.valueOf(walletTypeConfig.getWalletType()),
                    WebUtil.getWalletNameWithCurrency(getLocale(), walletTypeConfig.getWalletType())));
        }

        return SHOW;
    }

    @Action("/walletConfigToggleTransfer")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String toogleTransfer() {
        try {
            walletService.doToggleWalletTransferForWalletConfig();

            walletConfig = walletService.getDefaultWalletConfig();
            if (walletConfig.getWalletTransfer()) {
                successMessage = getText("successMessage.SystemConfigAction.enableTransferSuccess");
            } else {
                successMessage = getText("successMessage.SystemConfigAction.disableTransferSuccess");
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigToggleWithdraw")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String toogleWithdraw() {
        try {
            walletService.doToggleWalletWithdrawForWalletConfig();

            walletConfig = walletService.getDefaultWalletConfig();
            if (walletConfig.getWalletWithdraw()) {
                successMessage = getText("successMessage.SystemConfigAction.enableWithdrawSuccess");
            } else {
                successMessage = getText("successMessage.SystemConfigAction.disableWithdrawSuccess");
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigGetWalletTypeInfo")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String retrieveWalletType() {
        walletCurrency = WebUtil.getWalletCurrency(getLocale(), walletType);
        walletName = WebUtil.getWalletName(getLocale(), walletType);

        WalletTypeConfig walletTypeConfig = walletService.findWalletTypeConfig(Global.UserType.MEMBER, walletType);
        if (walletTypeConfig != null) {
            status = walletTypeConfig.getStatus();
        }

        return JSON_GET_WALLET_INFO;
    }

    @Action("/walletConfigUpdateWalletType")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String updateWalletType() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            walletService.updateWalletTypeConfig(Global.UserType.MEMBER, walletType, walletCurrency, walletName, status);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigUpdateWalletTransferConfig")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String updateWalletTransferConfig() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, walletTransferConfig);
            walletService.updateWalletTransferConfig(walletTransferConfig.getMinimumAmount(), walletTransferConfig.getAdminFee(),
                    walletTransferConfig.getAdminFeeByAmount());
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigSaveWalletTransferTypeConfig")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String saveWalletTransferTypeConfig() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, walletTransferTypeConfig);
            walletService.saveWalletTransferTypeConfig(getLocale(), walletTransferTypeConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigUpdateWalletTransferTypeConfig")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String updateWalletTransferTypeConfig() {
        try {
            walletTransferTypeConfig.setTransferTypeId(transferTypeId);
            VoUtil.toTrimUpperCaseProperties(this, walletTransferTypeConfig);
            walletService.updateWalletTransferTypeConfig(walletTransferTypeConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigGetWalletTransferTypeConfigInfo")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String retrieveWalletTransferTypeConfig() {
        walletTransferTypeConfig = walletService.getWalletTransferTypeConfig(transferTypeId);

        return JSON_GET_WALLET_TRANSFER_TYPE_CONFIG_INFO;
    }

    @Action("/walletConfigDeleteWalletTransferTypeConfig")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String deleteWalletTransferTypeConfig() {
        try {
            walletService.deleteWalletTransferTypeConfigAndUpdateSeq(transferTypeId);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigUpWalletTransferTypeConfig")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String moveUpWalletTransferTypeConfig() {
        try {
            walletService.doMoveUpWalletTransferTypeConfig(transferTypeId);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigDownWalletTransferTypeConfig")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String moveDownWalletTransferTypeConfig() {
        try {
            walletService.doMoveDownWalletTransferTypeConfig(transferTypeId);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/walletConfigToggleBlockchainProcess")
    @Access(accessCode = AP.WALLET_CONFIG, updateMode = true, adminMode = true)
    public String toggleWithdrawProcess() {
        try {
            // toggle
//            CryptoProcessStatus.stopProcess(!CryptoProcessStatus.isStopProcess());
//
//            if (!CryptoProcessStatus.isStopProcess()) {
//                successMessage = getText("successMessage.SystemConfigAction.enableBlockchainProcessSuccess");
//            } else {
//                successMessage = getText("successMessage.SystemConfigAction.disableBlockchainProcessSuccess");
//            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public WalletConfig getWalletConfig() {
        return walletConfig;
    }

    public void setWalletConfig(WalletConfig walletConfig) {
        this.walletConfig = walletConfig;
    }

    public List<WalletTypeConfig> getWalletTypeConfigs() {
        return walletTypeConfigs;
    }

    public void setWalletTypeConfigs(List<WalletTypeConfig> walletTypeConfigs) {
        this.walletTypeConfigs = walletTypeConfigs;
    }

    public String getWalletCurrency() {
        return walletCurrency;
    }

    public void setWalletCurrency(String walletCurrency) {
        this.walletCurrency = walletCurrency;
    }

    public String getWalletName() {
        return walletName;
    }

    public void setWalletName(String walletName) {
        this.walletName = walletName;
    }

    public int getWalletType() {
        return walletType;
    }

    public void setWalletType(int walletType) {
        this.walletType = walletType;
    }

    public List<OptionBean> getWalletTypeStatusList() {
        return walletTypeStatusList;
    }

    public void setWalletTypeStatusList(List<OptionBean> walletTypeStatusList) {
        this.walletTypeStatusList = walletTypeStatusList;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public WalletTransferConfig getWalletTransferConfig() {
        return walletTransferConfig;
    }

    public void setWalletTransferConfig(WalletTransferConfig walletTransferConfig) {
        this.walletTransferConfig = walletTransferConfig;
    }

    public List<OptionBean> getAdminFeeTypeList() {
        return adminFeeTypeList;
    }

    public void setAdminFeeTypeList(List<OptionBean> adminFeeTypeList) {
        this.adminFeeTypeList = adminFeeTypeList;
    }

    public List<WalletTransferTypeConfig> getWalletTransferTypeConfigs() {
        return walletTransferTypeConfigs;
    }

    public void setWalletTransferTypeConfigs(List<WalletTransferTypeConfig> walletTransferTypeConfigs) {
        this.walletTransferTypeConfigs = walletTransferTypeConfigs;
    }

    public WalletTransferTypeConfig getWalletTransferTypeConfig() {
        return walletTransferTypeConfig;
    }

    public void setWalletTransferTypeConfig(WalletTransferTypeConfig walletTransferTypeConfig) {
        this.walletTransferTypeConfig = walletTransferTypeConfig;
    }

    public List<OptionBean> getWalletTransferTypeList() {
        return walletTransferTypeList;
    }

    public void setWalletTransferTypeList(List<OptionBean> walletTransferTypeList) {
        this.walletTransferTypeList = walletTransferTypeList;
    }

    public List<OptionBean> getWalletTypeList() {
        return walletTypeList;
    }

    public void setWalletTypeList(List<OptionBean> walletTypeList) {
        this.walletTypeList = walletTypeList;
    }

    public String getTransferTypeId() {
        return transferTypeId;
    }

    public void setTransferTypeId(String transferTypeId) {
        this.transferTypeId = transferTypeId;
    }

    public List<OptionBean> getWalletTransferTypeStatusList() {
        return walletTransferTypeStatusList;
    }

    public void setWalletTransferTypeStatusList(List<OptionBean> walletTransferTypeStatusList) {
        this.walletTransferTypeStatusList = walletTransferTypeStatusList;
    }

    public boolean isBlockchainProcess() {
        return blockchainProcess;
    }

    public void setBlockchainProcess(boolean blockchainProcess) {
        this.blockchainProcess = blockchainProcess;
    }

    public Map<String, Boolean> getBlockchainProcessMap() {
        return blockchainProcessMap;
    }

    public void setBlockchainProcessMap(Map<String, Boolean> blockchainProcessMap) {
        this.blockchainProcessMap = blockchainProcessMap;
    }
    // ---------------- GETTER & SETTER (END) -------------
}
