package struts.app.wallet;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTopupPackage;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", WalletTopupPackageListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletTopupPackageListDatagridAction extends BaseDatagridAction<WalletTopupPackage> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", "
            + "rows\\[\\d+\\]\\.walletType, "
            + "rows\\[\\d+\\]\\.packageAmount, "
            + "rows\\[\\d+\\]\\.fiatCurrency, "
            + "rows\\[\\d+\\]\\.fiatCurrencyAmount, "
            + "rows\\[\\d+\\]\\.status ";

    private WalletService walletService;

    @ToUpperCase
    @ToTrim
    private Integer packageAmount;
    private Integer walletType;
    private String status;

    public WalletTopupPackageListDatagridAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    @Action(value = "/walletTopupPackageListDatagrid")
    @Access(accessCode = AP.WALLET_TOP_UP_PACKAGE, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        walletService.findWalletTopupPackagesForListing(getDatagridModel(), walletType, packageAmount, status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Integer getWalletType() {
        return walletType;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public Integer getPackageAmount() {
        return packageAmount;
    }

    public void setPackageAmount(Integer packageAmount) {
        this.packageAmount = packageAmount;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
