package struts.app.wallet;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                WalletListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletListDatagridAction extends BaseDatagridAction<WalletTrx> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " + "rows\\[\\d+\\]\\.trxId, "//
            + "rows\\[\\d+\\]\\.ownerId, " //
            + "rows\\[\\d+\\]\\.ownerType, " //
            + "rows\\[\\d+\\]\\.walletType, " //
            + "rows\\[\\d+\\]\\.trxDatetime, " //
            + "rows\\[\\d+\\]\\.inAmt, " //
            + "rows\\[\\d+\\]\\.outAmt, " //
            + "rows\\[\\d+\\]\\.trxDesc, " //
            + "rows\\[\\d+\\]\\.status ";

    private WalletService walletService;
    private MemberService memberService;

    @ToUpperCase
    @ToTrim
    private String memberCode;
    private Integer walletType;
    private Date dateFrom;
    private Date dateTo;

    public WalletListDatagridAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/walletListDatagrid")
    @Access(accessCode = AP.MEMBER, readMode = true, adminMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        Member member = memberService.findMemberByMemberCode(memberCode);
        if (member != null && walletType != null) {
            walletService.findWalletTrxsForListing(getDatagridModel(), member.getMemberId(), Global.UserType.MEMBER, walletType, dateFrom, dateTo);
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
