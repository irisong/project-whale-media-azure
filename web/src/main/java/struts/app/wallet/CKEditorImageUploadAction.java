package struts.app.wallet;

import com.compalsolutions.compal.AnnouncementFileUploadConfiguration;
import com.compalsolutions.compal.GiftFileUploadConfiguration;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.azure.AzureBlobStorageService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.core.env.Environment;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.UUID;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "htmlMessage")
})

public class CKEditorImageUploadAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private File upload;
    private String uploadContentType;
    private String uploadFileName;

    private String CKEditorFuncNum;
    private String CKEditor;
    private String langCode;
    private String imageUrl;
    private String type;

    @Action(value = "/ckeditorImageUpload")
    public String uploadImage() {
        String uploadPath = "";
        String parentFileUrl = "";
        AzureBlobStorageService azureBlobStorageService = Application.lookupBean(AzureBlobStorageService.class);
        Environment env = Application.lookupBean(Environment.class);
        boolean isProdServer = env.getProperty("server.production", Boolean.class, true);

        try {
            HttpServletResponse response = ServletActionContext.getResponse();

            PrintWriter out = response.getWriter();
            response.setCharacterEncoding("UTF-8");
            response.setHeader("X-Frame-Options", "SAMEORIGIN");
            response.setContentType("text/html;charset=UTF-8");
            // Obtain CKEditor params
            String callback = ServletActionContext.getRequest().getParameter("CKEditorFuncNum");

            String expandedName = "";  //file extension
            if (uploadContentType.equals("image/pjpeg") || uploadContentType.equals("image/jpeg")) {
                // The headimageContentType of .jpg image uploaded from IE6 is image/pjpeg，whereas with IE9 above and firefox is image/jpeg
                expandedName = ".jpg";
            } else if (uploadContentType.equals("image/png") || uploadContentType.equals("image/x-png")) {
                //The headimageContentType of .png image uploaded from IE6 is image/x-png
                expandedName = ".png";
            } else if (uploadContentType.equals("image/gif")) {
                expandedName = ".gif";
            } else if (uploadContentType.equals("image/bmp")) {
                expandedName = ".bmp";
            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("window.parent.CKEDITOR.tools.callFunction(" + callback + ",''," + "'Invalid File Format（Only .jpg/.gif/.bmp/.png files are accepted）');");
                out.println("</script>");
                return null;
            }

            if ("announcement.body".equalsIgnoreCase(CKEditor) || "addLanguageBody".equalsIgnoreCase(CKEditor) || "announcement.bodyCn".equalsIgnoreCase(CKEditor)) {
                AnnouncementFileUploadConfiguration config = Application.lookupBean(AnnouncementFileUploadConfiguration.BEAN_NAME, AnnouncementFileUploadConfiguration.class);
                uploadPath = config.getAnnouncementFileUploadPath();
                parentFileUrl = config.getFullAnnouncementParentFileUrl();
            } else {
                out.println("<script type=\"text/javascript\">");
                out.println("window.parent.CKEDITOR.tools.callFunction(" + callback + ",''," + "'Invalid Web Module');");
                out.println("</script>");
                return null;
            }

            UUID uuid = UUID.randomUUID();
            if (!isProdServer) {
                File directory = new File(uploadPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
            }
            String fileName = uuid.toString() + expandedName;

            if (isProdServer) {
                azureBlobStorageService.uploadFileToAzureBlobStorageByOutputStream(fileName, upload,
                        uploadContentType, AnnouncementFileUploadConfiguration.FOLDER);
            } else {
                InputStream is = new FileInputStream(upload);
                File toFile = new File(uploadPath, fileName);
                OutputStream os = new FileOutputStream(toFile);

                byte[] buffer = new byte[1024];
                int length = 0;
                while ((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }
            }

            // return js and display image for CKEditor
            out.println("<script type=\"text/javascript\">");
            out.println("window.parent.CKEDITOR.tools.callFunction(" + callback + ",'" + parentFileUrl + "/" + fileName + "','')");
            out.println("</script>");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return null;
    }
    // ---------------- GETTER & SETTER (START) -------------

    public String getCKEditorFuncNum() {
        return CKEditorFuncNum;
    }

    public void setCKEditorFuncNum(String CKEditorFuncNum) {
        this.CKEditorFuncNum = CKEditorFuncNum;
    }

    public String getLangCode() {
        return langCode;
    }

    public void setLangCode(String langCode) {
        this.langCode = langCode;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCKEditor() {
        return CKEditor;
    }

    public void setCKEditor(String CKEditor) {
        this.CKEditor = CKEditor;
    }

    public File getUpload() {
        return upload;
    }

    public void setUpload(File upload) {
        this.upload = upload;
    }

    public String getUploadContentType() {
        return uploadContentType;
    }

    public void setUploadContentType(String uploadContentType) {
        this.uploadContentType = uploadContentType;
    }

    public String getUploadFileName() {
        return uploadFileName;
    }

    public void setUploadFileName(String uploadFileName) {
        this.uploadFileName = uploadFileName;
    }
    // ---------------- GETTER & SETTER (END) -------------
}
