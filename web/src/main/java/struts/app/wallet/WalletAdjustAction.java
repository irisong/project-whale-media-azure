package struts.app.wallet;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletAdjust;

@Results(value = {
        @Result(name = BaseAction.ADD, location = "walletAdjustAdd"),
        @Result(name = BaseAction.LIST, location = "walletAdjustList"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", Global.JsonInclude.Default })
})
public class WalletAdjustAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private WalletService walletService;
    private MemberService memberService;

    private Member member = new Member(true);
    private MemberDetail memberDetail = new MemberDetail(true);

    private WalletAdjust walletAdjust = new WalletAdjust(true);

    private List<OptionBean> walletTypes = new ArrayList<>();
    private List<OptionBean> allWalletTypes = new ArrayList<>();
    private List<OptionBean> adjustTypes = new ArrayList<>();
    private List<OptionBean> allAdjustTypes = new ArrayList<>();

    @ToTrim
    @ToUpperCase
    private String memberCode;
    private boolean memberExist = false;

    public WalletAdjustAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_WALLET_ADJUST })
    @Action(value = "/walletAdjustList")
    @Access(accessCode = AP.WALLET_ADJUST, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        return LIST;
    }

    private void populateMember() {
        if (StringUtils.isNotBlank(memberCode)) {
            member = memberService.findMemberByMemberCode(memberCode);

            if (member == null) {
                addActionError(getText("invalidMember"));
            } else {
                memberDetail = member.getMemberDetail();
                memberExist = true;
            }
        }
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());

        walletTypes = walletService.findWalletTypeForWalletAdjustment(getLocale());
        allWalletTypes.add(optionBeanUtil.generateOptionAll());
        allWalletTypes.addAll(walletTypes);

        adjustTypes = optionBeanUtil.getWalletAdjustTypes();
        allAdjustTypes = optionBeanUtil.getWalletAdjustTypesWithOptionAll();
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_WALLET_ADJUST)
    @Action("/walletAdjustAdd")
    @Access(accessCode = AP.WALLET_ADJUST, createMode = true, adminMode = true)
    public String add() {
        VoUtil.toTrimUpperCaseProperties(this);
        init();
        populateMember();
        return ADD;
    }

    @Action("/walletAdjustSave")
    @Access(accessCode = AP.WALLET_ADJUST, createMode = true, adminMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, walletAdjust);

            walletAdjust.setOwnerId(member.getMemberId());
            walletAdjust.setOwnerType(Global.UserType.MEMBER);
            walletService.createWalletAdjust(getLocale(), walletAdjust);
            successMessage = getText("successMessage.WalletAdjustAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    public List<OptionBean> getWalletTypes() {
        return walletTypes;
    }

    public void setWalletTypes(List<OptionBean> walletTypes) {
        this.walletTypes = walletTypes;
    }

    public List<OptionBean> getAllWalletTypes() {
        return allWalletTypes;
    }

    public void setAllWalletTypes(List<OptionBean> allWalletTypes) {
        this.allWalletTypes = allWalletTypes;
    }

    public List<OptionBean> getAdjustTypes() {
        return adjustTypes;
    }

    public void setAdjustTypes(List<OptionBean> adjustTypes) {
        this.adjustTypes = adjustTypes;
    }

    public List<OptionBean> getAllAdjustTypes() {
        return allAdjustTypes;
    }

    public void setAllAdjustTypes(List<OptionBean> allAdjustTypes) {
        this.allAdjustTypes = allAdjustTypes;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public boolean isMemberExist() {
        return memberExist;
    }

    public void setMemberExist(boolean memberExist) {
        this.memberExist = memberExist;
    }

    public WalletAdjust getWalletAdjust() {
        return walletAdjust;
    }

    public void setWalletAdjust(WalletAdjust walletAdjust) {
        this.walletAdjust = walletAdjust;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
