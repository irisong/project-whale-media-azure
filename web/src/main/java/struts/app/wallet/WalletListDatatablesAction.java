package struts.app.wallet;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTrx;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                WalletListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class WalletListDatatablesAction extends BaseSecureDatatablesAction<WalletTrx> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.ownerId, " //
            + "data\\[\\d+\\]\\.ownerType, " //
            + "data\\[\\d+\\]\\.walletType, " //
            + "data\\[\\d+\\]\\.trxDatetime, " //
            + "data\\[\\d+\\]\\.inAmt, " //
            + "data\\[\\d+\\]\\.outAmt, " //
            + "data\\[\\d+\\]\\.trxDesc, " //
            + "data\\[\\d+\\]\\.trxDescCn, " //
            + "data\\[\\d+\\]\\.remark, " //
            + "data\\[\\d+\\]\\.balance, " //
            + "data\\[\\d+\\]\\.status ";;

    private WalletService walletService;
    private MemberService memberService;

    @ToUpperCase
    @ToTrim
    private String memberCode;
    private Integer walletType;
    private Date dateFrom;
    private Date dateTo;

    public WalletListDatatablesAction() {
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);

        // implicitly disabled sorting.
        setSortable(false);
    }

    @Action(value = "/walletListDatatables")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true) })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        LoginInfo loginInfo = getLoginInfo();

        Member member = null;
        if (WebUtil.isAdmin(loginInfo)) {
            member = memberService.findMemberByMemberCode(memberCode);

            if (member == null) {
                addActionError(getText("invalidMember"));
                return JSON;
            }

        } else if (WebUtil.isMember(loginInfo)) {
            member = WebUtil.getLoginMember(loginInfo);
        }

        if (member != null && walletType != null) {
            walletService.findWalletTrxsForWalletStatement(getDatagridModel(), member.getMemberId(), Global.UserType.MEMBER, walletType, dateFrom, dateTo);
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public void setWalletType(Integer walletType) {
        this.walletType = walletType;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
