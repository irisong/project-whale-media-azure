package struts.app.user;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.BaseWorkFlowAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.EDIT, location = "changePassword"), //
        @Result(name = BaseAction.INPUT, location = "changePassword"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class ChangePasswordAction extends BaseWorkFlowAction {
    private static final long serialVersionUID = 1L;

    protected UserDetailsService userDetailsService;

    protected User user;

    protected String oldPassword;
    protected String newPassword;
    protected String confirmPassword;

    public ChangePasswordAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_CHANGE_PASSWORD)
    @Action(value = "/changePassword")
    @Override
    public String edit() {
        user = getLoginUser();

        return EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_CHANGE_PASSWORD)
    @Action(value = "/changePasswordUpdate")
    @Override
    public String update() {
        userDetailsService.changePassword(getLocale(), getLoginUser().getUserId(), oldPassword, newPassword);

        successMessage = getText("successMessage.ChangePasswordAction");
        successMenuKey = MP.FUNC_AD_CHANGE_PASSWORD;
        return SUCCESS;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
