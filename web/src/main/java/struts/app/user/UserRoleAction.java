package struts.app.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.UserAccessCat;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "userRoleAdd"), //
        @Result(name = BaseAction.EDIT, location = "userRoleEdit"), //
        @Result(name = BaseAction.LIST, location = "userRoleList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "userRoleAdd") })
public class UserRoleAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    protected UserDetailsService userDetailsService;

    protected UserRole userRole = new UserRole(true);
    protected List<UserAccessCat> userAccessCats;

    protected List<Boolean> checkAllModes = new ArrayList<Boolean>();
    protected List<Boolean> checkCreateModes = new ArrayList<Boolean>();
    protected List<Boolean> checkReadModes = new ArrayList<Boolean>();
    protected List<Boolean> checkUpdateModes = new ArrayList<Boolean>();
    protected List<Boolean> checkDeleteModes = new ArrayList<Boolean>();
    protected List<Boolean> checkAdminModes = new ArrayList<Boolean>();

    protected Long version;

    public UserRoleAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);

        showSuccessExitButton = true;
        showSuccessAddNewButton = true;
        successExitAction = "userRoleList";
        successAddNewAction = "userRoleAdd";
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER_ROLE)
    @Action(value = "/userRoleList")
    @Access(accessCode = AP.USER_ROLE, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER_ROLE)
    @Action(value = "/userRoleAdd")
    @Access(accessCode = AP.USER_ROLE, createMode = true)
    public String add() {
        userAccessCats = userDetailsService.findUserAccessCatsWithUserAccessForAdding();

        for (int i = 0; i < userAccessCats.size(); i++) {
            checkAllModes.add(false);
            checkCreateModes.add(false);
            checkReadModes.add(false);
            checkUpdateModes.add(false);
            checkDeleteModes.add(false);
            checkAdminModes.add(false);
        }

        return BaseAction.ADD;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER_ROLE)
    @Access(accessCode = AP.USER_ROLE, createMode = true)
    @Action(value = "/userRoleSave")
    public String save() throws Exception {
        try {
            VoUtil.toTrimUpperCaseProperties(userRole);

            userDetailsService.saveUserRole(userRole, userAccessCats);

            successMessage = "Add user role successful";
            successMenuKey = MP.FUNC_AD_USER_ROLE;
        } catch (Exception ex) {
            add();

            throw ex;
        }
        return BaseAction.SUCCESS;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER_ROLE)
    @Action(value = "/userRoleEdit")
    @Access(accessCode = AP.USER_ROLE, updateMode = true)
    public String edit() {
        userRole = userDetailsService.findUserRoleByUserRoleId(userRole.getRoleId());

        if (userRole != null) {
            version = userRole.getVersion();
        }

        userAccessCats = userDetailsService.findUserAccessCatsWithUserAccessByUserRoleId(userRole.getRoleId());

        for (int i = 0; i < userAccessCats.size(); i++) {
            checkAllModes.add(false);
            checkCreateModes.add(false);
            checkReadModes.add(false);
            checkUpdateModes.add(false);
            checkDeleteModes.add(false);
            checkAdminModes.add(false);
        }

        return BaseAction.EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER_ROLE)
    @Action(value = "/userRoleUpdate")
    @Access(accessCode = AP.USER_ROLE, updateMode = true)
    public String update() throws Exception {
        try {
            userDetailsService.updateUserRole(userRole, userAccessCats, version);

            successMessage = "Update user role successful";
            successMenuKey = MP.FUNC_AD_USER_ROLE;
        } catch (Exception ex) {
            edit();
            throw ex;
        }
        return BaseAction.SUCCESS;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public List<UserAccessCat> getUserAccessCats() {
        return userAccessCats;
    }

    public void setUserAccessCats(List<UserAccessCat> userAccessCats) {
        this.userAccessCats = userAccessCats;
    }

    public List<Boolean> getCheckAllModes() {
        return checkAllModes;
    }

    public void setCheckAllModes(List<Boolean> checkAllModes) {
        this.checkAllModes = checkAllModes;
    }

    public List<Boolean> getCheckCreateModes() {
        return checkCreateModes;
    }

    public void setCheckCreateModes(List<Boolean> checkCreateModes) {
        this.checkCreateModes = checkCreateModes;
    }

    public List<Boolean> getCheckReadModes() {
        return checkReadModes;
    }

    public void setCheckReadModes(List<Boolean> checkReadModes) {
        this.checkReadModes = checkReadModes;
    }

    public List<Boolean> getCheckUpdateModes() {
        return checkUpdateModes;
    }

    public void setCheckUpdateModes(List<Boolean> checkUpdateModes) {
        this.checkUpdateModes = checkUpdateModes;
    }

    public List<Boolean> getCheckDeleteModes() {
        return checkDeleteModes;
    }

    public void setCheckDeleteModes(List<Boolean> checkDeleteModes) {
        this.checkDeleteModes = checkDeleteModes;
    }

    public List<Boolean> getCheckAdminModes() {
        return checkAdminModes;
    }

    public void setCheckAdminModes(List<Boolean> checkAdminModes) {
        this.checkAdminModes = checkAdminModes;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
