package struts.app.user;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class UserListDatagridAction extends BaseDatagridAction<User> {
    private static final long serialVersionUID = 1L;

    protected UserDetailsService userDetailsService;

    @ToUpperCase
    @ToTrim
    private String username;

    @ToUpperCase
    @ToTrim
    private String fullname;

    @ToUpperCase
    @ToTrim
    private String email;

    @ToUpperCase
    @ToTrim
    private String status;
    private Date datetimeAdd;

    public UserListDatagridAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
    }

    @Action(value = "/userListDatagrid")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        userDetailsService.findUsersForListing(getDatagridModel(), username, fullname, null, null, email, status, datetimeAdd, Global.UserType.HQ);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDatetimeAdd() {
        return datetimeAdd;
    }

    public void setDatetimeAdd(Date datetimeAdd) {
        this.datetimeAdd = datetimeAdd;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
