package struts.app.user;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;

@Results(value = { //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.INPUT, location = "jsonMessage") })
public class UserFormUserRolesAction extends BaseDatagridAction<UserRole> {
    private static final long serialVersionUID = 1L;

    @Action("/userFormUserRoles")
    @Access(accessCode = AP.USER, createMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        List<UserRole> userRoles = getUserRoles();

        getDatagridModel().setRecords(userRoles);
        getDatagridModel().sortRecords();

        return JSON;
    }

    /**
     * Do not provide PUBLIC modifier, jsp should use #session.userRoles
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<UserRole> getUserRoles() {
        if (!session.containsKey(UserAction.SESSION_USERROLES)) {
            List<UserRole> userRoles = new ArrayList<UserRole>();
            session.put(UserAction.SESSION_USERROLES, userRoles);

        }
        return (List<UserRole>) session.get(UserAction.SESSION_USERROLES);
    }
}
