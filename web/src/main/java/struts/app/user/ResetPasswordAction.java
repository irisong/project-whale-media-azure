package struts.app.user;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseAction.ResultType;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.EDIT, location = "resetPassword"), //
        @Result(name = BaseAction.INPUT, location = "resetPassword"), //
        @Result(name = BaseAction.SUCCESS, type = ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app", "successMessage",
                "${successMessage}", "successMenuKey", "${successMenuKey}" }) })
public class ResetPasswordAction extends ChangePasswordAction {
    private static final long serialVersionUID = 1L;

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action(value = "/resetPassword")
    @Access(accessCode = AP.USER, updateMode = true)
    @Override
    public String edit() {
        user = userDetailsService.findUserByUserId(user.getUserId());

        return EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_USER)
    @Action(value = "/resetPasswordUpdate")
    @Access(accessCode = AP.USER, updateMode = true)
    @Override
    public String update() {
        try {
            if (!StringUtils.equals(newPassword, confirmPassword)) {
                addActionError(getText("errorMessage.new.password.and.confirm.password.not.same"));
                return EDIT;
            }
            userDetailsService.doResetPassword(user.getUserId(), newPassword);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }

        successMessage = getText("successMessage.ChangePasswordAction");
        successMenuKey = MP.FUNC_AD_USER;
        return SUCCESS;
    }
}
