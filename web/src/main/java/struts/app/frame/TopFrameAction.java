package struts.app.frame;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserMenu;
import com.compalsolutions.compal.security.SecurityUtil;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.user.service.UserService;

public class TopFrameAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private UserService userService;

    private List<UserMenu> mainMenus;

    public TopFrameAction() {
        userService = (UserService) Application.lookupBean(UserService.BEAN_NAME);
    }

    @Action(value = "/topFrame", results = { //
            @Result(location = "/WEB-INF/content/app/frame/topFrame.jsp"), //
            @Result(name = BaseAction.INPUT, location = "/WEB-INF/content/app/frame/topFrame.jsp") })
    @Override
    public String execute() throws Exception {
        User loginUser = (User) SecurityUtil.getLoginUser();

        mainMenus = userService.findAuthorizedUserMenu(loginUser.getUserId());

        return SUCCESS;
    }

    public List<UserMenu> getMainMenus() {
        return mainMenus;
    }
}
