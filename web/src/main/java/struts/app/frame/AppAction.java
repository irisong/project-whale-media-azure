package struts.app.frame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.MemberUserType;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.service.DocFileService;
import com.compalsolutions.compal.general.vo.DocFile;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "app", type = "tiles"), //
        @Result(name = AppAction.MEMBER, location = "member", type = "tiles") //
})
public class AppAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String MEMBER = "member";

    private CurrencyService currencyService;
    private AnnouncementService announcementService;
    private DocFileService docFileService;

    private List<CurrencyExchange> currencyExchanges = new ArrayList<CurrencyExchange>();
    private int totalPageAnnouncements;
    private int pageSizeAnnouncements;

    private List<DocFile> docFiles = new ArrayList<DocFile>();

    public AppAction() {
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
        docFileService = Application.lookupBean(DocFileService.BEAN_NAME, DocFileService.class);
    }

    @Action("/app")
    @Override
    @EnableTemplate
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;
        boolean isAgent = loginInfo.getUserType() instanceof AgentUserType;
        boolean isMember = loginInfo.getUserType() instanceof MemberUserType;

        String userGroup;
        if (isAdmin) {
            userGroup = Global.PublishGroup.ADMIN_GROUP;
        } else if (isAgent) {
            userGroup = Global.PublishGroup.AGENT_GROUP;
        } else if (isMember) {
            userGroup = Global.PublishGroup.MEMBER_GROUP;
        } else {
            userGroup = Global.PublishGroup.PUBLIC_GROUP;
        }

        currencyExchanges = currencyService.findLatestCurrencyExchanges();
        pageSizeAnnouncements = 6;
        totalPageAnnouncements = (announcementService.getAnnouncementTotalRecordsFor(Arrays.asList(userGroup)) / pageSizeAnnouncements) + 1;

        docFiles = docFileService.findDocFilesForDashboard(userGroup);

        return INPUT;
    }



    public List<CurrencyExchange> getCurrencyExchanges() {
        return currencyExchanges;
    }

    public void setCurrencyExchanges(List<CurrencyExchange> currencyExchanges) {
        this.currencyExchanges = currencyExchanges;
    }

    public int getTotalPageAnnouncements() {
        return totalPageAnnouncements;
    }

    public void setTotalPageAnnouncements(int totalPageAnnouncements) {
        this.totalPageAnnouncements = totalPageAnnouncements;
    }

    public int getPageSizeAnnouncements() {
        return pageSizeAnnouncements;
    }

    public void setPageSizeAnnouncements(int pageSizeAnnouncements) {
        this.pageSizeAnnouncements = pageSizeAnnouncements;
    }

    public List<DocFile> getDocFiles() {
        return docFiles;
    }

    public void setDocFiles(List<DocFile> docFiles) {
        this.docFiles = docFiles;
    }
}
