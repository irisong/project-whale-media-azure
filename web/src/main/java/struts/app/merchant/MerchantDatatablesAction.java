package struts.app.merchant;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.merchant.service.MerchantService;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                MerchantDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class MerchantDatatablesAction extends BaseSecureDatatablesAction<Merchant> {
    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.merchantId, "
            + "data\\[\\d+\\]\\.name, "
            + "data\\[\\d+\\]\\.nameCn, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.type, "
            + "data\\[\\d+\\]\\.email, "
            + "data\\[\\d+\\]\\.licenseNo, "
            + "data\\[\\d+\\]\\.contactNo ";

    private MerchantService merchantService;

    @ToTrim
    @ToUpperCase
    private String merchantType;
    private String status;

    public MerchantDatatablesAction() {
        merchantService = Application.lookupBean(MerchantService.BEAN_NAME, MerchantService.class);
    }

    @Action(value = "/merchantDatatables")
    @Access(accessCode = AP.MERCHANT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        merchantService.findMerchantForListing(getDatagridModel(), merchantType, status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMerchantType() {
        return merchantType;
    }

    public void setMerchantType(String merchantType) {
        this.merchantType = merchantType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------

}
