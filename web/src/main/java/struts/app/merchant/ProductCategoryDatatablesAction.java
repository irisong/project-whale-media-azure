package struts.app.merchant;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.merchant.service.ProductService;
import com.compalsolutions.compal.merchant.vo.ProductCategory;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                ProductCategoryDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class ProductCategoryDatatablesAction extends BaseSecureDatatablesAction<ProductCategory> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.categoryId, "
            + "data\\[\\d+\\]\\.category, "
            + "data\\[\\d+\\]\\.categoryCn, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.sortOrder ";

    private ProductService productService;

    @ToUpperCase
    @ToTrim
    private String status;

    public ProductCategoryDatatablesAction() {
        productService = Application.lookupBean(ProductService.BEAN_NAME, ProductService.class);
    }

    @Action(value = "/productCategoryDatatables")
    @Access(accessCode = AP.PRODUCT_CATEGORY, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        productService.findProductCategoryForDatagrid(getDatagridModel(), status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    // ---------------- GETTER & SETTER (END) -------------
}


