package struts.app.merchant;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.merchant.service.MerchantService;
import com.compalsolutions.compal.merchant.vo.Merchant;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "merchantList"),
        @Result(name = BaseAction.EDIT, location = "merchantListEdit"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default})
})
public class MerchantAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private MerchantService merchantService;

    private static final String SESSION_MERCHANT = "merchant";

    private Merchant merchant = new Merchant(true);

    private boolean clear;

    // for update status
    @ToUpperCase
    @ToTrim
    private String status;
    private String merchantId;

    // for display/add/edit
    private List<OptionBean> allStatusList = new ArrayList<>();
    private List<OptionBean> allMerchantsList = new ArrayList<>();
    private List<OptionBean> statusList = new ArrayList<>();
    private List<OptionBean> merchantList = new ArrayList<>();

    private String language = Global.Language.ENGLISH;

    public MerchantAction() {merchantService = Application.lookupBean(MerchantService.BEAN_NAME, MerchantService.class); }

    public void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allStatusList = optionBeanUtil.getStatusWithOptionAll();
        allMerchantsList = optionBeanUtil.getMerchantTypeWithOptionAll();
        statusList = optionBeanUtil.getStatus();
        merchantList  = optionBeanUtil.getMerchantType();
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_MERCHANT})
    @Action(value = "/merchantList")
    @Access(accessCode = AP.MERCHANT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        init();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_MERCHANT);
        } else if (session.containsKey(SESSION_MERCHANT)) {
            objectMap = (Map<String, Object>) session.get(SESSION_MERCHANT);
        } else {
            session.put(SESSION_MERCHANT, objectMap);
        }

        merchantId = (String) objectMap.get("merchantId");
        status = (String) objectMap.get("status");

        return LIST;
    }

    @Action("/saveMerchant")
    @Access(accessCode = AP.MERCHANT, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String saveMerchant() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, merchant);

            merchantService.doCreateNewMerchant(getLocale(), merchant);
            successMessage = getText("successMessage.MerchantAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_MERCHANT)
    @Action(value = "/merchantListEdit")
    @Access(accessCode = AP.MERCHANT, adminMode = true, updateMode = true)
    public String edit() {
        try {
            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statusList = optionBeanUtil.getStatus();
            merchantList = optionBeanUtil.getMerchantType();

            merchant = merchantService.getMerchant(merchant.getMerchantId());
            if(merchant == null) {
                addActionError(getText("invalidMerchant"));
                return list();
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/merchantUpdate")
    @Access(accessCode = AP.MERCHANT, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, merchant);
            merchantService.doUpdateMerchant(getLocale(), merchant);
            successMessage = getText("successMessage.MerchantAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/updateMerchantStatus")
    @Access(accessCode = AP.MERCHANT, updateMode = true)
    public String updateMerchantStatus() {
        try {
            merchantService.doUpdateMerchantStatus(merchantId, status);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<OptionBean> getAllMerchantsList() {
        return allMerchantsList;
    }

    public void setAllMerchantsList(List<OptionBean> allMerchantsList) {
        this.allMerchantsList = allMerchantsList;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getMerchantList() {
        return merchantList;
    }

    public void setMerchantList(List<OptionBean> merchantList) {
        this.merchantList = merchantList;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
