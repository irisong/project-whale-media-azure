package struts.app.merchant;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.merchant.service.ProductService;
import com.compalsolutions.compal.merchant.vo.ProductCategory;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "productCategoryList"),
        @Result(name = BaseAction.EDIT, location = "productCategoryEdit"),
        @Result(name = ProductCategoryAction.JSON_NOTE, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", ProductCategoryAction.GET_JSON_NOTE_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default})
})
public class ProductCategoryAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    public static final String JSON_NOTE = "jsonNote";
    public static final String GET_JSON_NOTE_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", sortOrder";

    private ProductService productService;

    private static final String SESSION_PRODUCT_CATEGORY = "productCategory";

    private ProductCategory productCategory = new ProductCategory(false);

    private boolean clear;

    // for update status
    @ToUpperCase
    @ToTrim
    private String status;
    private String categoryId;

    // for display/add/edit
    private List<OptionBean> allStatusList = new ArrayList<>();
    private List<OptionBean> statusList = new ArrayList<>();

    private String id;

    // for return latest sortOrder
    private int sortOrder;

    public ProductCategoryAction() {productService = Application.lookupBean(ProductService.BEAN_NAME, ProductService.class); }

    @EnableTemplate(menuKey = {MP.FUNC_AD_PRODUCT_CATEGORY})
    @Action(value = "/productCategoryList")
    @Access(accessCode = AP.PRODUCT_CATEGORY, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allStatusList = optionBeanUtil.getStatusWithOptionAll();

        VoUtil.toTrimUpperCaseProperties(this);
        Map<String, Object> objectMap = new HashMap<>();

        if (clear) {
            session.remove(SESSION_PRODUCT_CATEGORY);
        } else if (session.containsKey(SESSION_PRODUCT_CATEGORY)) {
            objectMap = (Map<String, Object>) session.get(SESSION_PRODUCT_CATEGORY);
        } else {
            session.put(SESSION_PRODUCT_CATEGORY, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("status", status);
        } else {
            status = (String) objectMap.get("status");
        }

        return LIST;
    }

    @Action("/saveProductCategory")
    @Access(accessCode = AP.PRODUCT_CATEGORY, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String saveProductCategory() {
        try {
            productService.doCreateNewProductCategory(productCategory);
            successMessage = getText("submitSuccessfully");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/getNextProductCategorySortOrder")
    public String getNextProductCategorySortOrder() {
        VoUtil.toTrimUpperCaseProperties(this);
        sortOrder = productService.getNextProductCategorySortOrder();

        return JSON_NOTE;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_PRODUCT_CATEGORY})
    @Access(accessCode = AP.PRODUCT_CATEGORY, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Action(value = "/editProductCategory")
    public String editProductCategory() {
        try {
            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statusList = optionBeanUtil.getStatus();
            productCategory = productService.findProductCategory(id);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action("/updateProductCategory")
    @Access(accessCode = AP.PRODUCT_CATEGORY, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String updateProductCategory() throws IOException {
        try {
            productService.doUpdateProductCategory(productCategory);
            successMessage = getText("updateSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/updateCategoryStatus")
    @Access(accessCode = AP.PRODUCT_CATEGORY, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String updateCategoryStatus() {
        try {
            productService.doUpdateProductCategoryStatus(categoryId, status);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }

    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
