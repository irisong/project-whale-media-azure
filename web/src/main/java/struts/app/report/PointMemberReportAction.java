package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "pointMemberReport"), //
})
public class PointMemberReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> userTypes = new ArrayList<>();

    private String userType;

    private Date dateFrom;

    private Date dateTo;

    private boolean giftListing;

    private String memberCode;

    private String reportType;

    private String dateType;

    private String memberId;

    private PointService pointService;

    private MemberService memberService;

    public PointMemberReportAction() {
        pointService = Application.lookupBean(PointService.class);
        memberService = Application.lookupBean(MemberService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        userTypes = optionBeanUtil.getPointReportUserType();
        if (StringUtils.isBlank(userType)) {
            userType = userTypes.get(0).getKey();
        }

        if (StringUtils.isNotBlank(memberId)) {
            memberCode = memberService.getMember(memberId).getMemberCode();
        }
        if (StringUtils.isNotBlank(reportType)) {
            switch (reportType) {
                case PointTrx.MONTHLY:
                    dateTo = new Date();
                    dateFrom = DateUtil.getFirstDayOfMonth();
                    if (StringUtils.isNotBlank(dateType)) {
                        String today = DateUtil.format(dateTo, "yyyy/MM");
                        if (!today.equalsIgnoreCase(dateType)) {
                            DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                            String dateFromString = dateType.replace("/", "-").concat("-01");
                            try {
                                dateFrom = sdf.parse(dateFromString);
                                Date convertedDate = sdf.parse(dateFromString);
                                Calendar c = Calendar.getInstance();
                                c.setTime(convertedDate);
                                c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));
                                dateTo = c.getTime();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    break;
                case PointTrx.WEEKLY:
                    dateTo = new Date();
                    dateFrom = DateUtil.getFirstDayOfWeek();
                    break;
                case PointTrx.DAILY:
                    dateTo = new Date();
                    dateFrom = dateTo;
                    break;
                default:
                    dateTo = null;
                    dateFrom = null;
                    break;
            }
        }

    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_POINT_MEMBER_REPORT})
    @Action(value = "/pointMemberReport")
    @Access(accessCode = AP.REPORT_POINT_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(List<OptionBean> userTypes) {
        this.userTypes = userTypes;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public boolean isGiftListing() {
        return giftListing;
    }

    public void setGiftListing(boolean giftListing) {
        this.giftListing = giftListing;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getReportType() {
        return reportType;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public PointService getPointService() {
        return pointService;
    }

    public void setPointService(PointService pointService) {
        this.pointService = pointService;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
