package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "topupHistoryReport"),
})
public class TopupHistoryReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Date DateFrom;
    private Date DateTo;
    private List<OptionBean> allRemark = new ArrayList<>();

    public TopupHistoryReportAction() { }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allRemark = optionBeanUtil.getTrxHistoryRemarkWithOptionAll();
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_TOPUP_HISTORY_REPORT})
    @Action(value = "/topupHistoryReport")
    @Access(accessCode = AP.REPORT_TOPUP_HISTORY, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Date getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(Date DateFrom) {
        this.DateFrom = DateFrom;
    }

    public Date getDateTo() {
        return DateTo;
    }

    public void setDateTo(Date DateTo) {
        this.DateTo = DateTo;
    }

    public List<OptionBean> getAllRemark() {
        return allRemark;
    }

    public void setAllRemark(List<OptionBean> allRemark) {
        this.allRemark = allRemark;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
