package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                PointLeaderBoardListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class PointLeaderBoardListDatatablesAction extends BaseSecureDatatablesAction<PointTrx> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.memberId, "//
            + "data\\[\\d+\\]\\.profileName, "//
            + "data\\[\\d+\\]\\.memberCode, "//
            + "data\\[\\d+\\]\\.point, "//
            + "data\\[\\d+\\]\\.rowNum ";

    @ToUpperCase
    @ToTrim
    private String reportType;
    private String userType;
    private String dateType;

    private MemberService memberService;
    private PointService pointService;

    public PointLeaderBoardListDatatablesAction() {
        memberService = Application.lookupBean(MemberService.class);
        pointService = Application.lookupBean(PointService.class);
    }

    @Action(value = "/pointLeaderBoardDatatables")
    @Accesses(access = {@Access(accessCode = AP.REPORT_POINT_LEADER_BOARD, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        pointService.findPointTrxForDatagrid(getDatagridModel(), userType, reportType, null, null, null, dateType);
        getDatagridModel().getRecords().forEach(e -> {
            Member member = memberService.getMember(e.getMemberId());
            e.setProfileName(member.getMemberDetail().getProfileName());
            e.setMemberCode(member.getMemberCode());
        });
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
