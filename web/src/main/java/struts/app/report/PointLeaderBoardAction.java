package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "pointLeaderBoard"), //
})
public class PointLeaderBoardAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private List<OptionBean> reportTypes = new ArrayList<>();

    private List<OptionBean> dateTypes = new ArrayList<>();

    private List<OptionBean> userTypes = new ArrayList<>();

    private String reportType;

    private String userType;

    private String dateType;

    private PointService pointService;

    public PointLeaderBoardAction() {
        pointService = Application.lookupBean(PointService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        reportTypes = optionBeanUtil.getPointReportType();
        userTypes = optionBeanUtil.getPointReportUserType();
        List<String> listDateStr = pointService.getReportAvailableDate();
        String today = DateUtil.format(new Date(), "yyyy/MM");
        if (!listDateStr.contains(today)) {
            listDateStr.add(today);
        }
        Collections.sort(listDateStr, Collections.reverseOrder());
        for (String dateStr : listDateStr) {
            dateTypes.add(new OptionBean(dateStr, dateStr));
        }
        userType = userTypes.get(0).getKey();
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_POINT_LEADER_BOARD})
    @Action(value = "/pointLeaderBoard")
    @Access(accessCode = AP.REPORT_POINT_LEADER_BOARD, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getReportTypes() {
        return reportTypes;
    }

    public void setReportTypes(List<OptionBean> reportTypes) {
        this.reportTypes = reportTypes;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public List<OptionBean> getUserTypes() {
        return userTypes;
    }

    public void setUserTypes(List<OptionBean> userTypes) {
        this.userTypes = userTypes;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public List<OptionBean> getDateTypes() {
        return dateTypes;
    }

    public void setDateTypes(List<OptionBean> dateTypes) {
        this.dateTypes = dateTypes;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
