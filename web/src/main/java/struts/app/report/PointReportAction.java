package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "pointReportDetail"), //
})
public class PointReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;
    protected WalletService walletService;

    @ToTrim
    @ToUpperCase

    private String userType;

    private String channelId;

    private String memberId;

    private String memberCode;

    private String memberCodeForm;

    private boolean giftListing;

    public PointReportAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
    }

    private void init() {
        if (StringUtils.isNotBlank(memberCodeForm)) {
            memberCode = memberCodeForm;
        }
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_POINT_MEMBER_REPORT})
    @Action(value = "/pointReportDetail")
    @Access(accessCode = AP.REPORT_POINT_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public boolean isGiftListing() {
        return giftListing;
    }

    public void setGiftListing(boolean giftListing) {
        this.giftListing = giftListing;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getMemberCodeForm() {
        return memberCodeForm;
    }

    public void setMemberCodeForm(String memberCodeForm) {
        this.memberCodeForm = memberCodeForm;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
