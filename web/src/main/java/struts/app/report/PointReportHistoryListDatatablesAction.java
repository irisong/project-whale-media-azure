package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointReport;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                PointReportHistoryListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class PointReportHistoryListDatatablesAction extends BaseSecureDatatablesAction<PointReport> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            //==================================
            + "data\\[\\d+\\]\\.profileName, "
            + "data\\[\\d+\\]\\.inAmt, "
            + "data\\[\\d+\\]\\.outAmt, "
            + "data\\[\\d+\\]\\.memberId ";

    @ToUpperCase
    @ToTrim
    private String memberId;

    private String userType;

    private String channelId;

    private boolean giftListing;

    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String guildId;

    private MemberService memberService;
    private PointService pointService;
    private BroadcastService broadcastService;

    public PointReportHistoryListDatatablesAction() {
        memberService = Application.lookupBean(MemberService.class);
        pointService = Application.lookupBean(PointService.class);
        broadcastService = Application.lookupBean(BroadcastService.class);
    }


    @Action(value = "/guildMemberIncomeReportListDatatables")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true, updateMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true, updateMode = true)
    })
    public String webExecute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }
//        BroadcastGuild guild = broadcastService.findBroadcastGuildByAgentId(agentId);
//        pointService.findPointReportForListing(getDatagridModel(), guild.getId());//this will get whole datagrid without any filtering
//        getDatagridModel().getRecords();

//       ListmemberService.findGuildMemberListByGuildId(guildId);//can use owner id also
//      fing guild member list
//        if (StringUtils.isBlank(memberId)) {
//            Member member = memberService.findMemberByMemberCode(memberCode);
//            if (member != null) {
//                memberId = member.getMemberId();
//            }
//        }
//        pointService.findPointTrxForDatagrid(getDatagridModel(), giftListing, channelId, memberId);
//        getDatagridModel().getRecords().forEach(e -> {
//            if (StringUtils.isBlank(e.getMemberCode())) {
//                e.setMemberCode(memberService.getMember(giftListing ? e.getContributorId() : e.getMemberId()).getMemberCode());
//            }
//        });
//
//        obj.setPoint(new BigDecimal(rs.getString("totalPoint")));
//        obj.setMemberId(rs.getString("contributor_id"));
//        obj.setChannelId(rs.getString("channel_id"));

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public boolean isGiftListing() {
        return giftListing;
    }

    public void setGiftListing(boolean giftListing) {
        this.giftListing = giftListing;
    }

    public MemberService getMemberService() {
        return memberService;
    }

    public void setMemberService(MemberService memberService) {
        this.memberService = memberService;
    }

    public PointService getPointService() {
        return pointService;
    }

    public void setPointService(PointService pointService) {
        this.pointService = pointService;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
