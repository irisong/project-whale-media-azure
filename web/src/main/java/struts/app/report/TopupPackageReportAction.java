package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "topupPackageReport"),
})
public class TopupPackageReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Date dateFrom;
    private Date dateTo;

    public TopupPackageReportAction() { }

    @EnableTemplate(menuKey = {MP.FUNC_AD_TOPUP_PACKAGE_REPORT})
    @Action(value = "/topupPackageReport")
    @Access(accessCode = AP.REPORT_TOPUP_PACKAGE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------



    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
