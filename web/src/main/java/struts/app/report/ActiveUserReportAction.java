package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

import static com.compalsolutions.compal.struts.BaseAction.LIST;

@Results(value = {
        @Result(name = LIST, location = "activeUserReport"),
})
public class ActiveUserReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Date DateFrom;
    private Date DateTo;

    public ActiveUserReportAction() {
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_ACTIVE_USER_REPORT})
    @Action(value = "/activeUserReport")
    @Access(accessCode = AP.REPORT_ACTIVE_USER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Date getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        DateFrom = dateFrom;
    }

    public Date getDateTo() {
        return DateTo;
    }

    public void setDateTo(Date dateTo) {
        DateTo = dateTo;
    }
    // ---------------- GETTER & SETTER (END) -------------
}
