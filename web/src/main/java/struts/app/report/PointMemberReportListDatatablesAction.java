package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.datagrid.DatagridModel;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import jnr.a64asm.Mem;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import struts.util.ExcelGeneratorUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                PointMemberReportListDatatablesAction.JSON_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.STREAM, params = {"contentType", "application/vnd.ms-excel",
                "inputName", "excelStream", "contentDisposition", "attachment;filename=${excelFileName}", "bufferSize", "2048"})
})
public class PointMemberReportListDatatablesAction extends BaseSecureDatatablesAction<LiveStreamChannel> {
    private static final long serialVersionUID = 1L;

    //excel file content header
    private static final String MEMBER_POINT_REPORT_HEADER = "Member ID;Host Whalelive ID;HostName;Channel Start Time;Channel End Time;Highest View;Total Point;Total Time;Total Time In Minutes and seconds(mins(secs))";

    //excel file name
    public static final String SHEET_NAME = "Member Point Report";

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.channelId, "//
            + "data\\[\\d+\\]\\.startDate, "//
            + "data\\[\\d+\\]\\.endDate, "//
            + "data\\[\\d+\\]\\.totalView, "//
            + "data\\[\\d+\\]\\.totalPoint, "//
            + "data\\[\\d+\\]\\.whaleLiveId, "//
            + "data\\[\\d+\\]\\.hostName, "//
            + "data\\[\\d+\\]\\.memberCode ";

    @ToUpperCase
    @ToTrim
    private String userType;
    private String dateType;
    private Date dateFrom;
    private Date dateTo;
    private String memberCode;

    private MemberService memberService;
    private PointService pointService;
    private LiveStreamService liveStreamService;

    //Export Excel
    private String excelFileName;
    private InputStream excelStream;

    public PointMemberReportListDatatablesAction() {
        memberService = Application.lookupBean(MemberService.class);
        pointService = Application.lookupBean(PointService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
    }

    @Action(value = "/pointMemberReportDatatables")
    @Accesses(access = {@Access(accessCode = AP.REPORT_POINT_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

//        pointService.findPointTrxForDatagrid(getDatagridModel(), userType, null, null, dateFrom, dateTo, dateType);
        liveStreamService.findLiveStreamChannelForDatagrid(getDatagridModel(), userType, memberCode, dateFrom, dateTo);
        getDatagridModel().getRecords().forEach(e -> {
            Member host = memberService.getMember(e.getMemberId());
            e.setWhaleLiveId(host.getMemberDetail().getWhaleliveId());
            e.setHostName(host.getMemberDetail().getProfileName());
            if (userType.equalsIgnoreCase(PointTrx.POPULAR)) {
                if (e.getChannelId() != null) {
                    BigDecimal totalPoint = pointService.getTotalPointReceivedByChannelId(e.getChannelId());
                    e.setTotalPoint(totalPoint);
                }
            }
        });
        return JSON;
    }

    @Action("/memberPointReportExportExcel")
    @Access(accessCode = AP.REPORT_POINT_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String memberPointReportExportExcel() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);

            List<LiveStreamChannel> memberPointReport = liveStreamService.findLiveStreamChannelByMemberCodeAndUserType(memberCode, userType, dateFrom, dateTo);

            memberPointReport.forEach(e -> {
                Member host = memberService.getMember(e.getMemberId());
                e.setWhaleLiveId(host.getMemberDetail().getWhaleliveId());
                e.setHostName(host.getMemberDetail().getProfileName());
                if (userType.equalsIgnoreCase(PointTrx.POPULAR)) {
                    if (e.getChannelId() != null) {
                        BigDecimal totalPoint = pointService.getTotalPointReceivedByChannelId(e.getChannelId());
                        e.setTotalPoint(totalPoint);
                    }
                }
            });

            this.initExcelFile();
            this.exportExcel(memberPointReport);

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return SUCCESS;
    }

    private void initExcelFile() {
        DateFormat df = new SimpleDateFormat("yyyyMMdd");
        if(StringUtils.isBlank(memberCode)) {
            this.setExcelFileName("MemberPointReport_All_" + userType + "_" + df.format(new Date()) + ".xls");
        }
        else {
            this.setExcelFileName("MemberPointReport_" + memberCode + "_" + userType + "_" + df.format(new Date()) + ".xls");
        }

    }

    private void exportExcel(List<LiveStreamChannel> liveStreamChannels) throws Exception {
        try {
            ExcelGeneratorUtil excelGeneratorUtil = new ExcelGeneratorUtil(MEMBER_POINT_REPORT_HEADER);
            /**
             *used HSSFWorkbook for Excel files in .xls format.
             **/
            excelGeneratorUtil.createHeader(SHEET_NAME);
            String dateFormat = "yyyy-MM-dd HH:mm:ss";
            HSSFRow row;
            HSSFWorkbook wb = excelGeneratorUtil.getWb(); //wb = workbook
            HSSFSheet sheet = excelGeneratorUtil.getSheet(); //defined sheet name
            long difference_In_Time;
            long difference_In_Minutes;

            int i = 1;
            DecimalFormat df = new DecimalFormat("#.##");
            if(liveStreamChannels.size() > 0) {
                for(LiveStreamChannel liveStreamChannel : liveStreamChannels) {
                    //calculate total time
                    difference_In_Time = liveStreamChannel.getEndDate().getTime() - liveStreamChannel.getStartDate().getTime();

                    int seconds = (int) (difference_In_Time / 1000) % 60 ;
                    int minutes = (int) ((difference_In_Time / (1000*60)) % 60);
                    int hours   = (int) ((difference_In_Time / (1000*60*60)) % 24);

                    int differenceMinutes =(int)(difference_In_Time / 1000) / 60;

                    row = sheet.createRow(i);
                    row.createCell(0).setCellValue(liveStreamChannel.getMemberId());
                    row.createCell(1).setCellValue(liveStreamChannel.getWhaleLiveId());
                    row.createCell(2).setCellValue(liveStreamChannel.getHostName());
                    row.createCell(3).setCellValue(DateUtil.format(liveStreamChannel.getStartDate(),dateFormat));
                    row.createCell(4).setCellValue(DateUtil.format(liveStreamChannel.getEndDate(),dateFormat));
                    row.createCell(5).setCellValue(liveStreamChannel.getTotalView());
                    row.createCell(6).setCellValue(df.format(liveStreamChannel.getTotalPoint()));
                    row.createCell(7).setCellValue(hours+" hours "+minutes+" minutes "+seconds+" seconds ");
                    row.createCell(8).setCellValue(differenceMinutes+"("+seconds+")");
                    i++;
                }
                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
                sheet.autoSizeColumn(3);
                sheet.autoSizeColumn(4);
                sheet.autoSizeColumn(5);
                sheet.autoSizeColumn(6);
                sheet.autoSizeColumn(7);
                sheet.autoSizeColumn(8);
            } else {
                row = sheet.createRow(i);
                row.createCell(0).setCellValue("No record found");
            }

            //Stored document in streaming
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            wb.write(os);
            byte[] fileContent = os.toByteArray();
            ByteArrayInputStream is = new ByteArrayInputStream(fileContent);
            excelStream = is;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getDateType() {
        return dateType;
    }

    public void setDateType(String dateType) {
        this.dateType = dateType;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getExcelFileName() {
        return excelFileName;
    }

    public void setExcelFileName(String excelFileName) {
        this.excelFileName = excelFileName;
    }

    public InputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(InputStream excelStream) {
        this.excelStream = excelStream;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
