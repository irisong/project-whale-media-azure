package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.PointTrx;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                PointReportListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class PointReportListDatatablesAction extends BaseSecureDatatablesAction<PointTrx> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.remark, "//
            + "data\\[\\d+\\]\\.amt, "//
            + "data\\[\\d+\\]\\.price, "//
            + "data\\[\\d+\\]\\.totalAmt, "//
            + "data\\[\\d+\\]\\.point, "//
            + "data\\[\\d+\\]\\.memberCode, "//
            + "data\\[\\d+\\]\\.channelId, "//
            + "data\\[\\d+\\]\\.datetimeAdd, "//
            + "data\\[\\d+\\]\\.memberId ";

    @ToUpperCase
    @ToTrim
    private String memberId;

    private String userType;

    private String channelId;

    private boolean giftListing;

    private String memberCode;

    private MemberService memberService;
    private PointService pointService;

    public PointReportListDatatablesAction() {
        memberService = Application.lookupBean(MemberService.class);
        pointService = Application.lookupBean(PointService.class);
    }

    @Action(value = "/pointReportListDatatables")
    @Accesses(access = {@Access(accessCode = AP.REPORT_POINT_MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        if (StringUtils.isBlank(memberId)) {
            Member member = memberService.findMemberByMemberCode(memberCode);
            if (member != null) {
                memberId = member.getMemberId();
            }
        }
        pointService.findPointTrxForDatagrid(getDatagridModel(), giftListing, channelId, memberId);
        getDatagridModel().getRecords().forEach(e -> {
            if (StringUtils.isBlank(e.getMemberCode())) {
                e.setMemberCode(memberService.getMember(giftListing ? e.getContributorId() : e.getMemberId()).getMemberCode());
            }
        });
//
//        obj.setPoint(new BigDecimal(rs.getString("totalPoint")));
//        obj.setMemberId(rs.getString("contributor_id"));
//        obj.setChannelId(rs.getString("channel_id"));

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------


    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public boolean isGiftListing() {
        return giftListing;
    }

    public void setGiftListing(boolean giftListing) {
        this.giftListing = giftListing;
    }

    public MemberService getMemberService() {
        return memberService;
    }

    public void setMemberService(MemberService memberService) {
        this.memberService = memberService;
    }

    public PointService getPointService() {
        return pointService;
    }

    public void setPointService(PointService pointService) {
        this.pointService = pointService;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
