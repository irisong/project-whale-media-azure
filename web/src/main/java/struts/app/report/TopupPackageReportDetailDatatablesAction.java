package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;

import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.RichTextString;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import struts.util.ExcelGeneratorUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                TopupPackageReportDetailDatatablesAction.JSON_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.STREAM, params = {"contentType", "application/vnd.ms-excel",
                "inputName", "excelStream", "contentDisposition", "attachment;filename=${excelFileName}", "bufferSize", "2048"})
})
public class TopupPackageReportDetailDatatablesAction extends BaseSecureDatatablesAction<HashMap<String,String>> {
    private static final long serialVersionUID = 1L;

    //content header
    private static final String TOPUP_PACKAGE_EXCEL_FILE_HEADER = "Package Amount;Transaction Date;Member ID;Topup Way";

    //file name
    public static final String SHEET_NAME = "Top Up Package Report";

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.packageAmount, "
            + "data\\[\\d+\\]\\.datetimeAdd, "
            + "data\\[\\d+\\]\\.memberCode, "
            + "data\\[\\d+\\]\\.remark ";

    @ToUpperCase
    @ToTrim
    private Date dateFrom;
    private Date dateTo;
    private int packageAmount;
    private String memberCode;
    private String topupWay;
    private InputStream excelStream;
    private String excelFileName;

    private WalletService walletService;

    public TopupPackageReportDetailDatatablesAction() {
        walletService = Application.lookupBean(WalletService.class);
    }

    @Action(value = "/topupPackageReportDetailDatatables")
    @Accesses(access = {@Access(accessCode = AP.REPORT_TOPUP_PACKAGE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        walletService.findTopupPackageDetailForListing2(getDatagridModel(), dateFrom, dateTo, packageAmount, memberCode, topupWay);
        return JSON;
    }

    @Action(value = "/topupPackageDetailReportExportExcel")
    @Access(accessCode = AP.REPORT_TOPUP_PACKAGE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String excel() throws Exception {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            List<WalletTrx> walletTrxes = walletService.findTopupPackageDetailByTopupWay(dateFrom, dateTo, packageAmount, memberCode, topupWay);

            this.initExcelFile();
            this.exportExcel(walletTrxes);
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return SUCCESS;
    }

    private void initExcelFile() {
        if(topupWay.equals("") && packageAmount == 0){
            this.setExcelFileName("Top Up Package Report ALL" + ".xls");
        } else{
            this.setExcelFileName("Top Up Package Report with " + topupWay + packageAmount +".xls");
        }
    }

    public void exportExcel(List<WalletTrx> walletTrxes) throws IOException {
        try{
            ExcelGeneratorUtil excel = new ExcelGeneratorUtil(TOPUP_PACKAGE_EXCEL_FILE_HEADER);
            /**
             *used HSSFWorkbook for Excel files in .xls format.
             **/
            excel.createHeader(SHEET_NAME);

            HSSFRow row;
            HSSFWorkbook wb = excel.getWb(); //wb = workbook
            HSSFSheet sheet = excel.getSheet(); //defined sheet name

            int i = 1;
            if(walletTrxes.size() > 0) {
                for(WalletTrx walletTrx : walletTrxes) {
                    row = sheet.createRow(i);

                    row.createCell(0).setCellValue(walletTrx.getInAmt().toString());
                    row.createCell(1).setCellValue(walletTrx.getDatetimeAdd().toString());
                    row.createCell(2).setCellValue(walletTrx.getMemberCode());
                    row.createCell(3).setCellValue(walletTrx.getRemark());
                    i++;
                }
                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
                sheet.autoSizeColumn(3);
            } else
            {
                row = sheet.createRow(i);
                row.createCell(0).setCellValue("No record found");
            }

            //step 7, stored document in streaming
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            wb.write(os);
            byte[] fileContent = os.toByteArray();
            ByteArrayInputStream is = new ByteArrayInputStream(fileContent);
            excelStream = is;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Date getDateFrom() { return dateFrom; }

    public void setDateFrom(Date dateFrom) { this.dateFrom = dateFrom; }

    public Date getDateTo() { return dateTo; }

    public void setDateTo(Date dateTo) { this.dateTo = dateTo; }

    public int getPackageAmount() { return packageAmount; }

    public void setPackageAmount(int packageAmount) { this.packageAmount = packageAmount; }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getTopupWay() {
        return topupWay;
    }

    public void setTopupWay(String topupWay) {
        this.topupWay = topupWay;
    }

    public InputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(InputStream excelStream) {
        this.excelStream = excelStream;
    }

    public String getExcelFileName() {
        return excelFileName;
    }

    public void setExcelFileName(String excelFileName) {
        this.excelFileName = excelFileName;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
