package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.DateUtil;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.wallet.dto.ActiveUserReport;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import struts.util.ExcelGeneratorUtil;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                ActiveUserReportListDatatablesAction.JSON_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.STREAM, params = {"contentType", "application/vnd.ms-excel",
                "inputName", "excelStream", "contentDisposition", "attachment;filename=${excelFileName}", "bufferSize", "2048"})
})
public class ActiveUserReportListDatatablesAction extends BaseSecureDatatablesAction<HashMap<String,String>> {
    private static final long serialVersionUID = 1L;

    //excel file content header
    private static final String ACTIVE_USER_REPORT_FILE_HEADER = "Date;Send Gift Per Unique User;View Live Stream Per Unique User;Daily Register User Count;Total Gift Price Sent(Whale Coin)";

    //excel file name
    public static final String SHEET_NAME = "Active User Report";

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.date, "
            + "data\\[\\d+\\]\\.sendGiftCount, "
            + "data\\[\\d+\\]\\.audienceCount, "
            + "data\\[\\d+\\]\\.newUserCount, "
            + "data\\[\\d+\\]\\.totalAmt ";

    private Date dateFrom;
    private Date dateTo;
    private WalletService walletService;
    private String excelFileName;
    private InputStream excelStream;

    public ActiveUserReportListDatatablesAction() { walletService = Application.lookupBean(WalletService.class); }

    @Action(value = "/activeUserReportDatatables")
    @Access(accessCode = AP.REPORT_ACTIVE_USER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        walletService.findActiveUserForListing(getDatagridModel(), dateFrom, dateTo);

        return JSON;
    }

    @Action("/activeUserReportExportExcel")
    @Accesses(access = {@Access(accessCode = AP.REPORT_ACTIVE_USER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true)})
    public String activeUserReportExportExcel() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);

            List<ActiveUserReport> activeUser = walletService.findActiveUser(dateFrom,dateTo);

            this.initExcelFile();
            this.exportExcel(activeUser);

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return SUCCESS;
    }

    private void initExcelFile() {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        this.setExcelFileName("ActiveUserReport_" + df.format(new Date()) + ".xls");
    }

    public void exportExcel(List<ActiveUserReport> activeUserReports) throws IOException {
        try{
            ExcelGeneratorUtil excel = new ExcelGeneratorUtil(ACTIVE_USER_REPORT_FILE_HEADER);
            /**
             *used HSSFWorkbook for Excel files in .xls format.
             **/
            excel.createHeader(SHEET_NAME);
            String dateFormat = "yyyy-MM-dd";
            HSSFRow row;
            HSSFWorkbook wb = excel.getWb(); //wb = workbook
            HSSFSheet sheet = excel.getSheet(); //defined sheet name

            int i = 1;
            if(activeUserReports.size() > 0) {
                for(ActiveUserReport activeUserReport : activeUserReports) {
                    row = sheet.createRow(i);
                    row.createCell(0).setCellValue(DateUtil.format(activeUserReport.getDate(),dateFormat));
                    row.createCell(1).setCellValue(activeUserReport.getSendGiftCount());
                    row.createCell(2).setCellValue(activeUserReport.getAudienceCount());
                    row.createCell(3).setCellValue(activeUserReport.getNewUserCount());
                    row.createCell(4).setCellValue(activeUserReport.getTotalAmt());
                    i++;
                }
                sheet.autoSizeColumn(0);
                sheet.autoSizeColumn(1);
                sheet.autoSizeColumn(2);
                sheet.autoSizeColumn(3);
                sheet.autoSizeColumn(4);
            } else
            {
                row = sheet.createRow(i);
                row.createCell(0).setCellValue("No record found");
            }

            //Stored document in streaming
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            wb.write(os);
            byte[] fileContent = os.toByteArray();
            ByteArrayInputStream is = new ByteArrayInputStream(fileContent);
            excelStream = is;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getExcelFileName() {
        return excelFileName;
    }

    public void setExcelFileName(String excelFileName) {
        this.excelFileName = excelFileName;
    }

    public InputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(InputStream excelStream) {
        this.excelStream = excelStream;
    }
// ---------------- GETTER & SETTER (END) -------------
}
