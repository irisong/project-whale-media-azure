package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "incomeReport"),
})
public class IncomeReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String guildName;

    private List<OptionBean> allMonthList = new ArrayList<>();

    private List<OptionBean> allYearList = new ArrayList<>();

    public IncomeReportAction() { }

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allMonthList = optionBeanUtil.getMonthWithOptionAll();
        allYearList = optionBeanUtil.getYearWithOptionAll();
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_INCOME_REPORT})
    @Action(value = "/incomeReport")
    @Access(accessCode = AP.REPORT_INCOME, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        initialize();
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public List<OptionBean> getAllMonthList() {
        return allMonthList;
    }

    public void setAllMonthList(List<OptionBean> allMonthList) {
        this.allMonthList = allMonthList;
    }

    public List<OptionBean> getAllYearList() {
        return allYearList;
    }

    public void setAllYearList(List<OptionBean> allYearList) {
        this.allYearList = allYearList;
    }

    // ---------------- GETTER & SETTER (END) -------------
}