package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "OMCWalletHistoryReport"),
})
public class OMCWalletHistoryReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Date DateFrom;
    private Date DateTo;
    private List<OptionBean> OMCRemark = new ArrayList<>();


    public OMCWalletHistoryReportAction() { }

    private void init() {
        //For show the dropdownlist selection
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        OMCRemark = optionBeanUtil.getOMCTrxHistoryMarkWithOptionAll();
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_OMC_Wallet_HISTORY_REPORT})
    @Action(value = "/OMCWalletHistoryReport")
    @Access(accessCode = AP.REPORT_OMC_WALLET_HISTORY, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Date getDateFrom() {
        return DateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        DateFrom = dateFrom;
    }

    public Date getDateTo() {
        return DateTo;
    }

    public void setDateTo(Date dateTo) {
        DateTo = dateTo;
    }

    public List<OptionBean> getAllRemark() {
        return OMCRemark;
    }

    public void setAllRemark(List<OptionBean> allRemark) {
        this.OMCRemark = allRemark;
    }
}
