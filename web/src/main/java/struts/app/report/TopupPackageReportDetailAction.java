package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "topupPackageReportDetail"),
})
public class TopupPackageReportDetailAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Date filterDateFrom;
    private Date filterDateTo;
    private int packageAmount;

    private List<OptionBean> allTopupwayList = new ArrayList<>();

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allTopupwayList = optionBeanUtil.getTopupwayWithOptionAll();
    }

    public TopupPackageReportDetailAction() { }

    @EnableTemplate(menuKey = {MP.FUNC_AD_TOPUP_PACKAGE_REPORT})
    @Action(value = "/topupPackageReportDetail")
    @Access(accessCode = AP.REPORT_TOPUP_PACKAGE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        initialize();
        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------


    public List<OptionBean> getAllTopupwayList() {
        return allTopupwayList;
    }

    public void setAllTopupwayList(List<OptionBean> allTopupwayList) {
        this.allTopupwayList = allTopupwayList;
    }

    public Date getFilterDateFrom() {
        return filterDateFrom;
    }

    public void setFilterDateFrom(Date filterDateFrom) {
        this.filterDateFrom = filterDateFrom;
    }

    public Date getFilterDateTo() {
        return filterDateTo;
    }

    public void setFilterDateTo(Date filterDateTo) {
        this.filterDateTo = filterDateTo;
    }

    public int getPackageAmount() { return packageAmount; }

    public void setPackageAmount(int packageAmount) { this.packageAmount = packageAmount; }
    // ---------------- GETTER & SETTER (END) -------------
}