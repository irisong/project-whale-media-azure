package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.income.IncomeProvider;
import com.compalsolutions.compal.income.dto.IncomeProviderDto;
import com.compalsolutions.compal.income.service.IncomeService;
import com.compalsolutions.compal.income.vo.IncomeReport;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.ORWrapper;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.core.env.Environment;
import struts.pub.WhaleMediaWebpageAction;
import struts.util.ExcelGeneratorUtil;

import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Results(value = {
        @Result(name = BaseAction.SEARCH, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                IncomeReportListDatatablesAction.JSON_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = BaseAction.PRINT, type = BaseAction.ResultType.JASPER, params = { //
                "location", "/WEB-INF/jasperreports/incomeProviderReport.jasper", //
                "contentDisposition", "attachment;filename=${filename}", //
                "dataSource", "incomeProviderDto", //
                "wrapField", "false",
                "dirPath", "dirPath",
                "reportParameters", "reportParameters"}),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.STREAM, params = {"contentType", "application/vnd.ms-excel",
                "inputName", "excelStream", "contentDisposition", "attachment;filename=${excelFileName}", "bufferSize", "2048"})
})
public class IncomeReportListDatatablesAction extends BaseSecureDatatablesAction<IncomeReport> {
    private static final long serialVersionUID = 1L;


    private static String INCOME_EXCEL_FILE_HEADER = "Guild Name;Rank Name;Member Profile Name;" +
            "Whale Live Id; Day Earn;" +
            "Total Day;Duration Earn(Hours(minutes));"+
            "Total Duration(Hours(minutes));Total Point;" +
            "Basic Salary;Income Remark;" +
            "Withdrawable BWP;Total Available BWP";


    public static final String SHEET_NAME = "Guild Income";

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.guildId, "
            + "data\\[\\d+\\]\\.memberId, "
            + "data\\[\\d+\\]\\.point, "
            + "data\\[\\d+\\]\\.pointEarn, "
            + "data\\[\\d+\\]\\.totalIncome, "
            + "data\\[\\d+\\]\\.whaleLiveId, "
            + "data\\[\\d+\\]\\.profileName, "
            + "data\\[\\d+\\]\\.incomeMonth, "
            + "data\\[\\d+\\]\\.basicSalary, "
            + "data\\[\\d+\\]\\.rankName, "
            + "data\\[\\d+\\]\\.trxDate ";

    @ToUpperCase
    @ToTrim

    private BroadcastService broadcastService;
    private IncomeService incomeService;
    private MemberService memberService;
    private String guildName;
    private String memberId;

    private String agentId;

    private String incomeMonth;
    private String incomeYear;

    private InputStream excelStream;
    private String excelFileName;

    private String dirPath;
    private String filename;
    private IncomeProviderDto incomeProviderDto = new IncomeProviderDto();

    private IncomeProvider incomeProvider;

    public IncomeReportListDatatablesAction() {
        Environment env = Application.lookupBean(Environment.class);
        dirPath = env.getProperty("jasper.whalemedia.path");
        incomeService = Application.lookupBean(IncomeService.class);
        incomeProvider = Application.lookupBean(IncomeProvider.class);
        memberService = Application.lookupBean(MemberService.class);
        broadcastService = Application.lookupBean(BroadcastService.class);

        SqlDatagridModel<IncomeReport> model = new SqlDatagridModel<>(new ORWrapper(new IncomeReport(false), "i"));
        model.addJoinTable(new ORWrapper(new Member(false), "m"));
        model.addJoinTable(new ORWrapper(new MemberDetail(false), "md"));
        model.addJoinTable(new ORWrapper(new BroadcastGuild(false), "g"));
        model.addJoinTable(new ORWrapper(new BroadcastGuildMember(false), "gm"));
        model.addJoinTable(new ORWrapper(new BroadcastConfig(false), "bc"));
        setDatagridModel(model);
    }

    @Action(value = "/incomeReportDatatables")
    @Accesses(access = {@Access(accessCode = AP.REPORT_INCOME, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        incomeService.findIncomeForReport(getDatagridModel(), guildName,incomeMonth,incomeYear);

        return SEARCH;
    }

    @Action(value = "/guildIncomeReportDatatables")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String execute2() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        BroadcastGuild broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

        incomeService.findIncomeForReport(getDatagridModel(), broadcastGuild.getName(),incomeMonth,incomeYear);

        return SEARCH;
    }

    @Action(value = "/incomeSummaryExportExcel")
    @Accesses(access = {@Access(accessCode = AP.REPORT_INCOME, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    public String excel() throws Exception {
        try {

            List<IncomeReport> incomeReports = incomeService.findIncomeReportByGuildAndMonth(guildName,incomeMonth,incomeYear);
            String name = "";
            Date genDate = new Date();
            if(incomeReports.size()>0)
            {
                name = incomeReports.get(0).getGuildDisplayId();
                genDate = incomeReports.get(0).getDatetimeAdd();
            }

            this.initExcelFile(name);
            this.exportExcel(incomeReports,guildName,incomeMonth,genDate);

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return SUCCESS;

    }

    @Action(value = "/clientIncomeSummaryExportExcel")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String excel2() throws Exception {
        try {

            LoginInfo loginInfo = getLoginInfo();
            if (loginInfo.getUserType() instanceof AgentUserType) {
                agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
            }

            BroadcastGuild broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

            List<IncomeReport> incomeReports = incomeService.findIncomeReportByGuildAndMonth(broadcastGuild.getName(),incomeMonth,incomeYear);
            String name = "";
            Date genDate = new Date();
            if(incomeReports.size()>0)
            {
                name = incomeReports.get(0).getGuildDisplayId();
                genDate = incomeReports.get(0).getDatetimeAdd();
            }

            this.initExcelFile(name);
            this.exportExcel(incomeReports,broadcastGuild.getName(),incomeMonth,genDate);

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return SUCCESS;

    }

    private void initExcelFile(String guildDisplayId) {
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        this.setExcelFileName("Income_"+guildDisplayId+"_" + df.format(new Date()) + ".xls");
    }

    public void exportExcel(List<IncomeReport> incomeReports,String guildName,String incomeMonth,Date genDate) {
        try {
            String month = incomeMonth;
            Date now = new Date();
            DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
            boolean sameMonth = now.getMonth() == Integer.parseInt(incomeMonth)-1;

            LoginInfo loginInfo = getLoginInfo();

            User user1 = loginInfo.getUser();

            String header = INCOME_EXCEL_FILE_HEADER;

            if(user1.getUserType().equalsIgnoreCase(Global.UserType.GM))
            {
                header = header.replace("Total Duration(Hours(minutes));",";");
                header = header.replace("Total Day;",";");

            }


            if(sameMonth) {
                header = header.replace("Withdrawable BWP;Total Available BWP","");
            }

            ExcelGeneratorUtil excel = new ExcelGeneratorUtil(header);
            /**
             *used HSSFWorkbook for Excel files in .xls format.
             **/
            excel.createHeader(SHEET_NAME);
            HSSFRow row;
            HSSFWorkbook wb = excel.getWb();
//          defined sheet name
            HSSFSheet sheet = excel.getSheet();
            sheet.autoSizeColumn(0);
            sheet.autoSizeColumn(1);
            sheet.autoSizeColumn(2);
            sheet.autoSizeColumn(3);
            sheet.autoSizeColumn(4);
            sheet.autoSizeColumn(5);
            sheet.autoSizeColumn(6);
            sheet.autoSizeColumn(7);
            sheet.autoSizeColumn(8);
            sheet.autoSizeColumn(9);
            sheet.autoSizeColumn(10);
            sheet.autoSizeColumn(11);
            sheet.autoSizeColumn(12);


            int i = 1;
            if(incomeReports.size()>0) {
                DecimalFormat df2 = new DecimalFormat("#.##");
                for (IncomeReport incomeReport : incomeReports) {
                    row = sheet.createRow(i);
                    row.createCell(0).setCellValue(incomeReport.getGuildName());
                    row.createCell(1).setCellValue(incomeReport.getRankName());
                    row.createCell(2).setCellValue(incomeReport.getProfileName());
                    row.createCell(3).setCellValue(incomeReport.getWhaleLiveId());
                    row.createCell(4).setCellValue(incomeReport.getDay().toString());



                    int duration = incomeReport.getDuration();
                    int hours = duration / 60;
                    int minutes = duration % 60;
                    row.createCell(6).setCellValue( hours+"("+minutes+")");




                    row.createCell(8).setCellValue(incomeReport.getPoint().toString());

                    if(!user1.getUserType().equalsIgnoreCase(Global.UserType.GM))
                    {
                        row.createCell(5).setCellValue(incomeReport.getDayArchieved().toString());
                        int duration2 = incomeReport.getDurationArchieved();
                        int hours2 = duration2 / 60;
                        int minutes2 = duration2 % 60;
                        row.createCell(7).setCellValue(hours2+"("+minutes2+")");
                    }


                    if(!sameMonth) {
                        row.createCell(9).setCellValue(incomeReport.getBasicSalary().setScale(2, RoundingMode.CEILING).toString());
                        row.createCell(11).setCellValue(incomeReport.getWithdrawableBWP().setScale(2, RoundingMode.CEILING).toString());
                        row.createCell(12).setCellValue(incomeReport.getTotalAvailableBWP().setScale(2, RoundingMode.CEILING).toString());
                    }
                    row.createCell(10).setCellValue(incomeReport.getIncomeRemark());

                    i++;


                }
                month = incomeReports.get(0).getIncomeMonth();
            }else
            {
                row = sheet.createRow(i);
                row.createCell(0).setCellValue("No record found");
            }

            i=i+4;

            row = sheet.createRow(i);
            row.createCell(0).setCellValue("Guild Name:");
            row.createCell(1).setCellValue(guildName);

            i=i+1;

            row = sheet.createRow(i);
            row.createCell(0).setCellValue("Month:");
            row.createCell(1).setCellValue(month);

            i=i+1;

            row = sheet.createRow(i);
            row.createCell(0).setCellValue("Report Generate Date:");
            row.createCell(1).setCellValue(df.format(genDate));

            //step 7, stored document in streaming
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            wb.write(os);
            byte[] fileContent = os.toByteArray();
            ByteArrayInputStream is = new ByteArrayInputStream(fileContent);
            excelStream = is;
        } catch (
                Exception e) {
            e.printStackTrace();
        }

    }


    @Action("/generateTemporaryIncomeReport")
    public String generateIncomeReport() throws Exception {
        try {
            incomeProvider.doGenTempIncomeReport();
            successMessage = "Generate Income Report Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;

    }

    @Action("/clientGenerateTemporaryIncomeReport")
    public String generateClientIncomeReport() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();
            if (loginInfo.getUserType() instanceof AgentUserType) {
                agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
            }

            BroadcastGuild broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

            incomeProvider.doGenClientTempIncomeReport(broadcastGuild.getId());
            Date now = new Date();
            String month = new SimpleDateFormat("MM").format(now);
            String year =  new SimpleDateFormat("yyyy").format(now);

            List<IncomeReport> incomeReports = incomeService.findIncomeReportByGuildAndMonth(broadcastGuild.getName(),month,year);
            String name = "";
            Date genDate = new Date();
            if(incomeReports.size()>0)
            {
                name = incomeReports.get(0).getGuildDisplayId();
                genDate = incomeReports.get(0).getDatetimeAdd();
            }

            this.initExcelFile(name);
            this.exportExcel(incomeReports,broadcastGuild.getName(),month,genDate);

            successMessage = "Generate Income Report Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return SUCCESS;

    }
    // ---------------- GETTER & SETTER (START) -------------

    public String getGuildName() {
        return guildName;
    }

    public void setGuildName(String guildName) {
        this.guildName = guildName;
    }

    public String getIncomeMonth() {
        return incomeMonth;
    }

    public void setIncomeMonth(String incomeMonth) {
        this.incomeMonth = incomeMonth;
    }

    public String getIncomeYear() {
        return incomeYear;
    }

    public void setIncomeYear(String incomeYear) {
        this.incomeYear = incomeYear;
    }

    public InputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(InputStream excelStream) {
        this.excelStream = excelStream;
    }

    public String getExcelFileName() {
        return excelFileName;
    }

    public void setExcelFileName(String excelFileName) {
        this.excelFileName = excelFileName;
    }

    public String getDirPath() {
        return dirPath;
    }

    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public IncomeProviderDto getIncomeProviderDto() {
        return incomeProviderDto;
    }

    public void setIncomeProviderDto(IncomeProviderDto incomeProviderDto) {
        this.incomeProviderDto = incomeProviderDto;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
