package struts.app.report;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;

import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;
import java.util.HashMap;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                PrizeReportListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class PrizeReportListDatatablesAction extends BaseSecureDatatablesAction<HashMap<String,String>> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.remark, "
            + "data\\[\\d+\\]\\.datetimeAdd, "
            + "data\\[\\d+\\]\\.amt, "
            + "data\\[\\d+\\]\\.price, "
            + "data\\[\\d+\\]\\.totalAmt, "
            + "data\\[\\d+\\]\\.memberCode ";

    @ToUpperCase
    @ToTrim

    private String remark;
    private WalletService walletService;

    public PrizeReportListDatatablesAction() {
        walletService = Application.lookupBean(WalletService.class);
    }

    @Action(value = "/prizeReportDatatables")
    @Accesses(access = {@Access(accessCode = AP.REPORT_PRIZE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        walletService.findPrizeForReport(getDatagridModel(), remark);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    // ---------------- GETTER & SETTER (END) -------------
}