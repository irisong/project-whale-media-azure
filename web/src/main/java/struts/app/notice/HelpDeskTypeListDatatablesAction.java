package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                HelpDeskTypeListDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class HelpDeskTypeListDatatablesAction extends BaseSecureDatatablesAction<HelpDeskType> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.ticketTypeId, "
            + "data\\[\\d+\\]\\.typeName, "
            + "data\\[\\d+\\]\\.typeNameCn, "
            + "data\\[\\d+\\]\\.typeNameMs, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.sortOrder, "
            + "data\\[\\d+\\]\\.datetimeAdd";

    private HelpDeskService helpDeskService;

    @ToTrim
    @ToUpperCase
    private String status;

    public HelpDeskTypeListDatatablesAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
    }

    @Action(value = "/helpDeskTypeListDatatables")
    @Access(accessCode = AP.HELPDESK_TYPE, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        helpDeskService.findHelpDeskTypeForListing(getDatagridModel(), status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}