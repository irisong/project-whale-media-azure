package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.FreqAskQuesService;
import com.compalsolutions.compal.general.vo.FreqAskQues;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", FreqAskQuesListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class FreqAskQuesListDatatablesAction extends BaseSecureDatatablesAction<FreqAskQues> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + "," //
            + "data\\[\\d+\\]\\.questionId, "
            + "data\\[\\d+\\]\\.datetimeAdd, "
            + "data\\[\\d+\\]\\.title, "
            + "data\\[\\d+\\]\\.titleCn, "
            + "data\\[\\d+\\]\\.titleMs, "
            + "data\\[\\d+\\]\\.sortOrder, "
            + "data\\[\\d+\\]\\.freqAskQuesFileUrlEn, "
            + "data\\[\\d+\\]\\.freqAskQuesFileUrlCn, "
            + "data\\[\\d+\\]\\.freqAskQuesFileUrlMs, "
            + "data\\[\\d+\\]\\.freqAskQuesFileUrlCombine, "
            + "data\\[\\d+\\]\\.status ";

    private String status;

    private FreqAskQuesService freqAskQuesService;

    public FreqAskQuesListDatatablesAction() {
        freqAskQuesService = Application.lookupBean(FreqAskQuesService.BEAN_NAME, FreqAskQuesService.class);
    }

    @Action(value = "/freqAskQuesListDatatables")
    @Accesses(access = {@Access(accessCode = AP.FAQ, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true)})
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimProperties(this);

        try {
            freqAskQuesService.findFreqAskListForListing(getDatagridModel(), status);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
