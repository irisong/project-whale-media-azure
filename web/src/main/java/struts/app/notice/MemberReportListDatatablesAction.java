package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberReportService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberReport;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                MemberReportListDatatablesAction.JSON_INCLUDE_PROPERTIES})})

public class MemberReportListDatatablesAction extends BaseSecureDatatablesAction<MemberReport> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.reportId, "
            + "data\\[\\d+\\]\\.datetimeAdd, "
            + "data\\[\\d+\\]\\.memberReportType\\.typeName, "
            + "data\\[\\d+\\]\\.memberCode, "
            + "data\\[\\d+\\]\\.description, "
            + "data\\[\\d+\\]\\.readStatus, "
            + "data\\[\\d+\\]\\.warningCount";

    private MemberReportService memberReportService;
    private MemberService memberService;

    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String memberReportType;

    @ToTrim
    private String reportMemberId;
    private String readStatus;

    public MemberReportListDatatablesAction() {
        memberReportService = Application.lookupBean(MemberReportService.BEAN_NAME, MemberReportService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/memberReportListDatatables")
    @Accesses(access = {@Access(accessCode = AP.MEMBER_REPORT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true)})
    public String execute() throws Exception{
        VoUtil.toTrimUpperCaseProperties(this);

        System.out.println(readStatus);
        Boolean isReadByAdmin = StringUtils.isNotBlank(readStatus) ? Boolean.valueOf(readStatus) : null;
        memberReportService.findMemberReportForListing(getDatagridModel(), dateFrom, dateTo, memberReportType, reportMemberId, isReadByAdmin);

        Member member;
        for (MemberReport memberReport : getDatagridModel().getRecords()) {
            member = memberService.getMember(memberReport.getReportMemberId());
            memberReport.setMemberCode(member.getMemberCode());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getMemberReportType() {
        return memberReportType;
    }

    public void setMemberReportType(String memberReportType) {
        this.memberReportType = memberReportType;
    }

    public String getReportMemberId() {
        return reportMemberId;
    }

    public void setReportMemberId(String reportMemberId) {
        this.reportMemberId = reportMemberId;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
