package struts.app.notice;

import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import struts.util.AjaxUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                AnnounceListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class AnnounceListDatatablesAction extends BaseSecureDatatablesAction<Announcement> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.announceId, "
            + "data\\[\\d+\\]\\.publishDate, "
            + "data\\[\\d+\\]\\.title, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.announcementType, "
            + "data\\[\\d+\\]\\.userGroups, "
            + "data\\[\\d+\\]\\.datetimeAdd";

    private AnnouncementService announcementService;

    private String userGroups;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String announcementType;

    public AnnounceListDatatablesAction() {
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
    }

    @Action(value = "/announceListDatatables")
    @Access(accessCode = AP.ANNOUNCE, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        List<String> userGroupsList = AjaxUtil.stringArrayToList(userGroups);
        announcementService.findAnnouncementsForListing(getDatagridModel(), userGroupsList, status, announcementType, dateFrom, dateTo);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAnnouncementType() {
        return announcementType;
    }

    public void setAnnouncementType(String announcementType) {
        this.announcementType = announcementType;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    // ---------------- GETTER & SETTER (END) -------------
}