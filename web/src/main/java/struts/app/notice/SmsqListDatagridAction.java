package struts.app.notice;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.SmsQueueService;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", SmsqListDatagridAction.JSON_INCLUDE_PROPERTIES }) })

public class SmsqListDatagridAction extends BaseDatagridAction<SmsQueue> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.smsId, " //
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.smsTo, " //
            + "rows\\[\\d+\\]\\.body, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private SmsQueueService smsQueueService;

    @ToTrim
    @ToUpperCase
    private String status;
    private String smsTo;

    public SmsqListDatagridAction() {
        smsQueueService = Application.lookupBean(SmsQueueService.BEAN_NAME, SmsQueueService.class);
    }

    @Action(value = "/smsqListDatagrid")
    @Access(accessCode = AP.SMSQ, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        smsQueueService.findSmsqForListing(getDatagridModel(), smsTo, status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSmsTo() {
        return smsTo;
    }

    public void setSmsTo(String smsTo) {
        this.smsTo = smsTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}