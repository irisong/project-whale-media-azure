package struts.app.notice;

import java.util.*;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.general.service.MessageService;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "announceList"),
        @Result(name = BaseAction.ADD, location = "announceAdd"),
        @Result(name = BaseAction.ADD_DETAIL, location = "announceAdd"),
        @Result(name = BaseAction.EDIT, location = "announceEdit"),
        @Result(name = BaseAction.SHOW, location = "announceShow"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class AnnounceAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_ANNOUNCE_LIST = "announceList";

    private AnnouncementService announcementService;
    private MessageService messageService;

    private List<OptionBean> statuses = new ArrayList<>();
    private List<OptionBean> publishGroups = new ArrayList<>();
    private List<OptionBean> languages = new ArrayList<>();
    private List<OptionBean> announcementTypes = new ArrayList<>();

    private Announcement announcement = new Announcement(true);

    private List<String> userGroups = new ArrayList<>();

    @ToTrim
    private String announcementId;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String announcementType;

    private boolean blnAddLanguage = false;
    private boolean blnChooseLanguage = false;
    private boolean disablePublishDate = false;
    private boolean displayCn = false;

    private String languageCode = "";
    private String addLanguageTitle;
    private String addLanguageBody;

    private boolean clear;

    public AnnounceAction() {
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
        messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceList")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatusWithOptionAll();
        languages = optionBeanUtil.getLanguagesWithOptionAll();
        publishGroups = optionBeanUtil.getPublishGroups();
        announcementTypes = optionBeanUtil.getAnnouncementTypeWithOptionAll();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_ANNOUNCE_LIST);
        } else if (session.containsKey(SESSION_ANNOUNCE_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_ANNOUNCE_LIST);
        } else {
            session.put(SESSION_ANNOUNCE_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("status", status);
            objectMap.put("userGroups", userGroups);
            objectMap.put("dateFrom", dateFrom);
            objectMap.put("dateTo", dateTo);
        } else {
            status = (String) objectMap.get("status");
            userGroups = (List<String>) objectMap.get("userGroups");
            dateFrom = (Date) objectMap.get("dateFrom");
            dateTo = (Date) objectMap.get("dateTo");
        }

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceAdd")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatus();
        languages = optionBeanUtil.getLanguages();
        publishGroups = optionBeanUtil.getPublishGroups();
        announcementTypes = optionBeanUtil.getAnnouncementType();
        languageCode = Global.Language.ENGLISH;

        return ADD;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceAddLanguage")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true)
    public String addLanguage() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatus();
        languages = optionBeanUtil.getLanguages();
        publishGroups = optionBeanUtil.getPublishGroups();
        announcementTypes = optionBeanUtil.getAnnouncementType();

        announcement = announcementService.getAnnouncement(announcement.getAnnounceId());

        if (announcement.getStatus().equals(Global.Status.PENDING))
            announcement.setStatus(Global.Status.ACTIVE); // send notification using scheduler

        String[] groups = StringUtils.split(announcement.getUserGroups(), "|");
        for (String g : groups) {
            userGroups.add(g);
        }

        return ADD_DETAIL;
    }

    @Action(value = "/announceSave")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, announcement);
            String groups = "";
            if (languageCode.equalsIgnoreCase(Global.Language.ENGLISH)) {
                String announceId = announcement.getAnnounceId();
                if(StringUtils.isBlank(announceId)) {  //add new
                    for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext(); ) {
                        groups += iterator.next();
                        if (iterator.hasNext())
                            groups += "|";
                    }
                    announcement.setUserGroups(groups);

                    if (announcement.getStatus().equals(Global.Status.ACTIVE))
                        announcement.setStatus(Global.Status.PENDING); // send notification using scheduler

                    announcementService.saveAnnouncement(getLocale(), announcement);
                }
                else {  //update
                    announcement = announcementService.getAnnouncement(announceId);

                    if (announcement.getStatus().equals(Global.Status.ACTIVE))
                        announcement.setStatus(Global.Status.PENDING); // send notification using scheduler

                    announcement.setTitle(addLanguageTitle);
                    announcement.setBody(addLanguageBody);

                    announcementService.updateAnnouncement(getLocale(), announcement);
                }
            }
            else {
                announcement = announcementService.getAnnouncement(announcement.getAnnounceId());

                if (announcement.getStatus().equals(Global.Status.ACTIVE))
                    announcement.setStatus(Global.Status.PENDING); // send notification using scheduler

                if (languageCode.equalsIgnoreCase(Global.Language.CHINESE)) {
                    announcement.setTitleCn(addLanguageTitle);
                    announcement.setBodyCn(addLanguageBody);
                }

                announcementService.updateAnnouncement(getLocale(), announcement);
            }

            announcementId = announcement.getAnnounceId();
            languageCode = "";
            blnChooseLanguage = true;

            successMessage = getText("successMessage.AnnounceAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/announceAddExit")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, createMode = true)
    public String exit() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, announcement);
            announcementId = announcement.getAnnounceId();
            if (announcementId != null) {
                announcement = announcementService.getAnnouncement(announcement.getAnnounceId());

                if (announcement.getStatus().equals(Global.Status.PENDING) && !announcement.getPublishDate().after(new Date())) { // not future notification
                    announcementService.doProcessAnnouncementNotification(announcement);

                    announcement.setStatus(Global.Status.ACTIVE);
                    announcementService.updateAnnouncement(new Locale("en"), announcement);
                }
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_ANNOUNCE)
    @Action(value = "/announceEdit")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, updateMode = true)
    public String edit() {
        try {
            announcement = announcementService.getAnnouncement(announcement.getAnnounceId());
            if (announcement == null) {
                addActionError(getText("invalidAnnouncement"));
                return list();
            }

            String[] groups = StringUtils.split(announcement.getUserGroups(), "|");
            for (String g : groups) {
                userGroups.add(g);
            }

            //not allow to change publish date after send phone notification
            if (!announcement.getPublishDate().after(new Date()) || announcement.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                disablePublishDate = true;
            }
            //change pending to active
            if (announcement.getStatus().equalsIgnoreCase(Global.Status.PENDING)) {
                announcement.setStatus(Global.Status.ACTIVE);
            }

//            String[] languageSet;
//            List<String[]> languageList = new ArrayList<>();
//            if (announcement.getTitleCn() != null) {
//                languageSet = new String[]{Global.Language.CHINESE, announcement.getTitleCn(), announcement.getBodyCn()};
//                languageList.add(languageSet);
//            }
//            announcement.setLanguageSet(languageList);

            if (StringUtils.isNotBlank(announcement.getTitleCn())) {
                displayCn = true;
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getAnnouncementStatus();
            languages = optionBeanUtil.getLanguages();
            publishGroups = optionBeanUtil.getPublishGroups();
            announcementTypes = optionBeanUtil.getAnnouncementType();
            languageCode = Global.Language.CHINESE; //temporary hard code

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/announceUpdate")
    @Access(accessCode = AP.ANNOUNCE, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, announcement);

            String groups = "";
            for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext();) {
                groups += iterator.next();
                if (iterator.hasNext())
                    groups += "|";
            }
            announcement.setUserGroups(groups);

//            if (announcement.getLanguageSet() != null) {
//                for (String[] languageSets : announcement.getLanguageSet()) {
//                    if (languageSets[0].equalsIgnoreCase(Global.Language.CHINESE)) {
//                        announcement.setTitleCn(languageSets[1]);
//                        announcement.setBodyCn(languageSets[2]);
//                    }
//                }
//            }

            if (announcement.getStatus().equals(Global.Status.ACTIVE) && announcement.getPublishDate().after(new Date()))
                announcement.setStatus(Global.Status.PENDING); // set back Pending to allow send notification using scheduler

            announcementService.updateAnnouncement(getLocale(), announcement);
            messageService.doUpdateAnnouncementMessage(announcement); // update message

            successMessage = getText("successMessage.AnnounceAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/announceShow")
    public String show() {
        announcement = announcementService.getAnnouncement(announcement.getAnnounceId());

        String[] languageSet;
        List<String[]> languageList = new ArrayList<>();
        if (announcement.getTitleCn() != null) {
            languageSet = new String[]{Global.Language.CHINESE, announcement.getTitleCn(), announcement.getBodyCn()};
            languageList.add(languageSet);
            displayCn = true;
        }
        announcement.setLanguageSet(languageList);
        return SHOW;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public List<OptionBean> getAnnouncementTypes() {
        return announcementTypes;
    }

    public void setAnnouncementTypes(List<OptionBean> announcementTypes) {
        this.announcementTypes = announcementTypes;
    }

    public List<OptionBean> getPublishGroups() {
        return publishGroups;
    }

    public void setPublishGroups(List<OptionBean> publishGroups) {
        this.publishGroups = publishGroups;
    }

    public Announcement getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(Announcement announcement) {
        this.announcement = announcement;
    }

    public String getAnnouncementId() {
        return announcementId;
    }

    public void setAnnouncementId(String announcementId) {
        this.announcementId = announcementId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAnnouncementType() {
        return announcementType;
    }

    public void setAnnouncementType(String announcementType) {
        this.announcementType = announcementType;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public boolean isBlnAddLanguage() {
        return blnAddLanguage;
    }

    public void setBlnAddLanguage(boolean blnAddLanguage) {
        this.blnAddLanguage = blnAddLanguage;
    }

    public boolean isBlnChooseLanguage() {
        return blnChooseLanguage;
    }

    public void setBlnChooseLanguage(boolean blnChooseLanguage) {
        this.blnChooseLanguage = blnChooseLanguage;
    }

    public boolean isDisablePublishDate() {
        return disablePublishDate;
    }

    public void setDisablePublishDate(boolean disablePublishDate) {
        this.disablePublishDate = disablePublishDate;
    }

    public boolean isDisplayCn() {
        return displayCn;
    }

    public void setDisplayCn(boolean displayCn) {
        this.displayCn = displayCn;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getAddLanguageTitle() {
        return addLanguageTitle;
    }

    public void setAddLanguageTitle(String addLanguageTitle) {
        this.addLanguageTitle = addLanguageTitle;
    }

    public String getAddLanguageBody() {
        return addLanguageBody;
    }

    public void setAddLanguageBody(String addLanguageBody) {
        this.addLanguageBody = addLanguageBody;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
