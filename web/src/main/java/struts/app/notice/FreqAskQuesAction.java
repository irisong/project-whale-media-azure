package struts.app.notice;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.FreqAskQuesService;
import com.compalsolutions.compal.general.vo.FreqAskQues;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.File;
import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "freqAskQuesList"),
        @Result(name = BaseAction.ADD, location = "freqAskQuesAdd"),
        @Result(name = BaseAction.EDIT, location = "freqAskQuesEdit"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default})
})
public class FreqAskQuesAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_FREQ_ASK_QUES_LIST = "freqAskQuesList";

    private FreqAskQuesService freqAskQuesService;

    private List<OptionBean> statuses = new ArrayList<>();
    private List<OptionBean> statusOption = new ArrayList<>();

    private FreqAskQues freqAskQues = new FreqAskQues();
    private FreqAskQuesFile freqAskQuesFile;

    @ToTrim
    @ToUpperCase
    private String status;

    private boolean clear;

    // Files upload
    private File enFileUpload;
    private File cnFileUpload;
    private File msFileUpload;
    private File combineFileUpload;

    private String enFileUploadFileName;
    private String cnFileUploadFileName;
    private String msFileUploadFileName;
    private String combineFileUploadFileName;

    private String enFileUploadContentType;
    private String cnFileUploadContentType;
    private String msFileUploadContentType;
    private String combineFileUploadContentType;

    private String questionId;
    private String faqDeleteLanguage;

    public FreqAskQuesAction() {
        freqAskQuesService = Application.lookupBean(FreqAskQuesService.BEAN_NAME, FreqAskQuesService.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getDocFileStatusWithOptionAll();
        statusOption = optionBeanUtil.getDocFileStatus();
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_FAQ)
    @Action(value = "/freqAskQuesList")
    @Access(accessCode = AP.FAQ, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_FREQ_ASK_QUES_LIST);
        } else if (session.containsKey(SESSION_FREQ_ASK_QUES_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_FREQ_ASK_QUES_LIST);
        } else {
            session.put(SESSION_FREQ_ASK_QUES_LIST, objectMap);
        }

        objectMap.put("status", status);

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_FAQ)
    @Action(value = "/freqAskQuesAdd")
    @Access(accessCode = AP.FAQ, adminMode = true, createMode = true)
    public String add() {
        init();
        freqAskQues.setStatus(Global.Status.ACTIVE);

        return ADD;
    }

    @Action(value = "/freqAskQuesSave")
    @Access(accessCode = AP.FAQ, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, freqAskQues);

            freqAskQuesService.doSaveNewFreqAskQues(getLocale(), freqAskQues, getFaqFileList());
            successMessage = getText("successMessage.freqAskQues.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return add();
        }

        return JSON;
    }

    public List<FreqAskQuesFile> getFaqFileList() {
        List<FreqAskQuesFile> freqAskQuesFileList = new ArrayList<>();
        if (enFileUpload != null)
            freqAskQuesFileList.add(setFaqFile(FreqAskQuesFile.LANG_EN, enFileUpload, enFileUploadFileName, enFileUploadContentType));
        if (cnFileUpload != null)
            freqAskQuesFileList.add(setFaqFile(FreqAskQuesFile.LANG_CN, cnFileUpload, cnFileUploadFileName, cnFileUploadContentType));
        if (msFileUpload != null)
            freqAskQuesFileList.add(setFaqFile(FreqAskQuesFile.LANG_MS, msFileUpload, msFileUploadFileName, msFileUploadContentType));
        if (combineFileUpload != null)
            freqAskQuesFileList.add(setFaqFile(FreqAskQuesFile.COMBINED, combineFileUpload, combineFileUploadFileName, combineFileUploadContentType));

        return freqAskQuesFileList;
    }

    public FreqAskQuesFile setFaqFile(String language, File fileUpload, String fileName, String contentType) {
        freqAskQuesFile = new FreqAskQuesFile(true);
        freqAskQuesFile.setLanguage(language);
        freqAskQuesFile.setFileUpload(fileUpload);
        freqAskQuesFile.setFilename(fileName);
        freqAskQuesFile.setContentType(contentType);
        freqAskQuesFile.setFileSize(fileUpload.length());

        return freqAskQuesFile;
    }

    @Action("/freqAskQuesEdit")
    @EnableTemplate(menuKey = {MP.FUNC_AD_FAQ})
    @Accesses(access = {@Access(accessCode = AP.FAQ, updateMode = true, adminMode = true)})
    public String edit() {
        init();
        freqAskQues = freqAskQuesService.findFreqAskQuesByQuestionId(questionId);
        freqAskQuesService.setFaqFileUrl(freqAskQues);

        return EDIT;
    }

    @Action("/deleteFaqFile")
    @EnableTemplate(menuKey = {MP.FUNC_AD_FAQ})
    @Accesses(access = {@Access(accessCode = AP.FAQ, updateMode = true, adminMode = true)})
    public String deleteFaqFile() {
        FreqAskQuesFileUploadConfiguration config = Application.lookupBean(FreqAskQuesFileUploadConfiguration.BEAN_NAME, FreqAskQuesFileUploadConfiguration.class);

        FreqAskQuesFile faqFile = freqAskQuesService.findFreqAskQuesFileByQuestionIdAndLanguage(questionId, faqDeleteLanguage);
        freqAskQuesService.doDeleteFaqFile(config.getFaqFileUploadPath(), faqFile);

        return JSON;
    }

    @Action("/freqAskQuesUpdate")
    @Accesses(access = {@Access(accessCode = AP.FAQ, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)})
    public String update() {
        init();

        try {
            freqAskQuesService.doUpdateFreqAskQues(getLocale(), freqAskQues, getFaqFileList());
            successMessage = getText("successMessage.freqAskQues.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public List<OptionBean> getStatusOption() {
        return statusOption;
    }

    public void setStatusOption(List<OptionBean> statusOption) {
        this.statusOption = statusOption;
    }

    public FreqAskQues getFreqAskQues() {
        return freqAskQues;
    }

    public void setFreqAskQues(FreqAskQues freqAskQues) {
        this.freqAskQues = freqAskQues;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public File getEnFileUpload() {
        return enFileUpload;
    }

    public void setEnFileUpload(File enFileUpload) {
        this.enFileUpload = enFileUpload;
    }

    public File getCnFileUpload() {
        return cnFileUpload;
    }

    public void setCnFileUpload(File cnFileUpload) {
        this.cnFileUpload = cnFileUpload;
    }

    public File getMsFileUpload() {
        return msFileUpload;
    }

    public void setMsFileUpload(File msFileUpload) {
        this.msFileUpload = msFileUpload;
    }

    public File getCombineFileUpload() {
        return combineFileUpload;
    }

    public void setCombineFileUpload(File combineFileUpload) {
        this.combineFileUpload = combineFileUpload;
    }

    public String getEnFileUploadFileName() {
        return enFileUploadFileName;
    }

    public void setEnFileUploadFileName(String enFileUploadFileName) {
        this.enFileUploadFileName = enFileUploadFileName;
    }

    public String getCnFileUploadFileName() {
        return cnFileUploadFileName;
    }

    public void setCnFileUploadFileName(String cnFileUploadFileName) {
        this.cnFileUploadFileName = cnFileUploadFileName;
    }

    public String getMsFileUploadFileName() {
        return msFileUploadFileName;
    }

    public void setMsFileUploadFileName(String msFileUploadFileName) {
        this.msFileUploadFileName = msFileUploadFileName;
    }

    public String getCombineFileUploadFileName() {
        return combineFileUploadFileName;
    }

    public void setCombineFileUploadFileName(String combineFileUploadFileName) {
        this.combineFileUploadFileName = combineFileUploadFileName;
    }

    public String getEnFileUploadContentType() {
        return enFileUploadContentType;
    }

    public void setEnFileUploadContentType(String enFileUploadContentType) {
        this.enFileUploadContentType = enFileUploadContentType;
    }

    public String getCnFileUploadContentType() {
        return cnFileUploadContentType;
    }

    public void setCnFileUploadContentType(String cnFileUploadContentType) {
        this.cnFileUploadContentType = cnFileUploadContentType;
    }

    public String getMsFileUploadContentType() {
        return msFileUploadContentType;
    }

    public void setMsFileUploadContentType(String msFileUploadContentType) {
        this.msFileUploadContentType = msFileUploadContentType;
    }

    public String getCombineFileUploadContentType() {
        return combineFileUploadContentType;
    }

    public void setCombineFileUploadContentType(String combineFileUploadContentType) {
        this.combineFileUploadContentType = combineFileUploadContentType;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getFaqDeleteLanguage() {
        return faqDeleteLanguage;
    }

    public void setFaqDeleteLanguage(String faqDeleteLanguage) {
        this.faqDeleteLanguage = faqDeleteLanguage;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
