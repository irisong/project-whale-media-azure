package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberReportService;
import com.compalsolutions.compal.member.vo.MemberReportType;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "memberReportTypeList"),
        @Result(name = BaseAction.ADD, location = "memberReportTypeAdd"),
        @Result(name = BaseAction.EDIT, location = "memberReportTypeEdit"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class MemberReportTypeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_MEMBER_REPORT_TYPES_LIST = "memberReportTypesList";

    private MemberReportService memberReportService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();

    private MemberReportType memberReportType = new MemberReportType(true);

    @ToTrim
    @ToUpperCase
    private String status;

    private boolean clear;

    public MemberReportTypeAction() {
        memberReportService = Application.lookupBean(MemberReportService.BEAN_NAME, MemberReportService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER_REPORT_TYPE)
    @Action(value = "/memberReportTypeList")
    @Access(accessCode = AP.MEMBER_REPORT_TYPE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getStatusWithOptionAll();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_MEMBER_REPORT_TYPES_LIST);
        } else if (session.containsKey(SESSION_MEMBER_REPORT_TYPES_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_MEMBER_REPORT_TYPES_LIST);
        } else {
            session.put(SESSION_MEMBER_REPORT_TYPES_LIST, objectMap);
        }

        status = (String) objectMap.get("status");

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER_REPORT_TYPE)
    @Action(value = "/memberReportTypeAdd")
    @Access(accessCode = AP.MEMBER_REPORT_TYPE, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getStatus();

        return ADD;
    }

    @Action(value = "/memberReportTypeSave")
    @Access(accessCode = AP.MEMBER_REPORT_TYPE, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, memberReportType);
            memberReportService.saveMemberReportType(memberReportType);
            successMessage = getText("successMessage.MemberReportTypeAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER_REPORT_TYPE)
    @Action(value = "/memberReportTypeEdit")
    @Access(accessCode = AP.MEMBER_REPORT_TYPE, adminMode = true, updateMode = true)
    public String edit() {
        try {
            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getStatus();

            memberReportType = memberReportService.getMemberReportType(memberReportType.getReportTypeId());
            if (memberReportType == null) {
                addActionError(getText("invalidMemberReportType"));
                return list();
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/memberReportTypeUpdate")
    @Access(accessCode = AP.MEMBER_REPORT_TYPE, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, memberReportType);
            memberReportService.updateMemberReportType(getLocale(), memberReportType);
            successMessage = getText("successMessage.MemberReportTypeAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public MemberReportType getMemberReportType() {
        return memberReportType;
    }

    public void setMemberReportType(MemberReportType memberReportType) {
        this.memberReportType = memberReportType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    // ---------------- GETTER & SETTER (END) -------------
}
