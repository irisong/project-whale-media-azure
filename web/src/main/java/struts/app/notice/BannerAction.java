package struts.app.notice;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.RedisCacheProvider;
import com.compalsolutions.compal.general.service.BannerService;
import com.compalsolutions.compal.general.service.LiveCategoryService;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.general.vo.BannerFile;
import com.compalsolutions.compal.general.vo.FreqAskQuesFile;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.vo.Symbolic;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "bannerList"),
        @Result(name = BaseAction.ADD, location = "bannerAdd"),
        @Result(name = BaseAction.ADD_DETAIL, location = "bannerAdd"),
        @Result(name = BaseAction.EDIT, location = "bannerEdit"),
        @Result(name = BaseAction.SHOW, location = "bannerShow"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }),
        @Result(name = BannerAction.JSON_NOTE, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", BannerAction.GET_JSON_NOTE_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })

public class BannerAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    public static final String JSON_NOTE = "jsonNote";
    public static final String GET_JSON_NOTE_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", seq";

    private static final String SESSION_ANNOUNCE_LIST = "bannerList";

    private BannerService bannerService;
    private LiveCategoryService categoryService;
    private MessageService messageService;
    private MemberService memberService;
    private RedisCacheProvider redisCacheProvider;

    private List<OptionBean> statuses = new ArrayList<>();
    private List<OptionBean> publishGroups = new ArrayList<>();
    private List<OptionBean> languages = new ArrayList<>();
    private List<OptionBean> bannerTypes = new ArrayList<>();
    private List<OptionBean> liveStreamcategories = new ArrayList<>();

    private Banner banner = new Banner(true);
    private BannerFile bannerFile = new BannerFile();

    private List<String> userGroups = new ArrayList<>();

    @ToTrim
    private String bannerId;
    private String categoryId;
    private String category;
    private String categoryCn;
    private String categoryMs;
    private String updateType;
    private int seq;
    private String memberCode;
    private String redirectUrl;
    private String categorySearch;
    private String deleteLanguage;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String bannerType;

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    private File fileUploadEn;
    private String fileUploadEnContentType;
    private String fileUploadEnFileName;

    private File fileUploadMs;
    private String fileUploadMsContentType;
    private String fileUploadMsFileName;

    private File fileUploadCn;
    private String fileUploadCnContentType;
    private String fileUploadCnFileName;

    public static final String SESSION_BANNER_UPLOAD_FILES = "bannerUploadFiles";

    private boolean clear;

    public BannerAction() {
        bannerService = Application.lookupBean(BannerService.BEAN_NAME, BannerService.class);
        categoryService = Application.lookupBean(LiveCategoryService.BEAN_NAME, LiveCategoryService.class);
        messageService = Application.lookupBean(MessageService.BEAN_NAME, MessageService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        redisCacheProvider = Application.lookupBean(RedisCacheProvider.BEAN_NAME, RedisCacheProvider.class);

    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_BANNER)
    @Action(value = "/bannerList")
    @Access(accessCode = AP.BANNER , adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getBannerStatusWithOptionAll();
        languages = optionBeanUtil.getLanguagesWithOptionAll();
        publishGroups = optionBeanUtil.getPublishGroups();
        bannerTypes = optionBeanUtil.getBannerTypeWithOptionAll();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_ANNOUNCE_LIST);
        } else if (session.containsKey(SESSION_ANNOUNCE_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_ANNOUNCE_LIST);
        } else {
            session.put(SESSION_ANNOUNCE_LIST, objectMap);
        }

        Locale locale = getLocale();
        List<LiveCategory> liveCategories = categoryService.doFindAllLiveCategory(true);
        liveStreamcategories.add(optionBeanUtil.generateOptionAll());
        for (LiveCategory liveCategory : liveCategories) {
            boolean isCn = locale.equals(Global.LOCALE_CN);
            boolean isMs = locale.equals(Global.LOCALE_MS);
            liveStreamcategories.add(new OptionBean(liveCategory.getCategoryId(), isCn ? liveCategory.getCategoryCn() : isMs ? liveCategory.getCategoryMs() : liveCategory.getCategory()));
        }

        if (isSubmitData()) {
            objectMap.put("status", status);
            objectMap.put("userGroups", userGroups);
            objectMap.put("categorySearch", categorySearch);

        } else {
            status = (String) objectMap.get("status");
            userGroups = (List<String>) objectMap.get("userGroups");
            categorySearch = (String) objectMap.get("categorySearch");
        }
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BANNER)
    @Action(value = "/bannerAdd")
    @Access(accessCode = AP.BANNER, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getBannerStatus();
        languages = optionBeanUtil.getLanguages();
        publishGroups = optionBeanUtil.getPublishGroups();
        bannerTypes = optionBeanUtil.getBannerType();
        updateType= Banner.OPEN_WEB_URL;

        Locale locale = getLocale();
        List<LiveCategory> liveCategories = categoryService.doFindAllLiveCategory(true);
        liveStreamcategories.add(new OptionBean("", getText("please_select")));
        for (LiveCategory liveCategory : liveCategories) {
            boolean isCn = locale.equals(Global.LOCALE_CN);
            boolean isMs = locale.equals(Global.LOCALE_MS);
            liveStreamcategories.add(new OptionBean(liveCategory.getCategoryId(), isCn ? liveCategory.getCategoryCn() : isMs ? liveCategory.getCategoryMs() : liveCategory.getCategory()));
        }
        return ADD;
    }


    @Action(value = "/updateBannerType")
    public String updateType() {

        return JSON;
    }

    @Action("/addLiveCategory")
    @Access(accessCode = AP.BANNER, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String addLiveStreamCategory() {
        try {
            categoryService.saveLiveCategory(category,categoryCn,categoryMs,seq);
            redisCacheProvider.setBannerStatus(false);
            successMessage = getText("successMessage.updateStatus");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/liveCategoryDelete")
    @Access(accessCode = AP.BANNER, createMode = true, adminMode = true)
    public String deleteCategory() {
        try {
            categoryService.doDeleteLiveCategory(categoryId);
            redisCacheProvider.setBannerStatus(false);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/liveCategoryStatusUpdate")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, adminMode = true)
    public String updateCategoryStatus() {
        try {
            categoryService.doUpdateLiveCategoryStatus(categoryId, status);
            redisCacheProvider.setBannerStatus(false);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/bannerSave")
    @Access(accessCode = AP.BANNER, adminMode = true, createMode = true)
    public String save() {
        try {
            if(memberCode.isEmpty()) {
                banner.setRedirectUrl(redirectUrl);
//                banner.setRedirectUrl(banner.getBannerType().equalsIgnoreCase(Banner.OPEN_VJ_PROFILE) ? member.getMemberId() : redirectUrl);
            } else {
                Member member = memberService.findMemberByMemberCodeOrWhaleliveId(memberCode);
                banner.setRedirectUrl(member.getMemberId());
            }

            VoUtil.toTrimUpperCaseProperties(this, banner);
            String groups = "";

            for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext(); ) {
                groups += iterator.next();

                if (iterator.hasNext())
                    groups += "|";
            }
            banner.setUserGroups(groups);
            bannerService.saveBanner(getLocale(), banner, getBannerFileList());

            bannerId = banner.getBannerId();
            redisCacheProvider.setBannerStatus(false);
            successMessage = getText("successMessage.BannerAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/bannerDelete")
    @Access(accessCode = AP.BANNER, createMode = true, adminMode = true)
    public String deleteBanner() {
        try {
            bannerService.doDeleteBanner(bannerId);
            redisCacheProvider.setBannerStatus(false);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BANNER)
    @Action(value = "/bannerEdit")
    @Access(accessCode = AP.BANNER, adminMode = true, updateMode = true)
    public String edit() {
        try {
            banner = bannerService.getBanner(banner.getBannerId());
            if (banner == null) {
                addActionError(getText("invalidAnnouncement"));
                return list();
            }

            String[] groups = StringUtils.split(banner.getUserGroups(), "|");
            for (String g : groups) {
                userGroups.add(g);
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            Locale locale = getLocale();
            statuses = optionBeanUtil.getBannerStatus();
            languages = optionBeanUtil.getLanguages();
            publishGroups = optionBeanUtil.getPublishGroups();
            bannerTypes = optionBeanUtil.getBannerType();
            bannerService.setBannerFileUrl(banner);
            updateType=banner.getBannerType();
            if(banner.getBannerType().equalsIgnoreCase(Banner.OPEN_VJ_PROFILE)) {
                Member member = memberService.getMember(banner.getRedirectUrl());
                memberCode = member.getMemberCode();
            }
            else{
                redirectUrl = banner.getRedirectUrl();
            }
            List<LiveCategory> liveCategories = categoryService.doFindAllLiveCategory(true);
            for (LiveCategory liveCategory : liveCategories) {
                boolean isCn = locale.equals(Global.LOCALE_CN);
                boolean isMs = locale.equals(Global.LOCALE_MS);
                liveStreamcategories.add(new OptionBean(liveCategory.getCategoryId(), isCn ? liveCategory.getCategoryCn() : isMs ? liveCategory.getCategoryMs() : liveCategory.getCategory()));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action("/deleteBannerFile")
    @Accesses(access = {@Access(accessCode = AP.BANNER, updateMode = true, adminMode = true)})
    public String deleteBannerFile() {
        bannerService.doDeleteBannerFile(bannerId,deleteLanguage);
        return JSON;
    }

    @Action(value = "/bannerUpdate")
    @Access(accessCode = AP.BANNER, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, banner);
            BannerFile bannerFile = new BannerFile();

            if(memberCode.isEmpty()) {
                banner.setRedirectUrl(redirectUrl);
            }else{
                Member member = memberService.findMemberByMemberCodeOrWhaleliveId(memberCode);
                banner.setRedirectUrl(member.getMemberId());
            }

            if(fileUpload != null){
                byte[] readFile = FileUtils.readFileToByteArray(fileUpload);

                bannerFile.setData(bannerService.getScaledImage(readFile, 500, 0));
                bannerFile.setFilename(fileUploadFileName);
                bannerFile.setContentType(fileUploadContentType);
                bannerFile.setFileSize(fileUpload.length());
            }
//            banner.setRedirectUrl(banner.getBannerType().equalsIgnoreCase(Banner.OPEN_VJ_PROFILE) ? member.getMemberId() : redirectUrl);

            String groups = "";
            for (Iterator<String> iterator = userGroups.iterator(); iterator.hasNext();) {
                groups += iterator.next();
                if (iterator.hasNext())
                    groups += "|";
            }
            banner.setUserGroups(groups);
            bannerService.updateBanner(getLocale(), banner, getBannerFileList());
            redisCacheProvider.setBannerStatus(false);
            successMessage = getText("successMessage.BannerAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    public BannerFile setMultiFile(String language, File fileUpload, String fileName, String contentType) {
        bannerFile = new BannerFile();
        bannerFile.setLanguage(language);
        bannerFile.setFileUpload(fileUpload);
        bannerFile.setFilename(fileName);
        bannerFile.setContentType(contentType);
        bannerFile.setFileSize(fileUpload.length());

        return bannerFile;
    }

    public List<BannerFile> getBannerFileList() {
        List<BannerFile> bannerFileList = new ArrayList<>();
        if (fileUploadEn != null)
            bannerFileList.add(setMultiFile(BannerFile.LANG_EN, fileUploadEn, fileUploadEnFileName, fileUploadEnContentType));
        if (fileUploadCn != null)
            bannerFileList.add(setMultiFile(BannerFile.LANG_CN, fileUploadCn, fileUploadCnFileName, fileUploadCnContentType));
        if (fileUploadMs != null)
            bannerFileList.add(setMultiFile(BannerFile.LANG_MS, fileUploadMs, fileUploadMsFileName, fileUploadMsContentType));
        if (fileUpload != null)
            bannerFileList.add(setMultiFile(BannerFile.NON_SPECIFIED, fileUpload, fileUploadFileName, fileUploadContentType));

        return bannerFileList;
    }

    @Action("/getNextBannerSeq")
    public String getNextBannerSeq() {
        VoUtil.toTrimUpperCaseProperties(this);
        seq = bannerService.getNextBannerSeq(categoryId);

        return JSON_NOTE;
    }

    @Action("/getNextBannerCategorySeq")
    public String getNextBannerCategorySeq() {
        VoUtil.toTrimUpperCaseProperties(this);
        seq = bannerService.getNextBannerCategorySeq();

        return JSON_NOTE;
    }


    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public List<OptionBean> getPublishGroups() {
        return publishGroups;
    }

    public void setPublishGroups(List<OptionBean> publishGroups) {
        this.publishGroups = publishGroups;
    }

    public Banner getBanner() {
        return banner;
    }

    public void setBanner(Banner banner) {
        this.banner = banner;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBannerType() {
        return bannerType;
    }

    public void setBannerType(String bannerType) {
        this.bannerType = bannerType;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryCn() {
        return categoryCn;
    }

    public void setCategoryCn(String categoryCn) {
        this.categoryCn = categoryCn;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public List<OptionBean> getLiveStreamcategories() {
        return liveStreamcategories;
    }

    public void setLiveStreamcategories(List<OptionBean> liveStreamcategories) {
        this.liveStreamcategories = liveStreamcategories;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public File getFileUploadEn() {
        return fileUploadEn;
    }

    public void setFileUploadEn(File fileUploadEn) {
        this.fileUploadEn = fileUploadEn;
    }

    public String getFileUploadEnContentType() {
        return fileUploadEnContentType;
    }

    public void setFileUploadEnContentType(String fileUploadEnContentType) {
        this.fileUploadEnContentType = fileUploadEnContentType;
    }

    public String getFileUploadEnFileName() {
        return fileUploadEnFileName;
    }

    public void setFileUploadEnFileName(String fileUploadEnFileName) {
        this.fileUploadEnFileName = fileUploadEnFileName;
    }

    public File getFileUploadMs() {
        return fileUploadMs;
    }

    public void setFileUploadMs(File fileUploadMs) {
        this.fileUploadMs = fileUploadMs;
    }

    public String getFileUploadMsContentType() {
        return fileUploadMsContentType;
    }

    public void setFileUploadMsContentType(String fileUploadMsContentType) {
        this.fileUploadMsContentType = fileUploadMsContentType;
    }

    public String getFileUploadMsFileName() {
        return fileUploadMsFileName;
    }

    public void setFileUploadMsFileName(String fileUploadMsFileName) {
        this.fileUploadMsFileName = fileUploadMsFileName;
    }

    public File getFileUploadCn() {
        return fileUploadCn;
    }

    public void setFileUploadCn(File fileUploadCn) {
        this.fileUploadCn = fileUploadCn;
    }

    public String getFileUploadCnContentType() {
        return fileUploadCnContentType;
    }

    public void setFileUploadCnContentType(String fileUploadCnContentType) {
        this.fileUploadCnContentType = fileUploadCnContentType;
    }

    public String getFileUploadCnFileName() {
        return fileUploadCnFileName;
    }

    public void setFileUploadCnFileName(String fileUploadCnFileName) {
        this.fileUploadCnFileName = fileUploadCnFileName;
    }

    public String getCategoryMs() {
        return categoryMs;
    }

    public void setCategoryMs(String categoryMs) {
        this.categoryMs = categoryMs;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public List<OptionBean> getBannerTypes() {
        return bannerTypes;
    }

    public void setBannerTypes(List<OptionBean> bannerTypes) {
        this.bannerTypes = bannerTypes;
    }

    public String getUpdateType() {
        return updateType;
    }

    public void setUpdateType(String updateType) {
        this.updateType = updateType;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getCategorySearch() {
        return categorySearch;
    }

    public void setCategorySearch(String categorySearch) {
        this.categorySearch = categorySearch;
    }

    public String getDeleteLanguage() {
        return deleteLanguage;
    }

    public void setDeleteLanguage(String deleteLanguage) {
        this.deleteLanguage = deleteLanguage;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
