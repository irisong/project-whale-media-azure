package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", HelpDeskListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class HelpDeskListDatatablesAction extends BaseSecureDatatablesAction<HelpDesk> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.ticketId, "
            + "data\\[\\d+\\]\\.ticketNo, "
            + "data\\[\\d+\\]\\.datetimeAdd, "
            + "data\\[\\d+\\]\\.owner\\.username, "
            + "data\\[\\d+\\]\\.helpDeskType\\.typeName, "
            + "data\\[\\d+\\]\\.subject, "
            + "data\\[\\d+\\]\\.repliedByAdmin, "
            + "data\\[\\d+\\]\\.adminReplyDatetime, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.closeDatetime, "
            + "data\\[\\d+\\]\\.closeByUser\\.username";

    private HelpDeskService helpDeskService;

    @ToTrim
    @ToUpperCase
    private String memberCode;

    private Date dateFrom;
    private Date dateTo;
    private String helpDeskType;
    private String status;
    private String repliedByAdmin;

    public HelpDeskListDatatablesAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
    }

    @Action(value = "/helpDeskListDatatables")
    @Accesses(access = {@Access(accessCode = AP.HELPDESK, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true),
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true)})
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        try {
            Boolean isRepliedByAdmin = StringUtils.isNotBlank(repliedByAdmin) ? Boolean.valueOf(repliedByAdmin) : null;
            helpDeskService.findHelpDeskListForListing(getDatagridModel(), memberCode, status, dateFrom, dateTo, helpDeskType, isRepliedByAdmin);
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getHelpDeskType() {
        return helpDeskType;
    }

    public void setHelpDeskType(String helpDeskType) {
        this.helpDeskType = helpDeskType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRepliedByAdmin() {
        return repliedByAdmin;
    }

    public void setRepliedByAdmin(String repliedByAdmin) {
        this.repliedByAdmin = repliedByAdmin;
    }


    // ---------------- GETTER & SETTER (END) -------------
}
