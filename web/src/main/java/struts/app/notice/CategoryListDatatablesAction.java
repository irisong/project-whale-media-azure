package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.LiveCategoryService;
import com.compalsolutions.compal.general.vo.LiveCategory;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                CategoryListDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class CategoryListDatatablesAction extends BaseSecureDatatablesAction<LiveCategory> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.categoryId, "//
            + "data\\[\\d+\\]\\.seq, "//
            + "data\\[\\d+\\]\\.category, "//
            + "data\\[\\d+\\]\\.categoryCn, "//
            + "data\\[\\d+\\]\\.categoryMs, "//
            + "data\\[\\d+\\]\\.status, "//
            + "data\\[\\d+\\]\\.statusDesc " ;

    private LiveCategoryService categoryService;

    @ToUpperCase
    @ToTrim
    private String category;

    @ToUpperCase
    @ToTrim
    private String categoryCn;

    @ToUpperCase
    @ToTrim
    private String categoryMs;

    @ToUpperCase
    @ToTrim
    private String status;

    @ToUpperCase
    @ToTrim
    private String categoryId;

    @ToUpperCase
    @ToTrim
    private int seq;

    public CategoryListDatatablesAction() {
        categoryService = Application.lookupBean(LiveCategoryService.BEAN_NAME, LiveCategoryService.class);
    }

    @Action(value = "/categoryListDatatables")
    @Accesses(access = {@Access(accessCode = AP.BANNER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true)})
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        categoryService.doFindVirtualCategory();//check if got virtual compulsary category, if no then add
        categoryService.findLiveCategoryForListing(getDatagridModel(), category, categoryCn ,categoryMs, status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryCn() {
        return categoryCn;
    }

    public void setCategoryCn(String categoryCn) {
        this.categoryCn = categoryCn;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryMs() {
        return categoryMs;
    }

    public void setCategoryMs(String categoryMs) {
        this.categoryMs = categoryMs;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
