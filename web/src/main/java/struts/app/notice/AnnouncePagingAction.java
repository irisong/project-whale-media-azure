package struts.app.notice;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AdminUserType;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.MemberUserType;
import com.compalsolutions.compal.general.service.AnnouncementService;
import com.compalsolutions.compal.general.vo.Announcement;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "announcePaging"), //
        @Result(name = BaseAction.SHOW, location = "announcePaging"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class AnnouncePagingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private AnnouncementService announcementService;

    private List<Announcement> announcements = new ArrayList<Announcement>();
    private int pageNo;
    private int pageSize;

    public AnnouncePagingAction() {
        announcementService = Application.lookupBean(AnnouncementService.BEAN_NAME, AnnouncementService.class);
    }

    @Action(value = "/announcePaging")
    @Override
    public String execute() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        boolean isAdmin = loginInfo.getUserType() instanceof AdminUserType;
        boolean isAgent = loginInfo.getUserType() instanceof AgentUserType;
        boolean isMember = loginInfo.getUserType() instanceof MemberUserType;

        String userGroup;
        if (isAdmin) {
            userGroup = Global.PublishGroup.ADMIN_GROUP;
        } else if (isAgent) {
            userGroup = Global.PublishGroup.AGENT_GROUP;
        } else if (isMember) {
            userGroup = Global.PublishGroup.MEMBER_GROUP;
        } else {
            userGroup = Global.PublishGroup.PUBLIC_GROUP;
        }

        announcements = announcementService.findAnnouncementsForDashboard(userGroup, pageNo, pageSize);

        return SHOW;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<Announcement> getAnnouncements() {
        return announcements;
    }

    public void setAnnouncements(List<Announcement> announcements) {
        this.announcements = announcements;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
