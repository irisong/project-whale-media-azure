package struts.app.notice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "helpDeskTypeList"),
        @Result(name = BaseAction.ADD, location = "helpDeskTypeAdd"),
        @Result(name = BaseAction.EDIT, location = "helpDeskTypeEdit"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class HelpDeskTypeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_HELP_DESK_TYPES_LIST = "helpDeskTypesList";

    private HelpDeskService helpDeskService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();

    private HelpDeskType helpDeskType = new HelpDeskType(true);

    @ToTrim
    @ToUpperCase
    private String status;

    private boolean clear;

    public HelpDeskTypeAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_HELPDESK_TYPE)
    @Action(value = "/helpDeskTypeList")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getStatusWithOptionAll();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_HELP_DESK_TYPES_LIST);
        } else if (session.containsKey(SESSION_HELP_DESK_TYPES_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_HELP_DESK_TYPES_LIST);
        } else {
            session.put(SESSION_HELP_DESK_TYPES_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("status", status);
        } else {
            status = (String) objectMap.get("status");
        }

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_HELPDESK_TYPE)
    @Action(value = "/helpDeskTypeAdd")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, createMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getStatus();

        return ADD;
    }

    @Action(value = "/helpDeskTypeSave")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, helpDeskType);

            helpDeskService.saveHelpDeskType(getLocale(), helpDeskType);
            successMessage = getText("successMessage.HelpDeskTypeAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_HELPDESK_TYPE)
    @Action(value = "/helpDeskTypeEdit")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, updateMode = true)
    public String edit() {
        try {
            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            statuses = optionBeanUtil.getStatus();

            helpDeskType = helpDeskService.getHelpDeskType(helpDeskType.getTicketTypeId());
            if (helpDeskType == null) {
                addActionError(getText("invalidHelpDeskType"));
                return list();
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/helpDeskTypeUpdate")
    @Access(accessCode = AP.HELPDESK_TYPE, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, helpDeskType);

            helpDeskService.updateHelpDeskType(getLocale(), helpDeskType);
            successMessage = getText("successMessage.HelpDeskTypeAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public HelpDeskType getHelpDeskType() {
        return helpDeskType;
    }

    public void setHelpDeskType(HelpDeskType helpDeskType) {
        this.helpDeskType = helpDeskType;
    }

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
