package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.BannerService;
import com.compalsolutions.compal.general.vo.Banner;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import struts.util.AjaxUtil;

import java.util.Date;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                BannerListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class BannerListDatatablesAction extends BaseSecureDatatablesAction<Banner> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.bannerId, "
            + "data\\[\\d+\\]\\.seq, "
            + "data\\[\\d+\\]\\.title, "
            + "data\\[\\d+\\]\\.titleCn, "
            + "data\\[\\d+\\]\\.titleMs, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.bannerType, "
            + "data\\[\\d+\\]\\.userGroups, "
            + "data\\[\\d+\\]\\.bannerTypeDesc, "
            + "data\\[\\d+\\]\\.fileUrl, "
            + "data\\[\\d+\\]\\.fileUrlEn, "
            + "data\\[\\d+\\]\\.fileUrlCn, "
            + "data\\[\\d+\\]\\.fileUrlMs ";

    private BannerService bannerService;

    private String userGroups;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    @ToUpperCase
    private String bannerType;

    @ToTrim
    @ToUpperCase
    private String category;

    public BannerListDatatablesAction() {
        bannerService = Application.lookupBean(BannerService.BEAN_NAME, BannerService.class);
    }

    @Action(value = "/bannerListDatatables")
    @Access(accessCode = AP.BANNER, readMode = true)
    @Override
    public String execute() throws Exception {
      //  VoUtil.toTrimUpperCaseProperties(this);

        List<String> userGroupsList = AjaxUtil.stringArrayToList(userGroups);
        bannerService.findBannersForListing(getDatagridModel(), userGroupsList, status, bannerType,category);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getBannerType() {
        return bannerType;
    }

    public void setBannerType(String bannerType) {
        this.bannerType = bannerType;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    // ---------------- GETTER & SETTER (END) -------------
}