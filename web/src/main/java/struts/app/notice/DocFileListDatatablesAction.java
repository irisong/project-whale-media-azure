package struts.app.notice;

import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.DocFileService;
import com.compalsolutions.compal.general.vo.DocFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

import struts.util.AjaxUtil;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                DocFileListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class DocFileListDatatablesAction extends BaseSecureDatatablesAction<DocFile> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.docId, "//
            + "data\\[\\d+\\]\\.title, "//
            + "data\\[\\d+\\]\\.status, " //
            + "data\\[\\d+\\]\\.userGroups, " //
            + "data\\[\\d+\\]\\.filename, " //
            + "data\\[\\d+\\]\\.datetimeAdd";

    private DocFileService docFileService;

    private String userGroups;
    private String languageCode;

    @ToTrim
    @ToUpperCase
    private String status;

    public DocFileListDatatablesAction() {
        docFileService = Application.lookupBean(DocFileService.BEAN_NAME, DocFileService.class);
    }

    @Action(value = "/docfileListDatatables")
    @Access(accessCode = AP.DOCFILE, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        List<String> userGroupsList = AjaxUtil.stringArrayToList(userGroups);
        docFileService.findDocFilesForListing(getDatagridModel(), languageCode, userGroupsList, status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(String userGroups) {
        this.userGroups = userGroups;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
