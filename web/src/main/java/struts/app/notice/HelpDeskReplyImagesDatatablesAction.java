package struts.app.notice;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.ArrayList;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = BaseAction.INPUT, location = "jsonMessage")})
public class HelpDeskReplyImagesDatatablesAction extends BaseDatagridAction<HelpDeskReplyFile> {
    private static final long serialVersionUID = 1L;

    @Action("/helpDeskReplyImagesDatatable")
    @Access(accessCode = AP.HELPDESK, createMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        List<HelpDeskReplyFile> replyImages = getHelpDeskReplyFile();
        getDatagridModel().setRecords(replyImages);
        getDatagridModel().sortRecords();

        return JSON;
    }

    @SuppressWarnings("unchecked")
    private List<HelpDeskReplyFile> getHelpDeskReplyFile() {
        if (!session.containsKey(HelpDeskAction.SESSION_UPLOAD_REPLY_IMAGE)) {
            List<HelpDeskReplyFile> replyImages = new ArrayList<>();
            session.put(HelpDeskAction.SESSION_UPLOAD_REPLY_IMAGE, replyImages);
        }

        return (List<HelpDeskReplyFile>) session.get(HelpDeskAction.SESSION_UPLOAD_REPLY_IMAGE);
    }
}
