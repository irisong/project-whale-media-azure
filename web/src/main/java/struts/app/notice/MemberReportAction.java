package struts.app.notice;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.member.service.FollowerService;
import com.compalsolutions.compal.member.service.MemberReportService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberReport;
import com.compalsolutions.compal.member.vo.MemberReportFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "memberReportList"),
        @Result(name = BaseAction.SHOW, location = "memberReportView"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" })
        })
public class MemberReportAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    private static final Log log = LogFactory.getLog(MemberReportAction.class);

    private MemberReportService memberReportService;
    private MemberService memberService;
    private MemberReportFileUploadConfiguration config;

    private MemberReport memberReport;
    private Member userMember;
    private Member reportedMember;

    private boolean clear;

    private List<OptionBean> allMemberReportTypeList = new ArrayList<OptionBean>();//can choose
    private List<OptionBean> allReadStatusList = new ArrayList<OptionBean>();//can choose

    private static final String SESSION_MEMBER_REPORT_LIST = "memberReport";
    private String language = Global.Language.ENGLISH;

    private Date dateTo;
    private Date dateFrom;

    @ToTrim
    @ToUpperCase
    private String memberReportType;
    private String reportMemberId;
    private String readStatus;

    private String warningReason;
    private String warningReasonCn;
    private String reportId;
    private String warningCount;

    public MemberReportAction() {
        memberReportService = Application.lookupBean(MemberReportService.BEAN_NAME, MemberReportService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        config = Application.lookupBean(MemberReportFileUploadConfiguration.BEAN_NAME, MemberReportFileUploadConfiguration.class);
    }

    private void initialize(){
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        if (Global.LOCALE_CN.equals(getLocale())) {
            language = Global.Language.CHINESE;
        } else if (Global.LOCALE_MS.equals(getLocale())){
            language = Global.Language.MALAY;
        }

        allMemberReportTypeList.add(optionBeanUtil.generateOptionAll());
        allMemberReportTypeList.addAll(optionBeanUtil.getMemberReportTypeByLanguage(language, Global.Status.ACTIVE));

        allReadStatusList = optionBeanUtil.getYesNoWithOptionAll();
    }

    @Action(value = "/memberReportList")
    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER_REPORT)
    @Access(accessCode = AP.MEMBER_REPORT, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list(){
        VoUtil.toTrimUpperCaseProperties(this);
        initialize();

        Map<String, Object> objectMap = new HashMap<>();
        if(clear) {
            session.remove(SESSION_MEMBER_REPORT_LIST);
        } else if(session.containsKey(SESSION_MEMBER_REPORT_LIST)) {
            objectMap = (Map<String,Object>) session.get(SESSION_MEMBER_REPORT_LIST);
        } else {
            session.put(SESSION_MEMBER_REPORT_LIST, objectMap);
        }

        dateFrom = (Date) objectMap.get("dateFrom");
        dateTo = (Date) objectMap.get("dateTo");
        memberReportType = (String) objectMap.get("memberReportType");
        reportMemberId = (String) objectMap.get("reportMemberId");
        readStatus = (String) objectMap.get("readStatus");

        return LIST;
    }

    @Action("/memberReportView")
    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER_REPORT)
    @Access(accessCode = AP.MEMBER_REPORT, updateMode = true, adminMode = true)
    public String show() {
        try {
            VoUtil.toTrimProperties(this);
            initialize();

            memberReport = memberReportService.getMemberReport(memberReport.getReportId());
            userMember = memberService.getMember(memberReport.getMemberId());
            reportedMember = memberService.getMember(memberReport.getReportMemberId());
            memberReportType = memberReportService.getMemberReportTypeByLanguage(memberReport.getReportTypeId(), language);

            //loop in the file and show if the file exist or not
            List<MemberReportFile> memberReportFiles = memberReport.getMemberReportFiles();
            for (MemberReportFile file : memberReportFiles) {
                file.setFileUrl(file.getFileUrlWithParentPath(config.getFullMemberReportParentFileUrl()));
            }

            if (memberReport.getReadStatus() == false) {
                memberReportService.updateMemberReportRead(memberReport.getReportId(), getLoginInfo());
            }
        }catch(Exception e){
            addActionError(e.getMessage());
        }
        return SHOW;
    }

    @Action("/warningMessage")
//    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER_REPORT)
    @Access(accessCode = AP.MEMBER_REPORT, updateMode = true, adminMode = true)
    public String proceed() {
        try {
            VoUtil.toTrimProperties(this);

            memberReport = memberReportService.getMemberReport(reportId);
            reportedMember = memberService.getMember(memberReport.getReportMemberId());
            Member member = memberService.getMember(memberReport.getMemberId());

            memberReportService.doProcessWarningMessage(reportedMember, member ,warningReason, warningReasonCn);
            memberReportService.doUpdateWarningCount(memberReport);

            if (memberReport.getReadStatus() == false) {
                memberReportService.updateMemberReportRead(memberReport.getReportId(), getLoginInfo());
            }
        } catch(Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }



    // ---------------- GETTER & SETTER (START) ---------------- //
    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public List<OptionBean> getAllMemberReportTypeList() {
        return allMemberReportTypeList;
    }

    public void setAllMemberReportTypeList(List<OptionBean> allMemberReportTypeList) {
        this.allMemberReportTypeList = allMemberReportTypeList;
    }

    public List<OptionBean> getAllReadStatusList() {
        return allReadStatusList;
    }

    public void setAllReadStatusList(List<OptionBean> allReadStatusList) {
        this.allReadStatusList = allReadStatusList;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getMemberReportType() {
        return memberReportType;
    }

    public void setMemberReportType(String memberReportType) {
        this.memberReportType = memberReportType;
    }

    public String getReportMemberId() {
        return reportMemberId;
    }

    public void setReportMemberId(String reportMemberId) {
        this.reportMemberId = reportMemberId;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public MemberReport getMemberReport() {
        return memberReport;
    }

    public void setMemberReport(MemberReport memberReport) {
        this.memberReport = memberReport;
    }

    public Member getUserMember() {
        return userMember;
    }

    public void setUserMember(Member userMember) {
        this.userMember = userMember;
    }

    public Member getReportedMember() {
        return reportedMember;
    }

    public void setReportedMember(Member reportedMember) {
        this.reportedMember = reportedMember;
    }

    public String getWarningReason() {
        return warningReason;
    }

    public void setWarningReason(String warningReason) {
        this.warningReason = warningReason;
    }

    public String getWarningReasonCn() {
        return warningReasonCn;
    }

    public void setWarningReasonCn(String warningReasonCn) {
        this.warningReasonCn = warningReasonCn;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public String getWarningCount() {
        return warningCount;
    }

    public void setWarningCount(String warningCount) {
        this.warningCount = warningCount;
    }

    // ---------------- GETTER & SETTER (END) ---------------- //
}
