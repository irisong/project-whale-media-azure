package struts.app.notice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.SmsQueueService;
import com.compalsolutions.compal.general.vo.SmsQueue;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.sms.service.SmsService;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.ADD, location = "smsqAdd"), //
        @Result(name = BaseAction.LIST, location = "smsqList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })

public class SmsqAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_SMSQ_LIST = "smsqList";

    private SmsQueueService smsQueueService;

    private List<OptionBean> statuses = new ArrayList<OptionBean>();

    private SmsQueue smsQueue = new SmsQueue(true);

    private SmsService smsService;

    @ToTrim
    @ToUpperCase
    private String status;

    private boolean clear;

    public SmsqAction() {
        smsQueueService = Application.lookupBean(SmsQueueService.BEAN_NAME, SmsQueueService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_SMSQ)
    @Action(value = "/smsqList")
    @Access(accessCode = AP.SMSQ, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getSmsqStatusWithOptionAll();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_SMSQ_LIST);
        } else if (session.containsKey(SESSION_SMSQ_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_SMSQ_LIST);
        } else {
            session.put(SESSION_SMSQ_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("status", status);
        } else {
            status = (String) objectMap.get("status");
            status = (status == null) ? "" : status;
        }

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_SMSQ)
    @Action(value = "/smsqAdd")
    @Access(accessCode = AP.SMSQ, adminMode = true, createMode = true)
    public String add() {
        return ADD;
    }

    @Action(value = "/smsqSave")
    @Access(accessCode = AP.SMSQ, adminMode = true, createMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, smsQueue);
            smsQueue.setStatus(SmsQueue.SMS_STATUS_PENDING);
            smsQueueService.saveSmsQueue(getLocale(), smsQueue);
            successMessage = getText("successMessage.SmsqAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public SmsQueue getSmsQueue() {
        return smsQueue;
    }

    public void setSmsQue(SmsQueue smsQue) {
        this.smsQueue = smsQueue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
