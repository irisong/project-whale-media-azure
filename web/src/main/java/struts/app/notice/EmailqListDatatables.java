package struts.app.notice;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.email.service.EmailService;
import com.compalsolutions.compal.function.email.vo.Emailq;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", EmailqListDatatables.JSON_INCLUDE_PROPERTIES }) })
public class EmailqListDatatables extends BaseSecureDatatablesAction<Emailq> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.docId, "//
            + "data\\[\\d+\\]\\.title, "//
            + "data\\[\\d+\\]\\.status, " //
            + "data\\[\\d+\\]\\.emailTo, " //
            + "data\\[\\d+\\]\\.emailCc, " //
            + "data\\[\\d+\\]\\.datetimeAdd";

    private EmailService emailService;

    @ToTrim
    @ToUpperCase
    private String status;

    public EmailqListDatatables() {
        emailService = Application.lookupBean(EmailService.BEAN_NAME, EmailService.class);
    }

    @Action(value = "/emailqListDatatables")
    @Access(accessCode = AP.EMAILQ, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        emailService.findEmailqForListing(getDatagridModel(), status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}