package struts.app.notice;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDesk;
import com.compalsolutions.compal.general.vo.HelpDeskReply;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.general.vo.HelpDeskType;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.CollectionUtil;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "helpDeskList"),
        @Result(name = BaseAction.SHOW, location = "helpDeskView"),
        @Result(name = BaseAction.EDIT, location = "helpDeskReply"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default}),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}"})
})
public class HelpDeskAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(HelpDeskAction.class);

    public static final String SESSION_UPLOAD_REPLY_IMAGE = "uploadReplyImage";

    private List<OptionBean> helpDeskTypeList = new ArrayList<OptionBean>();
    private List<OptionBean> allHelpDeskTypeList = new ArrayList<OptionBean>();
    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();
    private List<OptionBean> allHelpDeskRepliedStatus = new ArrayList<>();

//    List<HelpDeskReplyFile> helpDeskReplyImages = new ArrayList<>();

    private HelpDesk helpDesk = new HelpDesk(false);
    private HelpDeskReply helpDeskReply = new HelpDeskReply(false);
    private HelpDeskReplyFile helpDeskReplyFile = new HelpDeskReplyFile(false);

    private String language = Global.Language.ENGLISH;

    private String ticketId;
    private String status;
    private String helpDeskType;
    private String uploadType;
    private String tempId;
    private String repliedByAdmin;
    private String helpDeskReplyFileId;

    private String memberCode;
    private Date dateFrom;
    private Date dateTo;

    // File Upload
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private InputStream fileInputStream;

    private HelpDeskService helpDeskService;
    private HelpDeskFileUploadConfiguration config;

    private boolean clear;

    private static final String SESSION_HELP_SUPPORT_LIST = "supportList";

    public HelpDeskAction() {
        helpDeskService = Application.lookupBean(HelpDeskService.BEAN_NAME, HelpDeskService.class);
        config = Application.lookupBean(HelpDeskFileUploadConfiguration.BEAN_NAME, HelpDeskFileUploadConfiguration.class);
    }

    private void init() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());

        if (Global.LOCALE_CN.equals(getLocale())) {
            language = Global.Language.CHINESE;
        } else if (Global.LOCALE_MS.equals(getLocale())){
            language = Global.Language.MALAY;
        }

        helpDeskTypeList = optionBeanUtil.getHelpDeskTypesByLanguage(language, Global.Status.ACTIVE);

        allHelpDeskTypeList.add(optionBeanUtil.generateOptionAll());
        allHelpDeskTypeList.addAll(helpDeskTypeList);

        statusList = optionBeanUtil.getHelpDeskStatus();

        allStatusList.add(optionBeanUtil.generateOptionAll());
        allStatusList.addAll(statusList);

        allHelpDeskRepliedStatus = optionBeanUtil.getYesNoWithOptionAll();
    }

    @Action(value = "/helpDeskList")
    @EnableTemplate(menuKey = {MP.FUNC_AD_HELPDESK, MP.FUNC_AD_HELPDESK})
    @Accesses(access = {@Access(accessCode = AP.HELPDESK, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)})
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        clearSessionValue();
        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_HELP_SUPPORT_LIST);
        } else if (session.containsKey(SESSION_HELP_SUPPORT_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_HELP_SUPPORT_LIST);
        } else {
            session.put(SESSION_HELP_SUPPORT_LIST, objectMap);
        }

        memberCode = (String) objectMap.get("memberCode");
        dateFrom = (Date) objectMap.get("dateFrom");
        dateTo = (Date) objectMap.get("dateTo");
        helpDeskType = (String) objectMap.get("helpDeskType");
        status = (String) objectMap.get("status");
        repliedByAdmin = (String) objectMap.get("repliedByAdmin");

        init();
        if(StringUtils.isBlank(status)) {
            status = HelpDesk.STATUS_OPEN;
        }

        return LIST;
    }

    @Action("/helpDeskView")
    @EnableTemplate(menuKey = {MP.FUNC_AD_HELPDESK, MP.FUNC_AD_HELPDESK})
    @Accesses(access = {@Access(accessCode = AP.HELPDESK, updateMode = true, adminMode = true)})
    public String show() {
        VoUtil.toTrimProperties(this);
        init();
        setDisplayValue();

        return SHOW;
    }

    @Action("/helpDeskReply")
    @EnableTemplate(menuKey = {MP.FUNC_AD_HELPDESK, MP.FUNC_AD_HELPDESK})
    @Accesses(access = {@Access(accessCode = AP.HELPDESK, updateMode = true, adminMode = true)})
    public String edit() {
        clearSessionValue();
        init();
        setDisplayValue();

        return EDIT;
    }

    public void setDisplayValue() {
        final String serverUrl = config.getFullHelpDeskParentFileUrl();
        helpDesk = helpDeskService.getHelpDesk(ticketId);
        helpDeskType = helpDeskService.getHelpDeskTypeNameByLanguage(helpDesk.getTicketTypeId(), language);

        List<HelpDeskReply> helpDeskReplies = helpDesk.getHelpDeskReplies();
        helpDeskReplies.sort(Comparator.comparing(HelpDeskReply::getReplySeq));
        for (HelpDeskReply helpDeskReply : helpDeskReplies) {
            List<HelpDeskReplyFile> files = helpDeskReply.getHelpDeskReplyFiles();
            for (HelpDeskReplyFile file : files) {
                file.setFileUrl(file.getFileUrlWithParentPath(serverUrl));
            }
        }
    }

    @Action("/helpDeskSave")
    @EnableTemplate(menuKey = {MP.FUNC_AD_HELPDESK, MP.FUNC_AD_HELPDESK})
    @Accesses(access = {@Access(accessCode = AP.HELPDESK, updateMode = true, adminMode = true)})
    public String save() {
        try {
            helpDeskService.doProcessHelpDeskTicket(getLocale(), getLoginInfo().getUser(), ticketId,null,null, helpDeskReply.getMessage(), getHelpDeskReplyFile());
            successMessage = getText("successMessage.SupportAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }
        return JSON;
    }

    @Action("/attachHelpDeskReplyImages")
    public String attachImages() throws IOException {
        List<HelpDeskReplyFile> helpDeskReplyFileImages = getHelpDeskReplyFile();

        UUID uuid = UUID.randomUUID();
        String uuidStr = StringUtils.replace(uuid.toString(), "-", "");
        helpDeskReplyFile.setTempId(uuidStr);
        helpDeskReplyFile.setData(FileUtils.readFileToByteArray(fileUpload));
        helpDeskReplyFile.setFileSize(fileUpload.length());
        helpDeskReplyFile.setFilename(fileUploadFileName);
        helpDeskReplyFile.setContentType(fileUploadContentType);
        helpDeskReplyFileImages.add(helpDeskReplyFile);

        //when back to client, if successMessage is not blank means add successfully.
        successMessage = MSG_ADD_DETAIL_SUCCESS;

        return JSON;
    }

    @Action("/helpDeskImageRemoveDetail")
    public String removeDetail() {
        List<HelpDeskReplyFile> helpDeskImageList = getHelpDeskReplyFile();
        Iterator<HelpDeskReplyFile> i = helpDeskImageList.iterator();
        while (i.hasNext()) {
            HelpDeskReplyFile s = i.next();
            if (tempId.equalsIgnoreCase(s.getTempId())) {
                i.remove();
                break;
            }
        }

        //when back to client, if successMessage is not blank means add successfully.
        successMessage = MSG_REMOVE_DETAIL_SUCCESS;

        return JSON;
    }

    private List<HelpDeskReplyFile> getHelpDeskReplyFile() {
        if (!session.containsKey(HelpDeskAction.SESSION_UPLOAD_REPLY_IMAGE)) {
            List<HelpDeskReplyFile> helpDeskReplyFileImages = new ArrayList<>();
            session.put(HelpDeskAction.SESSION_UPLOAD_REPLY_IMAGE, helpDeskReplyFileImages);
        }

        return (List<HelpDeskReplyFile>) session.get(HelpDeskAction.SESSION_UPLOAD_REPLY_IMAGE);
    }

    protected void clearSessionValue() {
        session.remove(SESSION_UPLOAD_REPLY_IMAGE);
    }

    @Action("/helpDeskTicketReopen")
    public String helpDeskTicketReopen() {
        try {
            clearSessionValue();
            helpDeskService.updateHelpDeskStatus(ticketId, HelpDesk.STATUS_OPEN,null);
            successMessage = getText("successMessage.ReopenTicketAction.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/helpDeskTicketClose")
    public String helpDeskTicketClose() {
        try {
            clearSessionValue();
            helpDeskService.updateHelpDeskStatus(ticketId, HelpDesk.STATUS_CLOSE, getLoginInfo());
            successMessage = getText("successMessage.CloseTicketAction.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getHelpDeskTypeList() {
        return helpDeskTypeList;
    }

    public void setHelpDeskTypeList(List<OptionBean> helpDeskTypeList) {
        this.helpDeskTypeList = helpDeskTypeList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<OptionBean> getAllHelpDeskTypeList() {
        return allHelpDeskTypeList;
    }

    public void setAllHelpDeskTypeList(List<OptionBean> allHelpDeskTypeList) {
        this.allHelpDeskTypeList = allHelpDeskTypeList;
    }

    public HelpDesk getHelpDesk() {
        return helpDesk;
    }

    public void setHelpDesk(HelpDesk helpDesk) {
        this.helpDesk = helpDesk;
    }

    public HelpDeskReply getHelpDeskReply() {
        return helpDeskReply;
    }

    public void setHelpDeskReply(HelpDeskReply helpDeskReply) {
        this.helpDeskReply = helpDeskReply;
    }

    public void setHelpDeskReplyFile(HelpDeskReplyFile helpDeskReplyFile) {
        this.helpDeskReplyFile = helpDeskReplyFile;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHelpDeskType() {
        return helpDeskType;
    }

    public void setHelpDeskType(String helpDeskType) {
        this.helpDeskType = helpDeskType;
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getRepliedByAdmin() {
        return repliedByAdmin;
    }

    public void setRepliedByAdmin(String repliedByAdmin) {
        this.repliedByAdmin = repliedByAdmin;
    }

    public List<OptionBean> getAllHelpDeskRepliedStatus() {
        return allHelpDeskRepliedStatus;
    }

    public void setAllHelpDeskRepliedStatus(List<OptionBean> allHelpDeskRepliedStatus) {
        this.allHelpDeskRepliedStatus = allHelpDeskRepliedStatus;
    }

    public String getHelpDeskReplyFileId() {
        return helpDeskReplyFileId;
    }

    public void setHelpDeskReplyFileId(String helpDeskReplyFileId) {
        this.helpDeskReplyFileId = helpDeskReplyFileId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
