package struts.app.notice;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.DocFileService;
import com.compalsolutions.compal.general.vo.DocFile;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                DocFileListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class DocFileListDatagridAction extends BaseDatagridAction<DocFile> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.docId, "//
            + "rows\\[\\d+\\]\\.title, "//
            + "rows\\[\\d+\\]\\.status, " //
            + "rows\\[\\d+\\]\\.userGroups, " //
            + "rows\\[\\d+\\]\\.filename, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private DocFileService docFileService;

    private List<String> userGroups = new ArrayList<String>();
    private String languageCode;

    @ToTrim
    @ToUpperCase
    private String status;

    public DocFileListDatagridAction() {
        docFileService = Application.lookupBean(DocFileService.BEAN_NAME, DocFileService.class);
    }

    @Action(value = "/docfileListDatagrid")
    @Access(accessCode = AP.DOCFILE, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        docFileService.findDocFilesForListing(getDatagridModel(), languageCode, userGroups, status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<String> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<String> userGroups) {
        this.userGroups = userGroups;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
