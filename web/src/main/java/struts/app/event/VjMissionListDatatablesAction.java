package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                VjMissionListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class VjMissionListDatatablesAction extends BaseSecureDatatablesAction<VjMission> {
    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.missionId, "
            + "data\\[\\d+\\]\\.member\\.memberCode, "
            + "data\\[\\d+\\]\\.submitDatetime, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.startDatetime, "
            + "data\\[\\d+\\]\\.endDatetime, "
            + "data\\[\\d+\\]\\.vjMissionConfig\\.missionPeriod, "
            + "data\\[\\d+\\]\\.hasWinner";

    private VjMissionService vjMissionService;

    @ToTrim
    @ToUpperCase
    private String status;

    public VjMissionListDatatablesAction() {
        vjMissionService = Application.lookupBean(VjMissionService.BEAN_NAME, VjMissionService.class);
    }

    @Action(value = "/vjMissionListDatatables")
    @Access(accessCode = AP.MISSION, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        vjMissionService.doGetMissionForListing(getDatagridModel(), null, status);
        getDatagridModel().getRecords().forEach(vjMission -> {
            Member member = vjMissionService.getSpecialMissionWinner(vjMission.getMissionId());
            if (member != null) {
                vjMission.setHasWinner(true);
            }
        });

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
