package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import com.compalsolutions.compal.event.vo.LuckyDrawDefaultWinner;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                LuckyDrawDefaultWinnerDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class LuckyDrawDefaultWinnerDatatablesAction extends BaseSecureDatatablesAction<LuckyDrawDefaultWinner> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.winSeq, "
            + "data\\[\\d+\\]\\.winnerMember\\.memberCode ";

    private LuckyDrawService luckyDrawService;

    @ToTrim
    private String packageId;

    public LuckyDrawDefaultWinnerDatatablesAction() {
        luckyDrawService = Application.lookupBean(LuckyDrawService.BEAN_NAME, LuckyDrawService.class);
    }

    @Action(value = "/luckyDrawDefaultWinnerDatatables")
    @Access(accessCode = AP.LUCKY_DRAW, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        luckyDrawService.findLuckyDrawDefaultWinnerByItemForListing(getDatagridModel(), packageId);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }
    // ---------------- GETTER & SETTER (END) -------------
}