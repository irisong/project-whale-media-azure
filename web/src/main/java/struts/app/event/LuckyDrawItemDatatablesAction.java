package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                LuckyDrawItemDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class LuckyDrawItemDatatablesAction extends BaseSecureDatatablesAction<LuckyDrawPackage> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.itemSeq, "
            + "data\\[\\d+\\]\\.itemName, "
            + "data\\[\\d+\\]\\.itemNameCn, "
            + "data\\[\\d+\\]\\.itemQty, "
            + "data\\[\\d+\\]\\.remainingQty ";

    private LuckyDrawService luckyDrawService;

    @ToTrim
    private String packageNo;
    private Date luckyDrawDate;

    public LuckyDrawItemDatatablesAction() {
        luckyDrawService = Application.lookupBean(LuckyDrawService.BEAN_NAME, LuckyDrawService.class);
    }

    @Action(value = "/luckyDrawItemDatatables")
    @Access(accessCode = AP.LUCKY_DRAW, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        luckyDrawService.findLuckyDrawPackageItemForListing(getDatagridModel(), packageNo, luckyDrawDate);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public Date getLuckyDrawDate() {
        return luckyDrawDate;
    }

    public void setLuckyDrawDate(Date luckyDrawDate) {
        this.luckyDrawDate = luckyDrawDate;
    }

    // ---------------- GETTER & SETTER (END) -------------
}