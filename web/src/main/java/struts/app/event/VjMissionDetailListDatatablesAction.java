package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                VjMissionDetailListDatatablesAction.JSON_INCLUDE_PROPERTIES})})

public class VjMissionDetailListDatatablesAction extends BaseSecureDatatablesAction<VjMissionDetail> {
    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.detailId, "
            + "data\\[\\d+\\]\\.name, "
            + "data\\[\\d+\\]\\.prize, "
            + "data\\[\\d+\\]\\.point, "
            + "data\\[\\d+\\]\\.specialMissionId";

    private VjMissionService vjMissionService;

    private String missionId;

    public VjMissionDetailListDatatablesAction() {
        vjMissionService = Application.lookupBean(VjMissionService.BEAN_NAME, VjMissionService.class);
    }

    @Action(value = "/vjMissionDetailListDatatables")
    @Access(accessCode = AP.MISSION, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        vjMissionService.getVjMissionDetailForlisting(getDatagridModel(), missionId);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}