package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                LuckyDrawPackageDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class LuckyDrawPackageDatatablesAction extends BaseSecureDatatablesAction<LuckyDrawPackage> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.packageNo, "
            + "data\\[\\d+\\]\\.packageName, "
            + "data\\[\\d+\\]\\.packageNameCn, "
            + "data\\[\\d+\\]\\.senderMember\\.memberCode, "
            + "data\\[\\d+\\]\\.luckyDrawDate, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.luckyDrawStarted";

    private LuckyDrawService luckyDrawService;
    private MemberService memberService;

    @ToTrim
    private String memberCode;

    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    public LuckyDrawPackageDatatablesAction() {
        luckyDrawService = Application.lookupBean(LuckyDrawService.BEAN_NAME, LuckyDrawService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/luckyDrawPackageDatatables")
    @Access(accessCode = AP.LUCKY_DRAW, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        luckyDrawService.findLuckyDrawPackageForListing(getDatagridModel(), memberCode, dateFrom, dateTo, status);

        for (LuckyDrawPackage luckyDrawPackage : getDatagridModel().getRecords()) {
            boolean luckyDrawStarted = luckyDrawService.isLuckyDrawStarted(luckyDrawPackage.getPackageNo(), luckyDrawPackage.getLuckyDrawDate());
            if (!luckyDrawStarted && luckyDrawPackage.getStatus().equalsIgnoreCase(Global.Status.ACTIVE)) {
                luckyDrawStarted = false;
            } else {
                luckyDrawStarted = true;
            }

            luckyDrawPackage.setLuckyDrawStarted(luckyDrawStarted);
            luckyDrawPackage.setSenderMember(memberService.getMember(luckyDrawPackage.getSenderMemberId()));
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    // ---------------- GETTER & SETTER (END) -------------
}