package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import com.compalsolutions.compal.event.vo.LuckyDrawDefaultWinner;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "luckyDrawPackageList"),
        @Result(name = BaseAction.ADD, location = "luckyDrawPackageAdd"),
        @Result(name = BaseAction.PAGE1, location = "luckyDrawItemList"),
        @Result(name = BaseAction.PAGE2, location = "luckyDrawPackageEdit"),
        @Result(name = BaseAction.PAGE3, location = "luckyDrawDefaultWinnerList"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON)
})
public class LuckyDrawAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_LUCKY_DRAW_LIST = "luckyDrawPackageList";

    protected LuckyDrawService luckyDrawService;

    protected MemberService memberService;

    protected LuckyDrawPackage luckyDrawPackage;
    protected LuckyDrawDefaultWinner luckyDrawDefaultWinner;

    private List<OptionBean> allStatusList = new ArrayList<>();
    private List<OptionBean> statusList = new ArrayList<>();

    private boolean clear;

    /********************************
     * property for listing - START
     *******************************/
    @ToTrim
    private String memberCode;

    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;
    /********************************
     * property for listing - END
     *******************************/

    @ToTrim
    private String packageNo;
    private Date luckyDrawDate;

    @ToTrim
    private String packageId;

    @ToTrim
    private String id;

    private boolean blnLuckyDrawStarted = false;

    public LuckyDrawAction() {
        luckyDrawService = Application.lookupBean(LuckyDrawService.class);
        memberService = Application.lookupBean(MemberService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_LUCKY_DRAW)
    @Action(value = "/luckyDrawPackageList")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String packageList() {
        VoUtil.toTrimUpperCaseProperties(this);
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allStatusList = optionBeanUtil.getMemberStatusWithOptionAll();
        statusList = optionBeanUtil.getMemberStatus();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_LUCKY_DRAW_LIST);
        } else if (session.containsKey(SESSION_LUCKY_DRAW_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_LUCKY_DRAW_LIST);
        } else {
            session.put(SESSION_LUCKY_DRAW_LIST, objectMap);
        }

        memberCode = (String) objectMap.get("memberCode");
        dateFrom = (Date) objectMap.get("dateFrom");
        dateTo = (Date) objectMap.get("dateTo");
        status = (String) objectMap.get("status");

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_LUCKY_DRAW)
    @Action(value = "/luckyDrawPackageAdd")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String addPackage() {
        luckyDrawPackage = new LuckyDrawPackage(true);

        return ADD;
    }

    @Action(value = "/luckyDrawPackageSave")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String savePackage() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            luckyDrawPackage = luckyDrawService.doSaveNewLuckyDrawPackage(getLocale(), luckyDrawPackage, memberCode, "NEW");

            successMessage = getText("submitSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action(value = "/luckyDrawPackageDeactivate")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String deactivatePackage() {
        try {
            luckyDrawService.doDeactivateLuckyDrawPackage(packageNo, null);

            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_LUCKY_DRAW)
    @Action(value = "/luckyDrawItemList")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String itemList() {
        if (luckyDrawPackage != null) { // direct from add new package
            if (luckyDrawPackage.getSenderMemberId() != null) {
                Member member = memberService.getMember(luckyDrawPackage.getSenderMemberId());
                luckyDrawPackage.setSenderMember(member);
            }
        } else { // direct from view package or default winner page
            luckyDrawPackage = luckyDrawService.getLuckyDrawPackageByPackageNo(packageNo, null); //just need the header info (package no, member id, date)
        }

        blnLuckyDrawStarted = luckyDrawService.isLuckyDrawStarted(luckyDrawPackage.getPackageNo(), luckyDrawPackage.getLuckyDrawDate());

        return PAGE1;
    }

    @Action(value = "/luckyDrawPackageSaveItem")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String saveItem() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            luckyDrawPackage = luckyDrawService.doSaveNewLuckyDrawPackage(getLocale(), luckyDrawPackage, luckyDrawPackage.getSenderMember().getMemberCode(), "ADD_ITEM");

            successMessage = getText("submitSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action(value = "/luckyDrawPackageEdit")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String editItem() {
        luckyDrawPackage = luckyDrawService.getLuckyDrawPackageById(packageId);

        return PAGE2;
    }

    @Action(value = "/luckyDrawPackageUpdateItem")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String updateItem() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            luckyDrawPackage = luckyDrawService.doUpdateLuckyDrawPackage(getLocale(), luckyDrawPackage);

            if (luckyDrawPackage.getSenderMemberId() != null) {
                Member member = memberService.getMember(luckyDrawPackage.getSenderMemberId());
                luckyDrawPackage.setSenderMember(member);
            }

            successMessage = getText("updateSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action(value = "/luckyDrawPackageDelete")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String deleteItem() {
        try {
            luckyDrawService.doDeleteLuckyDrawPackageByItem(getLocale(), packageId);

            successMessage = getText("deleteSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_LUCKY_DRAW)
    @Action(value = "/luckyDrawDefaultWinnerList")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String defaultWinnerList() {
        luckyDrawPackage = luckyDrawService.getLuckyDrawPackageById(packageId);

        // for add default winner
        luckyDrawDefaultWinner = new LuckyDrawDefaultWinner(true);
        luckyDrawDefaultWinner.setLuckyDrawPackage(luckyDrawPackage);
        luckyDrawDefaultWinner.setPackageNo(luckyDrawPackage.getPackageNo());
        luckyDrawDefaultWinner.setPackageId(packageId);

        blnLuckyDrawStarted = luckyDrawService.isLuckyDrawStarted(luckyDrawPackage.getPackageNo(), luckyDrawPackage.getLuckyDrawDate());

        return PAGE3;
    }

    @Action(value = "/luckyDrawPackageSaveDefaultWinner")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String saveDefaultWinner() {
        try {
            VoUtil.toTrimUpperCaseProperties(luckyDrawDefaultWinner);
            luckyDrawService.doSaveLuckyDrawDefaultWinner(getLocale(), luckyDrawDefaultWinner, memberCode);

            successMessage = getText("submitSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action(value = "/luckyDrawDefaultWinnerDelete")
    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
    public String deleteDefaultWinner() {
        try {
            luckyDrawService.doDeleteLuckyDrawDefaultWinner(getLocale(), id);

            successMessage = getText("deleteSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

//    @Action(value = "/luckyDrawPackageUpdate")
//    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true, updateMode = true)
//    public String update() {
//        try {
//            VoUtil.toTrimUpperCaseProperties(this);
//            String liveTypes = "";
//            for (Iterator<String> iterator = liveCategories.iterator(); iterator.hasNext(); ) {
//                liveTypes += iterator.next();
//                if (iterator.hasNext())
//                    liveTypes += "|";
//            }
//
//            if (StringUtils.isNotBlank(liveStreamPrivate)) {
//                member.setLiveStreamPrivate(true);
//            } else {
//                member.setLiveStreamPrivate(false);
//            }
//
//            member.setLiveStreamTypes(liveTypes);
//            memberService.doUpdateMemberSetting(member);
//
//            successMessage = getText("successMessage.MemberAction.update");
//        } catch (Exception ex) {
//            addActionError(ex.getMessage());
//        }
//
//        return JSON;
//    }

//    @EnableTemplate(menuKey = {MP.FUNC_AD_LUCKY_DRAW})
//    @Action("/luckyDrawPackageInactive")
//    @Access(accessCode = AP.LUCKY_DRAW, adminMode = true)
//    public String inactiveLuckyDrawPackage() {
//        initialize();
//        getMemberInfo();
//
//        return packageList();
//    }

    // ---------------- GETTER & SETTER (START) -------------
    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LuckyDrawPackage getLuckyDrawPackage() {
        return luckyDrawPackage;
    }

    public void setLuckyDrawPackage(LuckyDrawPackage luckyDrawPackage) {
        this.luckyDrawPackage = luckyDrawPackage;
    }

    public LuckyDrawDefaultWinner getLuckyDrawDefaultWinner() {
        return luckyDrawDefaultWinner;
    }

    public void setLuckyDrawDefaultWinner(LuckyDrawDefaultWinner luckyDrawDefaultWinner) {
        this.luckyDrawDefaultWinner = luckyDrawDefaultWinner;
    }

    public String getPackageNo() {
        return packageNo;
    }

    public void setPackageNo(String packageNo) {
        this.packageNo = packageNo;
    }

    public Date getLuckyDrawDate() {
        return luckyDrawDate;
    }

    public void setLuckyDrawDate(Date luckyDrawDate) {
        this.luckyDrawDate = luckyDrawDate;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isBlnLuckyDrawStarted() {
        return blnLuckyDrawStarted;
    }

    public void setBlnLuckyDrawStarted(boolean blnLuckyDrawStarted) {
        this.blnLuckyDrawStarted = blnLuckyDrawStarted;
    }
    // ---------------- GETTER & SETTER (END) -------------
}
