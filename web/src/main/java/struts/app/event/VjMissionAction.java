package struts.app.event;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.event.vo.VjMissionConfig;
import com.compalsolutions.compal.event.vo.VjMissionDetail;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "missionList"),
        @Result(name = BaseAction.EDIT, location = "missionView"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default}),
        @Result(name = VjMissionAction.JSON_NOTE, type = BaseAction.ResultType.JSON, params = {"includeProperties", VjMissionAction.JSON_NOTE_INCLUDE_PROPERTIES}),
})
public class VjMissionAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    public static final String JSON_NOTE = "jsonNote";
    public static final String JSON_NOTE_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", member, member.memberDetail, member.memberDetail.profileName, member.memberDetail.whaleliveId, member.memberCode, member.point";

    private VjMissionService vjMissionService;
    private List<OptionBean> missionStatusList = new ArrayList<>();

    private String missionId;
    private VjMission mission;
    private String rejectRemark;
    private VjMissionDetail missionDetail;
    private VjMissionConfig missionConfig;
    private Member member;

    public VjMissionAction() {
        vjMissionService = Application.lookupBean(VjMissionService.BEAN_NAME, VjMissionService.class);
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_MISSION})
    @Action(value = "/missionList")
    @Accesses(access = { @Access(accessCode = AP.MISSION, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        missionStatusList  = optionBeanUtil.getMissionStatusWithOptionAll();

        return LIST;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_MISSION})
    @Action(value = "/missionView")
    @Access(accessCode = AP.MISSION, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String edit() {
        try {
            mission = vjMissionService.getMissionByMissionId(missionId);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action("/approveMission")
    @Access(accessCode = AP.MISSION, adminMode = true)
    public String approveMission() {
        try {
            vjMissionService.doApproveMission(getLocale(), missionId);
            successMessage = getText("approveSuccessfully");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/rejectMission")
    @Access(accessCode = AP.MISSION, adminMode = true)
    public String rejectMission() {
        try {
            vjMissionService.doRejectMission(getLocale(), missionId, rejectRemark);
            successMessage = getText("rejectSuccessfully");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/getWinnerInfo")
    @Access(accessCode = AP.MISSION, adminMode = true)
    public String getWinnerInfo() {
        try {
            member = vjMissionService.getSpecialMissionWinner(missionId);
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON_NOTE;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getMissionStatusList() {
        return missionStatusList;
    }

    public void setMissionStatusList(List<OptionBean> missionStatusList) {
        this.missionStatusList = missionStatusList;
    }

    public String getMissionId() {
        return missionId;
    }

    public void setMissionId(String missionId) {
        this.missionId = missionId;
    }

    public VjMission getMission() {
        return mission;
    }

    public void setMission(VjMission mission) {
        this.mission = mission;
    }

    public String getRejectRemark() {
        return rejectRemark;
    }

    public void setRejectRemark(String rejectRemark) {
        this.rejectRemark = rejectRemark;
    }

    public VjMissionDetail getMissionDetail() {
        return missionDetail;
    }

    public void setMissionDetail(VjMissionDetail missionDetail) {
        this.missionDetail = missionDetail;
    }

    public VjMissionConfig getMissionConfig() {
        return missionConfig;
    }

    public void setMissionConfig(VjMissionConfig missionConfig) {
        this.missionConfig = missionConfig;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
