package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "rankConfigList"),
        @Result(name = BaseAction.ADD, location = "rankConfigAdd"),
        @Result(name = BaseAction.EDIT, location = "rankConfigEdit"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}"}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON)})
public class RankConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_RANK_CONFIG_LIST = "rankConfigList";

    private RankService rankService;
    private MessageService messageService;

    private List<OptionBean> statuses = new ArrayList<>();

    private RankConfig rankConfig = new RankConfig(true);

    @ToTrim
    private Date dateFrom;
    private Date dateTo;

    private boolean clear;

    public RankConfigAction() {
        rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_RANK_CONFIG)
    @Action(value = "/rankConfigList")
    @Access(accessCode = AP.RANK_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatusWithOptionAll();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_RANK_CONFIG_LIST);
        } else if (session.containsKey(SESSION_RANK_CONFIG_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_RANK_CONFIG_LIST);
        } else {
            session.put(SESSION_RANK_CONFIG_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("dateFrom", dateFrom);
            objectMap.put("dateTo", dateTo);
        } else {
            dateFrom = (Date) objectMap.get("dateFrom");
            dateTo = (Date) objectMap.get("dateTo");
        }

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_RANK_CONFIG)
    @Action(value = "/rankConfigAdd")
    @Access(accessCode = AP.RANK_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statuses = optionBeanUtil.getAnnouncementStatus();

        return ADD;
    }

    @Action(value = "/rankConfigSave")
    @Access(accessCode = AP.RANK_CONFIG, adminMode = true, createMode = true)
    public String save() {
        try {
            boolean isExist = rankService.checkRank(rankConfig);
            if (isExist == true) {
                throw new ValidatorException("Entry overlap with other record");
            } else {
                rankService.doSaveRank(rankConfig);
                successMessage = getText("successMessage.RankConfig.save");
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_RANK_CONFIG)
    @Action(value = "/rankConfigEdit")
    @Access(accessCode = AP.RANK_CONFIG, adminMode = true, updateMode = true)
    public String edit() {
        try {
            rankConfig = rankService.getRank(rankConfig.getRankId());
            if (rankConfig == null) {
                addActionError(getText("invalidAnnouncement"));
                return list();
            }

            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            Locale locale = getLocale();

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/rankConfigUpdate")
    @Access(accessCode = AP.RANK_CONFIG, adminMode = true, updateMode = true)
    public String update() {
        try {
//            VoUtil.toTrimUpperCaseProperties(this, banner);
//            boolean isExist = rankService.checkRank(rankConfig);
//            if(isExist == true) {
//                throw new ValidatorException("Entry overlap with other record");
//            }else{
            rankService.updateRank(rankConfig);
            successMessage = getText("successMessage.RankConfig.update");
            // }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public RankConfig getRankConfig() {
        return rankConfig;
    }

    public void setRankConfig(RankConfig rankConfig) {
        this.rankConfig = rankConfig;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
