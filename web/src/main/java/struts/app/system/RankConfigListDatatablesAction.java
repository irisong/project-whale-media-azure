package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import struts.util.AjaxUtil;

import java.util.Date;
import java.util.List;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                RankConfigListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class RankConfigListDatatablesAction extends BaseSecureDatatablesAction<RankConfig> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.rankId, "
            + "data\\[\\d+\\]\\.rankFrom, "
            + "data\\[\\d+\\]\\.rankTo, "
            + "data\\[\\d+\\]\\.perExp, "
            + "data\\[\\d+\\]\\.rewardPercent, "
            + "data\\[\\d+\\]\\.startDate, "
            + "data\\[\\d+\\]\\.endDate, "
            + "data\\[\\d+\\]\\.remark";

    private Date dateFrom;
    private Date dateTo;

    private RankService rankService;


    public RankConfigListDatatablesAction() {
        rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
    }

    @Action(value = "/rankConfigListDatatables")
    @Access(accessCode = AP.RANK_CONFIG, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        rankService.findRankConfigForListing(getDatagridModel(), dateFrom, dateTo);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public RankService getRankService() {
        return rankService;
    }

    public void setRankService(RankService rankService) {
        this.rankService = rankService;
    }

    // ---------------- GETTER & SETTER (END) -------------
}