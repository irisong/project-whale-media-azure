package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "broadCastConfigList"),
        @Result(name = BaseAction.ADD, location = "broadCastConfigAdd"),
        @Result(name = BaseAction.EDIT, location = "broadCastConfigEdit"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class BroadCastConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_BROAD_CAST_CONFIG_LIST = "broadCastConfigList";

    private BroadcastService broadcastService;
    private MessageService messageService;

    private List<OptionBean> statuses = new ArrayList<>();

    private List<OptionBean> rankList = new ArrayList<>();

    private BroadcastConfig broadcastConfig = new BroadcastConfig(true);

    @ToTrim
    private String rankSearch;
    private String tempId;


    private boolean clear;

    public BroadCastConfigAction() {
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_BROAD_CAST_CONFIG)
    @Action(value = "/broadCastConfigList")
    @Access(accessCode = AP.BROAD_CAST, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        rankList = optionBeanUtil.getBroadcastRankWithAll();
        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_BROAD_CAST_CONFIG_LIST);
        } else if (session.containsKey(SESSION_BROAD_CAST_CONFIG_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_BROAD_CAST_CONFIG_LIST);
        } else {
            session.put(SESSION_BROAD_CAST_CONFIG_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("rankSearch", rankSearch);

        } else {

            rankSearch = (String)objectMap.get("rankSearch");

        }

        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BROAD_CAST_CONFIG)
    @Action(value = "/broadCastConfigAdd")
    @Access(accessCode = AP.BROAD_CAST, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String add() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        rankList = optionBeanUtil.getBroadcastRank();
        return ADD;
    }

    @Action(value = "/broadCastConfigSave")
    @Access(accessCode = AP.BROAD_CAST, adminMode = true, createMode = true)
    public String save() {
        try {
            boolean isExist = broadcastService.checkBroadcast(broadcastConfig);
            if(isExist == true) {
                throw new ValidatorException("Entry overlap with other record");
            }else{
                broadcastService.doSaveBroadcast(broadcastConfig);
                successMessage = getText("successMessage.Broadcast.save");
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BROAD_CAST_CONFIG)
    @Action(value = "/broadCastConfigEdit")
    @Access(accessCode = AP.BROAD_CAST, adminMode = true, updateMode = true)
    public String edit() {
        try {
            broadcastConfig = broadcastService.getBroadcast(broadcastConfig.getId());
            tempId = broadcastConfig.getRankName();
            OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
            rankList = optionBeanUtil.getBroadcastRank();
            if (broadcastConfig == null) {
                addActionError(getText("invalidAnnouncement"));
                return list();
            }


        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/broadCastConfigUpdate")
    @Access(accessCode = AP.BROAD_CAST, adminMode = true, updateMode = true)
    public String update() {
        try {
            boolean isExist = tempId.equalsIgnoreCase(broadcastConfig.getRankName())?false:broadcastService.checkBroadcast(broadcastConfig);
            if(isExist == true) {
                throw new ValidatorException("Entry overlap with other record");
            }else{
                broadcastService.updateBroadcast(broadcastConfig);
                successMessage = getText("successMessage.Broadcast.update");
           }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatuses() {
        return statuses;
    }

    public void setStatuses(List<OptionBean> statuses) {
        this.statuses = statuses;
    }

    public String getRankSearch() {
        return rankSearch;
    }

    public void setRankSearch(String rankSearch) {
        this.rankSearch = rankSearch;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public BroadcastConfig getBroadcastConfig() {
        return broadcastConfig;
    }

    public void setBroadcastConfig(BroadcastConfig broadcastConfig) {
        this.broadcastConfig = broadcastConfig;
    }

    public List<OptionBean> getRankList() {
        return rankList;
    }

    public void setRankList(List<OptionBean> rankList) {
        this.rankList = rankList;
    }

    public String getTempId() {
        return tempId;
    }

    public void setTempId(String tempId) {
        this.tempId = tempId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
