package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                LiveStreamGiftItemsListDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class LiveStreamGiftItemsListDatatablesAction extends BaseSecureDatatablesAction<SymbolicItem> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.symbolicId, "
            + "data\\[\\d+\\]\\.fileName, "
            + "data\\[\\d+\\]\\.name, "
            + "data\\[\\d+\\]\\.nameCn, "
            + "data\\[\\d+\\]\\.price, "
            + "data\\[\\d+\\]\\.seq, "
            + "data\\[\\d+\\]\\.fileUrl, "
            + "data\\[\\d+\\]\\.gifFileUrl, "
            + "data\\[\\d+\\]\\.type, "
            + "data\\[\\d+\\]\\.statusDesc, "
            + "data\\[\\d+\\]\\.status ";

    private PointService pointService;

    @ToUpperCase
    @ToTrim
    private String status;

    private String symbolicId;

    public LiveStreamGiftItemsListDatatablesAction() {
        pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
    }

    @Action(value = "/liveStreamGiftItemListDatatables")
    @Access(accessCode = AP.SYSTEM_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        pointService.findSymbolicItemForDatagrid(getDatagridModel(), symbolicId, null, null, status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSymbolicId() {
        return symbolicId;
    }

    public void setSymbolicId(String symbolicId) {
        this.symbolicId = symbolicId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
