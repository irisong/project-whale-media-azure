package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                FansIntimacyConfigListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class FansIntimacyConfigListDatatablesAction extends BaseSecureDatatablesAction<FansIntimacyConfig> {

    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.fansLevel, "
            + "data\\[\\d+\\]\\.deductPercent, "
            + "data\\[\\d+\\]\\.levelUpValue, "
            + "data\\[\\d+\\]\\.maxPerDay, "
            + "data\\[\\d+\\]\\.remark";

    private FansIntimacyService fansIntimacyService;

    public FansIntimacyConfigListDatatablesAction(){
        fansIntimacyService = Application.lookupBean(FansIntimacyService.BEAN_NAME,FansIntimacyService.class);
    }

    @Action(value = "/fansIntimacyConfigListDatatables")
    @Access(accessCode = AP.FANS_INTIMACY_CONFIG, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        fansIntimacyService.findFansIntimacyForListing(getDatagridModel());

        return JSON;
    }
}
