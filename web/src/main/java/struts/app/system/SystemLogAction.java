package struts.app.system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.application.ServerConfiguration;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "systemLog"), //
        @Result(name = BaseAction.LIST, location = "systemLogList"), //
        @Result(name = "systemLogShow", location = "systemLogShow"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) //
})
public class SystemLogAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String contentType;
    private String logFileName;
    private String logContent;
    private Date lastUpdateDate;
    private double fileSize; // in KB
    private List<String> fileNames = new ArrayList<String>();

    private ServerConfiguration serverConfiguration;

    public SystemLogAction() {
        serverConfiguration = Application.lookupBean(ServerConfiguration.BEAN_NAME, ServerConfiguration.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_SYSTEM_LOG)
    @Access(accessCode = AP.SYSTEM_LOG, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/systemLog")
    public String systemLog() {
        return INPUT;
    }

    @Access(accessCode = AP.SYSTEM_LOG, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/systemLogShow")
    public String showLog() {
        try {
            processFileContent();
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return "systemLogShow";
    }

    @Access(accessCode = AP.SYSTEM_LOG, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/systemLogList")
    public String list() {
        String rootPath = StringUtils.isNotBlank(serverConfiguration.getLogPath()) ? serverConfiguration.getLogPath() : "";
        String directory = rootPath + contentType;
        File dir = new File(directory);
        if (!dir.isDirectory())
            dir.mkdirs();

        String[] results = dir.list();
        fileNames = Arrays.asList(results);

        return LIST;
    }

    private void processFileContent() {
        StringBuffer sb = new StringBuffer();

        String rootPath = StringUtils.isNotBlank(serverConfiguration.getLogPath()) ? serverConfiguration.getLogPath() : "";
        String directory = rootPath + contentType;
        File dir = new File(directory);
        if (!dir.isDirectory())
            dir.mkdirs();

        String fileName = directory + "/" + logFileName;

        BufferedReader reader = null;
        try {
            File file = new File(fileName);
            lastUpdateDate = new Date(file.lastModified());
            fileSize = file.length() / 1024.0;

            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("<br>");
            }
        } catch (Exception ex) {
            throw new SystemErrorException(ex.getMessage(), ex.fillInStackTrace());
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        logContent = sb.toString();
    }

    // ---------------- GETTER & SETTER (START) -------------
    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public String getLogFileName() {
        return logFileName;
    }

    public void setLogFileName(String logFileName) {
        this.logFileName = logFileName;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getLogContent() {
        return logContent;
    }

    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }

    // ---------------- GETTER & SETTER (LOG) -------------
}
