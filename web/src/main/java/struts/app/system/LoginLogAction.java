package struts.app.system;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "loginLogList"), //
        @Result(name = BaseAction.LIST, location = "loginLogList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class LoginLogAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String username;
    private String ipAddress;
    private Date dateFrom;
    private Date dateTo;
    private List<OptionBean> statusList = new ArrayList<OptionBean>();

    @EnableTemplate(menuKey = MP.FUNC_AD_LOGIN_LOG)
    @Access(accessCode = AP.LOGIN_LOG, createMode = true, adminMode = true, readMode = true)
    @Action("/loginLogList")
    public String list() {
        statusList.add(new OptionBean("", getText("all")));
        statusList.add(new OptionBean(SessionLog.LOGIN_STATUS_SUCCESS, SessionLog.LOGIN_STATUS_SUCCESS));
        statusList.add(new OptionBean(SessionLog.LOGIN_STATUS_FAILED, SessionLog.LOGIN_STATUS_FAILED));
        statusList.add(new OptionBean(SessionLog.LOGIN_STATUS_FAILED_CAPTCHA, SessionLog.LOGIN_STATUS_FAILED_CAPTCHA));

        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
