package struts.app.system;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.dispatcher.HttpParameters;
import org.apache.struts2.dispatcher.Parameter;
import org.apache.struts2.interceptor.HttpParametersAware;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.exception.SystemErrorException;
import com.compalsolutions.compal.function.schedule.RunTaskParameter;
import com.compalsolutions.compal.function.schedule.SchedulerTaskConfig;
import com.compalsolutions.compal.function.schedule.SchedulerTaskFactory;
import com.compalsolutions.compal.function.schedule.service.ScheduleService;
import com.compalsolutions.compal.function.schedule.vo.RunTask;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "backendAdd"), //
        @Result(name = BaseAction.LIST, location = "backendList"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "backendAdd"), //
        @Result(name = BaseAction.SHOW, location = "backendShow"), //
        @Result(name = "showLog", location = "showLog"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class RunTaskAction extends BaseSecureAction implements HttpParametersAware {
    private static final long serialVersionUID = 1L;

    private ScheduleService scheduleService;

    private List<SchedulerTaskConfig> taskConfigs = new ArrayList<SchedulerTaskConfig>();

    private HttpParameters params;
    private boolean ableToRun = true;
    private String processCode;
    private String processName;
    private String processDesc;
    private String taskId;
    private boolean displayLog;
    private String logContent;
    private RunTask runTask = new RunTask(true);
    private SchedulerTaskConfig taskConfig = new SchedulerTaskConfig();

    // use by /backendList
    private Date dateFrom;
    private Date dateTo;
    private List<OptionBean> taskConfigOptions = new ArrayList<OptionBean>();
    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> logOptions = new ArrayList<OptionBean>();

    public RunTaskAction() {
        scheduleService = Application.lookupBean(ScheduleService.BEAN_NAME, ScheduleService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BACKEND_PROCESS_RUN)
    @Action("/backendShow")
    @Access(accessCode = AP.BACKEND_TASK_RUN, createMode = true, readMode = true, adminMode = true)
    public String show() {
        runTask = scheduleService.getRunTask(taskId);
        if (runTask == null)
            addActionError(getText("error.task.invalid"));
        else {
            taskConfig = SchedulerTaskFactory.getInstance().getConfig(runTask.getTaskCode());
        }

        return SHOW;
    }

    @Action("/backendShowLog")
    @Access(accessCode = AP.BACKEND_TASK_RUN, createMode = true, readMode = true, adminMode = true)
    public String showLog() {
        runTask = scheduleService.getRunTask(taskId);

        try {
            logContent = processFileContent();
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return "showLog";
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BACKEND_PROCESS_RUN)
    @Action("/backend")
    @Access(accessCode = AP.BACKEND_TASK_RUN, createMode = true, adminMode = true)
    public String add() {
        if (!SchedulerTaskFactory.canRun) {
            addActionError(getText("error.another.task.running"));
            ableToRun = false;
        }

        taskConfigs = SchedulerTaskFactory.getInstance().getAllConfigByAccessGroup(Global.UserType.HQ);

        return ADD;
    }

    @Action("/backendSave")
    @Access(accessCode = AP.BACKEND_TASK_RUN, createMode = true, adminMode = true)
    public String save() {
        SchedulerTaskFactory taskFactory = SchedulerTaskFactory.getInstance();
        if (!SchedulerTaskFactory.canRun) {
            addActionError(getText("error.another.task.running"));
        }

        SchedulerTaskConfig config = taskFactory.getConfig(processCode);
        if (config == null) {
            addActionError(getText("error.task.invalid"));
        }

        RunTask task = new RunTask();
        task.setTaskCode(processCode);

        for (RunTaskParameter p : config.getParameters()) {
            if (params.containsKey(p.getName())) {
                Parameter param = params.get(p.getName());
                if (param != null) {
                    String v = param.getValue();
                    if (StringUtils.isNotBlank(v)) {
                        task.getParams().put(p.getName(), v);
                    }
                }
            }
        }

        try {
            taskFactory.launchNewTask(task, false);
            taskId = task.getTaskId();
            successMessage = getText("successMessage.RunTask.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_BACKEND_PROCESS_VIEW)
    @Access(accessCode = AP.BACKEND_TASK_VIEW, createMode = true, adminMode = true, readMode = true)
    @Action("/backendList")
    public String list() {
        taskConfigs = SchedulerTaskFactory.getInstance().getAllConfigByAccessGroup(Global.UserType.HQ);

        taskConfigOptions.add(new OptionBean("", getText("all")));
        for (SchedulerTaskConfig config : taskConfigs) {
            taskConfigOptions.add(new OptionBean(config.getCode(), config.getCode() + " - " + config.getName()));
        }

        statusList.add(new OptionBean("", getText("all")));
        statusList.add(new OptionBean(RunTask.SUCCESS, RunTask.SUCCESS));
        statusList.add(new OptionBean(RunTask.ERROR, RunTask.ERROR));
        statusList.add(new OptionBean(RunTask.RUNNING, RunTask.RUNNING));

        logOptions.add(new OptionBean("", getText("all")));
        logOptions.add(new OptionBean("true", getText("enableLog")));
        logOptions.add(new OptionBean("false", getText("disableLog")));

        return LIST;
    }

    private String processFileContent() {
        StringBuffer sb = new StringBuffer();

        String directory = SchedulerTaskFactory.getLogFilePath() + runTask.getTaskCode();
        File dir = new File(directory);
        if (!dir.isDirectory())
            dir.mkdirs();

        String fileName = directory + "/" + runTask.getTaskNo() + "_" + (displayLog ? "LOG" : "DEBUG") + ".out";

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(fileName)));

            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line).append("<br>");
            }
        } catch (Exception e) {
            throw new SystemErrorException(e.getMessage(), e.fillInStackTrace());
        } finally {
            try {
                if (reader != null)
                    reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<SchedulerTaskConfig> getTaskConfigs() {
        return taskConfigs;
    }

    public void setTaskConfigs(List<SchedulerTaskConfig> taskConfigs) {
        this.taskConfigs = taskConfigs;
    }

    public boolean isAbleToRun() {
        return ableToRun;
    }

    public void setAbleToRun(boolean ableToRun) {
        this.ableToRun = ableToRun;
    }

    @Override
    public void setParameters(HttpParameters parameters) {
        this.params = parameters;
    }

    public String getProcessCode() {
        return processCode;
    }

    public void setProcessCode(String processCode) {
        this.processCode = processCode;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getProcessDesc() {
        return processDesc;
    }

    public void setProcessDesc(String processDesc) {
        this.processDesc = processDesc;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    public RunTask getRunTask() {
        return runTask;
    }

    public void setRunTask(RunTask runTask) {
        this.runTask = runTask;
    }

    public SchedulerTaskConfig getTaskConfig() {
        return taskConfig;
    }

    public void setTaskConfig(SchedulerTaskConfig taskConfig) {
        this.taskConfig = taskConfig;
    }

    public boolean isDisplayLog() {
        return displayLog;
    }

    public void setDisplayLog(boolean displayLog) {
        this.displayLog = displayLog;
    }

    public String getLogContent() {
        return logContent;
    }

    public void setLogContent(String logContent) {
        this.logContent = logContent;
    }

    public List<OptionBean> getTaskConfigOptions() {
        return taskConfigOptions;
    }

    public void setTaskConfigOptions(List<OptionBean> taskConfigOptions) {
        this.taskConfigOptions = taskConfigOptions;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getLogOptions() {
        return logOptions;
    }

    public void setLogOptions(List<OptionBean> logOptions) {
        this.logOptions = logOptions;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
