package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                BroadCastConfigListDatatablesAction.JSON_INCLUDE_PROPERTIES }) })
public class BroadCastConfigListDatatablesAction extends BaseSecureDatatablesAction<BroadcastConfig> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.rankName, "
            + "data\\[\\d+\\]\\.basicSalary, "
            + "data\\[\\d+\\]\\.minDuration, "
            + "data\\[\\d+\\]\\.minDay, "
            + "data\\[\\d+\\]\\.minPoint, "
            + "data\\[\\d+\\]\\.splitPercentage";

    private String rankSearch;

    private BroadcastService broadcastService;


    public BroadCastConfigListDatatablesAction() {
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
    }

    @Action(value = "/broadCastConfigListDatatables")
    @Access(accessCode = AP.BROAD_CAST, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        broadcastService.findBroadcastConfigForListing(getDatagridModel(),rankSearch);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getRankSearch() {
        return rankSearch;
    }

    public void setRankSearch(String rankSearch) {
        this.rankSearch = rankSearch;
    }


    // ---------------- GETTER & SETTER (END) -------------
}