package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.Symbolic;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                LiveStreamGiftListDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class LiveStreamGiftListDatatablesAction extends BaseSecureDatatablesAction<Symbolic> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.symbolicId, "
            + "data\\[\\d+\\]\\.category, "
            + "data\\[\\d+\\]\\.categoryCn, "
            + "data\\[\\d+\\]\\.seq, "
            + "data\\[\\d+\\]\\.statusDesc, "
            + "data\\[\\d+\\]\\.status " ;

    private PointService pointService;

    public LiveStreamGiftListDatatablesAction() {
        pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
    }

    @Action(value = "/liveStreamGiftListDatatables")
    @Access(accessCode = AP.SYSTEM_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        pointService.findSymbolicForListing(getDatagridModel(), null, null, null);

        return JSON;
    }
}
