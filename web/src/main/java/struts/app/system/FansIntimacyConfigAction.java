package struts.app.system;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.FansIntimacyService;
import com.compalsolutions.compal.member.vo.FansIntimacyConfig;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "fansIntimacyConfigList"),
        @Result(name = BaseAction.ADD, location = "fansIntimacyConfigAdd"),
        @Result(name = BaseAction.EDIT, location = "fansIntimacyConfigEdit"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT,
                params = {"actionName", "templateMessage", "namespace", "/app", "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}"}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON)})

public class FansIntimacyConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private FansIntimacyService fansIntimacyService;

    private FansIntimacyConfig fansIntimacyConfig = new FansIntimacyConfig(true);

    private boolean clear;

    public FansIntimacyConfigAction() {
        fansIntimacyService = Application.lookupBean(FansIntimacyService.BEAN_NAME, FansIntimacyService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_FANS_INTIMACY_CONFIG)
    @Action(value = "/fansIntimacyConfigList")
    @Access(accessCode = AP.FANS_INTIMACY_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list(){
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_FANS_INTIMACY_CONFIG)
    @Action(value = "/fansIntimacyConfigAdd")
    @Access(accessCode = AP.FANS_INTIMACY_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String add() {
        return ADD;
    }

    @Action(value = "/fansIntimacyConfigSave")
    @Access(accessCode = AP.FANS_INTIMACY_CONFIG, adminMode = true, createMode = true)
    public String save() {
        try {
            fansIntimacyService.doSaveFansIntimacyConfig(getLocale(), fansIntimacyConfig);
            successMessage = getText("successMessage.FansIntimacyConfig.save");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }
    @EnableTemplate(menuKey = MP.FUNC_AD_FANS_INTIMACY_CONFIG)
    @Action(value = "/fansIntimacyConfigEdit")
    @Access(accessCode = AP.FANS_INTIMACY_CONFIG, adminMode = true, updateMode = true)
    public String edit() {
        try {
            fansIntimacyConfig = fansIntimacyService.getFansIntimacyConfig(fansIntimacyConfig.getId());
            if (fansIntimacyConfig == null) {
                addActionError(getText("fansIntimacyConfigNotExist"));
                return list();
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action(value = "/fansIntimacyConfigUpdate")
    @Access(accessCode = AP.RANK_CONFIG, adminMode = true, updateMode = true)
    public String update() {
        try {
            fansIntimacyService.updateFansIntimacyConfig(getLocale(), fansIntimacyConfig); //already is new fans intimacy from jsp
            successMessage = getText("successMessage.FansIntimacyConfig.update");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public FansIntimacyConfig getFansIntimacyConfig() {
        return fansIntimacyConfig;
    }

    public void setFansIntimacyConfig(FansIntimacyConfig fansIntimacyConfig) {
        this.fansIntimacyConfig = fansIntimacyConfig;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
