package struts.app.system;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.general.vo.SystemConfigImage;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.SHOW, location = "systemConfigView"), @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", Global.JsonInclude.Default }) //
})
public class SystemConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private SystemConfigService systemConfigService;

    private SystemConfig systemConfig = new SystemConfig(true);

    private SystemConfigImage systemConfigImage = new SystemConfigImage(true);

    private List<SystemConfigImage> systemConfigImages = new ArrayList<SystemConfigImage>();

    private List<OptionBean> imageTypes = new ArrayList<OptionBean>();

    private File fileUpload;

    private String fileUploadContentType;

    private String fileUploadFileName;

    private InputStream fileInputStream;

    private String imageId;

    private List<CountryDesc> countries = new ArrayList<CountryDesc>();

    private String countryCode;

    public SystemConfigAction() {
        systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
    }

    @EnableTemplate(menuKey = { MP.FUNC_AD_SYSTEM_CONFIG })
    @Action("/systemConfig")
    @Accesses(access = { @Access(accessCode = AP.SYSTEM_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    @Override
    public String execute() throws Exception {
        systemConfig = systemConfigService.getDefaultSystemConfig();
        systemConfigImages = systemConfigService.findSystemConfigImages();

        countries = new ArrayList<>();

        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        imageTypes = optionBeanUtil.getSystemConfigImageType();
        return SHOW;
    }

    @Action("/systemConfigToogleRegister")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String toogleRegister() {
        try {
            systemConfigService.doToogleMemberRegisterForSystemConfig();

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
            if (systemConfig.getMemberRegister()) {
                successMessage = getText("successMessage.SystemConfigAction.enableRegisterSuccess");
            } else {
                successMessage = getText("successMessage.SystemConfigAction.disableRegisterSuccess");
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/systemConfigToogleLogin")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String toogleLogin() {
        try {
            systemConfigService.doToogleMemberLoginForSystemConfig();

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
            if (systemConfig.getMemberLogin()) {
                successMessage = getText("successMessage.SystemConfigAction.enableLoginSuccess");
            } else {
                successMessage = getText("successMessage.SystemConfigAction.disableLoginSuccess");
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/systemConfigApplicationNameSave")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String saveSystemConfigApplicationName() {
        try {
            VoUtil.toTrimProperties(this, systemConfig);
            systemConfigService.updateApplicationName(systemConfig.getApplicationName());

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/systemConfigCompanyNameSave")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String saveSystemConfigCompanyName() {
        try {
            VoUtil.toTrimProperties(this, systemConfig);
            systemConfigService.updateCompanyName(systemConfig.getCompanyName());

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/systemConfigSupportEmailSave")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String saveSystemConfigSupportEmail() {
        try {
            VoUtil.toTrimProperties(this, systemConfig);
            systemConfigService.updateSupportEmail(systemConfig.getSupportEmail());

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/systemConfigQrCodeExpirySave")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String saveSystemConfigQrCodeExpiry() {
        try {
            VoUtil.toTrimProperties(this, systemConfig);
            systemConfigService.updateQRCodeExpiry(systemConfig.getQrCodeExpiry());

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }


    @Action(value = "/systemConfigImageSave")
    @Access(accessCode = AP.SYSTEM_CONFIG, adminMode = true, createMode = true, updateMode = true)
    public String save() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, systemConfigImage);
            systemConfigImage.setFilename(fileUploadFileName);
            systemConfigImage.setFileSize(fileUpload.length());
            systemConfigImage.setContentType(fileUploadContentType);
            systemConfigImage.setData(FileUtils.readFileToByteArray(fileUpload));
            systemConfigService.saveOrUpdateSystemConfigImage(systemConfigImage);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/systemConfigImageDelete")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String delete() {
        try {
            systemConfigService.deleteSystemConfigImageById(imageId);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/systemConfigDefaultCountrySave")
    @Access(accessCode = AP.SYSTEM_CONFIG, updateMode = true, adminMode = true)
    public String saveSystemConfigDefaultCountry() {
        try {
            VoUtil.toTrimProperties(this, systemConfig);
            systemConfigService.updateDefaultCountryCode(systemConfig.getCountryCode());

            systemConfig = systemConfigService.getDefaultSystemConfig();
            WebUtil.setSystemConfig(systemConfig);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public SystemConfig getSystemConfig() {
        return systemConfig;
    }

    public void setSystemConfig(SystemConfig systemConfig) {
        this.systemConfig = systemConfig;
    }

    public SystemConfigImage getSystemConfigImage() {
        return systemConfigImage;
    }

    public void setSystemConfigImage(SystemConfigImage systemConfigImage) {
        this.systemConfigImage = systemConfigImage;
    }

    public List<SystemConfigImage> getSystemConfigImages() {
        return systemConfigImages;
    }

    public void setSystemConfigImages(List<SystemConfigImage> systemConfigImages) {
        this.systemConfigImages = systemConfigImages;
    }

    public List<OptionBean> getImageTypes() {
        return imageTypes;
    }

    public void setImageTypes(List<OptionBean> imageTypes) {
        this.imageTypes = imageTypes;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public List<CountryDesc> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryDesc> countries) {
        this.countries = countries;
    }

    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
