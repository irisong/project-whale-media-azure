package struts.app.system;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.point.vo.Symbolic;
import com.compalsolutions.compal.point.vo.SymbolicItem;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import javax.xml.bind.ValidationException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "liveStreamGift"),
        @Result(name = BaseAction.EDIT, location = "liveStreamGiftCategoryEdit"),
        @Result(name = BaseAction.EDIT_DETAIL, location = "liveStreamGiftEdit"),
        @Result(name = LiveStreamGiftAction.JSON_NOTE, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", LiveStreamGiftAction.GET_JSON_NOTE_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default})
})
public class LiveStreamGiftAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;
    public static final String JSON_NOTE = "jsonNote";
    public static final String GET_JSON_NOTE_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", seq";

    private PointService pointService;

    private static final String SESSION_LiVESTREAM_GIFT_ITEM_LIST = "liveStreamGift";

    private Symbolic symbolic = new Symbolic();
    private SymbolicItem symbolicItem = new SymbolicItem();

    private boolean clear;

    // for update status
    @ToUpperCase
    @ToTrim
    private String status;
    private String symbolicId;

    // for display/add/edit
    private String id;
    private String sendGlobalMsg;
    private List<String> remark = new ArrayList<>();
    private List<OptionBean> remarkList = new ArrayList<>();
    private List<OptionBean> symbolicItemType = new ArrayList<>();
    private List<OptionBean> giftCategories = new ArrayList<>();
    private List<OptionBean> statusList = new ArrayList<>();
    private List<SymbolicItem> symbolicItemList = new ArrayList<>();

    // for return latest seq
    private int seq;

    // File Upload
    private File fileUpload;
    private InputStream fileInputStream;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private String fileUrl;

    // GIF File Upload
    private File gifFileUpload;
    private InputStream gifFileInputStream;
    private String gifFileUploadContentType;
    private String gifFileUploadFileName;
    private String gifFileUrl;

    public LiveStreamGiftAction() {
        pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
    }

    private void init() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);

        statusList = optionBeanUtil.getStatusWithOptionAll();
        remarkList.add(new OptionBean(SymbolicItem.WHALE_VIP_CARD, getText("whaleVipCard")));
        symbolicItemType = optionBeanUtil.getSymbolicItemFileType();

        List<Symbolic> symbolicItems = pointService.findAllActiveSymbolic();
        for (Symbolic symbolic : symbolicItems) {
            boolean isCn = locale.equals(Global.LOCALE_CN);
            giftCategories.add(new OptionBean(symbolic.getSymbolicId(), isCn ? symbolic.getCategoryCn() : symbolic.getCategory()));
        }
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_GIFT_CONFIG})
    @Action(value = "/liveStreamGift")
    @Access(accessCode = AP.GIFT_CONFIG, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        init();
        VoUtil.toTrimUpperCaseProperties(this);

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_LiVESTREAM_GIFT_ITEM_LIST);
        } else if (session.containsKey(SESSION_LiVESTREAM_GIFT_ITEM_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_LiVESTREAM_GIFT_ITEM_LIST);
        } else {
            session.put(SESSION_LiVESTREAM_GIFT_ITEM_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("symbolicId", symbolicId);
        } else {
            symbolicId = (String) objectMap.get("symbolicId");
        }

        return LIST;
    }

    /************
     * SYMBOLIC
     ************/

    @Action("/getNextCategorySeq")
    public String getNextCategorySeq() {
        VoUtil.toTrimUpperCaseProperties(this);
        seq = pointService.getNextSymbolicSeq();

        return JSON_NOTE;
    }

    @Action("/saveGiftCategory")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String saveGiftCategory() {
        try {
            pointService.doCreateNewGiftCategory(symbolic);
            successMessage = getText("submitSuccessfully");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_GIFT_CONFIG})
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Action(value = "/editGiftCategory")
    public String editGiftCategory() {
        try {
            symbolic = pointService.findSymbolic(id);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @Action("/updateSymbolic")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String updateSymbolic() throws IOException {
        try {
            pointService.doUpdateGiftCategory(symbolic);
            successMessage = getText("updateSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/updateCategoryStatus")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String updateCategoryStatus() {
        try {
            pointService.doUpdateSymbolicCategoryStatus(symbolicId, status);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    /*****************
     * SYMBOLIC ITEM
     *****************/

    @Action("/getNextItemSeq")
    public String getNextItemSeq() {
        VoUtil.toTrimUpperCaseProperties(this);
        seq = pointService.getNextSymbolicItemSeq(symbolicItem.getSymbolicId());

        return JSON_NOTE;
    }

    @Action("/saveSymbolicItem")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String saveSymbolicItem() throws IOException {
        try {
            if (StringUtils.isBlank(fileUploadFileName)) {
                throw new ValidationException(getText("imagePngRequired"));
            }
            if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION) && StringUtils.isBlank(gifFileUploadFileName)) {
                throw new ValidationException(getText("imageGifRequired"));
            }
            if (!StringUtils.endsWithIgnoreCase(fileUploadFileName, "png")) {
                throw new ValidationException(getText("imageMustBePngExtension"));
            }
            if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION) && !StringUtils.endsWithIgnoreCase(gifFileUploadFileName, "gif")) {
                throw new ValidationException(getText("gifImageMustBeGifExtension"));
            }

            symbolicItem.setStatus(Global.Status.ACTIVE);

            String remarks = "";
            for (Iterator<String> iterator = remark.iterator(); iterator.hasNext(); ) {
                remarks += iterator.next();
                if (iterator.hasNext())
                    remarks += "|";
            }
            symbolicItem.setRemark(remarks);

            if (StringUtils.isNotBlank(sendGlobalMsg)) {
                symbolicItem.setSendGlobalMsg(true);
            } else {
                symbolicItem.setSendGlobalMsg(false);
            }

            byte[] readFile = FileUtils.readFileToByteArray(fileUpload);

            symbolicItem.setData(pointService.getScaledImage(readFile, 200, 200));
            symbolicItem.setFilename(fileUploadFileName);
            symbolicItem.setContentType(fileUploadContentType);
            symbolicItem.setFileSize(fileUpload.length());

            if (symbolicItem.getType().equalsIgnoreCase(SymbolicItem.GIFT_ANIMATION)) {
                if (StringUtils.isNotBlank(gifFileUploadFileName)) {
                    symbolicItem.setGifData(FileUtils.readFileToByteArray(gifFileUpload));
                    symbolicItem.setGifFilename(gifFileUploadFileName);
                    symbolicItem.setGifContentType(gifFileUploadContentType);
                    symbolicItem.setGifFileSize(gifFileUpload.length());
                }
            }

            pointService.doProcessGiftItemAndFile(symbolicItem);
            successMessage = getText("giftItemCreatedSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_GIFT_CONFIG})
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Action(value = "/editSymbolicItem")
    public String editSymbolicItem() {
        try {
            init();
            symbolicItem = pointService.findSymbolicItem(id);
            String[] remarks = StringUtils.split(symbolicItem.getRemark(), "|");
            if (remarks != null) {
                for (String dbRemark : remarks) {
                    remark.add(dbRemark);
                }
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT_DETAIL;
    }

    @Action("/updateSymbolicItem")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String updateSymbolicItem() throws IOException {
        try {
            if (StringUtils.isNotBlank(fileUploadFileName) && !StringUtils.endsWithIgnoreCase(fileUploadFileName, "png")) {
                throw new ValidationException(getText("imageMustBePngExtension")); //Invalid upload type, image must be png file
            }
            if (StringUtils.isNotBlank(gifFileUploadFileName) && !StringUtils.endsWithIgnoreCase(gifFileUploadFileName, "gif")) {
                throw new ValidationException(getText("gifImageMustBeGifExtension")); //Invalid upload type, Gif image must be gif file
            }

            boolean chgImage= false;
            boolean chgGifImage= false;

            String remarks = "";
            for (Iterator<String> iterator = remark.iterator(); iterator.hasNext(); ) {
                remarks += iterator.next();
                if (iterator.hasNext())
                    remarks += "|";
            }
            symbolicItem.setRemark(remarks);

            if (StringUtils.isNotBlank(sendGlobalMsg)) {
                symbolicItem.setSendGlobalMsg(true);
            } else {
                symbolicItem.setSendGlobalMsg(false);
            }

            if (StringUtils.isNotBlank(fileUploadFileName)) { //uploaded new file
                byte[] readFile = FileUtils.readFileToByteArray(fileUpload);

                symbolicItem.setData(pointService.getScaledImage(readFile, 200, 200));
                symbolicItem.setFilename(fileUploadFileName);
                symbolicItem.setContentType(fileUploadContentType);
                symbolicItem.setFileSize(fileUpload.length());
                chgImage = true;
            }

            if (StringUtils.isNotBlank(gifFileUploadFileName)) { //uploaded new file
                symbolicItem.setGifData(FileUtils.readFileToByteArray(gifFileUpload));
                symbolicItem.setGifFilename(gifFileUploadFileName);
                symbolicItem.setGifContentType(gifFileUploadContentType);
                symbolicItem.setGifFileSize(gifFileUpload.length());
//                symbolicItem.setStatus(Global.Status.INACTIVE);
                chgGifImage = true;
            }

            pointService.doUpdateGiftItemAndFile(getLocale(), symbolicItem, chgImage, chgGifImage);
            successMessage = getText("giftItemUpdateSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/updateItemStatus")
    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String updateItemStatus() {
        try {
            pointService.doUpdateSymbolicItemStatus(getLocale(), id, status);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

//    @Action("/activateGifBatch")
//    @Access(accessCode = AP.GIFT_CONFIG, createMode = true, readMode = true, deleteMode = true, updateMode = true)
//    public String activateGifBatch() {
//        try {
//            pointService.doActivateBatchVersion();
//            successMessage = getText("giftBatchUpdateSuccess");
//        } catch (Exception e) {
//            addActionError(e.getMessage());
//        }
//
//        return JSON;
//    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getSeq() {
        return seq;
    }

    public void setSeq(int seq) {
        this.seq = seq;
    }

    public String getSymbolicId() {
        return symbolicId;
    }

    public void setSymbolicId(String symbolicId) {
        this.symbolicId = symbolicId;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public Symbolic getSymbolic() {
        return symbolic;
    }

    public void setSymbolic(Symbolic symbolic) {
        this.symbolic = symbolic;
    }

    public SymbolicItem getSymbolicItem() {
        return symbolicItem;
    }

    public void setSymbolicItem(SymbolicItem symbolicItem) {
        this.symbolicItem = symbolicItem;
    }

    public List<SymbolicItem> getSymbolicItemList() {
        return symbolicItemList;
    }

    public void setSymbolicItemList(List<SymbolicItem> symbolicItemList) {
        this.symbolicItemList = symbolicItemList;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getFileUrl() {
        return fileUrl;
    }

    public void setFileUrl(String fileUrl) {
        this.fileUrl = fileUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getGiftCategories() {
        return giftCategories;
    }

    public void setGiftCategories(List<OptionBean> giftCategories) {
        this.giftCategories = giftCategories;
    }

    public List<String> getRemark() {
        return remark;
    }

    public void setRemark(List<String> remark) {
        this.remark = remark;
    }

    public List<OptionBean> getRemarkList() {
        return remarkList;
    }

    public void setRemarkList(List<OptionBean> remarkList) {
        this.remarkList = remarkList;
    }

    public File getGifFileUpload() {
        return gifFileUpload;
    }

    public void setGifFileUpload(File gifFileUpload) {
        this.gifFileUpload = gifFileUpload;
    }

    public String getGifFileUploadContentType() {
        return gifFileUploadContentType;
    }

    public void setGifFileUploadContentType(String gifFileUploadContentType) {
        this.gifFileUploadContentType = gifFileUploadContentType;
    }

    public String getGifFileUploadFileName() {
        return gifFileUploadFileName;
    }

    public void setGifFileUploadFileName(String gifFileUploadFileName) {
        this.gifFileUploadFileName = gifFileUploadFileName;
    }

    public InputStream getGifFileInputStream() {
        return gifFileInputStream;
    }

    public void setGifFileInputStream(InputStream gifFileInputStream) {
        this.gifFileInputStream = gifFileInputStream;
    }

    public String getGifFileUrl() {
        return gifFileUrl;
    }

    public void setGifFileUrl(String gifFileUrl) {
        this.gifFileUrl = gifFileUrl;
    }

    public List<OptionBean> getSymbolicItemType() {
        return symbolicItemType;
    }

    public void setSymbolicItemType(List<OptionBean> symbolicItemType) {
        this.symbolicItemType = symbolicItemType;
    }

    public String getSendGlobalMsg() {
        return sendGlobalMsg;
    }

    public void setSendGlobalMsg(String sendGlobalMsg) {
        this.sendGlobalMsg = sendGlobalMsg;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
