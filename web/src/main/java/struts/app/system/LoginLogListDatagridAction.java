package struts.app.system;

import java.util.Date;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.log.service.SessionLogService;
import com.compalsolutions.compal.function.log.vo.SessionLog;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                LoginLogListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class LoginLogListDatagridAction extends BaseDatagridAction<SessionLog> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", " //
            + "rows\\[\\d+\\]\\.sessionId, "//
            + "rows\\[\\d+\\]\\.username, "//
            + "rows\\[\\d+\\]\\.ipAddress, " //
            + "rows\\[\\d+\\]\\.loginStatus, " //
            + "rows\\[\\d+\\]\\.datetimeAdd";

    private SessionLogService sessionLogService;

    @ToTrim
    @ToUpperCase
    private String username;

    @ToTrim
    private String ipAddress;
    private Date dateFrom;
    private Date dateTo;

    @ToTrim
    @ToUpperCase
    private String status;

    public LoginLogListDatagridAction() {
        sessionLogService = Application.lookupBean(SessionLogService.BEAN_NAME, SessionLogService.class);
    }

    @Action(value = "/loginLogListDatagrid")
    @Access(accessCode = AP.LOGIN_LOG, createMode = true, adminMode = true, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        sessionLogService.findSessionLogsForListing(getDatagridModel(), username, ipAddress, dateFrom, dateTo, status);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
