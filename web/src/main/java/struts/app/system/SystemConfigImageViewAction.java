package struts.app.system;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.service.SystemConfigService;
import com.compalsolutions.compal.general.vo.SystemConfigImage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;

@Results(value = { @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.SHOW, type = BaseAction.ResultType.STREAM, params = { //
                "contentDisposition", "inline", //
                "contentType", "${contentType}", //
                "inputName", "inputStream" }) //
})
public class SystemConfigImageViewAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private String contentType;
    private InputStream inputStream;

    private String imageId;
    private String imageType;

    private SystemConfigService systemConfigService;

    public SystemConfigImageViewAction() {
        systemConfigService = Application.lookupBean(SystemConfigService.BEAN_NAME, SystemConfigService.class);
    }

    @Action("/systemConfigImageView")
    @Override
    public String execute() throws Exception {
        try {
            SystemConfigImage systemConfigImage = systemConfigService.findSystemConfigImageById(imageId);

            if (systemConfigImage == null) {
                addActionError(getText("invalidImage"));
                return INPUT;
            }

            contentType = systemConfigImage.getContentType();
            inputStream = new ByteArrayInputStream(systemConfigImage.getData());
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return INPUT;
        }

        return SHOW;
    }

    @Action("/systemConfigImageViewByType")
    public String viewSystemConfigImageViewByType() throws Exception {
        try {
            SystemConfigImage systemConfigImage = systemConfigService.findSystemConfigImageByType(imageType);

            if (systemConfigImage == null) {
                addActionError(getText("invalidImage"));
                return INPUT;
            }

            contentType = systemConfigImage.getContentType();
            inputStream = new ByteArrayInputStream(systemConfigImage.getData());
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return INPUT;
        }

        return SHOW;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public InputStream getInputStream() {
        return inputStream;
    }

    public void setInputStream(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    public String getImageId() {
        return imageId;
    }

    public void setImageId(String imageId) {
        this.imageId = imageId;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
