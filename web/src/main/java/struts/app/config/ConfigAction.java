package struts.app.config;

import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.income.IncomeProvider;
import com.compalsolutions.compal.member.service.FollowerService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.point.PointProvider;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.wallet.vo.PointWalletTrx;
import com.compalsolutions.compal.wallet.vo.WalletTrx;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.LanguageFrameworkService;
import com.compalsolutions.compal.init.AccessMenuSetup;
import com.compalsolutions.compal.init.WalletSetup;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.wallet.WalletInfoCacheUtil;
import com.compalsolutions.compal.wallet.service.WalletService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "config"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", Global.JsonInclude.Default})})
public class ConfigAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(ConfigAction.class);

    private WalletService walletService;

    private PointProvider pointProvider;

    private IncomeProvider incomeProvider;

    private MemberService memberService;

    private boolean canGenWalletConfig;

    private List<OptionBean> allMonthList = new ArrayList<>();
    private List<OptionBean> allYearList = new ArrayList<>();

    private String incomeMonth;
    private String incomeYear;

    public ConfigAction() {
        walletService = Application.lookupBean(WalletService.class);
        pointProvider = Application.lookupBean(PointProvider.BEAN_NAME, PointProvider.class);
        incomeProvider = Application.lookupBean(IncomeProvider.BEAN_NAME, IncomeProvider.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allMonthList = optionBeanUtil.getMonthWithOptionAll();
        allMonthList.remove(0);
        allYearList = optionBeanUtil.getYearWithOptionAll();
        allYearList.remove(0);
    }

    @Override
    public String execute() throws Exception {
        initialize();
        canGenWalletConfig = walletService.getDefaultWalletConfig() == null;

        return INPUT;
    }

    @Action("/configGenerateMenu")
    public String generateMenu() {
        try {
            new AccessMenuSetup().execute();
            successMessage = "Generate menu successfully";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configGenerateWallet")
    public String generateWallet() {
        try {
            if (walletService.getDefaultWalletConfig() != null) {
                throw new Exception("WalletConfig is exists!!");
            }

            new WalletSetup().execute();

            successMessage = "Generate wallet config successfully";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configGenerateWalletBalance")
    public String generateWalletBalance() {
        try {
            walletService.doGenerateWalletBalanceForAllMembers();

            successMessage = "Generate wallet balances successfully";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configResetWalletTypeDesc")
    public String resetWalletTypeDesc() {
        try {
            LanguageFrameworkService languageFrameworkService = Application.lookupBean(LanguageFrameworkService.class);
            languageFrameworkService.resetLanguageCache();
            WalletInfoCacheUtil.clearCache();

            successMessage = "Reset Wallet Type description successfully";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configRunMigrateWallet81ToPointWallet")
    public String runMigrateWallet81ToPointWallet() {
        PointService pointService = Application.lookupBean(PointService.BEAN_NAME, PointService.class);
        List<WalletTrx> walletTrxList = walletService.findWalletTrxsByTrxTypeAndWalletType(Global.WalletType.WALLET_81,
                Global.WalletTrxType.LIVE_STREAM_GIFT_REWARD);
        try {
            for (WalletTrx walletTrx : walletTrxList) {
                PointWalletTrx pointWalletTrx = new PointWalletTrx(true);
                pointWalletTrx.setInAmt(walletTrx.getInAmt());
                pointWalletTrx.setOutAmt(BigDecimal.ZERO);
                pointWalletTrx.setOwnerId(walletTrx.getOwnerId());
                pointWalletTrx.setOwnerType(walletTrx.getOwnerType());
                pointWalletTrx.setTrxDatetime(walletTrx.getTrxDatetime());
                pointWalletTrx.setTrxType(Global.WalletTrxType.LIVE_STREAM_GIFT_REWARD);
                pointWalletTrx.setWalletRefId(walletTrx.getTrxId());
                pointWalletTrx.setWalletType(Global.WalletType.WALLET_81);
                pointWalletTrx.setTrxDesc(walletTrx.getTrxDesc());
                pointWalletTrx.setTrxDescCn(walletTrx.getTrxDescCn());
                pointService.doSavePointWalletTrx(pointWalletTrx);
            }
            walletService.doDeleteWalletTrx(walletTrxList);
            successMessage = "Run masternode wallet 81 to point wallet";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configRunFreeCoinExistingUser")
    public String runFreeCoinExistingUser() {
        MemberService memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        List<Member> members = memberService.findAllMembers();
        int count = 0;
//        findWalletTrxsByOwnerIdAndOwnerTypeAndWalleTypeAndTrxType
        try {
            for (Member member : members) {
                if (member.getUserRole().equalsIgnoreCase(Global.UserType.MEMBER)) {
                    count++;
                    memberService.doProcessFreeWhaleCoinNewUser(member.getMemberId());
                }
            }
            successMessage = "Run Free Coin success: " + count;
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configRunResetExpSuccess")
    public String runResetExpSuccess() {
        FollowerService followerService = Application.lookupBean(FollowerService.BEAN_NAME, FollowerService.class);
        try {
//            followerService.doResetExperienceForExp();
            successMessage = "Run Reset Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configRunResetPointSuccess")
    public String runResetPointSuccess() {
        try {
            pointProvider.getListOfOwnerIdsFromPointBalance();
//            followerService.doResetExperienceForExp();
            successMessage = "Run Reset Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configRunResetPointReportSuccess")
    public String runResetPointReportSuccess() {
        try {
            pointProvider.getListOfOwnerIdsFromPointTrxForReport();
//            followerService.doResetExperienceForExp();
            successMessage = "Run Reset Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configRunResetIncomeReportSuccess")
    public String runResetIncomeReportSuccess() {
        try {
            incomeProvider.doConfigReprocessVjIncome(incomeMonth,incomeYear);
            successMessage = "Run Reset Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/configGenerateAgoraUid")
    public String generateAgoraUid() {
        try {
            memberService.doGenerateAgoraUidForAllUser();
            successMessage = "Generate Agora Uid Success";
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------


    public List<OptionBean> getAllMonthList() {
        return allMonthList;
    }

    public void setAllMonthList(List<OptionBean> allMonthList) {
        this.allMonthList = allMonthList;
    }

    public String getIncomeMonth() {
        return incomeMonth;
    }

    public void setIncomeMonth(String incomeMonth) {
        this.incomeMonth = incomeMonth;
    }

    public boolean isCanGenWalletConfig() {
        return canGenWalletConfig;
    }

    public void setCanGenWalletConfig(boolean canGenWalletConfig) {
        this.canGenWalletConfig = canGenWalletConfig;
    }

    public List<OptionBean> getAllYearList() {
        return allYearList;
    }

    public void setAllYearList(List<OptionBean> allYearList) {
        this.allYearList = allYearList;
    }

    public String getIncomeYear() {
        return incomeYear;
    }

    public void setIncomeYear(String incomeYear) {
        this.incomeYear = incomeYear;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
