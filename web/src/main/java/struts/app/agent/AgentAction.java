package struts.app.agent;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.currency.vo.Currency;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.vo.RemoteAgentUser;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.ADD, location = "agentAdd"), //
        @Result(name = BaseAction.EDIT, location = "agentEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}" }), //
        @Result(name = BaseAction.INPUT, location = "agentAdd"), //
        @Result(name = BaseAction.LIST, location = "agentList"), //
        @Result(name = BaseAction.ADD_DETAIL, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.REMOVE_DETAIL, type = BaseAction.ResultType.JSON), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class AgentAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String DUMMY_PASSWORD = "_dummy_";

    private AgentService agentService;
    private CurrencyService currencyService;

    private Agent agent = new Agent(true);
    private RemoteAgentUser remoteAgentUser = new RemoteAgentUser(true);
    private String password;
    private boolean enableRemote = true;
    private String remotePassword;
    private List<Currency> currencies = new ArrayList<Currency>();

    private List<OptionBean> statusList = new ArrayList<OptionBean>();
    private List<OptionBean> allStatusList = new ArrayList<OptionBean>();

    public AgentAction() {
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_AGENT)
    @Action("/agentAdd")
    @Access(accessCode = AP.AGENT, createMode = true)
    public String add() {
        init();

        return ADD;
    }

    protected void init() {
        currencies = currencyService.findAllCurrencies();
        statusList.add(new OptionBean(Global.Status.ACTIVE, getText("statActive")));
        statusList.add(new OptionBean(Global.Status.INACTIVE, getText("statInactive")));

        allStatusList.add(new OptionBean("", getText("all")));
        allStatusList.addAll(statusList);
    }

    @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/agentGet")
    public String get() {
        try {
            agent = agentService.getAgent(agent.getAgentId());
            if (agent == null) {
                addActionError("Invalid agent");
            }

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Access(accessCode = AP.AGENT, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)
    @Action("/agentResetPassword")
    public String resetPassword() {
        try {
            agentService.doResetSuperAgentUserPasswordByAgentId(agent.getAgentId(), password);

            successMessage = getText("successMessage.ResetPasswordAction");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_AGENT)
    @Action("/agentSave")
    @Access(accessCode = AP.AGENT, createMode = true)
    public String save() throws Exception {
        User loginUser = getLoginUser();

        try {
            VoUtil.toTrimUpperCaseProperties(agent, remoteAgentUser);

            remoteAgentUser.setPassword(remotePassword);
            agentService.doCreateAgent(getLocale(), agent, password, loginUser.getCompId(), enableRemote, remoteAgentUser);

            successMessage = getText("successMessage.AgentAction.save");
            successMenuKey = MP.FUNC_AD_AGENT;

            return SUCCESS;
        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return INPUT;
        }
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_AGENT)
    @Action("/agentEdit")
    @Access(accessCode = AP.AGENT, updateMode = true)
    public String edit() {
        init();
        agent = agentService.getAgent(agent.getAgentId());

        if (agent == null) {
            addActionError(getText("invalidAgent"));
            return EDIT;
        }

        remoteAgentUser = agentService.findRemoteAgentUserByAgentId(agent.getAgentId());
        if (remoteAgentUser == null) {
            enableRemote = false;
            remoteAgentUser = new RemoteAgentUser(true); // better don't have null object for Struts2
        } else {
            enableRemote = true;
            remotePassword = DUMMY_PASSWORD;
        }

        return EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_AGENT)
    @Action("/agentUpdate")
    @Access(accessCode = AP.AGENT, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(agent, remoteAgentUser);

            if (!DUMMY_PASSWORD.equals(remotePassword)) {
                remoteAgentUser.setPassword(remotePassword);
            }

            agentService.updateAgent(getLocale(), agent, enableRemote, remoteAgentUser);
            successMessage = getText("successMessage.AgentAction.update");
            successMenuKey = MP.FUNC_AD_AGENT;
        } catch (Exception ex) {
            init();
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_AGENT)
    @Action(value = "/agentList")
    @Access(accessCode = AP.AGENT, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        init();

        return LIST;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public boolean isEnableRemote() {
        return enableRemote;
    }

    public void setEnableRemote(boolean enableRemote) {
        this.enableRemote = enableRemote;
    }

    public RemoteAgentUser getRemoteAgentUser() {
        return remoteAgentUser;
    }

    public void setRemoteAgentUser(RemoteAgentUser remoteAgentUser) {
        this.remoteAgentUser = remoteAgentUser;
    }

    public String getRemotePassword() {
        return remotePassword;
    }

    public void setRemotePassword(String remotePassword) {
        this.remotePassword = remotePassword;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
