package struts.app.master;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.currency.service.CurrencyService;
import com.compalsolutions.compal.currency.vo.CurrencyExchange;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = { //
        @Result(name = BaseAction.LIST, location = "exchangeRateList"), //
        @Result(name = BaseAction.INPUT, location = "exchangeRateList"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties", ExchangeRateAction.JSON_INCLUDE_PROPERTIES }) //
})
public class ExchangeRateAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", " //
            + "currencyExchange, "//
            + "currencyExchange.currencyCodeFrom, " //
            + "currencyExchange.currencyCodeTo, " //
            + "currencyExchange.rate";

    private CurrencyService currencyService;

    private CurrencyExchange currencyExchange = new CurrencyExchange(true);

    private List<CurrencyExchange> currencyExchanges = new ArrayList<CurrencyExchange>();

    public ExchangeRateAction() {
        currencyService = Application.lookupBean(CurrencyService.BEAN_NAME, CurrencyService.class);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_EXCHANGE_RATE)
    @Action(value = "/exchangeRateList")
    @Access(accessCode = AP.EXCHANGE_RATE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        currencyExchanges = currencyService.findLatestCurrencyExchanges();
        return LIST;
    }

    @Action(value = "/exchangeRateGetLatest")
    @Access(accessCode = AP.EXCHANGE_RATE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String get() {
        VoUtil.toTrimUpperCaseProperties(currencyExchange);
        currencyExchange = currencyService.findLatestCurrencyExchange(currencyExchange.getCurrencyCodeFrom(), currencyExchange.getCurrencyCodeTo());
        return JSON;
    }

    @Action(value = "/exchangeRateRemove")
    @Access(accessCode = AP.EXCHANGE_RATE, adminMode = true, deleteMode = true)
    public String remove() {
        VoUtil.toTrimUpperCaseProperties(currencyExchange);
        try {
            currencyService.deleteCurrencyExchangeByCurrencyCodeFromAndCurrencyCodeTo(currencyExchange.getCurrencyCodeFrom(),
                    currencyExchange.getCurrencyCodeTo());
            successMessage = getText("successMessage.ExchangeRateAction.remove");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action(value = "/exchangeRateSaveUpdate")
    @Access(accessCode = AP.EXCHANGE_RATE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String saveOrUpdate() {
        VoUtil.toTrimUpperCaseProperties(currencyExchange);
        try {
            currencyService.saveCurrencyExchange(currencyExchange);
            successMessage = getText("successMessage.ExchangeRateAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public List<CurrencyExchange> getCurrencyExchanges() {
        return currencyExchanges;
    }

    public void setCurrencyExchanges(List<CurrencyExchange> currencyExchanges) {
        this.currencyExchanges = currencyExchanges;
    }

    public CurrencyExchange getCurrencyExchange() {
        return currencyExchange;
    }

    public void setCurrencyExchange(CurrencyExchange currencyExchange) {
        this.currencyExchange = currencyExchange;
    }

    // ---------------- GETTER & SETTER (ENG) -------------
}
