package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                GuildListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class GuildListDatagridAction extends BaseDatagridAction<BroadcastGuild> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", "
            + "rows\\[\\d+\\]\\.name, "
            + "rows\\[\\d+\\]\\.president, "
            + "rows\\[\\d+\\]\\.displayId";

    private BroadcastService broadcastService;

    public GuildListDatagridAction() {
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
    }

    @Action(value = "/guildListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.GUILD, readMode = true), //
            @Access(accessCode = AP.GUILD, readMode = true) //
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        broadcastService.findGuildsForListing(getDatagridModel(),  null);

        return JSON;
    }
}
