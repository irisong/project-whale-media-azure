package struts.app.member;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.jeasyui.BaseDatagridAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = { "includeProperties",
                MemberListDatagridAction.JSON_INCLUDE_PROPERTIES }) })
public class MemberListDatagridAction extends BaseDatagridAction<Member> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datagrid + ", "
            + "rows\\[\\d+\\]\\.memberId, "
            + "rows\\[\\d+\\]\\.memberCode, "
            + "rows\\[\\d+\\]\\.identityType, "
            + "rows\\[\\d+\\]\\.identityNo, "
            + "rows\\[\\d+\\]\\.status, "
            + "rows\\[\\d+\\]\\.joinDate, "
            + "rows\\[\\d+\\]\\.memberDetail\\.profileName, "
            + "rows\\[\\d+\\]\\.memberDetail\\.whaleliveId, "
            + "rows\\[\\d+\\]\\.memberDetail\\.email";

    private MemberService memberService;

    public MemberListDatagridAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/memberListDatagrid")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, readMode = true), //
            @Access(accessCode = AP.ROLE_AGENT, readMode = true) //
    })
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        memberService.findMembersForListing(getDatagridModel(), agentId, null, null, null, null,null);

        return JSON;
    }
}
