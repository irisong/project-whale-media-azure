package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                GuildListDatatablesAction.JSON_INCLUDE_PROPERTIES})})
public class GuildListDatatablesAction extends BaseSecureDatatablesAction<BroadcastGuild> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.id, "
            + "data\\[\\d+\\]\\.name, "
            + "data\\[\\d+\\]\\.president, "
            + "data\\[\\d+\\]\\.description, "
            + "data\\[\\d+\\]\\.fileUrl";

    private BroadcastService broadcastService;

    @ToTrim
    @ToUpperCase
    private String name;

    public GuildListDatatablesAction() {
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
    }

    @Action(value = "/guildListDatatables")
    @Access(accessCode = AP.GUILD, readMode = true)
    @Override
    public String execute() throws Exception {
        //  VoUtil.toTrimUpperCaseProperties(this);

        broadcastService.findGuildsForListing(getDatagridModel(), name);
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    // ---------------- GETTER & SETTER (END) -------------
}