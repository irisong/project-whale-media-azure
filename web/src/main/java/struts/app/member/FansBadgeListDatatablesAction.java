package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.FansBadgeService;
import com.compalsolutions.compal.member.vo.FansBadge;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                FansBadgeListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class FansBadgeListDatatablesAction extends BaseSecureDatatablesAction<FansBadge> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.fansBadgeId, "
            + "data\\[\\d+\\]\\.createDate, "
            + "data\\[\\d+\\]\\.member\\.memberCode, "
            + "data\\[\\d+\\]\\.fansBadgeName, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.activeDatetime, "
            + "data\\[\\d+\\]\\.rejectDatetime, "
            + "data\\[\\d+\\]\\.remark ";

    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String status;

    private Date createDateFrom;
    private Date createDateTo;

    private FansBadgeService fansBadgeService;

    public FansBadgeListDatatablesAction() {
        fansBadgeService = Application.lookupBean(FansBadgeService.class);
    }

    @Action(value = "/fansBadgeListDatatables")
    @Access(accessCode = AP.FANS_BADGE, readMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        fansBadgeService.findFansBadgesForListing(getDatagridModel(), memberCode, createDateFrom, createDateTo, status);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateDateFrom() {
        return createDateFrom;
    }

    public void setCreateDateFrom(Date createDateFrom) {
        this.createDateFrom = createDateFrom;
    }

    public Date getCreateDateTo() {
        return createDateTo;
    }

    public void setCreateDateTo(Date createDateTo) {
        this.createDateTo = createDateTo;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
