package struts.app.member;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.function.user.vo.UserRole;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.io.File;
import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "guildList"),
        @Result(name = BaseAction.ADD, location = "guildAdd"),
        @Result(name = BaseAction.ADD_DETAIL, location = "guildAdd"),
        @Result(name = BaseAction.EDIT, location = "guildEdit"),
        @Result(name = "manage", location = "guildManage"),
        @Result(name = BaseAction.SHOW, location = "guildShow"),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "templateMessage", "namespace", "/app",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}"}),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON)})

public class GuildAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Default + ", "
            + "memberId, "
            + "id ";

    private static final String SESSION_GUILD_LIST = "guildList";

    private BroadcastService broadcastService;

    private MemberService memberService;

    private AgentService agentService;

    private UserService userService;

    private BroadcastGuild broadcastGuild = new BroadcastGuild(true);
    private Agent agent = new Agent(true);
    private User user = new User(true);

    @ToTrim
    private String name;
    private String desc;

    private String id;

    @ToTrim
    private String memberId;

    @ToTrim
    private String guildId;

    @ToTrim
    private String memberRank;

    @ToTrim
    private String memberCode;

    @ToTrim
    @ToUpperCase
    private String status;

    @ToTrim
    private String startHalfMonth;

    @ToTrim
    private String endHalfMonth;

    @ToTrim
    private Date dateFrom;

    @ToTrim
    private Date dateTo;

    private boolean agentExisted;

    private String username;
    private String password;
    private String email;
    private String phone;

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    private boolean clear;

    private List<OptionBean> allGuildRankList = new ArrayList<>();

    public GuildAction() {
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
    }

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allGuildRankList = optionBeanUtil.getMemberRankWithPleaseSelect();

    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_GUILD)
    @Action(value = "/guildList")
    @Access(accessCode = AP.GUILD, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_GUILD_LIST);
        } else if (session.containsKey(SESSION_GUILD_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_GUILD_LIST);
        } else {
            session.put(SESSION_GUILD_LIST, objectMap);
        }

        Locale locale = getLocale();
        if (isSubmitData()) {
            objectMap.put("name", name);

        } else {
            name = (String) objectMap.get("name");
        }
        return LIST;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_GUILD)
    @Action(value = "/guildAdd")
    @Access(accessCode = AP.GUILD, adminMode = true, createMode = true)
    public String add() {
        return ADD;
    }

    @Action(value = "/guildSave")
    @Access(accessCode = AP.GUILD, adminMode = true, createMode = true)
    public String save() {
        try {
            Locale locale = getLocale();
            boolean isAvailable = userService.checkUserAvailability(locale,username);

            if(!isAvailable)
            {
                throw new ValidatorException("Username not available");
            }

            agent.setAgentName(broadcastGuild.getPresident());
            agent.setEmail(email);
            agent.setPhoneNo(phone);
            agent.setDefaultCurrencyCode(Global.WalletType.MYR);
            agent.setStatus(Global.Status.ACTIVE);

            user.setUsername(username);
            user.setPassword(password);
            user.setPassword2(password);
            user.setStatus(Global.Status.ACTIVE);
            user.setUserType(Global.UserType.GM);


            VoUtil.toTrimUpperCaseProperties(this, broadcastGuild);
            broadcastGuild.setFileUpload(fileUpload);
            broadcastGuild.setFilename(fileUploadFileName);
            broadcastGuild.setContentType(fileUploadContentType);
            broadcastGuild.setFileSize(fileUpload.length());
            broadcastService.saveBroadcastGuild(broadcastGuild,agent,user);

            id = broadcastGuild.getId();

            successMessage = getText("successMessage.GuildAction.save");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/guildMemberDelete")
    @Access(accessCode = AP.GUILD, createMode = true, adminMode = true)
    public String deleteMember() {
        try {
            Member member = memberService.getMember(memberId);
            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberWithoutStatus(member.getMemberId(),id);
            broadcastService.doAddDeleteGuildMember(memberId, broadcastGuildMember.getGuildId(), broadcastGuildMember.getBroadcastConfigId(), false,null,null,null,null);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/guildMemberAdd")
    @Access(accessCode = AP.GUILD, createMode = true, adminMode = true)
    public String addMember() {
        try {
            Member member = memberService.findMemberByMemberCode(memberCode);
            Boolean start = false;
            Boolean end = false;
            if (member == null) {
                throw new ValidatorException("Member not exist");
            }

            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberApproved(member.getMemberId());

            if (broadcastGuildMember != null) {
                throw new ValidatorException("Member already in other guild");

//                if (id.equalsIgnoreCase(member.getBroadcastGuildId())) {
//                    throw new ValidatorException("Member already in this guild");
//                }
//                if (StringUtils.isNotBlank(member.getBroadcastGuildId())) {
//                    throw new ValidatorException("Member already in other guild");
//                }

            }
//            memberService.doUpdateMemberGuild(member.getMemberId(), id, memberRank);
            if(startHalfMonth!=null)
            {
                start = true;
            }
            if(endHalfMonth!=null)
            {
                end = true;
            }

            broadcastService.doAddDeleteGuildMember(member.getMemberId(), id, memberRank, true,dateFrom,dateTo,start,end);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/guildMemberApprove")
    @Access(accessCode = AP.GUILD, createMode = true, adminMode = true)
    public String approveMember() {
        try {
            Member member = memberService.getMember(memberId);
            Boolean start = false;
            Boolean end = false;
            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberWithoutStatus(member.getMemberId(),id);

            if(startHalfMonth!=null)
            {
                start = true;
            }
            if(endHalfMonth!=null)
            {
                end = true;
            }
            if(memberRank.equalsIgnoreCase(""))
            {
                throw new ValidatorException("Please select rank");
            }

            if(dateFrom.getTime()>dateTo.getTime())
            {
                throw new ValidatorException("Please insert serve date properly");
            }

            broadcastService.doApproveMember(memberId, broadcastGuildMember.getGuildId(),memberRank,dateFrom,dateTo,start,end);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @Action("/guildAgentAdd")
    @Access(accessCode = AP.GUILD, createMode = true, adminMode = true)
    public String addAgentGuild() {
        try {

            Locale locale = getLocale();
            boolean isAvailable = userService.checkUserAvailability(locale,username);

            if(!isAvailable)
            {
                throw new ValidatorException("Username not available");
            }

            BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(id);

            agent.setAgentName(broadcastGuild.getPresident());
            agent.setEmail(email);
            agent.setPhoneNo(phone);
            agent.setDefaultCurrencyCode(Global.WalletType.MYR);
            agent.setStatus(Global.Status.ACTIVE);

            user.setUsername(username);
            user.setPassword(password);
            user.setPassword2(password);
            user.setStatus(Global.Status.ACTIVE);
            user.setUserType(Global.UserType.GM);

            broadcastService.doAddAgentBroadcastGuild(broadcastGuild,agent,user);



            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }


    @Action("/guildDelete")
    @Access(accessCode = AP.GUILD, createMode = true, adminMode = true)
    public String deleteGuild() {
        try {
            broadcastService.doDeleteGuild(id);
            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_GUILD)
    @Action(value = "/guildEdit")
    @Access(accessCode = AP.GUILD, adminMode = true, updateMode = true)
    public String edit() {
        GuildFileUploadConfiguration config = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);

        try {
            broadcastGuild = broadcastService.getBroadcastGuild(id);
            if (broadcastGuild == null) {
                addActionError(getText("invalidGuild"));
                return list();
            }
            String fileUrl = broadcastGuild.getFileUrlWithParentPath(config.getFullGuildParentFileUrl());
            broadcastGuild.setFileUrl(fileUrl);

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_GUILD)
    @Action(value = "/guildManage")
    @Access(accessCode = AP.GUILD, adminMode = true, updateMode = true)
    public String manage() {
        initialize();
        GuildFileUploadConfiguration config = Application.lookupBean(GuildFileUploadConfiguration.BEAN_NAME, GuildFileUploadConfiguration.class);

        try {
            broadcastGuild = broadcastService.getBroadcastGuild(id);

            if(!broadcastGuild.getAgentId().equalsIgnoreCase(""))
            {
                agentExisted = true;
                AgentUser agent = agentService.findAgentUserByAgentId(broadcastGuild.getAgentId());
                broadcastGuild.setAgentUserId(agent.getUsername());
            }

            if (broadcastGuild == null) {
                addActionError(getText("invalidGuild"));
                return list();
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return "manage";
    }

    @Action(value = "/guildUpdate")
    @Access(accessCode = AP.GUILD, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this, broadcastGuild);
            BroadcastGuild newBroadcastGuild = new BroadcastGuild();

            if (fileUpload != null) {
                newBroadcastGuild.setFileUpload(fileUpload);
                newBroadcastGuild.setFilename(fileUploadFileName);
                newBroadcastGuild.setContentType(fileUploadContentType);
                newBroadcastGuild.setFileSize(fileUpload.length());
            }
            newBroadcastGuild.setId(broadcastGuild.getId());
            newBroadcastGuild.setName(broadcastGuild.getName());
            newBroadcastGuild.setStatus(broadcastGuild.getStatus());
            newBroadcastGuild.setDescription(broadcastGuild.getDescription());
            newBroadcastGuild.setPresident(broadcastGuild.getPresident());
            broadcastService.doUpdateGuild(getLocale(), newBroadcastGuild);
            successMessage = getText("successMessage.GuildAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }


    @Action("/guildMemberEdit")
    @Access(accessCode = AP.GUILD, createMode = true, adminMode = true)
    public String editMember() {
        try {
            Member member = memberService.getMember(memberId);
            Boolean start = false;
            Boolean end = false;
            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberWithoutStatus(member.getMemberId(),id);
            BroadcastConfig broadcastConfig = broadcastService.getBroadcast(broadcastGuildMember.getBroadcastConfigId()); //get broadcastConfig detail
            String oldRankName = broadcastConfig.getRankName();
            if (startHalfMonth != null) {
                start = true;
            }
            if (endHalfMonth != null) {
                end = true;
            }
            if (StringUtils.isBlank(memberRank)) {
                throw new ValidatorException("Please select rank");
            }

            broadcastService.doUpdateGuildMember(memberId, broadcastGuildMember.getGuildId(), memberRank, dateFrom, dateTo, start, end);

            //send notification email
            broadcastConfig = broadcastService.getBroadcast(broadcastGuildMember.getBroadcastConfigId()); //get updated broadcastConfig detail
            BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(broadcastGuildMember.getGuildId());

            String title = ("Notification: Change guild member ranking");
            String body = (member.getMemberCode() + ": " + member.getMemberDetail().getProfileName() + "'s rank from guild[" + broadcastGuild.getName() + "] changed from  " + oldRankName + " to " + broadcastConfig.getRankName());
            memberService.doSendDifferentNotificationEmailType(Global.EmailType.ADJUST_RANK, broadcastGuildMember.getGuildId(), null, null, title, body);


            successMessage = getText("processSuccessfully");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public BroadcastGuild getBroadcastGuild() {
        return broadcastGuild;
    }

    public void setBroadcastGuild(BroadcastGuild broadcastGuild) {
        this.broadcastGuild = broadcastGuild;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public String getMemberRank() {
        return memberRank;
    }

    public void setMemberRank(String memberRank) {
        this.memberRank = memberRank;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public List<OptionBean> getAllGuildRankList() {
        return allGuildRankList;
    }

    public void setAllGuildRankList(List<OptionBean> allGuildRankList) {
        this.allGuildRankList = allGuildRankList;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    public String getStartHalfMonth() {
        return startHalfMonth;
    }

    public void setStartHalfMonth(String startHalfMonth) {
        this.startHalfMonth = startHalfMonth;
    }

    public String getEndHalfMonth() {
        return endHalfMonth;
    }

    public void setEndHalfMonth(String endHalfMonth) {
        this.endHalfMonth = endHalfMonth;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isAgentExisted() {
        return agentExisted;
    }

    public void setAgentExisted(boolean agentExisted) {
        this.agentExisted = agentExisted;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
