package struts.app.member;

import java.util.Date;
import java.util.List;

import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastConfig;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                MemberListDatatablesAction.JSON_INCLUDE_PROPERTIES}),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.STREAM, params = {"contentType", "application/vnd.ms-excel",
                "inputName", "excelStream", "contentDisposition", "attachment;filename=${excelFileName}", "bufferSize", "2048"})
})
public class MemberListDatatablesAction extends BaseSecureDatatablesAction<Member> {
    private static final long serialVersionUID = 1L;

//    private static final String MEMBER_EXCEL_FILE_HEADER = "Registration Date;Member ID;" +
//            "Full Name; IC./Passport No.;VIP;" +
//            "VIP Star;Masternode;Sponsor ID;" +
//            "Password;Transaction Password;" +
//            "Lock Transfer;LockWithdrawal";
//
//    public static final String SHEET_NAME = "All Member";

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.memberId, "
            + "data\\[\\d+\\]\\.memberCode, "
            + "data\\[\\d+\\]\\.memberDetail\\.profileName, "
            + "data\\[\\d+\\]\\.memberDetail\\.whaleliveId, "
            + "data\\[\\d+\\]\\.memberDetail\\.liveChannelId, "
            + "data\\[\\d+\\]\\.fullName, "
            + "data\\[\\d+\\]\\.identityNo, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.joinDate, "
            + "data\\[\\d+\\]\\.activeDatetime, "
            + "data\\[\\d+\\]\\.guildStatus, "
            + "data\\[\\d+\\]\\.liveStreamStatus, "
            + "data\\[\\d+\\]\\.liveStreamTypes, "
            + "data\\[\\d+\\]\\.rankName, "
            + "data\\[\\d+\\]\\.serveStartDate, "
            + "data\\[\\d+\\]\\.serveEndDate, "
            + "data\\[\\d+\\]\\.liveStreamPrivate ";

    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String whaleliveId;

    @ToUpperCase
    @ToTrim
    private String memberStatus;

    private Date joinDateFrom;
    private Date joinDateTo;

    private MemberService memberService;
    private LiveStreamService liveStreamService;
    private BroadcastService broadcastService;

    @ToUpperCase
    @ToTrim
    private String guildid;

//    private InputStream excelStream;
//    private String excelFileName;

    public MemberListDatatablesAction() {
        memberService = Application.lookupBean(MemberService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        broadcastService = Application.lookupBean(BroadcastService.class);

    }

    @Action(value = "/memberListDatatables")
    @Accesses(access = {@Access(accessCode = AP.MEMBER, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        memberService.findMembersForListing(getDatagridModel(), agentId, memberCode, joinDateFrom, joinDateTo, memberStatus,whaleliveId);

        List<Member> members = getDatagridModel().getRecords();
        for (Member member : members) {
            member.setLiveStreamTypes(StringUtils.replace(member.getLiveStreamTypes(), "|", ", "));

            LiveStreamChannel liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(member.getMemberId(), null); //any status
            if (liveStreamChannel != null) {
                member.setLiveStreamStatus(liveStreamChannel.getStatus());
                if (!liveStreamChannel.getStatus().equalsIgnoreCase(Global.Status.INACTIVE)) {
                    member.getMemberDetail().setLiveChannelId(liveStreamChannel.getChannelId());
                }
            } else {
                member.setLiveStreamStatus(Global.Status.INACTIVE);
            }
        }

        return JSON;
    }

    @Action(value = "/guildMemberListDatatables")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String execute2() throws Exception {

        String agentId = null;
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        memberService.findGuildMembersForListing(getDatagridModel(), guildid);

        List<Member> members = getDatagridModel().getRecords();
        for (Member member : members) {

            BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberWithoutStatus(member.getMemberId(),guildid);
            if (broadcastGuildMember != null && StringUtils.isNotBlank(broadcastGuildMember.getBroadcastConfigId())) {
                BroadcastConfig broadcastConfig = broadcastService.getBroadcast(broadcastGuildMember.getBroadcastConfigId());
                member.setJoinDate(broadcastGuildMember.getJoinedDate());
                member.setRankName(broadcastConfig.getRankName());
                member.setGuildStatus(broadcastGuildMember.getStatus());
                member.setServeStartDate(broadcastGuildMember.getServedStartDate());
                member.setServeEndDate(broadcastGuildMember.getServedEndDate());
            } else {
                member.setRankName("");
            }
        }

        return JSON;
    }


//    @Action("/memberExportExcel")
//    @Access(accessCode = AP.MEMBER, createMode = true, readMode = true, deleteMode = true, updateMode = true)
//    public String specialEventExportExcel() {
//        try {
//            List<Member> members = memberService.findAllMembers();
//            this.initExcelFile();
//            this.exportExcel(members);
//        } catch (Exception e) {
//            addActionError(e.getMessage());
//        }
//
//        return SUCCESS;
//    }

//    private void initExcelFile() {
//        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
//        this.setExcelFileName("Member_" + df.format(new Date()) + ".xls");
//    }

//    public void exportExcel(List<Member> memberList) {
//        try {
//            ExcelGeneratorUtil excel = new ExcelGeneratorUtil(MEMBER_EXCEL_FILE_HEADER);
//            /**
//             *used HSSFWorkbook for Excel files in .xls format.
//             **/
//            excel.createHeader(SHEET_NAME);
//            HSSFRow row;
//            HSSFWorkbook wb = excel.getWb();
////          defined sheet name
//            HSSFSheet sheet = excel.getSheet();
//            int i = 1;
//            for (Member member : memberList) {
//                row = sheet.createRow(i);
//                row.createCell(0).setCellValue(member.getJoinDate().toString());
//                row.createCell(1).setCellValue(member.getMemberCode());
//                row.createCell(2).setCellValue(member.getFullName());
//                row.createCell(3).setCellValue(member.getIdentityNo());
//                i++;
//            }
//            //step 7, stored document in streaming
//            ByteArrayOutputStream os = new ByteArrayOutputStream();
//            wb.write(os);
//            byte[] fileContent = os.toByteArray();
//            ByteArrayInputStream is = new ByteArrayInputStream(fileContent);
//            excelStream = is;
//        } catch (
//                Exception e) {
//            e.printStackTrace();
//        }
//    }

    // ---------------- GETTER & SETTER (START) -------------


    public String getGuildid() {
        return guildid;
    }

    public void setGuildid(String guildid) {
        this.guildid = guildid;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Date getJoinDateFrom() {
        return joinDateFrom;
    }

    public void setJoinDateFrom(Date joinDateFrom) {
        this.joinDateFrom = joinDateFrom;
    }

    public Date getJoinDateTo() {
        return joinDateTo;
    }

    public void setJoinDateTo(Date joinDateTo) {
        this.joinDateTo = joinDateTo;
    }

    public String getMemberStatus() {
        return memberStatus;
    }

    public void setMemberStatus(String memberStatus) {
        this.memberStatus = memberStatus;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    //    public InputStream getExcelStream() {
//        return excelStream;
//    }

//    public void setExcelStream(InputStream excelStream) {
//        this.excelStream = excelStream;
//    }
//
//    public String getExcelFileName() {
//        return excelFileName;
//    }
//
//    public void setExcelFileName(String excelFileName) {
//        this.excelFileName = excelFileName;
//    }

    // ---------------- GETTER & SETTER (END) -------------
}
