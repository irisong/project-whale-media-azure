package struts.app.member;
import java.io.InputStream;
import java.util.Date;

import com.compalsolutions.compal.datagrid.SqlDatagridModel;
import com.compalsolutions.compal.identity.service.IdentityService;
import com.compalsolutions.compal.identity.vo.Identity;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.vo.ORWrapper;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
       // @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                IdentityListDatatablesAction.JSON_INCLUDE_PROPERTIES})})


public class IdentityListDatatablesAction extends BaseSecureDatatablesAction<Member> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", " //
            + "data\\[\\d+\\]\\.memberId, "//
            + "data\\[\\d+\\]\\.memberDetail\\.whaleliveId, "
            + "data\\[\\d+\\]\\.memberCode, " //
            + "data\\[\\d+\\]\\.icFrontUrl, " //
            + "data\\[\\d+\\]\\.icBackUrl, " //
            + "data\\[\\d+\\]\\.fullName, " //
            + "data\\[\\d+\\]\\.identityNo, " //
            + "data\\[\\d+\\]\\.kycStatus, " //
            + "data\\[\\d+\\]\\.kycSubmitDate, " //
            + "data\\[\\d+\\]\\.kycRemark ";
//            + "data\\[\\d+\\]\\.icImageIds, "//
//            + "data\\[\\d+\\]\\.icImageIds\\[\\d+\\], "//
//            + "data\\[\\d+\\]\\.icImageUrls, "//
//            + "data\\[\\d+\\]\\.icImageUrls\\[\\d+\\]";

    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String status;

    @ToUpperCase
    @ToTrim
    private String whaleliveId;

    private Date dateFrom;
    private Date dateTo;

    private String memberId;

    private MemberService memberService;
    private IdentityService identityService;


    private InputStream excelStream;
    private String excelFileName;

    public IdentityListDatatablesAction() {
        identityService = Application.lookupBean(IdentityService.BEAN_NAME, IdentityService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);


        SqlDatagridModel<Member> model = new SqlDatagridModel<>(new ORWrapper(new Member(false), "m"));
        model.addJoinTable(new ORWrapper(new MemberDetail(false), "b"));
        model.addJoinTable(new ORWrapper(new MemberFile(false), "f"));
        setDatagridModel(model);
    }

    @Action(value = "/identityListDatatables")
    @Accesses(access = {@Access(accessCode = AP.IDENTITY, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true), //
            @Access(accessCode = AP.ROLE_MEMBER, adminMode = true)})
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

       // identityService.findIdentityForListing(getDatagridModel(), memberCode, status, dateFrom, dateTo);
        memberService.findMembersForKYCListing(getDatagridModel(), memberCode, status,whaleliveId,dateFrom, dateTo);

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public InputStream getExcelStream() {
        return excelStream;
    }

    public void setExcelStream(InputStream excelStream) {
        this.excelStream = excelStream;
    }

    public String getExcelFileName() {
        return excelFileName;
    }

    public void setExcelFileName(String excelFileName) {
        this.excelFileName = excelFileName;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}

