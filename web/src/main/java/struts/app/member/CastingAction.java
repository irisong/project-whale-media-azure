package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.CastingService;
import com.compalsolutions.compal.member.vo.Casting;

import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "castingList"),
        @Result(name = BaseAction.LIST, location = "castingList"),
        @Result(name = BaseAction.EDIT, location = "castingEdit"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "casting", "namespace", "/app/member/admin",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}"}),
})
public class CastingAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Log log = LogFactory.getLog(CastingAction.class);

    private static final String SESSION_CASTING = "casting";

    private CastingService castingService;

    private boolean clear;

    private Date dateFrom;
    private Date dateTo;
    private Casting casting;

    private String id;

    public CastingAction() {
        castingService = Application.lookupBean(CastingService.BEAN_NAME, CastingService.class);
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_CASTING})
    @Action(value = "/castingList")
    @Access(accessCode = AP.CASTING, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        return LIST;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_CASTING})
    @Action(value = "/castingEdit")
    @Access(accessCode = AP.CASTING, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String edit() {
        try {
            casting = castingService.getCastingByWhaleliveId(id);
            if (casting == null) {
                addActionError(getText("invalidCasting"));
                return list();
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }
        return EDIT;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public Casting getCasting() {
        return casting;
    }

    public void setCasting(Casting casting) {
        this.casting = casting;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
