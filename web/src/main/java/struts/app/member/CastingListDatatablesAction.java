package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Casting;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.datatables.BaseSecureDatatablesAction;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;
import java.util.List;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "jsonMessage"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties",
                CastingListDatatablesAction.JSON_INCLUDE_PROPERTIES})
})
public class CastingListDatatablesAction extends BaseSecureDatatablesAction<Casting> {
    private static final long serialVersionUID = 1L;

    public static final String JSON_INCLUDE_PROPERTIES = Global.JsonInclude.Datatables + ", "
            + "data\\[\\d+\\]\\.castId, "
            + "data\\[\\d+\\]\\.datetimeAdd, "
            + "data\\[\\d+\\]\\.whaleLiveId, "
            + "data\\[\\d+\\]\\.guildName, "
            + "data\\[\\d+\\]\\.whaleliveProfileName, "
            + "data\\[\\d+\\]\\.fullName, "
            + "data\\[\\d+\\]\\.gender, "
            + "data\\[\\d+\\]\\.age, "
            + "data\\[\\d+\\]\\.phoneNo, "
            + "data\\[\\d+\\]\\.email, "
            + "data\\[\\d+\\]\\.status, "
            + "data\\[\\d+\\]\\.url";

    private MemberService memberService;

    private BroadcastService broadcastService;

    @ToUpperCase
    @ToTrim
    private String whaleLiveId;

    @ToUpperCase
    @ToTrim
    private String guildId;

    private String agentId;

    private Date dateFrom;
    private Date dateTo;

    public CastingListDatatablesAction() {
        broadcastService = Application.lookupBean(BroadcastService.class);
        memberService = Application.lookupBean(MemberService.class);
    }

    @Action(value = "/castingDatatables")
    @Access(accessCode = AP.CASTING, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        memberService.findCastForListing(getDatagridModel(), whaleLiveId, dateFrom, dateTo);

        List<Casting> castings = getDatagridModel().getRecords();
        for (Casting casting : castings) {
            if(StringUtils.isNotBlank(casting.getGuildId())){
                BroadcastGuild broadcastGuild = broadcastService.getBroadcastGuild(casting.getGuildId());
                casting.setGuildName(broadcastGuild.getName());
            }else
            {
                casting.setGuildName("Global");
            }

            MemberDetail memberDetail = memberService.findMemberDetailByWhaleliveId(casting.getWhaleLiveId());
            casting.setWhaleliveProfileName(memberDetail.getProfileName());


        }

        return JSON;
    }

    @Action(value = "/guildCastingDatatables")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String execute2() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);


        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        BroadcastGuild broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

        memberService.findCastForListingWithGuildId(getDatagridModel(), broadcastGuild.getId());


        List<Casting> castings = getDatagridModel().getRecords();
        for (Casting casting : castings) {
            MemberDetail memberDetail = memberService.findMemberDetailByWhaleliveId(casting.getWhaleLiveId());
            casting.setWhaleliveProfileName(memberDetail.getProfileName());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getGuildId() {
        return guildId;
    }

    public void setGuildId(String guildId) {
        this.guildId = guildId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
