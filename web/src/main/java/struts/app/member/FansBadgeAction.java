package struts.app.member;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.FansBadgeService;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.*;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "fansBadgeList"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON)
})
public class FansBadgeAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_FANS_BADGE_LIST = "fansBadgeList";

    protected FansBadgeService fansBadgeService;

    private List<OptionBean> allStatusList = new ArrayList<>();
    private List<OptionBean> statusList = new ArrayList<>();

    @ToTrim
    private String fansBadgeId;

    @ToTrim
    private String rejectReason;

    private boolean clear;

    /********************************
     * property for listing - START
     *******************************/
    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String status;

    private Date createDateFrom;
    private Date createDateTo;

    /********************************
     * property for listing - END
     *******************************/

    public FansBadgeAction() {
        fansBadgeService = Application.lookupBean(FansBadgeService.class);
    }

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        statusList = optionBeanUtil.getFansBadgeStatus();
        allStatusList.add(optionBeanUtil.generateOptionAll());
        allStatusList.addAll(statusList);
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_FANS_BADGE)
    @Action(value = "/fansBadgeList")
    @Access(accessCode = AP.FANS_BADGE, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        initialize();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_FANS_BADGE_LIST);
        } else if (session.containsKey(SESSION_FANS_BADGE_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_FANS_BADGE_LIST);
        } else {
            session.put(SESSION_FANS_BADGE_LIST, objectMap);
        }

        memberCode = (String) objectMap.get("memberCode");
        createDateFrom = (Date) objectMap.get("createDateFrom");
        createDateTo = (Date) objectMap.get("createDateTo");
        status = (String) objectMap.get("status");

        return LIST;
    }

    @Action("/approveFansBadge")
    @Access(accessCode = AP.FANS_BADGE, adminMode = true)
    public String approveFansBadge() {
        try {
            fansBadgeService.doApproveFansBadge(getLocale(), fansBadgeId);
            successMessage = getText("updateSuccessfully");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @Action("/rejectFansBadge")
    @Access(accessCode = AP.FANS_BADGE, adminMode = true)
    public String rejectFansBadge() {
        try {
            fansBadgeService.doRejectFansBadge(getLocale(), fansBadgeId, rejectReason);
            successMessage = getText("updateSuccessfully");
        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public Date getCreateDateFrom() {
        return createDateFrom;
    }

    public void setCreateDateFrom(Date createDateFrom) {
        this.createDateFrom = createDateFrom;
    }

    public Date getCreateDateTo() {
        return createDateTo;
    }

    public void setCreateDateTo(Date createDateTo) {
        this.createDateTo = createDateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getFansBadgeId() {
        return fansBadgeId;
    }

    public void setFansBadgeId(String fansBadgeId) {
        this.fansBadgeId = fansBadgeId;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
