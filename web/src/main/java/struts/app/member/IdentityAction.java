package struts.app.member;

import java.io.InputStream;
import java.util.*;

import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.identity.IdentityConfiguration;
import com.compalsolutions.compal.identity.service.IdentityService;
import com.compalsolutions.compal.identity.vo.Identity;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberFile;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.springframework.core.env.Environment;

@Results(value = {
        @Result(name = BaseAction.INPUT, location = "identityEdit"), //
        @Result(name = BaseAction.LIST, location = "identityList"),
        @Result(name = BaseAction.EDIT, location = "identityEdit"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "identityList", "namespace", "/app/member",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}", "memberId", "${memberId}"}), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON), //
})

public class IdentityAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private Log log = LogFactory.getLog(IdentityAction.class);

    private static final String SESSION_IDENTITY_LIST = "identityList";

    protected Identity identity = new Identity(true);

    protected Member member = new Member(true);

    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String fullName;

    @ToUpperCase
    @ToTrim
    private String status;

    @ToUpperCase
    @ToTrim
    private String updateStatus;

    @ToUpperCase
    @ToTrim
    private String remark;

    private String identityId;
    private String identityTypeDesc;
    private String ids;
    private String verifyRemark;
    private String fileId;
    private String fileContentType;
    private String fileFileName;
    private String statusCode;

    private boolean showApproveRejectButton;
    private boolean clear;

    private InputStream fileInputStream;

    private List<OptionBean> allIdentityStatusCodeList = new ArrayList<>();
    private List<OptionBean> identityStatusCodeList = new ArrayList<>();
    private List<OptionBean> identityUpdateStatusCodeList = new ArrayList<>();

    private IdentityService identityService;
    private MemberService memberService;

    public IdentityAction() {
        identityService = Application.lookupBean(IdentityService.BEAN_NAME, IdentityService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    private void init() {
        Locale locale = getLocale();

        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);
        allIdentityStatusCodeList = optionBeanUtil.getIdentityStatusCodeWithOptionAll();
        identityStatusCodeList = optionBeanUtil.getIdentityUpdateStatusCodeWithOption();
        identityUpdateStatusCodeList = optionBeanUtil.getIdentityUpdateStatusCodeWithOption();
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_IDENTITY})
    @Action(value = "/identityList")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String list() {

        init();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_IDENTITY_LIST);
        } else if (session.containsKey(SESSION_IDENTITY_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_IDENTITY_LIST);
        } else {
            session.put(SESSION_IDENTITY_LIST, objectMap);
        }

        if (isSubmitData()) {
            objectMap.put("memberCode", memberCode);
            objectMap.put("status", status);

        } else {
            memberCode = (String) objectMap.get("memberCode");
            status = (String) objectMap.get("status");
        }
        return LIST;
    }

    @Action("/identityUpdateStatus")
    @Access(accessCode = AP.ROLE_MEMBER, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String identityUpdateStatus() {

        try {
            log.debug("Ids:" + ids);
            log.debug("Update Status Code:" + updateStatus);

            if (StringUtils.isNotBlank(ids)) {
                String[] idList = StringUtils.split(ids, ",");
                if (idList != null && idList.length > 0) {
                    identityService.doApproveOrRejectIdentity(getLocale(), updateStatus, remark, Arrays.asList(idList));
                }
            }

            successMessage = getText("successMessage.updateStatus");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_IDENTITY})
    @Action("/identityEdit")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String edit() {
        try {
          //  init();
            MemberFile icFront = new MemberFile();
            MemberFile icBack = new MemberFile();
            showApproveRejectButton = false;
            member = memberService.getMember(identityId);
            if(member.getKycStatus().equalsIgnoreCase("")){
                member.setKycStatus(Member.KYC_STATUS_PENDING_UPLOAD);
            }
            if(member.getKycStatus().equalsIgnoreCase(Member.KYC_STATUS_PENDING_APPROVAL)) {
                showApproveRejectButton = true;
            }
            Environment env = Application.lookupBean(Environment.class);
            icFront = memberService.findMemberFileByMemberIdAndType(member.getMemberId(),MemberFile.UPLOAD_TYPE_IC_FRONT);
            icBack = memberService.findMemberFileByMemberIdAndType(member.getMemberId(),MemberFile.UPLOAD_TYPE_IC_BACK);
            icFront.setFileUrlWithParentPath(env.getProperty("fileServerUrl")+"/file/memberFile");
            icBack.setFileUrlWithParentPath(env.getProperty("fileServerUrl")+"/file/memberFile");

            member.setIcFrontUrl(icFront!=null?icFront.getFileUrl():"");
            member.setIcBackUrl(icBack!=null?icBack.getFileUrl():"");
          //  initEdit();

        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return EDIT;
    }

   // @EnableTemplate(menuKey = {MP.FUNC_AD_IDENTITY})
   @Action("/identityApprove")
  //  @Access(accessCode = AP.ROLE_MEMBER, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String approve() {
        try {
               memberService.doUpdateMemberKycStatus(identityId, Member.KYC_STATUS_APPROVED);
               successMessage = getText("processSuccessfully");


        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }


    // @EnableTemplate(menuKey = {MP.FUNC_AD_IDENTITY})
    @Action("/identityReject")
    //  @Access(accessCode = AP.ROLE_MEMBER, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String reject() {
        try {
            memberService.doUpdateMemberKycStatus(identityId, Member.KYC_STATUS_REJECT);
            successMessage = getText("processSuccessfully");


        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }


    @EnableTemplate(menuKey = {MP.FUNC_AD_IDENTITY})
    @Action("/identityUpdate")
    @Accesses(access = { @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true) })
    public String update() {
        try {
            Member memberDb = memberService.getMember(member.getMemberId());

            if (updateStatus.equalsIgnoreCase("REJECTED_UPLOAD")) {
                memberDb.setKycStatus(Member.KYC_STATUS_REJECT);
            } else {
                memberDb.setKycStatus(Member.KYC_STATUS_APPROVED);
            }

            memberDb.setKycRemark(member.getKycRemark());

            memberService.updateMember(memberDb);
//            log.debug("identityId:" + identity.getIdentityId());
//            log.debug("Update Status Code:" + updateStatus);
//
//            dbIdentityMain = identityService.getIdentity(identity.getIdentityId());
//            if (dbIdentityMain == null) {
//                throw new ValidatorException("Invalid identity Form");
//            } else {
//                // stupid code to force loading
//                //identity.setDetailFiles(dbIdentityMain.getDetailFiles());
//                identity.getIcImageIds();
//                identity.getPassImageIds();
//            }
//
//            if (StringUtils.isEmpty(updateStatus)) {
//                updateStatus = identity.getStatus();
//            }
//
//            if (updateStatus.equals(identity.STATUS_NEW) || updateStatus.equals(identity.STATUS_PENDING_UPLOAD)) {
//                identityService.doUpdateIdentityByAdmin(getLocale(), identity);
//            } else {
//                identityService.doUpdateIdentityByAdmin(getLocale(), identity);
//                identityService.doApproveOrRejectIdentity(getLocale(), updateStatus, identity.getIdentityId(), identity.getVerifyRemark());
//            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            return EDIT;
        }

        return SUCCESS;
    }

    @Action("/deleteidentityIc")
    @Accesses(access = {@Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)})
    public String deleteIdentityIc() {
        try {
            log.debug("identity Id: " + identityId);

            //identityService.doDeleteIdentityType(identityId, IdentityFile.UPLOAD_TYPE_IC);

            successMessage = getText("successMessage.removeIC");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    @Action("/deleteidentityIcBack")
    @Accesses(access = {@Access(accessCode = AP.ROLE_MEMBER, adminMode = true, createMode = true, deleteMode = true, readMode = true, updateMode = true)})
    public String deleteIdentityIcBack() {
        try {
            log.debug("identity Id: " + identityId);

            //identityService.doDeleteIdentityType(identityId, IdentityFile.UPLOAD_TYPE_IC_BACK);

            successMessage = getText("successMessage.removeIC");

        } catch (Exception e) {
            addActionError(e.getMessage());
        }
        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------


    public Identity getIdentity() {
        return identity;
    }

    public void setIdentity(Identity identity) {
        this.identity = identity;
    }

/*    public IdentityFile getIdentityFile() {
        return identityFile;
    }*/

/*    public void setIdentityFile(IdentityFile identityFile) {
        this.identityFile = identityFile;
    }*/

    public String getIdentityId() {
        return identityId;
    }

    public void setIdentityId(String identityId) {
        this.identityId = identityId;
    }

    public List<OptionBean> getAllIdentityStatusCodeList() {
        return allIdentityStatusCodeList;
    }

    public void setAllIdentityStatusCodeList(List<OptionBean> allIdentityStatusCodeList) {
        this.allIdentityStatusCodeList = allIdentityStatusCodeList;
    }

    public List<OptionBean> getIdentityStatusCodeList() {
        return identityStatusCodeList;
    }

    public void setIdentityStatusCodeList(List<OptionBean> identityStatusCodeList) {
        this.identityStatusCodeList = identityStatusCodeList;
    }

    public List<OptionBean> getIdentityUpdateStatusCodeList() {
        return identityUpdateStatusCodeList;
    }

    public void setIdentityUpdateStatusCodeList(List<OptionBean> identityUpdateStatusCodeList) {
        this.identityUpdateStatusCodeList = identityUpdateStatusCodeList;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdentityTypeDesc() {
        return identityTypeDesc;
    }

    public void setIdentityTypeDesc(String identityTypeDesc) {
        this.identityTypeDesc = identityTypeDesc;
    }

    public boolean isShowApproveRejectButton() {
        return showApproveRejectButton;
    }

    public void setShowApproveRejectButton(boolean showApproveRejectButton) {
        this.showApproveRejectButton = showApproveRejectButton;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getIds() {
        return ids;
    }

    public void setIds(String ids) {
        this.ids = ids;
    }

    public String getVerifyRemark() {
        return verifyRemark;
    }

    public void setVerifyRemark(String verifyRemark) {
        this.verifyRemark = verifyRemark;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileContentType() {
        return fileContentType;
    }

    public void setFileContentType(String fileContentType) {
        this.fileContentType = fileContentType;
    }

    public String getFileFileName() {
        return fileFileName;
    }

    public void setFileFileName(String fileFileName) {
        this.fileFileName = fileFileName;
    }

    public InputStream getFileInputStream() {
        return fileInputStream;
    }

    public void setFileInputStream(InputStream fileInputStream) {
        this.fileInputStream = fileInputStream;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getUpdateStatus() {
        return updateStatus;
    }

    public void setUpdateStatus(String updateStatus) {
        this.updateStatus = updateStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
