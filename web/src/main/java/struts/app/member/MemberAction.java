package struts.app.member;

import java.util.*;

import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.AP;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.MP;
import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.EnableTemplate;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = {
        @Result(name = BaseAction.LIST, location = "memberList"),
        @Result(name = BaseAction.EDIT, location = "memberEdit"),
        @Result(name = BaseAction.SHOW, location = "memberView"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON)
})
public class MemberAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final String SESSION_MEMBER_LIST = "memberList";

    protected MemberService memberService;
    protected CountryService countryService;
    protected LiveStreamService liveStreamService;
    protected LiveStreamProvider liveStreamProvider;
    protected LiveChatRoomProvider liveChatRoomProvider;
    protected NotificationService notificationService;

    protected Member member = new Member(true);
    protected MemberDetail memberDetail = new MemberDetail(false);

    private List<OptionBean> allStatusList = new ArrayList<>();
    private List<OptionBean> statusList = new ArrayList<>();
    private List<OptionBean> liveStreamCategoryList = new ArrayList<>();
    private List<OptionBean> genderList = new ArrayList<>();
    private List<OptionBean> countryDescList = new ArrayList<>();
    private List<String> liveCategories = new ArrayList<>();

    private String password;
    private String password2;

    @ToTrim
    private String memberId;

    @ToTrim
    private String blockReason;

    private boolean clear;

    private String liveStreamPrivate;

    /********************************
     * property for listing - START
     *******************************/
    @ToUpperCase
    @ToTrim
    private String memberCode;

    @ToUpperCase
    @ToTrim
    private String whaleliveId;

    @ToUpperCase
    @ToTrim
    private String status;

    private Date joinDateFrom;
    private Date joinDateTo;

    /********************************
     * property for listing - END
     *******************************/

    public MemberAction() {
        memberService = Application.lookupBean(MemberService.class);
        countryService = Application.lookupBean(CountryService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.class);
        notificationService = Application.lookupBean(NotificationService.class);
    }

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allStatusList = optionBeanUtil.getMemberStatusWithOptionAll();
        statusList = optionBeanUtil.getMemberStatus();
        liveStreamCategoryList = optionBeanUtil.getActiveLiveStreamCategoryForMember();
        genderList = optionBeanUtil.getGenders();
        countryDescList = optionBeanUtil.getCountryDescList();
    }

    @SuppressWarnings("unchecked")
    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER)
    @Action(value = "/memberList")
    @Access(accessCode = AP.MEMBER, adminMode = true, createMode = true, readMode = true, deleteMode = true, updateMode = true)
    public String list() {
        VoUtil.toTrimUpperCaseProperties(this);
        initialize();

        Map<String, Object> objectMap = new HashMap<>();
        if (clear) {
            session.remove(SESSION_MEMBER_LIST);
        } else if (session.containsKey(SESSION_MEMBER_LIST)) {
            objectMap = (Map<String, Object>) session.get(SESSION_MEMBER_LIST);
        } else {
            session.put(SESSION_MEMBER_LIST, objectMap);
        }
        memberCode = (String) objectMap.get("memberCode");
        status = (String) objectMap.get("status");
        joinDateFrom = (Date) objectMap.get("joinDateFrom");
        joinDateTo = (Date) objectMap.get("joinDateTo");
        whaleliveId = (String) objectMap.get("whaleliveId");

        return LIST;
    }

    @Action("/memberUnblockLive")
    @Access(accessCode = AP.MEMBER, adminMode = true)
    public String unblockLiveStream() {
        LiveStreamChannel liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(memberId, Global.Status.DISABLED);
        if (liveStreamChannel != null) {
            liveStreamService.doBlockLiveStreamChannel(liveStreamChannel, Global.Status.INACTIVE, null);
        }

        return JSON;
    }

    @Action("/memberBlockLive")
    @Access(accessCode = AP.MEMBER, adminMode = true)
    public String blockLiveStream() {
        LiveStreamChannel liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(memberId, Global.Status.ACTIVE);

        if (liveStreamChannel != null) {
            liveStreamService.doBlockLiveStreamChannel(liveStreamChannel, Global.Status.DISABLED, blockReason);

            // send message to chat room to inform the host that he/she is blocked by admin
            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(Global.COMPANY_MEMBER, Global.AccessModule.WHALE_MEDIA);
            if (account != null) {
               liveChatRoomProvider.doSendMessageToChatRoom("NORMAL", liveStreamChannel.getChannelId(), account.getAccountId(), "ADMIN", "", blockReason, "BLOCK_CHANNEL", "", null, "", 0);
            }

            // force close live stream
            liveStreamProvider.doDeleteVCloudLiveStreamChannel(liveStreamChannel.getChannelId(), 0, 0);
        } else {
            addActionError(getText("liveStreamIsClosed"));
        }

        return JSON;
    }

    @Action("/memberResetPassword")
    @Access(accessCode = AP.MEMBER, updateMode = true, adminMode = true)
    public String resetPassword() {
        try {
            memberService.doResetPassword(getLocale(), memberId, password);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action("/memberResetTransactionPassword")
    @Access(accessCode = AP.MEMBER, updateMode = true, adminMode = true)
    public String resetTransactionPassword() {
        try {
            memberService.doResetPassword2(getLocale(), memberId, password2);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = MP.FUNC_AD_MEMBER)
    @Action(value = "/memberEdit")
    @Access(accessCode = AP.MEMBER, adminMode = true, updateMode = true)
    public String edit() {
        initialize();
        getMemberInfo();
        setDisplayValue();

        return EDIT;
    }

    @Action(value = "/memberUpdate")
    @Access(accessCode = AP.MEMBER, adminMode = true, updateMode = true)
    public String update() {
        try {
            VoUtil.toTrimUpperCaseProperties(this);
            String liveTypes = "";
            for (Iterator<String> iterator = liveCategories.iterator(); iterator.hasNext(); ) {
                liveTypes += iterator.next();
                if (iterator.hasNext())
                    liveTypes += "|";
            }

            if (StringUtils.isNotBlank(liveStreamPrivate)) {
                member.setLiveStreamPrivate(true);
            } else {
                member.setLiveStreamPrivate(false);
            }

            member.setLiveStreamTypes(liveTypes);
            memberService.doUpdateMemberSetting(member);

            successMessage = getText("successMessage.MemberAction.update");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @EnableTemplate(menuKey = {MP.FUNC_AD_MEMBER})
    @Action("/memberView")
    @Access(accessCode = AP.MEMBER, adminMode = true)
    public String view() {
        initialize();
        getMemberInfo();
        setDisplayValue();

        return SHOW;
    }

    private void getMemberInfo() {
        if (StringUtils.isNotBlank(member.getMemberId())) {
            member = memberService.getMember(member.getMemberId());
            if (member == null)
                addActionError(getText("invalidMember"));
        } else {
            addActionError(getText("invalidMember"));
        }
    }

    private void setDisplayValue() {
        String[] categories = StringUtils.split(member.getLiveStreamTypes(), "|");
        if (categories != null) {
            for (String category : categories) {
                liveCategories.add(category);
            }
        }
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMemberId() {
        return memberId;
    }

    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

    public List<OptionBean> getAllStatusList() {
        return allStatusList;
    }

    public void setAllStatusList(List<OptionBean> allStatusList) {
        this.allStatusList = allStatusList;
    }

    public List<OptionBean> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<OptionBean> statusList) {
        this.statusList = statusList;
    }

    public List<OptionBean> getLiveStreamCategoryList() {
        return liveStreamCategoryList;
    }

    public void setLiveStreamCategoryList(List<OptionBean> liveStreamCategoryList) {
        this.liveStreamCategoryList = liveStreamCategoryList;
    }

    public List<OptionBean> getGenderList() {
        return genderList;
    }

    public void setGenderList(List<OptionBean> genderList) {
        this.genderList = genderList;
    }

    public List<OptionBean> getCountryDescList() {
        return countryDescList;
    }

    public void setCountryDescList(List<OptionBean> countryDescList) {
        this.countryDescList = countryDescList;
    }

    public List<String> getLiveCategories() {
        return liveCategories;
    }

    public void setLiveCategories(List<String> liveCategories) {
        this.liveCategories = liveCategories;
    }

    public String getMemberCode() {
        return memberCode;
    }

    public void setMemberCode(String memberCode) {
        this.memberCode = memberCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getJoinDateFrom() {
        return joinDateFrom;
    }

    public void setJoinDateFrom(Date joinDateFrom) {
        this.joinDateFrom = joinDateFrom;
    }

    public Date getJoinDateTo() {
        return joinDateTo;
    }

    public void setJoinDateTo(Date joinDateTo) {
        this.joinDateTo = joinDateTo;
    }

    public boolean isClear() {
        return clear;
    }

    public void setClear(boolean clear) {
        this.clear = clear;
    }

    public String getPassword2() {
        return password2;
    }

    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    public String getBlockReason() {
        return blockReason;
    }

    public void setBlockReason(String blockReason) {
        this.blockReason = blockReason;
    }

    public String getLiveStreamPrivate() {
        return liveStreamPrivate;
    }

    public void setLiveStreamPrivate(String liveStreamPrivate) {
        this.liveStreamPrivate = liveStreamPrivate;
    }

    public String getWhaleliveId() {
        return whaleliveId;
    }

    public void setWhaleliveId(String whaleliveId) {
        this.whaleliveId = whaleliveId;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
