package struts.app.jasper;

import java.util.List;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.struts.jasper.BaseJasperAction;

public class UserJasperAction extends BaseJasperAction {
    private static final long serialVersionUID = 1L;

    private UserDetailsService userDetailService;
    private User user;
    private List<User> userList = null;

    public UserJasperAction() {
        userDetailService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
    }

    public String execute() throws Exception {
        // log.debug("Start");
        // long start = System.currentTimeMillis();
        userList = userDetailService.findUserForReport(user.getUsername().toUpperCase(), "");

        // log.debug("End");
        // long end = System.currentTimeMillis();
        // log.debug("time taken : " + (end - start) / 1000.0 + "sec");
        return SUCCESS;
    }

    public List<User> getUserList() {
        return userList;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
