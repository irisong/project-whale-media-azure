package struts.app.jasper;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.income.dto.IncomeProviderDto;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.util.VoUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.core.env.Environment;

import javax.servlet.http.HttpServletRequest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/*@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "htmlMessage"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.JASPER, params = { //
                "location", "/WEB-INF/jasperreports/incomeProviderReport.jasper", //
                "contentDisposition", "attachment;filename=${filename}", //
                "dataSource", "incomeProviderDto", //
                "wrapField", "false",
                "dirPath", "dirPath",
                "reportParameters", "reportParameters"}) //
})*/
public class IncomeJasperAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private static final Log log = LogFactory.getLog(IncomeJasperAction.class);

    private String dirPath;
    private String filename;
    private IncomeProviderDto incomeProviderDto = new IncomeProviderDto();

    public IncomeJasperAction() {
        Environment env = Application.lookupBean(Environment.class);
        dirPath = env.getProperty("jasper.whalemedia.path");
    }

  /*  @Action("/incomeProvider")
    public String execute() throws Exception {
        if(StringUtils.isBlank(dirPath)) {
            dirPath = System.getProperty("user.dir");
            dirPath+= "/src/main/webapp";

        }
        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        this.filename = "receipt_whalemedia_ "+ df.format(new Date()) + ".pdf";
        HttpServletRequest request = ServletActionContext.getRequest();


        Locale locale = new Locale("EN");

        VoUtil.toTrimUpperCaseProperties(this);

        incomeProviderDto.setMemberId("213123123213");

        return SUCCESS;
    }*/

    public String getDirPath() {
        return dirPath;
    }

    public void setDirPath(String dirPath) {
        this.dirPath = dirPath;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public IncomeProviderDto getIncomeProviderDto() {
        return incomeProviderDto;
    }

    public void setIncomeProviderDto(IncomeProviderDto incomeProviderDto) {
        this.incomeProviderDto = incomeProviderDto;
    }
}
