package struts.pub;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.event.service.VjMissionService;
import com.compalsolutions.compal.event.vo.VjMission;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.live.chatroom.LiveChatRoomProvider;
import com.compalsolutions.compal.live.chatroom.client.dto.LiveChatResponse;
import com.compalsolutions.compal.live.streaming.LiveStreamProvider;
import com.compalsolutions.compal.live.streaming.client.dto.Channel;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.notification.client.YunxinClient;
import com.compalsolutions.compal.notification.client.dto.YunXinResponse;
import com.compalsolutions.compal.notification.service.NotificationService;
import com.compalsolutions.compal.notification.vo.NotificationAccount;
import com.compalsolutions.compal.point.service.PointService;
import com.compalsolutions.compal.rank.service.RankService;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.event.service.LuckyDrawService;
import com.compalsolutions.compal.event.vo.LuckyDrawPackage;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.springframework.core.env.Environment;
import rest.vo.SimpleOptionBean;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.sql.Timestamp;
import java.util.*;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", LiveStreamChannelAction.DEFAULT_JSON_INCLUDE})
})
public class LiveStreamChannelAction extends BaseAction {
    private static final Log log = LogFactory.getLog(LiveStreamChannelAction.class);

    public static final String DEFAULT_JSON_INCLUDE = "message, liveStream.*, levelUrl, entryMsgCode, entryMsg.*, spamDuration, " +
            "topUpMsg.*, luckyDrawItem, maxQty, hasVjMission, vjConnectRejectDesc, vjConnectRejectOptions.*";

    private TokenProvider tokenProvider;
    private LiveStreamProvider liveStreamProvider;
    private LiveChatRoomProvider liveChatRoomProvider;
    private NotificationService notificationService;
    private LiveStreamService liveStreamService;
    private MemberService memberService;
    private LuckyDrawService luckyDrawService;
    private RankService rankService;
    private PointService pointService;
    private VjMissionService vjMissionService;
    private I18n i18n;
    private Environment env;

    public LiveStreamChannelAction() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
        liveStreamProvider = Application.lookupBean(LiveStreamProvider.class);
        liveChatRoomProvider = Application.lookupBean(LiveChatRoomProvider.class);
        notificationService = Application.lookupBean(NotificationService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.class);
        memberService = Application.lookupBean(MemberService.class);
        luckyDrawService = Application.lookupBean(LuckyDrawService.class);
        rankService = Application.lookupBean(RankService.class);
        pointService = Application.lookupBean(PointService.class);
        vjMissionService = Application.lookupBean(VjMissionService.class);
        env = Application.lookupBean(Environment.class);
        i18n = Application.lookupBean(I18n.class);
    }

    private String token;
    private String languageCode;

    @ToTrim
    private String message;

    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    @ToUpperCase
    @ToTrim
    private String category;
    private String title;
    private boolean privateLive;
    private String privateAccessCode;
    private String announcement;

    private Channel liveStream = null;

    private String levelUrl;
    private String entryMsgCode;
    private long spamDuration;
    private List<String> entryMsg;
    private Map<String, List<String>> topUpMsg;
    private String vjConnectRejectDesc;
    private List<SimpleOptionBean> vjConnectRejectOptions;
    private String luckyDrawItem = null;
    private int maxQty = 0;
    private boolean hasVjMission = false;

    @Action("/createChannel")
    public String createChannel() {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse respond = ServletActionContext.getResponse();

        if (StringUtils.isBlank(languageCode)) {
            languageCode = request.getHeader("Content-Language");
        }

        if (StringUtils.isBlank(token)) {
            String bearerToken = request.getHeader("Authorization");
            String[] ss = StringUtils.split(bearerToken);

            if (ss.length != 2) {
                log.debug("invalid token format " + bearerToken);
            }

            token = ss[1];
        }

        try {
            VoUtil.toTrimUpperCaseProperties(this);
            Locale locale = new Locale(languageCode);

            String memberId;
            try { //check token
                memberId = tokenProvider.getMemberIdByToken(locale, token);
            } catch (Exception ex) {
                respond.setStatus(401);
                message = ex.getMessage();
                return JSON;
            }

//            if (true) {
//                throw new Exception(i18n.getText("systemUpgrade", locale));
//            }

            /*********************
             * VALIDATION - START
             *********************/
            if (StringUtils.isBlank(category)) {
                throw new Exception(i18n.getText("invalidCategory", locale));
            }

            if (privateLive) {
                if (StringUtils.isBlank(privateAccessCode)) {
                    throw new Exception(i18n.getText("privacyLivePasscodeInvalid", locale));
                }
            }

            Member member = memberService.getMember(memberId);
            if (!member.getKycStatus().equalsIgnoreCase(Member.KYC_STATUS_APPROVED)) {
                throw new Exception(i18n.getText("kycNotApprovedYet", locale));
            }

            // check if user have been blocked to do Live Streaming
            boolean isBlockedChannel = liveStreamService.checkIsBlockedMemberChannel(memberId, false);
            if (isBlockedChannel) {
                respond.setStatus(403); //Response.Status.FORBIDDEN
                message = i18n.getText("blockedUserNotAllowStream", locale);
                return JSON;
            }

            // double check fans badge exists
            if (StringUtils.isBlank(member.getFansBadgeName())) {
                throw new Exception(i18n.getText("fansBadgeDoesNotExist", locale));
            }
            /*********************
             * VALIDATION - END
             *********************/

            String serverUrl = env.getProperty("serverUrl");
            String fileServerUrl = env.getProperty("fileServerUrl");

            NotificationAccount account = notificationService.findNotifAccByMemberAndAccessModule(memberId, Global.AccessModule.WHALE_MEDIA);
            if (account == null) {
                YunxinClient yunxinClient = new YunxinClient();
                YunXinResponse response = yunxinClient.createUserAccount(notificationService.generateAccId(), Global.AccessModule.WHALE_MEDIA);
                if (response != null && response.getInfo() != null) {
                    account = notificationService.doCreateNotificationAccount(memberId, Global.AccessModule.WHALE_MEDIA, response.getInfo());
                }
            }

            // create point report first, in case duplicate during gift spamming later
            pointService.doCreatePointSummaryIfNotExists(memberId);
            pointService.doCreateCreditPointReportTypeIfNotExists(memberId, new Date(), null);

            // create channel
            liveStream = liveStreamProvider.doCreateVCloudLiveStreamChannel(memberId, category, title, privateLive, privateAccessCode);

            // create chat room
            LiveChatResponse liveChatResponse;
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            String md5Hex = DigestUtils.md5Hex(memberId + timestamp);
            if (liveStream != null) {
                liveChatResponse = liveChatRoomProvider.doCreateLiveChatRoom(liveStream.getChannelId(), account.getAccountId(), md5Hex, announcement);
                if (liveChatResponse != null) {
                    liveStream.setRoomId(liveChatResponse.getChatroom().getRoomid());
                }
            }

            // upload cover photo
            LiveStreamChannel liveStreamChannel = liveStreamService.getLatestLiveStreamChannelByMemberId(memberId, Global.Status.PENDING);
            if (fileUpload != null) {
                liveStreamChannel = liveStreamService.doProcessLiveStreamCoverUploadFile(locale, memberId, fileUpload, fileUploadContentType, fileUploadFileName);
                if (StringUtils.isNotBlank(liveStreamChannel.getFilename())) {
                    liveStream.setCoverUrl(liveStreamChannel.getFileUrlWithParentPath(fileServerUrl + "/file/liveStreamCover"));
                }
            }

            levelUrl = serverUrl + "/asset/image/rank/" + Member.getRankInString(rankService.getCurrentRank(member.getTotalExp())) + ".png";

            entryMsgCode = "#FEEFB0";
            entryMsg = new ArrayList<>();
            entryMsg.add(i18n.getText("enterStreamDefaultMsg", locale));
            entryMsg.add(i18n.getText("enterStreamDefaultMsg2", locale));

            List<String> contentEN = new ArrayList<>();
            contentEN.add(i18n.getText("topUpDescription", Global.LOCALE_EN));

            List<String> contentCN = new ArrayList<>();
            contentCN.add(i18n.getText("topUpDescription", Global.LOCALE_CN));

            spamDuration = 120000;

            topUpMsg = new HashMap<>();
            topUpMsg.put("zh", contentCN);
            topUpMsg.put("en", contentEN);

            vjConnectRejectDesc = i18n.getText("doNotReceiveVjConnectFrmThisVj", locale);
            vjConnectRejectOptions = Arrays.asList(
                    new SimpleOptionBean("30", "30 "+i18n.getText("minutes", locale)),
                    new SimpleOptionBean("40", "40 "+i18n.getText("minutes", locale)),
                    new SimpleOptionBean("50", "50 "+i18n.getText("minutes", locale)),
                    new SimpleOptionBean("60", "60 "+i18n.getText("minutes", locale))
            );

            if (liveStreamChannel != null) {
                //when change status = ACTIVE only send notification
//                if (liveStreamChannel.getMemberId().equals("ff8081817714c47b017714dd952a09a2")) { //小琪
//                    String msg = member.getMemberCode() + " [" + member.getMemberDetail().getProfileName() + "] is on streaming.";
//                    BotClient botClient = new BotClient();
//                    botClient.doSendReportMessage(msg);
//                }
//                liveStreamService.doProcessLiveStreamNotification(liveStreamChannel); // pop up notification

                //check vj has lucky draw
                LuckyDrawPackage luckyDrawPackage = luckyDrawService.getNextLuckyDrawItem(liveStreamChannel.getMemberId(), new Date());
                if (luckyDrawPackage != null) {
                    luckyDrawItem = luckyDrawPackage.getItemName();
                    maxQty = luckyDrawPackage.getRemainingQty();
                }

                //check vj has set mission
                VjMission vjMission = vjMissionService.doGetLatestVjMissionByMemberId(liveStreamChannel.getMemberId());
                if (vjMission != null && vjMission.getStatus().equalsIgnoreCase(VjMission.STATUS_ACTIVE)) {
                    hasVjMission = true;
                }
            }

            message = i18n.getText("statSuccess", locale);
            respond.setStatus(200);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            respond.setStatus(404);
            message = ex.getMessage();
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Collection<String> getErrorMessages() {
        return this.getActionErrors();
    }

    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPrivateLive() {
        return privateLive;
    }

    public void setPrivateLive(boolean privateLive) {
        this.privateLive = privateLive;
    }

    public String getPrivateAccessCode() {
        return privateAccessCode;
    }

    public void setPrivateAccessCode(String privateAccessCode) {
        this.privateAccessCode = privateAccessCode;
    }

    public String getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(String announcement) {
        this.announcement = announcement;
    }

    public String getLuckyDrawItem() {
        return luckyDrawItem;
    }

    public void setLuckyDrawItem(String luckyDrawItem) {
        this.luckyDrawItem = luckyDrawItem;
    }

    public int getMaxQty() {
        return maxQty;
    }

    public void setMaxQty(int maxQty) {
        this.maxQty = maxQty;
    }

    public Channel getLiveStream() {
        return liveStream;
    }

    public void setLiveStream(Channel liveStream) {
        this.liveStream = liveStream;
    }

    public String getLevelUrl() {
        return levelUrl;
    }

    public void setLevelUrl(String levelUrl) {
        this.levelUrl = levelUrl;
    }

    public String getEntryMsgCode() {
        return entryMsgCode;
    }

    public void setEntryMsgCode(String entryMsgCode) {
        this.entryMsgCode = entryMsgCode;
    }

    public List<String> getEntryMsg() {
        return entryMsg;
    }

    public void setEntryMsg(List<String> entryMsg) {
        this.entryMsg = entryMsg;
    }

    public long getSpamDuration() {
        return spamDuration;
    }

    public void setSpamDuration(long spamDuration) {
        this.spamDuration = spamDuration;
    }

    public Map<String, List<String>> getTopUpMsg() {
        return topUpMsg;
    }

    public void setTopUpMsg(Map<String, List<String>> topUpMsg) {
        this.topUpMsg = topUpMsg;
    }

    public String getVjConnectRejectDesc() {
        return vjConnectRejectDesc;
    }

    public void setVjConnectRejectDesc(String vjConnectRejectDesc) {
        this.vjConnectRejectDesc = vjConnectRejectDesc;
    }

    public List<SimpleOptionBean> getVjConnectRejectOptions() {
        return vjConnectRejectOptions;
    }

    public void setVjConnectRejectOptions(List<SimpleOptionBean> vjConnectRejectOptions) {
        this.vjConnectRejectOptions = vjConnectRejectOptions;
    }

    public boolean isHasVjMission() {
        return hasVjMission;
    }

    public void setHasVjMission(boolean hasVjMission) {
        this.hasVjMission = hasVjMission;
    }

    // ---------------- GETTER & SETTER (END) -------------
}