package struts.pub;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.identity.service.IdentityService;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.jose4j.jwt.JwtClaims;

import java.io.File;
import java.util.Collection;
import java.util.Locale;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", IdentityFormAction.DEFAULT_JSONINCLUDE})
})
public class IdentityFormAction extends BaseAction {
    private static final Log log = LogFactory.getLog(IdentityFormAction.class);

    public static final String DEFAULT_JSONINCLUDE = "errorMessages, errorMessages\\[\\d+\\], successMessage";

    private TokenProvider tokenProvider;
    private I18n i18n;
    private IdentityService identityService;

    public IdentityFormAction() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
        i18n = Application.lookupBean(I18n.class);
        identityService = Application.lookupBean(IdentityService.class);
    }

    // File Upload
    @ToUpperCase
    @ToTrim
    private String uploadType;
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;

    private String token;
    private String languageCode;

    @Action("/identityUploadFile")
    public String uploadFile() {
        try {
            Locale locale = new Locale(languageCode);

            Pair<JwtClaims, JwtToken> pair = tokenProvider.validateJwtToken(locale, token);
            JwtToken jwtToken = pair.getRight();

            String memberId = ((MemberUser) jwtToken.getUser()).getMemberId();
            VoUtil.toTrimUpperCaseProperties(this);

            //identityService.doProcessIdentityUploadFile(locale, memberId, uploadType, fileUpload, fileUploadFileName, fileUploadContentType);
            successMessage = i18n.getText("uploadFileSuccess", locale);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Collection<String> getErrorMessages() {
        return this.getActionErrors();
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
