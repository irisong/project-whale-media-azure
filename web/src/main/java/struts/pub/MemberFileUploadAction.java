package struts.pub;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberFile;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.user.vo.MemberUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.InvalidJwtException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Collection;
import java.util.Locale;

@Results(value = {//
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", MemberFileUploadAction.DEFAULT_JSON_INCLUDE})//
})
public class MemberFileUploadAction extends BaseAction {
    private static final Log log = LogFactory.getLog(MemberFileUploadAction.class);

    public static final String DEFAULT_JSON_INCLUDE = "message";

    private TokenProvider tokenProvider;
    private I18n i18n;
    private MemberService memberService;

    public MemberFileUploadAction() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
        i18n = Application.lookupBean(I18n.class);
        memberService = Application.lookupBean(MemberService.class);
    }

    // File Upload
    @ToTrim
    private String message;

    @ToUpperCase
    @ToTrim
    private String uploadType;
    private File fileUpload;
    private String fileUploadContentType;
    private String fileUploadFileName;
    private String token;
    private String languageCode;

    private File icFrontUpload;
    private String icFrontUploadContentType;
    private String icFrontUploadFileName;

    private File icBackUpload;
    private String icBackUploadContentType;
    private String icBackUploadFileName;


    /****************************
     * KYC - START
     ****************************/
    @ToTrim
    @ToUpperCase
    private String fullName;

    private String ic_no;

    /****************************
     * KYC - END
     ****************************/

    @Action("/memberFileUpload")
    public String uploadMemberFile() {
        HttpServletRequest request = ServletActionContext.getRequest();
        if (StringUtils.isBlank(languageCode)) {
            languageCode = request.getHeader("Content-Language");
        }

        if (StringUtils.isBlank(token)) {
            String bearerToken = request.getHeader("Authorization");
            String[] ss = StringUtils.split(bearerToken);

            if (ss.length != 2) {
                log.debug("invalid token format " + bearerToken);
            }

            token = ss[1];
        }

        try {
            Locale locale = new Locale(languageCode);

            String memberId;
            try { //check token
                memberId = tokenProvider.getMemberIdByToken(locale, token);
            } catch (Exception ex) {
                HttpServletResponse respond = ServletActionContext.getResponse();
                respond.setStatus(401);
                message = ex.getMessage();
                return JSON;
            }
            VoUtil.toTrimUpperCaseProperties(this);

            memberService.doProcessMemberUploadFile(memberId, uploadType, fileUpload, fileUploadContentType, fileUploadFileName);
            message = i18n.getText("uploadFileSuccess", locale);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            HttpServletResponse res = ServletActionContext.getResponse();
            res.setStatus(404);
            message = ex.getMessage();
        }
        return JSON;
    }

    @Action("/memberKYCUpload")
    public String uploadMemberKYCFile() {
        HttpServletRequest request = ServletActionContext.getRequest();

        if (StringUtils.isBlank(languageCode)) {
            languageCode = request.getHeader("Content-Language");
        }

        if (StringUtils.isBlank(token)) {
            String bearerToken = request.getHeader("Authorization");
            String[] ss = StringUtils.split(bearerToken);

            if (ss.length != 2) {
                log.debug("invalid token format " + bearerToken);
            }

            token = ss[1];
        }

        try {
            Locale locale = new Locale(languageCode);

            String memberId;
            try { //check token
                memberId = tokenProvider.getMemberIdByToken(locale, token);
            } catch (Exception ex) {
                HttpServletResponse respond = ServletActionContext.getResponse();
                respond.setStatus(401);
                message = ex.getMessage();
                return JSON;
            }
            VoUtil.toTrimUpperCaseProperties(this);

            memberService.doUpdateMember(memberId, fullName, ic_no);

            if (StringUtils.isNotBlank(icFrontUploadFileName)) {
                memberService.doProcessMemberUploadFile(memberId, MemberFile.UPLOAD_TYPE_IC_FRONT, icFrontUpload, icFrontUploadContentType, icFrontUploadFileName);
            }
            if (StringUtils.isNotBlank(icBackUploadFileName)) {
                memberService.doProcessMemberUploadFile(memberId, MemberFile.UPLOAD_TYPE_IC_BACK, icBackUpload, icBackUploadContentType, icBackUploadFileName);
            }

            memberService.doUpdateMemberKycStatus(memberId, Member.KYC_STATUS_PENDING_APPROVAL);

            //SEND EMAIL TO ADMIN
            Member member = memberService.getMember(memberId);
            if (member.getKycStatus().equals(Member.KYC_STATUS_PENDING_APPROVAL)) {
                Locale localeCn = Global.LOCALE_CN;
                String title = i18n.getText("memberSubmitKycApplication") + " / " + i18n.getText("memberSubmitKycApplication", localeCn);
                String body = i18n.getText("followingMemberHasSubmittedKycApplication", member.getMemberCode(), member.getFullName())
                        + "<br/><br/>" + i18n.getText("followingMemberHasSubmittedKycApplication", localeCn, member.getMemberCode(), member.getFullName());
                memberService.doSendNotificationEmailToAdmin(null, null, title, body);
            }

            message = i18n.getText("uploadFileSuccess", locale);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            HttpServletResponse res = ServletActionContext.getResponse();
            res.setStatus(404);
            message = ex.getMessage();
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------
    public Collection<String> getErrorMessages() {
        return this.getActionErrors();
    }

    public String getUploadType() {
        return uploadType;
    }

    public void setUploadType(String uploadType) {
        this.uploadType = uploadType;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getIc_no() {
        return ic_no;
    }

    public void setIc_no(String ic_no) {
        this.ic_no = ic_no;
    }

    public static Log getLog() {
        return log;
    }

    public File getIcFrontUpload() {
        return icFrontUpload;
    }

    public void setIcFrontUpload(File icFrontUpload) {
        this.icFrontUpload = icFrontUpload;
    }

    public String getIcFrontUploadContentType() {
        return icFrontUploadContentType;
    }

    public void setIcFrontUploadContentType(String icFrontUploadContentType) {
        this.icFrontUploadContentType = icFrontUploadContentType;
    }

    public String getIcFrontUploadFileName() {
        return icFrontUploadFileName;
    }

    public void setIcFrontUploadFileName(String icFrontUploadFileName) {
        this.icFrontUploadFileName = icFrontUploadFileName;
    }

    public File getIcBackUpload() {
        return icBackUpload;
    }

    public void setIcBackUpload(File icBackUpload) {
        this.icBackUpload = icBackUpload;
    }

    public String getIcBackUploadContentType() {
        return icBackUploadContentType;
    }

    public void setIcBackUploadContentType(String icBackUploadContentType) {
        this.icBackUploadContentType = icBackUploadContentType;
    }

    public String getIcBackUploadFileName() {
        return icBackUploadFileName;
    }

    public void setIcBackUploadFileName(String icBackUploadFileName) {
        this.icBackUploadFileName = icBackUploadFileName;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
