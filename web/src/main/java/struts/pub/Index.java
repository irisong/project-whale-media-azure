package struts.pub;

import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.struts.BaseAction;

@Results(value = { //
        @Result(name = "input", location = "memberLogin") //
})
public class Index extends BaseAction {
    private static final long serialVersionUID = 1L;

    @Override
    public String execute() throws Exception {
        return INPUT;
    }
}
