package struts.pub.login;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.I18nInterceptor;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.web.security.BaseSecurityUtil;

@SuppressWarnings("serial")
@Results(value = { //
        @Result(name = "input", location = "adminLogin"), //
        @Result(name = "success", type = "redirectAction", params = { "actionName", "app", "namespace", "/app" }),
        // @Result(name = "memberInput", location = "login", params = { "namespace", "/pub/shop" }), //
        @Result(name = "memberInput", location = "logingin"),
        @Result(name = "memberSuccess", type = "redirectAction", params = { "actionName", "main", "namespace", "/pub/shop" }), //
})
public class LoginAction extends BaseAction implements ServletResponseAware, ServletRequestAware {
    @SuppressWarnings("unused")
    private static final Log log = LogFactory.getLog(LoginAction.class);

    @ToUpperCase
    private String username;
    private String password;
    private String securityCode;

    private boolean disableLogin;
    private String userType;

    private String language;
    private Locale systemLocale;
    private List<OptionBean> languages = new ArrayList<OptionBean>();

    private HttpServletResponse httpServletResponse;
    private HttpServletRequest httpServletRequest;

    public LoginAction() {
        systemLocale = Application.lookupBean("systemLocale", Locale.class);
    }

    private void initLanguage() {
        if (StringUtils.isBlank(language)) {
            language = getLanguageCodeFromCookies();
        }
        if (StringUtils.isBlank(language)) {
            language = systemLocale.getLanguage();
        }

        List<Language> tempLanguages = WebUtil.getLanguages();
        for (Language language : tempLanguages) {
            languages.add(new OptionBean(language.getLanguageCode(), language.getLanguageName()));
        }

        setLanguageCodeToCookies();
        Locale locale = new Locale(language);
        session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
    }

    private void setLanguageCodeToCookies() {
        Cookie cookie = new Cookie(Global.USER_LANGUAGE_COOKIES, language);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        httpServletResponse.addCookie(cookie);
    }

    private String getLanguageCodeFromCookies() {
        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies == null)
            return null;

        for (Cookie cookie : cookies) {
            if (Global.USER_LANGUAGE_COOKIES.equals(cookie.getName()))
                return cookie.getValue();
        }
        return null;
    }

    @Action(value = "/adminLogin")
    public String execute() {
        BaseSecurityUtil.setLoginUrlToCookies(httpServletResponse, Global.UserType.HQ);

        initLanguage();
        return INPUT;
    }

    @Action(value = "/login")
    public String login() {
        BaseSecurityUtil.setLoginUrlToCookies(httpServletResponse, Global.UserType.MEMBER);

        initLanguage();

        userType = Global.UserType.MEMBER;

        SystemConfig systemConfig = WebUtil.getSystemConfig();
        if (!systemConfig.getMemberLogin()) {
            disableLogin = true;
            addActionError(getText("systemIsOffiline"));
        }

        return "memberInput";
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        httpServletResponse = response;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        httpServletRequest = request;
    }

    public String getSecurityCode() {
        return securityCode;
    }

    public void setSecurityCode(String securityCode) {
        this.securityCode = securityCode;
    }

    public boolean isDisableLogin() {
        return disableLogin;
    }

    public void setDisableLogin(boolean disableLogin) {
        this.disableLogin = disableLogin;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
