package struts.pub.misc;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.service.PlacementService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class VerifyPlacementIdAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;
    private PlacementService placementService;

    // do not have getter and setter.
    private Member memberSponsor;
    private Member memberPlacement;

    @ToTrim
    @ToUpperCase
    private String sponsorId;

    private String sponsorUsername;

    @ToTrim
    @ToUpperCase
    private String placementDistId;

    private String placementUsername;

    public VerifyPlacementIdAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        placementService = Application.lookupBean(PlacementService.BEAN_NAME, PlacementService.class);
    }

    @Action(value = "/verifyActivePlacementId")
    @Override
    public String execute() throws Exception {
        verifyPlacementId();
        if (memberSponsor != null && !Global.Status.ACTIVE.equalsIgnoreCase(memberSponsor.getStatus())) {
            sponsorUsername = "";
        }

        return JSON;
    }

    @Action(value = "/verifyPlacementId")
    public String verifyPlacementId() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        memberSponsor = memberService.findMemberByMemberCode(sponsorId);
        memberPlacement = memberService.findMemberByMemberCode(placementDistId);

        sponsorUsername = "";
        placementUsername = "";

        if (memberSponsor == null || memberPlacement == null) {
            return JSON;
        }
        if (memberSponsor != null)
            sponsorUsername = memberSponsor.getMemberCode();

        if (memberPlacement != null)
            placementUsername = memberPlacement.getMemberCode();

        boolean isSameGroup = placementService.isSponsorIdAndPlacementIdSameGroup(memberSponsor.getMemberId(), memberPlacement.getMemberId());
        if (!isSameGroup) {
            sponsorUsername = "";
            placementUsername = "";
            return JSON;
        }

        return JSON;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorUsername() {
        return sponsorUsername;
    }

    public void setSponsorUsername(String sponsorUsername) {
        this.sponsorUsername = sponsorUsername;
    }

    public String getPlacementDistId() {
        return placementDistId;
    }

    public void setPlacementDistId(String placementDistId) {
        this.placementDistId = placementDistId;
    }

    public String getPlacementUsername() {
        return placementUsername;
    }

    public void setPlacementUsername(String placementUsername) {
        this.placementUsername = placementUsername;
    }
}
