package struts.pub.misc;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.service.SponsorService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Deprecated
@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class VerifySponsorIdAction extends BaseSecureAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;
    private SponsorService sponsorService;

    // do not have getter and setter.
    private Member member;

    @ToTrim
    @ToUpperCase
    private String sponsorId;

    private String sponsorUsername;

    private boolean verifySameGroup;

    public VerifySponsorIdAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        sponsorService = Application.lookupBean(SponsorService.BEAN_NAME, SponsorService.class);
    }

    @Action(value = "/verifyActiveSponsorId")
    @Override
    public String execute() throws Exception {
        verifySponsorId();

        if (StringUtils.isBlank(sponsorUsername)) {
            return JSON;
        }

//        if (member != null && !Global.Status.ACTIVE.equalsIgnoreCase(member.getStatus()) && StringUtils.isNotBlank(member.getRankId())) {
//            sponsorUsername = "";
//            return JSON;
//        }

        if (verifySameGroup) {
            LoginInfo loginInfo = getLoginInfo();
            if (loginInfo == null) {
                sponsorUsername = "";
                return JSON;
            }

            String memberId = WebUtil.getLoginMemberId(loginInfo);
            boolean isSameGroup = sponsorService.isSameGroup(memberId, member.getMemberId());
            if (!isSameGroup) {
                sponsorUsername = "";
                return JSON;
            }
        }

        return JSON;
    }

    @Action(value = "/verifySponsorId")
    public String verifySponsorId() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);

        member = memberService.findMemberByMemberCode(sponsorId);

        if (member == null) {
            sponsorUsername = "";
        } else {
            sponsorUsername = member.getMemberCode();
        }

        return JSON;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorUsername() {
        return sponsorUsername;
    }

    public void setSponsorUsername(String sponsorUsername) {
        this.sponsorUsername = sponsorUsername;
    }

    public boolean isVerifySameGroup() {
        return verifySameGroup;
    }

    public void setVerifySameGroup(boolean verifySameGroup) {
        this.verifySameGroup = verifySameGroup;
    }
}
