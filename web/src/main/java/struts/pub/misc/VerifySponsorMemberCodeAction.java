package struts.pub.misc;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "jsonMessage"), //
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON) })
public class VerifySponsorMemberCodeAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private MemberService memberService;

    @ToTrim
    @ToUpperCase
    private String sponsorMemberCode;

    public VerifySponsorMemberCodeAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/verifyActiveSponsorMemberCode")
    @Override
    public String execute() throws Exception {
        try {
            VoUtil.toTrimUpperCaseProperties(this);

            Member sponsorMember = memberService.findActiveMemberByMemberCode(sponsorMemberCode);

            if (sponsorMember == null) {
                throw new Exception(getText("invalid_referrer_id"));
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getSponsorMemberCode() {
        return sponsorMemberCode;
    }

    public void setSponsorMemberCode(String sponsorMemberCode) {
        this.sponsorMemberCode = sponsorMemberCode;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
