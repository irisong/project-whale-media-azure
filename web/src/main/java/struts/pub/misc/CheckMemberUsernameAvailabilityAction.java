package struts.pub.misc;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.jquery.validate.CheckAvailable;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.DISPATCHER, location = "/jsp/misc/checkAvailability.jsp"), //
        @Result(name = BaseAction.INPUT, type = BaseAction.ResultType.DISPATCHER, location = "/jsp/misc/checkAvailability.jsp") //
})
public class CheckMemberUsernameAvailabilityAction extends BaseAction implements CheckAvailable {
    private static final long serialVersionUID = 1L;

    private boolean isAvailable = false;

    private Member member = new Member(true);

    @ToTrim
    @ToUpperCase
    private String username;

    private UserDetailsService userDetailsService;

    public CheckMemberUsernameAvailabilityAction() {
        userDetailsService = (UserDetailsService) Application.lookupBean(UserDetailsService.BEAN_NAME);
    }

    public boolean isAvailable() {
        return isAvailable;
    }

    @Action("/checkMemberUsernameAvailability")
    @Override
    public String execute() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this, member);

        if (StringUtils.isBlank(username) && StringUtils.isBlank(member.getMemberCode())) {
            isAvailable = false;
            return SUCCESS;
        }

        if (StringUtils.isNotBlank(username)) {
            isAvailable = userDetailsService.isUsernameAvailable(null, username);
        } else {
            isAvailable = userDetailsService.isUsernameAvailable(null, member.getMemberCode());
        }

        return SUCCESS;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
}
