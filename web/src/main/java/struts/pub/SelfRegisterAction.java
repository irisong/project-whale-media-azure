package struts.pub;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.OptionBeanUtil;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.general.vo.CountryDesc;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.BaseNode;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.member.vo.TreeConstant;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;

@Results(value = { //
        @Result(name = BaseAction.INPUT, location = "selfRegister"), //
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = { "actionName", "selfRegisterInfo", "namespace", "/pub", "username",
                "${username}" }), //
        @Result(name = "selfRegisterInfo", location = "selfRegisterInfo") })
public class SelfRegisterAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    protected MemberService memberService;

    protected Member member = new Member(true);
    protected MemberDetail memberDetail = new MemberDetail(true);

    @ToTrim
    @ToUpperCase
    protected String sponsorId;
    protected String sponsorName;
    protected String password;
    protected String confirmPassword;
    protected String securityPassword;
    protected String confirmSecurityPassword;

    @ToTrim
    @ToUpperCase
    protected String username;

    @ToTrim
    @ToUpperCase
    protected String email2;

    @ToTrim
    @ToUpperCase
    protected String alternateEmail2;
    protected boolean termsRisk;
    protected String signName;
    protected Date date;
    protected String captcha;

    protected List<CountryDesc> countryDescs = new ArrayList<CountryDesc>();
    protected List<OptionBean> genders = new ArrayList<OptionBean>();

    public SelfRegisterAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
    }

    @Action(value = "/selfRegister")
    public String selfRegister() {
        initForm();

        if (isSubmitData()) {
            try {
                VoUtil.toTrimUpperCaseProperties(this, member, memberDetail);

                BaseNode placementInfo = new BaseNode(false);
                placementInfo.setParentId(TreeConstant.NO_PLACEMENT);

                member.setMemberCode(username);

                // no registeredBy, and package id is 0 (no price)
                memberService.doCreateSimpleMember(username, password);

                return SUCCESS;
            } catch (Exception ex) {
                addActionError(ex.getMessage());
            }
        }

        return INPUT;
    }

    protected void initForm() {
        Locale locale = getLocale();
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(locale);

        countryDescs = new ArrayList<>();
        genders = optionBeanUtil.getGendersWithPleaseSelect();
        date = new Date();
    }

    @Action(value = "/selfRegisterInfo")
    public String selfRegisterInfo() {
        successMessage = getText("your_username_is", new String[] { username });
        return "selfRegisterInfo";
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getSponsorId() {
        return sponsorId;
    }

    public void setSponsorId(String sponsorId) {
        this.sponsorId = sponsorId;
    }

    public String getSponsorName() {
        return sponsorName;
    }

    public void setSponsorName(String sponsorName) {
        this.sponsorName = sponsorName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getSecurityPassword() {
        return securityPassword;
    }

    public void setSecurityPassword(String securityPassword) {
        this.securityPassword = securityPassword;
    }

    public String getConfirmSecurityPassword() {
        return confirmSecurityPassword;
    }

    public void setConfirmSecurityPassword(String confirmSecurityPassword) {
        this.confirmSecurityPassword = confirmSecurityPassword;
    }

    public List<CountryDesc> getCountryDescs() {
        return countryDescs;
    }

    public void setCountryDescs(List<CountryDesc> countryDescs) {
        this.countryDescs = countryDescs;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public MemberDetail getMemberDetail() {
        return memberDetail;
    }

    public void setMemberDetail(MemberDetail memberDetail) {
        this.memberDetail = memberDetail;
    }

    public List<OptionBean> getGenders() {
        return genders;
    }

    public void setGenders(List<OptionBean> genders) {
        this.genders = genders;
    }

    public String getEmail2() {
        return email2;
    }

    public void setEmail2(String email2) {
        this.email2 = email2;
    }

    public String getAlternateEmail2() {
        return alternateEmail2;
    }

    public void setAlternateEmail2(String alternateEmail2) {
        this.alternateEmail2 = alternateEmail2;
    }

    public boolean getTermsRisk() {
        return termsRisk;
    }

    public void setTermsRisk(boolean termsRisk) {
        this.termsRisk = termsRisk;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCaptcha() {
        return captcha;
    }

    public void setCaptcha(String captcha) {
        this.captcha = captcha;
    }
}
