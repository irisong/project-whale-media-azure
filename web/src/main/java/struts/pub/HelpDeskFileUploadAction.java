package struts.pub;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.HelpDeskService;
import com.compalsolutions.compal.general.vo.HelpDeskReplyFile;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.vo.JwtToken;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.jose4j.jwt.JwtClaims;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON, params = {"includeProperties", HelpDeskFileUploadAction.DEFAULT_JSON_INCLUDE})
})
public class HelpDeskFileUploadAction extends BaseSecureAction {
    private static final Log log = LogFactory.getLog(HelpDeskFileUploadAction.class);

    public static final String DEFAULT_JSON_INCLUDE = "message";

    private HelpDeskService helpDeskService;
    private TokenProvider tokenProvider;
    private I18n i18n;

    public HelpDeskFileUploadAction() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
        helpDeskService = Application.lookupBean(HelpDeskService.class);
        i18n = Application.lookupBean(I18n.class);
    }

    private String languageCode;
    private String token;

    @ToTrim
    private String subject;

    @ToTrim
    private String message;

    private String ticketId;
    private String ticketTypeId;
    private String fileUploadFileName;
    private String fileUpload2FileName;
    private String fileUpload3FileName;
    private String fileUploadContentType;
    private String fileUpload2ContentType;
    private String fileUpload3ContentType;
    private File fileUpload;
    private File fileUpload2;
    private File fileUpload3;

    private List<HelpDeskReplyFile> uploadImageList = new ArrayList<>();

    @Action("/helpDeskTicketImageUpload")
    public String uploadFile() {
        try {
            HttpServletRequest request = ServletActionContext.getRequest();

            if (StringUtils.isBlank(languageCode)) {
                languageCode = request.getHeader("Content-Language");
            }

            if (StringUtils.isBlank(token)) {
                String bearerToken = request.getHeader("Authorization");
                String[] ss = StringUtils.split(bearerToken);

                if (ss.length != 2) {
                    log.debug("invalid token format " + bearerToken);
                }

                token = ss[1];
            }

            Locale locale = new Locale(languageCode);

            Pair<JwtClaims, JwtToken> pair = tokenProvider.validateJwtToken(locale, token);
            JwtToken jwtToken = pair.getRight();
            User user = jwtToken.getUser();

            VoUtil.toTrimProperties(this);

            //Add uploaded image into a List
            if (fileUpload != null)
                uploadImageList.add(new HelpDeskReplyFile(FileUtils.readFileToByteArray(fileUpload), fileUploadContentType, fileUploadFileName, fileUpload.length(), 1));

            if (fileUpload2 != null)
                uploadImageList.add(new HelpDeskReplyFile(FileUtils.readFileToByteArray(fileUpload2), fileUpload2ContentType, fileUpload2FileName, fileUpload2.length(), 2));

            if (fileUpload3 != null)
                uploadImageList.add(new HelpDeskReplyFile(FileUtils.readFileToByteArray(fileUpload3), fileUpload3ContentType, fileUpload3FileName, fileUpload3.length(), 3));

            helpDeskService.doProcessHelpDeskTicket(locale, user, ticketId, subject, ticketTypeId, message, uploadImageList);
            message = i18n.getText("uploadFileSuccess", locale);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            HttpServletResponse res = ServletActionContext.getResponse();
            res.setStatus(404);
            message = ex.getMessage();
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Collection<String> getErrorMessages() {
        return this.getActionErrors();
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getToken() {
        return token;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTicketId() {
        return ticketId;
    }

    public void setTicketId(String ticketId) {
        this.ticketId = ticketId;
    }

    public String getTicketTypeId() {
        return ticketTypeId;
    }

    public void setTicketTypeId(String ticketTypeId) {
        this.ticketTypeId = ticketTypeId;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getFileUpload2FileName() {
        return fileUpload2FileName;
    }

    public void setFileUpload2FileName(String fileUpload2FileName) {
        this.fileUpload2FileName = fileUpload2FileName;
    }

    public String getFileUpload3FileName() {
        return fileUpload3FileName;
    }

    public void setFileUpload3FileName(String fileUpload3FileName) {
        this.fileUpload3FileName = fileUpload3FileName;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUpload2ContentType() {
        return fileUpload2ContentType;
    }

    public void setFileUpload2ContentType(String fileUpload2ContentType) {
        this.fileUpload2ContentType = fileUpload2ContentType;
    }

    public String getFileUpload3ContentType() {
        return fileUpload3ContentType;
    }

    public void setFileUpload3ContentType(String fileUpload3ContentType) {
        this.fileUpload3ContentType = fileUpload3ContentType;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public File getFileUpload2() {
        return fileUpload2;
    }

    public void setFileUpload2(File fileUpload2) {
        this.fileUpload2 = fileUpload2;
    }

    public File getFileUpload3() {
        return fileUpload3;
    }

    public void setFileUpload3(File fileUpload3) {
        this.fileUpload3 = fileUpload3;
    }

    public List<HelpDeskReplyFile> getUploadImageList() {
        return uploadImageList;
    }

    public void setUploadImageList(List<HelpDeskReplyFile> uploadImageList) {
        this.uploadImageList = uploadImageList;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
