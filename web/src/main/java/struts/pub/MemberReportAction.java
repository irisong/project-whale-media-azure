package struts.pub;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.member.service.MemberReportService;
import com.compalsolutions.compal.member.vo.MemberReportFile;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", MemberReportAction.DEFAULT_JSON_INCLUDE})
})
public class MemberReportAction extends BaseAction {
    private static final Log log = LogFactory.getLog(MemberReportAction.class);

    public static final String DEFAULT_JSON_INCLUDE = "message";

    private TokenProvider tokenProvider;
    private MemberReportService memberReportService;
    private I18n i18n;

    public MemberReportAction() {
        tokenProvider = Application.lookupBean(TokenProvider.class);
        memberReportService = Application.lookupBean(MemberReportService.class);
        i18n = Application.lookupBean(I18n.class);
    }

    private String languageCode;
    private String token;
    private String message;

    @ToTrim
    private String reportTypeId;

    @ToTrim
    private String reportMemberId;

    @ToTrim
    private String description;

    private String fileUploadFileName;
    private String fileUploadContentType;
    private File fileUpload;

    private List<MemberReportFile> uploadImageList = new ArrayList<>();

    @Action("/reportUser")
    public String reportUser() {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse respond = ServletActionContext.getResponse();

        if (StringUtils.isBlank(languageCode)) {
            languageCode = request.getHeader("Content-Language");
        }

        if (StringUtils.isBlank(token)) {
            String bearerToken = request.getHeader("Authorization");
            String[] ss = StringUtils.split(bearerToken);

            if (ss.length != 2) {
                log.debug("invalid token format " + bearerToken);
            }

            token = ss[1];
        }

        try {
            Locale locale = new Locale(languageCode);

            String memberId;
            try { //check token
                memberId = tokenProvider.getMemberIdByToken(locale, token);
            } catch (Exception ex) {
                respond.setStatus(401);
                message = ex.getMessage();
                return JSON;
            }
            VoUtil.toTrimUpperCaseProperties(this);

            //Add uploaded image into a List
            if (fileUpload != null)
                uploadImageList.add(new MemberReportFile(FileUtils.readFileToByteArray(fileUpload), fileUploadContentType, fileUploadFileName, fileUpload.length(), 1));

            memberReportService.doProcessMemberReport(locale, memberId, reportMemberId, reportTypeId, description, uploadImageList);
            message = i18n.getText("statSuccess", locale);
            respond.setStatus(200);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            HttpServletResponse res = ServletActionContext.getResponse();
            res.setStatus(404);
            message = ex.getMessage();
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Collection<String> getErrorMessages() {
        return this.getActionErrors();
    }

    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(String reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    public String getReportMemberId() {
        return reportMemberId;
    }

    public void setReportMemberId(String reportMemberId) {
        this.reportMemberId = reportMemberId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
