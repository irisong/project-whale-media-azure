package struts.pub;

import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.struts.BaseAction;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@Results(value = {//
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", AndroidIapAction.DEFAULT_JSONINCLUDE}) //
})
public class AndroidIapAction extends BaseAction {
    private static final Log log = LogFactory.getLog(IdentityFormAction.class);

    public static final String DEFAULT_JSONINCLUDE = "errorMessages, errorMessages\\[\\d+\\], successMessage";

    public AndroidIapAction() {
    }

    @Action("/androidIap")
    public String androidIap() throws ServletException, IOException {
        try {
            HttpServletRequest request = ServletActionContext.getRequest();

            String requestData = request.getReader().lines().collect(Collectors.joining());
            log.debug(requestData);
            /*
            JSONObject jsonObject = new JSONObject(requestData);
            log.debug(jsonObject.toString());

            IosPaymentClient client = new IosPaymentClient();
            client.iosInAppPurchase(jsonObject.toString());*/
            return JSON;


        } catch (Exception e) {
            addActionError(e.getMessage());
        }

        return JSON;
    }
    // ---------------- GETTER & SETTER (START) -------------

    // ---------------- GETTER & SETTER (END) -------------
}
