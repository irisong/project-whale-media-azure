package struts.pub;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.general.service.MessageService;
import com.compalsolutions.compal.general.vo.Message;
import com.compalsolutions.compal.struts.BaseAction;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import java.util.Date;
import java.util.Locale;

@Results(value = {
        @Result(name = "input", location = "webView")
})
public class WebViewAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private MessageService messageService;
    private I18n i18n;

    private Message message = new Message();

    private String messageId;
    private String languageCode;
    private String content;
    private Date date;
    private String title;
//    private String imageUrl;
//    private boolean containImage = false;
    private String contentAlignment = "text-center";

    public WebViewAction() {
        messageService = Application.lookupBean(MessageService.class);
        i18n = Application.lookupBean(I18n.class);
    }

    @Override
    @Action("/webView")
    public String execute() throws Exception {
        if (StringUtils.isBlank(languageCode)) {
            languageCode = Global.Language.ENGLISH;
        }
        if (StringUtils.isNotBlank(messageId)) {
            Locale locale = new Locale(languageCode);
            message = messageService.findMessageByMessageId(messageId);
            if (message != null) {
                content = message.getContent();
                title = message.getTitle();
                date = message.getSentDate();
//                imageUrl = message.getImageUrl();
//                if (StringUtils.isNotBlank(imageUrl)) {
//                    containImage = true;
//                }
                if (locale.equals(Global.LOCALE_CN)) {
                    content = message.getContentCn();
                    title = message.getTitleCn();
                }
            }
        } else {
            title = getText("errorMessage.error404");
            content = getText("errorMessage.message.not.found");
        }

        return INPUT;
    }

    @Action("/fansBadgeInfo")
    public String getFansBadgeInfo() {
        if (StringUtils.isBlank(languageCode)) {
            languageCode = Global.Language.ENGLISH;
        }

        contentAlignment = "text-left";
//        content = getText("levelDesc");
        content = i18n.getText("fansBadgeInfoContent", new Locale(languageCode));

        return INPUT;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

//    public String getImageUrl() {
//        return imageUrl;
//    }
//
//    public void setImageUrl(String imageUrl) {
//        this.imageUrl = imageUrl;
//    }
//
//    public boolean isContainImage() {
//        return containImage;
//    }
//
//    public void setContainImage(boolean containImage) {
//        this.containImage = containImage;
//    }

    public String getContentAlignment() {
        return contentAlignment;
    }

    public void setContentAlignment(String contentAlignment) {
        this.contentAlignment = contentAlignment;
    }
}
