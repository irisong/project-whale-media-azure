package struts.pub;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.merchant.dto.MerchantOrderDto;
import com.compalsolutions.compal.merchant.service.MerchantOrderService;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.Collection;
import java.util.Locale;

@Results(value = {
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON,
                params = {"includeProperties", FileUploadAction.DEFAULT_JSON_INCLUDE})
})
public class FileUploadAction extends BaseAction {
    private static final Log log = LogFactory.getLog(FileUploadAction.class);

    public static final String DEFAULT_JSON_INCLUDE = "message, dto, dto.orderId, "
            + "dto.orderRefNo, "
            + "dto.orderStatus, "
            + "dto.orderDatetime, "
            + "dto.expiryDatetime, "
            + "dto.address, "
            + "dto.address.contactName, "
            + "dto.address.contactNo, "
            + "dto.address.longAddress, "
            + "dto.productDetailList, "
            + "dto.productDetailList\\[\\d+\\], "
            + "dto.productDetailList\\[\\d+\\].productDetId, "
            + "dto.productDetailList\\[\\d+\\].productName, "
            + "dto.productDetailList\\[\\d+\\].variant, "
            + "dto.productDetailList\\[\\d+\\].currencyCode, "
            + "dto.productDetailList\\[\\d+\\].price, "
            + "dto.productDetailList\\[\\d+\\].productQty, "
            + "dto.productDetailList\\[\\d+\\].fileUrl, "
            + "dto.currencyCode, "
            + "dto.orderTotal, "
            + "dto.shippingFee, "
            + "dto.grandTotal";

    private I18n i18n;
    private TokenProvider tokenProvider;
    private MerchantOrderService merchantOrderService;

    public FileUploadAction() {
        i18n = Application.lookupBean(I18n.class);
        tokenProvider = Application.lookupBean(TokenProvider.class);
        merchantOrderService = Application.lookupBean(MerchantOrderService.class);
    }

    private String languageCode;
    private String token;
    private String message;

    @ToTrim
    private String orderId;

    @ToTrim
    private String referenceNo;

    private String fileUploadFileName;
    private String fileUploadContentType;
    private File fileUpload;

    private MerchantOrderDto dto;

    @Action("/merchantPaymentUpload")
    public String reportUser() {
        HttpServletRequest request = ServletActionContext.getRequest();
        HttpServletResponse respond = ServletActionContext.getResponse();

        if (StringUtils.isBlank(languageCode)) {
            languageCode = request.getHeader("Content-Language");
        }

        if (StringUtils.isBlank(token)) {
            String bearerToken = request.getHeader("Authorization");
            String[] ss = StringUtils.split(bearerToken);

            if (ss.length != 2) {
                log.debug("invalid token format " + bearerToken);
            }

            token = ss[1];
        }

        try {
            Locale locale = new Locale(languageCode);

            try { //check token
                tokenProvider.getMemberIdByToken(locale, token);
            } catch (Exception ex) {
                respond.setStatus(401);
                message = ex.getMessage();
                return JSON;
            }

            VoUtil.toTrimUpperCaseProperties(this);
            dto = merchantOrderService.doUploadMerchantBankInSlip(locale, orderId, referenceNo, fileUpload, fileUploadFileName, fileUploadContentType);

            message = i18n.getText("statSuccess", locale);
            respond.setStatus(200);
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            HttpServletResponse res = ServletActionContext.getResponse();
            res.setStatus(404);
            message = ex.getMessage();
        }

        return JSON;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public Collection<String> getErrorMessages() {
        return this.getActionErrors();
    }

    public String getToken() {
        return token;
    }

    @Override
    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public File getFileUpload() {
        return fileUpload;
    }

    public void setFileUpload(File fileUpload) {
        this.fileUpload = fileUpload;
    }

    public String getFileUploadFileName() {
        return fileUploadFileName;
    }

    public void setFileUploadFileName(String fileUploadFileName) {
        this.fileUploadFileName = fileUploadFileName;
    }

    public String getFileUploadContentType() {
        return fileUploadContentType;
    }

    public void setFileUploadContentType(String fileUploadContentType) {
        this.fileUploadContentType = fileUploadContentType;
    }

    public MerchantOrderDto getDto() {
        return dto;
    }

    public void setDto(MerchantOrderDto dto) {
        this.dto = dto;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
