package struts.pub;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;

import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.security.SecurityUtil;
import com.compalsolutions.compal.security.UserDetails;
import com.compalsolutions.compal.struts.BaseAction;

@Results(value = { //
        @Result(name = BaseAction.SUCCESS, location = "accessDenied"), //
        @Result(name = "noLogin", type = "redirect", location = "/") })
public class AccessDeniedAction extends BaseAction {
    private static final long serialVersionUID = 1L;

    private boolean isAdmin;
    private boolean isMember;
    private boolean isAgent;
    private String username;

    @Action("/access-denied")
    @Override
    public String execute() throws Exception {
        UserDetails userDetails = SecurityUtil.getLoginUser();

        // Anonymous, redirect to root path
        if (userDetails == null) {
            return "noLogin";
        }

        if (userDetails instanceof User) {
            User user = (User) userDetails;
            username = user.getUsername();
            isAdmin = Global.UserType.HQ.equalsIgnoreCase(user.getUserType());
            isAgent = Global.UserType.AGENT.equalsIgnoreCase(user.getUserType());
            isMember = Global.UserType.MEMBER.equalsIgnoreCase(user.getUserType());
        }

        return super.execute();
    }

    // ---------------- GETTER & SETTER (START) -------------

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isMember() {
        return isMember;
    }

    public void setMember(boolean member) {
        isMember = member;
    }

    public boolean isAgent() {
        return isAgent;
    }

    public void setAgent(boolean agent) {
        isAgent = agent;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    // ---------------- GETTER & SETTER (END) -------------
}
