package struts.pub;

import com.compalsolutions.compal.*;
import com.compalsolutions.compal.agent.service.AgentService;
import com.compalsolutions.compal.agent.vo.Agent;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.broadcast.service.BroadcastService;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuild;
import com.compalsolutions.compal.broadcast.vo.BroadcastGuildMember;
import com.compalsolutions.compal.crypto.CryptoProvider;
import com.compalsolutions.compal.exception.ValidatorException;
import com.compalsolutions.compal.function.language.service.I18n;
import com.compalsolutions.compal.function.language.vo.Language;
import com.compalsolutions.compal.function.user.service.UserDetailsService;
import com.compalsolutions.compal.function.user.vo.AgentUserType;
import com.compalsolutions.compal.function.user.vo.LoginInfo;
import com.compalsolutions.compal.function.user.vo.User;
import com.compalsolutions.compal.general.service.CountryService;
import com.compalsolutions.compal.general.service.DocNoService;
import com.compalsolutions.compal.general.vo.SystemConfig;
import com.compalsolutions.compal.member.service.CastingService;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Casting;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.member.vo.MemberDetail;
import com.compalsolutions.compal.omnicplus.OmnicplusClient;
import com.compalsolutions.compal.payment.dto.TopupOptionInfoDto;
import com.compalsolutions.compal.payment.dto.TopUpWalletTypeInfoDto;
import com.compalsolutions.compal.paymentGateway.dto.RazerPayResponse;
import com.compalsolutions.compal.paymentGateway.service.PaymentGatewayService;
import com.compalsolutions.compal.paymentGateway.vo.RazerTopup;
import com.compalsolutions.compal.security.TokenProvider;
import com.compalsolutions.compal.security.annotation.Access;
import com.compalsolutions.compal.security.annotation.Accesses;
import com.compalsolutions.compal.struts.BaseAction;
import com.compalsolutions.compal.struts.BaseSecureAction;
import com.compalsolutions.compal.struts.bean.OptionBean;
import com.compalsolutions.compal.user.service.UserService;
import com.compalsolutions.compal.user.vo.AgentUser;
import com.compalsolutions.compal.util.VoUtil;
import com.compalsolutions.compal.vo.annotation.ToTrim;
import com.compalsolutions.compal.vo.annotation.ToUpperCase;
import com.compalsolutions.compal.wallet.service.WalletService;
import com.compalsolutions.compal.web.security.BaseSecurityUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.I18nInterceptor;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import rest.ResponseBuilder;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Results(value = { //
        @Result(name = BaseAction.PAGE1, location = "wl_index"),
        @Result(name = BaseAction.PAGE2, location = "wl_live"),
        @Result(name = BaseAction.PAGE3, location = "wl_privacy_policy"),
        @Result(name = BaseAction.PAGE4, location = "wl_term"),
        @Result(name = BaseAction.PAGE5, location = "wl_contact_us"),
        @Result(name = BaseAction.PAGE6, location = "wl_about_us"),
        @Result(name = BaseAction.PAGE7, location = "wl_legal"),
        @Result(name = BaseAction.PAGE8, location = "wl_copyright"),
        @Result(name = BaseAction.PAGE9, location = "wl_community"),
        @Result(name = WhaleMediaWebpageAction.PAGE10, location = "wl_content_management"),
        @Result(name = WhaleMediaWebpageAction.PAGE11, location = "wl_razer_pay"),
        @Result(name = WhaleMediaWebpageAction.PAGE12, location = "wl_recharge_success"),
        @Result(name = WhaleMediaWebpageAction.PAGE13, location = "wl_download"),

        @Result(name = WhaleMediaWebpageAction.PAGE14, location = "wl_login"),
        @Result(name = WhaleMediaWebpageAction.PAGE15, location = "wl_guild"),
        @Result(name = WhaleMediaWebpageAction.PAGE16, location = "wl_change_password"),

        @Result(name = WhaleMediaWebpageAction.PAGE17, location = "wl_casting"),
        @Result(name = WhaleMediaWebpageAction.PAGE18, location = "wl_casting_success"),
        @Result(name = WhaleMediaWebpageAction.PAGE19, location = "wl_income_report"),
        @Result(name = WhaleMediaWebpageAction.PAGE20, location = "wl_change_profile"),
        @Result(name = WhaleMediaWebpageAction.PAGE21, location = "wl_guild_casting"),
        @Result(name = WhaleMediaWebpageAction.PAGE22, location = "wl_guild_casting_view"),
        @Result(name = WhaleMediaWebpageAction.PAGE23, location = "wl_guild_casting_edit"),
        @Result(name = BaseAction.INPUT, location = "wl_recharge"),
        @Result(name = BaseAction.SHOW, location = "wl_recharge"),
        @Result(name = BaseAction.JSON, type = BaseAction.ResultType.JSON),
        @Result(name = BaseAction.SUCCESS, type = BaseAction.ResultType.REDIRECT, params = {"actionName", "successMessage", "namespace", "/member",
                "successMessage", "${successMessage}", "successMenuKey", "${successMenuKey}"}) //
})
public class WhaleMediaWebpageAction extends BaseSecureAction implements ServletRequestAware, ServletResponseAware {
    private static final Log log = LogFactory.getLog(WhaleMediaWebpageAction.class);
    private static final long serialVersionUID = 1L;
    public static final String PAGE10 = "PAGE10";
    public static final String PAGE11 = "PAGE11";
    public static final String PAGE12 = "PAGE12";
    public static final String PAGE13 = "PAGE13";
    public static final String PAGE14 = "PAGE14";

    public static final String PAGE15 = "PAGE15";
    public static final String PAGE16 = "PAGE16";
    public static final String PAGE17 = "PAGE17";
    public static final String PAGE18 = "PAGE18";
    public static final String PAGE19 = "PAGE19";
    public static final String PAGE20 = "PAGE20";
    public static final String PAGE21 = "PAGE21";
    public static final String PAGE22 = "PAGE22";
    public static final String PAGE23 = "PAGE23";

    @ToTrim
    private String password;

    @ToTrim
    private String contactPerson;

    private BigDecimal balance;

    protected User user;

    //For Top-up modal
    @ToUpperCase
    @ToTrim
    private String phonoNoTopUp;

    @ToUpperCase
    @ToTrim
    private String paymentOption_topUp;

    @ToTrim
    private BigDecimal whaleCoinAmount_topUp;

    @ToTrim
    private BigDecimal fromWalletAmt_topUp;

    @ToTrim
    private String transactionPassword;


    private String agentId;

    private String insert_errorMessage;

    private String login_errorMessage;
    private String topUp_errorMessage;

    private String displayCode;


    private MemberService memberService;
    private UserDetailsService userDetailsService;
    private UserService userService;
    private AgentService agentService;
    private CountryService countryService;
    private CryptoProvider cryptoProvider;
    private TokenProvider tokenProvider;
    private WalletService walletService;
    private PaymentGatewayService paymentGatewayService;
    private DocNoService docNoService;
    private BroadcastService broadcastService;
    private CastingService castingService;
    private String returnUrl;
    private String paymentGateWayUrl;
    private String userType;

    private String language;
    private Locale systemLocale;
    private List<OptionBean> languages = new ArrayList<OptionBean>();
    private List<TopupOptionInfoDto> topUpOptionInfoDtos = new ArrayList<>();
    private Boolean showBalanceAndTopUpOption;
    private Member member = new Member();
    private MemberDetail memberDetail = new MemberDetail();
    private Agent agent = new Agent();

    private HttpServletResponse httpServletResponse;
    private HttpServletRequest httpServletRequest;
    private I18n i18n;

    // callback response param
    private String tranID;
    private String orderid;
    private String status;
    private String domain;
    private String nbcb;
    private String amount;
    private String currency;
    private String skey;
    private String paydate;
    private String appcode;
    private String error_code;
    private String error_desc;

    protected String oldPassword;
    protected String newPassword;
    protected String confirmPassword;

    private String whaleLiveId;

    @ToUpperCase
    @ToTrim
    private String fullName;

    @ToUpperCase
    @ToTrim
    private String phoneNo;

    @ToTrim
    private String gender;

    @ToTrim
    private String email;

    @ToTrim
    private String age;

    @ToTrim
    private String talent;

    @ToTrim
    private String url;

    @ToTrim
    private String tiktokId;

    @ToTrim
    private String fbId;

    @ToTrim
    private String igId;

    @ToTrim
    private String weiboId;

    @ToTrim
    private String identityNo;

    private String testLiveTimestamp;

    private String experience;

    @ToTrim
    private String phoneModel;

    @ToTrim
    private String location;

    @ToTrim
    private Integer tiktokFans;

    @ToTrim
    private Integer igFans;

    @ToTrim
    private Integer weiboFans;

    @ToTrim
    private String id;


    private BroadcastGuild broadcastGuild = new BroadcastGuild(true);
    private Casting casting = new Casting(true);

    private List<OptionBean> allGender = new ArrayList<>();
    private List<OptionBean> allGuildRankList = new ArrayList<>();
    private List<OptionBean> allMonthList = new ArrayList<>();
    private List<OptionBean> allYearList = new ArrayList<>();
    private List<OptionBean> getGender = new ArrayList<>();

    public WhaleMediaWebpageAction() {
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        userService = Application.lookupBean(UserService.BEAN_NAME, UserService.class);
        agentService = Application.lookupBean(AgentService.BEAN_NAME, AgentService.class);
        userDetailsService = Application.lookupBean(UserDetailsService.BEAN_NAME, UserDetailsService.class);
        i18n = Application.lookupBean(I18n.class);
        systemLocale = Application.lookupBean("systemLocale", Locale.class);
        countryService = Application.lookupBean(CountryService.BEAN_NAME, CountryService.class);
        cryptoProvider = Application.lookupBean(CryptoProvider.BEAN_NAME, CryptoProvider.class);
        walletService = Application.lookupBean(WalletService.BEAN_NAME, WalletService.class);
        tokenProvider = Application.lookupBean(TokenProvider.BEAN_NAME, TokenProvider.class);
        paymentGatewayService = Application.lookupBean(PaymentGatewayService.BEAN_NAME, PaymentGatewayService.class);
        docNoService = Application.lookupBean(DocNoService.BEAN_NAME, DocNoService.class);
        broadcastService = Application.lookupBean(BroadcastService.BEAN_NAME, BroadcastService.class);
        castingService = Application.lookupBean(CastingService.BEAN_NAME, CastingService.class);
    }

    private void initialize() {
        OptionBeanUtil optionBeanUtil = new OptionBeanUtil(getLocale());
        allGender = optionBeanUtil.getGendersWithPleaseSelect();
        allGuildRankList = optionBeanUtil.getMemberRankWithPleaseSelect();
        allMonthList = optionBeanUtil.getMonthWithOptionAll();
        allYearList = optionBeanUtil.getYearWithOptionAll();
    }

    @Action(value = "/wl_index")
    public String whale_media_index_page() throws Exception {
        return PAGE1;
    }

    @Action(value = "/wl_razer_pay")
    public String whale_media_razer_pay() throws Exception {
        RazerPayResponse razerPayResponse = new RazerPayResponse();
        razerPayResponse.setOrderid(getOrderid());
        razerPayResponse.setStatus(getStatus());
        razerPayResponse.setNbcb(getNbcb());
        razerPayResponse.setAmount(getAmount());
        razerPayResponse.setCurrency(getCurrency());
        razerPayResponse.setSkey(getSkey());
        razerPayResponse.setPaydate(getPaydate());
        razerPayResponse.setAppcode(getAppcode());
        razerPayResponse.setError_code(getError_code());
        razerPayResponse.setError_desc(getError_desc());
        razerPayResponse.setTranID(getTranID());

        log.debug("razerPayResponse callback====" + razerPayResponse.toString());
        paymentGatewayService.doTopUpWhaleCoinUsingRazerPayByWebpage(getLocale(), razerPayResponse);
        if (!String.valueOf(razerPayResponse.getStatus()).equals("11")) {
            return PAGE12;
        } else {
            return PAGE11;
        }
    }

    @Action(value = "/wl_live")
    public String whale_media_live_page() throws Exception {
        return PAGE2;
    }

    @Action(value = "/wl_recharge")
    public String whale_media_recharge_page() {
        log.debug("Username : " + phoneNo);
        final String userType = Global.UserType.MEMBER;
        try {

            if(StringUtils.isNotBlank(whaleLiveId)) {
                phoneNo = whaleLiveId;
            }
            showBalanceAndTopUpOption = false;
            displayCode = phoneNo;
            if (StringUtils.isNotBlank(phoneNo)) {

                member = memberService.findMemberByMemberCodeOrWhaleliveId(phoneNo);
                if (member == null) {
                    throw new ValidatorException("invalidMember", getLocale());
                }

                phoneNo = member.getMemberCode();
                member.setDisplayId(displayCode);
                member.setDisplayProfileName(member.getMemberDetail().getProfileName());
                //tokenProvider.checkIsValidUser(getLocale(), phoneNo, password);

                balance = walletService.doCreateWalletBalanceIfNotExists(member.getMemberId(), userType, Global.WalletType.WALLET_80).getAvailableBalance();
                List<String> touUpOption = Arrays.asList(Global.TopUpOptionType.RAZER, Global.TopUpOptionType.OMC);
            //    touUpOption = Arrays.asList(Global.TopUpOptionType.OMC);

                Map<Double, Double> goldCoin2Omc = paymentGatewayService.goldCoin2Omc();
                Map<Double, Double> goldCoin2OmcPromo = paymentGatewayService.goldCoin2OmcPromo();

                Map<Double, Double> goldCoin2Myr = paymentGatewayService.goldCoin2Myr();
                Map<Double, Double> goldCoin2MyrPromo = paymentGatewayService.goldCoin2MyrPromo();
                String serverUrl = cryptoProvider.getServerUrl();

                topUpOptionInfoDtos = touUpOption.stream()
                        .map(t -> {
                            TopupOptionInfoDto topupInfo = new TopupOptionInfoDto();
                            List<TopUpWalletTypeInfoDto> walletTypeInfoDtos = new ArrayList<>();

                            topupInfo.setPaymentOption(t);

                            if (Global.TopUpOptionType.RAZER.equalsIgnoreCase(t)) {
                                topupInfo.setPaymentOptionName(i18n.getText(Global.TopUpOptionType.RAZER, getLocale()));
                                topupInfo.setIconUrl(serverUrl + "/asset/whale_media/asset/payment/1.png");

                                for (Map.Entry<Double, Double> entry : goldCoin2Myr.entrySet()) {
                                    TopUpWalletTypeInfoDto walletTypeInfoDto = new TopUpWalletTypeInfoDto();
                                    Double freeCoin = goldCoin2MyrPromo.get(entry.getKey());
                                    walletTypeInfoDto.setTopUpOption(Global.TopUpOptionType.RAZER);
                                    walletTypeInfoDto.setWhaleCoinAmt(entry.getKey());
                                    walletTypeInfoDto.setFromWalletAmount(entry.getValue());
                                    String optionDesc = entry.getKey().intValue() + " = MYR "
                                            + SysFormatter.ensureTwoDecimal(entry.getValue()) + " (" + i18n.getText("giveAwayExtraCoin", getLocale(),
                                            SysFormatter.ensureTwoDecimal(freeCoin)) + ")";

                                    String promoteDesc = i18n.getText("giveAwayExtraCoin", getLocale(), SysFormatter.ensureTwoDecimal(freeCoin));
                                    String walletAmountDesc = "MYR "+SysFormatter.ensureTwoDecimal(entry.getValue());
                                    walletTypeInfoDto.setOption(optionDesc);
                                    walletTypeInfoDto.setPromoteDesc(promoteDesc);
                                    walletTypeInfoDto.setWalletAmountDesc(walletAmountDesc);
                                    walletTypeInfoDtos.add(walletTypeInfoDto);
                                }
                            } else if (Global.WalletType.OMC.equalsIgnoreCase(t)) {
                                topupInfo.setPaymentOptionName(i18n.getText(Global.TopUpOptionType.OMC, getLocale()));
                                topupInfo.setIconUrl(serverUrl + "/asset/image/coin/omc.png");

                                for (Map.Entry<Double, Double> entry : goldCoin2Omc.entrySet()) {
                                    TopUpWalletTypeInfoDto walletTypeInfoDto = new TopUpWalletTypeInfoDto();
                                    Double freeCoinPercent = goldCoin2OmcPromo.get(entry.getKey());
                                    walletTypeInfoDto.setTopUpOption(Global.TopUpOptionType.OMC);
                                    walletTypeInfoDto.setWhaleCoinAmt(entry.getKey());
                                    OmnicplusClient omnicplusClient = new OmnicplusClient();
                                    BigDecimal cryptoRate = BDUtil.roundUp4dp(omnicplusClient.getOmcCoinRealtimeRate());
                                    BigDecimal cryptoAmt = BDUtil.roundUp4dp(new BigDecimal("" + entry.getValue()).divide(cryptoRate, 8, RoundingMode.HALF_UP));

                                    String walletAmountDesc = BDUtil.roundUp4dp(cryptoAmt)+ " OMC";

                                    String optionDesc = entry.getKey().intValue() + " ≈ " + BDUtil.roundUp4dp(cryptoAmt)
                                            + " " + "OMC" + " (" + i18n.getText("giveAwayExtraCoin", getLocale(),
                                            SysFormatter.ensureTwoDecimal(freeCoinPercent)) + ")";

                                    String promoteDesc = i18n.getText("giveAwayExtraCoin", getLocale(), SysFormatter.ensureTwoDecimal(freeCoinPercent));


                                    String rateDesc = "(" + entry.getValue() + " " + "USD" + " " + " , "
                                            + i18n.getText("estimatedRate", getLocale()) + ": " + cryptoRate + ")";

                                    walletTypeInfoDto.setOption(optionDesc);
                                    walletTypeInfoDto.setRateDesc(rateDesc);
                                    walletTypeInfoDto.setFromWalletAmount(cryptoAmt.doubleValue());
                                    walletTypeInfoDto.setPromoteDesc(promoteDesc);
                                    walletTypeInfoDto.setWalletAmountDesc(walletAmountDesc);
                                    walletTypeInfoDtos.add(walletTypeInfoDto);
                                }
                            }

                            walletTypeInfoDtos = walletTypeInfoDtos.stream()
                                    .sorted(Comparator.comparingDouble(TopUpWalletTypeInfoDto::getWhaleCoinAmt))
                                    .collect(Collectors.toList());

                            topupInfo.setTopupWalletTypeInfoDtoList(walletTypeInfoDtos);

                            return topupInfo;
                        })
                        .collect(Collectors.toList());

                showBalanceAndTopUpOption = true;

                successMessage = getText("successful.webpage.login");
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            login_errorMessage = getActionErrors().toArray()[0].toString();
        }
        return SHOW;
    }

    @Action(value = "/addCasting")
    public String whale_media_add_casting_page() throws Exception {
        try {

            if(StringUtils.isNotBlank(phoneNo) && StringUtils.isNotBlank(whaleLiveId)){
                member = memberService.findMemberByMemberCodeOrWhaleliveId(whaleLiveId);
                if (member == null) {
                    throw new ValidatorException("Invalid Whalelive Id", getLocale());
                }
                BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberApproved(member.getMemberId());
                if(broadcastGuildMember!=null)
                {
                    throw new ValidatorException("Member in other guild", getLocale());
                }


                Casting duplicateCasting = castingService.getCastingByWhaleliveId(whaleLiveId);
                if (duplicateCasting != null) {
                    throw new ValidatorException("Duplicate casting request", getLocale());
                }
                else {
                    casting.setWhaleLiveId(whaleLiveId);
                    casting.setFullName(fullName);
                    casting.setIdentityNo(identityNo);
                    casting.setPhoneNo(phoneNo);
                    casting.setGender(gender);
                    casting.setEmail(email);
                    casting.setAge(age);
                    casting.setTalent(talent);
                    casting.setUrl(url);
                    casting.setTiktokId(tiktokId);
                    casting.setFbId(fbId);
                    casting.setIgId(igId);
                    memberService.doCreateCasting(casting);
                }
            }else
            {
                throw new ValidatorException("Please insert required field", getLocale());
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            insert_errorMessage = getActionErrors().toArray()[0].toString();
        }
        return JSON;
    }

    @Action(value = "/guildAddCasting")
    public String whale_media_add_guild_casting_page() throws Exception {
        try {
            LoginInfo loginInfo = getLoginInfo();

            if (loginInfo.getUserType() instanceof AgentUserType) {
                agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
            }

            if(StringUtils.isNotBlank(phoneNo) && StringUtils.isNotBlank(whaleLiveId)){
                member = memberService.findMemberByMemberCodeOrWhaleliveId(whaleLiveId);
                if (member == null) {
                    throw new ValidatorException("Invalid Whalelive Id", getLocale());
                }
                BroadcastGuildMember broadcastGuildMember = broadcastService.findGuildMemberApproved(member.getMemberId());
                if(broadcastGuildMember!=null)
                {
                    throw new ValidatorException(i18n.getText("memberAlreadyJoinOtherGuild", getLocale()));
                }


                Casting duplicateCasting = castingService.getCastingByWhaleliveId(whaleLiveId);
                if (duplicateCasting != null) {
                    throw new ValidatorException("Duplicate casting request", getLocale());
                }
                else {
                    casting.setWhaleLiveId(whaleLiveId);
                    casting.setFullName(fullName);
                    casting.setIdentityNo(identityNo);
                    casting.setPhoneNo(phoneNo);
                    casting.setGender(gender);
                    casting.setEmail(email);
                    casting.setAge(age);
                    casting.setTalent(talent);
                    casting.setUrl(url);
                    casting.setTiktokId(tiktokId);
                    casting.setFbId(fbId);
                    casting.setIgId(igId);

                    if(experience == null) {
                        casting.setExperience(false);
                    }
                    else {
                        casting.setExperience(true);
                    }


                    broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);
                    casting.setGuildId(broadcastGuild.getId());
                    casting.setIgFan(igFans);
                    casting.setLocation(location);
                    casting.setPhoneModel(phoneModel);

                    DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
                    Date date = (Date)formatter.parse(testLiveTimestamp);

                    casting.setTestLiveDate(date);
                    casting.setWeiboId(weiboId);
                    casting.setTiktokFan(tiktokFans);
                    casting.setWeiboFan(weiboFans);

                    memberService.doCreateCasting(casting);


                    BroadcastGuildMember pendingApprovalMember = broadcastService.findMemberInPending(member.getMemberId(), broadcastGuild.getId());
                    if (pendingApprovalMember == null) {
                        broadcastService.doAddDeleteGuildMemberFromAPI(member.getMemberId(), broadcastGuild.getId(), true);
                    }
                }

                //send notification email
                String title = ("Whale Media Notification: Register casting");
                String body = ("[WHALE MEDIA]" + member.getMemberCode() + ": " + member.getMemberDetail().getProfileName() + " has registered as casting and pending for your approval.");
                memberService.doSendDifferentNotificationEmailType(Global.EmailType.CASTING, broadcastGuild.getId(), null, null, title, body);

            }else
            {
                throw new ValidatorException("Please insert required field", getLocale());
            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            insert_errorMessage = getActionErrors().toArray()[0].toString();
        }
        return JSON;
    }

    @Action(value = "/topUpWhaleCoin")
    public String topUpWhaleCoin() {
        try {
            if (StringUtils.isBlank(paymentOption_topUp)) {
                throw new ValidatorException(i18n.getText("invalidTopUpOption", getLocale()));
            }

            if (Global.WalletType.OMC.equalsIgnoreCase(paymentOption_topUp)) {
                tokenProvider.checkIsValidUser(getLocale(), phoneNo, password);
                walletService.doTopUpWhaleCoinUsingCryptoByWebpage(getLocale(), paymentOption_topUp, phoneNo, transactionPassword, whaleCoinAmount_topUp);
            }

            successMessage = i18n.getText("successful.topup");
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            topUp_errorMessage = getActionErrors().toArray()[0].toString();
        }

        return JSON;
    }

    @Action(value = "/razerPayRequest")
    public String topUpWhaleCoinViaRazor() {
        try {
            if (StringUtils.isBlank(paymentOption_topUp)) {
                throw new ValidatorException(i18n.getText("invalidTopUpOption", getLocale()));
            }
            if (StringUtils.isBlank(phoneNo)) {
                throw new ValidatorException(i18n.getText("invalidMember", getLocale()));
            }
            Member member = memberService.findMemberByMemberCode(phoneNo);
            if (member == null) {
                throw new ValidatorException(i18n.getText("invalidMember", getLocale()));
            }

            if (Global.TopUpOptionType.RAZER.equalsIgnoreCase(paymentOption_topUp)) {
                RazerTopup razerTopup = paymentGatewayService.doTopUpWhaleCoinUsingRazerPayByWebpage(
                        getLocale(), member.getMemberId(), whaleCoinAmount_topUp, fromWalletAmt_topUp);
                paymentGateWayUrl = "https://www.onlinepayment.com.my/MOLPay/pay/whalemediamy/"
                        + "?amount=" + razerTopup.getAmount() + "&orderid=" + razerTopup.getOrderId() +
                        "&vcode=" + razerTopup.getVcode();
                returnUrl = cryptoProvider.getServerUrl() + "/pub/whale_media/wl_live.php";
            }
            successMessage = i18n.getText("successful.topup");
        } catch (Exception ex) {
            log.debug(ex.getMessage());
            addActionError(ex.getMessage());
            topUp_errorMessage = getActionErrors().toArray()[0].toString();
        }
        return JSON;
    }

    @Action(value = "/initLangWhaleLivePage")
    public String initLang() throws Exception {
        VoUtil.toTrimUpperCaseProperties(this);
        BaseSecurityUtil.setLoginUrlToCookies(httpServletResponse, Global.UserType.HQ);
        initLanguage();
        return JSON;
    }

    private void initLanguage() {
        if (StringUtils.isBlank(language)) {
            language = getLanguageCodeFromCookies();
        }
        if (StringUtils.isBlank(language)) {
            language = systemLocale.getLanguage();
        }

        List<Language> tempLanguages = WebUtil.getLanguages();
        for (Language language : tempLanguages) {
            languages.add(new OptionBean(language.getLanguageCode(), language.getLanguageName()));
        }

        setLanguageCodeToCookies();
        Locale locale = new Locale(language);
        session.put(I18nInterceptor.DEFAULT_SESSION_ATTRIBUTE, locale);
    }

    private void setLanguageCodeToCookies() {
        Cookie cookie = new Cookie(Global.USER_LANGUAGE_COOKIES, language);
        cookie.setPath("/");
        cookie.setMaxAge(-1); // never expire
        httpServletResponse.addCookie(cookie);
    }

    private String getLanguageCodeFromCookies() {
        Cookie[] cookies = httpServletRequest.getCookies();

        if (cookies == null)
            return null;

        for (Cookie cookie : cookies) {
            if (Global.USER_LANGUAGE_COOKIES.equals(cookie.getName()))
                return cookie.getValue();
        }
        return null;
    }

    @Action(value = "/wl_privacy_policy")
    public String whale_media_policy_page() {
        return PAGE3;
    }

    @Action(value = "/wl_term")
    public String whale_media_term_page() throws Exception {
        return PAGE4;
    }

    @Action(value = "/wl_contact_us")
    public String whale_media_contact_page() throws Exception {
        return PAGE5;
    }

    @Action(value = "/wl_about_us")
    public String whale_media_about_us_page() throws Exception {
        return PAGE6;
    }

    @Action(value = "/wl_legal")
    public String whale_media_legal_page() throws Exception {
        return PAGE7;
    }

    @Action(value = "/wl_copyright")
    public String whale_media_copyright_page() throws Exception {
        return PAGE8;
    }

    @Action(value = "/wl_community")
    public String whale_media_community_page() {
        return PAGE9;
    }

    @Action(value = "/wl_content_management")
    public String whale_media_content_management_page() {
        return PAGE10;
    }

    @Action(value = "/wl_recharge_success")
    public String whale_media_recharge_success_page() throws Exception {
        return PAGE12;
    }

    @Action(value = "/wl_download")
    public String whale_media_download_page() throws Exception {
        return PAGE13;
    }

    @Action(value = "/wl_login")
    public String whale_media_login_page() throws Exception {
        BaseSecurityUtil.setLoginUrlToCookies(httpServletResponse, Global.UserType.GM);

        return PAGE14;
    }

    @Action(value = "/wl_guild")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String whale_media_guild_page() throws Exception {
        initialize();
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

        return PAGE15;
    }

    @Action(value = "/wl_change_password")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String whale_media_change_password_page() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        user = loginInfo.getUser();

        return PAGE16;
    }

    @Action(value = "/wl_casting")
    public String whale_media_casting_page() throws Exception {
        try {

            if(StringUtils.isNotBlank(phoneNo) && StringUtils.isNotBlank(whaleLiveId) && StringUtils.isNotBlank(url) && StringUtils.isNotBlank(contactPerson)){
                member = memberService.findMemberByMemberCodeOrWhaleliveId(whaleLiveId);
                if (member == null) {
                    throw new ValidatorException("invalidWhaleLiveId", getLocale());
                }
                else {
                    // memberService.doCreateCasting(url, phoneNo, contactPerson, whaleLiveId);
                    // throw new ValidatorException("successfullyAdded", getLocale());

                }

            }

        } catch (Exception ex) {
            addActionError(ex.getMessage());
            insert_errorMessage = getActionErrors().toArray()[0].toString();
        }
        return PAGE17;
    }

    @Action(value = "/wl_casting_success")
    public String whale_media_casting_success_page() throws Exception {
        return PAGE18;
    }

    @Action(value = "/wl_income_report")
    public String whale_media_income_report_page() throws Exception {
        initialize();
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }

        broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

        return PAGE19;
    }

    @Action(value = "/wl_change_profile")
    public String whale_media_change_profile_page() throws Exception {
        initialize();
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agent = ((AgentUser) loginInfo.getUser()).getAgent();
        }
        user = loginInfo.getUser();

        return PAGE20;
    }

    @Action(value = "/wl_guild_casting")
    public String whale_media_guild_casting_page() throws Exception {
        LoginInfo loginInfo = getLoginInfo();
        if (loginInfo.getUserType() instanceof AgentUserType) {
            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
        }
        broadcastGuild = broadcastService.findBroadcastGuildByAgentId(agentId);

        return PAGE21;
    }

    @Action(value = "/wl_guild_casting_view")
    public String whale_media_guild_casting_view_page() throws Exception {

        casting = castingService.getCasting(id);

        MemberDetail memberDetail = memberService.findMemberDetailByWhaleliveId(casting.getWhaleLiveId());
        casting.setWhaleliveProfileName(memberDetail.getProfileName());

        return PAGE22;
    }

    @Action(value = "/wl_guild_casting_edit")
    public String whale_media_guild_casting_edit_page() throws Exception {

        casting = castingService.getCasting(id);

        MemberDetail memberDetail = memberService.findMemberDetailByWhaleliveId(casting.getWhaleLiveId());
        casting.setWhaleliveProfileName(memberDetail.getProfileName());

        return PAGE23;
    }

    @Action(value = "/guildEditCasting")
    public String whale_media_edit_guild_casting_page() throws Exception {
        try {
            Casting checkCasting = castingService.getCastingByWhaleliveId(whaleLiveId);

            if (checkCasting != null) {
                casting.setWhaleLiveId(whaleLiveId);
                casting.setFullName(fullName);
                casting.setIdentityNo(identityNo);
                casting.setPhoneNo(phoneNo);
                casting.setGender(gender);
                casting.setEmail(email);
                casting.setAge(age);
                casting.setTalent(talent);
                casting.setUrl(url);
                casting.setTiktokId(tiktokId);
                casting.setFbId(fbId);
                casting.setIgId(igId);

                casting.setIgFan(igFans);
                casting.setLocation(location);
                casting.setPhoneModel(phoneModel);

                DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm");
                Date date = (Date)formatter.parse(testLiveTimestamp);

                casting.setTestLiveDate(date);
                casting.setWeiboId(weiboId);
                casting.setTiktokFan(tiktokFans);
                casting.setWeiboFan(weiboFans);

                memberService.doUpdateCasting(casting);
            }
        } catch (Exception ex) {
            addActionError(ex.getMessage());
            insert_errorMessage = getActionErrors().toArray()[0].toString();
        }
        return JSON;
    }

    @Action(value = "/clientChangePasswordUpdate")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String clientUpdate() {
        try {
            userDetailsService.changePassword(getLocale(), getLoginUser().getUserId(), oldPassword, newPassword);

            successMessage = getText("successMessage.ChangePasswordAction");
        } catch (Exception ex) {
            log.debug(ex.getMessage());
            addActionError(ex.getMessage());
        }

        return JSON;
    }

    @Action(value = "/clientChangeProfileUpdate")
    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
    public String clientProfileUpdate() {
        try {
            LoginInfo loginInfo = getLoginInfo();
            User user1 = loginInfo.getUser();

            Agent agent1 = ((AgentUser) loginInfo.getUser()).getAgent();


            if(!user1.getUsername().equalsIgnoreCase(user.getUsername()))
            {
                if(userDetailsService.isUsernameAvailable(user.getCompId(),user.getUsername())){
                    user1.setUsername(user.getUsername());

                    userService.updateUser(user1);

                    agent1.setAgentName(agent.getAgentName());
                    agent1.setPhoneNo(agent.getPhoneNo());
                    agent1.setEmail(agent.getEmail());

                    agentService.updateAgentUserProfile(getLocale(),agent1);



                }else
                {
                    throw new ValidatorException("invalidWhaleLiveId", getLocale());
                }
            }

            successMessage = getText("successMessage.ChangePasswordAction");
        } catch (Exception ex) {
            log.debug(ex.getMessage());
            addActionError(ex.getMessage());
        }

        return JSON;
    }

//    @Action(value = "/wl_point")
//    @Accesses(access = {@Access(accessCode = AP.GUILD, readMode = true), @Access(accessCode = AP.ROLE_AGENT, readMode = true)})
//    public String whale_media_guild_point_page() throws Exception {
//
////        LoginInfo loginInfo = getLoginInfo();
////        if (loginInfo.getUserType() instanceof AgentUserType) {
////            agentId = ((AgentUser) loginInfo.getUser()).getAgentId();
////        }
//
//        return PAGE17;
//    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getInsert_errorMessage() {
        return insert_errorMessage;
    }

    public void setInsert_errorMessage(String insert_errorMessage) {
        this.insert_errorMessage = insert_errorMessage;
    }

    @Override
    public void setServletResponse(HttpServletResponse response) {
        httpServletResponse = response;
    }

    @Override
    public void setServletRequest(HttpServletRequest request) {
        httpServletRequest = request;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getTransactionPassword() {
        return transactionPassword;
    }

    public void setTransactionPassword(String transactionPassword) {
        this.transactionPassword = transactionPassword;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<OptionBean> getLanguages() {
        return languages;
    }

    public void setLanguages(List<OptionBean> languages) {
        this.languages = languages;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public List<TopupOptionInfoDto> getTopUpOptionInfoDtos() {
        return topUpOptionInfoDtos;
    }

    public void setTopUpOptionInfoDtos(List<TopupOptionInfoDto> topUpOptionInfoDtos) {
        this.topUpOptionInfoDtos = topUpOptionInfoDtos;
    }

    public Boolean getShowBalanceAndTopUpOption() {
        return showBalanceAndTopUpOption;
    }

    public void setShowBalanceAndTopUpOption(Boolean showBalanceAndTopUpOption) {
        this.showBalanceAndTopUpOption = showBalanceAndTopUpOption;
    }

    public String getPhonoNoTopUp() {
        return phonoNoTopUp;
    }

    public void setPhonoNoTopUp(String phonoNoTopUp) {
        this.phonoNoTopUp = phonoNoTopUp;
    }

    public String getPaymentOption_topUp() {
        return paymentOption_topUp;
    }

    public void setPaymentOption_topUp(String paymentOption_topUp) {
        this.paymentOption_topUp = paymentOption_topUp;
    }

    public BigDecimal getWhaleCoinAmount_topUp() {
        return whaleCoinAmount_topUp;
    }

    public void setWhaleCoinAmount_topUp(BigDecimal whaleCoinAmount_topUp) {
        this.whaleCoinAmount_topUp = whaleCoinAmount_topUp;
    }

    public String getLogin_errorMessage() {
        return login_errorMessage;
    }

    public void setLogin_errorMessage(String login_errorMessage) {
        this.login_errorMessage = login_errorMessage;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }

    public String getTopUp_errorMessage() {
        return topUp_errorMessage;
    }

    public void setTopUp_errorMessage(String topUp_errorMessage) {
        this.topUp_errorMessage = topUp_errorMessage;
    }

    public BigDecimal getFromWalletAmt_topUp() {
        return fromWalletAmt_topUp;
    }

    public void setFromWalletAmt_topUp(BigDecimal fromWalletAmt_topUp) {
        this.fromWalletAmt_topUp = fromWalletAmt_topUp;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getPaymentGateWayUrl() {
        return paymentGateWayUrl;
    }

    public void setPaymentGateWayUrl(String paymentGateWayUrl) {
        this.paymentGateWayUrl = paymentGateWayUrl;
    }

    public String getTranID() {
        return tranID;
    }

    public void setTranID(String tranID) {
        this.tranID = tranID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNbcb() {
        return nbcb;
    }

    public void setNbcb(String nbcb) {
        this.nbcb = nbcb;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPaydate() {
        return paydate;
    }

    public void setPaydate(String paydate) {
        this.paydate = paydate;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSkey() {
        return skey;
    }

    public void setSkey(String skey) {
        this.skey = skey;
    }

    public String getAppcode() {
        return appcode;
    }

    public void setAppcode(String appcode) {
        this.appcode = appcode;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_desc() {
        return error_desc;
    }

    public void setError_desc(String error_desc) {
        this.error_desc = error_desc;
    }

    public String getDisplayCode() {
        return displayCode;
    }

    public void setDisplayCode(String displayCode) {
        this.displayCode = displayCode;
    }

    public String getWhaleLiveId() {
        return whaleLiveId;
    }

    public void setWhaleLiveId(String whaleLiveId) {
        this.whaleLiveId = whaleLiveId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public BroadcastGuild getBroadcastGuild() {
        return broadcastGuild;
    }

    public void setBroadcastGuild(BroadcastGuild broadcastGuild) {
        this.broadcastGuild = broadcastGuild;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    // ---------------- GETTER & SETTER (START) -------------

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Casting getCasting() {
        return casting;
    }

    public void setCasting(Casting casting) {
        this.casting = casting;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getTalent() {
        return talent;
    }

    public void setTalent(String talent) {
        this.talent = talent;
    }

    public String getTiktokId() {
        return tiktokId;
    }

    public void setTiktokId(String tiktokId) {
        this.tiktokId = tiktokId;
    }

    public String getFbId() {
        return fbId;
    }

    public void setFbId(String fbId) {
        this.fbId = fbId;
    }

    public String getIgId() {
        return igId;
    }

    public void setIgId(String igId) {
        this.igId = igId;
    }

    public List<OptionBean> getAllGender() {
        return allGender;
    }

    public void setAllGender(List<OptionBean> allGender) {
        this.allGender = allGender;
    }

    public String getIdentityNo() {
        return identityNo;
    }

    public void setIdentityNo(String identityNo) {
        this.identityNo = identityNo;
    }

    public List<OptionBean> getAllGuildRankList() {
        return allGuildRankList;
    }

    public void setAllGuildRankList(List<OptionBean> allGuildRankList) {
        this.allGuildRankList = allGuildRankList;
    }

    public List<OptionBean> getAllMonthList() {
        return allMonthList;
    }

    public void setAllMonthList(List<OptionBean> allMonthList) {
        this.allMonthList = allMonthList;
    }

    public List<OptionBean> getAllYearList() {
        return allYearList;
    }

    public void setAllYearList(List<OptionBean> allYearList) {
        this.allYearList = allYearList;
    }

    public Agent getAgent() {
        return agent;
    }

    public void setAgent(Agent agent) {
        this.agent = agent;
    }

    public String getTestLiveTimestamp() {
        return testLiveTimestamp;
    }

    public void setTestLiveTimestamp(String testLiveTimestamp) {
        this.testLiveTimestamp = testLiveTimestamp;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getPhoneModel() {
        return phoneModel;
    }

    public void setPhoneModel(String phoneModel) {
        this.phoneModel = phoneModel;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getTiktokFans() {
        return tiktokFans;
    }

    public void setTiktokFans(Integer tiktokFans) {
        this.tiktokFans = tiktokFans;
    }

    public Integer getIgFans() {
        return igFans;
    }

    public void setIgFans(Integer igFans) {
        this.igFans = igFans;
    }

    public Integer getWeiboFans() {
        return weiboFans;
    }

    public void setWeiboFans(Integer weiboFans) {
        this.weiboFans = weiboFans;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWeiboId() {
        return weiboId;
    }

    public void setWeiboId(String weiboId) {
        this.weiboId = weiboId;
    }

    public List<OptionBean> getGetGender() {
        return getGender;
    }

    public void setGetGender(List<OptionBean> getGender) {
        this.getGender = getGender;
    }

    // ---------------- GETTER & SETTER (START) -------------

}
