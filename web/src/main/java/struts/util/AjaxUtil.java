package struts.util;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AjaxUtil {

    public static List<String> stringArrayToList(String params) {
        if (params == null || params.trim().isEmpty() || !params.contains(",")) {
            return null;
        }
        return Stream.of(params.split(",")).collect(Collectors.toList());
    }

}
