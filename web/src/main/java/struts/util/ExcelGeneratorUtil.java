package struts.util;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.HorizontalAlignment;

import java.util.Arrays;
import java.util.LinkedList;

public class ExcelGeneratorUtil {

    private HSSFWorkbook wb;
    private HSSFSheet sheet;
    private HSSFRow row;
    private LinkedList<String> headerList = new LinkedList<>();
    private String sheetName;

    public ExcelGeneratorUtil(String excelHeaderName) {
        convertStringIntoList(excelHeaderName);
    }

    public void createHeader(String sheetName) {
        /**
         *used HSSFWorkbook for Excel files in .xls format.
         **/
        wb = new HSSFWorkbook();
//          defined sheet name
        sheet = wb.createSheet(sheetName);
        row = sheet.createRow(0);
        HSSFCellStyle style = wb.createCellStyle();
        style.setAlignment(HorizontalAlignment.CENTER);

        HSSFCell cell;
        int colNum = 0;
        for (String header : headerList) {
            cell = row.createCell(colNum);
            cell.setCellValue(header.trim());
            cell.setCellStyle(style);
            colNum++;
        }
    }

    /**
     * example : "header1,header2,header3,header4,........."
     * criteria : assign in sequence desired to be displayed
     **/
    private void convertStringIntoList(String headerStr) {
        headerList = new LinkedList<>(Arrays.asList(headerStr.split(";")));
    }


    public HSSFWorkbook getWb() {
        return wb;
    }

    public void setWb(HSSFWorkbook wb) {
        this.wb = wb;
    }

    public HSSFSheet getSheet() {
        return sheet;
    }

    public void setSheet(HSSFSheet sheet) {
        this.sheet = sheet;
    }

    public HSSFRow getRow() {
        return row;
    }

    public void setRow(HSSFRow row) {
        this.row = row;
    }

    public LinkedList<String> getHeaderList() {
        return headerList;
    }

    public void setHeaderList(LinkedList<String> headerList) {
        this.headerList = headerList;
    }

    public String getSheetName() {
        return sheetName;
    }

    public void setSheetName(String sheetName) {
        this.sheetName = sheetName;
    }
}
