import com.compalsolutions.compal.SysFormatter;

/**
 * This class as workaround for SysFormatter. The main purpose is shorten syntax for accessing static properties in OGNL
 */
public class SF extends SysFormatter {
}
