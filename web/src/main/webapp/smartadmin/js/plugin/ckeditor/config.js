/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';

    //Remove text in the picture preview
    config.image_previewText = ' ';
    //Enable the picture upload function and add actian url for backend process
    config.filebrowserImageUploadUrl= "/app/ckeditorImageUpload.php?type=image";
};
