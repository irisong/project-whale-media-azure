<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<script type="text/javascript">
    $(function() {
        $("#bankInfoEditForm").validate({
            submitHandler: function(form){
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $('.btnSave').prop('disabled', true);

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: defaultJsonCallback
                    });
                });
            },
            rules: {
                "memberDetail.bankName" : {
                    required : true
                },
                "memberDetail.bankBranch" : {
                    required : true
                },
                "memberDetail.bankAddress" : {
                    required : true,
                    digits : true
                },
                "memberDetail.bankCode" : {
                    required : true
                },
                "memberDetail.bankAccountNo" : {
                    required : true
                },
                "memberDetail.bankAccountName" : {
                    required : true
                }
            }
        });

        $("#imageForm").validate({
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $('.btnSave').prop('disabled', true);

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: defaultJsonCallback
                    });
                });
            }, // submitHandler
            rules: {
            }
        });
    });

    function defaultJsonCallback(json){
        $("body").unmask();

        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location.reload();
                });
            },
            onFailure: function (json, error) {
                $('.btnSave').removeAttr('disabled');
                messageBox.alert(error);
            }
        });
    }

    function viewImage(fileUrl){
        $("#imageViewer").removeAttr('src');

        // window.open(fileUrl, "_blank");
        $("#imageViewerModal").dialog('open');
        $("#imageViewer").attr('src', fileUrl);
    }
</script>

<sc:title title="title.bankAccountInfo"/>
<sc:displayErrorMessage align="center" />

<div class="row">
    <article class="col-sm-12">
        <div class="well">
            <s:form action="bankInfoUpdate" name="bankInfoEditForm" id="bankInfoEditForm" cssClass="form-horizontal">
                <fieldset>
                    <legend><s:text name="title.bankAccountInfo"/></legend>
                </fieldset>
                <h6><s:text name="fillUpAllBankAccountInformation"/></h6><br/>
                <s:if test="%{allowEditForm && memberDetail.bankVerifyRemark!=null}">
                    <h6 style="color:red"><s:text name="remark"/>: <s:property value="memberDetail.bankVerifyRemark"/></h6>
                </s:if>

                <s:textfield name="member.memberCode" id="member.memberCode" label="%{getText('memberCode')}" maxlength="20" disabled="true"/>
                <s:textfield name="member.fullName" id="member.fullName" label="%{getText('fullName')}" maxlength="20" disabled="true"/>

                <s:if test="allowEditForm">
                    <s:textfield name="memberDetail.bankName" id="memberDetail.bankName" label="%{getText('bankName')}" maxlength="100" required="true"/>
                    <s:textfield name="memberDetail.bankBranch" id="memberDetail.bankBranch" label="%{getText('bankBranch')}" maxlength="100" required="true"/>
                    <s:textfield name="memberDetail.bankAddress" id="memberDetail.bankAddress" label="%{getText('bankAddress')}" maxlength="100" required="true"/>
                    <s:textfield name="memberDetail.bankCode" id="memberDetail.bankCode" label="%{getText('bankCode')}" maxlength="100" required="true"/>
                    <s:textfield name="memberDetail.bankAccountNo" id="memberDetail.bankAccountNo" label="%{getText('bankAccountNo')}" maxlength="100" required="true"/>
                    <s:textfield name="memberDetail.bankAccountName" id="memberDetail.bankAccountName" label="%{getText('bankAccountName')}" maxlength="100" required="true"/>
                </s:if>
                <s:else>
                    <s:textfield name="memberDetail.bankName" id="memberDetail.bankName" label="%{getText('bankName')}" maxlength="100" required="true" disabled="true"/>
                    <s:textfield name="memberDetail.bankBranch" id="memberDetail.bankBranch" label="%{getText('bankBranch')}" maxlength="100" required="true" disabled="true"/>
                    <s:textfield name="memberDetail.bankAddress" id="memberDetail.bankAddress" label="%{getText('bankAddress')}" maxlength="100" required="true" disabled="true"/>
                    <s:textfield name="memberDetail.bankCode" id="memberDetail.bankCode" label="%{getText('bankCode')}" maxlength="100" required="true" disabled="true"/>
                    <s:textfield name="memberDetail.bankAccountNo" id="memberDetail.bankAccountNo" label="%{getText('bankAccountNo')}" maxlength="100" required="true" disabled="true"/>
                    <s:textfield name="memberDetail.bankAccountName" id="memberDetail.bankAccountName" label="%{getText('bankAccountName')}" maxlength="100" required="true" disabled="true"/>
                </s:else>

                <s:textfield name="status" id="status" label="%{getText('status')}" maxlength="20" disabled="true"/>

                <s:if test="allowEditForm">
                    <ce:buttonRow>
                        <button type="submit" class="btn btn-primary btnSave">
                            <i class="fa fa-floppy-o"></i>
                            <s:text name="btnSave"/>
                        </button>
                    </ce:buttonRow>
                </s:if>
            </s:form>
        </div>
    </article>

    <article class="col-sm-12">
    <div class="well">
        <s:form action="bankInfoImageSave" name="imageForm" id="imageForm" cssClass="form-horizontal">
            <fieldset>
                <legend><s:text name="uploadFile"/></legend>
            </fieldset>
            <h6><s:text name="uploadProofOfBankAccountResidenceAndIdPhoto"/></h6><br/>
            <s:if test="%{allowUpload && memberDetail.bankVerifyRemark!=null}">
                <h6 style="color:red"><s:text name="remark"/>: <s:property value="memberDetail.bankVerifyRemark"/></h6>
            </s:if>

            <div class="form-group">
                <label class="col-sm-3 control-label"><s:text name="proofOfBankAccount"/>:</label>

                <div class="col-sm-9">
                    <div class="btn-group">
                        <s:if test="bankUploaded">
                            <button type="button" class="btn btn-default btn-success" onclick="viewImage('<s:property value="memberDetail.bankImageUrl"/>')"><s:text name="viewImage"/></button>
                        </s:if>

                        <s:if test="%{bankUploaded && allowUpload}">
                            <s:file name="fileUploadBank" cssClass="btn btn-default" theme="simple"/>
                        </s:if>
                        <s:elseif test="allowUpload">
                            <s:file name="fileUploadBank" required="true" cssClass="btn btn-default" theme="simple"/>
                        </s:elseif>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><s:text name="proofOfResidence"/>:</label>

                <div class="col-sm-9">
                    <div class="btn-group">
                        <s:if test="residenceUploaded">
                            <button type="button" class="btn btn-default btn-success" onclick="viewImage('<s:property value="memberDetail.residenceImageUrl"/>')"><s:text name="viewImage"/></button>
                        </s:if>

                        <s:if test="%{residenceUploaded && allowUpload}">
                            <s:file name="fileUploadResidence" cssClass="btn btn-default" theme="simple"/>
                        </s:if>
                        <s:elseif test="allowUpload">
                            <s:file name="fileUploadResidence" required="true" cssClass="btn btn-default" theme="simple"/>
                        </s:elseif>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><s:text name="passportId"/>:</label>

                <div class="col-sm-9">
                    <div class="btn-group">
                        <s:if test="icUploaded">
                            <button type="button" class="btn btn-default btn-success" onclick="viewImage('<s:property value="memberDetail.icImageUrl"/>')"><s:text name="viewImage"/></button>
                        </s:if>

                        <s:if test="%{icUploaded && allowUpload}">
                            <s:file name="fileUploadIc" cssClass="btn btn-default" theme="simple"/>
                        </s:if>
                        <s:elseif test="allowUpload">
                            <s:file name="fileUploadIc" required="true" cssClass="btn btn-default" theme="simple"/>
                        </s:elseif>
                    </div>
                </div>
            </div>

            <s:if test="allowUpload">
                <ce:buttonRow>
                <button type="submit" class="btn btn-primary btnSave">
                    <i class="fa fa-floppy-o"></i>
                    <s:text name="btnSave"/>
                </button>
                </ce:buttonRow>
            </s:if>
        </s:form>
    </article>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true" resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>
