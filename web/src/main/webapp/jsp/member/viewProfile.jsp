<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="padding">
    <div class="content_title"></div>
</div>
<!--<div class="content_line"></div>-->
<br class="clear">

<script type="text/javascript">
$(function() {
    $.populateDOB({
        dobYear : $("#dob_year")
        ,dobMonth : $("#dob_month")
        ,dobDay : $("#dob_day")
        ,dobFull : $("#memberDetail\\.dob")
        ,defaultValue : $("#memberDetail\\.dob").val()
    });

    $("#registerForm").validate({
        rules : {
            /*"fullname" : {
                required : true,
                minlength : 2
            },*/
            "ic" : {
                required : true,
                minlength : 3
            },
            "memberDetail.address" : {
                required : true,
                minlength : 3
            },
            "memberDetail.city" : {
                required : true
            },
            "memberDetail.state" : {
                required : true
            },
            "memberDetail.postcode" : {
                required : true,
                minlength : 3
            },
            "memberDetail.email" : {
                required : true
                , email: true
            },
            "memberDetail.contact" : {
                required : true
                , minlength : 10
            },
            "memberDetail.gender" : {
                required : true
            }
        },
        submitHandler: function(form) {
            waiting();
            form.submit();
        },
        success: function(label) {
            //label.addClass("valid").text("Valid captcha!")
        }
    });
    $("#uploadForm").validate({
        rules : {
            "bankPassBook" : {
                required: "#bankPassBook.length > 0",
                minlength : 3,
                accept:'docx?|pdf|bmp|jpg|jpeg|gif|png|tif|tiff|xls|xlsx'
            },
            "proofOfResidence" : {
                required: "#proofOfResidence.length > 0",
                minlength : 3,
                accept:'docx?|pdf|bmp|jpg|jpeg|gif|png|tif|tiff|xls|xlsx'
            },
            "nric" : {
                required: "#nric.length > 0",
                minlength : 3,
                accept:'docx?|pdf|bmp|jpg|jpeg|gif|png|tif|tiff|xls|xlsx'
            }
        },
        submitHandler: function(form) {
            waiting();
            form.submit();
        },
        success: function(label) {
            //label.addClass("valid").text("Valid captcha!")
        }
    });
    $("#btnUpdate").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });
    $("#btnUpload").button({
        icons: {
            primary: "ui-icon-circle-arrow-n"
        }
    });

    jQuery.validator.addMethod("latinRegex", function(value, element) {
        return this.optional(element) || /[a-zA-Z\-\'\ ]/i.test(value);
    }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

    $("#bankForm").validate({
        messages : {
            transactionPassword: {
                remote: "<s:text name="security_password_is_not_valid"/>"
            }
        },
        rules : {
            "memberDetail.bankName" : {
                required : true
            },
            "memberDetail.bankAccNo" : {
                required : true,
                minlength : 3
            },
            "memberDetail.bankHolderName" : {
                required : true
            },
            "memberDetail.bankCountryCode" : {
                required : true
            },
            "memberDetail.bankAccountCurrency" : {
                required : true
            },
            "memberDetail.bankAddress" : {
                required : true
            },
            "memberDetail.bankBranchName" : {
                required : true
            },
            /*"bankCode" : {
                required : true
            },*/
            "transactionPassword" : {
                required : true,
                remote: "<s:url action="verifyTransactionPassword" />"
            }
        },
        submitHandler: function(form) {
            waiting();

            if ($("#bankCountry").val() == "Singapore" && $("#bankCode").val() == "") {
                error("Bank Code is required.");
                return false;
            }
            form.submit();
        },
        success: function(label) {
            //label.addClass("valid").text("Valid captcha!")
        }
    });
    $("#moneyTracForm").validate({
        messages : {
            transactionPassword: {
                remote: "<s:text name="security_password_is_not_valid"/>"
            }
        },
        rules : {
            "memberDetail.moneytracUsername" : {
                required : true
            },
            "memberDetail.moneytracCustomerId" : {
                required : true
            },
            "transactionPassword" : {
                required : true,
                remote: "<s:url action="verifyTransactionPassword" />"
            }
        },
        submitHandler: function(form) {
            waiting();
            form.submit();
        },
        success: function(label) {
            //label.addClass("valid").text("Valid captcha!")
        }
    });
    $("#iaccountForm").validate({
        messages : {
            transactionPassword: {
                remote: "<s:text name="security_password_is_not_valid"/>"
            }
        },
        rules : {
            "memberDetail.iaccount" : {
                required : true
            },
            "memberDetail.iaccountUsername" : {
                required : true
            },
            "transactionPassword" : {
                required : true,
                remote: "<s:url action="verifyTransactionPassword" />"
            }
        },
        submitHandler: function(form) {
            waiting();
            form.submit();
        },
        success: function(label) {
            //label.addClass("valid").text("Valid captcha!")
        }
    });

    $("#btnBankUpdate").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });
    $("#btnMoneyTrac").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });
    $("#btnIAccount").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });

    $("#passwordForm").validate({
        messages : {
            newPassword2: {
                equalTo: "<s:text name="please_enter_the_same_password_as_above"/>"
            }
        },
        rules : {
            "oldPassword" : {
                required : true,
                minlength : 3
            },
            "newPassword" : {
                required : true,
                minlength : 3
            },
            "newPassword2" : {
                required : true,
                minlength : 3,
                equalTo: "#newPassword"
            }
        },
        submitHandler: function(form) {
            waiting();
            form.submit();
        }
    });

    $("#btnPasswordUpdate").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });

    $("#btnBeneficiaryUpdate").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });

    $("#securityPasswordForm").validate({
        messages : {
            newSecurityPassword2: {
                equalTo: "<s:text name="please_enter_the_same_password_as_above"/>"
            }
        },
        rules : {
            "oldSecurityPassword" : {
                required : true,
                minlength : 3
            },
            "newSecurityPassword" : {
                required : true,
                minlength : 3
            },
            "newSecurityPassword2" : {
                required : true,
                minlength : 3,
                equalTo: "#newSecurityPassword"
            }
        },
        submitHandler: function(form) {
            waiting();
            form.submit();
        }
    });
    $("#btnSecurityUpdate").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    });
});
</script>

<table cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="personal_information"/></span></td>
    </tr>
    <tr>
        <td><br/><sc:displayMemberMessage/></td>
    </tr>
    <tr>
        <td>
            <s:form action="updateProfile" id="registerForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th><s:text name="personal_detail"/></th>
                    <th class="tbl_content_right"></th>
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="fullname"/></td>
                    <td><s:textfield id="member.fullName" name="member.fullName" readonly="true" tabindexBak="5" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="country"/></td>
                    <td><s:select list="countryDescs" name="memberDetail.countryCode" id="memberDetail.countryCode" listKey="countryCode" listValue="countryName"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="dateOfBirth"/></td>
                    <td><select id="dob_year"></select>
                        <select id="dob_month"></select>
                        <select id="dob_day"></select>
                        <s:hidden name="memberDetail.dob" readonly="readonly" type="hidden" id="memberDetail.dob"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="address"/></td>
                    <td>
                        <s:textfield id="memberDetail.address" name="memberDetail.address" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td>
                        <s:textfield id="memberDetail.address2" name="memberDetail.address2" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="city_town"/></td>
                    <td>
                        <s:textfield id="memberDetail.city" name="memberDetail.city" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="state_province"/></td>
                    <td>
                        <s:textfield id="memberDetail.state" name="memberDetail.state" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="zip_postal_code"/></td>
                    <td>
                        <s:textfield id="memberDetail.postcode" name="memberDetail.postcode" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="email"/></td>
                    <td>
                        <s:textfield id="memberDetail.email" name="memberDetail.email" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="alternate_email"/></td>
                    <td>
                        <s:textfield id="memberDetail.alternateEmail" name="memberDetail.alternateEmail" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="contact_number"/></td>
                    <td>
                        <s:textfield id="memberDetail.contact" name="memberDetail.contact" tabindexBak="13" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="gender"/></td>
                    <td>
                        <s:radio list="genders" name="memberDetail.gender" id="memberDetail.gender" listKey="key" listValue="value"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnUpdate"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>
            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="updateBeneficiary" id="updateBeneficiaryForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>

                <tbody>
                <tr class="row_header">
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th><s:text name="beneficiary_nominee"/></th>
                    <th></th>
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>


                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="name"/></td>
                    <td>
                        <s:textfield id="memberDetail.nomineeName" name="memberDetail.nomineeName"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>


                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="relationship"/></td>
                    <td>
                        <s:textfield id="memberDetail.nomineeRelationship" name="memberDetail.nomineeRelationship"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="ic_passport_no"/></td>
                    <td>
                        <s:textfield id="memberDetail.nomineeIc" name="memberDetail.nomineeIc"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>


                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="contact_number"/></td>
                    <td>
                        <s:textfield id="memberDetail.nomineeContactno" name="memberDetail.nomineeContactno"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnBeneficiaryUpdate"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>
            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>

                <tbody>
                <tr class="row_header">
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th><s:text name="affiliate_setting"/></th>
                    <th></th>
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>


                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="affiliate_link"/></td>
                    <td>
                        <s:textfield id="affiliateLink" name="affiliateLink" cssClass="inputbox" size="40"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="changePassword" id="passwordForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th colspan="2"><s:text name="change_account_login_password"/></th>
<!--                    <th class="tbl_content_right"></th>-->
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="old_login_password"/></td>
                    <td><input name="oldPassword" type="password" id="oldPassword" tabindexBak="1"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="new_login_password"/></td>
                    <td><input name="newPassword" type="password" id="newPassword" tabindexBak="2"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="re_enter_login_password"/></td>
                    <td><input name="newPassword2" type="password" id="newPassword2" tabindexBak="3"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnPasswordUpdate"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>

            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="changeSecondPassword" id="securityPasswordForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th colspan="2"><s:text name="change_security_password"/></th>
<!--                    <th class="tbl_content_right"></th>-->
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="old_security_password"/></td>
                    <td>
                        <input name="oldSecurityPassword" type="password" id="oldSecurityPassword" tabindexBak="1" />
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="new_security_password"/></td>
                    <td>
                        <input name="newSecurityPassword" type="password" id="newSecurityPassword" tabindexBak="2" />
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="re_enter_security_password"/></td>
                    <td>
                        <input name="newSecurityPassword2" type="password" id="newSecurityPassword2" tabindexBak="3" />
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnSecurityUpdate"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>

            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="updateBankInformation" id="bankForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th><s:text name="bank_account_details"/></th>
                    <th class="tbl_content_right"></th>
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_name"/></td>
                    <td><s:textfield id="memberDetail.bankName" name="memberDetail.bankName" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_branch"/></td>
                    <td><s:textfield id="memberDetail.bankBranchName" name="memberDetail.bankBranchName" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_address"/></td>
                    <td><s:textfield id="memberDetail.bankAddress" name="memberDetail.bankAddress" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td>Country</td>
                    <td><s:select list="countryDescs" name="memberDetail.bankCountryCode" id="memberDetail.bankCountryCode" listKey="countryCode" listValue="countryName"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="currency"/></td>
                    <td><s:textfield id="memberDetail.bankAccountCurrency" name="memberDetail.bankAccountCurrency" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_account_number"/></td>
                    <td><s:textfield id="memberDetail.bankAccNo" name="memberDetail.bankAccNo" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <!--<tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td><input name="rebankAccNo" type="text" id="rebankAccNo" size="30"
                                                         value=""/>
                    </td>
                    <td>&nbsp;</td>
                </tr>-->

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_account_holder_name"/></td>
                    <td><s:textfield id="memberDetail.bankHolderName" name="memberDetail.bankHolderName" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_swift_code_aba"/></td>
                    <td><s:textfield id="memberDetail.bankSwiftCode" name="memberDetail.bankSwiftCode" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_code"/></td>
                    <td><s:textfield id="memberDetail.bankCode" name="memberDetail.bankCode" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="maxim_trader_visa_debit_card"/></td>
                    <td><s:textfield id="memberDetail.visaDebitCard" name="memberDetail.visaDebitCard" size="30" maxlength="16"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="security_password"/></td>
                    <td><s:password id="transactionPassword" name="transactionPassword" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnBankUpdate"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>

            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="updateIAccount" id="iaccountForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th><s:text name="i_account_details"/></th>
                    <th class="tbl_content_right"></th>
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="account_name"/></td>
                    <td><s:textfield id="memberDetail.iaccountUsername" name="memberDetail.iaccountUsername" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="account_number"/></td>
                    <td><s:textfield id="memberDetail.iaccount" name="memberDetail.iaccount" size="30" maxlength="16"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="security_password"/></td>
                    <td><s:password id="transactionPassword" name="transactionPassword" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnIAccount"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td colspan="2"><br><span style="color: #0080c8;"><s:text name="sample"/></span>:<br><img src="/images/iAccount.jpg"></td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>

            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="updateMoneyTrac" id="moneyTracForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th><s:text name="money_trac_details"/></th>
                    <th class="tbl_content_right"></th>
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="money_trac_fullname"/></td>
                    <td><s:textfield id="memberDetail.moneytracUsername" name="memberDetail.moneytracUsername" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="money_trac_customer_id"/></td>
                    <td><s:textfield id="memberDetail.moneytracCustomerId" name="memberDetail.moneytracCustomerId" size="30"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="security_password"/></td>
                    <td>
                        <input name="transactionPassword" type="password" id="mtTransactionPassword" size="30"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnMoneyTrac"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td colspan="2"><br><span style="color: #0080c8;"><s:text name="sample"/></span>:<br><img src="/images/money_trac.png"></td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>

            <div class="info_bottom_bg"></div>
            <div class="clear"></div>
            <br>

            <s:form action="doUploadFile" id="uploadForm" method="post" enctype="multipart/form-data">
                <input type="hidden" name="submitData" value="true"/>
            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th colspan="2"><s:text name="upload_bank_residence_id"/></th>
                    <!--<th class="tbl_content_right"></th>-->
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="bank_account_proof"/></td>
                    <td>
                        <input type="file" name="bankPassBook" id="bankPassBook" value="" />
                        <s:if test="hasBankPassBook">
                            <span class="ui-icon ui-icon-circle-check" style="display:inline-block;"></span> <span style="color: #468847"><s:text name="uploaded_successfully"/></span>
                        </s:if>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td><s:text name="proof_of_residence"/></td>
                    <td>
                        <input type="file" name="proofOfResidence" id="proofOfResidence" value="" />
                        <s:if test="hasProofOfResidence">
                            <span class="ui-icon ui-icon-circle-check" style="display:inline-block;"></span> <span style="color: #468847"><s:text name="uploaded_successfully"/></span>
                        </s:if>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="passport_photo_id"/></td>
                    <td>
                        <input type="file" name="nric" id="nric" value="" />
                        <s:if test="hasNric">
                            <span class="ui-icon ui-icon-circle-check" style="display:inline-block;"></span> <span style="color: #468847"><s:text name="uploaded_successfully"/></span>
                        </s:if>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td colspan="5">
                        <font color="#dc143c"><s:text name="upload_file_notes"/></font>
                    </td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td></td>
                    <td align="right">
                        <button id="btnUpload"><s:text name="btnUpdate"/></button>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                </tbody>
            </table>
            </s:form>
        </td>
    </tr>
    </tbody>
</table>
    
<c:import url="/WEB-INF/templates/include/footer.jsp"/>