<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="padding">
        <div class="content_title"></div>
    </div>
    <!--<div class="content_line"></div>-->
    <br class="clear">

<script type="text/javascript" language="javascript">
$(function() {
    $.populateDOB({
        dobYear : $("#dob_year")
        ,dobMonth : $("#dob_month")
        ,dobDay : $("#dob_day")
        ,dobFull : $("#memberDetail\\.dob")
    });

    jQuery.validator.addMethod("noSpace", function(value, element) {
        return value.indexOf(" ") < 0 && value != "";
    }, "<s:text name="no_space_please_and_dont_leave_it_empty"/>");

    jQuery.validator.addMethod("loginRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s\_]+$/i.test(value);
    }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

    jQuery.validator.addMethod("latinRegex", function(value, element) {
        return this.optional(element) || /^[a-z0-9\-\s\_\/\.]+$/i.test(value);
    }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

    $("#registerForm").validate({
        messages : {
            confirmPassword: {
                equalTo: "<s:text name="please_enter_the_same_password_as_above"/>"
            },
            username: {
                remote: "<s:text name="username_already_in_use"/>"
            },
            "member.fullName": {
                remote: "<s:text name="full_name_already_in_use"/>"
            }
        },
        rules : {
            "username" : {
                required : true,
                noSpace: true,
                loginRegex: true,
                minlength : 6,
                remote: "<s:url action="checkMemberUsernameAvailability" namespace="/pub/misc"/>"
            },
            "sponsorId" : {
                required : true
            },
            "password" : {
                required : true,
                minlength : 6
            },
            "confirmPassword" : {
                required : true,
                minlength : 6,
                equalTo: "#password"
            },
            "securityPassword" : {
                required : true,
                minlength : 6
            },
            "confirmSecurityPassword" : {
                required : true,
                minlength : 6,
                equalTo: "#securityPassword"
            },
            "member.fullName" : {
                required : true,
                latinRegex: true,
                minlength : 2
//                , remote: "/member/verifyFullName"
            },
            "memberDetail.dob" : {
                required : true
            },
            "memberDetail.address" : {
                required : true
            },
            "memberDetail.gender" : {
                required : true
            },
            "memberDetail.contact" : {
                required : true
                , minlength : 10
            },
            "memberDetail.email" : {
                required : true
                , email: true
            },
            "email2" : {
                required : true,
                equalTo: "#memberDetail\\.email"
            },
            "alternateEmail2" : {
                equalTo: "#memberDetail\\.alternateEmail"
            },
            /*"terms_bis" : {
                required : true
            },*/
            "termsRisk" : {
                required : true
            },
            "memberDetail.termCondition" : {
                required : true
            },
            "memberDetail.signName" : {
                latinRegex: true,
                required : true
            }
            /*"privateInvestmentAgreement" : {
                required : true
            }*/
        },
        submitHandler: function(form) {
            waiting();
            $.ajax({
                type : 'POST',
                url : "<s:url action="verifySponsorId" namespace="/pub/misc"/>",
                dataType : 'json',
                cache: false,
                data: {
                    sponsorId : $('#sponsorId').val()
                },
                success : function(data) {
                    waiting();
                    if (data == null || data == "" || data.sponsorUsername == "") {
                        alert("<s:text name="invalid_referrer_id"/>");
                        $('#sponsorId').focus();
                        $("#sponsorName").val("");
                    } else {
                        if ($("#registerForm_placementType0").is(':checked') == true) {
                            waiting();
                            $.ajax({
                                type : 'POST',
                                url : "<s:url action="verifyActivePlacementId" namespace="/pub/misc"/>",
                                dataType : 'json',
                                cache: false,
                                data: {
                                    sponsorId : $('#sponsorId').val()
                                    , placementDistId : $('#placementDistId').val()
                                },
                                success : function(data) {
                                    if (data == null || data == "" || data.placementUsername == "") {
                                        error("<s:text name="invalid_placement_id"/>");
                                        $('#placementDistId').focus();
                                        $("#placementDistName").val("");
                                    } else {
                                        form.submit();
                                    }
                                },
                                error : function(XMLHttpRequest, textStatus, errorThrown) {
                                    alert("<s:text name="your_login_attempt_was_not_successful"/>");
                                }
                            });
                        } else {
                            form.submit();
                        }
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    alert("<s:text name="your_login_attempt_was_not_successful"/>");
                }
            });
        },
        success: function(label) {
        }
    });

    $("#registerForm_placementType1").click(function(){
        $(".tr_manualPlacement").hide();
        $(".tr_autoPlacement").show();
        // $("#registerForm_position1L").attr("checked", "checked");
    });
    $("#registerForm_placementType0").click(function(){
        $(".tr_manualPlacement").show();
        $(".tr_autoPlacement").hide();
        // $("#registerForm_position2L").attr("checked", "checked");
    });

    $("#sponsorId").change(function() {
        if ($.trim($('#sponsorId').val()) != "") {
            verifySponsorId();
        }
    });
    $("#placementDistId").change(function() {
        if ($.trim($('#placementDistId').val()) != "") {
            verifyPlacementDistId();
        }
    });

    $("#registerForm_placementType${placementType}").click();
});

function verifySponsorId() {
    waiting();
    $.ajax({
        type : 'POST',
        url : "<s:url action="verifyActiveSponsorId" namespace="/pub/misc"/>",
        dataType : 'json',
        cache: false,
        data: {
            sponsorId : $('#sponsorId').val()
            , verifySameGroup : true
        },
        success : function(data) {
            if (data == null || data == "" || data.sponsorUsername == "") {
                error("<s:text name="invalid_referrer_id"/>");
                $('#sponsorId').focus();
                $("#sponsorName").val("");
            } else {
                $.unblockUI();
                $("#sponsorName").val(data.sponsorUsername);
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert("<s:text name="your_login_attempt_was_not_successful"/>");
        }
    });
}
function verifyPlacementDistId____bak() {
    waiting();
    $.ajax({
        type : 'POST',
        url : "/member/verifyPlacementUnderSameSponsorGroupBySponsorId",
        dataType : 'json',
        cache: false,
        data: {
            sponsorId : $('#sponsorId').val()
            , placementDistCode : $('#placementDistId').val()
        },
        success : function(data) {
            if (data == null || data == "") {
                error("Invalid Placement ID");
                $('#placementDistId').focus();
                $("#placementDistName").val("");
            } else {
                $.unblockUI();
                $("#placementDistName").val(data.userName);
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert("Your login attempt was not successful. Please try again.");
        }
    });
}

function verifyPlacementDistId() {
    waiting();
    $.ajax({
        type : 'POST',
        url : "<s:url action="verifyActivePlacementId" namespace="/pub/misc"/>",
        dataType : 'json',
        cache: false,
        data: {
            sponsorId : $('#sponsorId').val()
            , placementDistId : $('#placementDistId').val()
        },
        success : function(data) {
            if (data == null || data == "" || data.placementUsername == "") {
                error("<s:text name="invalid_placement_id"/>");
                $('#placementDistId').focus();
                $("#placementDistName").val("");
            } else {
                $.unblockUI();
                $("#placementDistName").val(data.placementUsername);
            }
        },
        error : function(XMLHttpRequest, textStatus, errorThrown) {
            alert("<s:text name="your_login_attempt_was_not_successful"/>");
        }
    });
}
</script>

<s:form name="memberRegistration2" id="registerForm" method="post">
    <input type="hidden" name="submitData" value="true"/>
<table cellspacing="0" cellpadding="0">
<colgroup>
    <col width="1%">
    <col width="99%">
    <col width="1%">
</colgroup>
<tbody>
<tr>
    <td rowspan="3">&nbsp;</td>
    <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="member_registration"/></span></td>
    <td rowspan="3">&nbsp;</td>
</tr>
<tr>
    <td><br><sc:displayMemberErrorMessage/></td>
</tr>
<tr>
<td>

<table cellspacing="0" cellpadding="0" class="textarea1">
<tbody>
<tr>
<td>

<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>
    <tbody>
    <tr>
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th colspan="2"><s:text name="referrer_and_placement_position"/></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="referrer_id"/></td>
        <td>
            <s:textfield id="sponsorId" name="sponsorId" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="referrer_name"/></td>
        <td>
            <s:textfield id="sponsorName" name="sponsorName" cssClass="inputbox" readonly="true"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd placementClass">
        <td>&nbsp;</td>
        <td><s:text name="placement_type"/></td>
        <td>
            <div style="width:350px;">
                <s:radio list="placementTypes" name="placementType" listKey="key" listValue="value"/>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_even tr_autoPlacement placementClass">
        <td>&nbsp;</td>
        <td><s:text name="auto_placement"/></td>
        <td>
            <div style="width:350px;">
                <s:radio list="autoPositionTypes" name="position1" listKey="key" listValue="value"/>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_even tr_manualPlacement placementClass" style="display: none">
        <td>&nbsp;</td>
        <td><s:text name="placement_id"/></td>
        <td>
            <s:textfield id="placementDistId" name="placementDistId" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd tr_manualPlacement placementClass" style="display: none">
        <td>&nbsp;</td>
        <td><s:text name="placement_name"/></td>
        <td>
            <s:textfield id="placementDistName" name="placementDistName" cssClass="inputbox" readonly="true"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_even tr_manualPlacement placementClass" style="display: none">
        <td>&nbsp;</td>
        <td><s:text name="placement_position"/></td>
        <td>
            <div style="width:350px;">
                <s:radio list="manualPositionTypes" name="position2" listKey="key" listValue="value"/>
                <!--
                <input type="radio" id="radio_position1_manual_1" value="1" name="position1"> <label for="radio_position1_manual_1" style="display: inline; font-size: 12px !important;">Left</label>&nbsp;
                <input type="radio" id="radio_position1_manual_2" value="2" name="position1"> <label for="radio_position1_manual_2" style="display: inline; font-size: 12px !important;">Right</label>
                -->
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
    </tbody>
</table>
<br>

<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>
    <tbody>
    <tr>
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th><s:text name="account_login_details"/></th>
        <th class="tbl_content_right"><!--Step 1 of 3--></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="username"/></td>
        <td>
            <s:textfield id="username" name="username" cssClass="inputbox"/>
            &nbsp;
            <br>
            <s:text name="username_guideline"/>
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="set_password"/></td>
        <td>
            <s:password id="password" name="password" cssClass="inputbox"/>
            <br>
            <s:text name="set_password_guideline"/>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="confirmPassword"/></td>
        <td>
            <s:password id="confirmPassword" name="confirmPassword" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="security_password"/></td>
        <td>
            <s:password id="securityPassword" name="securityPassword" cssClass="inputbox"/>
            <br>
            <s:text name="security_password_guideline"/><br>
            <s:text name="security_password_guideline2"/><br>
            <s:text name="security_password_guideline3"/>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="confirm_security_password"/></td>
        <td>
            <s:password id="confirmSecurityPassword" name="confirmSecurityPassword" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_listing_end">
        <td colspan="4">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>

<!--<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>

    <tbody>
    <tr class="row_header">
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th>Trading Account Details</th>
        <th></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>


    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td>Leverage</td>
        <td>
            <select id="leverage" class="inputbox" name="leverage">
                <option selected="selected" value="">Please Select</option>
                <option value="50">1:50</option>
                <option value="100">1:100</option>
                <option value="200">1:200</option>
                <option value="300">1:300</option>
                <option value="400">1:400</option>
                <option value="500">1:500</option>
            </select>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td>Spread</td>
        <td>
            <div class="td_desc">
                <select id="spread" class="inputbox" name="spread">
                    <option selected="selected" value="">Please Select</option>
                    <option value="F">Fixed Spread</option>
                    <option value="V">Variable Spread</option>
                    <option value="E">ECN Premier Spread</option>
                </select>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_listing_end">
        <td colspan="4">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>

    <tbody>
    <tr class="row_header">
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th>Deposit Information</th>
        <th></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>


    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td>Deposit Currency</td>
        <td>
            <div>
                <select id="deposit_currency" class="inputbox" name="deposit_currency">
                    <option selected="selected" value="USD">USD</option>
                    <option value="EUR">EUR</option>
                    <option value="GBP">GBP</option>
                    <option value="AUD">AUD</option>
                    <option value="SGD">SGD</option>
                </select>
            </div>
            <div class="td_desc" id="fielddesc__deposit_currency">If a Money Manager handles your account, the currency of
                your account will match the currency that your Money Manager uses to make trades on your behalf. For
                example, if your manager trades in USD, your account will be in USD regardless of which currency you choose.
            </div>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td>Deposit Amount</td>
        <td>
            <div>
                <input type="text" id="deposit_amount" value="" class="inputbox" name="deposit_amount">
            </div>
            <div class="td_desc" id="fielddesc__deposit_amount"></div>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_listing_end">
        <td colspan="4">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>-->

<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>

    <tbody>
    <tr class="row_header">
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th><s:text name="personal_information"/></th>
        <th></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>


    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="fullname"/></td>
        <td>
            <s:textfield id="member.fullName" name="member.fullName" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="dateOfBirth"/></td>
        <td>
            <select id="dob_year"></select>
            <select id="dob_month"></select>
            <select id="dob_day"></select>
            <input name="memberDetail.dob" readonly="readonly" type="hidden" id="memberDetail.dob" class="bp_05"/>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="address"/></td>
        <td>
            <s:textfield id="memberDetail.address" name="memberDetail.address" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td></td>
        <td>
            <s:textfield id="memberDetail.address2" name="memberDetail.address2" cssClass="inputbox"/>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="city_town"/></td>
        <td>
            <s:textfield id="memberDetail.city" name="memberDetail.city" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="zip_postal_code"/></td>
        <td>
            <s:textfield id="memberDetail.postcode" name="memberDetail.postcode" cssClass="inputbox"/>
            <br><s:text name="zip_postal_code_guideline"/>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="state_province"/></td>
        <td>
            <s:textfield id="memberDetail.state" name="memberDetail.state" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="country"/></td>
        <td>
            <s:select list="countryDescs" name="memberDetail.countryCode" id="memberDetail.countryCode" listKey="countryCode" listValue="countryName"/>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="gender"/></td>
        <td>
            <s:select list="genders" name="memberDetail.gender" id="memberDetail.gender" listKey="key" listValue="value"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_listing_end">
        <td colspan="4">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>

    <tbody>
    <tr class="row_header">
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th><s:text name="contact_detials"/></th>
        <th></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>


    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="telephone_number"/></td>
        <td>
            <s:textfield id="memberDetail.contact" name="memberDetail.contact" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="primary_email"/></td>
        <td>
            <div>
                <s:textfield id="memberDetail.email" name="memberDetail.email" cssClass="inputbox"/>
            </div>
            <div class="td_desc" id="fielddesc__email">
                <s:text name="primary_email_guildline"/>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="retype_your_email"/></td>
        <td>
            <s:textfield id="email2" name="email2" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td><s:text name="alternate_email"/></td>
        <td>
            <div>
                <s:textfield id="memberDetail.alternateEmail" name="memberDetail.alternateEmail" cssClass="inputbox"/>
            </div>
            <div class="td_desc" id="fielddesc__alt_email">
                <s:text name="alternate_email_guideline"/>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="retype_alternate_email"/></td>
        <td>
            <s:textfield id="alternateEmail2" name="alternateEmail2" cssClass="inputbox"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_listing_end">
        <td colspan="4">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>

<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>

    <tbody>
    <tr class="row_header">
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th><s:text name="selected_package"/></th>
        <th></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>


    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td><s:text name="package"/></td>
        <td>
            <s:textfield id="bonusPackage.packageName" name="bonusPackage.packageName" readonly="true"/>
            <s:hidden id="pid" name="pid"/>
            <s:hidden id="productCode" name="productCode"/>
            <s:textfield value="%{'USD '+ @SF@formatDecimal(bonusPackage.price)}" readonly="true"/>
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <!--<tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td></td>
        <td>
            <div style="width:350px;">
                <input type="radio" id="radio_position1_0" checked="checked" value="0" name="position1"><label for="radio_position1_2"></label>&nbsp;
                <input type="radio" id="radio_position1_1" value="1" name="position1"><label for="radio_position1_1"></label>&nbsp;
                <input type="radio" id="radio_position1_2" value="2" name="position1"> <label for="radio_position1_2"></label>
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>-->

    <tr class="tbl_listing_end">
        <td colspan="4">
            &nbsp;
        </td>
    </tr>
    </tbody>
</table>

<!--<table cellspacing="0" cellpadding="0" class="tbl_form">
    <colgroup>
        <col width="1%">
        <col width="30%">
        <col width="69%">
        <col width="1%">
    </colgroup>

    <tbody>
    <tr class="row_header">
        <th class="tbl_header_left">
            <div class="border_left_grey">&nbsp;</div>
        </th>
        <th></th>
        <th></th>
        <th class="tbl_header_right">
            <div class="border_right_grey">&nbsp;</div>
        </th>
    </tr>


    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td></td>
        <td>
            <input type="text" class="inputbox" id="nomineeName" name="nomineeName">
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td></td>
        <td>
            <input type="text" class="inputbox" id="nomineeRelationship" name="nomineeRelationship">
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    <tr class="tbl_form_row_odd">
        <td>&nbsp;</td>
        <td></td>
        <td>
            <input type="text" class="inputbox" id="nomineeIc" name="nomineeIc">
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>


    <tr class="tbl_form_row_even">
        <td>&nbsp;</td>
        <td></td>
        <td>
            <input type="text" class="inputbox" id="nomineeContactNo" name="nomineeContactNo">
            &nbsp;
        </td>
        <td>&nbsp;</td>
    </tr>

    </tbody>
</table>
<br>-->


<table cellspacing="0" cellpadding="0" class="tbl_form">
<colgroup>
    <col width="1%">
    <col width="53%">
    <col width="18%">
    <col width="3%">
    <col width="8%">
    <col width="8%">
    <col width="1%">
</colgroup>

<tbody>
<tr class="row_header">
    <th class="tbl_header_left">
        <div class="border_left_grey">&nbsp;</div>
    </th>
    <th colspan="5"><s:text name="accept_term_agreements"/></th>
    <th class="tbl_header_right">
        <div class="border_right_grey">&nbsp;</div>
    </th>
</tr>


<tr class="tbl_form_row_odd">
    <td>&nbsp;</td>
    <td colspan="5">
        <p><s:text name="term_agreements"/></p>
        <p><strong><s:text name="term_agreements2"/></strong></p>
    </td>
    <td>&nbsp;</td>
</tr>

<!--<tr class="tbl_form_row_odd">
    <td>&nbsp;</td>
    <td><input type="checkbox" class="checkbox" id="terms_bis" name="terms_bis">
        <label for="terms_bis">Terms Of Business, Trading Policies &amp; Procedures</label></td>
    <td colspan="4">
        <a target="_blank" href="/download/termsOfBusiness">Download Agreement (343 KB PDF)</a>
    </td>
    <td>&nbsp;</td>
</tr>-->

<tr class="tbl_form_row_even">
    <td>&nbsp;</td>
    <td>
        <s:checkbox id="termsRisk" name="termsRisk" cssClass="checkbox" theme="simple"/>
        <label for="termsRisk"><s:text name="mcl_rick_disclosure_statement"/></label>
    </td>
    <td colspan="4">
        <a target="_blank" href="/download/Risk-Disclosure-Statement.pdf"><s:text name="download_agreement"/> (381 KB PDF)</a>
    </td>
    <td>&nbsp;</td>
</tr>

<!--<tr class="tbl_form_row_odd">
    <td>&nbsp;</td>
    <td><input type="checkbox" class="checkbox" id="privateInvestmentAgreement" name="privateInvestmentAgreement">
        <label for="privateInvestmentAgreement">Private Investment Agreement</label></td>
    <td colspan="4">
        <a target="_blank" href="/download/privateInvestmentAgreement">Download Agreement (67 KB Doc)</a>
    </td>
    <td>&nbsp;</td>
</tr>-->

<tr class="tbl_form_row_odd">
    <td>&nbsp;</td>
    <td colspan="5">

        <p><s:text name="self_declaration"/></p>

        <table align="center" cellspacing="0" cellpadding="0" style="border-style:hidden">
            <tbody><tr style="border-style:hidden">
                <td align="right" style="border-style:hidden" class="td_1st"><s:text name="name"/>:</td>
                <td style="border-style:hidden" class="td_2nd"><s:textfield id="memberDetail.signName" name="memberDetail.signName" cssClass="inputbox"/></td>
            </tr>
            <tr>
                <td align="right" style="border-style:hidden" class="td_1st"><s:text name="date"/>:</td>
                <td style="border-style:hidden" class="td_2nd"><s:textfield id="date" name="date" cssClass="inputbox" cssStyle="background-color: #d9d9d9;" readonly="true"/></td>
            </tr>
            <tr>
                <td align="left" valign="top" style="border-style:hidden" colspan="2">
                    <s:checkbox id="memberDetail.termCondition" name="memberDetail.termCondition" theme="simple" cssStyle="float:left; margin-right:4px;"/>
                    <label><p><s:text name="self_declaration2"/></p></label>
                </td>
            </tr>
            <tr>
                <td valign="top" style="border-style:hidden" class="td_1st" colspan="2">&nbsp;</td>
            </tr>
        </tbody></table>
    </td>
    <td>&nbsp;</td>
</tr>

<tr class="tbl_listing_end">
    <td>&nbsp;</td>
    <td colspan="5" class="tbl_content_right">
        <span class="button">
            <s:submit value="%{getText('btnSubmit')}"/>
        </span>
    </td>
    <td>&nbsp;</td>
</tr>
</tbody>
</table>

</td>
</tr>

<tr>
    <td></td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>
</s:form>

<c:import url="/WEB-INF/templates/include/footer.jsp"/>