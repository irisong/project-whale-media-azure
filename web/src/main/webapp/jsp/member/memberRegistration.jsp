<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="padding">
        <div class="content_title"></div>
    </div>
    <!--<div class="content_line"></div>-->
    <br class="clear">

<script type="text/javascript" language="javascript">
$(function() {
        $("#topupForm").validate({
        messages : {
            transactionPassword: {
                remote: "<s:text name="security_password_is_not_valid"/>"
            }
        },
        rules : {
            "privateInvestmentAgreement" : {
                required : "#rdoFxgold:checked"
            },
            "mteAgreement" : {
                required : "#rdoMte:checked"
            }
            /*"transactionPassword" : {
                required : true
                , remote: "/member/verifyTransactionPassword"
            },
            "privateInvestmentAgreement" : {
                required : true
            }*/
        },
        submitHandler: function(form) {
            var epoint = $('#topup_pointAvail').autoNumericGet();
            var epointPackageNeeded = $('#epointNeeded').autoNumericGet();

            if ($("#topup_pointAvail").val() == 0 || $("#topup_pointAvail").val() == "" || parseFloat(epoint) < parseFloat(epointPackageNeeded)) {
                error("<s:text name="in_sufficient_fund_to_purchase_package"/>");
            } else {
                waiting();
                form.submit();
            }/*else {
                if ($.trim($("#transactionPassword").val()) == "") {
                    error("Security Password is empty");
                    $("#transactionPassword").focus();
                    return false;
                }
                waiting();
                form.submit();
            }*/
        }
    });
    $(".activeLink").button({
        icons: {
            primary: "ui-icon-circle-check"
        }
    }).click(function(event) {
        event.preventDefault();

        var epointNeeded = $(this).attr("ref");
        var pid = $(this).attr("pid");
        if (pid >= 5) {
            epointNeeded = $("#specialPackageId option:selected").attr("price");
            pid = $("#specialPackageId").val();
        }
        if (false) {
            epointNeeded = parseInt(epointNeeded, 10);
            epointNeeded += (epointNeeded * 10 / 100);
        }
        var sure = confirm("<s:text name="are_you_sure_want_to_purchase_this_package"/> " + epointNeeded + "?");
        if (sure) {
            $('#epointNeeded').val(epointNeeded);
            $('#pid').val(pid);
            $("#topupForm").submit();
        }
    });

    $("#rdoFxgold").click(function(event){
        $(".tdFxGold").show();
        $(".tdMte").hide();
    });
    $("#rdoMte").click(function(event){
        $(".tdFxGold").hide();
        $(".tdMte").show();
    });
});
</script>

<table cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="member_registration"/></span></td>
    </tr>
    <tr>
        <td><br><sc:displayMemberErrorMessage/></td>
    </tr>
    <tr>
        <td>
            <s:form action="memberRegistration2" id="topupForm" name="topupForm" method="post">
            <input type="hidden" id="formattedValue">
            <input type="hidden" id="pid" name="pid" value=""/>
            <input type="hidden" id="epointNeeded" value="0"/>

            <table cellspacing="0" cellpadding="0" class="tbl_form">
                <colgroup>
                    <col width="1%">
                    <col width="30%">
                    <col width="69%">
                    <col width="1%">
                </colgroup>
                <tbody>
                <tr>
                    <th class="tbl_header_left">
                        <div class="border_left_grey">&nbsp;</div>
                    </th>
                    <th colspan="2"><s:text name="package_purchase"/></th>
<!--                    <th class="tbl_content_right"></th>-->
                    <th class="tbl_header_right">
                        <div class="border_right_grey">&nbsp;</div>
                    </th>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><s:text name="cp1_account"/></td>
                    <td><s:textfield name="totalCp1" id="topup_pointAvail" size="20px" readonly="true"/></td>
                    <td>&nbsp;</td>
                </tr>

                <tr>
                    <td colspan="4">
                        <table class="pbl_table" border="1" cellspacing="0">
                            <tbody>
                            <tr class="pbl_header">
                                <td valign="middle"><s:text name="join_package"/></td>
                                <td valign="middle"><s:text name="membership"/></td>
                                <td valign="middle"><s:text name="price"/>(USD)</td>
                            </tr>

                            <s:iterator var="bonusPackages" value="bonusPackageGroups" status="iterStatus">
                                <tr class="<s:if test="#groupStatus.odd == true ">row0</s:if><s:else>row1</s:else>">

                                    <s:set var="bonusPackage" value="#bonusPackages[0]"/>

                                    <td align='center'>
                                        <a class="activeLink" ref="<s:property value="%{@SF@formatInteger(#bonusPackage.price, false)}"/>" pid="${bonusPackage.packageId}" href="#"><s:text name="sign_up"/>
                                        </a>
                                    </td>
                                    <td align='center'>${bonusPackage.packageName}</td>
                                <s:if test="%{#bonusPackages.size()==1}">
                                    <td align='center'><s:property value="%{@SF@formatDecimal(#bonusPackage.price)}"/></td>
                                </s:if>
                                <s:else>
                                    <td align='center'>
                                        <select id="specialPackageId">
                                        <s:iterator var="bonusPackage" value="#bonusPackages">
                                            <option value="<s:property value="#bonusPackage.packageId"/>" price="<s:property value="%{@SF@formatInteger(#bonusPackage.price, false)}"/>"><s:property value="%{@SF@formatInteger(#bonusPackage.price)}"/></option>
                                        </s:iterator>
                                        </select>
                                    </td>
                                </s:else>
                                </tr>
                            </s:iterator>
                            </tbody>
                        </table>
                    </td>
                </tr>

                <tr class="tbl_form_row_even" style="display: none">
                    <td>&nbsp;</td>
                    <td colspan="5">
                        &nbsp;<input name="productCode" type="radio" value="fxgold" id="rdoFxgold" checked="checked">&nbsp; <label for="rdoFxgold"><s:text name="fx_gold_a"/></label>
                        <span style="display: none">&nbsp;<input name="productCode" type="radio" value="mte" id="rdoMte">&nbsp; <label for="rdoMte"><s:text name="maxim_trade_executor"/></label></span>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td colspan="5">
                        <p><s:text name="member_registration_term"/></p>
                        <br>
                        <p><s:text name="member_registration_agree_term"/></p>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="tbl_form_row_even tdFxGold">
                    <td>&nbsp;</td>
                    <td><input type="checkbox" class="checkbox" id="privateInvestmentAgreement" name="privateInvestmentAgreement">
                        <label for="privateInvestmentAgreement"><s:text name="private_investment_agreement"/></label></td>
                    <td colspan="3">
                        <a target="_blank" href="/download/Private_Investment_Agreement.pdf"><s:text name="download_pdf"/></a>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <!--<tr class="tbl_form_row_odd tdFxGold">
                    <td>&nbsp;</td>
                    <td colspan="5">
                        <br>
                        <p> <a href="mailto:managedfund@maximtrader.com">managedfund@maximtrader.com</a>.</p>
                    </td>
                    <td>&nbsp;</td>
                </tr>-->

                <tr class="tbl_form_row_even tdMte" style="display: none">
                    <td>&nbsp;</td>
                    <td><input type="checkbox" class="checkbox" id="mteAgreement" name="mteAgreement">
                        <label for="mteAgreement"><s:text name="mte_aggrement"/></label></td>
                    <td colspan="3">
                        <a target="_blank" href="/download/mteAgreement"><s:text name="download_mte_agreement"/></a>
                    </td>
                    <td>&nbsp;</td>
                </tr>
                <tr class="tbl_form_row_odd tdMte" style="display: none">
                    <td>&nbsp;</td>
                    <td colspan="5">
                        <br>
                        <p><s:text name="please_sign_and_send_to_support"/></p>
                    </td>
                    <td>&nbsp;</td>
                </tr>


                <!--<tr class="tbl_form_row_odd">
                    <td>&nbsp;</td>
                    <td><input type="checkbox" class="checkbox" id="privateInvestmentAgreement" name="privateInvestmentAgreement">
                        <label for="privateInvestmentAgreement">Private Investment Agreement</label></td>
                    <td colspan="4">
                        <a target="_blank" href="/download/privateInvestmentAgreement">Download Agreement (67 KB Doc)</a>
                    </td>
                    <td>&nbsp;</td>
                </tr>

                <tr class="tbl_form_row_even">
                    <td>&nbsp;</td>
                    <td></td>
                    <td>
                        <input name="transactionPassword" type="password" id="transactionPassword"/>
                    </td>
                    <td>&nbsp;</td>
                </tr>-->

                </tbody>
            </table>
            </s:form>
        </td>
    </tr>
    </tbody>
</table>

<c:import url="/WEB-INF/templates/include/footer.jsp"/>