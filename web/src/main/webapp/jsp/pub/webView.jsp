<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<div class="row">
    <div class="col-sm-12 col-md-12">
        <table class="table table-striped table-forum">
            <thead>
<%--                <tr>--%>
<%--                    <h2 align="center"><b><s:property value="title" escapeHtml="true"/></b></h2>--%>
<%--                    <p align="center"><i><s:property value="date"></s:property></i></p>--%>
<%--                </tr>--%>
            </thead>
            <tbody>
<%--                <tr>--%>
<%--                    <td class="text-center" style="width: 100%;"> &nbsp; <h2><strong>--%>
<%--                        <s:text name="systemAdmin"></s:text>--%>
<%--                    </strong></h2></td>--%>
<%--                    <td><h2><s:text name="publishDate"></s:text>: <em><s:property value="date"></s:property></em></h2></td>--%>
<%--                </tr>--%>
                <tr>
<%--                    <td class="text-center" style="width: 100%;"><s:property value="content" escapeHtml="false"/></td>--%>
                    <td style="width: 100%;padding: 20px;"><s:property value="content" escapeHtml="false"/></td>
                </tr>
            </tbody>
        </table>
    </div>

</div>