<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function(){
        $.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0 && value != "";
        }, "<s:text name="no_space_please_and_dont_leave_it_empty"/>");

        $.validator.addMethod("loginRegex", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-\s\_]+$/i.test(value);
        }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

        $.validator.addMethod("latinRegex", function(value, element) {
            return this.optional(element) || /^[a-z0-9\-\s\_\/\.]+$/i.test(value);
        }, "<s:text name="this_field_only_accept_latin_word_numbers_or_dashes"/>");

        $("#registerForm").validate({
            submitHandler : function(form) {
                var sponsorMemberCode = $("#sponsorMemberCode").val();
                if(sponsorMemberCode && sponsorMemberCode.trim()!==''){

                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $.post('<s:url action="verifyActiveSponsorMemberCode" namespace="/pub/misc"/>', {
                        sponsorMemberCode : sponsorMemberCode
                    }, function(json) {

                        $("body").unmask();

                        new JsonStat(json, {
                            onSuccess: function (json) {
                                form.submit();
                            },
                            onFailure: function (json, error) {
                                messageBox.alert(error);
                            }
                        });

                    });
                } else {
                    form.submit();
                }
            }, // submitHandler
            messages: {
                confirmPassword: {
                    equalTo: "<s:text name="please_enter_the_same_password_as_above"/>"
                },
                "member.memberCode": {
                    remote: "<s:text name="username_already_in_use"/>"
                }
            },
            rules: {
                "member.memberCode": {
                    required : true,
                    noSpace: true,
                    loginRegex: true,
                    minlength : 6,
                    remote: "<s:url action="checkMemberUsernameAvailability" namespace="/pub/misc"/>"
                },
                "member.firstName": {
                    required: true
                },
                "member.lastName": {
                    required: true
                },
                "memberDetail.email": {
                    email: true
                },
                "email2" : {
                    required : true,
                    equalTo: "#memberDetail\\.email"
                },
                "password" : {
                    required : true,
                    minlength : 6
                },
                "confirmPassword" : {
                    required : true,
                    minlength : 6,
                    equalTo: "#password"
                }
                /*,
                "securityPassword" : {
                    required : true,
                    minlength : 6
                },
                "confirmSecurityPassword" : {
                    required : true,
                    minlength : 6,
                    equalTo: "#securityPassword"
                }
                */
            }
        });

        $("#btnNext").click(function(event){
           event.preventDefault();

            $("#registerForm").submit();
        });
    });
</script>

<!-- LIGHT SECTION -->
<sc:pageTitle title="registration" bgImage="false"/>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix stepsWrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="innerWrapper clearfix stepsPage">
                    <div class="row progress-wizard" style="border-bottom:0;">

                        <div class="col-xs-4 progress-wizard-step complete fullBar">
                            <div class="text-center progress-wizard-stepnum"><s:text name="membershipPackage"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>

                        <div class="col-xs-4 progress-wizard-step active">
                            <div class="text-center progress-wizard-stepnum"><s:text name="personal_information"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>

                        <div class="col-xs-4 progress-wizard-step disabled">
                            <div class="text-center progress-wizard-stepnum"><s:text name="review"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>
                    </div>

                    <s:form action="register3" name="registerForm" id="registerForm">
                        <div class="col-xs-12">
                            <div class="page-header">
                                <sc:displayMemberErrorMessage/>
                                <h4><s:text name="personal_information"/></h4>
                            </div>
                        </div>

                        <div class="form-group col-md-12 col-xs-12">
                            <label for="sponsorMemberCode"><s:text name="referrer_id"/></label>
                            <s:textfield name="sponsorMemberCode" id="sponsorMemberCode" cssClass="form-control" aria-describedby="sponsorMemberCodeHelpBlock"/>
                            <span id="sponsorMemberCodeHelpBlock" class="help-block text-danger">Leave it blank if you don't have any Referrer</span>
                        </div>

                        <div class="form-group col-md-12 col-xs-12">
                            <label for="member.memberCode"><s:text name="username"/></label>
                            <s:textfield name="member.memberCode" id="member.memberCode" cssClass="form-control"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="member.firstName"><s:text name="firstName"/></label>
                            <s:textfield name="member.firstName" id="member.firstName" cssClass="form-control"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="member.lastName"><s:text name="lastName"/></label>
                            <s:textfield name="member.lastName" id="member.lastName" cssClass="form-control"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="memberDetail.email"><s:text name="email"/></label>
                            <s:textfield name="memberDetail.email" id="memberDetail.email" cssClass="form-control"/>
                        </div>
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="email2"><s:text name="confirmEmail"/></label>
                            <s:textfield name="email2" id="email2" cssClass="form-control"/>
                        </div>

                        <div class="col-xs-12">
                            <div class="page-header">
                                <h4><s:text name="password"/></h4>
                            </div>
                        </div>

                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="password"><s:text name="password"/></label>
                            <s:password name="password" id="password" cssClass="form-control" showPassword="true"/>
                        </div>

                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="confirmPassword"><s:text name="confirmPassword"/></label>
                            <s:password name="confirmPassword" id="confirmPassword" cssClass="form-control" showPassword="true"/>
                        </div>
                        <%--
                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="securityPassword"><s:text name="security_password"/></label>
                            <s:password name="securityPassword" id="securityPassword" cssClass="form-control" showPassword="true"/>
                        </div>

                        <div class="form-group col-sm-6 col-xs-12">
                            <label for="confirmSecurityPassword"><s:text name="confirm_security_password"/></label>
                            <s:password name="confirmSecurityPassword" id="confirmSecurityPassword" cssClass="form-control" showPassword="true"/>
                        </div>
                        --%>

                        <div class="col-xs-12">
                            <div class="well well-lg clearfix">
                                <ul class="pager">
                                    <s:url var="backUrl" action="register">
                                        <s:param name="wizardLink" value="true"/>
                                    </s:url>
                                    <li class="previous"><a href="${backUrl}" id="btnBack"><s:text name="btnBack"/></a></li>
                                    <li class="next"><a href="#" id="btnNext"><s:text name="continue"/></a>
                                </ul>
                            </div>
                        </div>
                    </s:form>
                </div>
            </div>
        </div>
    </div>
</section>