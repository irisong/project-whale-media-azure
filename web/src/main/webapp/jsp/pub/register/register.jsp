<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function(){
       var packageValue = '${bonusPackage.packageId}';
       if(packageValue!=''){
           $("input[name=bonusPackage\\.packageId]").val([packageValue]);
       }

       $("#btnNext").click(function(event){
            event.preventDefault();

            if(${disabledRegister}){
                return;
            }

            if(!$("input[name=bonusPackage\\.packageId]:checked").length){
                messageBox.alert("<s:text name="pleaseSelectYourMembershipPackage"/>");
                return;
            }

            $("#registerForm").submit();
       });
    });
</script>

<!-- LIGHT SECTION -->
<sc:pageTitle title="registration" bgImage="false"/>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix stepsWrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="innerWrapper clearfix stepsPage">
                    <div class="row progress-wizard" style="border-bottom:0;">

                        <div class="col-xs-4 progress-wizard-step complete">
                            <div class="text-center progress-wizard-stepnum"><s:text name="membershipPackage"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>

                        <div class="col-xs-4 progress-wizard-step disabled">
                            <div class="text-center progress-wizard-stepnum"><s:text name="personal_information"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>

                        <div class="col-xs-4 progress-wizard-step disabled">
                            <div class="text-center progress-wizard-stepnum"><s:text name="review"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>
                    </div>

                    <s:form action="register2" name="registerForm" id="registerForm">
                        <div class="col-xs-12">
                            <div class="page-header">
                                <h4><s:text name="membershipPackage"/></h4>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">
                            <sc:displayMemberErrorMessage/>
                            <s:text name="pleaseSelectYourMembershipPackage"/><br/>
                        </div>

                        <div class="col-xs-12">
                            <div class="cartListInner">
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th><s:text name="packageName"/></th>
                                            <th><s:text name="description"/></th>
                                            <th><s:text name="price"/></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <s:iterator var="tempBonusPackage" value="bonusPackages">
                                        <tr>
                                            <td class="col-xs-1">
                                                <div class="radio">
                                                    <label>
                                                        <input type="radio" name="bonusPackage.packageId" value="${tempBonusPackage.packageId}" class="radiobox style-0"><span></span>
                                                    </label>
                                                </div>
                                            </td>
                                            <td class="col-xs-4">${tempBonusPackage.packageName}</td>
                                            <td class="col-xs-5">${tempBonusPackage.packageDesc}</td>
                                            <td class="col-xs-2">$ <s:property value="%{@SF@formatDecimal(#tempBonusPackage.unitPrice)}"/></td>
                                        </tr>

                                        </s:iterator>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12">
                            <div class="well well-lg clearfix">
                                <ul class="pager">
                                    <li class="previous"><a href="#" class="hideContent">back</a></li>
                                    <li class="next"><a href="#" id="btnNext"><s:text name="continue"/></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </s:form>
                </div>
            </div>
        </div>
    </div>
</section>