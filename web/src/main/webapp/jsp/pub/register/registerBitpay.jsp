<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<!-- LIGHT SECTION -->
<sc:pageTitle title="registration" bgImage="false"/>

<section class="mainContent clearfix setp5">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <sc:displayMemberErrorMessage/>
                <div class="thanksContent">
                    <h2><s:text name="registrationSuccess"/>
                        <small style="text-transform: none"><s:text name="youWillReceiveAnEmailOfYourRegistrationForBitcoinPayment"/></small>
                        <small style="text-transform: none; color: red"><s:text name="youWillReceiveAnEmailOfYourRegistrationForBitcoinPayment2"/></small>
                    </h2>
                    <h3><s:text name="personal_information"/></h3>
                    <div class="thanksInner">
                        <div class="row">
                            <div class="col-sm-6 col-xs-12 tableBlcok">
                                <address>
                                    <span><s:text name="username"/>:</span> ${member.memberCode}<br/>
                                    <span><s:text name="fullname"/>:</span> ${member.fullName}<br/>
                                    <span><s:text name="email"/>:</span> ${memberDetail.email}<br/>
                                    <br/>
                                    <span><s:text name="membershipPackage"/>:</span> ${bonusPackage.packageName}<br/>
                                    <span><s:text name="price"/>:</span> <s:property value="%{@SF@formatDecimal(bonusPackage.unitPrice)}" />
                                </address>
                            </div>
                            <div class="col-sm-6 col-xs-12">
                                <div class="well">
                                    <h2><small><s:text name="username"/></small>${member.memberCode}</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>