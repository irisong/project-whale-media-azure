<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    var disabledNextButton = false;

    $(function(){
        $("#btnNext").click(function(event){
            if(disabledNextButton){
                return;
            }
            event.preventDefault();
            disabledNextButton = !disabledNextButton;
            $("#registerForm").submit();
        });
    });
</script>

<!-- LIGHT SECTION -->
<sc:pageTitle title="registration" bgImage="false"/>

<!-- MAIN CONTENT SECTION -->
<section class="mainContent clearfix stepsWrapper">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="innerWrapper clearfix stepsPage">
                    <div class="row progress-wizard" style="border-bottom:0;">

                        <div class="col-xs-4 progress-wizard-step complete fullBar">
                            <div class="text-center progress-wizard-stepnum"><s:text name="membershipPackage"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>

                        <div class="col-xs-4 progress-wizard-step complete fullBar">
                            <div class="text-center progress-wizard-stepnum"><s:text name="personal_information"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>

                        <div class="col-xs-4 progress-wizard-step active">
                            <div class="text-center progress-wizard-stepnum"><s:text name="review"/></div>
                            <div class="progress"><div class="progress-bar"></div></div>
                            <div class="progress-wizard-dot"></div>
                        </div>
                    </div>

                    <s:form action="register4" name="registerForm" id="registerForm">
                        <div class="col-xs-12">
                            <div class="page-header">
                                <sc:displayMemberErrorMessage/>
                                <h4><s:text name="review"/></h4>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><s:text name="membershipPackage"/></h4>
                                </div>
                                <div class="panel-body" style="padding: 15px">
                                    <address>
                                        <span><s:text name="packageName"/>: ${bonusPackage.packageName}</span><br/>
                                        <span><s:text name="price"/>: <s:property value="%{@SF@formatDecimal(bonusPackage.unitPrice)}" /></span><br/>
                                        <s:if test="%{bitcoinAmount > 0}">
                                            <span><s:text name="bitcoinPrice"/>: <b><s:property value="%{@SF@formatCosting(bitcoinAmount)}"/> BTC</b>&nbsp;&nbsp;<sub class="text-danger">( 1 BTC = <s:property value="%{@SF@formatForex(bitcoinRate)}"/> USD )</sub></span><br/>
                                        </s:if>
                                    </address>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-xs-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title"><s:text name="personal_information"/></h4>
                                </div>
                                <div class="panel-body" style="padding: 15px">
                                    <address>
                                        <s:if test="hasSponsorCode">
                                            <span><s:text name="referrer_id"/>: ${sponsorMemberCode}</span><br/>
                                        </s:if>
                                        <span><s:text name="username"/>: ${member.memberCode}</span><br/>
                                        <span><s:text name="firstName"/>: ${member.firstName}</span><br/>
                                        <span><s:text name="lastName"/>: ${member.lastName}</span><br/>
                                        <span><s:text name="email"/>: ${memberDetail.email}</span><br/>
                                    </address>
                                </div>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="well well-lg clearfix">
                                <ul class="pager">
                                    <s:url var="backUrl" action="register2">
                                        <s:param name="wizardLink" value="true"/>
                                    </s:url>
                                    <li class="previous"><a href="${backUrl}" id="btnBack"><s:text name="btnBack"/></a></li>
                                    <li class="next"><a href="#" id="btnNext"><s:text name="btnConfirm"/></a>
                                </ul>
                            </div>
                        </div>
                    </s:form>
                </div>
            </div>
        </div>
    </div>
</section>