<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script>
$(function(){
  $("#language").change(function(event){
    $("#request_locale").val($("#language").val());
    var f = $("#loginForm");
    f.attr("action", "login.php");
    f.submit();
  });

  $("#captchaImage").click(function(event){
     $(this).attr("src", "/simpleCaptcha.png?timestamp=" + new Date().getTime());
  });
});
</script>

<div class="middle-box text-center loginscreen animated fadeInDown">

  <div>
    <a href="/"><h1 class="logo-name">Whalelive Media</h1></a>
  </div>

  <!-- Login -->
  <div id="login-box" class="widget-box visible">

    <s:if test="hasActionErrors()">
      <div id="global_error" class="alert alert-danger m-b-sm">
        <s:actionerror /><s:property value="exception.message" />
      </div>
    </s:if>

    <s:if test="%{#parameters['captchaFailed']}">
      <div id="global_error" class="alert alert-danger m-b-sm">
        <s:text name="errorMessage.invalid.security.code"/>
      </div>
    </s:if>

    <s:if test="%{#parameters['loginFailed']}">
      <div id="global_error" class="alert alert-danger m-b-sm">
        <s:text name="errorMessage.invalid.username.password"/>
      </div>
    </s:if>

    <br/>

    <h4><s:text name="pleaseEnterYourInformation"/></h4>
    <form id="loginForm" action="/login" class="m-t" method="post">
      <div class="form-group">
        <s:textfield name="username" id="username" cssClass="form-control" placeholder="%{getText('username')}" required="" />
      </div>
      <div class="form-group">
        <s:password name="password" id="password" cssClass="form-control" placeholder="%{getText('password')}" required="" />
      </div>
      <div class="form-group form-inline input-group">
        <input type="text" name="securityCode" id="securityCode" class="form-control" placeholder="<s:text name="securityCode"/>" required="" /> 
        <span class="input-group-addon" style="padding: inherit;"> 
          <img id="captchaImage" src="<s:url value="/simpleCaptcha.png"/>" height="33" width="80" />
        </span>
      </div>
      <div class="form-group form-inline input-group">
        <span class="input-group-addon" style="padding: inherit; background:none; border:0px; padding:10px;"> 
          <label class="control-label">
            <s:text name="language" />
          </label>
        </span>
        <s:select list="languages" listKey="key" listValue="value" key="language" id="language" cssClass="form-control" />
        <input type="hidden" name="request_locale" id="request_locale" />
      </div>
      <s:submit type="button" id="btnLogin" cssClass="btn btn-primary block full-width m-b">
        <s:text name="btnLogin" />
      </s:submit>
      <!-- hide forgot password -->
      <!--
      <div>
        <a href="#" onclick="show_box('forgot-box'); return false;"> 
          <small class="font-bold">Forgot password?</small>
        </a>
      </div>
      -->
    </form>
    <p class="m-t">
      <small>Whale Media &copy; 2020</small>
    </p>
  </div>

  <!-- forgot password -->
  <!-- hide forgot password -->
  <!--
  <div id="forgot-box" class="widget-box panel-danger">
    <h4>Please Enter Your Information.</h4>
    <form class="m-t" role="form">
      <div class="form-group form-inline-static">
        <p>Enter your email and to receive instructions</p>
      </div>
      <div class="form-group">
        <input type="email" class="form-control" placeholder="Email" required="" />
      </div>
      <button onclick="return false;" class="btn btn-warning block full-width m-b">Send Me!</button>
      <div>
        <a href="#" onclick="show_box('login-box'); return false;">
          <small class="font-bold">Back to login</small>
        </a>
      </div>
    </form>
    <p class="m-t">
      <small>IN+ Project Shop &copy; 2017</small>
    </p>
  </div>
  -->
</div>

<script type="text/javascript">
  function show_box(id) {
    $('.widget-box.visible').removeClass('visible');
    $('#'+id).addClass('visible');
  }
</script>
