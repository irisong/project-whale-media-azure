<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<!-- LIGHT SECTION -->
<sc:pageTitle title="accessDenied" bgImage="false"/>

<section class="mainContent clearfix">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h4 class="sub-title"><s:text name="accessDenied"/></h4>
                <div class="well">
                    <s:if test="admin">
                        <s:text name="accessDeniedAdminMsg">
                            <s:param>${username}</s:param>
                        </s:text>
                        <br/>
                        <s:text name="accessDeniedAdminMsg2"/>
                        <br/><br/>

                        <s:url var="loginUrl" action="memberLogin" namespace="/pub/shop"/>
                        <a href="${loginUrl}" class="btn btn-primary-filled btn-rounded">
                            <s:text name="loginAsMember"/>
                        </a>
                    </s:if>
                    <s:elseif test="member">
                        <s:text name="accessDeniedMemberMsg">
                            <s:param>${username}</s:param>
                        </s:text>
                        <br/>
                        <s:text name="accessDeniedMemberMsg2"/>
                        <br/><br/>

                        <s:url var="loginUrl" action="login" namespace="/pub/login"/>
                        <a href="${loginUrl}" class="btn btn-primary-filled btn-rounded">
                            <s:text name="loginAsAdmin"/>
                        </a>
                    </s:elseif>
                </div>
            </div>
        </div>
    </div>
</section>



