<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/asset/whale_media/asset/js/common-jquery-struts2.js"/>" type="text/javascript" ></script>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild.php">公会管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild_casting.php">招募管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_income_report.php">月入报告</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">资料管理</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="wl_change_profile.php" >更换资料</a>
                        <a class="dropdown-item" href="wl_change_password.php">更换密码</a>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="btn btn-secondary" href="/logout" title="Sign Out" data-action="userLogout">登出</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>


            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1" style="margin-top: -10%">
        <div class="container-fluid body py-6">
            <div class="main container card-header" style="background-color:#400B75 ;color:#ffffff">
                <h2 class="text-left">招募资料管理</h2>
            </div>
            <div class="main container card-header">
                <s:form action="guildEditCasting" name="castingForm" id="castingForm" cssClass="form-horizontal">
                    <fieldset>
                        <div id="legend">
                            <legend class="" style="margin-top:2%;margin-bottom:2%;font-size: 22px">个人资料</legend>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="testLiveTimestamp">试播日期</label>
                                <input type="datetime-local" class="form-control casting" id="testLiveTimestamp" name="testLiveTimestamp" value="">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="whaleLiveId">鲸娱号</label>
                                <input type="text" class="form-control casting" id="whaleLiveId" name="whaleLiveId"  readonly value="${casting.whaleLiveId}">
                            </div>
                            <div class="col-sm-6">
                                <label for="whaleLiveName">鲸娱用户名</label>
                                <input type="text" class="form-control casting" id="whaleLiveName" name="whaleLiveName" readonly value="${casting.whaleliveProfileName}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="fullName">姓名</label>
                                <input type="text" class="form-control casting" id="fullName" name="fullName" value="${casting.fullName}">
                            </div>
                            <div class="col-sm-6">
                                <label for="identityNo">身份证号码</label>
                                <input type="text" class="form-control casting" id="identityNo" name="identityNo" value="${casting.identityNo}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="phoneNo">联系号码</label>
                                <input type="text" class="form-control casting" id="phoneNo" name="phoneNo" value="${casting.phoneNo}">
                            </div>
                            <div class="col-sm-6">
                                <label for="phoneModel">手机型号</label>
                                <input type="text" class="form-control casting" id="phoneModel" name="phoneModel" value="${casting.phoneModel}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="gender" >性别:</label>
                                <select name="gender" id="gender" class="form-control casting">
                                    <option value="M" selected="">男</option>
                                    <option value="F" selected="">女</option>
                                </select>

                            </div>
                            <div class="col-sm-6">
                                <label for="age">年龄</label>
                                <input type="text" class="form-control casting" id="age" name="age" value="${casting.age}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="email">电邮地址</label>
                                <input type="text" class="form-control casting" id="email" name="email" value="${casting.email}">
                            </div>
                            <div class="col-sm-6">
                                <label for="location">所在城市</label>
                                <input type="text" class="form-control casting" id="location" name="location" value="${casting.location}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="fbId">Facebook</label>
                                <input type="text" class="form-control casting" id="fbId" name="fbId" value="${casting.fbId}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="igId">Instagram</label>
                                <input type="text" class="form-control casting" id="igId" name="igId" value="${casting.igId}">
                            </div>
                            <div class="col-sm-6">
                                <label for="igFans">Instagram Fans/Follower</label>
                                <input type="text" class="form-control casting" id="igFans" name="igFans" value="${casting.igFan}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="tiktokId">抖音号</label>
                                <input type="text" class="form-control casting" id="tiktokId" name="tiktokId" value="${casting.tiktokId}">
                            </div>
                            <div class="col-sm-6">
                                <label for="tiktokFans">抖音 Fans/Follower</label>
                                <input type="text" class="form-control casting" id="tiktokFans" name="tiktokFans" value="${casting.tiktokFan}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="weiboId">微博号</label>
                                <input type="text" class="form-control casting" id="weiboId" name="weiboId" value="${casting.weiboId}">
                            </div>
                            <div class="col-sm-6">
                                <label for="weiboFans">微博 Fans/Follower</label>
                                <input type="number" class="form-control casting" id="weiboFans" name="weiboFans" value="${casting.weiboFan}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="url">云端硬盘链接</label>
                                <input type="text" class="form-control casting" id="url" name="url" value="${casting.url}" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="talent">才艺</label>
                                <input type="text" class="form-control casting" id="talent" name="talent" value="${casting.talent}">
                            </div>
                        </div>

                        <div class="control-group">
                            <button id="btnSubmit" type="submit" class="btn btn-success btn-block" style="border-radius: 30px;margin-top: 3%"><s:text name="btnSave"/></button>
                            <button id="btnExit" class="btn btn-danger btn-block btnExit" style="border-radius: 30px;margin-top: 3%"><s:text name="btnExit"/></button>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2">
                <h5>菜单</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_recharge.php">充值</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 mt-2">
                <h5>社交媒体</h5>
                <ul class="nav">
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 mt-2">
                <h5>联络我们</h5>
                <table>
                    <tbody>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-phone mr-1"></i></td>
                        <td style="color:white">+6014-711 2439</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-envelope mr-1"></i></td>
                        <td style="color:white">corporate@whalemediamy.com</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fab fa-weixin mr-1"></i></td>
                        <td style="color:white">whalelive01</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-map-marker-alt mr-1"></i></td>
                        <td style="color:white">(Level 8) 159, Jalan Temple, Pjs 8, 46050, Petaling jaya, Selangor.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    $("#castingForm").validate({
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: defaultJsonCallback

            });
        },rules : {
            "email" : {
                required : true,
                email: true,
                minlength : 5
            },
            "fullName" : {
                required : true
            },
            "identityNo" : {
                required : true,
                minlength : 10
            },
            "talent" : {
                required : true,
            },
            "age" : {
                required : true,
                maxlength : 3,
                min: 1
            },
            "phoneNo" : {
                required : true,
                minlength : 10
            },
            "gender" : {
                required : true,
            },
            "igFans" : {
                min : 0,
            },
            "weiboFans" : {
                min : 0,
            },
            "tiktokFans" : {
                min : 0,
            }
        }
    });

    var delayTime;

    /*        $('#btnSubmit').click(function () {
                $.ajax({
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    url: "<s:url action="wl_casting"/>",
                cache: false,
                data: {
                    whaleliveId: $('#whaleLiveId').val(),
                    fullName: $('#fullName').val(),
                },
                success: function (data, status, xhr) {
                    if (data == null || data == "" || data.actionErrors == "") {
                        window.location.reload();
                    } else {

                    }
                }
            });
        });*/

    $(document).on("click", ".btnExit", function (event) {
        $("#castingForm").rules("remove");
        $("#castingForm").attr("action", "<s:url action="wl_guild_casting"/>")
        $("#castingForm").submit();
    });


    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });

    // function defaultJsonCallback(json) {
    //     new JsonStat(json, {
    //         onSuccess: function (json) {
    //             alert("Add Casting Successful");
    //         },
    //         onFailure: function (json, error) {
    //             alert(error);
    //         }
    //     });
    // }

    function defaultJsonCallback(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                window.location = "<s:url action="wl_guild_casting"/>"
            },
            onFailure: function (json, error) {
                alert(error);
            }
        });
    }

    function init() {
        var val = "${casting.gender}";
        var time = "${casting.testLiveDate}";
        //split time with space
        time = time.replace(" ","T");
        document.getElementById("testLiveTimestamp").value = time;

        $('#gender').val("${casting.gender}");
        $('#gender option[value=' + val + ']').attr(selected,"selected");

    }
    window.onload = init();

</script>

<style>

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }


    .recharge-1 {
        padding-top: 18.3rem;
        background-color: #400b75;
        color: #0b0b0b;
    }

    .main.container { min-height: calc(10% - 1.1rem); margin-bottom: 0; margin-top: 0.1rem; padding-bottom: 0.2rem;background-color: #FFFFFF }


    .recharge-1 .body .form-control {
        background-color: #ffffff;
        border: solid 1px #0b0b0b;
        border-radius: 0rem;
        padding: 0rem;
        text-align: center;
        color:#0b0b0b;
    }

    form-horizontal .control-group{margin-bottom:20px;*zoom:1;}.form-horizontal .control-group:before,.form-horizontal .control-group:after{display:table;content:"";line-height:0;}
    .form-horizontal .control-group:after{clear:both;}
    .form-horizontal .control-label{float:left;width:160px;padding-top:5px;}
    .form-horizontal .controls{*display:inline-block;*padding-left:20px;margin-left:180px;*margin-left:0;}.form-horizontal .controls:first-child{*padding-left:180px;}


</style>


