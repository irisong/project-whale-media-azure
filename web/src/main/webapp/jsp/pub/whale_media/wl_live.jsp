<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<body>
<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>
        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_casting.php">网络艺人招募</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_recharge.php">充值</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_download.php">应用程序</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_login.php">登入</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>

            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="live-1">
        <div id="carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="<c:url value="/asset/whale_media/asset/banner/2.jpg"/>" class="img">
                </div>
                <div class="carousel-item">
                    <img src="<c:url value="/asset/whale_media/asset/banner/2.jpg"/>" class="img">
                </div>
                <div class="carousel-item">
                    <img src="<c:url value="/asset/whale_media/asset/banner/2.jpg"/>" class="img">
                </div>
            </div>
        </div>

        <div class="body">
            <div class="content">
                <h1 class="big">鲸娱直播</h1>
                <h2>就算全世界都抛弃你 我们。。。</h2>
                <h2>都会在这里等你</h2>
            </div>

            <span class="circle-1"></span>
            <span class="circle-2"></span>

        </div>
    </section>

    <section class="live-2">
        <div class="container py-5 text-center">
            <h3>直播与客群平台</h3>
            <p>全马最全面娱乐直播</p>

            <div class="grid medium-4 xsmall-2">
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/9.png"/>" class="img">
                    <h4 class="title">网红聊天直播室</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/10.png"/>" class="img">
                    <h4 class="title">艺人才艺直播</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/11.png"/>" class="img">
                    <h4 class="title">PK礼物直播</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/12.png"/>" class="img">
                    <h4 class="title">电商带货直播</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/13.png"/>" class="img">
                    <h4 class="title">口碑导购推荐</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/14.png"/>" class="img">
                    <h4 class="title">广告推广发布</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/15.png"/>" class="img">
                    <h4 class="title">企业号购物商城</h4>
                </div>
            </div>
        </div>
    </section>

    <section class="live-3">
        <div class="container py-3">
            <div class="row">
                <div class="col-md-6 my-auto">
                    <div class="grid gap-2 xsmall-2">
                        <img src="<c:url value="/asset/whale_media/asset/misc/4.jpg"/>" class="img-fluid">
                        <img src="<c:url value="/asset/whale_media/asset/misc/5.jpg"/>" class="img-fluid">
                        <img src="<c:url value="/asset/whale_media/asset/misc/6.jpg"/>" class="img-fluid">
                        <img src="<c:url value="/asset/whale_media/asset/misc/7.jpg"/>" class="img-fluid">
                    </div>
                </div>
                <div class="col-md-6 body">
                    <img src="<c:url value="/asset/whale_media/asset/misc/8.png"/>" class="img-fluid">
                    <div class="content">
                        <h1>加入直播行业的手机界面</h1>
                        <p>网红聊天直播室</p>
                        <p>艺人才艺直播</p>
                        <p>PK礼物直播</p>
                        <p>电商带货直播</p>
                        <p>口碑导购推荐</p>
                        <p>广告推广发布</p>
                        <p>企业号购物商城</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="live-4">
        <div class="container py-5">
            <h3 class="text-center mb-3">WHALE LIVE 鲸娱<br>电商直播概念</h3>

            <div class="grid gap-5 medium-3 xsmall-2">
                <div class="item">
                            <span class="circle">
                                <span class="number">1</span>
                            </span>
                    <img src="<c:url value="/asset/whale_media/asset/app/1.png"/>" class="img-fluid">
                </div>
                <div class="item">
                            <span class="circle">
                                <span class="number">2</span>
                            </span>
                    <img src="<c:url value="/asset/whale_media/asset/app/2.png"/>" class="img-fluid">
                </div>
                <div class="item">
                            <span class="circle">
                                <span class="number">3</span>
                            </span>
                    <img src="<c:url value="/asset/whale_media/asset/app/3.png"/>" class="img-fluid">
                </div>
                <div class="item">
                            <span class="circle">
                                <span class="number">4</span>
                            </span>
                    <img src="<c:url value="/asset/whale_media/asset/app/4.png"/>" class="img-fluid">
                </div>
                <div class="item">
                            <span class="circle">
                                <span class="number">5</span>
                            </span>
                    <img src="<c:url value="/asset/whale_media/asset/app/5.png"/>" class="img-fluid">
                </div>
                <div class="item">
                            <span class="circle">
                                <span class="number">6</span>
                            </span>
                    <span class="number"></span>
                    <img src="<c:url value="/asset/whale_media/asset/app/6.png"/>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2">
                <h5>菜单</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_recharge.php">充值</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 mt-2">
                <h5>社交媒体</h5>
                <ul class="nav">
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 mt-2">
                <h5>联络我们</h5>
                <table>
                    <tbody>
                    <tr>
                        <td class="align-top"><i class="fas fa-phone mr-1"></i></td>
                        <td>+6014-711 2439</td>
                    </tr>
                    <tr>
                        <td class="align-top"><i class="fas fa-envelope mr-1"></i></td>
                        <td>corporate@whalemediamy.com</td>
                    </tr>
                    <tr>
                        <td class="align-top"><i class="fab fa-weixin mr-1"></i></td>
                        <td>whalelive01</td>
                    </tr>
                    <tr>
                        <td class="align-top"><i class="fas fa-map-marker-alt mr-1"></i></td>
                        <td>(Level 8) 159, Jalan Temple, Pjs 8, 46050, Petaling jaya, Selangor.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</footer>

<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>

<script>
    function initLang() {
        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });
</script>
</body>
</html>
