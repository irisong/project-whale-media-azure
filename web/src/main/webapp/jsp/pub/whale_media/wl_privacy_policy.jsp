<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!doctype html>
<html>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <style>
        .container {
            padding-right: 50px;
            padding-left: 50px;
        }

        ol {
            padding-left: 20px;
        }

        ul {
            padding-left: 20px;
        }

        li {
            margin-bottom: 10px;
            text-align: justify;
        }

        p {
            text-align: justify;
        }

        .media-4:after {
            transform: skewY(-1.70deg);
        }
    </style>
</head>

<body>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<main>
    <s:if test="language=='zh'">
        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h3 style="padding-top:40px;">用户隐私政策</h3>
            </div>
        </section>

        <section class="live-2" style="padding-top:20px">
            <div class="container py-1">
                <p>Whale Live致力于保护用户的隐私和个人信息。</p>
                <p>当您选择使用Whale Live提供的服务时，您同意Whale Live可能会收集和使用您的信息。Whale Live将根据本用户隐私政策(“政策”)收集和保护您的信息。</p>
                <p>当您使用Whale Live提供的任何服务时，您同意允许Whale Live根据本政策收集、使用和保护您的隐私和个人信息。</p>
            </div>
            <div class="container py-2">
                <h5>Whale Live 信息收集</h5>
                <p>Whale Live 在提供服务时可收集及使用下列资料。您同意，如果您未能提供任何相关信息，您可能无法享受Whale Live提供的全方位服务和利益。</p>
                <ol>
                    <li>
                        <b>您提供给 Whale Live 的信息</b>
                        <p>当您在Whale Live注册帐户时，您同意提供您的个人信息，包括但不限于您的姓名、性别、出生日期、身份证号码、联系电话、电子邮件地址、银行卡号等。由于Whale Live是基于您的移动地理位置的服务提供商，您需要在注册时授权Whale Live访问您的地理位置信息。您对Whale Live账户的完整注册将被视为同意并授权Whale Live访问您的地理位置信息，为您提供其服务。</p>
                    </li>
                    <li>
                        <b>其他信息</b>
                        <p>当您使用Whale Live提供的任何服务时，Whale Live可能会进一步收集以下信息：</p>
                        <ul style="list-style-type:disc;">
                            <li style="text-align: justify">日志信息：使用Whale Live服务及其系统可能包括通过cookie、web信标或其他方式自动收集的技术信息。此信息可能包括但不限于以下内容：您的移动设备制造、型号、版本和移动设备使用的识别码，以及IP地址。</li>
                            <li style="text-align: justify">地理位置信息：指在使用任何需要访问您的地理位置的Whale Live服务时所需的相关地理位置信息。</li>
                        </ul>
                    </li>
                </ol>
            </div>

            <div class="container py-2">
                <h5>Whale Live保护您的个人信息</h5>
                <p>Whale Live将努力采取各种安全技术和程序保护您的个人信息，包括但不限于SSL、加密存储、地理位置去模糊化等，以保护或防止信息泄露、损坏或丢失。然而，由于技术的不断变化和其他可能的技术风险，Whale Live不能确保所有的信息都是绝对安全的，而是于尽最大努力加强对Whale Live的安全。</p>
            </div>

            <div class="container py-2">
                <h5>其他</h5>
                <p>全国/城市/区域用户数据及解释分享排行榜：</p>
                <ul style="list-style-type:disc;">
                    <li>收集用户发送礼物、接收礼物、分享、关注列表、点赞、观看直播等互动行为，进行解读，提升用户体验。</li>
                    <li>Whale Live会将该等数据显示在用户体验列表中，如送礼物列表、收礼物列表、粉丝列表、喜欢列表、主播列表、分享列表等。</li>
                    <li>此外，此类数据还应用于广播电视名人堂名单，如每日贡献名单的显示；出资总额清单;共享列表；总播放时间列表等。</li>
                </ul>
            </div>
        </section>
    </s:if>
    <s:else>
        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h3 style="padding-top:40px;">USER PRIVACY POLICY</h3>
            </div>
        </section>

        <section class="live-2" style="padding-top:20px">
            <div class="container py-1">
                <p>Whale Live is committed to the protection of Whale Live's users' privacy and personal information.</p>
                <p>When you opt to use the services provided by Whale Live, you agree that Whale Live may collect and use your information. Whale Live shall collect and protect your information in accordance to this User Privacy Policy (“Policy”).</p>
                <p>When you use any of the services provided by Whale Live, you agree to allow Whale Live to collect, use and protect your privacy and personal information according to this Policy.</p>
            </div>
            <div class="container py-2">
                <h5>Collection of Information by Whale Live</h5>
                <p>Whale Live may collect and use the following information when providing its services. You agree that should you fail to provide any relevant information, you may not be able to enjoy the full-range services and benefits provided by Whale Live.</p>
                <ol>
                    <li>
                        <b>The information you provide to Whale Live</b>
                        <p>When you register an account with Whale Live, you agree to provide your personal information, including but not limited to your name, gender, date of birth, identification card number, contact number, email address, bank card number, and etc. As Whale Live is a service provider that is based on your mobile geographic location, you are required to authorise Whale Live access information of your geographic location upon registration. Your complete registration of a Whale Live account shall be deemed an agreement and authorisation to Whale Live to access to your geographic location information to provide its services to you.</p>
                    </li>
                    <li>
                        <b>Others information</b>
                        <p>When you use any services provided by Whale Live, Whale Live may further collect the following information:</p>
                        <ul style="list-style-type:disc;">
                            <li>Log information: The use of Whale Live services and its system may include technical information that is automatically collected through cookies, web beacon or other ways. This information may include but not limited to the following: your mobile device make, model, version and identification code used by mobile devices, as well as the IP address.</li>
                            <li>Geographic location information: Means the relevant geographic location information for the use of any Whale Live services which requires the access of your geographic location.</li>
                        </ul>
                    </li>
                </ol>
            </div>

            <div class="container py-2">
                <h5>Whale Live protects your personal information</h5>
                <p>Whale Live shall strive to take all kinds of security technologies and procedures to protect your personal information, including but not limited to SSL, encryption storage, defuzzification of geographic location for the purposes of protection or prevention of information leaks, damage or loss. Nevertheless, due to the ever-changing of technology and other possible technological risks, Whale Live cannot ensure that all information is absolutely safe but shall endeavour to enhance its security to Whale Live’s best possible effort.</p>
            </div>

            <div class="container py-2">
                <h5>Others</h5>
                <p>Nationwide/City/Region league table of user's data and explanation sharing:</p>
                <ul style="list-style-type:disc;">
                    <li>Users' interactive behaviour such as sending gift, receiving gift, sharing, follow list, followers list, likes, watching live stream is also collected to be interpreted and enhance the user's experience and.</li>
                    <li>Whale Live shall display such data on the list of user's experiences such as list of sending gift, list of receive gift, list of fans, list of likes, list of broadcaster, list of sharing and etc.</li>
                    <li>In addition, such data shall also be used on the list of broadcasters' hall of fame such as the display of list of daily contribution; list of total contribution; list of sharing; list of total duration broadcasted and etc.</li>
                </ul>
            </div>
        </section>
    </s:else>
</main>

<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd. All Rights Reserved.
    </div>
</footer>

</body>
</html>
