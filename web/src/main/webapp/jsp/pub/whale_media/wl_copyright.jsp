<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!doctype html>
<html>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <style>
        .container {
            padding-right: 50px;
            padding-left: 50px;
        }

        ol {
            padding-left: 20px;
        }

        ul {
            padding-left: 20px;
        }

        li {
            margin-bottom: 10px;
        }

        .media-4:after {
            transform: skewY(-1.70deg);
        }
    </style>
</head>

<body>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<main>
    <s:if test="language=='zh'">
    <section class="media-4">
        <div class="text-center" style="height:80px">
            <h3 style="padding-top:40px;">版权政策</h3>
        </div>
    </section>

    <section class="live-2" style="padding-top:20px">
        <div class="container py-1">
            <h5>Whale Live 用户</h5>
            <p align="justify">Whale Live 尊重并坚持知识产权精神。</p>
            <p align="justify">Whale Live 用户须对所有发布的内容负责，以确保发布的内容不侵犯任何第三方的权利，并防止违反任何其他适用的法律或法规。</p>
            <p align="justify">Whale Live提供的服务中的所有文本、数据、图像、图形、音频和/或视频信息和其他材料都是Whale Live的财产，受版权、商标和/或其他产权法的保护。本政策中的任何内容均不应解释为向其用户授予Whale Live任何知识产权或此类材料的任何许可。</p>
            <p align="justify">通过使用和/或上传任何直播内容或其他内容通过 Whale Live 网站公开进接地区, 您给 Whale Live 其分许可的授权。其分许可的授权将会是免费的, 永久的、不可撤销的、非排他性和完全分许可权利和许可, 没有任何领土或时间限制, 不需要任何批准和/ 或补偿, 使用, 复制, 修改, 适应, 出版、翻译、编辑、处理、创造衍生物的作品, 分发, 执行并公开展示该等内容(全部或部分)，以及/或将该等内容合并到现有或未来的工作、媒体或技术形式中，前提是该等内容是用户的原创作品，且不侵犯任何第三方的权利。</p>
        </div>
        <div class="container py-2">
            <h5>违反本政策的侵权通知:</h5>
            <p align="justify">此政策阐述 Whale Live 如何回应其用户侵犯版权的指控。因此，请注意本政策只适用于版权侵权的指控。</p>
            <p align="justify">任何其他涉嫌侵犯知识产权或专有权利的行为(如商标侵权或内容不当等违反Whale Live用户协议的行为，请参阅Whale Live用户协议(包括任何其他相关适用政策)或通过admin@whalemediamy.com联系我们。</p>
            <p align="justify">如果我们收到怀疑侵犯版权的通知("通知")，我们会采取合理的步骤通知被指称侵犯版权的用户。</p>
            <p align="justify">该侵权通知应由申诉人通过电子邮件提交。</p>
            <p align="justify">请注意，我们可根据法律要求或为保护我们的利益而合理要求，保留或向任何人提供任何通知的副本。同时我们也不会评估或调查是否发生了侵犯版权的情况。</p>
            <p align="justify">任何争议都应由当事人通过法律程序解决。</p>
            <p align="justify">在提交通知后，Whale Live也许从相关的Whale Live服务中删除相关材料。</p>
            <p align="justify">在提交任何通知之前，请仔细考虑所有涉嫌侵权的因素，特别是如果您不确定您是否是版权作品的合法拥有者。在这种情况下， 我们建议您向您的律师咨询 免得发出虚假或恶意的通知，造成法律责任与后果。</p>
            <h5>通知及其内容的提交</h5>
            <p align="justify">提交通知时，你必须是资料版权拥有人(或版权拥有人的授权代理人)，才可提交与该等涉嫌侵权有关的通知。</p>
            <p align="justify">该通知必须是书面的，并且必须包含足够的细节，以提供合理的、令人满意的证据，证明您或您的委托人(如果您是授权代理人)是该版权作品的所有者。</p>
            <p align="justify">请同时向我们提供资料以支持您的侵权主张，并提供任何其他信息，以便我们能够充分识别和定位侵权指控。</p>
            <p align="justify">还请在您的通知尾部包括以下声明:</p>
            <p align="justify">我/我们真诚地相信，如上述所述，对 Whale Live 所包含的版权内容的使用，没有得到版权所有者及其代理的授权，或受到法律的保护。" 本人/吾等保证在作伪证的情况下，本通知所载资料均属准确无误，并保证本人/吾等是版权所有人或被授权代表据称受到侵犯的专有权行事。" </p>
            <p align="justify">您的通知还必须包括您的联系地址，联系电话和电子邮件地址。</p>
            <p align="justify">完成上述要求后，请将完整通知电邮至 : admin@whalemediamy.com</p>
        </div>
    </section>
    </s:if>
<s:else>
    <section class="media-4">
        <div class="text-center" style="height:80px">
            <h3 style="padding-top:40px;">COPYRIGHT POLICY</h3>
        </div>
    </section>

    <section class="live-2" style="padding-top:20px">
        <div class="container py-1">
            <h5>Whale Live Users</h5>
            <p align="justify">Whale Live respect the and adheres to the spirit of intellectual property rights.</p>
            <p align="justify">Users of the Whale Live services are held responsible for all posted content to ensure that any contents posted do not infringe on the rights of any third parties and also prevents any violation of any other applicable law or regulations.</p>
            <p align="justify">All text, data, images, graphics, audio and/or video information and other materials within the Services provided by Whale Live are property of Whale Live are protected by copyright, trademark and/or other property rights laws. Nothing in this Agreement shall be construed as conferring any license of any intellectual property rights or such materials by Whale Live to its users.</p>
            <p align="justify">By using and/or uploading any Live stream content or other content through Whale Live to publicly accessible areas of Whale Live website, you grant to Whale Live and its sub-licensees the permission, free, permanent, irrevocable, non-exclusive and fully sub-licensable rights and license, without any territorial or time limitations and without requiring any approvals and/or compensations, to use, copy, modify, adapt, publish, translate, edit, dispose, create derivate works of, distribute, perform and publicly display such content (in whole or in part), and/or incorporate such content into existing or future forms of work, media or technology PROVIDED THAT such content are original works of the user and does not infringe the rights of any third party.</p>
        </div>
        <div class="container py-2">
            <h5>Notice of Infringement Pursuant to this Policy:</h5>
            <p align="justify">This policy sets out on how Whale Live respond to allegations of copyright infringement committed by its users. As such, please note that this policy is only relevant and applicable for allegations of copyright infringements only. </p>
            <p align="justify">Any other alleged infringement of intellectual property or proprietary rights (such as trademarks infringement or violation of Whale Live User Agreement such as inappropriate content, please refer to Whale Live User Agreement (including any other relevant and applicable policies) or contact us at legal@whalemediamy.com</p>
            <p align="justify">If we receive notices of suspected copyright infringement ("Notice"), we will take reasonable steps to notify the User being alleged of such copyright infringement.</p>
            <p align="justify">Such infringement Notice should be submitted by email by the complainant.</p>
            <p align="justify">Please note that we may retain or provide a copy of any Notice to anyone as required by law or as we reasonably require to protect our interests.</p>
            <p align="justify">Please also note that we do not assess or investigate whether copyright infringement has occurred. </p>
            <p align="justify">Any dispute should be resolved by the parties concerned through the process of law.</p>
            <p align="justify">After submitting a Notice, Whale Live may remove the relevant material from the relevant Whale Live services.</p>
            <p align="justify">Please consider carefully all elements of the alleged infringement prior to submitting any Notice, especially if you are unsure whether you are the rightful owner of the copyrighted work. You are recommended to contact your lawyer in such instances as you may be held legally responsible should you make a false or malicious Notice.</p>
            <h5>Submission of Notice and Its Contents</h5>
            <p align="justify">To submit a Notice, you must be the owner of the material copyright (or the authorized agent of the copyright owner) to submit a Notice which relate to such alleged infringement.</p>
            <p align="justify">The Notice must be in writing and must contain sufficient detail to provide reasonable and satisfactory evidence of the copyrighted work that you believe has been infringed and verify that you, or your principal, if you are an authorized agent that you are the owner of such copyrighted work.</p>
            <p align="justify">Please also provide to us the materials to support your claim of copyright infringement and provide us any other information so that it would sufficiently enable us to identify and locate the alleged infringement.</p>
            <p align="justify">Kindly also include the following declaration at the end of your Notice: </p>
            <p align="justify">"I/We sincerely believe that the use of copyrighted content contained on Whale Live as described above, is not authorized by the copyright owner, his agent, or is protected by law. I/We pledge that in subject to perjury, the information in this notice is accurate, and that I/We am the copyright owner or authorized to act on behalf of the exclusive right allegedly infringed."</p>
            <p align="justify">Your Notice must also include your contact address, contact number and email address.</p>
            <p align="justify">Upon completion of the above requirements, please email the complete Notice to: legal@whalemediamy.com</p>
        </div>
    </section>
    </s:else>
</main>

<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd. All Rights Reserved.
    </div>
</footer>

</body>
</html>
