<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!doctype html>
<html>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <style>
        .container {
            padding-right: 50px;
            padding-left: 50px;
        }

        ol {
            padding-left: 20px;
            counter-reset: item;
        }
        ol li {
            padding-top: 10px;
        }

        p,
        ul {
            padding-left: 20px;
            text-align: justify;
        }

        li {
            margin-bottom: 10px;
            text-align: justify;
            display: block;
        }
        li:before {
            content: counters(item, ".") " ";
            counter-increment: item;
        }
        li.main-li:before {
            content: counters(item, ".") ". ";
        }

        .media-4:after {
            transform: skewY(-1.70deg);
        }
    </style>
</head>

<body>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<main>
    <s:if test="language=='zh'">
        <section class="media-4">
            <div class="text-center" style="height:130px">
                <h3 style="padding-top:40px;">WHALE LIVE 社区准则</h3>
                <h3 style="margin-top:-15px;">（“社区准则”）</h3>
            </div>
        </section>

        <section class="live-2" style="padding-top:20px">
            <div class="container py-1">
                <ol>
                    <li class="main-li">
                        <b>概述</b>
                        <ol>
                            <li>作为 Whale Live 业务管理的一部分，Whale Live 将为所有用户（“广播者”）维持并执行在此规定的标准社区准则（“社区准则”），以确保健康和谐地 Whale Live 服务运作。</li>
                            <li>本社区准则适用于所有 Whale Live服务或业务。</li>
                        </ol>
                    </li>
                    <li class="main-li">
                        <b>直播室内视频违规</b>
                        <ol>
                            <li>广播者必须严格遵守此社区准则，否则 Whale Live 将会根据按照案件的违反行为的严重程度对广播者进行处罚。Whale Live 的任何决定将会是最终决定。</li>
                            <li>广播者有责任为 Whale Live 的观众确保一个有序和健康的现场直播的环境。若有任何违规行为都将会导致 Whale Live 对广播者或其相关人员进行处罚。</li>
                            <li>严禁广播者作出任何可能被视为违反政府的行为。例如：
                                <ol>
                                    <li>违反宪法确立的基本原则；</li>
                                    <li>危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一；</li>
                                    <li>损害国家声誉和利益的；</li>
                                    <li>挑拨民怨，歧视民族；</li>
                                    <li>破坏国家宗教政策，宣扬邪教迷信；</li>
                                    <li>散布谣言，对社会不公正，破坏社会稳定；</li>
                                    <li>传播暴力、恐怖或者教唆犯罪的；</li>
                                    <li>煽动非法集会、结社、游行、示威、集会，扰乱社会秩序的；和</li>
                                    <li>其他可能对国家产生不利影响的政治话题。</li>
                                </ol>
                            </li>
                            <li>严禁播放违反法律法规、平台政策等内容。例如：
                                <ol>
                                    <li>展示药物样本，进行和传播药物使用或注射，说明药物生产等；</li>
                                    <li>组织、推广、诱导用户加入可疑组织的营销；</li>
                                    <li>赌博/赌场、毒品、卖淫等的现场直播；</li>
                                    <li>性行为、过度裸露肌肤等；</li>
                                    <li>使用绿色屏风及/或马赛克；和</li>
                                    <li>残疾人和军人未经事先同意进行现场直播。</li>
                                </ol>
                            </li>
                            <li>严禁广播者进行威胁生命和健康或使用枪支或刀具的表演。例如：
                                <ol>
                                    <li>使用刀具、仿真工具、仿真枪，以及/或进行高风险类似的现场活动；</li>
                                    <li>执行危及或造成人身安全伤害的内容。例如：殴打、威胁等；</li>
                                    <li>执行危及自身安全的内容。例如，自杀等等；</li>
                                    <li>播放危及动物生命和健康的内容。例如虐待小动物等；和</li>
                                    <li>本准则未提及的危及生命的其他表现。</li>
                                </ol>
                            </li>
                            <li>严禁广播者传播任何形式的性或侮辱性内容。例如：
                                <ol>
                                    <li>传播侵犯他人隐私的信息，如服装故障、未经允许偷拍他人照片、泄露照片等；</li>
                                    <li>散布有害信息，如拉客、通奸、性虐待、性事务等；</li>
                                    <li>传播色情游戏和动画；</li>
                                    <li>散布淫亵及色情网站；和</li>
                                    <li>传播含有色情、性挑逗、性暗示等内容的图像、文字、歌曲、文字、视频、音频等。</li>
                                </ol>
                            </li>
                            <li>禁止发布类广告信息：
                                <ol>
                                    <li>禁止的广告包括但不限于：赌博、关注订阅者、钻石、成人用品、枪、军刀、弓弩、成人录影带、性用品、保健品、香烟、毒品、性传播疾病治疗等广告。</li>
                                </ol>
                            </li>
                            <li>广播者严禁着装不当，如暴露胸部、裸体、挑衅或暗示性等。例如：
                                <ol>
                                    <li>女人赤裸着后背，男人赤裸着上身；</li>
                                    <li>穿有暗示性、挑逗性、裸露性、透明性、半截性、露背性文字、图片等内容的制服或服装的；</li>
                                    <li>上衣：撩人或松开纽扣或拉链的衣服；下半身：穿在臀部以上的短裙或短裤</li>
                                    <li>其他不雅或不适当的化妆。</li>
                                </ol>
                            </li>
                            <li>严禁您侵犯他人的合法权益。例如：
                                <ol>
                                    <li>侵犯他人隐私，危害公共利益。在未获有关人士同意的情况下，披露个人资料，例如身分证、名片、姓名、地址、联络号码及其他私人资料，以及任何形式的会面或互动；</li>
                                    <li>使用任何形式的文本、语音，图片，等促进语音和视频等其他类似的平台；</li>
                                    <li>播放未经授权的录像作品；和</li>
                                    <li>播放未经授权及/或非官方活动及/或冒充媒体、电视台、记者等。</li>
                                </ol>
                            </li>
                            <li>严禁在任何直播过程中做出不当行为。例如：
                                <ol>
                                    <li>注意镜头在胸部、臀部、腿部和脚等具有挑逗性的部位，比如：对着镜头展示臀部等等；</li>
                                    <li>性暗示或挑逗行为，包括但不限于：抖胸、猜测内衣颜色、脱衣/穿袜子、撕破或剪断衣物等；</li>
                                    <li>使用性用品或者性暗示道具进行表演的；</li>
                                    <li>表演其他涉及政治、性、非法、侵犯或威胁他人生命和安全的表演，以及其他违反 Whale Live 团体的秩序规则（可能会不时被告知）的表演；</li>
                                    <li>以任何形式表演任何暗示或挑衅的游戏。如：爱抚胸部，爱抚臀部，用臀部画圈，用臀部模拟交媾动作等等；</li>
                                    <li>用紧身的上衣和衣服来暗示舞蹈；</li>
                                    <li>吸烟、酗酒、吸毒、驾驶和其他；</li>
                                    <li>自虐或伤害他人的。这包括但不限于酗酒、虐待自己、殴打他人、吃东西或展示各种淫秽物品，活着或不活着，等等；</li>
                                    <li>组织骚扰和挑衅的现场活动；</li>
                                    <li>讨论政治敏感话题；</li>
                                    <li>促进色情内容；</li>
                                    <li>诽谤、言语攻击、地域攻击、骚扰、戏弄他人；</li>
                                    <li>骚扰或煽动他人骚扰、模仿性刺激的声音等行为，例如性挑逗等等；</li>
                                    <li>描述或宣传任何赌博、赌场、麻将、扑克、纸牌游戏等；</li>
                                    <li>散布可能危害他人的虚假报道或者新闻；</li>
                                    <li>在暗示性或挑逗性的场所，如沐浴中心、夜总会等进行广播；</li>
                                    <li>展示除播音人以外的其他人进行违反本准则的行为，如饮酒、吸烟、裸体、衣服过度暴露等。</li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li class="main-li">
                        <b>处罚</b>
                        <p>如有违反，将视情节轻重予以处罚。</p>
                        <p>处罚可能包括但不限于：暂时或永久禁止使用任何 Whale Live 服务、删除内容、警告等。</p>
                    </li>
                    <li class="main-li">
                        <b>其他</b>
                        <ol>
                            <li>本社区准则由 Whale Live 观察和管理，Whale Live 有权随时修改其内容，并将本社区准则公之于众。在任何条款有歧义或冲突的情况下，Whale Live 保留对本共同体准则的最终解释权。</li>
                            <li>本准则自发布之日起生效。</li>
                            <li>任何一方对任何违反本准则的行为的默许都不能作为以后或以前违约的借口。</li>
                        </ol>
                    </li>
                </ol>
            </div>
        </section>
    </s:if>
    <s:else>
        <section class="media-4">
            <div class="text-center" style="height:130px">
                <h3 style="padding-top:40px;">WHALE LIVE COMMUNITY GUIDELINE</h3>
                <h3 style="margin-top:-15px;">(“Community Guideline”)</h3>
            </div>
        </section>

        <section class="live-2" style="padding-top:20px">
            <div class="container py-1">
                <ol>
                    <li class="main-li">
                        <b>Overview</b>
                        <ol>
                            <li>As part of Whale Live’s business management to ensure the healthy and harmony use of Whale Live services, Whale Live maintains and enforces a standard guideline to all users (“<b>Broadcaster</b>”) as stipulated herein (“<b>Community Guideline</b>”).</li>
                            <li>This Community Guideline shall be applicable across to all services or businesses of Whale Live.</li>
                        </ol>
                    </li>
                    <li class="main-li">
                        <b>Violation Within Broadcast Live Room Video</b>
                        <ol>
                            <li>Broadcasters must strictly follow this Community Guideline, failing which, a penalty shall be imposed on the broadcaster in accordance to the seriousness of violation as accessed by Whale Live, which shall be final and conclusive.</li>
                            <li>Broadcasters has the responsibility to ensure that the environment of the live broadcast is orderly and healthy for Whale Live’s audiences. Any violation may result in a penalty being imposed by Whale Live on the broadcaster or its related persons.</li>
                            <li>Broadcasters are strictly prohibited to conduct any act that may be construed as against the government. For instance:
                                <ol>
                                    <li>Violation of the basic principles of the constitution;</li>
                                    <li>Endanger national security, divulge state secrets, subvert the authority of the nation, prejudicing national unity</li>
                                    <li>Damaging the nation's reputation and interests;</li>
                                    <li>Stirring up discord among people, ethnic discrimination, undermining national unity;</li>
                                    <li>Undermining the nation’s religious policy, advocates cults and superstitions;</li>
                                    <li>Spreading rumors, committing injustice to society, destabilizing the society;</li>
                                    <li>Spreading violence, terror or abetting crime;</li>
                                    <li>Incitement to illegal assembly, association, procession, demonstration, and assembly to disrupt public order;</li>
                                    <li>Other political topics that may adversely affects the nation;</li>
                                </ol>
                            </li>
                            <li>Broadcasters are strictly prohibited to broadcast content that violates legal regulations, platform policies and etc. For instance:
                                <ol>
                                    <li>Display drug samples, perform and spread drug use or injection, explain drug production and more;</li>
                                    <li>Organize, promote, and induce users to join the marketing of suspicious organizations;</li>
                                    <li>Live broadcast of gambling/casino, drugs, prostitution and more;</li>
                                    <li>Behavior such as sexuality, excessive show of skin and etc;</li>
                                    <li>The use of Green screens and/or mosaics;</li>
                                    <li>Live broadcast by disabled and military personnel without prior consent;</li>
                                </ol>
                            </li>
                            <li>Broadcasters are strictly prohibited to conduct threats to life and health or the use of firearms or knives performance. For example:
                                <ol>
                                    <li>Use of knives, simulation tools, guns, simulation guns, and/or  perform similar live content with high risk;</li>
                                    <li>Perform content that endangers or cause harm to a person's safety. For example: beating, threatening and etc;</li>
                                    <li>Perform content that endangers your own safety. For example, self-inflicted suicide and etc;</li>
                                    <li>Perform content that endangers the life and health of animals. Such as abuse of small animals and etc;</li>
                                    <li>Other life-threatening performances not mentioned herein;</li>
                                </ol>
                            </li>
                            <li>Broadcasters are strictly prohibited to disseminate any form of sexual, or insulting content. For example:
                                <ol>
                                    <li>Disseminate information that infringes on the privacy of others, such as wardrobe malfunction, capturing someone's picture without permission, leaking such picture and etc;</li>
                                    <li>Disseminate harmful information such as soliciting, adultery, sexual abuse, sexual affairs and etc;</li>
                                    <li>Spread erotic games and anime;</li>
                                    <li>Spread obscene and pornographic websites;</li>
                                    <li>Spread images, words, songs, text, videos, audio and others that involve pornography, sexual teasing, sexual cues and etc;</li>
                                </ol>
                            </li>
                            <li>Advertising-like information is prohibited:
                                <ol>
                                    <li>The prohibited advertisement shall include but not limited to the following: gambling, selling fans, diamonds, adult products, guns, military knives, bow crossbows, adult video tape, sex products, health care products, cigarettes, drugs, and sexually transmitted diseases treatment advertisements and etc.</li>
                                </ol>
                            </li>
                            <li>Broadcast are also strictly prohibited to dress inappropriately such as exposed breast/chest, nudity, provocative and suggestive cues, and etc. For example:
                                <ol>
                                    <li>Women exposing their bare back and men exposing their naked upper body;</li>
                                    <li>Wearing uniforms or clothing that are sexually suggestive, flirtatious, provocative, exposed, bare, transparent, half-cut, low-back, contains sexually suggestive text or pictures and etc;</li>
                                    <li>Upper wear: Provocative or loosened button or zip clothing. Lower part: Skirt or shorts that are worn above the hip.</li>
                                    <li>Other indecent or inappropriate make-up.</li>
                                </ol>
                            </li>
                            <li>Broadcasters are strictly prohibited to infringe upon the legitimate rights and interests of other. For example:
                                <ol>
                                    <li>Encroaching on the privacy of others and endangering the public interest. The disclosure of personal data such as identification cards, business cards, names, address, contact number and other private information, and any form of interview or interaction without the consent of the person concerned;</li>
                                    <li>Use other similar broadcast live platforms or guilds in any form, such as text, audio, pictures, videos, etc;</li>
                                    <li>Play unauthorized video animation or games;</li>
                                    <li>Live or unauthorized, unofficial activities, impersonate media, television station, journalists, etc;</li>
                                </ol>
                            </li>
                            <li>Broadcasters are strictly prohibited to conduct inappropriate acts during any live broadcast. For example:
                                <ol>
                                    <li>Focus on provocative areas such as the chest, hips, legs, and feet in front of the lens, such as: Displaying your buttocks toward the lens and etc.</li>
                                    <li>Sexual hinting or provocative action, including but not limited to: shaking your chest, guessing the color of your underwear, live stripping/wearing socks, tearing or cutting of clothing and etc;</li>
                                    <li>Use of sexual products or suggestive props to conduct any kind of  performance;</li>
                                    <li>Perform other types of performances that involve political, sexual, illegal, infringing, or threatening the life and safety of others and other types of performance that violate the rules of order of Whale Live community that may be informed from time to time;</li>
                                    <li>Perform any suggestive or provocative game in any form. Such as: caressing the chest, caressing the hip, drawing circles with the hip, simulating the coitus movement with the hip and etc.</li>
                                    <li>Dancing with suggestive cues is with tight tops and dresses;</li>
                                    <li>Smoking, drinking, consumption of drugs, driving and others;</li>
                                    <li>Self-abuse or causing harm to others. This includes but not limited to alcohol abuse, ill-treatment of oneself, beating another, eating or displaying various obscene objects, living or not, and etc;</li>
                                    <li>Organize live events that are harassing and provocative;</li>
                                    <li>Discuss politically sensitive topics;</li>
                                    <li>Promote pornographic content;</li>
                                    <li>Defamation, verbal attacks, geographical attacks, harassment, and tease of another person;</li>
                                    <li>Acts that harass or incite others to harass, mimic sexual-inspired sounds and etc, such as sexual advances and etc;</li>
                                    <li>Describe or promote any gambling, casino, mahjong, poker, card games and etc;</li>
                                    <li>Dissemination of false reports or news that may cause harm to others;</li>
                                    <li>Conduct broadcast in suggestive or provocative venue, such as bathing centre, night club and etc;</li>
                                    <li>Display of persons, other than the broadcaster, conducting actions that are in violation of this Community Guideline, such as alcohol consumption, smoking, naked body, overexposed dress, and etc.</li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li class="main-li">
                        <b>Penalty</b>
                        <p>If a broadcaster commits a violation, he will be punished according to the seriousness of the case</p>
                        <p>Penalties may include but not limited to: Temporary or permanent ban of the user of any Whale Live services, content deletions, warnings and etc.</p>
                    </li>
                    <li class="main-li">
                        <b>Others</b>
                        <ol>
                            <li>This Community Guideline is observed and managed by Whale Live, who has the right to modify from time to time its content as Whale Live deems fit and to publicize this Community Guideline. In any event of ambiguity and conflict of terms, Whale Live reserves the right of final interpretation to this Community Guideline.</li>
                            <li>This Community Guideline shall be effective on the date of its publication.</li>
                            <li>No acquiescence in any breach of this Community Guideline by either party shall operate to excuse any subsequent or prior breach.</li>
                        </ol>
                    </li>
                </ol>
            </div>
        </section>
    </s:else>
</main>

<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd. All Rights Reserved.
    </div>
</footer>

</body>
</html>
