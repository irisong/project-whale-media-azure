<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!doctype html>
<html>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <style>
        .container {
            padding-right: 50px;
            padding-left: 50px;
        }

        ol {
            padding-left: 20px;
            counter-reset: item;
        }
        ol li {
            padding-top: 10px;
        }

        p,
        ul {
            padding-left: 20px;
            text-align: justify;
        }

        li {
            margin-bottom: 10px;
            text-align: justify;
            display: block;
        }
        li:before {
            content: counters(item, ".") " ";
            counter-increment: item;
        }
        li.main-li:before {
            content: counters(item, ".") ". ";
        }

        .media-4:after {
            transform: skewY(-1.70deg);
        }
    </style>
</head>

<body>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<main>
<%--    <s:if test="language=='zh'">--%>
        <section class="media-4">
            <div class="text-center" style="height:130px">
                <h3 style="padding-top:40px;">WHALE LIVE 内容管理准则</h3>
                <h3 style="margin-top:-15px;">（“准则”）</h3>
            </div>
        </section>

        <section class="live-2" style="padding-top:20px">
            <div class="container py-1">
                <ol>
                    <li class="main-li">
                        <b>概述</b>
                        <p>准则旨在加强对 Whale Live 服务管理，并为用户提供健康和谐的用户体验和环境。您必须遵守以下规则，否则可能会受到处罚。违反行为的严重程度由 Whale Live 官员 根据违反意图、违反时间、违反主体等客观因素综合评估。</p>
                        <p>您对任何内容负责，并有责任维护一个健康的社区、环境和平台。</p>
                    </li>
                    <li class="main-li">
                        <b>禁止内容、表演和行为</b>
                        <ol>
                            <li>严禁播出任何反政府、侮辱、败坏国家名誉的节目。例如：
                                <ol>
                                    <li>违反宪法确立的基本原则；</li>
                                    <li>危害国家安全，泄露国家秘密，颠覆国家政权，破坏国家统一；</li>
                                    <li>损害国家声誉和利益的；</li>
                                    <li>挑拨民怨，歧视民族；</li>
                                    <li>破坏国家宗教政策，宣扬邪教迷信；</li>
                                    <li>散布谣言，对社会不公正，破坏社会稳定；</li>
                                    <li>传播暴力、恐怖或者教唆犯罪的；</li>
                                    <li>煽动非法集会、结社、游行、示威、集会，扰乱社会秩序的；和</li>
                                    <li>其他可能对国家产生不利影响的政治话题。</li>
                                </ol>
                            </li>
                            <li>严禁播放违反法律法规、平台政策等内容。例如：
                                <ol>
                                    <li>展示药物样本，进行和传播药物使用或注射，说明药物生产等；</li>
                                    <li>组织、推广、诱导用户加入可疑组织的营销；</li>
                                    <li>赌博/赌场、毒品、卖淫等的现场直播；和</li>
                                    <li>性行为、过度裸露肌肤等。</li>
                                </ol>
                            </li>
                            <li>严禁进行威胁生命和健康或使用火器或刀具的表演。例如：
                                <ol>
                                    <li>使用刀具、仿真工具、仿真枪，以及/或进行高风险类似的现场活动；</li>
                                    <li>执行危及或造成人身安全伤害的内容。例如:殴打、威胁等；</li>
                                    <li>执行危及自身安全的内容。例如，自杀等等；</li>
                                    <li>播放危及动物生命和健康的内容。例如虐待小动物等；和</li>
                                    <li>本准则未提及的危及生命的其他表现。</li>
                                </ol>
                            </li>
                            <li>严禁您侵犯他人的合法权益。例如：
                                <ol>
                                    <li>侵犯他人隐私，危害公共利益。在未获有关人士同意的情况下，披露个人资料，例如身分证、名片、姓名、地址、联络号码及其他私人资料，以及任何形式的会面或互动；</li>
                                    <li>使用任何形式的文本、语音、图片、等促进语音和视频等其他类似的平台；</li>
                                    <li>播放未经授权的录像作品；</li>
                                    <li>播放未经授权及/或非官方活动及/或冒充媒体、电视台、记者等；和</li>
                                    <li>发布广告信息：包括但不限于：赌博、订阅者/关注者、钻石、成人用品、枪支、军刀、弓弩、成人录影带、性产品、保健品、香烟、毒品、性传播疾病治疗广告等。</li>
                                </ol>
                            </li>
                            <li>严禁在任何直播过程中做出不当行为。例如：
                                <ol>
                                    <li>注意镜头在胸部、臀部、腿部和脚等具有挑逗性的部位，比如：对着镜头展示臀部等等；</li>
                                    <li>性暗示或挑逗行为，包括但不限于：抖胸、猜测内衣颜色、脱衣/穿袜子、撕破或剪断衣物等；</li>
                                    <li>使用性用品或者性暗示道具进行表演的；</li>
                                    <li>表演其他涉及政治、性、非法、侵犯或威胁他人生命和安全的表演，以及其他违反 Whale Live 团体的秩序规则（可能会不时被告知）的表演；</li>
                                    <li>以任何形式表演任何暗示或挑衅的游戏。如：爱抚胸部，爱抚臀部，用臀部画圈，用臀部模拟交媾动作等等；</li>
                                    <li>用紧身的上衣和衣服来暗示舞蹈；</li>
                                    <li>吸烟、酗酒、吸毒、驾驶和其他；</li>
                                    <li>自虐或伤害他人的。这包括但不限于酗酒、虐待自己、殴打他人、吃东西或展示各种淫秽物品，活着或不活着，等等；</li>
                                    <li>组织骚扰和挑衅的现场活动；</li>
                                    <li>讨论政治敏感话题；</li>
                                    <li>促进色情内容；</li>
                                    <li>诽谤、言语攻击、地域攻击、骚扰、戏弄他人；</li>
                                    <li>骚扰或煽动他人骚扰、模仿性刺激的声音等行为，例如性挑逗等等；</li>
                                    <li>描述或宣传任何赌博、赌场、麻将、扑克、纸牌游戏等；</li>
                                    <li>散布可能危害他人的虚假报道或者新闻；</li>
                                    <li>在暗示性或挑逗性的场所，如沐浴中心、夜总会等进行广播；</li>
                                    <li>展示除播音人以外的其他人进行违反本准则的行为，如饮酒、吸烟、裸体、衣服过度暴露等。</li>
                                </ol>
                            </li>
                            <li>额外的禁忌：
                                <ol>
                                    <li>女人赤裸着后背，男人赤裸着上身；</li>
                                    <li>穿有暗示性、挑逗性、裸露性、透明性、半截性、露背性文字、图片等内容的制服或服装的；</li>
                                    <li>上衣：撩人或松开纽扣或拉链的衣服；</li>
                                    <li>下半身：穿在臀部以上的短裙或短裤；</li>
                                    <li>其他不雅或不适当的化妆；</li>
                                    <li>在镜头前抽烟、喝酒、吃东西；</li>
                                    <li>恶意冒犯他人，包括言语辱骂、诽谤、地域歧视等；</li>
                                    <li>在拍摄过程中，发布或推广非 Whale Live 授权商业广告，如微商、网店等；</li>
                                    <li>拍摄过程中出现非 Whale Live 关联标志或品牌、外部平台广告信息、引导用户或受众到外部平台等；</li>
                                    <li>任何其他违反 Whale Live 服务精神的表演或内容，如任何其他政治、色情、非法或威胁生命和安全的行为。</li>
                                </ol>
                            </li>
                        </ol>
                    </li>
                    <li class="main-li">
                        <b>默许</b>
                        <p>任何一方对任何违反本准则的行为的默许都不能作为以后或以前违约的借口</p>
                    </li>
                </ol>
            </div>
        </section>
<%--    </s:if>--%>
<%--    <s:else>--%>
<%--    </s:else>--%>
</main>

<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd. All Rights Reserved.
    </div>
</footer>

</body>
</html>
