<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>

<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>
<s:if test="language=='zh'">
    <body onload="initLang('zh','<s:text name="CN"></s:text>')"></body>
</s:if>
<s:else>
    <body onload="initLang('en','<s:text name="EN"></s:text>')"></body>
</s:else>
<main>

    <s:if test="language=='zh'">
        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h2 style="padding-top:40px;">联系我们</h2>
            </div>
        </section>
        <section class="live-2" style="padding-top:20px">
                <%--            <div class="container py-1">--%>
                <%--                <p align="justify"><b>设置</b></p>--%>
                <%--            </div>--%>
            <div class="container py-2">
                <p align="justify"><b>联系我们（关于我们）</b></p>
                <ol>
                    <p align="justify">WHALE LIVE鲸娱直播是马来西亚WHALE MEDIA SDN
                        BHD鲸娱媒体在2020年所推出的综合性大数据库的直播平台。鲸娱直播将会采用区块链技术让客户及主播的数据进行上链。</p>
                    <p align="justify">WHALE LIVE鲸娱直播的后台技术将会由马来西亚两家科技技术公司AIO,OMC
                        GROUP共同携手打造，鲸娱媒体将会在2020年全力推广首发产品（鲸娱直播），将会以三个阶段性进行发展，首阶段会以娱乐直播进行，目的为了打造大量的主播及粉丝群体入驻平台，第二阶段将会以直播带货的方式发展，目的是带货主播自身拥有大量粉丝群体可让商家的产品有更高曝光率，第三阶段将会以MCN（multi
                        channel network）网路艺人孵化中心，目的可让更多平台主播可以升级成为网路艺人，往东南亚及更多地方发展。</p>

                </ol>
            </div>
            <div class="container py-2">
                <p align="justify"><b>联系方式</b></p>
                <p align="justify">公司 ：</p>
                <p align="justify">WHALE MEDIA SDN. BHD.</p>
            </div>

            <div class="container py-2">
                <p align="justify"><b>地址 ：</b></p>
                <p align="justify">159(LEVEL 8), JALAN TEMPLE, PJS 8,</p>
                <p align="justify">46050 PETALING JAYA. SELANGOR,</p>
                <p align="justify">MALAYSIA.</p>
            </div>

            <div class="container py-2">
                <p align="justify"><b>公会，签约主播咨询及客服服务 ：</b></p>
                <p align="justify"> 邮件 ： Corporate@whalemediamy.com</p>
                <p align="justify"> 微信号 ：whalelive01</p>
                <p align="justify"> 微信号 ：whalelive02</p>
            </div>
        </section>
    </s:if>
    <s:else>

        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h2 style="padding-top:40px;">Contact Us</h2>
            </div>
        </section>
        <section class="live-2" style="padding-top:20px">
                <%--            <div class="container py-1">--%>
                <%--                <p align="justify"><b>Setting</b></p>--%>
                <%--            </div>--%>
            <div class="container py-2">
                <p align="justify"><b>Contact Us（About Us）</b></p>
                <ol>
                    <p align="justify">WHALE LIVE is the WHALE MEDIA SDN in Malaysia BHD Whale Entertainment Media will
                        launch a comprehensive large database live broadcast platform in 2020. Whale Entertainment will
                        use blockchain technology to upload data from customers and anchors.</p>
                    <p align="justify">The background technology of WHALE LIVE will be managed by two Malaysian
                        technology companies AIO and OMC.
                        GROUP worked together to create, Whale Entertainment Media will make every effort to promote the
                        first product (Whale Entertainment Live) in 2020. It will be developed in three phases. The
                        first phase will be live entertainment for the purpose of creating a large number of anchors and
                        fan groups. Settled on the platform, the second stage will be developed in the way of live
                        broadcasting. The purpose is to bring the goods. The anchor has a large number of fans and can
                        make the business’s products more exposed. The third stage will use MCN (multi
                        Channel network) Internet artist incubation center, the purpose is to allow more platform
                        anchors to upgrade to online artist, and develop in Southeast Asia and more places.</p>

                </ol>
            </div>
            <div class="container py-2">
                <p align="justify"><b>Contact:</b></p>
                <p align="justify">Company ：</p>
                <p align="justify">WHALE MEDIA SDN. BHD.</p>
            </div>

            <div class="container py-2">
                <p align="justify"><b>Address:</b></p>
                <p align="justify">159(LEVEL 8), JALAN TEMPLE, PJS 8,</p>
                <p align="justify">46050 PETALING JAYA. SELANGOR,</p>
                <p align="justify">MALAYSIA.</p>
            </div>

            <div class="container py-2">
                <p align="justify"><b>Guild, contract anchor consultation and customer service:</b></p>
                <p align="justify"> Email ： Corporate@whalemediamy.com</p>
                <p align="justify"> Wechat ：whalelive01</p>
                <p align="justify"> Wechat ：whalelive02</p>
            </div>
        </section>
    </s:else>
</main>
<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd 版权所有
    </div>
</footer>

<script type="text/javascript">

    function initLang(langCode, currentLang) {

        if ((currentLang != 'English' && langCode == 'en') || (currentLang == 'English' && langCode == 'zh')) {
            $.ajax({
                type: 'POST',
                url: "<s:url action="initLangWhaleLivePage"/>",
                dataType: 'json',
                cache: false,
                data: {
                    language: langCode
                },
                success: function (data, status, xhr) {
                    window.location.reload();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
    }

    function reload() {
        window.location.reload();
    }


</script>

<style>

    .container {
        padding-right: 50px;
        padding-left: 50px;
    }

    .media-4:after {
        transform: skewY(-1.70deg);
    }

    ol {
        list-style-type: none;
        counter-reset: item;
        margin: 0;
        padding: 0;
    }

    ol > li {
        display: table;
        counter-increment: item;
        margin-bottom: 0.6em;
    }

    ol > li:before {
        content: counters(item, ".") ". ";
        display: table-cell;
        padding-right: 0.6em;
    }

    li ol > li {
        margin: 0;
    }

    li ol > li:before {
        content: counters(item, ".") " ";
    }

</style>

<!-- #modal-dialog -->

