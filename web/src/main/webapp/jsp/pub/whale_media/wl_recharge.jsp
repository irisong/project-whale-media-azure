<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>

<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
               <li class="nav-item">
                    <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_casting.php">网络艺人招募</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_recharge.php">充值</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_download.php">应用程序</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_login.php">登入</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>
                <%--                <div style="position: center">--%>
                <%--                    <li class="nav-item" style="display: inline-block">--%>
                <%--                        <a class="nav-link" onclick="initLang('en'); return false;"--%>
                <%--                           style="display: inline-block"><s:text name="title.language.EN"></s:text></a>--%>
                <%--                    </li>--%>

                <%--                    <li class="nav-item" style="display: inline-block">--%>
                <%--                        <a class="nav-link" onclick="initLang('zh'); return false;"--%>
                <%--                           style="display: inline-block"><s:text name="title.language.CN"></s:text></a>--%>
                <%--                    </li>--%>
                <%--                </div>--%>

            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1">
        <div class="container-fluid body py-3">
            <span class="circle-bg"></span>
            <span class="circle-sm"></span>
            <s:form id="topUpForm" name="topUpForm">

                <s:if test="!showBalanceAndTopUpOption">
                    <div class="col-md-6 mx-auto">
                        <div class="form-group">
                            <input name="phoneNo" id="phoneNo" type="text" class="form-control"
                                   placeholder="输入需要充值的鲸娱账户" required="true">
                        </div>


                        <div class="login_error_message" style="color: red; text-align: center">
                            <h6><s:property value="login_errorMessage"/></h6>
                        </div>

                        <s:submit id="btnLogin" type="button" class="btn btn-secondary" style="margin-left: 42%;">
                            <i class="fa fa-floppy-o"></i>
                            <s:text name="btnLogin"/>
                        </s:submit>
                    </div>
                </s:if>

                <s:if test="showBalanceAndTopUpOption">
                    <div class="col-lg-6 col-md-8 mx-auto mt-3">
                        <h5>账号</h5>
                        <div>
                            <s:textfield name='member.displayId' id='member.displayId' label="%{getText('memberId')}"
                                         readonly="true"/>
                            <s:textfield name='member.displayProfileName' id='member.displayProfileName' label="%{getText('fullName')}"
                                         readonly="true"/>
                        </div>
                    </div>

<%--                    <div class="col-lg-6 col-md-8 mx-auto mt-3">--%>
<%--                        <h5>余额</h5>--%>
<%--                        <div style="margin-left: 42%">--%>
<%--                            <img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img" width="50"--%>
<%--                                 height="50">--%>
<%--                            <span class="title" style="font-weight: bold; font-size: large"> = <s:property--%>
<%--                                    value="balance"/></span>--%>
<%--                        </div>--%>
<%--                    </div>--%>

                    <div class="col-lg-6 col-md-8 mx-auto mt-3">
                        <h5>充值方式</h5>

                     <%--   <s:iterator status="iterStatus" value="topUpOptionInfoDtos" var="topUpOption">
                            <div class="card">
                                <a class="card-link" data-toggle="collapse" href="#${topUpOption.paymentOption}_id">
                                    <div class="card-header">
                                        <div>
                                            <img src="${topUpOption.iconUrl}" class="img">
                                            <span class="title"><s:property
                                                    value="%{#topUpOption.paymentOptionName}"/> </span>
                                        </div>
                                        <i class="fas fa-chevron-down"></i>
                                    </div>
                                </a>

                                <div id="${topUpOption.paymentOption}_id" class="collapse">
                                    <div class="card-body">
                                        <table class="table table-hover mb-0">
                                            <tbody>
                                            <s:iterator status="iStatus" value="#topUpOption.topupWalletTypeInfoDtoList"
                                                        var="walletTypeInfo">
                                                <tr>
                                                    <td><img
                                                            src="<c:url value="/asset/image/coin/wc.png"/>"
                                                            class="img"></td>
                                                    <td class="text-left" onclick="topUpWhaleCoin(<s:property
                                                            value="%{#walletTypeInfo.whaleCoinAmt}"/>,
                                                            '${walletTypeInfo.topUpOption}', <s:property
                                                            value="%{#walletTypeInfo.fromWalletAmount}"/>)">
                                                        <span style="font-weight: bold; font-size: large">
                                                            <s:property value="%{#walletTypeInfo.option}"/>
                                                        </span>
                                                        <br/>
                                                        <span style="color: dimgrey; overflow: auto">
                                                            <s:property value="%{#walletTypeInfo.rateDesc}"/>
                                                        </span>
                                                    </td>
                                                </tr>
                                            </s:iterator>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </s:iterator>--%>
                        <!--
                        <div class="card">
                            <a class="card-link" data-toggle="collapse" href="#grab">
                                <div class="card-header">
                                    <div>
                                        <img src="<c:url value="/asset/whale_media/asset/payment/2.png"/>" class="img">
                                        <span class="title">GRABPAY</span>
                                    </div>
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </a>

                            <div id="grab" class="collapse">
                                <div class="card-body">
                                    <table class="table table-hover mb-0">
                                        <tbody>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>100</td>
                                            <td class="text-right">MYR 3.90</td>
                                        </tr>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>300</td>
                                            <td class="text-right">MYR 6.90</td>
                                        </tr>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>1000</td>
                                            <td class="text-right">MYR 15.90</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <a class="card-link" data-toggle="collapse" href="#boost">
                                <div class="card-header">
                                    <div>
                                        <img src="<c:url value="/asset/whale_media/asset/payment/3.png"/>" class="img">
                                        <span class="title">BOOST EWALLET</span>
                                    </div>
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </a>

                            <div id="boost" class="collapse">
                                <div class="card-body">
                                    <table class="table table-hover mb-0">
                                        <tbody>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>100</td>
                                            <td class="text-right">MYR 3.90</td>
                                        </tr>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>300</td>
                                            <td class="text-right">MYR 6.90</td>
                                        </tr>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>1000</td>
                                            <td class="text-right">MYR 15.90</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <a class="card-link" data-toggle="collapse" href="#card">
                                <div class="card-header">
                                    <div>
                                        <img src="<c:url value="/asset/whale_media/asset/payment/4.png"/>" class="img">
                                        <span class="title">CREDIT / DEBIT CARD</span>
                                    </div>
                                    <i class="fas fa-chevron-down"></i>
                                </div>
                            </a>

                            <div id="card" class="collapse">
                                <div class="card-body">
                                    <table class="table table-hover mb-0">
                                        <tbody>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>100</td>
                                            <td class="text-right">MYR 3.90</td>
                                        </tr>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>300</td>
                                            <td class="text-right">MYR 6.90</td>
                                        </tr>
                                        <tr>
                                            <td><img src="<c:url value="/asset/whale_media/asset/payment/5.png"/>" class="img"></td>
                                            <td>1000</td>
                                            <td class="text-right">MYR 15.90</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        -->
                    </div>
                    <div class="main container card-header">
                            <div class="rec-method">
                                <a href="javascript:;" class="charge-item tab-cash on" > <img class="img-razer" src="<c:url value="/asset/whale_media/asset/payment/fpxLogo.png"/>" class="img">Razer</a>
                            </div>
                        <s:iterator status="iterStatus" value="topUpOptionInfoDtos" var="topUpOption">
                            <s:if test="paymentOption=='RAZER'">
                                    <div id="${topUpOption.paymentOption}" class="pk-item" >
                            </s:if>
                            <s:else>
                                <div id="${topUpOption.paymentOption}" class="pk-item" style="display:none" >
                            </s:else>
                                <ul class="package-list games-list">
                                <s:iterator status="iStatus" value="#topUpOption.topupWalletTypeInfoDtoList"
                                            var="walletTypeInfo">
                                            <li class="buttonbox" ondblclick="topUpWhaleCoin(<s:property
                                                    value="%{#walletTypeInfo.whaleCoinAmt}"/>,
                                                    '${walletTypeInfo.topUpOption}', <s:property
                                                    value="%{#walletTypeInfo.fromWalletAmount}"/>)" ><input type="hidden" name="productId" value="%{#walletTypeInfo.whaleCoinAmt}">
                                                <div class="desc">
                                                    <p class="amount-text">
                                                        <img src="<c:url value="/asset/image/coin/wc.png"/>" class="img coin"><span style="font-weight: bold; font-size: large; color:black"><s:property value="%{#walletTypeInfo.whaleCoinAmt}"/></span>
                                                    </p>
                                                    <p>
                                                        <span style="font-weight: bold; font-size: large; color:black"><s:property value="%{#walletTypeInfo.walletAmountDesc}"/></span>
                                                    </p>
                                                    <span style="color: #443B00; overflow: auto">
                                                            <s:property value="%{#walletTypeInfo.rateDesc}"/>
                                                    </span>
                                                </div>
                                                <div class="reward">
                                                    <span>
                                                              <s:property value="%{#walletTypeInfo.promoteDesc}"/>
                                                    </span>
                                                </div>
                                            </li>
                                </s:iterator>
                                </ul>
                            </div>
                        </s:iterator>
                                <a href="javascript:;" onclick="reload();"  class="btn-submit" id="btnSubmit" >充值</a>
                            </div>
                    </div>
                </s:if>
            </s:form>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2">
                <h5>菜单</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_recharge.php">充值</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 mt-2">
                <h5>社交媒体</h5>
                <ul class="nav">
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 mt-2">
                <h5>联络我们</h5>
                <table>
                    <tbody>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-phone mr-1"></i></td>
                        <td style="color:white">+6014-711 2439</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-envelope mr-1"></i></td>
                        <td style="color:white">corporate@whalemediamy.com</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fab fa-weixin mr-1"></i></td>
                        <td style="color:white">whalelive01</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-map-marker-alt mr-1"></i></td>
                        <td style="color:white">(Level 8) 159, Jalan Temple, Pjs 8, 46050, Petaling jaya, Selangor.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    var delayTime;


    $(function () {
        $('.topUpWhaleCoinModal').click(function () {
            $('.topUpWhaleCoinModal').hide();
        });
        $('.topUpWhaleCoinModal').click(function () {
            $('.topUpWhaleCoinModal').hide();
        });

        // When the user clicks on <span> (x), close the modal
        $('.close').click(function () {
            console.log('s');
            $('#myModal').hide();
        })

        $("#btnProceed").click(function (event) {
            event.preventDefault();
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                url: "<s:url action="topUpWhaleCoin"/>",
                data: {
                    transactionPassword: $('#transactionPassword').val(),
                    paymentOption_topUp: $('#paymentOption_topUp').val(),
                    phoneNo: $('#phonoNoTopUp').val(),
                    password: $('#password').val(),
                    whaleCoinAmount_topUp: $('#whaleCoinAmount_topUp').val(),
                },
                success: function (data, status, xhr) {
                    if (data == null || data == "" || data.actionErrors == "") {
                        alert(data.successMessage);
                        window.location.reload();
                    } else {
                        console.log(data.topUp_errorMessage);
                        alert(data.topUp_errorMessage);
                    }
                }
            });
        });

        $('#btnLogin').click(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "<s:url action="wl_recharge"/>",
                cache: false,
                data: {
                    phoneNo: $('#phoneNo').val(),
                    password: $('#password').val()
                },
                success: function (data, status, xhr) {
                    if (data == null || data == "" || data.actionErrors == "") {
                        window.location.reload();
                    } else {
                        console.log(data.login_errorMessage);
                        $('#login_errorMessage').val(data.login_errorMessage);
                        $('.login_error_message').show();
                    }
                }
            });
        });
    });


    function topUpWhaleCoin(whaleCoinAmt, paymentOption, fromWalletAmt) {
        console.log(whaleCoinAmt);
        console.log(paymentOption);
        console.log(fromWalletAmt);
        var phoneNo = "<s:property value="phoneNo"/>";

        if ("OMC" === paymentOption) {
            $('#myModal').show();
        } else {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                cache: false,
                url: "<s:url action="razerPayRequest"/>",
                data: {
                    paymentOption_topUp: paymentOption,
                    phoneNo: phoneNo,
                    whaleCoinAmount_topUp: whaleCoinAmt,
                    fromWalletAmt_topUp: fromWalletAmt
                },
                success: function (data, status, xhr) {
                    if (data == null || data == "" || data.actionErrors == "") {
                        window.location = data.paymentGateWayUrl;
                    } else {
                        console.log(data.topUp_errorMessage);
                        alert(data.topUp_errorMessage);
                    }
                }
            });
        }

        $("#phonoNoTopUp").val(phoneNo);
        $("#whaleCoinAmount_topUp").val(whaleCoinAmt);
        $("#paymentOption_topUp").val(paymentOption);
        $("#fromWalletAmt_topUp").val(fromWalletAmt);
    }

    function initLang() {

        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }


    $('.charge-item').click(function (event) {
        $target = $(event.target);
        $target.addClass("on").siblings().removeClass("on");
        if($target.hasClass("tab-cash")){
            $("#RAZER").show();
            $("#OMC").hide();
        }else
        {
            $("#OMC").show();
            $("#RAZER").hide();

        }
    })


    $('.buttonbox').click(function (event) {
        $target = $(event.target);
        $target.addClass("on").siblings().removeClass("on");
        $(this).addClass("on").siblings().removeClass("on");
        $("div.reward").removeClass("on");
        $(this).children("div.reward").addClass("on");
        $('.buttonbox').not(this).removeClass('on');
    })

    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });

    function reload()
    {
       if($("li.buttonbox.on").length>0){
           $("li.buttonbox.on").dblclick();
       }else
       {
           alert("Please select one package to reload");
       }
    }
</script>

<style>

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }

    .buttonbox
    {
        width: 260px;
        height: 185px;
        display: inline-block;
        text-align: center;
        margin: 0 30px 30px 0;
        background: #f7f7f7;
        border-radius: 12px;
        cursor: pointer;
        transition: 0.3s all ease;
        -webkit-transition: 0.3s all ease;
        -moz-transition: 0.3s all ease;
        -o-transition: 0.3s all ease;
        -ms-transition: 0.3s all ease;
        overflow: hidden;
        position: relative;
    }

    .buttonbox:hover{
        box-shadow: 0 2px 5px rgb(193 193 193);
    }

    .buttonbox.on {
        background:linear-gradient(45deg, #2F1C53, #A46DFF, #F6D1FD);
    }

    .amount-text{
        padding-top: 10px;
    }

    .main.container { min-height: calc(100% - 1.1rem); margin-bottom: 0; margin-top: 0.1rem; padding-bottom: 0.2rem;background-color: #FFFFFF }





    .main .rec-method { width: 100%; height: 60px; line-height: 60px; margin-top: 20px; margin-bottom: 20px;;border-bottom: 1px solid black; }

    .main .rec-method a { margin-left: 48px; color: #0b0b0b;font-weight:bold ;font-size: 16px; display: inline-block; position: relative; height: inherit; line-height: inherit; }

    .main .rec-method a.on { color: #fd085e; border-bottom: 2px solid #fd085e; }

    .reward.on {
        color: #443B00;
        background: #ffdc00;
    }

    .reward {
        font-size: 13px;
        color: #FD085E;
        min-width: 123px;
        display: inline-block;
        padding: 0 10px;
        min-height: 22px;
        line-height: 22px;
        background: #ffe4ee;
        border-radius: 13px;
        margin: 11px auto 0;
    }

    .coin{
        height:40px;
        width:40px;
    }


    .main .btn-submit { width: 450px; height: 52px; line-height: 52px; background-color: #FD085E; border-radius: 28px; display: block; /* float: right; */ text-align: center; margin: 100px auto 20px; font-size: 16px; font-family: PingFangSC-Regular; font-weight: 400; color: white; }

    .main .btn-submit p { line-height: 17px; }

    .img-razer{
        height:32px;
        width:32px;
        margin:0px 16px 0px 0px;
    }

    .img-omc{
        height:32px;
        width:32px;
        margin:0px 16px 0px 0px;
    }
    @media screen and (min-width: 320px) and (max-width: 800px){
        .main .rec-method {
            margin-top: 0.5rem;
            height: 2.5rem;
            margin-left:10px;
            line-height: 1.2rem;
        }

        .package-list {
            margin-right: 0;
            display: flex;
            flex-flow: wrap;
            justify-content: space-between;
        }

        .main .rec-method a {
            font-size: 13px;
            margin-right: 0.6rem;
            margin-left: 0;
        }

        .main .btn-submit{
            width:180px;
        }

        .img-razer{
            height:30px;
            width:30px;
        }

        .img-omc{
            height:30px;
            width:30px;
        }
    }

</style>

<!-- #modal-dialog -->

<div id="myModal" class="modal">
    <div class="modal-content">
        <span id="close" class="close">&times;</span>
        <s:form cssClass="form-horizontal" name="topUpWhaleCoinForm"
                id="topUpWhaleCoinForm">

            <h4 class="modal-title">
                <s:text name="topUp"/>
            </h4>

            <div class="form-group">
                <label class="control-label"><s:text name="memberId"/></label>
                <div>
                    <s:textfield name="displayCode" id="displayCode" readonly="true"/>
                    <s:textfield name="phonoNoTopUp" id="phonoNoTopUp" hidden="true" readonly="true"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><s:text name="fromWallet"/></label>
                <div>
                    <s:textfield name="paymentOption_topUp" id="paymentOption_topUp" readonly="true"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><s:text name="estimateCryptoAmount"/></label>
                <div>
                    <s:textfield name="fromWalletAmt_topUp" id="fromWalletAmt_topUp" readonly="true"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><s:text name="totalWhaleCoin"/></label>
                <div>
                    <s:textfield name="whaleCoinAmount_topUp" id="whaleCoinAmount_topUp" readonly="true"/>
                    <div style="color: red">
                        <s:text name="cryptoAmountMightChangeDueToExchangeRate"/>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><s:text name="password"/></label>
                <div>
                    <s:password name="password" id="password"/>
                </div>
            </div>

            <div class="form-group">
                <label class="control-label"><s:text name="transactionPassword"/></label>
                <div>
                    <s:password name="transactionPassword" id="transactionPassword"/>
                </div>
            </div>

            <s:submit id="btnProceed" type="button" class="btn btn-secondary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnProceed"/>
            </s:submit>
        </s:form>
    </div>
</div>

<%--<form method="POST" target="_blank" action="<s:property value="paymentGateWayUrl"/>"--%>
<%--      enctype="multipart/form-data"--%>
<%--      name="molpayGatewayForm" id="molpayGatewayForm">--%>
<%--    <s:hidden name="bill_mobile" id="bill_mobile"/>--%>
<%--    <s:hidden name="amount" id="amount"/>--%>
<%--    <s:hidden name="vcode" id="vcode"/>--%>
<%--    <s:hidden name="orderid" id="orderid"/>--%>
<%--    &lt;%&ndash;    <s:hidden name="returnurl" id="razerDto.returnUrl"/>&ndash;%&gt;--%>
<%--    <s:submit type="submit" value="go"/>--%>
<%--</form>--%>
