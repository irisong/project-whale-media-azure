<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-ui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/datatables/jquery.dataTables.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/asset/plugin/jeasyui/jquery.easyui.custom.min.js"/>" type="text/javascript" ></script>
<script src="<c:url value="/asset/whale_media/asset/js/common-jquery-struts2.js"/>" type="text/javascript" ></script>

<script type="text/javascript">
    var table = null;
    $(function () {
        table = $('#castingTable').DataTable({
            "order": [],
            "ajax": {
                "url": "/app/member/admin/guildCastingDatatables.php"
            },
            "columns": [
                {"data": "datetimeAdd"},
                {"data": "whaleLiveId"},
                {"data": "whaleliveProfileName"},
                {"data": "fullName"},
                {"data": "phoneNo"},
                {"data": "gender"},
                {"data": "age"},
                {
                    "data": "status",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '';
                        result += '<button type="button" class="btn btn-danger btn-xs btnView"><s:text name="btnView"/></button>&nbsp;';
                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "castId"
        });

        $(document).on("click", ".btnAddCasting", function (event) {
            $("#addCastingModal").dialog().dialog('open');
            $("#whaleLiveId").focus();
        });


        $("#guildCastingAddForm").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback

                });
            },rules : {
                "whaleLiveId" : "required",
                "email" : {
                    required : true,
                    email: true,
                    minlength : 5
                },
                "fullName" : {
                    required : true
                },
                "identityNo" : {
                    required : true,
                    minlength : 10
                },
                "talent" : {
                    required : true,
                },
                "age" : {
                    required : true,
                    maxlength : 3,
                    min: 1
                },
                "phoneNo" : {
                    required : true,
                    minlength : 10
                },
                "gender" : {
                    required : true,
                },
                "igFans" : {
                    min : 0,
                },
                "weiboFans" : {
                    min : 0,
                },
                "tiktokFans" : {
                    min : 0,
                }
            }
        });

        $(document).on("click", ".btnView", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="wl_guild_casting_view"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnEdit", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="wl_guild_casting_edit"/>")
            $("#navForm").submit();
        });


    });


    function dismissAllDialog() {
        $("#addCastingModal").dialog('close');
    }

    function defaultJsonCallback(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                dismissAllDialog();
               window.location.reload();
            },
            onFailure: function (json, error) {
                alert(error);
            }
        });
    }

</script>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/asset/whale_media/asset/css/jquery.dataTables.min.css"/>" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/asset/plugin/jeasyui/themes/bootstrap/easyui.css"/>" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/smartadmin/css/smartadmin-production-plugins.min.css"/>" type="text/css" />

</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild.php">公会管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild_casting.php">招募管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_income_report.php">月入报告</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">资料管理</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="wl_change_profile.php" >更换资料</a>
                        <a class="dropdown-item" href="wl_change_password.php">更换密码</a>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="btn btn-secondary" href="/logout" title="Sign Out" data-action="userLogout">登出</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>


            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1">
        <div class="container-fluid body py-3">
            <span class="circle-bg"></span>
            <span class="circle-sm"></span>
            <form id="navForm" method="post">
                <input type="hidden" name="id" id="id"/>
            </form>

            <s:form id="guildCastingForm" name="guildCastingForm">
                <div class="col-lg-6 col-md-8 mx-auto mt-3">
                    <h5>Guild Name</h5>
                    <div>
                        <s:textfield name="broadcastGuild.name" id="broadcastGuild.name" label="%{getText('name')}" cssClass="input-xxlarge" readonly="true"/>
                        <s:hidden  name="broadcastGuild.id" id="broadcastGuild.id" cssClass="input-xxlarge"/>

                    </div>
                </div>
                <div class="title">
                    <s:text name="title.casting"/>
                </div>
                <div class="main container card-header">
                    <div class="button" >
                        <button id="btnAddCasting" type="button" class="btn btn-primary btnAddCasting">
                            <s:text name="btnAddCasting"/>
                        </button>

                    </div>
                    <article class="col-md-12 col-sm-12">
                        <div class="well">
                            <article class="col-md-12 col-sm-12">
                                <table id="castingTable" class="table table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th><s:text name="datetimeAdd"/></th>
                                        <th><s:text name="whaleliveId"/></th>
                                        <th><s:text name="profileName"/></th>
                                        <th><s:text name="contactPerson"/></th>
                                        <th><s:text name="phoneNo"/></th>
                                        <th><s:text name="gender"/></th>
                                        <th><s:text name="age"/></th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </article>
                        </div>
                    </article>
                </div>
            </s:form>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>

        </div>
    </div>
</footer>
<div id="addCastingModal" class="easyui-dialog editModal" title="<s:text name="title.casting"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <s:form action="guildAddCasting" cssClass="form-horizontal" name="guildCastingAddForm"
            id="guildCastingAddForm">
        <fieldset>
            <div id="legend">
                <legend class="" style="margin-top:2%;margin-bottom:2%;font-size: 22px">个人资料</legend>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="testLiveTimestamp">试播日期</label>
                    <input type="datetime-local" class="form-control casting" id="testLiveTimestamp" name="testLiveTimestamp">
                </div>
                <div class="col-sm-6">
                    <label for="experience">直播经验</label>
                    <input type="checkbox" class="form-control casting" id="experience" name="experience" >
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="whaleLiveId">鲸娱号</label>
                    <input type="number" class="form-control casting" id="whaleLiveId" name="whaleLiveId" placeholder="鲸娱号" >
                </div>
                <div class="col-sm-6">
                    <label for="whaleLiveName">鲸娱用户名</label>
                    <input type="text" class="form-control casting" id="whaleLiveName" name="whaleLiveName" placeholder="鲸娱用户名" >
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="fullName">姓名</label>
                    <input type="text" class="form-control casting" id="fullName" name="fullName" placeholder="姓名">
                </div>
                <div class="col-sm-6">
                    <label for="identityNo">身份证号码</label>
                    <input type="number" class="form-control casting" id="identityNo" name="identityNo" placeholder="身份证号码">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="phoneNo">联系号码</label>
                    <input type="number" class="form-control casting" id="phoneNo" name="phoneNo" placeholder="输入号码而已" >
                </div>
                <div class="col-sm-6">
                    <label for="phoneModel">手机型号</label>
                    <input type="text" class="form-control casting" id="phoneModel" name="phoneModel" placeholder="手机型号" >
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="gender" >性别:</label>
                    <select name="gender" id="gender" class="form-control casting">
                        <option value="">请选择</option>
                        <option value="M">男</option>
                        <option value="F">女</option>
                    </select>

                </div>
                <div class="col-sm-6">
                    <label for="age">年龄</label>
                    <input type="number" class="form-control casting" id="age" name="age">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="email">电邮地址</label>
                    <input type="text" class="form-control casting" id="email" name="email" placeholder="Example@hotmail.com">
                </div>
                <div class="col-sm-6">
                    <label for="location">所在城市</label>
                    <input type="text" class="form-control casting" id="location" name="location" placeholder="所在城市">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="fbId">Facebook</label>
                    <input type="text" class="form-control casting" id="fbId" name="fbId" >
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="igId">Instagram</label>
                    <input type="text" class="form-control casting" id="igId" name="igId" >
                </div>
                <div class="col-sm-6">
                    <label for="igFans">Instagram Fans/Follower</label>
                    <input type="number" class="form-control casting" id="igFans" name="igFans">
                </div>


            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="tiktokId">抖音号</label>
                    <input type="text" class="form-control casting" id="tiktokId" name="tiktokId">
                </div>
                <div class="col-sm-6">
                    <label for="tiktokFans">抖音 Fans/Follower</label>
                    <input type="number" class="form-control casting" id="tiktokFans" name="tiktokFans">
                </div>
            </div>
            <div class="form-group row">
                <div class="col-sm-6">
                    <label for="weiboId">微博号</label>
                    <input type="text" class="form-control casting" id="weiboId" name="weiboId">
                </div>
                <div class="col-sm-6">
                    <label for="weiboFans">微博 Fans/Follower</label>
                    <input type="number" class="form-control casting" id="weiboFans" name="weiboFans">
                </div>
            </div>

            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="url">云端硬盘链接</label>
                    <input type="text" class="form-control casting" id="url" name="url" placeholder="www.xxx.com" >
                </div>

            </div>
            <div class="form-group row">
                <div class="col-sm-12">
                    <label for="talent">才艺</label>
                    <input type="text" class="form-control casting" id="talent" name="talent">
                </div>

            </div>
            <div class="control-group">
                <!-- Button -->
                <button id="btnSubmit" type="submit" class="btn btn-success btn-block" style="border-radius: 30px;margin-top: 3%">Register</button>
            </div>
        </fieldset>
    </s:form>
</div>
<script type="text/javascript">
    var delayTime;

    function initLang() {

        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }


    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });



</script>

<style>

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .recharge-1 {
        padding-top: 10.3rem;
        background-color: #400b75;
        color: #fff;
    }

    .btn-danger{
        background-color: #0f75f5;
        border-color: #212529;
    }

    .btn-warning{
        background-color: #ffee00;
        border-color: #212529;
    }

    .main.container { min-height: calc(100% - 1.1rem); margin-bottom: 0; margin-top: 0.1rem; padding-bottom: 0.2rem;background-color: #FFFFFF }

    .row{
        margin-left : 15px;
        margin-top  : 10px;
    }

    .title
    {
        padding-left: 300px;
        font-weight: bold;
        font-size:1.575rem;
    }

    .editModal
    {
        display: none;
        width:1100px;
        height:750px;
    }

    .button{
        margin-left: 250px;
        margin-top:50px;
    }

    @media screen and (min-width: 320px) and (max-width: 800px){
        .title
        {
            padding-left: 0px;
            font-weight: bold;
            font-size:0.9rem;
        }
        .main.container {
            overflow: scroll;
        }

        table.dataTable {
            margin-left: -40px;
            font-size: 0.8rem;
        }

        .editModal
        {
            display: none;
            width:300px;
            height:350px;
        }

        .button{
            margin-left: 100px;
            margin-top:30px;
        }

    }


</style>


