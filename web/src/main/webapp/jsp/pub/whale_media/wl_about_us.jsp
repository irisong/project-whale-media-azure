<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>

<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>
<s:if test="language=='zh'">
    <body onload="initLang('zh','<s:text name="CN"></s:text>')"></body>
</s:if>
<s:else>
    <body onload="initLang('en','<s:text name="EN"></s:text>')"></body>
</s:else>
<main>

    <s:if test="language=='zh'">
        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h2 style="padding-top:40px;">关于我们</h2>
            </div>
        </section>
        <section class="live-2" style="padding-top:20px">

            <div class="container py-2">
                <p align="justify">WHALE LIVE鲸娱直播是马来西亚WHALE MEDIA SDN BHD鲸娱媒体在2020年所推出的综合性大数据库的直播平台，更是全马第一家实行智慧产权上链的直播平台，可以确保每一位创作人的作品版权受到保护，以及获得价值肯定。</p>
                <p align="justify">WHALE LIVE鲸娱直播的后台技术由马来西亚科技技术公司AIO Synergy、OMC GROUP共同携手打造，鲸娱媒体于2020年全力推广首发产品—WHALE LIVE鲸娱直播，以三个阶段性进行，第一阶段以“娱乐直播”为发展方向，目标打造海量主播及粉丝群体直播平台，第二阶段以“直播带货”为发展方向，目标以带货主播自身拥有的大量粉丝群体帮助商家打开产品的曝光和销量，第三阶段以孵化“MCN（multi channel network）网络艺人”为发展方向，目标让更多平台主播升级为网络艺人，火遍东南亚乃至全世界，为怀有梦想的人搭建更广阔的舞台，赋予无限的机遇！</p>
            </div>
        </section>
    </s:if>
    <s:else>
        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h2 style="padding-top:40px;">About Us</h2>
            </div>
        </section>
        <section class="live-2" style="padding-top:20px">

            <div class="container py-2">
                <p align="justify">WHALE LIVE is a comprehensive large database live broadcast platform launched by WHALE MEDIA SDN BHD Malaysia WHALE MEDIA SDN BHD Whale Entertainment Media in 2020. It is also the first live broadcast platform in Malaysia to implement smart property rights on the chain, ensuring every creation The copyright of people’s works is protected and value affirmed.</p>
                <p align="justify">The background technology of WHALE LIVE whale entertainment live broadcast is jointly developed by Malaysian technology companies AIO Synergy and OMC GROUP. Whale entertainment media will fully promote the first product-WHALE LIVE whale entertainment live broadcast in 2020. It will be carried out in three phases. "Entertainment live broadcast" is the development direction, and the goal is to build a massive live broadcast platform for anchors and fan groups. The second stage is to "live live and bring goods" as the development direction. The goal is to bring goods to the host’s large number of fans to help businesses open up product exposure and sales. , The third stage takes the incubation of "MCN (multi channel network) network artists" as the development direction. The goal is to upgrade more platform anchors to network artists, popular in Southeast Asia and the world, and build a broader stage for people with dreams. Give unlimited opportunities!</p>
            </div>
        </section>
    </s:else>
</main>
<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd 版权所有
    </div>
</footer>

<script type="text/javascript">

    function initLang(langCode, currentLang) {

        if ((currentLang != 'English' && langCode == 'en') || (currentLang == 'English' && langCode == 'zh')) {
            $.ajax({
                type: 'POST',
                url: "<s:url action="initLangWhaleLivePage"/>",
                dataType: 'json',
                cache: false,
                data: {
                    language: langCode
                },
                success: function (data, status, xhr) {
                    window.location.reload();
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }
            });
        }
    }

    function reload() {
        window.location.reload();
    }


</script>

<style>

    .container {
        padding-right: 50px;
        padding-left: 50px;
    }

    .media-4:after {
        transform: skewY(-1.70deg);
    }

    ol {
        list-style-type: none;
        counter-reset: item;
        margin: 0;
        padding: 0;
    }

    ol > li {
        display: table;
        counter-increment: item;
        margin-bottom: 0.6em;
    }

    ol > li:before {
        content: counters(item, ".") ". ";
        display: table-cell;
        padding-right: 0.6em;
    }

    li ol > li {
        margin: 0;
    }

    li ol > li:before {
        content: counters(item, ".") " ";
    }

</style>

<!-- #modal-dialog -->

