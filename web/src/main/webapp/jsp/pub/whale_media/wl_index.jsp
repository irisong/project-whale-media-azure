<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!doctype html>
<html lang="en">
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<body>
<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_casting.php">网络艺人招募</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_recharge.php">充值</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_download.php">应用程序</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_login.php">登入</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>

            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="media-1">
        <div class="body">
            <div class="content font-alt">
                <h1>全马首个最大网络艺人孵化中心</h1>
                <h1>马来西亚最大市场数据库平台</h1>
            </div>
        </div>
    </section>

    <section class="media-2">
        <div class="container">
            <div class="row">
                <div class="col-md-6 order-md-2">
                    <img src="<c:url value="/asset/whale_media/asset/misc/1.png"/>" class="img-fluid mb-3">
                </div>
                <div class="col-md-6 order-md-1">
                    <h3 class="font-alt">01</h3>
                    <h3 class="mb-3">提供市场99%口碑导购率</h3>
                    <h3 class="font-alt">02</h3>
                    <h3>给用户百分百的品质推荐</h3>
                </div>
            </div>
        </div>
    </section>
    <section class="media-3">
        <div class="container py-5">
            <h3 class="text-center">B2B2C & O2O</h3>

            <div class="grid medium-4 xsmall-2">
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/1.png"/>" class="img">
                    <h4 class="title">活动策划</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/2.png"/>" class="img">
                    <h4 class="title">发布与推广</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/3.png"/>" class="img">
                    <h4 class="title">网络艺人与博客策略</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/4.png"/>" class="img">
                    <h4 class="title">跨领域品牌结合</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/5.png"/>" class="img">
                    <h4 class="title">市场分析调查</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/6.png"/>" class="img">
                    <h4 class="title">创造品牌价值</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/7.png"/>" class="img">
                    <h4 class="title">线上广告投放</h4>
                </div>
                <div class="item">
                    <img src="<c:url value="/asset/whale_media/asset/service/8.png"/>" class="img">
                    <h4 class="title">社交平台客服管理</h4>
                </div>
            </div>
        </div>
    </section>

    <section class="media-4">
        <div class="container py-3">
            <h3 class="text-center">Whale Media 企业文案</h3>
            <p>互联网普及、电商基础设施完善促进了网络直播的风起云涌，成为如今最火的互联网“风口”之一。</p>
            <p>疫情之下，万物皆可直播。一场直播上千万人围观，100万套面膜瞬间抢光，2小时带货2亿多元。</p>
            <p>时代，真的变了！你准备好了吗？</p>
            <p>现在素人要出头，已经不再需要经过电视台或经纪公司的多方检验，娱乐圈里的大浪淘沙，网络直播已经成为推动素人出头的孵化神器、成名的快速途径。</p>
            <p>WHALE MEDIA鲸娱媒体抓住直播带货及娱乐直播的大趋势，打造鲸娱直播平台，致力于将素人打造成为当红直播。</p>
        </div>
    </section>


    <section class="media-6">
        <div class="container">
            <div class="row align-items-center">

            </div>
        </div>
    </section>
    

    <section class="media-8">
        <div class="container py-5">
            <div class="row align-items-center">
                <div class="col-md-6 ml-md-auto"><img src="<c:url value="/asset/whale_media/asset/event/1.png"/>"
                                                      class="img mx-auto ml-md-auto mr-md-0"></div>
                <div class="col-md-6">
                    <h3 class="text-purple font-alt">1.0</h3>
                    <h3>娱乐直播</h3>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 mr-md-auto order-md-2"><img
                        src="<c:url value="/asset/whale_media/asset/event/2.png"/>" class="img mx-auto mx-md-0"></div>
                <div class="col-md-6 text-md-right order-md-1">
                    <h3 class="text-purple font-alt">2.0</h3>
                    <h3>直播带货/口碑导购</h3>
                </div>
            </div>
            <div class="row align-items-center">
                <div class="col-md-6 ml-md-auto"><img src="<c:url value="/asset/whale_media/asset/event/3.png"/>"
                                                      class="img mx-auto ml-md-auto mr-md-0"></div>
                <div class="col-md-6">
                    <h3 class="text-purple font-alt">3.0</h3>
                    <h3>MCN孵化中心</h3>
                </div>
            </div>
        </div>
    </section>

    <section class="media-9">
        <span class="circle-bg"></span>
        <span class="circle-sm"></span>
        <div class="container py-3">
            <div class="row">
                <div class="col-md-3">
                    <h1>公司发展方向</h1>
                    <p>将打造12,600方尺打造全方位办公空间</p>
                </div>
                <div class="col-md-9">
                    <div class="grid gap-2 medium-3 xsmall-2">
                        <a href="<c:url value="/asset/whale_media/asset/office/1.png"/>" target="_blank"><img
                                src="<c:url value="/asset/whale_media/asset/office/1.png"/>" class="img-fluid"></a>
                        <a href="<c:url value="/asset/whale_media/asset/office/1.png"/>" target="_blank"><img
                                src="<c:url value="/asset/whale_media/asset/office/2.png"/>" class="img-fluid"></a>
                        <a href="<c:url value="/asset/whale_media/asset/office/1.png"/>" target="_blank"><img
                                src="<c:url value="/asset/whale_media/asset/office/3.png"/>" class="img-fluid"></a>
                        <a href="<c:url value="/asset/whale_media/asset/office/1.png"/>" target="_blank"><img
                                src="<c:url value="/asset/whale_media/asset/office/4.png"/>" class="img-fluid"></a>
                        <a href="<c:url value="/asset/whale_media/asset/office/1.png"/>" target="_blank"><img
                                src="<c:url value="/asset/whale_media/asset/office/5.png"/>" class="img-fluid"></a>
                        <a href="<c:url value="/asset/whale_media/asset/office/1.png"/>" target="_blank"><img
                                src="<c:url value="/asset/whale_media/asset/office/6.png"/>" class="img-fluid"></a>

                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2">
                <h5>菜单</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_recharge.php">充值</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 mt-2">
                <h5>社交媒体</h5>
                <ul class="nav">
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 mt-2">
                <h5>联络我们</h5>
                <table>
                    <tbody>
                    <tr>
                        <td class="align-top"><i class="fas fa-phone mr-1"></i></td>
                        <td>+6014-711 2439</td>
                    </tr>
                    <tr>
                        <td class="align-top"><i class="fas fa-envelope mr-1"></i></td>
                        <td>corporate@whalemediamy.com</td>
                    </tr>
                    <tr>
                        <td class="align-top"><i class="fab fa-weixin mr-1"></i></td>
                        <td>whalelive01</td>
                    </tr>
                    <tr>
                        <td class="align-top"><i class="fas fa-map-marker-alt mr-1"></i></td>
                        <td>(Level 8) 159, Jalan Temple, Pjs 8, 46050, Petaling jaya, Selangor.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</footer>

<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>

<script>
    function initLang() {
        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });
</script>
</body>
</html>
