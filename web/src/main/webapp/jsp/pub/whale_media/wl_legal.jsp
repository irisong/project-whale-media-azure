<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!doctype html>
<html>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <style>
        .container {
            padding-right: 50px;
            padding-left: 50px;
        }

        ol {
            padding-left: 20px;
        }

        ul {
            padding-left: 20px;
        }

        li {
            margin-bottom: 10px;
        }

        .media-4:after {
            transform: skewY(-1.70deg);
        }
    </style>
</head>

<body>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<main>
    <s:if test="language=='zh'">
    <section class="media-4">
        <div class="text-center" style="height:80px">
            <h3 style="padding-top:40px;">Whale Live 法律执行请求策略 (“策略”)</h3>
            <h3 style="padding-top:40px;"></h3>
        </div>
    </section>

    <section class="live-2" style="padding-top:20px">
        <div class="container py-1">
            <p align="justify">Whale Live 将根据其使用条款、隐私政策和本政策，回应有关用户数据的法律执行和其他法律请求。这项政策并不是要创造任何针对Whale Live 的可强制执行的权利。本公司的政策日后可能会更新或更改，而毋须另行通知有关政府或执法机关。与此同时，Whale Live 活保留所有权利。</p>

        </div>
        <div class="container py-2">
            <h5>执法部门的请求</h5>
            <ul style="list-style-type:disc;">
                Whale Live 将考虑以下与用户数据相关的请求:

            </ul>
            <ul>
                <li><b>数据请求.</b> 除紧急请求外(如下文第2段所述), 所有有关用户数据的请求都必须通过有效的法律程序。我们将根据法律要求对这些要求作出回应。我们将考虑的数据要求包括但不限于:
                </li>
                <ol style="list-style-type:lower-latin;">
                    <li style="text-align: justify">必须与正式刑事调查有关的有效传票以强制用户披露基本信息，包括:姓名、电话号码、电子邮件地址、个人资料照片、兴趣、年龄、性别、家乡、教育程度、职业以及最近的登录/注销IP地址(如果有的话);</li>
                    <li style="text-align: justify">除上述基本用户信息外，还需要有效的法庭命令, 以强制披露与账户有关的某些记录或其他信息, 不包括通信内容其中可能包括的邮件标题和IP地址;</li>
                    <li style="text-align: justify">需要根据适用的刑事和/或民事法律程序在显示可能的原因情况下签发有效的手令，包括搜查令，以迫使披露任何帐户的存储内容，其中可能包括消息、照片、视频、评论和位置信息;</li>
                    <li style="text-align: justify">根据相互法律援助条约 (“MLAT”) 提出的请求，而这种请求是按照适用于有关多边法律援助条约的规定程序提出的;和</li>
                    <li style="text-align: justify"> 来自任何司法管辖区的请求，如果我们真诚相信该司法管辖区的法律要求需作出回应，受影响的用户也在该司法管辖区，且该请求符合国际公认标准。</li>
                </ol>
                <li style="text-align: justify"><b>紧急请求。</b>我们将在我们认为如果我们不立即作出回应，可能涉及真实和紧迫的死亡危险或严重的人身伤害、对未成年人的伤害或其他紧急危险的情况下，我们将对用户的数据请求作出回应。紧急请求必须由执法人员提交，并在使用紧急请求表格作伪证的处罚下签署。如果你不是一个执法代理人和意识到紧急情况下, 请通知你们当地的执法官员。如果我们选择作出这样的信息披露,我们将只披露需要解决紧急情况的用户信息。所有紧急情况的披露必须得到Whale Live法律团队的批准，他们的联系方式如下。</li>
                <li style="text-align: justify"><b>保存请求。</b>我们也会考虑在官方刑事调查或其他官方法律程序中保存用户现有数据的请求。如果我们认为保存请求有效，我们将在正式数据请求服务之前，将用户现有数据保存30天或更长的时间。在适用法律允许的情况下，也可以进一步延长保存期限。</li>
                <li style="text-align: justify"><b>其他信息请求。</b>我们将考虑有关信息的非执法请求，包括来自监管机构和税务机关的请求。这样的请求可以通过联系WhaleLive法律团队admin@whalemedia 来提出。</li>

            </ul>
            <ul>
            <h5>请求内容</h5>

               <p align="justify">所有请求必须以官方的政府/执法机构信笺发出，并包括以下所有信息:</p>

            <ol style="list-style-type:decimal;">
                <li>有效法律程序原件(如适用);</li>
                <li>机构名称;</li>
                <li>代理人名称;</li>
                <li>代理人徽章/身份证号码;</li>
                <li>代理人雇主发出的电子邮件地址;</li>
                <li>代理人直拨电话号码;</li>
                <li>代理人邮寄地址;</li>
                <li>请求响应时间;</li>
                <li>特定用户的 Whale Live ID或用户ID (“UID”); 和</li>
                <li>所要求的具体信息</li>
            </ol>
            </ul>

            <p align="justify">我们将拒绝不包含上述信息的请求。我们也会拒绝那些我们认为不是事实的或实质上有效的请求，或者如果我们认为这些请求过于广泛、模糊, 不清楚或过于繁琐的请求。如果正式要求不是英文的，我们鼓励执法机构提供英文翻译。在没有特殊情况下，Whale Live有义务在遵守法律下执行有效的要求, 我们将不会披露超过必要的用户信息。
            </p>

            <h5>与用户沟通</h5>
            <p align="justify">Whale Live的政策是在信息披露前通知用户他们的信息请求，除非 (i) 有效法律程序、法院命令或适用法律禁止提供此类通知的, (ii) 在通知可能对可识别的个人造成伤害或死亡风险的情况下, 或者 (iii) 涉及对未成年人潜在伤害的案件. 如果数据请求包含保密命令，Whale Live 将在该命令被推翻或到期时通知受影响的用户。</p>

            <h5>用户同意</h5>
            <p align="justify">执法人员要求提供有关用户的信息並且已获得用户同意该执法人员的进入，应指示该用户自行从其帐户获取该信息</p>

            <h5>法律程序</h5>
            <p align="justify"> 我们鼓励执法机构通过电子邮件admin@whalemediamy.com与我们联系。在提出正式请求之前应考虑用户信息的可用性。资料要求可通过挂号信、速递或亲自送达下列地点:</p>

            <h6><b>Whale Media Sdn Bhd</b><br>Attention: Legal Team<br>10 Jalan Templer<br>46050 Petaling, Selangor Darul Ehsan</h6>

            <p align="justify">任何这些方式对法律程序的遵守都是为了方便，并不放弃任何反对意见，包括缺乏管辖权或服务不当。我们无法处理通过电子邮件发送的数据请求。</p>

            <p align="justify">我们可以提供认证证书，但我们一般无法提供当面或专家证人的证词。</p>

            <h5>费用</h5>

            <p align="justify">Whale Live 可能会要求发还因法律许可的执法部门要求提供信息而产生的费用，并可能会对因响应异常或繁重的请求而产生的费用收取额外费用。</p>

            <h5>法律执行实践问题和答案</h5>

            <ul>
                <ol style="list-style-type:decimal;">
                    <li style="text-align: justify">如执法请求政策(“政策”)的请求内容里的一栏所需的信息有缺失，我们是否应予以合作?</li>
                    <p align="justify">我们需要仔细检查是否提供了政策中所列的所有信息。如果有必要的信息缺失，我们可以决定要求相关机构补充缺失的部分。如果这些信息不足以让我们核实，我们可能会忽略这个要求。</p>
                    <li style="text-align: justify">如果相关代理未能提供数据要求的官方文件，我们是否应予以合作? 考虑到这样的代理只是写一封电子邮件来请求用户的数据。</li>
                    <p align="justify">一般而言，数据请求的有效官方文件应在披露用户数据之前提供, 除非是紧急情况. 在这种情况下，执法部门仍应提供一份填妥的紧急请求表格。如果该请求是通过电子邮件而没有任何官方文件，我们可能不会配合该请求。</p>
                    <li style="text-align: justify">如果当地执法机构要求我们的当地机构提供数据，这些当地机构是否需要合作或将要求引导到  Whale Live?</li>
                    <p align="justify">是否需要当地人员与当地执法请求合作，将取决于当地人员是否拥有、保管或控制所要求的资料。这一决定将取决于许多因素，例如本地企业是否有进入数据的技术权限，本地企业是否在其正常业务过程中进入数据，以及与 Whale Live 的关系。如果本地机构没有对数据的占有、保管或控制，它应告知它没有相关的数据来满足请求。当地机构也可将执法人员引向 Whale Live。
                    </p>
                    <li style="text-align: justify">在合作前, 我们是否应该要求执法人员提供硬拷贝文件?</li>
                    <p align="justify">虽然硬拷贝文件可能更容易审查其真实性, 如果执法机构提供了政策所要求的信息,并提供了必要的合法程序的电子副本，且该电子副本看来是有效的，我们不会忧虑是否配合这项请求。如果执法机构提供带有正式文件的紧急请求表格的电子副本，这也适用于紧急情况.
                    </p>
                </ol>
            </ul>

        </div>
    </section>
    </s:if>
<s:else>
    <section class="media-4">
        <div class="text-center" style="height:80px">
            <h3 style="padding-top:40px;">Whale Live Legal Enforcement Request Policy
                (“Policy”)
            </h3>
            <h3 style="padding-top:40px;"></h3>
        </div>
    </section>

    <section class="live-2" style="padding-top:20px">
        <div class="container py-1">
            <p align="justify">Whale Live will respond to legal enforcement and other legal requests for user data in accordance with its Terms of Use, Privacy Policy, and this Policy. Nothing in this Policy is meant to create any enforceable rights against Whale Live. Whale Live’s policies may be updated or changed in the future without further notice to the relevant government or legal enforcement authorities. Meanwhile, Whale Live reserves all rights.
            </p>

        </div>
        <div class="container py-2">
            <h5>Legal Enforcement Requests</h5>
            <ul style="list-style-type:disc;">
                Whale Live will consider the following requests related to users' data:

            </ul>
            <ul>
                <li><b>Data Requests.</b> With the exception of Emergency Requests (as described in paragraph 2 below), all requests for user data must be properly served through valid legal process. We will respond to such requests as required by law. Data requests that we will consider include, but are not limited to:
                </li>
                <ol style="list-style-type:lower-latin;">
                    <li style="text-align: justify">a valid subpoena issued in connection with an official criminal investigation is required to compel the disclosure of basic subscriber information, which may include: name, phone number, email address, profile photo, interests, age, gender, hometown, education, occupation, and any recent login/logout IP address(es), if available;</li>
                    <li style="text-align: justify">a valid court order is required to compel the disclosure of certain records or other information pertaining to the account, not including contents of communications, which may include message headers and IP addresses, in addition to the basic subscriber information identified above;</li>
                    <li style="text-align: justify">a valid warrant, including a search warrant, issued under applicable criminal and/or civil law procedures upon a showing of probable cause is required to compel the disclosure of the stored contents of any account, which may include messages, photos, videos, comments, and location information;</li>
                    <li style="text-align: justify">requests issued pursuant to a Mutual Legal Assistance Treaty (“MLAT”) where such requests have been issued in accordance with the prescribed process, as applicable to the relevant MLAT; and</li>
                    <li style="text-align: justify">requests from any jurisdictions where we have a good-faith belief that a response is required by law in that jurisdiction, the affected users are in that jurisdiction, and the request is consistent with internationally recognized standards.</li>
                </ol>
                <li style="text-align: justify"><b>Emergency Requests. </b>We will respond to requests for user data in situations that we believe in good faith may involve a real and imminent danger of death or serious bodily harm, harm to a minor or other pressing danger as we may determine if we do not respond without delay. Such emergency requests must be submitted by a legal enforcement officer and signed under penalty of perjury using Emergency Request Form. If you are not a legal enforcement agent and are aware of an emergency situation, please notify your local legal enforcement officials. If we choose to make such a disclosure, we will only disclose the user information needed to resolve the emergency. All emergency disclosures must be approved by the Whale Live Legal Team whose contact details are set out below.</li>
                <li style="text-align: justify"><b>Preservation Requests.</b>We will also consider requests to preserve a user's existing data in connection with official criminal investigations or other official legal proceeding. If we deem the preservation request valid, we will maintain a user's existing data for thirty (30) days or longer period, pending service of a formal data request. The preservation hold may also be further extended as permitted under applicable law.</li>
                <li style="text-align: justify"><b>Other Information Requests.</b>We will consider non-legal enforcement requests for information including requests from regulatory agencies and tax authorities. Such requests can be made by contacting the Whale Live Legal Team at legal@whalemediamy.com.
                </li>

            </ul>
            <ul>
                <h5>Request Contents</h5>

                <p>All requests must be sent together with the official government/legal enforcement letterhead and include all of the following information:</p>

                <ol style="list-style-type:decimal;">
                    <li>Original copy of the valid legal process, if applicable;</li>
                    <li>Agency Name;</li>
                    <li>Agent Name;</li>
                    <li>Agent Badge/Identification Number;</li>
                    <li>Agent Employer-Issued Email Address;</li>
                    <li>Agent Direct Phone Number;</li>
                    <li>Agent Mailing Address;</li>
                    <li>Requested Response Date;</li>
                    <li>A specific user's Whale Live ID or user id ("UID"); and</li>
                    <li>The specific information requested</li>
                </ol>
            </ul>

            <p align="justify">We will reject requests that do not include the above information. We will also reject requests that we believe are not facially or substantively valid or if we believe the requests are overly broad, vague, unclear, or unduly burdensome. If the formal request is not in English, we encourage the legal enforcement agencies to provide an English translation. In the absence of exceptional circumstances, we will not disclose any more user information than is necessary to comply with a valid request which Whale Live is obliged to comply with as a matter of law.</p>

            <h5>Communications with Users</h5>
            <p align="justify">Whale Live's policy is to notify users of requests for their information prior to disclosure except (i) where providing such notice is prohibited by the valid legal process, court order, or applicable law, (ii) where notice could create a risk of injury or death to an identifiable individual(s), or (iii) cases that involve potential harm to minors. If a data request includes a nondisclosure order, Whale Live will notify the affected user as soon as the order is overturned or expires on its own terms.</p>

            <h5>User Consent</h5>
            <p align="justify">If a legal enforcement official requests information about a user who has provided consent for the official to access or obtain the user’s account information, the user should be directed to obtain that information on their own from their account.</p>

            <h5>Legal Process</h5>
            <p align="justify">We encourage legal enforcement agencies to contact us via email at legal@whalemediamy.com regarding the availability of user information before serving the formal request. Data requests may be served by certified mail, express courier, or in person at the following location:</p>

            <h6><b>Whale Media Sdn Bhd</b><br>Attention: Legal Team<br>10 Jalan Templer<br>46050 Petaling, Selangor Darul Ehsan</h6>

            <p align="justify">Acceptance of legal process by any of these means is for convenience and does not waive any objections, including lack of jurisdiction or improper service. We are unable to process data requests served via email.</p>

            <p align="justify">We may provide a certificate of authentication, but we generally are not able to provide in-person or expert witness testimony.</p>

            <h5>Costs</h5>

            <p align="justify">Whale Live may seek reimbursement for costs associated with responding to legal enforcement requests for information as permitted by law and may charge additional fees for costs incurred in responding to unusual or burdensome requests.</p>

            <h5>Legal Enforcement Practice Questions & Answers</h5>

            <ul>
                <ol style="list-style-type:decimal;">
                    <li style="text-align: justify">Should we cooperate if any of the information required for the requests, as set out in the Policy is missing?</li>
                    <p align="justify">We need to check carefully if all the information set out in the Policy is provided. If any essential information is missing, we may decide to request the relevant agency to supplement with the missing part. If the information is not sufficient for us to check, we may ignore the request.</p>
                    <li style="text-align: justify">Should we cooperate if the relevant agents fail to provide official documentation of a data request? Consider that such agents simply write an email to request for users’ data.</li>
                    <p align="justify">In general, valid, official documentation of a data request should be provided prior to disclosing user data unless it is an emergency situation, in which case legal enforcement should still provide a completed emergency request form. If the request came via email without any official documentation, we may not cooperate with the request.</p>
                    <li style="text-align: justify">If a local legal enforcement agent requests our local presence to provide data, does such local presence need to cooperate or direct it to Whale Live?</li>
                    <p align="justify">Whether a local presence would be required to cooperate with a local legal enforcement request would depend on whether such local presence has possession, custody, or control of the requested information. This determination will depend on a number of factors, such as whether the local presence has technical access to the data, the local presence accesses the data in the ordinary course of its business, and its ties with Whale Live. If the local presence does not have possession, custody, or control of the data, it should comply with the request by reporting that it does not have responsive data. This local presence may also direct legal enforcement agent to Whale Live.
                    </p>
                    <li style="text-align: justify">Should we require the legal enforcement agent to deliver hard copy documents prior to cooperation?</li>
                    <p align="justify">While hard copy documents may be easier to review for authenticity, if a legal enforcement agency provides the information requested by the policy and provides an electronic copy of the required legal process that appears to be valid, we would not have concerns with cooperating with the request. This would also apply in emergency situations if the legal enforcement agency provides an electronic copy of the emergency request form with official documentations
                    </p>
                </ol>
            </ul>

        </div>
    </section>
    </s:else>
</main>

<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd. All Rights Reserved.
    </div>
</footer>

</body>
</html>
