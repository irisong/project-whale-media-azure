<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-3.1.1.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-ui.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/datatables/jquery.dataTables.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/asset/plugin/jeasyui/jquery.easyui.custom.min.js"/>" type="text/javascript" ></script>

<script type="text/javascript">
    var table = null;
    $(function () {

        table = $('#guildMemberListTable').DataTable({
            "ajax": {
                "url": "/app/member/guildMemberListDatatables.php",
                "data": {
                    guildid: $('#broadcastGuild\\.id').val()
                }
            },
            "columns": [
/*                {"data": "joinDate"},*/
                {"data": "serveStartDate"},
                {"data": "serveEndDate"},
                {"data": "memberCode"               },
                {"data": "memberDetail.whaleliveId"},
                {"data": "memberDetail.profileName"},
                {"data": "rankName"},
                {
                    "data": "guildStatus",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = "";
                        if (data == 'APPROVED') {
                            result = "Approved"
                        }else if(data == 'PENDING_APPROVAL'){
                            result = "Pending Approval"

                        }
                        return result;
                    }
                }

            ],
            "rowId": 'memberId'
        });

        $(document).on("click", ".btnEdit", function (event) {
            var id = $(this).parents("tr").attr("id");
            var guildid = $('#broadcastGuild\\.id').val();
            $("#editMemberForm").trigger("reset");
            $("#editMemberForm_memberId").val(id);
            $("#editMemberModal").dialog().dialog('open');
            $("#memberRank").focus();
        });

        $(document).on("click", ".btnApprove", function (event) {
            var id = $(this).parents("tr").attr("id");
            var guildid = $('#broadcastGuild\\.id').val();
            $("#approveMemberForm_memberId").val(id);
            $("#approveMemberModal").dialog().dialog('open');
            $("#approveMemberRank").focus();
        });

    });

    function dismissAllDialog() {
        $("#addMemberModal").dialog('close');
        $("#dateFrom").val();
        $("#dateTo").val();
        $("#startHalfMonth").checked=false;
        $("#endHalfMonth").checked=false;
        $("#approveMemberModal").dialog('close');
        $("#editMemberModal").dialog('close');
    }

    function defaultJsonCallback(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                dismissAllDialog();
                table.ajax.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

</script>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
    <link rel="stylesheet" href="<c:url value="/asset/whale_media/asset/css/jquery.dataTables.min.css"/>" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/asset/plugin/jeasyui/themes/bootstrap/easyui.css"/>" type="text/css" />
    <link rel="stylesheet" href="<c:url value="/smartadmin/css/smartadmin-production-plugins.min.css"/>" type="text/css" />

</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild.php">公会管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild_casting.php">招募管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_income_report.php">月入报告</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">资料管理</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="wl_change_profile.php" >更换资料</a>
                        <a class="dropdown-item" href="wl_change_password.php">更换密码</a>
                    </div>
                </li>


                <li class="nav-item">
                    <a class="btn btn-secondary" href="/logout" title="Sign Out" data-action="userLogout">登出</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>


            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1">
        <div class="container-fluid body py-3">
            <span class="circle-bg"></span>
            <span class="circle-sm"></span>
            <form id="navForm" method="post">
                <input type="hidden" name="id" id="id"/>
            </form>

            <s:form id="guildMemberForm" name="guildMemberForm">
                <div class="col-lg-6 col-md-8 mx-auto mt-3">
                    <h5>Guild Name</h5>
                    <div>
                        <s:textfield name="broadcastGuild.name" id="broadcastGuild.name" label="%{getText('name')}" cssClass="input-xxlarge" readonly="true"/>
                        <s:hidden  name="broadcastGuild.id" id="broadcastGuild.id" cssClass="input-xxlarge"/>

                    </div>
                </div>
                <div class="title">
                    <s:text name="title.memberList"/>
                </div>
                <div class="main container card-header">
                        <article class="col-md-12 col-sm-12">
                            <div class="well">
                                    <article class="col-md-12 col-sm-12">
                                        <table id="guildMemberListTable" class="display" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
<%--                                                <th><s:text name="dateJoin"/></th>--%>
                                                <th><s:text name="serveStartDate"/></th>
                                                <th><s:text name="serveEndDate"/></th>
                                                <th><s:text name="phoneNo"/></th>
                                                <th><s:text name="whaleliveId"/></th>
                                                <th><s:text name="profileName"/></th>
                                                <th><s:text name="rankName"/></th>
                                                <th><s:text name="status"/></th>
                                              <%--  <th></th>--%>
                                            </tr>
                                            </thead>
                                        </table>
                                    </article>
                                </div>
                        </article>
                </div>
            </s:form>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>

        </div>
    </div>
</footer>
<div id="approveMemberModal" class="easyui-dialog editModal"
     title="<s:text name="title.approveMember"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <s:form action="guildMemberApprove" cssClass="form-horizontal" name="approveMemberForm"
            id="approveMemberForm">
        <s:hidden name="id" />
        <s:hidden name="memberId" />
        <div class="form-group row">
            <div class="col-sm-3" style="alignment: left">
                <label for="memberRank">Member Rank :</label>
            </div>
            <div class="col-sm-6">
                <select name="memberRank" id="appoveMemberRank" class="form-control">
                    <s:iterator status="iterStatus" value="allGuildRankList" var="rank">
                        <option value="<s:property value="%{#rank.key}"/>"><s:property value="%{#rank.value}"/></option>
                    </s:iterator>
                </select>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3" style="alignment: left">
                <label for="dateFrom">From :</label>
            </div>
            <div class="col-sm-5">
                <input type="date" id="appoveDateFrom" name="dateFrom"  >
            </div>
            <div class="col-sm-4">
                <label class="control-label"><s:text name="halfMonth"/>: </label>
                <input type="checkbox" id="appoveStartHalfMonth" name="startHalfMonth"/>
            </div>
        </div>
        <div class="form-group row">
            <div class="col-sm-3" style="alignment: left">
                <label for="dateFrom">To :</label>
            </div>
            <div class="col-sm-5">
                <input type="date" id="appoveDateTo" name="dateTo"  >
            </div>
            <div class="col-sm-4">
                <label class="control-label"><s:text name="halfMonth"/>: </label>
                <input type="checkbox" id="appoveEndHalfMonth" name="endHalfMonth"/>
            </div>
        </div>
        <div class="button" >
            <button class="btn btn-primary btnSave" type="submit">
                <i class="fa fa-plus-circle"></i>
                <s:text name="btnSave"/>
            </button>
        </div>
    </s:form>
</div>

<div id="editMemberModal" class="easyui-dialog editModal"
     title="<s:text name="title.editMember"/>" closed="true" resizable="true" maximizable="true" modal="true">
            <s:form action="guildMemberEdit" cssClass="form-horizontal" name="editMemberForm"
                    id="editMemberForm">
                <s:hidden name="id" />
                <s:hidden name="memberId" />
                <div class="form-group row">
                    <div class="col-sm-3" style="alignment: left">
                        <label for="memberRank">Member Rank :</label>
                    </div>
                    <div class="col-sm-6">
                        <select name="memberRank" id="memberRank" class="form-control">
                            <s:iterator status="iterStatus" value="allGuildRankList" var="rank">
                                <option value="<s:property value="%{#rank.key}"/>"><s:property value="%{#rank.value}"/></option>
                            </s:iterator>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3" style="alignment: left">
                        <label for="dateFrom">From :</label>
                    </div>
                    <div class="col-sm-5">
                        <input type="date" id="dateFrom" name="dateFrom"  >
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                        <input type="checkbox" name="startHalfMonth"/>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3" style="alignment: left">
                        <label for="dateFrom">To :</label>
                    </div>
                    <div class="col-sm-5">
                        <input type="date" id="dateTo" name="dateTo"  >
                    </div>
                    <div class="col-sm-4">
                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                        <input type="checkbox" name="endHalfMonth"/>
                    </div>
                </div>
                <div class="button" >
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                </div>
            </s:form>
</div>
<script type="text/javascript">
    var delayTime;

    function initLang() {

        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }


    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });

</script>

<style>

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .recharge-1 {
        padding-top: 10.3rem;
        background-color: #400b75;
        color: #fff;
    }


    .main.container { min-height: calc(100% - 1.1rem); margin-bottom: 0; margin-top: 0.1rem; padding-bottom: 0.2rem;background-color: #FFFFFF }

    .row{
        margin-left : 15px;
        margin-top  : 10px;
    }

    .title
    {
        padding-left: 300px;
        font-weight: bold;
        font-size:1.575rem;
    }

    .editModal
    {
        display: none;
        width:700px;
        height:350px;
    }

    .button{
        margin-left: 250px;
        margin-top:50px;
    }

    @media screen and (min-width: 320px) and (max-width: 800px){
        .title
        {
            padding-left: 0px;
            font-weight: bold;
            font-size:0.9rem;
        }
        .main.container {
            overflow: scroll;
        }

        table.dataTable {
            margin-left: -40px;
            font-size: 0.8rem;
        }

        .editModal
        {
            display: none;
            width:300px;
            height:350px;
        }

        .button{
            margin-left: 100px;
            margin-top:30px;
        }

    }


</style>


