<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/datatables/jquery.dataTables.min.js"/>" type="text/javascript"></script>
<script type="text/javascript">
    var table = null;
    $(function () {
        table = $('#guildMemberListTable').DataTable({
            "ajax": {
                "url": "/app/member/guildMemberIncomeReportListDatatables.php"
            },
            "columns": [
                {"data": "profileName"},
                {"data": "inAmt"},
                {"data": "outAmt"}
                // {"data": "totalAmt"}
            ],
            "rowId": 'memberId'
        });


    });

    function dismissAllDialog() {
        $("#addMemberModal").dialog('close');
        $("#dateFrom").val();
        $("#dateTo").val();
        $("#startHalfMonth").checked=false;
        $("#endHalfMonth").checked=false;
        $("#approveMemberModal").dialog('close');
        $("#editMemberModal").dialog('close');
    }

    function defaultJsonCallback(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                dismissAllDialog();
                table.ajax.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

</script>
<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_guild.php">公会管理</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_change_password.php">更换密码</a>
                </li>


                <li class="nav-item">
                    <a class="btn btn-secondary" href="/logout" title="Sign Out" data-action="userLogout">登出</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>


            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1">
        <div class="container-fluid body py-3">
            <span class="circle-bg"></span>
            <span class="circle-sm"></span>


            <s:form id="guildMemberForm" name="guildMemberForm">
                <div class="col-lg-6 col-md-8 mx-auto mt-3">
                    <h5>Guild Name</h5>
                    <div>
                        <s:textfield name="broadcastGuild.name" id="broadcastGuild.name" label="%{getText('name')}" cssClass="input-xxlarge" readonly="true"/>
                        <s:hidden  name="broadcastGuild.id" id="broadcastGuild.id" cssClass="input-xxlarge"/>
                    </div>
                </div>
                <div style="padding-left:300px">
                    <h3><s:text name="title.memberList"/></h3>
                </div>
                <div class="main container card-header">
                    <div class="row">
                        <article class="col-md-12 col-sm-12">
                            <div class="well">
                                <div class="row">
                                    <article class="col-md-12 col-sm-12">
                                        <table id="guildMemberListTable" class="table table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th><s:text name="profileName"/></th>
                                                <th><s:text name="inAmt"/></th>
                                                <th><s:text name="outAmt"/></th>
                                                <th><s:text name="withdrawalStatus"/></th>
<%--                                                <th><s:text name="availableBalance"/></th>--%>
<%--                                                <th><s:text name="action"/></th>--%>
                                            </tr>
                                            </thead>
                                        </table>
                                    </article>
                                </div>
                            </div>
                        </article>
                    </div>
                </div>
            </s:form>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>

        </div>
    </div>
</footer>

<script type="text/javascript">
    var delayTime;


    function initLang() {

        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }


    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });

</script>

<style>

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
    .recharge-1 {
        padding-top: 10.3rem;
        background-color: #400b75;
        color: #fff;
    }

    .main.container { min-height: calc(100% - 1.1rem); margin-bottom: 0; margin-top: 0.1rem; padding-bottom: 0.2rem;background-color: #FFFFFF }


</style>


