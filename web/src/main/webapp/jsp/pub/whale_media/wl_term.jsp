<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>

<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>
<s:if test="language=='zh'">
    <main>

        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h2 style="padding-top:40px;">Whale Live 用户使用条款</h2>
            </div>
        </section>
        <div class="container py-1">
            <section class="live-2" style="padding-top:20px">
                </br>
                <p align="justify">本协议是由鲸娱直播服务(以下定义)的用户(“<b>您</b>”)与Whale Live (“<b>Whale Live</b>”)根据马来西亚法律签订的. </p>
                <p align="justify">请仔细阅读并理解本Whale Live用户使用条款(“使用条款”)的条款和条件。除非您接受本使用条款中的条款，否则您无权注册、登录或使用Whale Live的服务。一旦您注册、签署、使用Whale Live 服务，您将被视为接受我们在本协议中规定的条款并受其约束. 如果您不同意本协议的条款，请不要注册、登录或使用我们提供的服务。 </p>

                <ol>
                    <li><b>服务条款及条件</b>
                        <ol></br>
                            <li style="text-align: justify">Whale Live提供在线广播服务、在线游戏服务以及根据本使用条款未定义的其他在线服务(“<b>服务</b>”) 。 </li></br>
                            <li style="text-align: justify">当您完成Whale Live帐户注册后，您将收到一个Whale Live帐户。您必须在每个活动和事件中对您自己的帐户负责。如果您在使用该帐户时以任何方式造成任何损害或损失，您将个人承担责任并在此同意赔偿Whale Live或任何第三方所遭受的一切损害或损失</li></br>
                            <li style="text-align: justify">您承诺只使用您自己的注册凭证登录Whale Live。 </li></br>
                            <li style="text-align: justify">用户理解并接受，Whale Live只是一个服务提供者. 用户须对使用任何相关网络服务设备(例如:个人电脑、手机及其他接入互联网/移动互联网服务的系统或网络)负全部责任和所需费用(例如上网时支付电话费和互联网费，使用移动互联网时支付电话费)。 </li></br>
                        </ol>
                    </li>
                    <li><b>使用规则</b>
                        <ol></br>
                            <li style="text-align: justify">在申请使用Whale Live服务时，你必须提供足够的个人资料，如资料有任何更改，你必须不时更新资料。您有责任确保所有的信息的真实性。 </li></br>
                            <li style="text-align: justify">您不得将您的账户转让、出借或以任何超出您控制的形式与他人进行交易。您应立即通知Whale Live任何未经授权使用您的帐户或任何其他违反安全的行为。如果您未能保持您的帐户安全和机密，Whale Live不承担因您未能遵守这些义务而引起的任何损失或损害。</li></br>
                            <li style="text-align: justify">您有责任承担注册账户所遭受的损失，Whale Live不对您或第三方所造成的任何损失或损害承担责任</li></br>
                            <li style="text-align: justify">您同意展示由Whale Live或Whale Live任何其他授权第三方或Whale Live合作伙伴提供的与您使用服务有关的广告。 </li></br>
                            <li style="text-align: justify">您同意在使用Whale Live服务时遵守以下义务:
                                <ol></br>
                                    <li style="text-align: justify">遵守本使用条款和任何其他相关的Whale Live的政策、规则和指引;</li></br>
                                    <li style="text-align: justify">不允许滥用或滥用Whale Live的服务; </li></br>
                                    <li style="text-align: justify">切勿使用Whale Live的服务进行会或可能影响互联网或流动互联网运作的任何活动;</li></br>
                                    <li style="text-align: justify">切勿使用Whale Live的服务上载、发布或传播任何虚假、歧视、威胁、低俗淫亵或其他非法资料及资讯; </li></br>
                                    <li style="text-align: justify">不得侵犯Whale Live及任何第三方的权利、版权、商标权、名誉权或其他合法权利;</li></br>
                                    <li style="text-align: justify">切勿利用Whale Live的服务进行任何可能导致对Whale Live提出投诉/法律诉讼的行动; </li></br>
                                    <li style="text-align: justify">您应立即通知Whale Live任何未经授权使用您的帐户/密码以及任何其他违反安全的行为。</li></br>
                                </ol>
                            </li>
                            <li style="text-align: justify">如果您在使用服务时出现安全漏洞，Whale Live或Whale Live的授权人有权要求您纠正或立即采取行动，以减少任何潜在的或现有的非法行为的影响。</li></br>
                            </li>
                        </ol>
                    </li>
                    <li><b>变更，停止或终止</b>
                        <ol></br>
                            <li style="text-align: justify">您同意Whale Live保留在不通知您的情况下自行决定更改、停止或终止部分或全部服务的权利，在这种情况下，Whale Live不会对您或任何第三方造成的任何损害负责。 </li></br>
                            <li style="text-align: justify">您同意Whale Live应定期或在必要时对服务或任何相关设备进行维护，且在提前通知您的情况下，在合理时间内对服务的任何中断不负任何责任。 </li></br>
                            <li style="text-align: justify">如出现下列任何情况，Whale Live有权随时终止或停止您使用服务，并终止本使用条款，而无须向您或任何第三方承担任何责任:
                                <ol></br>
                                    <li style="text-align: justify">您提供虚假或不准确的个人信息;</li></br>
                                    <li style="text-align: justify">您违反了任何法律、法规或违反了本协议的任何条款。 </li></br>
                                </ol>
                            </li></br>
                        </ol>
                    </li>

                    <li><b>知识产权</b>
                        <ol></br>
                            <li style="text-align: justify">Whale Live提供的服务应包括但不限于以下内容:文字、照片、图形、音频和视频材料，所有内容均受版权、商标或其他法律保护。因此，未经Whale Live明确的书面许可，您不得将Whale Live的任何财产用于您或任何第三方的任何商业目的。 </li></br>
                            <li style="text-align: justify">Whale Live提供的任何软件工具，包括但不限于任何图像、图片、动画、录像、录音、歌曲、文字以及附带命令和有用资料的权利绝对属于Whale Live。因此，在没有Whale Live明确的书面同意的情况下，您不能对Whale Live的任何财产进行反向工程、反编译或反汇编。 </li></br>
                        </ol>
                    </li>

                    <li><b>隐私保护</b>
                        <ol></br>
                            <li>Whale Live承诺严格保密并不会向其他个人或组织披露您在本服务上的注册信息和任何收集到的内容，除非出现以下情况:
                                <ol></br>
                                    <li style="text-align: justify">已获得您的明确授权;</li></br>
                                    <li style="text-align: justify">依照并符合法律或法律要求; </li></br>
                                    <li style="text-align: justify">根据政府部门的明确要求; </li></br>
                                    <li style="text-align: justify">保障公众利益;和 </li></br>
                                    <li style="text-align: justify">保护Whale Live 的合法权益。 </li></br>
                                </ol>
                            </li>
                            <li style="text-align: justify">Whale Live可能会与第三方合作为您提供服务。在这种情况下，如果第三方承诺根据本使用条款和隐私政策保护和保障您的隐私，您将允许Whale Live向第三方提供用户的注册信息。 </li></br>
                            <li style="text-align: justify">允许Whale Live分析您的整个用户数据，并将其用于Whale Live的业务目的。 </li></br>
                        </ol>
                    </li>

                    <li><b>免责声明</b>
                        <ol></br>
                            <li style="text-align: justify">如果发生与本协议任何条款相关的任何争议，您同意首先通过善意协商解决争议;否则，任何一方可将相关争议提交吉隆坡仲裁委员会，仲裁员的任何裁决均为终局裁决，对各方均有约束力。 </li></br>
                            <li style="text-align: justify">Whale Live不保证任何超出其控制范围的第三方托管的外部链接的准确性和完整性，并且对于因您对其中内容的依赖而造成的任何损害，Whale Live不承担任何责任。 </li></br>
                            <li style="text-align: justify">Whale Live不承担任何对你或第三方应该在任何通信系统或互联网网络错误,计算机故障或病毒、信息腐败、电脑系统问题或其他相关问题的损失,但Whale Live 将会以最快可能应当努力纠正任何问题。 </li></br>
                        </ol>
                    </li>
                    <li><b>制定法律和解决争议</b>
                        </br></br>
                        如果发生与本协议任何条款相关的任何争议，您同意首先通过善意协商解决争议;否则，任何一方可将相关争议提交吉隆坡仲裁委员会，仲裁员的任何裁决均为终局裁决，对各方均有约束力。 </br></br>

                    </li>
                    <li><b>物品出售</b>
                        <ol></br>
                            <li style="text-align: justify">Whale Live的商业本质是在线直播服务。 </li></br>
                            <li style="text-align: justify">如果您年满18岁(或您所在国家/地区的法定年龄)，您可以通过充值购买Whale Live的虚拟货币Whale Coin。您可以使用Whale Coin购买虚拟礼品，接收用户将获得虚拟 Baby Whale Point。 </li></br>
                            <li style="text-align: justify">Whale Coin 的价格在购买时显示，并可能不时波动。所有Whale Coin 的费用和支付均以购买时指定的货币通过相关支付网关进行。货币兑换结算费用、对外交易费用、支付渠道费用(如有)均基于您与相应支付网关之间的协议。 </li></br>
                            <li style="text-align: justify">Whale Coin 没有有效期限。 </li></br>
                            <li style="text-align: justify">您将承担购买 Whale Coin期间产生的任何费用。购买交易完成后，购买的 Whale Coin 将存入您的账户。您可以使用Whale Coin 购买礼品，但 Whale Coin 不能兑换现金、法定货币、任何国家、地区或政治实体的任何货币和任何其他形式的信用。Whale Coin 的使用不仅局限于 Whale Live 平台，而且还跨越了其他Whale Live 服务。除特别注明外，Whale Coin 不得与本公司其他优惠、优惠券、折扣及特别优惠一并使用，除非本公司另有指定。 </li></br>
                        </ol>
                    </li>
                    <li><b>退款条例</b>
                        </br></br>
                        除另有指明外，所 Whale Coin 的销售均为最终销售。所有购买的Whale Coin，Whale Live不提供任何退款。Whale Coin及使用 Whale Coin购买的礼品不得兑换、兑换或退回现金。 </br>
                        </br>
                    </li>
                    <li><b>其他规定</b>
                        <ol></br>
                            <li style="text-align: justify">如果本协议的任何部分被认定为无效或不可执行，或因任何原因违反法律，受影响的条款应被视为删除，但本协议的其余条款仍然有效并具有约束力。 </li></br>
                            <li style="text-align: justify">Whale Live保留根据法律法规的任何变化以及根据Whale Live的经营、需要和策略单方面改变或调整本协议条款的权利，而无需事先通知。修改后的协议将在Whale Live的网站和/或相关平台上公布，您可以通过Whale Live的渠道查看最新的协议条款。为避免争议的解决产生疑问，应以修改后的协议为准。如果您不同意最新的使用条款条款，您有权停止使用Whale Live提供的服务。但是，您继续使用任何Whale Live服务将被视为您对Whale Live对本协议所做的所有修改的同意。 </li></br>
                            <li style="text-align: justify">在适用法律允许的范围内，Whale Live保留对本协议中任何条款以及对本协议的任何修改的最终解释权。</li></br>
                            <li style="text-align: justify">为免生疑问，任何协议、政策、规则、政策、指南等的英文版本应是Whale Live的主要语言，并应优先于任何其他辅助语言。 </li></br>
                        </ol>
                    </li>
                    </li>
                </ol>
            </section>
        </div>


    </main>
</s:if>
<s:else>
    <main>

        <section class="media-4">
            <div class="text-center" style="height:80px">
                <h2 style="padding-top:40px;">WHALE LIVE TERMS OF USE</h2>
            </div>
        </section>
        <div class="container py-1">
            <section class="live-2" style="padding-top:20px">
                </br>
                <p align="justify">This agreement is entered into in accordance to the law of Malaysia between the user of Whale Live Services (as hereinafter defined) ("<b>You</b>") and Whale Live ("<b>Whale Live</b>").</p>
                <p align="justify">Please carefully read and understand the terms and conditions of this Whale Live user terms of use ("Terms of Use"). You are not authorized to register, sign in or use the service of Whale Live unless if you accept the terms and conditions of this Terms of Use. Once you have registered, signed in, use Whale Live's Services, you shall be deemed to have accepted our terms as stipulated in this Agreement and to be bound by it. If you do not agree to the terms of the Agreement, please do not register, sign in or use the any of Whale Live Services. </p>

                <ol>
                    <li><b><b>Services Terms and Conditions</b></b>
                        <ol></br>
                            <li style="text-align: justify">Whale Live provides online broadcasting services, online game services and other online services not defined herein ("<b>Services</b>") in accordance to this Terms of Use. </li></br>
                            <li style="text-align: justify">You will receive a Whale Live account upon completion of your registration of an account with Whale Live. It is your responsibility for every activity and event performed or held in your account. Should there be any damage or loss in whatsoever manner caused by you while using the account, you shall be held personally responsible and you hereby agree to indemnify Whale Live for all damage or loss caused to Whale Live or to any third party.</li></br>
                            <li style="text-align: justify">You undertake to only log into Whale Live with your own registered credentials only. </li></br>
                            <li style="text-align: justify">You understand and accepts that Whale Live is only a service provider. You are fully responsible for the use of any related network services device e.g. personal computer/laptop, cell phone, and other systems or networks that access to internet/mobile internet services) and its required charges e.g. payment for phone bill and internet charges). </li></br>
                        </ol>
                    </li>
                    <li><b><b>Usage Rules</b></b>
                        <ol></br>
                            <li style="text-align: justify">You must provide sufficient personal information as required by Whale Live when applying for the use of Whale Live's Services and you are required to update from time to time if there are any changes in those information. You are entirely responsible to ensure that all of the information is true.</li></br>
                            <li style="text-align: justify">You shall not transfer your account, lend or transact in any form that is beyond your control to any other person. You shall notify Whale Live immediately of any unauthorized use of your account, or any other breach of security. Should you fail to keep your account secure and confidential, Whale Live shall not be liable for any loss or damage arising from your failure to comply with these obligations.</li></br>
                            <li style="text-align: justify">You may be held personally liable for losses under your own registered account, Whale Live shall not be liable for any loss or damage caused by you or third-party.</li></br>
                            <li style="text-align: justify">You agree to display ads provided by Whale Live or any of Whale Live's any other authorised third party or Whale Live's partners in connection with your use of the Services.</li></br>
                            <li style="text-align: justify">You agree to comply with the obligations below while using Whale Live's Services:
                                <ol></br>
                                    <li style="text-align: justify">Comply with this Terms of Use and any other relevant Whale Live's policy, rules and guidelines;</li></br>
                                    <li style="text-align: justify">Do not allow misuse or abuse Whale Live's Services;</li></br>
                                    <li style="text-align: justify">Do not make use of Whale Live's Services to perform any activities which would or may affect the operations of the internet or mobile internet;</li></br>
                                    <li style="text-align: justify">Do not make use of Whale Live's Services to upload, publish or spread any false, discriminating, threatening, vulgar obscene, or other unlawful materials and information;</li></br>
                                    <li style="text-align: justify">Do not infringe upon the rights, copyrights, trademark rights, reputation or other legal rights of Whale Live and any third party;</li></br>
                                    <li style="text-align: justify">Do not make use of Whale Live's Services to perform any actions which may result in any complaint/legal action made against Whale Live;</li></br>
                                    <li style="text-align: justify">You shall notify Whale Live immediately of any unauthorized use of your account/password, and any other breach of security.</li></br>
                                </ol>
                            </li>
                            <li style="text-align: justify">Should there be a breach of security occurs when you are using the Services, Whale Live or Whale Live's authorized person has the right to request you to rectify or immediately take action to reduce the impacts of any potential or existing illegal acts.</li></br>
                            </li>
                        </ol>
                    </li>
                    <li><b>Change, Discontinue or Termination</b>
                        <ol></br>
                            <li style="text-align: justify">You agree that Whale Live reserves the right, at its sole discretion, to change, discontinue or terminate part of or all of the Services without notifying you and in such event, Whale Live shall not be responsible to any damage done to you or any third party.</li></br>
                            <li style="text-align: justify">You agree that Whale Live shall provide maintenance to the Services or any related devices regularly or as and when necessary, and Whale Live shall not be held responsible for any interruption in the Services within a reasonable time, provided that advance notice is given to you. </li></br>
                            <li style="text-align: justify">Should any of the below situation arises, Whale Live has the absolute right to discontinue or stop you from using the Services and terminate this Terms of Use with you anytime without being liable to you or any third party:
                                <ol></br>
                                    <li style="text-align: justify">You provide false or inaccurate personal information;</li></br>
                                    <li style="text-align: justify">You have breached any law, regulations or has violated any of the terms in this Agreement.</li></br>
                                </ol>
                            </li></br>
                        </ol>
                    </li>

                    <li><b>Intellectual Property Rights</b>
                        <ol></br>
                            <li style="text-align: justify">The Services provided by Whale Live shall include but not limited to the following: text, photographs, graphics, audios and videos materials, all of which are subject to copyright, trademark or other law protection. Therefore, you shall not, without Whale Live's express written permission, use any of Whale Live's property for any business purpose of yours or any third party.</li></br>
                            <li style="text-align: justify">The rights of any software tools provided for by Whale Live including but not limited to any image, pictures, animation, video recordings, audio recordings, songs, text and attached orders and helpful materials belongs to Whale Live absolutely. Therefore, you are not allowed to reverse engineer, decompile or disassemble any of Whale Live's property without Whale Live's express written consent.</li></br>
                        </ol>
                    </li>

                    <li><b>Privacy Protection</b>
                        <ol></br>
                            <li>Whale Live undertakes to keep strict confidentiality and shall not disclose your registered information and any collected content on the Services with other individuals or organizations, except in the following situations below:
                                <ol></br>
                                    <li style="text-align: justify">Your prior explicit authorization has been obtained;</li></br>
                                    <li style="text-align: justify">The disclosure is in accordance or required by law; </li></br>
                                    <li style="text-align: justify">On the explicit request of any related government or legal enforcement departments; </li></br>
                                    <li style="text-align: justify">The disclosure is in the interests of the public; and</li></br>
                                    <li style="text-align: justify">To protect the legitimate rights and interests of Whale Live.</li></br>
                                </ol>
                            </li>
                            <li style="text-align: justify">From time to time, Whale Live may collaborate with third parties to provide you the Services. In this case, should the third party undertake to protect and secure your privacy consistent with this Terms of Use and Privacy Policy, you allow Whale Live to provide such registered information to the third party for that purpose.</li></br>
                            <li style="text-align: justify">Whale Live is permitted to analyze your entire user data and use it for Whale Live's business purposes. </li></br>
                        </ol>
                    </li>

                    <li><b>Disclaimers</b>
                        <ol></br>
                            <li style="text-align: justify">Whale Live do not guarantee that the Services may fulfill the demand of you and makes no guarantee that the services provided shall not be interrupted. You agree that there may be some discrepancy as to the timeliness, security and accuracy of the information displayed on the Services provided by Whale Live.</li></br>
                            <li style="text-align: justify">Whale Live do not guarantee the accuracy and completeness of any external links hosted by any third party that is beyond the control of Whale Live and Whale Live shall take no responsibility for any damages due to your reliance on the content therein. </li></br>
                            <li style="text-align: justify">Whale Live shall not be responsible for any loss to you or third party should there by any telecommunication system or internet network error, computer breakdown or virus, information corruption, computer system issue or any other related issues, but Whale Live shall strive to rectify any such issue soonest possible.</li></br>
                        </ol>
                    </li>
                    <li><b>Governing Law and Dispute Resolution</b>
                        </br></br>
                        In the event of any dispute with respect to any terms of this Agreement, you agree to first resolve the dispute through negotiation in good faith; failing which, either party may then submit the relevant dispute to the Kuala Lumpur Arbitration Committee and any judgment by the arbitrator shall be final and binding on all parties.  </br></br>

                    </li>
                    <li><b>Items for sale</b>
                        <ol></br>
                            <li style="text-align: justify">The commercial nature of Whale Live is online live broadcast service. </li></br>
                            <li style="text-align: justify">If you are eighteen (18) years of age (or legal age in your country/region), you can purchase Whale Live's virtual currency known as Whale Coin through recharging your account. You can purchase virtual gifts with Whale Coin and the receiver user shall receive virtual galaxy coin known as Baby Whale Point. </li></br>
                            <li style="text-align: justify">The price of Whale Coin is displayed at the point of purchase and may fluctuate from time to time. All fees and payments of Whale Coin shall be transacted in the currency specified at the time of purchase through the relevant payment gateway. Currency exchange settlements, foreign transaction costs and payment channel costs , if any are based on the agreement between you and the applicable payment gateway. </li></br>
                            <li style="text-align: justify">Whale Coin shall have no expiry dates. </li></br>
                            <li style="text-align: justify">You will be responsible for any expenses incurred during the purchase of Whale Coin. Upon completion of the purchase transaction, the purchased Whale Coin shall be credited into your account. You may purchase gifts with Whale Coin, however, Whale Coin cannot be exchanged for cash, legal tender, any currency in any states, regions or political entity and any other forms of credit. The use of Whale Coin is not confined only to the Whale Live broadcast platform but also across other Whale Live services. Unless otherwise specified, Whale Coin cannot be used in combination with our other offers, coupons, discounts and special promotions unless designated by us.</li></br>
                        </ol>
                    </li>
                    <li><b>Refund regulations</b>
                        </br></br>
                        The sale of all Whale Coinis final unless otherwise specified otherwise. Whale Live do not provide any refund for the purchase of all Whale Coin. Whale Coin and gifts purchased using Whale Coin cannot be exchanged, converted or returned into cash. </br>
                        </br>
                    </li>
                    <li><b>Other Provisions</b>
                        <ol></br>
                            <li style="text-align: justify">Should any part of this Agreement be held invalid or unenforceable, or violate the law for any reason, the affected provision shall be deemed to be removed, but the remaining provisions of this Agreement shall still be valid and binding.</li></br>
                            <li style="text-align: justify">Whale Live reserves the right, at its discretion, to unilaterally change or adjust the terms of this Agreement according to any changes in laws and regulations and in accordance to the Whale Live's operation, needs and strategy without prior notice. The modified agreement, if any, shall be announced at Whale Live's website and/or relevant platform and you may check the latest agreement provisions via Whale Live's such channels. For the avoidance of doubt in the resolution of a dispute, the modified agreement shall prevail. You have the right to stop using the Services provided by Whale Live should you disagree with the updated terms of the Terms of Use. However, the continuing use of any Whale Live services by you shall be deemed to be your consent to all of the modification of the Agreement made by Whale Live. </li></br>
                            <li style="text-align: justify">Whale Live reserves the right of final interpretation of any terms herein and to any modifications to this Agreement to the extent permitted by applicable law.</li></br>
                            <li style="text-align: justify">For the avoidance of doubt, the English version of any agreement, policy, rules, policies, guidelines and etc shall be the primary language of Whale Live and shall prevail over any other secondary language. </li></br>
                        </ol>
                    </li>
                    </li>
                </ol>
            </section>
        </div>


    </main>
</s:else>


<footer>
    <div class="container-fluid">
        ©2020 Whale Media Sdn Bhd 版权所有
    </div>
</footer>

<style>

    .container {
        padding-right: 50px;
        padding-left: 50px;
    }
    .media-4:after {
        transform: skewY(-1.70deg);
    }
    ol {
        list-style-type: none;
        counter-reset: item;
        margin: 0;
        padding: 0;
    }

    ol > li {
        display: table;
        counter-increment: item;
        margin-bottom: 0.6em;
    }

    ol > li:before {
        content: counters(item, ".") ". ";
        display: table-cell;
        padding-right: 0.6em;
    }

    li ol > li {
        margin: 0;
    }

    li ol > li:before {
        content: counters(item, ".") " ";
    }

</style>

<!-- #modal-dialog -->

