<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>
<script src="<c:url value="/asset/whale_media/asset/js/common-jquery-struts2.js"/>" type="text/javascript" ></script>




<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_casting.php">网络艺人招募</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_recharge.php">充值</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_download.php">应用程序</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_login.php">登入</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>

            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1" style="margin-top: -10%">
        <div class="container-fluid body py-6">
            <div class="main container card-header" style="background-color:#400B75 ;color:#ffffff">
                <h2 class="text-left">网络艺人招募</h2>
            </div>
            <div class="main container card-header">
                <s:form action="addCasting" name="castingForm" id="castingForm" cssClass="form-horizontal">
                    <fieldset>
                        <div id="legend">
                            <legend class="" style="margin-top:2%;margin-bottom:2%;font-size: 22px">个人资料</legend>
                        </div>
                            <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="whaleLiveId">鲸娱号</label>
                                <input type="number" class="form-control casting" id="whaleLiveId" name="whaleLiveId" placeholder="鲸娱号" >
                            </div>
                            <div class="col-sm-6">
                                <label for="identityNo">身份证号码</label>
                                <input type="number" class="form-control casting" id="identityNo" name="identityNo" placeholder="身份证号码">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="fullName">姓名</label>
                                <input type="text" class="form-control casting" id="fullName" name="fullName" placeholder="姓名">
                            </div>

                            <div class="col-sm-6">
                                <label for="phoneNo">联系号码</label>
                                <input type="number" class="form-control casting" id="phoneNo" name="phoneNo" placeholder="输入号码而已" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                    <label for="gender" >性别:</label>
                                        <select name="gender" id="gender" class="form-control casting">
                                            <option value="">请选择</option>
                                            <option value="M">男</option>
                                            <option value="F">女</option>
                                        </select>

                                </div>
                            <div class="col-sm-6">
                                <label for="age">年龄</label>
                                <input type="number" class="form-control casting" id="age" name="age">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="email">电邮地址</label>
                                <input type="text" class="form-control casting" id="email" name="email" placeholder="Example@hotmail.com">
                            </div>
                            <div class="col-sm-6">
                                <label for="igId">Instagram</label>
                                <input type="text" class="form-control casting" id="igId" name="igId" >
                            </div>

                        </div>

                        <div class="form-group row">
                            <div class="col-sm-6">
                                <label for="tiktokId">抖音号</label>
                                <input type="text" class="form-control casting" id="tiktokId" name="tiktokId">
                            </div>
                            <div class="col-sm-6">
                                <label for="fbId">Facebook</label>
                                <input type="text" class="form-control casting" id="fbId" name="fbId" >
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="url">云端硬盘链接</label>
                                <input type="text" class="form-control casting" id="url" name="url" placeholder="www.xxx.com" >
                            </div>

                        </div>
                        <div class="form-group row">
                            <div class="col-sm-12">
                                <label for="talent">才艺</label>
                                <input type="text" class="form-control casting" id="talent" name="talent">
                            </div>

                        </div>
                        <div class="control-group">
                            <!-- Button -->
                                <button id="btnSubmit" type="submit" class="btn btn-success btn-block" style="border-radius: 30px;margin-top: 3%">Register</button>
                        </div>
                    </fieldset>
                </s:form>
            </div>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2">
                <h5>菜单</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_recharge.php">充值</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 mt-2">
                <h5>社交媒体</h5>
                <ul class="nav">
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 mt-2">
                <h5>联络我们</h5>
                <table>
                    <tbody>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-phone mr-1"></i></td>
                        <td style="color:white">+6014-711 2439</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-envelope mr-1"></i></td>
                        <td style="color:white">corporate@whalemediamy.com</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fab fa-weixin mr-1"></i></td>
                        <td style="color:white">whalelive01</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-map-marker-alt mr-1"></i></td>
                        <td style="color:white">(Level 8) 159, Jalan Temple, Pjs 8, 46050, Petaling jaya, Selangor.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    var delayTime;

/*        $('#btnSubmit').click(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "<s:url action="wl_casting"/>",
                cache: false,
                data: {
                    whaleliveId: $('#whaleLiveId').val(),
                    fullName: $('#fullName').val(),
                },
                success: function (data, status, xhr) {
                    if (data == null || data == "" || data.actionErrors == "") {
                        window.location.reload();
                    } else {

                    }
                }
            });
        });*/

    $("#castingForm").validate({
        submitHandler: function (form) {
            $(form).ajaxSubmit({
                dataType: 'json',
                success: defaultJsonCallback
            });
        },rules : {
            "whaleLiveId" : "required",
            "email" : {
                required : true,
                email: true,
                minlength : 5
             },
            "fullName" : {
                required : true
            },
            "identityNo" : {
                required : true,
                minlength : 10
            },
            "talent" : {
                required : true,
            },
            "age" : {
                required : true,
                maxlength : 3
            },
            "phoneNo" : {
                required : true,
                minlength : 10
            },
            "gender" : {
                required : true,
            }
        }
    });

    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    function defaultJsonCallback(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                window.location = "<s:url action="wl_casting_success"/>"
            },
            onFailure: function (json, error) {
                alert(error);
            }
        });
    }
    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });

</script>

<style>

    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }


    .recharge-1 {
        padding-top: 18.3rem;
        background-color: #400b75;
        color: #0b0b0b;
    }

    .main.container { min-height: calc(10% - 1.1rem); margin-bottom: 0; margin-top: 0.1rem; padding-bottom: 0.2rem;background-color: #FFFFFF }


    .recharge-1 .body .form-control {
        background-color: #ffffff;
        border: solid 1px #0b0b0b;
        border-radius: 0rem;
        padding: 0rem;
        text-align: center;
        color:#0b0b0b;
    }

    form-horizontal .control-group{margin-bottom:20px;*zoom:1;}.form-horizontal .control-group:before,.form-horizontal .control-group:after{display:table;content:"";line-height:0;}
    .form-horizontal .control-group:after{clear:both;}
    .form-horizontal .control-label{float:left;width:160px;padding-top:5px;text-align:right;}
    .form-horizontal .controls{*display:inline-block;*padding-left:20px;margin-left:180px;*margin-left:0;}.form-horizontal .controls:first-child{*padding-left:180px;}

    .recharge-1 .body .casting{
        border-radius: 70px;
        border-color:#9945ee;
        border-width: 2px;
        height: 40px;
    }

</style>


