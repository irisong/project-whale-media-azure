<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script src="<c:url value="/asset/whale_media/asset/js/app.js"/>"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>"
        type="text/javascript"></script>
<script src="<c:url value="/smartadmin/js/plugin/jquery-form/jquery-form.min.js"/>" type="text/javascript"></script>

<head>
    <title>Whale Media</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css2?family=ZCOOL+QingKe+HuangYou&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<c:url value="/asset/whale_media/asset/css/app.css"/>"/>
</head>

<header>
    <nav class="navbar navbar-expand-md fixed-top">
        <a href="wl_index.php" class="navbar-brand"><img class="logo-sm"
                                                         src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                                                         alt="Whale Media"></a>

        <div class="navbar-toggler hamburger hamburger--squeeze" data-toggle="collapse"
             data-target="#collapsibleNavbar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
        </div>

        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav align-items-center ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_casting.php">网络艺人招募</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_recharge.php">充值</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="wl_download.php">应用程序</a>
                </li>
                <li class="nav-item">
                    <a class="btn btn-secondary" href="wl_login.php">登入</a>
                </li>

                <li class="nav-item">
                    <form class="nav-link form-inline">
                        <select id="langSelectionBox" class="form-control" onchange="initLang();">
                            <option value="zh">中文</option>
                            <option value="en">English</option>
                        </select>
                    </form>
                </li>

            </ul>
        </div>
    </nav>
</header>

<main>
    <section class="recharge-1">
        <div class="container-fluid body py-3">
            <span class="circle-bg"></span>
            <span class="circle-sm"></span>
            <s:form id="topUpForm" name="topUpForm">

                    <div class="col-md-6 mx-auto">
                        <div class="form-group">
                            <input name="successeng" id="successeng" type="text" class="form-control"
                                   placeholder="Payment Successful" disabled>
                        </div>
                        <div class="form-group">
                            <input name="successchn" id="successchn" type="text" class="form-control"
                                   placeholder="充值成功" disabled>
                        </div>

                            <a class="btn btn-secondary" style="margin-left: 45%;" href="wl_recharge.php">Back</a>
                    </div>


            </s:form>
        </div>
    </section>
</main>

<footer>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2 d-flex align-items-center">
                <img class="logo"
                     src="<c:url value="/asset/whale_media/asset/logo2.png"/>"
                     alt="Whale Media">
            </div>
            <div class="col-md-2 mt-2">
                <h5>菜单</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a class="nav-link" href="wl_index.php">鲸娱媒体</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_live.php">鲸娱直播</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="wl_recharge.php">充值</a>
                    </li>
                </ul>
            </div>
            <div class="col-md-2 mt-2">
                <h5>社交媒体</h5>
                <ul class="nav">
                    <li class="nav-item mr-2">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-facebook-square fa-2x"></i></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#" target="_blank"><i class="fab fa-instagram-square fa-2x"></i></a>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 mt-2">
                <h5>联络我们</h5>
                <table>
                    <tbody>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-phone mr-1"></i></td>
                        <td style="color:white">+6014-711 2439</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-envelope mr-1"></i></td>
                        <td style="color:white">corporate@whalemediamy.com</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fab fa-weixin mr-1"></i></td>
                        <td style="color:white">whalelive01</td>
                    </tr>
                    <tr>
                        <td style="color:white" class="align-top"><i class="fas fa-map-marker-alt mr-1"></i></td>
                        <td style="color:white">(Level 8) 159, Jalan Temple, Pjs 8, 46050, Petaling jaya, Selangor.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    var delayTime;

    $(function () {

        $('#btnBack').click(function () {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                url: "<s:url action="wl_live"/>",
                cache: false,
                data: {
                    phoneNo: $('#phoneNo').val(),
                    password: $('#password').val()
                },
                success: function (data, status, xhr) {

                }
            });
        });
    });



    function initLang() {
        var langSelectionBox = document.getElementById("langSelectionBox");
        var langCode = langSelectionBox.options[langSelectionBox.selectedIndex].value;
        $.ajax({
            type: 'POST',
            url: "<s:url action="initLangWhaleLivePage"/>",
            dataType: 'json',
            cache: false,
            data: {
                language: langCode
            },
            success: function (data, status, xhr) {
                window.location.reload();
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
            }
        });
    }

    $(window).on('scroll', function () {
        if ($('.navbar-collapse').hasClass('show')) {
            $('.navbar').addClass('bg-primary');
        } else {
            if ($(this).scrollTop() > 50) {
                $('.navbar').addClass('bg-primary');
            } else {
                $('.navbar').removeClass('bg-primary');
            }
        }
    });

    $('.navbar-collapse').on('show.bs.collapse', function () {
        $('.navbar').addClass('bg-primary');

    }).on('hide.bs.collapse', function () {
        if ($(window).scrollTop() <= 50) {
            $('.navbar').removeClass('bg-primary');
        }
    });

    $('.hamburger').click(function () {
        $(this).toggleClass('is-active');
    });
</script>

<style>
    body {
        font-family: Arial, Helvetica, sans-serif;
    }

    /* The Modal (background) */
    .modal {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1; /* Sit on top */
        padding-top: 120px; /* Location of the box */
        padding-left: 10%; /* Location of the box */
        padding-right: 10%; /* Location of the box */
        left: 0;
        top: 0;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0, 0, 0); /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4); /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        background-color: #fefefe;
        margin: auto;
        padding: 20px;
        border: 1px solid #888;
        width: 100%;
    }

    /* The Close Button */
    .close {
        color: #aaaaaa;
        float: right;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: #000;
        text-decoration: none;
        cursor: pointer;
    }
</style>

<!-- #modal-dialog -->




