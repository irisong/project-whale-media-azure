<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.exchangeRateList"/></h1>
</div>

<script type="text/javascript">
    $(function(){
        $("#btnCreate").click(function(){
            $("#exchangeRateForm").resetForm();
            $("#exchangeRateForm").form("clear"); // clear numberbox

            $("#currencyExchange\\.currencyCodeFrom").removeAttr("readonly", "readonly");
            $("#currencyExchange\\.currencyCodeTo").removeAttr("readonly", "readonly");
            $("#exchangeRateModal").dialog("setTitle", "<s:text name="createExchangeRate" />");
            $("#exchangeRateModal").dialog("open");
        });

        $("#exchangeRateForm").validate( {
            submitHandler: function(form){
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("#exchangeRateModal").dialog("close");
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });

        $(".editLink").click(function(event){
            // stop event
            event.preventDefault();

            var id = $(this).parents("tr").attr("rowid");
            var currencyCodes = id.split("-");

            $("body").loadmask("<s:text name="processing.msg"/>");

            $.post('<s:url action="exchangeRateGetLatest"/>',{
                "currencyExchange.currencyCodeFrom": currencyCodes[0],
                "currencyExchange.currencyCodeTo": currencyCodes[1]
            }, function(json){
                $("body").unmask();

                new JsonStat(json, {
                    onSuccess : function(json) {
                        $("#exchangeRateForm").resetForm();
                        $("#exchangeRateForm").form("clear"); // clear numberbox
                        $("#exchangeRateModal").dialog("setTitle", "<s:text name="editExchangeRate" />");

                        $("#currencyExchange\\.currencyCodeFrom").attr("readonly", "readonly").val(json.currencyExchange.currencyCodeFrom);
                        $("#currencyExchange\\.currencyCodeTo").attr("readonly", "readonly").val(json.currencyExchange.currencyCodeTo);
                        $("#currencyExchange\\.rate").numberbox("setValue", json.currencyExchange.rate);
                        $("#exchangeRateModal").dialog("open");
                    },
                    onFailure : function(json, error) {
                        messageBox.alert(error);
                    }
                });
            });
        });

        $(".deleteLink").click(function(event){
            // stop event
            event.preventDefault();

            var id = $(this).parents("tr").attr("rowid");
            var currencyCodes = id.split("-");

            messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="exchangeRateRemove"/>',{
                    "currencyExchange.currencyCodeFrom": currencyCodes[0],
                    "currencyExchange.currencyCodeTo": currencyCodes[1]
                }, function(json){
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess : function(json) {
                            messageBox.info(json.successMessage, function(){
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

    }); // end $(function())

    function processJsonSave(json){
        $("body").unmask();

        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location.reload();
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                messageBox.alert(error, function(){
                    $("#exchangeRateModal").dialog("open");
                });
            }
        });
    }
</script>

<s:form name="runTaskForm" id="runTaskForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <ce:buttonRow>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="icon-plus-sign"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>

<div class="row-fluid">
    <div class="span6">
        <table class="table table-striped table-bordered table-condensed">
            <thead>
                <tr>
                    <th class="text-center" colspan="2" style="width: 40%"><s:text name="currency"/></th>
                    <th class="text-center" rowspan="2" style="width: 20%"><s:text name="rate"/></th>
                    <th class="text-center" rowspan="2" style="width: 25%"><s:text name="lastUpdate"/></th>
                    <th class="text-center" rowspan="2" style="width: 15%">&nbsp;</th>
                </tr>
                <tr><th class="text-center"><s:text name="from"/></th><th class="text-center"><s:text name="to"/></th></tr>
            </thead>
            <tbody>
                <s:iterator var="currencyExchange" value="currencyExchanges">
                    <tr rowid="<s:property value="%{#currencyExchange.currencyCodeFrom + '-' + #currencyExchange.currencyCodeTo}" />">
                        <td><s:property value="#currencyExchange.currencyCodeFrom" /></td>
                        <td><s:property value="#currencyExchange.currencyCodeTo" /></td>
                        <td class="text-right"><s:property value="%{@SF@formatCosting(#currencyExchange.rate)}" /></td>
                        <td><s:date name="#currencyExchange.datetimeAdd" format="%{getText('default.server.datetime.12.format')}"/></td>
                        <td>
                            <a class="editLink blue" title="<s:text name="btnEdit"/>" href="#">
                                <i class="icon-pencil"></i>
                            </a>
                            <a class="deleteLink green" title="<s:text name="btnDelete"/>" href="#">
                                <i class="icon-trash"></i>
                            </a>
                        </td>
                    </tr>
                </s:iterator>
            </tbody>
        </table>
    </div>
</div>
</s:form>

<div id="exchangeRateModal" class="easyui-dialog" style="width:500px; height:300px" title="<s:text name="createExchangeRate"/>" closed="true">
    <div class="space-6"></div>
    <s:form action="exchangeRateSaveUpdate" name="exchangeRateForm" id="exchangeRateForm" cssClass="form-horizontal">
        <s:textfield name="currencyExchange.currencyCodeFrom" id="currencyExchange.currencyCodeFrom" label="%{getText('currencyFrom')}" required="true" size="20" maxlength="10"/>
        <s:textfield name="currencyExchange.currencyCodeTo" id="currencyExchange.currencyCodeTo" label="%{getText('currencyTo')}" required="true" size="20" maxlength="10"/>
        <s:textfield name="currencyExchange.rate" id="currencyExchange.rate" label="%{getText('exchangeRate')}" required="true" size="50" maxlength="10" cssClass="easyui-numberbox" data-options="min:0,precision:4"/>

        <ce:buttonRow>
            <button id="btnSave" type="submit" class="btn btn-primary">
                <i class="icon-save"></i>
                <s:text name="btnSave"/>
            </button>
        </ce:buttonRow>
    </s:form>
</div>