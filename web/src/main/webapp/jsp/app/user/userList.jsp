<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.userList" />

<script type="text/javascript">

$(function(){
	$("#userForm").validate({
		submitHandler: function(form) {
            $('#dg').datagrid('load',{
                username: $('#user\\.username').val(),
                fullname: $('#user\\.fullname').val()
            });
		}
	});

	$("#dg").datagrid({
        onResizeColumn:function(field, width){
            var col = $(this).datagrid('getColumnOption', field);
            col.width = '20%';  // reset the width. Total 5 columns
            $(this).datagrid('resize');
        }
    });

    $("#btnEdit").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        $("#user\\.userId").val(row.userId);
        $("#navForm").attr("action", "<s:url action="userEdit" />")
        $("#navForm").submit();
    });

    $("#btnCreate").click(function(event){
        $("#navForm").attr("action", "<s:url action="userAdd" />")
        $("#navForm").submit();
    });

    $("#btnResetPassword").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        $("#user\\.userId").val(row.userId);
        $("#navForm").attr("action", "<s:url action="resetPassword" />")
        $("#navForm").submit();
    });

}); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="user.userId" id="user.userId" />
</form>

<s:form name="userForm" id="userForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:textfield key="user.username" id="user.username" />
        <s:textfield key="user.fullname" id="user.fullname" />
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
        <button id="btnEdit" type="button" class="btn btn-warning">
            <i class="fa fa-pencil-square-o"></i>
            <s:text name="btnEdit"/>
        </button>
        <!--
        <button id="btnDelete" type="button" class="btn btn-danger">
            <i class="icon-trash"></i>
            <s:text name="btnDelete"/>
        </button>
        -->
        <button id="btnResetPassword" type="button" class="btn btn-pink">
            <i class="fa fa-key"></i>
            <s:text name="reset.password"/>
        </button>

    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" style="width:100%;height:250px"
           url="<s:url action="userListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="username">
        <thead>
        <tr>
            <th field="username" sortable="true"><s:text name="user.username"/></th>
            <th field="fullname" sortable="true"><s:text name="user.fullname"/></th>
            <th field="email" sortable="true"><s:text name="user.email"/></th>
            <th field="status"  sortable="true" formatter="$.datagridUtil.formatStatus"><s:text name="status"/></th>
            <th field="datetimeAdd" sortable="true"><s:text name="datetime.add"/></th>
        </tr>
        </thead>
    </table>

</s:form>