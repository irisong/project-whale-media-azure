<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="postNewEnquiry"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#enquiryForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            } // submitHandler
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="enquiryList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="enquirySave" name="enquiryForm" id="enquiryForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:textfield name="helpDesk.subject" id="helpDesk.subject" label="%{getText('subject')}" cssClass="input-xxlarge" required="true"/>
    <s:select name="helpDesk.ticketTypeId" id="helpDesk.ticketTypeId" label="%{getText('category')}" list="helpDeskTypes" listKey="key" listValue="value"/>
    <s:textarea name="helpDesk.message" id="helpDesk.message" label="%{getText('message')}" cssClass="input-xxlarge" required="true" rows="20"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="enquiryList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
