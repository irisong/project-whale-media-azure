<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header">
    <h2><s:text name="userProfile"/></h2>
</div>

<script type="text/javascript">
    $(function() {
        $("#userForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "user.fullname":{
                    required : true,
                    minlength : 5
                },
                "user.email":{
                    required: true,
                    email: true
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location.reload();
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="profileUpdate" name="userForm" id="userForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:textfield key="user.username" id="user.username" readonly="true"/>
    <s:textfield key="user.fullname" id="user.fullname" required="true" size="50" maxlength="100" cssClass="inputText"/>

    <s:textfield name="user.email" id="user.email" label="%{getText('email')}" required="true" size="50" maxlength="50" cssClass="inputText"/>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="app" namespace="/app"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>