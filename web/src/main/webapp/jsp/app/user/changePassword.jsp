<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
var jform = null;
$(function(){
	jform = $("#changePassword").validate({
		rules : {
			oldPassword : "required",
			newPassword : {
				required : true
			},
			confirmPassword : {
				required : true,
				equalTo: "#newPassword"
			}
		}
	});
});
</script>

<sc:title title="title.changePassword" />

<s:form action="changePasswordUpdate" name="changePassword" id="changePassword" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:textfield key="user.username" id="user.username" readonly="true"/>
    <s:password key="oldPassword" id="oldPassword"/>
    <s:password key="newPassword" id="newPassword"/>
    <s:password key="confirmPassword" id="confirmPassword"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="change.password" id="change.password" key="change.password" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-hdd-o"></i>
            <s:text name="change.password"/>
        </s:submit>
    </ce:buttonRow>
</table>
</s:form>
