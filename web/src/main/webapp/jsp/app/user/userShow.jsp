<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
var urlExit = "<s:url action="user_list_list"/>";
var urlUserFormUserRoles = "<s:url action="userFormUserRoles"/>";

var datagrid = null;

$(function() {
    datagrid = $("#datagrid").compalDataTable( {
        // compalDataTable extra params
        "idTr" : true, // assign <tr id='xxx'> from 1st columns
        // array(aoColumns);
        //"reassignEvent" : function() { // extra function for reassignEvent when
        // JSON is back from server
        //	reassignDatagridEventAttr();
        //},

        "bLengthChange" : false,
        "bFilter" : false,
        "bProcessing" : true,
        "bServerSide" : true,
        "sAjaxSource" : urlUserFormUserRoles,
        "bPaginate" : false,
        "bInfo" : false,
        "oLanguage" : {
            "sZeroRecords" : 'No records found' // not showing any messages if
            // datagrid is empty
        },
        "aoColumns" : [ {
            "sName" : "roleId",
            "bVisible" : false
        }, {
            "sName" : "roleName"
        }, {
            "sName" : "roleDesc"
        }]
    });

    // register exit button
    $("#btnExit").compalExit(urlExit);
}); // end $(function())
</script>

<s:form action="userShow" name="userForm" id="userForm">
	<table width="90%" border="0">
		<tr>
			<td>
			<h3><s:text name="title.user_view"/></h3>
			</td>
		</tr>
		<tr>
			<td>
			<table width="100%">
				<tr>
					<td><sc:displayErrorMessage align="center" /></td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0">
						<s:textfield key="user.username" id="user.username" label="Username" readonly="true" cssClass="inputTextDisable" size="20"/>
						<s:textfield key="user.fullname" id="user.fullname" label="Full Name" readonly="true" cssClass="inputTextDisable" size="50"/>
						<s:textfield key="user.staffCode" id="user.staffCode" label="Staff Code" readonly="true" cssClass="inputTextDisable" size="20"/>
						
						<s:select list="rootUserLocs" listKey="locId" listValue="locName" key="rootUserLocs" cssClass="inputTextDisable" name="user.rootLocId" id="user.rootLocId" disabled="true"/>
						<s:textfield key="userLoc" name="locName" id="locName" cssClass="inputTextDisable" readonly="true"/>
						 
						<tr>
    						<td class="tdLabel"><label for="deptName" class="label"><s:text name="dept"/></label></td>
 							<td>:</td>
 							<td>
 								<s:textfield name="deptName" id="deptName" label="Department" theme="simple" size="30" readonly="true" cssClass="inputTextDisable" />
 								<s:hidden id="user.deptId" name="user.deptId" />
 							</td>
							<td><s:fielderror fieldName="deptName"/></td>
						</tr>
						<s:select key="job" name="jobId" id="jobId" list="jobs" listKey="jobId" listValue="jobName" cssClass="inputTextDisable" disabled="true"/>
						<s:select key="grade" name="gradeCode" id="gradeCode" list="gradeCodes" listKey="key" listValue="value" cssClass="inputTextDisable" disabled="true"/>
						<s:select key="desg" name="user.desgId" id="user.desgId" list="desgs" listKey="desgId" listValue="desgName" required="true" cssClass="inputTextDisable" disabled="true"/>
						
						<s:textfield name="user.email" id="user.email" label="Email" readonly="true" size="50" maxlength="50" cssClass="inputTextDisable" />
						<s:textfield name="user.datetimeAdd" id="user.datetimeAdd" label="Created Date Time"  readonly="true" size="20" cssClass="inputTextDisable" />
						<s:textfield name="user.UserAdd.username" id="user.addBy" label="Created By"  readonly="true" cssClass="inputTextDisable" />
						<s:textfield name="user.datetimeUpdate" id="user.datetimeUpdate" label="Update Date Time"  readonly="true" cssClass="inputTextDisable" />
						<s:textfield name="user.UserUpdate.username" id="user.UserUpdate.username" label="Update By"  readonly="true" cssClass="inputTextDisable" />
					</table>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset><legend>User Role</legend>
						<table class="display" id="datagrid" border="0" width="100%">
						<thead>
							<tr>
								<th>Role ID[hidden]</th>
								<th>Role</th>
								<th>Description</th>
							</tr>
						</thead>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0">
						<ce:buttonRow>
							<input type="button" name="btnExit" id="btnExit" value="<s:text name="btnExit"/>"/>
						</ce:buttonRow>
					</table>
					</td>
				</tr>
			</table>
			</td>
	</table>
</s:form>