<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.userEdit" />

<script type="text/javascript">
    function forceSelect(event, ui){
        // force select
        if(!ui.item){
            $("#roleName").val("");
            $("#roleId").val("");
        }else{
            $("#roleId").val(ui.item.key);
        }
    }

    $(function() {
        $("#userForm").validate( {
            messages : {
                "user.username" : {
                    remote : jQuery.format("The username is already in use")
                }
            },
            submitHandler : function(form) {
                if($('#dg').datagrid('getData').total == 0){
                    $.messager.alert("", "Must select at least 1 user role");
                    return false;
                }
                form.submit();
            }, // submitHandler
            rules: {
                "user.fullname":{
                    required : true,
                    minlength : 5
                },
                "user.email":{
                    required: true,
                    email: true
                },
                "user.password":{
                    required: true,
                    minlength: 5
                }
            }
        });

        $("#dg").datagrid({
            onResizeColumn:function(field, width){
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '50%';  // reset the width. Total 2 columns
                $(this).datagrid('resize');
            }
        });

        $("#btnAddUserRole").click(function(event) {
            if($("#roleId").val()==""){
                $.messager.alert('',"No User Role selected");
            }else{
                $.post('<s:url action="userAddDetail" />', {
                    "roleId": $("#roleId").val()
                }, function(json){
                    new JsonStat(json, {
                        onSuccess : function(json) {
                            $("#roleName").val('').focus();
                            $("#roleId").val('');

                            // reload the grid
                            $('#dg').datagrid('reload');
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);

                            $("#roleName").select().focus();
                        }
                    });
                });
            }
        });

        $("#btnDelete").click(function(event){
            var row = $('#dg').datagrid('getSelected');
            if (row){
                $.post('<s:url action="userRemoveDetail" />', {
                    "roleId": row.roleId
                }, function(json){
                    new JsonStat(json, {
                        onSuccess : function(json) {
                            // reload the grid
                            $('#dg').datagrid('reload');
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                        }
                    });
                });
            }
        });

        $("#roleName").autocomplete({
            source: "<s:url action="userRoleAutoComplete"/>",
            minLength:1,
            change: forceSelect,
            select: forceSelect
        });
    });
</script>

<s:form action="userUpdate" name="userForm" id="userForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:hidden name="user.userId" id="user.userId" />
    <s:textfield key="user.username" id="user.username" readonly="true"/>
    <s:textfield key="user.fullname" id="user.fullname" required="true" size="50" maxlength="100" cssClass="inputText"/>

    <s:textfield name="user.email" id="user.email" label="%{getText('email')}" required="true" size="50" maxlength="50" cssClass="inputText"/>

    <h3><s:text name="userRole"/></h3>
    <hr/>
    <div class="form-group">
        <label for="roleName" class="col-sm-3 control-label"><s:text name="userRole"/>:</label>
        <div class="col-sm-9">
            <s:textfield name="roleName" id="roleName" theme="simple" size="30"/>
            <s:hidden id="roleId" name="roleId" />
            <button type="button" name="btnAddUserRole" id="btnAddUserRole" class="btn btn-primary">
                <i class="fa fa-plus-circle"></i>
                <s:text name="btnAdd"/>
            </button>
            <button type="button" name="btnSearchUserRole" id="btnSearchUserRole" class="btn btn-success">
                <i class="fa fa-search"></i>
                <s:text name="btnSearch"/>
            </button>
            <button id="btnDelete" type="button" class="btn btn-danger">
                <i class="fa fa-trash-o"></i>
                <s:text name="btnDelete"/>
            </button>
            <s:fielderror fieldName="roleName"/>
        </div>
    </div>

    <table id="dg" class="easyui-datagrid" style="width:100%;height:250px"
           url="<s:url action="userFormUserRoles"/>" fitColumns="true"
           rownumbers="true">
        <thead>
        <tr>
            <th field="roleName"><s:text name="userRole"/></th>
            <th field="roleDesc"><s:text name="userRole.roleDescription"/></th>
        </tr>
        </thead>
    </table>

    <br/>
    <ce:buttonRow>
        <ce:formExtra versioning="true" token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-hdd-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="userList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
<sc:userRoleLookup />