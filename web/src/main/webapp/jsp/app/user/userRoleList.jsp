<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.userRoleList" />

<script type="text/javascript">

$(function(){
	$("#userRoleForm").validate({
		submitHandler: function(form) {
            $('#dg').datagrid('load',{
                roleName: $('#userRole\\.roleName').val(),
                roleDesc: $('#userRole\\.roleDesc').val()
            });
		}
	});

    $("#dg").datagrid({
        onResizeColumn:function(field, width){
            var col = $(this).datagrid('getColumnOption', field);
            col.width = '25%';  // reset the width. Total 5 columns
            $(this).datagrid('resize');
        }
    });

    $("#btnEdit").click(function(event){
        var row = $('#dg').datagrid('getSelected');

        if(!row){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        $("#userRole\\.roleId").val(row.roleId);
        $("#navForm").attr("action", "<s:url action="userRoleEdit" />")
        $("#navForm").submit();
    });

    $("#btnCreate").click(function(event){
        $("#navForm").attr("action", "<s:url action="userRoleAdd" />")
        $("#navForm").submit();
    });

}); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="userRole.roleId" id="userRole.roleId"/>
</form>

<s:form name="userRoleForm" id="userRoleForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <div id="searchPanel">
        <s:textfield key="userRole.roleName" id="userRole.roleName"/>
        <s:textfield key="userRole.roleDesc" id="userRole.roleDesc"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
        <button id="btnEdit" type="button" class="btn btn-warning">
            <i class="fa fa-pencil-square-o"></i>
            <s:text name="btnEdit"/>
        </button>
        <!--
        <button id="btnDelete" type="button" class="btn btn-danger">
        <i class="icon-trash"></i>
        <s:text name="btnDelete"/>
        </button>
        -->
    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" style="width:100%;height:250px"
           url="<s:url action="userRoleListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true">
        <thead>
        <tr>
            <th field="roleName"><s:text name="userRole.roleName"/></th>
            <th field="roleDesc"><s:text name="userRole.roleDesc"/></th>
            <th field="status" formatter="$.datagridUtil.formatStatus"><s:text name="status"/></th>
            <th field="datetimeAdd"><s:text name="datetime.add"/></th>
        </tr>
        </thead>
    </table>

</s:form>