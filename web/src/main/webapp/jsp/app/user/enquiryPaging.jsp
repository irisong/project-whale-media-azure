<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th class="text-center" style="width: 20%"><s:text name="date"/></th>
        <th class="text-center" style="width: 80%"><s:text name="subject"/></th>
    </tr>
    </thead>
    <tbody>
    <s:if test="helpDesks.size()==0">
        <tr><td colspan="2"><div class="alert alert-info"><s:text name="noRecordFound"/></div></td></tr>
    </s:if>
    <s:iterator var="helpDesk" value="helpDesks">
        <tr>
            <td>${helpDesk.datetimeAdd}</td>
            <td><a href="<s:url namespace="/app/user" action="enquiryShow"/>?helpDesk.ticketId=${helpDesk.ticketId}">${helpDesk.subject}</a></td>
        </tr>
    </s:iterator>
    </tbody>
</table>