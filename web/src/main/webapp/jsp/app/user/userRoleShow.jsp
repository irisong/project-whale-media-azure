<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<script type="text/javascript">
var jform = null;

$(function(){
	$("#btnExit").click(function (event){
		window.location = "<s:url action="userRoleList"/>";
	});
}); // end $(function())
</script>

<s:form action="userRoleUpdate" name="userRoleForm" id="userRoleForm">
	<table width="90%" border="0">
		<tr>
			<td>
			<h3><s:text name="title.userRoleShow"/></h3>
			</td>
		</tr>
		<tr>
			<td>
			<table width="100%">
				<tr>
					<td><sc:displayErrorMessage align="center" /></td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0">
						<s:hidden name="userRole.roleId" id="userRole.roleId"/>
						<s:textfield name="userRole.roleName" id="userRole.roleName" label="Role Name" required="true" readonly="true"/>
						<s:textarea name="userRole.roleDesc" id="userRole.roleName" label="Description" cols="50" rows="8" readonly="true"/>
					</table>
					</td>
				</tr>
<script type="text/javascript">
// separate the datatable for easy maintain
$(function(){
	$("#tabs").tabs({
		collapsible: true
	})
	.find(".ui-tabs-nav").sortable({axis:'x'}); // tabs sortable

	<s:iterator status="iterStatus" var="tempUserAccessCat" value="userAccessCats">
		$("#datagrid_${iterStatus.index}").compalDataTable({
			"bLengthChange": false,
			"bFilter": false,
			"bPaginate": false,
			"bSort": false,
			"bInfo": false,
			"bAutoWidth": false // it will not able to display proper in 2nd tab
		});
	
	</s:iterator>	// end loop for userAccessCats
}); // end $(function())

</script>
				<tr>
					<td>
					<div id="tabs">
						<ul>
							<s:iterator status="iterStatus" var="tempUserAccessCat" value="userAccessCats">
								<li><a href="#tabs_${iterStatus.index}">${tempUserAccessCat.catName}</a></li>
							</s:iterator>
						</ul>
						<s:iterator status="iterStatus" var="tempUserAccessCat" value="userAccessCats">
							<div id="tabs_${iterStatus.index}">
								<table class="display" id="datagrid_${iterStatus.index}" border="0" width="100%">
									<thead>
										<tr>
											<th width="200px">Name</th>
											<th width="80px">All</th>
											<th width="90px">Read</th>
											<th width="100px">Create</th>
											<th width="100px">Update</th>
											<th width="100px">Delete</th>
											<th width="100px">Admin</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator var="tempUserAccess" value="#tempUserAccessCat.userAccessList" status="iterDetStatus">
											<tr>
												<td>${tempUserAccess.accessName}<s:hidden name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].accessCode" disabled="true"/></td>
												<td><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].allMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_allMode" theme="simple" disabled="true"/></td>
												<td><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].readMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_readMode" theme="simple" disabled="true"/></td>
												<td><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].createMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_createMode" theme="simple" disabled="true"/></td>
												<td><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].updateMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_updateMode" theme="simple" disabled="true"/></td>
												<td><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].deleteMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_deleteMode" theme="simple" disabled="true"/></td>
												<td><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].adminMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_adminMode" theme="simple" disabled="true"/></td>
											</tr>
										</s:iterator>
									</tbody>
								</table>
							</div>
						</s:iterator>
					</div>
					</td>
				</tr>
				<tr>
					<td>
					<table width="100%" border="0">
						<tr>
    						<td colspan="3">
    							<div align="center">
    								<input type="button" name="btnExit" id="btnExit" value="Exit">
    							</div>
    						</td>
    					</tr>
					</table>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</s:form>