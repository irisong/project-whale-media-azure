<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
var jform = null;
$(function(){
	jform = $("#resetPassword").validate({
		rules : {
			newPassword : {
				required : true
			},
			confirmPassword : {
				required : true,
				equalTo: "#newPassword"
			}
		}
	});
});
</script>

<sc:title title="title.resetPassword" />

<s:form action="resetPasswordUpdate" name="resetPassword" id="resetPassword" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:textfield key="user.username" id="user.username" readonly="true"/>
    <s:password key="newPassword" id="newPassword"/>
    <s:password key="confirmPassword" id="confirmPassword"/>
    <s:hidden name="user.userId"/>
    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="change.password" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-hdd-o"></i>
            <s:text name="change.password"/>
        </s:submit>
        <s:url var="urlExit" action="userList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
