<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<script type="text/javascript">

$(function(){
	$("#userRoleForm").validate({
		rules : {
			"userRole.roleName" : {
				required : true,
				minlength : 3
			}
		},
		submitHandler: function(form) {
			//alert("Submit");
			form.submit();
		}
	});
}); // end $(function())
</script>

<sc:title title="title.userRoleEdit" />

<s:form action="userRoleUpdate" name="userRoleForm" id="userRoleForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:hidden name="userRole.roleId" id="userRole.roleId"/>
    <s:textfield key="userRole.roleName" id="userRole.roleName" required="true" />
    <s:textarea key="userRole.roleDesc" id="userRole.roleName" cols="50" rows="8" cssClass="input-xxlarge"/>

<script type="text/javascript">
// separate the datatable for easy maintain
$(function(){
	<s:iterator status="iterStatus" var="tempUserAccessCat" value="userAccessCats">
		// 'checkAllModes' header button
		$("#checkAllModes_${iterStatus.index}").click(function(event){
			var index = ${iterStatus.index};
			var check = $(this).prop("checked");
	
			// select all checkbox where id start with 'check' and end with 'Modes_index' AND id is not start with 'checkAllModes'
			var s = ":checkbox[id^=check][id$=Modes_" + index+"]:not([id^=checkAllModes])";
			$(s).each(function(){
				$(this).prop("checked", check);
			});
	
			checkAll_allModes(index, check);
			checkAll_createModes(index, check);
			checkAll_readModes(index, check);
			checkAll_updateModes(index, check);
			checkAll_deleteModes(index, check);
			checkAll_adminModes(index, check);
		});

		// 'checkCreateModes' header button
		$("#checkCreateModes_${iterStatus.index}").click(function(event){
			var index = ${iterStatus.index};
			var check = $(this).prop("checked");
	
			checkAll_createModes(index, check);

			if(!check){
				$("#checkAllModes_"+index).prop("checked", check);
				checkAll_allModes(index, check);
			}
		});

		// 'checkReadModes' header button
		$("#checkReadModes_${iterStatus.index}").click(function(event){
			var index = ${iterStatus.index};
			var check = $(this).prop("checked");
	
			checkAll_readModes(index, check);

			if(!check){
				$("#checkAllModes_"+index).prop("checked", check);
				checkAll_allModes(index, check);
			}
		});

		// 'checkUpdateModes' header button
		$("#checkUpdateModes_${iterStatus.index}").click(function(event){
			var index = ${iterStatus.index};
			var check = $(this).prop("checked");
	
			checkAll_updateModes(index, check);

			if(!check){
				$("#checkAllModes_"+index).prop("checked", check);
				checkAll_allModes(index, check);
			}
		});

		// 'checkDeleteModes' header button
		$("#checkDeleteModes_${iterStatus.index}").click(function(event){
			var index = ${iterStatus.index};
			var check = $(this).prop("checked");
	
			checkAll_deleteModes(index, check);

			if(!check){
				$("#checkAllModes_"+index).prop("checked", check);
				checkAll_allModes(index, check);
			}
		});

		// 'checkAdminModes' header button
		$("#checkAdminModes_${iterStatus.index}").click(function(event){
			var index = ${iterStatus.index};
			var check = $(this).prop("checked");
	
			checkAll_adminModes(index, check);

			if(!check){
				$("#checkAllModes_"+index).prop("checked", check);
				checkAll_allModes(index, check);
			}
		});

		<s:iterator var="tempUserAccess" value="#tempUserAccessCat.userAccessList" status="iterDetStatus">
			// tempUserAccess_x_x_allMode
			$("#tempUserAccess_${iterStatus.index}_${iterDetStatus.index}_allMode").click(function (event){
				var check = $(this).prop("checked");
				var index = ${iterStatus.index};
				var indexDet = ${iterDetStatus.index};

				var $tr = $(event.target).parent().parent();

				// select all checkbox within the <tr> and the checkbox id is not constain 'allMode'
				$(":checkbox:not([id*=allMode])", $tr).each(function(){
					$(this).prop('checked',check);
				});

				if(!check){
					$("#checkAllModes_" + index).prop("checked", check);
				}
			});
			
			// select all checkbox in row (tempUserAccess_x_x_xxxMode) except 'allMode'			
			$(":checkbox[id^=tempUserAccess_${iterStatus.index}_${iterDetStatus.index}_]:not([id$=allMode])").click(function (event){
				var check = $(this).prop("checked");
				var id = $(this).attr("id");
				var index = ${iterStatus.index};
				var indexDet = ${iterDetStatus.index};

				var $tr = $(event.target).parent().parent();
				
				checkRow_allModes($tr, check);

				if(!check){
					// uncheck the header button
					if(id.indexOf("readMode") > -1){
						$("#checkReadModes_" + index).prop("checked", check);
					}else if(id.indexOf("createMode") > -1){
						$("#checkCreateModes_" + index).prop("checked", check);
					}else if(id.indexOf("updateMode") > -1){
						$("#checkUpdateModes_" + index).prop("checked", check);
					}else if(id.indexOf("deleteMode") > -1){
						$("#checkDeleteModes_" + index).prop("checked", check);
					}else if(id.indexOf("adminMode") > -1){
						$("#checkAdminModes_" + index).prop("checked", check);
					}

					$("#checkAllModes_" + index).prop("checked", check);
				}
			});
		</s:iterator> // end loop for #tempUserAccessCat.userAccessList
	
	</s:iterator>	// end loop for userAccessCats
}); // end $(function())


function checkRow_allModes($tr, check){
	if(!check){
		// select all checkbox within the <tr> and the checkbox id is constain 'allMode'
		$(":checkbox[id*=allMode]", $tr).each(function(){
			var allModeCheck = $(this).prop('checked');
			if(allModeCheck){
				$(this).prop('checked',check);
			}
		});
	}else{
		// select all checkbox where check status equals to 'FALSE' and the checkbox id is not constain 'allMode'
		//var s = ":checkbox[checked=false]:not([id*=allMode])";
        var s = ":checkbox:not(:checked):not([id*=allMode])";
		if($(s, $tr).length == 0){
			$(":checkbox[id*=allMode]", $tr).each(function(){
				$(this).prop('checked',check);
			});
		}
	}
}

function checkAll_allModes(index, check){
	// select all checkbox where id start with 'tempUserAccess_index' and end with 'allMode'
	var s = ":checkbox[id^=tempUserAccess_"+index+"][id$=allMode]";
	var a = $(s);
	//alert(a.length);
	$(s).each(function(){
		$(this).prop('checked',check);
	});
}

function checkAll_createModes(index, check){
	// select all checkbox where id start with 'tempUserAccess_index' and end with 'createMode'
	var s = ":checkbox[id^=tempUserAccess_"+index+"][id$=createMode]";
	var a = $(s);
	//alert(a.length);
	$(s).each(function(){
		$(this).prop('checked',check);
	});
}

function checkAll_readModes(index, check){
	// select all checkbox where id start with 'tempUserAccess_index' and end with 'readMode'
	var s = ":checkbox[id^=tempUserAccess_"+index+"][id$=readMode]";
	var a = $(s);
	//alert(a.length);
	$(s).each(function(){
		$(this).prop('checked',check);
	});
}

function checkAll_updateModes(index, check){
	// select all checkbox where id start with 'tempUserAccess_index' and end with 'updateMode'
	var s = ":checkbox[id^=tempUserAccess_"+index+"][id$=updateMode]";
	var a = $(s);
	//alert(a.length);
	$(s).each(function(){
		$(this).prop('checked',check);
	});
}

function checkAll_deleteModes(index, check){
	// select all checkbox where id start with 'tempUserAccess_index' and end with 'deleteMode'
	var s = ":checkbox[id^=tempUserAccess_"+index+"][id$=deleteMode]";
	var a = $(s);
	//alert(a.length);
	$(s).each(function(){
		$(this).prop('checked',check);
	});
}

function checkAll_adminModes(index, check){
	// select all checkbox where id start with 'tempUserAccess_index' and end with 'adminMode'
	var s = ":checkbox[id^=tempUserAccess_"+index+"][id$=adminMode]";
	var a = $(s);
	//alert(a.length);
	$(s).each(function(){
		$(this).prop('checked',check);
	});
}
</script>
    <ul id="myTab" class="nav nav-tabs bordered">
        <s:iterator status="iterStatus" var="tempUserAccessCat" value="userAccessCats">
            <li  class="${iterStatus.index == 0 ? 'active' : ''}"><a href="#tabs_${iterStatus.index}" data-toggle="tab"><s:text name="%{#tempUserAccessCat.catName}"/></a></li>
        </s:iterator>
    </ul>

    <div id="myTabContent" class="tab-content">
        <s:iterator status="iterStatus" var="tempUserAccessCat" value="userAccessCats">
            <div class="tab-pane fade ${iterStatus.index == 0 ? ' in active ' : ''}" id="tabs_${iterStatus.index}">
                <div class="row-fluid">
                    <div class="span12">
                        <table class="table table-striped table-bordered table-hover" id="datagrid_${iterStatus.index}" border="0" width="100%">
                            <thead>
                            <tr>
                                <th width="200px"><s:text name="name"/></th>
                                <th width="80px" class="center">
                                    <s:checkbox name="checkAllModes[%{#iterStatus.index}]" id="checkAllModes_%{#iterStatus.index}" theme="simple" cssClass="ace"/>
                                    <span class="lbl"></span>
                                    <s:text name="all"/>
                                </th>
                                <th width="90px" class="center">
                                    <s:checkbox name="checkReadModes[%{#iterStatus.index}]" id="checkReadModes_%{#iterStatus.index}" theme="simple" cssClass="ace"/>
                                    <span class="lbl"></span>
                                    <s:text name="read"/>
                                </th>
                                <th width="100px" class="center">
                                    <s:checkbox name="checkCreateModes[%{#iterStatus.index}]" id="checkCreateModes_%{#iterStatus.index}" theme="simple" cssClass="ace"/>
                                    <span class="lbl"></span>
                                    <s:text name="create"/>
                                </th>
                                <th width="100px" class="center">
                                    <s:checkbox name="checkUpdateModes[%{#iterStatus.index}]" id="checkUpdateModes_%{#iterStatus.index}" theme="simple" cssClass="ace"/>
                                    <span class="lbl"></span>
                                    <s:text name="update"/>
                                </th>
                                <th width="100px" class="center">
                                    <s:checkbox name="checkDeleteModes[%{#iterStatus.index}]" id="checkDeleteModes_%{#iterStatus.index}" theme="simple" cssClass="ace"/>
                                    <span class="lbl"></span>
                                    <s:text name="delete"/>
                                </th>
                                <th width="100px" class="center">
                                    <s:checkbox name="checkAdminModes[%{#iterStatus.index}]" id="checkAdminModes_%{#iterStatus.index}" theme="simple" cssClass="ace"/>
                                    <span class="lbl"></span>
                                    <s:text name="admin"/>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <s:iterator var="tempUserAccess" value="#tempUserAccessCat.userAccessList" status="iterDetStatus">
                                <tr>
                                    <td><s:text name="%{#tempUserAccess.accessName}"/><s:hidden name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].accessCode" theme="simple"/></td>
                                    <td class="center"><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].allMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_allMode" theme="simple" cssClass="ace"/><span class="lbl"></span></td>
                                    <td class="center"><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].readMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_readMode" theme="simple" cssClass="ace"/><span class="lbl"></span></td>
                                    <td class="center"><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].createMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_createMode" theme="simple" cssClass="ace"/><span class="lbl"></span></td>
                                    <td class="center"><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].updateMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_updateMode" theme="simple" cssClass="ace"/><span class="lbl"></span></td>
                                    <td class="center"><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].deleteMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_deleteMode" theme="simple" cssClass="ace"/><span class="lbl"></span></td>
                                    <td class="center"><s:checkbox name="userAccessCats[%{#iterStatus.index}].userAccessList[%{#iterDetStatus.index}].adminMode" id="tempUserAccess_%{#iterStatus.index}_%{#iterDetStatus.index}_adminMode" theme="simple" cssClass="ace"/><span class="lbl"></span></td>
                                </tr>
                            </s:iterator>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </s:iterator>
    </div>

    <br/>
    <ce:buttonRow>
        <s:token/>
        <s:hidden name="version" id="version" />
        <s:submit key="btnSave" id="btnSave" theme="simple" cssClass="btn btn-primary" type="button">
            <i class="fa fa-hdd-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="userRoleList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>

</s:form>