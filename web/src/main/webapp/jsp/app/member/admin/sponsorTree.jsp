<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<!-- required js & css for treanjs (START)-->
<link rel="stylesheet" href="<c:url value="/asset/plugin/treantjs/Treant.css"/>">
<link rel="stylesheet" href="<c:url value="/asset/plugin/treantjs/treantjs-tree.css"/>">
<link rel="stylesheet" href="<c:url value="/asset/plugin/treantjs/perfect-scrollbar/perfect-scrollbar.css"/>">

<script src="<c:url value="/asset/plugin/treantjs/raphael.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/Treant.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/jquery.easing.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/jquery.mousewheel.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/perfect-scrollbar/perfect-scrollbar.js"/>"></script>
<!-- required js & css for treanjs (END) -->


<s:if test="memberExist">
<script type="text/javascript">
    var rootMemberId = "${member.memberId}";
    var noOfDirectDownlines = 0;
    var startIndex = 0;
    var size = 5;
    var currentMemberId = rootMemberId;

    // implement stack (FILO) for member id and startIndex.
    var memberIds = [];
    var startIndices = [];

    function loadTree(){
        $("body").loadmask("<s:text name="processing.msg"/>");

        $.post("<s:url action="sponsorTreeTreantData" namespace="/app/member"/>", {
            rootMemberId: rootMemberId,
            memberId : currentMemberId,
            startIndex : startIndex
        }, function(json){
           $("body").unmask();
           noOfDirectDownlines = json.noOfDirectDownlines;

           json.treantObject.chart.callback = {
               onTreeLoaded: function(tree){
                   $(".Treant .collapse-switch").each(function (index) {
                       // add bootstrap tooltip on 'collapse switch'
                       $(this).data('toggle', 'tooltip').attr("title", "<s:text name="open/close"/>");

                       $(this).append("<i class='fa fa-expand'></i>");
                   });
               },
               onToggleCollapseFinished: function(treeNode, collapsed){
                   if(collapsed){
                       $(treeNode.nodeDOM).find(".collapse-switch i").removeClass("fa-compress").addClass("fa-expand");
                   }else{
                       $(treeNode.nodeDOM).find(".collapse-switch i").removeClass("fa-expand").addClass("fa-compress");
                   }
               }
           };

           new Treant(json.treantObject, null, $);
        });
    }

    $(function(){
        loadTree();

        $(document).on("click", ".btnUp", function(event){
            currentMemberId = memberIds.pop();
            startIndex = startIndices.pop();
            loadTree();
        });

        $(document).on("click", ".btnDown", function(event){
            memberIds.push(currentMemberId);
            startIndices.push(startIndex);

            startIndex = 0; // reset
            currentMemberId = $(this).attr("memberid");

            loadTree();
        });

        $(document).on("click", ".lessNode", function(event){
            startIndex -= size;
            if(startIndex < 0)
                startIndex = 0;
            loadTree();
        });

        $(document).on("click", ".moreNode", function(event){
            var memberId = $(this).children().attr("memberid");
            if(memberId === currentMemberId){
                startIndex += size;
            }else{
                memberIds.push(currentMemberId);
                startIndices.push(startIndex);

                startIndex = 0; // reset
                currentMemberId = memberId;
            }
            loadTree();
        });
    });
</script>
</s:if>


<sc:title title="title.gidTree" />

<sc:displayErrorMessage/>

<div class="row">
    <article class="col-md-6 col-sm-12">
        <sc:widget id="widget11" title="searchMember" cssClass="jarviswidget-color-blueLight">
            <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                <sc:memberTextField name="memberCode" required="true"/>

                <ce:buttonRow>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-angle-double-right"></i>
                        <s:text name="btnSubmit"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </sc:widget>
    </article>
</div>

<s:if test="memberExist">
    <sc:widget id="widget12" title="title.gidTree" fullScreenButton="true">
        <div class="row">
            <article class="col-md-offset-1 col-md-10 col-xs-12">
                <div class="treeChart" id="treeChart"></div>
            </article>
        </div>
    </sc:widget>
</s:if>