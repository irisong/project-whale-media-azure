<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.guildEdit"/>

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function () {
        $("#guildForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });

            }, // submitHandler
            rules: {
                "broadcastGuild.name": "required",
                "broadcastGuild.president": "required",
                "broadcastGuild.description": "required",
                "fileUpload": "required"
            }
        });

        //change textarea to ckEditor
        //change textarea to ckEditor
        $('#broadcastGuild\\.description').ckeditor({height: 300});

    });

    function changeUploadFile(fileUploadType, language) {
        $.getJSON("<s:url action="deleteGuildFile"/>", {
            bannerId: '<s:property value="broadcastGuild.id"/>',
            deleteLanguage: language
        }, function () {
            $("#" + fileUploadType).html('<input type="file" name="' + fileUploadType + '">');
        });
    }

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    window.location = "<s:url action="guildList"/>";
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="guildUpdate" name="guildForm" id="guildForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:hidden name="broadcastGuild.id" id="broadcastGuild.id"/>

    <div class="row">
        <article class="col-md-12">
            <div class="well">
                <s:textfield name="broadcastGuild.name" id="broadcastGuild.name" label="%{getText('name')}"
                             cssClass="input-xxlarge"/>
                <s:textfield name="broadcastGuild.president" id="broadcastGuild.president" label="%{getText('president')}"
                             cssClass="input-xxlarge"/>
                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label"><s:text name="Common"/>:</label>
                    <div class="col-md-9 col-sm-9" id="fileUpload">
                        <s:if test="%{broadcastGuild.fileUrl != null && broadcastGuild.fileUrl.length()>0}">
                            <a href="${broadcastGuild.fileUrl}" target="_blank"><img
                                    src="<c:url value="/images/fileopen.png"/>"/></a>
                        </s:if>
                        <s:file name="fileUpload" id="fileUpload"/>
                    </div>
                </div>
                <s:textarea name="broadcastGuild.description" id="broadcastGuild.description" theme="simple"/>
            </div>
        </article>
    </div>


    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="guildList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
