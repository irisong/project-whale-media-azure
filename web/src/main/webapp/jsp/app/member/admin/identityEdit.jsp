<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#identityEditForm").validate({
            submitHandler: function(form){
                form.submit();
              //  $('#btnUpdate').prop('disabled', true);
             //   $('#btnApprove').prop('disabled', true);
                $('#btnReject').prop('disabled', true);
            },
            // rules: {
            //     "identity.fullName" : {
            //         required : true
            //     },
            //     "identity.identityNo" : {
            //         required : true
            //     }
            // }
        });
    });


    function rejectUpload() {
        $("#updateStatus").val('REJECTED_UPLOAD');
        var remark = $("#member\\.kycRemark").val();
        if(remark === ''){
            messageBox.alert('<s:text name="youNeedToPutRemarkWhenYouRejectThisUpload"/>');
            return;
        }
        $("#identityEditForm").submit();
    }

    function approveUpload() {

        $("#updateStatus").val('APPROVE_UPLOAD');

        $("#identityEditForm").submit();
    }

    function viewImage(fileUrl){
        $("#imageViewer").removeAttr('src');

        // window.open(fileUrl, "_blank");
        $("#imageViewerModal").dialog('open');
        $("#imageViewer").attr('src', fileUrl);
    }


</script>


<sc:title title="title.identityEdit" />

<sc:displayErrorMessage align="center" />

<div class="row">
    <article class="col-sm-12">
        <div class="well">
            <s:form action="identityUpdate" name="identityEditForm" id="identityEditForm" cssClass="form-horizontal">

                <s:hidden name="member.memberId" id="member.memberId"/>
                <s:hidden name="updateStatus" id="updateStatus"/>
                <s:hidden name="member.kycStatus" id="member.kycStatus"/>
                <fieldset>
                    <legend><s:text name="identityForm"/></legend>
                    <s:textfield name="member.memberId" id="member.memberId" label="%{getText('memberCode')}" maxlength="20" disabled="true"/>

                    <s:textfield name="member.fullName" id="member.fullName" label="%{getText('fullName')}" maxlength="100" />
                    <s:textfield name="member.identityNo" id="member.identityNo" label="%{getText('identityNo')}" maxlength="50" />

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="Front IC" /></label>
                        <div class="col-md-6">
                            <s:if test="%{member.icFrontUrl.length() > 0}">
                                <s:iterator var="imageUrl" value="member.icFrontUrl">
                                    <a onclick="viewImage('${imageUrl}')"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </s:iterator>
                            </s:if>
                            <s:else>
                                <s:text name="noIcUploaded"></s:text>
                            </s:else>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="Back IC" /></label>
                        <div class="col-md-6">
                            <s:if test="%{member.icBackUrl.length() > 0}">
                                <s:iterator var="imageUrl" value="member.icBackUrl">
                                    <a onclick="viewImage('${imageUrl}')"><img src="<c:url value="/images/fileopen.png"/>" /></a>
                                </s:iterator>
                            </s:if>
                            <s:else>
                                <s:text name="noIcUploaded"></s:text>
                            </s:else>
                        </div>
                    </div>

                    <s:textfield name="member.kycStatus" id="member.kycStatus" label="%{getText('status')}" maxlength="50" disabled="true" />

                    <br /> <br />

                    <hr>


                    <s:if test="showApproveRejectButton">
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_remarks" /></label>
                            <div class="col-md-6">
                                <s:textarea id="member.kycRemark" name="member.kycRemark" cols="50" rows="5" cssClass="form-control" />
                            </div>
                        </div>
                    </s:if>
                    <s:else>
                        <div class="form-group">
                            <label class="col-md-3 control-label"><s:text name="label_remarks" /></label>
                            <div class="col-md-6">
                                <s:textarea id="member.kycRemark" name="member.kycRemark" cols="50" rows="5" cssClass="form-control" disabled="true" />
                            </div>
                        </div>
                    </s:else>

                    <ce:buttonRow>
                        <s:if test="showApproveRejectButton">

                            <button type="button" id="btnApprove" class="btn btn-warning m-r-5 m-b-5" onclick="approveUpload();">
                                <s:text name="btnApprove" />
                            </button>
                            <button type="button" id="btnRejectUpload" class="btn btn-warning m-r-5 m-b-5" onclick="rejectUpload();">
                                <s:text name="btnRejectUpload" />
                            </button>
                        </s:if>
                        <s:else>

                        </s:else>
                        <s:url var="urlExit" action="identityList" />
                        <ce:buttonExit url="%{urlExit}" type="button" class="btn" theme="simple">
                            <i class="fa fa-times"></i>
                            <s:text name="btnExit"/>
                        </ce:buttonExit>
                    </ce:buttonRow>
                </fieldset>
            </s:form>
        </div>
    </article>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true" resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>
