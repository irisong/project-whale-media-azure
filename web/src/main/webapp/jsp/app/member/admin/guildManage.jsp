<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.memberManage" />

<script type="text/javascript">
    var table = null;
    $(function () {
        table = $('#guildMemberListTable').DataTable({
            "ajax": {
                "url": "<s:url action="guildMemberListDatatables"/>",
                "data": {
                    guildid: $('#broadcastGuild\\.id').val()
                }
            },
            "columns": [
                {"data": "joinDate"},
                {"data": "serveStartDate"},
                {"data": "serveEndDate"},
                {"data": "memberCode"               },
                {"data": "memberDetail.profileName"},
                {"data": "rankName"},
                {
                    "data": "status",
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {
                    "data": "guildStatus",
                    "orderable": false,
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {
                    "data": "guildStatus",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = "";
                        result += '<button type="button" class="btn btn-danger btn-xs btnDelete"><s:text name="btnDelete"/></button>&nbsp;';
                        if (data != 'APPROVED') {
                            result += '<button type="button" class="btn btn-info btn-xs btnApprove"><s:text name="btnApprove"/></button>&nbsp;';
                        }else if(data == 'APPROVED'){
                            result += '<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';

                        }
                        return result;
                    }
                }
            ],
            "rowId": 'memberId'
        });

        $(document).on("click", ".btnDelete", function (event) {
            var id = $(this).parents("tr").attr("id");
            var guildid = $('#broadcastGuild\\.id').val();
            $("#memberId").val(id);
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.ajax({
                    type: 'POST',
                    url: "<s:url action="guildMemberDelete"/>",
                    dataType: 'json',
                    cache: false,
                    data: {
                        memberId: id,
                        id : guildid
                    },
                    success: function (data, status, xhr) {
                        $("body").unmask();
                        table.ajax.reload();
                    },
                    error: function (data, status, xhr)
                    {
                        $("body").unmask();
                    }
                });

            });
        });

        $(document).on("click", ".btnApprove", function (event) {
            var id = $(this).parents("tr").attr("id");
            var guildid = $('#broadcastGuild\\.id').val();
            $("#approveMemberForm_memberId").val(id);
            $("#approveMemberModal").dialog('open').dialog('vcenter');

        });

        $(document).on("click", ".btnAddMember", function (event) {
            var id =  $('#broadcastGuild\\.id').val();
            $("#id").val(id);
            $("#addMemberModal").dialog('open').dialog('vcenter');
            $("#memberCode").val('');
            $("#memberCode").focus();
        });


        $(document).on("click", ".btnAddAgent", function (event) {
            var id =  $('#broadcastGuild\\.id').val();
            $("#id").val(id);
            $("#addAgentModal").dialog('open').dialog('vcenter');
        });

        $(document).on("click", ".btnEdit", function (event) {
            var id = $(this).parents("tr").attr("id");
            var guildid = $('#broadcastGuild\\.id').val();
            $("#editMemberForm").trigger("reset");
            $("#editMemberForm_memberId").val(id);
            $("#editMemberModal").dialog('open').dialog('vcenter');

        });

        $(".btnCancel").click(function (event) {
            $("#addMemberModal").dialog('close');
            $("#approveMemberModal").dialog('close');
            $("#editMemberModal").dialog('close');
        });


        $("#addMemberForm").validate({
            submitHandler: function (form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "memberCode": {
                    required: true
                }
            }
        });

        $("#approveMemberForm").validate({
            submitHandler: function (form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }
        });

        $("#editMemberForm").validate({
            submitHandler: function (form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }
        });

        $("#addAgentForm").validate({
            submitHandler: function (form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }
        });

    });

    function dismissAllDialog() {
        $("#addMemberModal").dialog('close');
        $("#dateFrom").val();
        $("#dateTo").val();
        $("#startHalfMonth").checked=false;
        $("#endHalfMonth").checked=false;
        $("#approveMemberModal").dialog('close');
        $("#editMemberModal").dialog('close');
        $("#addAgentModal").dialog('close');
        $("#username").val();
        $("#password").val();
        $("#email").val();
        $("#phone").val();
    }

    function defaultJsonCallback(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                dismissAllDialog();
                window.location.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

</script>

<form id="navForm" method="post">
    <input type="hidden" name="memberId" id="memberId"/>
</form>

<s:form name="guildMemberForm" id="guildMemberForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:textfield name="broadcastGuild.name" id="broadcastGuild.name" label="%{getText('name')}" cssClass="input-xxlarge" readonly="true"/>
        <s:hidden  name="broadcastGuild.id" id="broadcastGuild.id" cssClass="input-xxlarge"/>
        <s:textfield name="broadcastGuild.president" id="broadcastGuild.president" label="President" cssClass="input-xxlarge" readonly="true"/>
        <s:if test="agentExisted">
            <s:textfield name="broadcastGuild.agentUserId" id="broadcastGuild.agentUserId" label="Username" cssClass="input-xxlarge" readonly="true"/>
        </s:if>
        <ce:buttonRow>
            <button type="button" name="btnAddMember" id="btnAddMember" class="btn btn-primary btnAddMember">
                <i class="fa fa-plus-circle"></i>
                <s:text name="btnAddMember"/>
            </button>
            <s:if test="!agentExisted">
            <button type="button" name="btnAddAgent" id="btnAddAgent" class="btn btn-primary btnAddAgent">
                <i class="fa fa-plus-circle"></i>
                <s:text name="btnAddAgent"/>
            </button>
            </s:if>
        </ce:buttonRow>
    </div>


    <h3><s:text name="title.memberList"/></h3>

    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <div class="row">
                    <article class="col-md-12 col-sm-12">
                        <table id="guildMemberListTable" class="table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><s:text name="dateJoin"/></th>
                                <th><s:text name="serveStartDate"/></th>
                                <th><s:text name="serveEndDate"/></th>
                                <th><s:text name="phoneNo"/></th>
                                <th><s:text name="profileName"/></th>
                                <th><s:text name="rankName"/></th>
                                <th><s:text name="memberStatus"/></th>
                                <th><s:text name="guildStatus"/></th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </article>
                </div>
            </div>
        </article>
    </div>
    <ce:buttonRow>
        <s:url var="urlExit" action="guildList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
<div id="addMemberModal" class="easyui-dialog" style="display:none; width:700px; height:400px"
     title="<s:text name="title.addMember"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="guildMemberAdd" cssClass="form-horizontal" name="addMemberForm"
                    id="addMemberForm">
                <sc:memberTextField name="memberCode"/>
                <s:select name="memberRank" list="allGuildRankList" label="%{getText('memberRank')}" listKey="key" listValue="value"/>
                <div class="form-group">
                    <label for="dateFrom" class="col-sm-3 control-label">From:</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                        <ce:datepicker theme="simple" name="dateFrom"  cssClass="form-control" autocomplete="off" cssStyle="z-index: 9999 !important;"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                    <div class="col-sm-12">
                                            <label class="control-label"><s:text name="halfMonth"/>: </label>
                                            <input type="checkbox" name="startHalfMonth"/>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="dateTo" class="col-sm-3 control-label">To:</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <ce:datepicker theme="simple" name="dateTo"  cssClass="form-control" autocomplete="off" cssStyle="z-index: 9999 !important;"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="col-sm-12">
                                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                                        <input type="checkbox" name="endHalfMonth" />
                                    </div>
                                </div>
                                <div class="input-group">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <s:hidden name="id" />

                <ce:buttonRow>
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button class="btn btnCancel" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>
<div id="approveMemberModal" class="easyui-dialog" style="display:none; width:700px; height:350px"
     title="<s:text name="title.approveMember"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="guildMemberApprove" cssClass="form-horizontal" name="approveMemberForm"
                    id="approveMemberForm">
                <s:select name="memberRank" list="allGuildRankList" label="%{getText('memberRank')}" listKey="key" listValue="value"/>
                <s:hidden name="id" />
                <s:hidden name="memberId" />
                <div class="form-group">
                    <label for="dateFrom" class="col-sm-3 control-label">From:</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <ce:datepicker theme="simple" name="dateFrom"  cssClass="form-control" autocomplete="off" cssStyle="z-index: 9999 !important;"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                    <div class="col-sm-9">
                                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                                        <input type="checkbox" name="startHalfMonth" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="dateTo" class="col-sm-3 control-label">To:</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <ce:datepicker theme="simple" name="dateTo"  cssClass="form-control" autocomplete="off" cssStyle="z-index: 9999 !important;"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="col-sm-9">
                                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                                        <input type="checkbox" name="endHalfMonth"/>
                                    </div>
                                </div>
                                <div class="input-group">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <ce:buttonRow>
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button class="btn btnCancel" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>
<div id="addAgentModal" class="easyui-dialog" style="display:none; width:700px; height:400px"
     title="<s:text name="title.addAgent"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="guildAgentAdd" cssClass="form-horizontal" name="addAgentForm"
                    id="addAgentForm">
                <s:hidden name="id" />
                <s:textfield key="username" id="username" required="true" size="20" maxlength="20" autocomplete="false"/>
                <s:password name="password" id="password" label="%{getText('password')}" required="true" size="20" maxlength="10" autocomplete="false"/>
                <s:textfield name="email" id="email" label="%{getText('email')}" required="true" size="50" maxlength="50"/>
                <s:textfield name="phone" id="phone" label="%{getText('phoneNo')}" required="true" size="50" maxlength="29"/>
                <ce:buttonRow>
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button class="btn btnCancel" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>
<div id="editMemberModal" class="easyui-dialog" style="display:none; width:700px; height:350px"
     title="<s:text name="title.editMember"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="guildMemberEdit" cssClass="form-horizontal" name="editMemberForm"
                    id="editMemberForm">
                <s:select name="memberRank" list="allGuildRankList" label="%{getText('memberRank')}" listKey="key" listValue="value"/>
                <s:hidden name="id" />
                <s:hidden name="memberId" />
                <div class="form-group">
                    <label for="dateFrom" class="col-sm-3 control-label">From:</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <ce:datepicker theme="simple" name="dateFrom"  cssClass="form-control" autocomplete="off" cssStyle="z-index: 9999 !important;"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>

                                    <div class="col-sm-9">
                                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                                        <input type="checkbox" name="startHalfMonth" />
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="dateTo" class="col-sm-3 control-label">To:</label>
                    <div class="col-sm-9">
                        <div class="form-group">
                            <div class="col-sm-12">
                                <div class="input-group">
                                    <ce:datepicker theme="simple" name="dateTo"  cssClass="form-control" autocomplete="off" cssStyle="z-index: 9999 !important;"/>
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <div class="col-sm-9">
                                        <label class="control-label"><s:text name="halfMonth"/>: </label>
                                        <input type="checkbox" name="endHalfMonth"/>
                                    </div>
                                </div>
                                <div class="input-group">

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <ce:buttonRow>
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button class="btn btnCancel" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>