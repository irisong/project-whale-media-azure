<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<!-- required js & css for treanjs (START)-->
<link rel="stylesheet" href="<c:url value="/asset/plugin/treantjs/Treant.css"/>">
<link rel="stylesheet" href="<c:url value="/asset/plugin/treantjs/treantjs-tree.css"/>">
<link rel="stylesheet" href="<c:url value="/asset/plugin/treantjs/perfect-scrollbar/perfect-scrollbar.css"/>">

<script src="<c:url value="/asset/plugin/treantjs/raphael.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/Treant.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/jquery.easing.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/jquery.mousewheel.js"/>"></script>
<script src="<c:url value="/asset/plugin/treantjs/perfect-scrollbar/perfect-scrollbar.js"/>"></script>
<!-- required js & css for treanjs (END) -->

<s:if test="memberExist">
<script type="text/javascript">
    var rootMemberId = "${planbTree.memberId}";
    var rootUnit = "${planbTree.unit}";

    var currentMemberId = rootMemberId;
    var currentUnit = rootUnit;

    // implement stack (FILO) for member id and units.
    var memberIds = [];
    var units = [];

    function loadTree(){
        $("body").loadmask("<s:text name="processing.msg"/>");

        $.post("<s:url action="planbTreeTreantData" namespace="/app/member"/>", {
            rootMemberId: rootMemberId,
            rootUnit: rootUnit,
            memberId : currentMemberId,
            unit: currentUnit
        }, function(json){
            $("body").unmask();

            json.treantObject.chart.callback = {
                onTreeLoaded: function(tree){
                    $(".Treant .collapse-switch").each(function (index) {
                        // add bootstrap tooltip on 'collapse switch'
                        $(this).data('toggle', 'tooltip').attr("title", "<s:text name="open/close"/>");

                        $(this).append("<i class='fa fa-expand'></i>");
                    });
                },
                onToggleCollapseFinished: function(treeNode, collapsed){
                    if(collapsed){
                        $(treeNode.nodeDOM).find(".collapse-switch i").removeClass("fa-compress").addClass("fa-expand");
                    }else{
                        $(treeNode.nodeDOM).find(".collapse-switch i").removeClass("fa-expand").addClass("fa-compress");
                    }
                }
            };

            new Treant(json.treantObject, null, $);
        });
    }

    $(function(){
        loadTree();

        $(document).on("click", ".btnUp", function(event){
            currentMemberId = memberIds.pop();
            currentUnit = units.pop();

            loadTree();
        });

        $(document).on("click", ".btnDown", function(event){
            memberIds.push(currentMemberId);
            units.push(currentUnit);

            currentMemberId = $(this).attr("memberid");
            currentUnit = $(this).attr("unit");

            loadTree();
        });
    });
</script>
</s:if>

<sc:title title="title.planbTree" />

<sc:displayErrorMessage/>

<s:if test="memberExist">
    <sc:widget id="widget11" title="planbInformation">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <dl class="dl-horizontal">
                    <ce:dd label="%{getText('memberId')}" value="member.memberCode"/>
                    <ce:dd label="%{getText('fullname')}" value="member.fullName"/>
                    <ce:dd label="%{getText('ranking')}" value="member.bonusRank.rankName"/>
                    <ce:dd label="%{getText('dateJoin')}" value="%{@SF@formatDate(member.joinDate)}"/>
                </dl>
            </div>
            <div class="col-md-6 col-sm-12">
                <dl class="dl-horizontal">
                    <ce:dd label="%{getText('unit')}" value="planbTree.displayName"/>

                    <s:if test="planbTree.maintained">
                        <s:set var="m" value="%{getText('yes')}"/>
                        <s:set var="dFrom" value="%{@SF@formatDate(planbTree.maintenance.effectiveDateFrom)}"/>
                        <s:set var="dTo" value="%{@SF@formatDate(planbTree.maintenance.effectiveDateTo)}"/>
                    </s:if>
                    <s:else>
                        <s:set var="m" value="%{getText('no')}"/>
                        <s:set var="dFrom">-</s:set>
                        <s:set var="dTo">-</s:set>
                    </s:else>

                    <ce:dd label="%{getText('maintenance')}" value="#m"/>
                    <ce:dd label="%{getText('effectiveDateFrom')}" value="#dFrom"/>
                    <ce:dd label="%{getText('effectiveDateTo')}" value="#dTo"/>
                </dl>
            </div>
        </div>
    </sc:widget>

    <sc:widget id="widget12" title="title.planbTree" fullScreenButton="true">
        <div class="row">
            <article class="col-md-offset-1 col-md-10 col-xs-12">
                <div class="treeChart" id="treeChart"></div>
            </article>
        </div>
    </sc:widget>
</s:if>