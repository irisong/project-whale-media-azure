<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.gidbonus" />

<sc:displayErrorMessage/>

<div class="row">
    <article class="col-md-6 col-sm-12">
        <sc:widget id="widget11" title="searchMember" cssClass="jarviswidget-color-blueLight">
            <s:form name="searchForm" id="searchForm" cssClass="form-horizontal" method="GET">
                <sc:memberTextField name="memberCode" required="true"/>

                <ce:buttonRow>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-angle-double-right"></i>
                        <s:text name="btnSubmit"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </sc:widget>
    </article>
</div>

<s:if test="memberExist">
    <script type="text/javascript">
        $(function() {
            $('#directSponsorTable').DataTable({
                "destroy":true,
                "order": [[ 0, "asc" ]],
                "ajax": {
                    "url" : "<s:url action="directSponsorListDatatables"/>",
                    "data" : {
                        "memberCode" : "<s:property value="memberCode"/>"
                    }
                },
                "columns" : [
                    {"data" : "memberCode"},
                    {"data" : "joinDate"},
                    {"data" : "teamMarketValue"},
                    {
                        "data" : "status",
                        "orderable": false,
                        "render": function(data, type, row) {
                            var result = '';
                            result += '&nbsp;<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';
                            return result;
                        }
                    }
                ],
                "rowId": 'memberCode'
            });

            $(document).on("click", ".btnView",function (event) {
                var id = $(this).parents("tr").attr("id");

                $("#memberCode").val(id);
                console.log(id);
                $("#searchForm").submit();
            });
        });
    </script>

    <div class="memberInformation" id="memberInformation">
        <div class="row">
            <article class="col-sm-12">
                <div class="well">
                    <fieldset>
                        <legend><s:text name="memberInformation"/></legend>
                    </fieldset>
                    <div class="clearfix">
                        <label class="col-md-2 control-label"><s:text name="memberId"/></label>
                            <div class="col-md-10">
                                <p><s:property value="memberCode"/></p>
                            </div>
                     </div>
                    <div class="clearfix">
                        <label class="col-md-2 control-label"><s:text name="vipLevel"/></label>
                            <div class="col-md-10">
                                <p><s:property value="vipLevel"/></p>
                            </div>
                    </div>
                    <div class="clearfix">
                        <label class="col-md-2 control-label"><s:text name="totalGgsValue"/></label>
                            <div class="col-md-10">
                                <p><s:property value="teamMarketValue"/></p>
                            </div>
                    </div>
                </div>
            </article>
        </div>
    </div>

    <div class="row">
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <sc:widget id="widget_111" title="">
                <table id="directSponsorTable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><s:text name="memberId"/></th>
                        <th><s:text name="dateJoin"/></th>
                        <th><s:text name="totalGgsValue"/></th>
                        <th>&nbsp</th>
                    </tr>
                    </thead>
                </table>
            </sc:widget>
        </article>
    </div>
</s:if>
