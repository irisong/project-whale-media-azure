<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.guildAdd"/>

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function () {

        $("#btnExit").click(function () {
            $.ajax({
                success: proceedExit
            });
        });

        $("#guildForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "broadcastGuild.name": "required",
                "broadcastGuild.president": "required",
                "broadcastGuild.description": "required",
                "fileUpload": "required",
                "username":{
                    required : true,
                    minlength : 3
                },
                "fullname":{
                    required : true,
                    minlength : 5
                },
                "email":{
                    required: true,
                    email: true
                },
                "password":{
                    required: true,
                    minlength: 5
                }
            }
        });
        $('#broadcastGuild\\.description').ckeditor({height: 300});
    });


    function proceedExit() {
        window.location = "<s:url action="guildList"/>";
    }


    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    window.location = "<s:url action="guildList"/>";
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="guildSave" name="guildForm" id="guildForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <%--    <s:hidden name="banner.bannerId" id="banner.bannerId"/>--%>
    <%--    <input type="hidden" class="hiddenMemberId" name="memberId"/>--%>

    <div class="row">
        <article class="col-md-12">
            <s:textfield name="broadcastGuild.name" id="broadcastGuild.name" label="%{getText('name')}"
                         cssClass="input-xxlarge"/>
            <s:textfield name="broadcastGuild.president" id="broadcastGuild.president" label="%{getText('president')}"
                         cssClass="input-xxlarge"/>
            <s:textfield key="username" id="username" required="true" size="20" maxlength="20" autocomplete="false"/>
            <s:password name="password" id="password" label="%{getText('password')}" required="true" size="20" maxlength="10" autocomplete="false"/>
            <s:textfield name="email" id="email" label="%{getText('email')}" required="true" size="50" maxlength="50"/>
            <s:textfield name="phone" id="phone" label="%{getText('phoneNo')}" required="true" size="50" maxlength="29"/>
            <s:file name="fileUpload" id="fileUpload" label="%{getText('File')}"/>
            <s:textarea name="broadcastGuild.description" id="broadcastGuild.description" theme="simple"
                        required="true"/>
        </article>

    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>


        <button id="btnExit" type="button" class="btn">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </button>

    </ce:buttonRow>
</s:form>
<!-- #modal-dialog -->
