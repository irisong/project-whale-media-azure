<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.fansBadgeList"/>

<script type="text/javascript">
    var table = null;

    $(function () {
        table = $('#fansBadgeListTable').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="fansBadgeListDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    createDateFrom: $('#createDateFrom').val(),
                    createDateTo: $('#createDateTo').val(),
                    status: $('#status').val()
                }
            },
            "columns": [
                {
                    "data": "createDate",
                    "render": function (data, type, row) {
                        if (data == null)
                            return '';
                        else
                            return $.datagridUtil.formatDate(data);
                    }},
                {"data": "member.memberCode"},
                {"data": "fansBadgeName"},
                {
                    "data": "status",
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {
                    "data": "activeDatetime",
                    "render": function (data, type, row) {
                        if (data == null)
                            return '';
                        else
                            return $.datagridUtil.formatDate(data);
                    }
                },
                {
                    "data": "rejectDatetime",
                    "render": function (data, type, row) {
                        if (data == null)
                            return '';
                        else
                            return $.datagridUtil.formatDate(data);
                    }
                },
                {"data": "remark"},
                {
                    "data": "status",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '';

                        if (data == 'PENDING') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnApprove"><s:text name="btnApprove"/></button>';
                            result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnReject"><s:text name="btnReject"/></button>';
                        }
                        return result;
                    }
                }
            ],
            "rowId": 'fansBadgeId'
        });

        $(document).on("click", ".btnApprove", function(event) {
            var id = $(this).parents("tr").attr("id");
            $.getJSON("<s:url action="approveFansBadge"/>", { fansBadgeId: id }, defaultJsonCallback);
        });

        $(document).on("click", ".btnReject", function() {
            var id = $(this).parents("tr").attr("id");
            $("#rejectModal").dialog('open').dialog('vcenter');
            $(".fansBadgeId").val(id);
            $("#rejectReason").val('');
            $("#rejectReason").focus();
        });

        $("#rejectFansBadgeForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: defaultJsonCallback
                    });
                });
            }
        });

        $(".btnCancel").click( function() {
            $("#rejectModal").dialog('close');
        });
    }); // end $(function())

    function defaultJsonCallback(json) {
        $("body").unmask();
        new JsonStat(json, {
            onSuccess: function () {
                table.ajax.reload();
                $("#rejectModal").dialog('close');
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form name="fansBadgeSearchForm" id="fansBadgeSearchForm" cssClass="form-horizontal">
    <sc:memberTextField name="memberCode"/>
    <sc:datepickerFromTo nameFrom="createDateFrom" nameTo="createDateTo" labelFrom="createDate"/>
    <s:select name="status" id="status" list="allStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>

    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.fansBadgeList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="fansBadgeListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="createDate"/></th>
                            <th><s:text name="phoneNo"/></th>
                            <th><s:text name="fansBadgeName"/></th>
                            <th><s:text name="status"/></th>
                            <th><s:text name="activeDate"/></th>
                            <th><s:text name="rejectDate"/></th>
                            <th><s:text name="reason"/></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="rejectModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="rejectFansBadge"/>"
     closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="rejectFansBadge" name="rejectFansBadgeForm" id="rejectFansBadgeForm" cssClass="form-horizontal"
                        enctype="multipart/form-data" method="post">

                    <label class="control-label" style="vertical-align: top;"><s:text name="reason"/> : </label>
                    <s:textarea name="rejectReason" id="rejectReason" theme="simple" rows="5" cols="60" required="true"/>

                    <input type="hidden" name="fansBadgeId" class="fansBadgeId"/>
                    <ce:buttonRow>
                        <button id="btnProceed" class="btn btn-primary" type="submit">
                            <s:text name="btnProceed"/>
                        </button>
                        <button class="btn btnCancel" type="button">
                            <s:text name="btnCancel"/>
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>