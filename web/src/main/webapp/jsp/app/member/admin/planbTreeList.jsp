<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.planbTree" />

<script type="text/javascript">
    $(function() {
        var table = $('#planbTreeListTable').DataTable({
            "order": [[ 0, "asc" ]],
            "ajax": {
                "url" : "<s:url action="planbTreeListDatatables"/>",
                "data" : {
                    memberCode : $('#memberCode').val(),
                    fullName: $('#fullName').val(),
                    maintained: $('#maintained').val()
                }
            },
            "columns" : [
                {"data" : "member.memberCode"},
                {
                    "data" : "displayName",
                    "orderable": false
                },
                {"data" : "member.fullName"},
                {"data" : "member.bonusRank.rankName"},
                {"data" : "level"},
                {
                    "data" : "maintained",
                    "orderable": false,
                    "render": function(data, type, row){
                        return $.datagridUtil.formatYesNo(data);
                    }
                },
                {
                    "data" : "maintenance.effectiveDateFrom",
                    "render": function(data, type, row){
                        if(data === null){
                            return "-";
                        }else{
                            return $.datagridUtil.formatDate(data);
                        }
                    }
                },
                {
                    "data" : "maintenance.effectiveDateTo",
                    "render": function(data, type, row){
                        if(data === null){
                            return "-";
                        }else{
                            return $.datagridUtil.formatDate(data);
                        }
                    }
                },
                {"data" : "datetimeAdd"},
                {
                    "data" : "memberId",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function(data, type, row){
                        var result = '<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": 'id'
        });

        $(document).on("click", ".btnView", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#planbTree\\.id").val(id);
            $("#navForm").attr("action", "<s:url action="planbTree" />");
            $("#navForm").submit();
        });
    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="planbTree.id" id="planbTree.id" />
</form>

<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
    <sc:memberTextField name="memberCode"/>
    <s:textfield name="fullName" id="fullName" label="%{getText('name')}"/>
    <s:select name="maintained" id="maintained" list="maintainOptions" label="%{getText('maintenance')}" listValue="value" listKey="key"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.planbTree"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="planbTreeListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th rowspan="2"><s:text name="memberId"/></th>
                            <th rowspan="2"><s:text name="unit"/></th>
                            <th rowspan="2"><s:text name="name"/></th>
                            <th rowspan="2"><s:text name="ranking"/></th>
                            <th rowspan="2"><s:text name="level"/></th>
                            <th rowspan="2"><s:text name="maintenance"/></th>
                            <th colspan="2"><s:text name="effectiveDate"/></th>
                            <th rowspan="2"><s:text name="creationDate"/></th>
                            <th rowspan="2">&nbsp;</th>
                        </tr>
                        <tr>
                            <th><s:text name="from"/></th>
                            <th><s:text name="to"/></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>