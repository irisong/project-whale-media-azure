<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.guild"/>

<script type="text/javascript">
    var table = null;
    $(function () {
        table = $('#guildListTable').DataTable({
            "ajax": {
                "url": "<s:url action="guildListDatatables"/>",
                "data": {
                    name: $('#name').val()
                }
            },
            "columns": [
                {"data": "name"},
                {"data": "president"},
                {"data": "description"},
                {
                    "data": "fileUrl",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data": "id",
                    "className": "text-nowrap text-center",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-danger btn-xs btnDeleteGuild"><s:text name="btnDelete"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-success btn-xs btnManage"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnManage"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "id"
        });

        $("#btnCreate").click(function (event) {
            $("#navForm").attr("action", "<s:url action="guildAdd" />")
            $("#navForm").submit();
        });

        <%--$("#btnAddNew").click(function (event) {--%>
        <%--    $("#addNewCategoryModal").dialog('open');--%>
        <%--});--%>

        // $("#newCategoryForm").validate({
        //     submitHandler: function (form) {
        //         $("#addNewCategoryModal").dialog('close');
        //         $(form).ajaxSubmit({
        //             dataType: 'json',
        //             success: processJsonSave
        //         });
        //     },
        //     rules: {
        //         "name": {
        //             required: true
        //         },
        //         "desc": {
        //             required: true
        //         }
        //     }
        // });

        $(document).on("click", ".btnEdit", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="guildEdit" />")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnManage", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="guildManage" />")
            $("#navForm").submit();
        });

         $(document).on("click", ".btnDeleteGuild", function (event) {
             var id = $(this).parents("tr").attr("id");
             $("#id").val(id);
             messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                 $("body").loadmask("<s:text name="processing.msg"/>");
                 $.ajax({
                     type: 'POST',
                     url: "<s:url action="guildDelete"/>",
                     dataType: 'json',
                     cache: false,
                     data: {
                         id: id
                     },
                     success: function (data, status, xhr) {
                         $("body").unmask();
                         table.ajax.reload();
                     },
                     error: function (data, status, xhr)
                     {
                         $("body").unmask();
                     }
                 });

             });
         });


        <%--    $(document).on("click", ".btnDeleteBanner", function (event) {--%>
        <%--        var id = $(this).parents("tr").attr("id");--%>
        <%--        selectedIds = [id];--%>
        <%--        messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {--%>
        <%--            $("body").loadmask("<s:text name="processing.msg"/>");--%>
        <%--            $.post("<s:url action="bannerDelete"/>", {--%>
        <%--                "bannerId": selectedIds.toString()--%>
        <%--            }, processJsonSave);--%>
        <%--        });--%>
        <%--    });--%>

        <%--    $(document).on("click", ".btnDeactivate", function (event) {--%>
        <%--        var id = $(this).parents("tr").attr("id");--%>
        <%--        selectedIds = [id];--%>
        <%--        messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {--%>
        <%--            $("body").loadmask("<s:text name="processing.msg"/>");--%>
        <%--            $.post("<s:url action="liveCategoryStatusUpdate"/>", {--%>
        <%--                "categoryId": selectedIds.toString(),--%>
        <%--                "status": "INACTIVE",--%>
        <%--            }, processJsonSave);--%>
        <%--        });--%>
        <%--    });--%>

        <%--    $(document).on("click", ".btnActivate", function (event) {--%>
        <%--        var id = $(this).parents("tr").attr("id");--%>
        <%--        selectedIds = [id];--%>
        <%--        messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {--%>
        <%--            $("body").loadmask("<s:text name="processing.msg"/>");--%>
        <%--            $.post("<s:url action="liveCategoryStatusUpdate"/>", {--%>
        <%--                "categoryId": selectedIds.toString(),--%>
        <%--                "status": "ACTIVE",--%>
        <%--            }, processJsonSave);--%>
        <%--        });--%>
        <%--    });--%>

    });

    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');

        $("#imageViewerModal").dialog('open');
        $("#imageViewer").attr('src', fileUrl);
    }

    function processJsonSave(json) {
        console.log(json);
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                messageBox.info(json.successMessage, function () {
                    // refresh page
                    window.location = "<s:url action="guildList"/>";
                });

            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

    // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="id" id="id"/>
</form>

<s:form name="guildSearchForm" id="guildSearchForm" cssClass="form-horizontal">
    <s:textfield name="name" id="name" label="%{getText('name')}"/>
    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>


<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.guildList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="guildListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="name"/></th>
                            <th><s:text name="president"/></th>
                            <th><s:text name="description"/></th>
                            <th><s:text name="fileCombine"/></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<%--<div class="row">--%>
<%--    <article class="col-md-12 col-sm-12">--%>
<%--        <button id="btnAddNew" type="button" class="btn btn-warning">--%>
<%--            <i class="fa fa-check-circle"></i>--%>
<%--            <s:text name="title.guildAdd"/>--%>
<%--        </button>--%>
<%--        <div class="well">--%>
<%--            <fieldset><legend><s:text name="title.guildList"/></legend></fieldset>--%>
<%--            <div class="row">--%>
<%--                <article class="col-md-12 col-sm-12">--%>
<%--                    <table id="guildListTable" class="table table-bordered" cellspacing="0" width="100%">--%>
<%--                        <thead>--%>
<%--                        <tr>--%>
<%--                            <th field="ck" checkbox="true"></th>--%>
<%--                            <th><s:text name="name"/></th>--%>
<%--                            <th><s:text name="description"/></th>--%>
<%--                            <th><s:text name="fileCombine"/></th>--%>
<%--                            <th>&nbsp;</th>--%>
<%--                        </tr>--%>
<%--                        </thead>--%>
<%--                    </table>--%>
<%--                </article>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--    </article>--%>
<%--</div>--%>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true"
     resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>