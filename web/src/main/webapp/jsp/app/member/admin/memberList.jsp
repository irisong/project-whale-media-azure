<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.memberList"/>

<script type="text/javascript">
    var table = null;

    $(function () {
        table = $('#memberListTable').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="memberListDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    whaleliveId:$('#whaleliveId').val(),
                    joinDateFrom: $('#joinDateFrom').val(),
                    joinDateTo: $('#joinDateTo').val(),
                    memberStatus: $('#memberStatus').val()
                }
            },
            "columns": [
                {"data": "joinDate"},
                {"data": "memberDetail.whaleliveId"},
                {"data": "memberCode"},
                {"data": "memberDetail.profileName"},
                {
                    "data": "status",
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {"data": "liveStreamTypes"},
                {
                    "data": "liveStreamPrivate",
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatYesNo(data);
                    }
                },
                {
                    "data": "liveStreamStatus",
                    "orderable": false,
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {
                    "data": "liveStreamStatus",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '';

                        result += '&nbsp;<div class="btn-group">\n' +
                            '    <button class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown"><s:text name="more"/>  <span class="caret"></span></button>\n' +
                            '    <ul class="dropdown-menu">\n' +
                            '        <li><a class="btnResetPassword">Reset Password</a></li>\n' +
                            '        <li><a class="btnResetTransactionPassword">Reset Transaction Password</a></li>\n' +
                            '    </ul>\n' +
                            '</div>';

                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';

                        if (data === 'DISABLED') {
                            result += '&nbsp;<button type="button" class="btn btn-success btn-xs btnUnblockLive"><s:text name="btnUnblockLive"/></button>';
                        } else if (data === 'ACTIVE') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnBlockLive"><s:text name="btnBlockLive"/></button>';
                        }

                        return result;
                    }
                }
            ],
            "rowId": 'memberId'
        });

        $(document).on("click", ".btnEdit", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#member\\.memberId").val(id);
            $("#navForm").attr("action", "<s:url action="memberEdit"/>");
            $("#navForm").submit();
        });

        $(document).on("click", ".btnView", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#member\\.memberId").val(id);
            $("#navForm").attr("action", "<s:url action="memberView"/>");
            $("#navForm").submit();
        });

        $(document).on("click", ".btnUnblockLive", function (event) {
            var id = $(this).parents("tr").attr("id");
            $.getJSON("<s:url action="memberUnblockLive"/>", { memberId: id }, defaultJsonCallback);
        });

        $(document).on("click", ".btnBlockLive", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#blockLiveStreamModal").dialog('open').dialog('vcenter');
            $(".hiddenMemberId").val(id);
            $("#blockReason").val('');
            $("#blockReason").focus();
        });

        $("#memberBlockLiveForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: defaultJsonCallback
                    });
                });
            }, // submitHandler
            rules: {
                "blockReason": "required"
            }
        });

        $(document).on("click", ".btnResetPassword", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#resetPasswordModal").dialog('open').dialog('vcenter');
            $(".hiddenMemberId").val(id);
            $("#password").val('');
            $("#password").focus();
        });

        $("#resetPasswordForm").validate({
            submitHandler: function (form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "password": {
                    required: true
                }
            }
        });

        $(document).on("click", ".btnResetTransactionPassword", function (event) {
            var id = $(this).parents("tr").attr("id");

            $("#resetTransactionPasswordModal").dialog('open').dialog('vcenter');
            $(".hiddenMemberId").val(id);
            $("#password2").val('');
            $("#password2").focus();
        });

        $("#resetTransactionPasswordForm").validate({
            submitHandler: function (form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "password2": {
                    required: true
                }
            }
        });

        <%--$("#btnMemberListExportExcel").click(function (event) {--%>
        <%--    document.location.href = '<s:url action="memberExportExcel"/>';--%>
        <%--});--%>

        $(".btnCancel").click(function (event) {
            dismissAllDialog();
        });
    }); // end $(function())

    function dismissAllDialog() {
        $("#resetPasswordModal").dialog('close');
        $("#resetTransactionPasswordModal").dialog('close');
        $("#blockLiveStreamModal").dialog('close');
    }

    function defaultJsonCallback(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                dismissAllDialog();
                table.ajax.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="member.memberId" id="member.memberId"/>
</form>

<s:form name="memberSearchForm" id="memberSearchForm" cssClass="form-horizontal">
    <sc:memberTextField name="memberCode"/>
    <s:textfield name="whaleliveId" id="whaleliveId" label="%{getText('whaleliveId')}"/>
    <sc:datepickerFromTo nameFrom="joinDateFrom" nameTo="joinDateTo" labelFrom="dateJoin"/>
    <s:select name="memberStatus" id="memberStatus" list="allStatusList" label="%{getText('memberStatus')}" listKey="key" listValue="value"/>

    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
<%--        <button type="button" name="btnMemberListExportExcel" id="btnMemberListExportExcel" class="btn btn-primary m-r-5 m-b-5">--%>
<%--        <s:text name="btnExportExcel"/>--%>
<%--        </button>--%>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.memberList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="memberListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="registerDate"/></th>
                            <th><s:text name="whaleliveId"/></th>
                            <th><s:text name="phoneNo"/></th>
                            <th><s:text name="profileName"/></th>
                            <th><s:text name="memberStatus"/></th>
                            <th><s:text name="liveStreamCategory"/></th>
                            <th><s:text name="liveStreamPrivate"/></th>
                            <th><s:text name="liveStreamStatus"/></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="resetPasswordModal" class="easyui-dialog" style="display:none; width:300px; height:150px"
     title="<s:text name="reset.password"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="memberResetPassword" cssClass="form-horizontal" name="resetPasswordForm"
                    id="resetPasswordForm">
                <s:textfield name="password" id="password" label="%{getText('password')}"/>

                <input type="hidden" class="hiddenMemberId" name="memberId"/>
                <ce:buttonRow>
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button class="btn btnCancel" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="resetTransactionPasswordModal" class="easyui-dialog" style="display:none; width:300px; height:150px"
     title="<s:text name="resetTransactionPassword"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="memberResetTransactionPassword" cssClass="form-horizontal"
                    name="resetTransactionPasswordForm" id="resetTransactionPasswordForm">
                <s:textfield name="password2" id="password2" label="%{getText('password')}"/>

                <input type="hidden" class="hiddenMemberId" name="memberId"/>
                <ce:buttonRow>
                    <button class="btn btn-primary btnSave" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button class="btn btnCancel" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="blockLiveStreamModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="blockLiveStream"/>"
     closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="memberBlockLive" name="memberBlockLiveForm" id="memberBlockLiveForm" cssClass="form-horizontal"
                        enctype="multipart/form-data" method="post">

                    <label class="control-label" style="vertical-align: top;"><s:text name="reason"/></label>
                    <s:textarea name="blockReason" id="blockReason" theme="simple" rows="5" cols="60" required="true"/>

                    <input type="hidden" class="hiddenMemberId" name="memberId"/>
                    <ce:buttonRow>
                        <button id="btnProceed" class="btn btn-primary" type="submit">
                            <s:text name="btnProceed"/>
                        </button>
                        <button class="btn btnCancel" type="button">
                            <s:text name="btnCancel"/>
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>