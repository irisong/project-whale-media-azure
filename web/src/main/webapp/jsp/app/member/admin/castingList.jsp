<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.casting"/>

<script type="text/javascript">
    var table;

    $(function () {
        table = $('#castingTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="castingDatatables"/>",
                "data": {
                    whaleLiveId: $('#whaleliveId').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val()
                }
            },
            "columns": [
                {"data": "datetimeAdd"},
                {"data": "whaleLiveId"},
                {"data": "whaleliveProfileName"},
                {"data": "fullName"},
                {"data": "guildName"},
                {"data": "phoneNo"},
                {"data": "email"},
                {
                    "data": "gender",
                    "render": function (data, type, row) {
                        var content = 'Male';
                        if (data == "F") {
                            content = 'Female';
                        }
                        return content;
                    }
                },
                {"data": "age"},
                {"data": "status"},
                {"data": "url"},
                {
                    "data": "status",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '';
                        result += '&nbsp;<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';

                        return result;
                    }
                }
            ],
            "rowId": "whaleLiveId"
        });


        $(document).on("click", ".btnView", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="castingEdit"/>")
            $("#navForm").submit();
        });

    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    $("#castingForm").submit(); // refresh page
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="id" id="id"/>
</form>

<s:form name="castingForm" id="castingForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:textfield name="whaleliveId" id="whaleliveId" label="%{getText('whaleliveId')}"/>
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="creationDate"/>
    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <table id="castingTable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><s:text name="datetimeAdd"/></th>
                    <th><s:text name="whaleliveId"/></th>
                    <th><s:text name="profileName"/></th>
                    <th><s:text name="contactPerson"/></th>
                    <th><s:text name="guildName"/></th>
                    <th><s:text name="phoneNo"/></th>
                    <th><s:text name="email"/></th>
                    <th><s:text name="gender"/></th>
                    <th><s:text name="age"/></th>
                    <th><s:text name="status"/></th>
                    <th><s:text name="url"/></th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>
