<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.identityList"/>

<script type="text/javascript">
    var table = null;
    $(function () {
        table = $('#identityTable').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="identityListDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    whaleliveId:$('#whaleliveId').val(),
                    dateFrom: $('#joinDateFrom').val(),
                    dateTo: $('#joinDateTo').val(),
                    status: $('#status').val()
                }
            },
            "columns": [
                {"data": "memberCode"},
                {"data": "memberDetail.whaleliveId", "orderable": false},
                {"data": "fullName"},
                {"data": "identityNo"},
                {
                    "data" : "icFrontUrl",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data" : "icBackUrl",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {"data": "kycStatus"},
                {"data": "kycSubmitDate","orderable": false},
                {
                    "data": "kycRemark",
                    "orderable": false
                },
                {
                    "data": "kycStatus",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '';
                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnManage"/></button>';
                        if (data === 'PENDING_APPROVAL') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnApprove"><s:text name="btnApprove"/></button>';
                          //  result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnReject"><s:text name="btnReject"/></button>';
                        }
                        return result;
                    }
                }

            ], "select":
                {
                    "style": 'multi'
                },
            "rowId": "memberId"
        });



        $(document).on("click", ".btnEdit", function (event) {
            var id = $(this).parents("tr").attr("id");

            $("#identityId").val(id);
            $("#navForm").attr("action", "<s:url action="identityEdit"/>").submit();
        });


        $(document).on("click", ".btnApprove", function (event) {
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="identityApprove"/>", {
                    "identityId": selectedIds.toString(),
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnReject", function (event) {
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="identityReject"/>", {
                    "identityId": selectedIds.toString(),
                }, processJsonSave);
            });
        });





        $("#btnUpdate").click(function (event) {
            var ids = [];
            var checked = [];
            checked = $(table.$('input[type="checkbox"]').map(function () {
                return $(this).prop("checked") ? $(this).closest('tr').attr("id") : null;
            }));
            console.log(checked);

            for (var i = 0; i < checked.length; i++) {
                ids.push(checked[i]);
            }
            console.log(ids)
            if (ids.length <= 0) {
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            $("#ids").val(ids.join(','));

            $("#statusModal").dialog('open');
        });

        function processJsonSave(json) {
            console.log(json);
            $("body").unmask();

            new JsonStat(json, {
                onSuccess: function (json) {
                    messageBox.info(json.successMessage, function () {
                        // refresh page
                        // window.location = "<s:url action="identityList"/>";
                        $("#identityForm").submit();
                    });

                },
                onFailure: function (json, error) {
                    messageBox.alert(error);
                }
            });
        }

        $("#statusUpdateForm").validate({
            submitHandler: function (form) {
                $("#statusModal").dialog('close');

                $.post('<s:url action="identityUpdateStatus"/>', {
                    "ids": $("#ids").val(),
                    "updateStatus": $("#updateStatus").val(),
                    "remark": $("#remark").val()
                }, function (json) {

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                $('#identityUpdateStatus').DataTable('load', {
                                    agentCode: $('#agentCode').val(),
                                    updateStatus: $('#updateStatus').val()
                                });
                            });
                            table.ajax.reload();
                        },
                        onFailure: function (json, error) {
                            $("#statusModal").dialog('open');
                            messageBox.alert(error);
                        }
                    });
                });
            }
        });
    })

    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');

        $("#imageViewerModal").dialog('open');
        $("#imageViewer").attr('src', fileUrl);
    }
</script>

<input type="hidden" name="ids" id="ids"/>

<form id="navForm" method="post">
    <input type="hidden" name="identityId" id="identityId"/>
</form>

<s:form name="identityForm" id="identityForm" cssClass="form-horizontal">
    <sc:memberTextField name="memberCode"/>
    <s:textfield name="whaleliveId" id="whaleliveId" label="%{getText('whaleliveId')}"/>
    <s:select name="status" id="status" list="allIdentityStatusCodeList" label="%{getText('status')}" listKey="key"
              listValue="value"/>
    <sc:datepickerFromTo nameFrom="joinDateFrom" nameTo="joinDateTo" labelFrom="kycSubmitDate"/>
    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>

<%--        <button type="button" name="btnUpdate" id="btnUpdate" class="btn btn-info m-r-5 m-b-5">--%>
<%--            <s:text name="btnUpdate"/>--%>
<%--        </button>--%>

        <!--
        <button type="button" name="btnExportExcel" id="btnExportExcel" class="btn btn-primary m-r-5 m-b-5">
        <s:text name="btnExportExcel"/>
        </button>
        -->

    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.identityList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="identityTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
<%--                            <th field="ck" checkbox="true"></th>--%>
                            <th><s:text name="phoneNo"/></th>
                            <th><s:text name="whaleliveId"/></th>
                            <th><s:text name="fullName"/></th>
                            <th><s:text name="identityNo"/></th>
                            <th><s:text name="icFront"/></th>
                            <th><s:text name="icBack"/></th>
                            <th><s:text name="kycStatus"/></th>
                            <th><s:text name="kycSubmitDate"/></th>
                            <th><s:text name="verifyRemark"/></th>
                            <th>&nbsp</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="statusModal" class="easyui-dialog" style="width:400px; height:300px" title="<s:text name="updateStatus"/>"
     closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">

        <s:form name="statusUpdateForm" id="statusUpdateForm" cssClass="form-horizontal">
            <div class="form-group">
                <label class="col-md-3 control-label"> <s:text name="status"/></label>
                <div class="col-md-9">
                    <s:select name="updateStatus" id="updateStatus" list="identityUpdateStatusCodeList"
                              label="%{getText('status')}" listKey="key" listValue="value" theme="simple"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"> <s:text name="remark"/></label>
                <div class="col-md-9">
                    <s:textfield theme="simple" name="remark" id="remark" size="50" maxlength="50"
                                 cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-9 col-md-offset-3">
                    <ce:buttonRow>
                        <button id="btnSave" type="submit" class="btn btn-primary">
                            <i class="icon-save"></i>
                            <s:text name="btnSave"/>
                        </button>
                    </ce:buttonRow>
                </div>
            </div>
        </s:form>
    </div>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true"
     resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>
