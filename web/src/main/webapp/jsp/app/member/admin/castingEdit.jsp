<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.guildEdit"/>

<script type="text/javascript">

</script>

<s:form  name="castingForm" id="castingForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:hidden name="casting.id" id="casting.id"/>

    <div class="row">
        <article class="col-md-12">
            <div class="well">
                <s:textfield name="casting.testLiveDate" id="casting.testLiveDate" label="Test Live Date"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.whaleLiveId" id="casting.whaleLiveId" label="Whalelive Id"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.fullName" id="casting.fullName" label="%{getText('name')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.identityNo" id="casting.identityNo" label="%{getText('identityNo')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.phoneNo" id="casting.phoneNo" label="%{getText('phoneNo')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.phoneModel" id="casting.phoneModel" label="Phone Model"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.age" id="casting.age" label="%{getText('age')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.email" id="casting.email" label="%{getText('email')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.location" id="casting.location" label="Location"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.url" id="casting.url" label="%{getText('url')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.fbId" id="casting.fbId" label="%{getText('fbId')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.igId" id="casting.igId" label="%{getText('igId')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.igFan" id="casting.igFan" label="Instagram Fans/Follower"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.tiktokId" id="casting.tiktokId" label="%{getText('tiktokId')}"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.tiktokFan" id="casting.tiktokFan" label="Tiktok Fans/Follower"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.weiboId" id="casting.weiboId" label="Weibo Id"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.weiboFan" id="casting.weiboFan" label="Weibo Fans/Follower"
                             cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="casting.talent" id="casting.talent" label="%{getText('talent')}"
                             cssClass="input-xxlarge" readonly="true"/>
            </div>
        </article>
    </div>

    <ce:buttonRow>
        <s:url var="urlExit" action="castingList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
