<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#memberForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                    // submitHandler
                });
            },
            rules: {
                "member.status": "required",
                "liveCategories": "required"
            }
        });

        if ($("#member\\.status option:selected").val() === 'INACTIVE') {
            $("#remark").css("visibility", "visible");
        }

    });

    function checkType() {
        if ($("#member\\.status option:selected").val() === 'INACTIVE') {
            $("#remark").css("visibility", "visible");

            if($("#member\\.remark").val() === '') {
                $("#member\\.remark").removeAttr("disabled");
            }
        } else {
            $("#remark").css("visibility", "hidden");


        }
    }
        function processJsonSave(json) {
            new JsonStat(json, {
                onSuccess : function(json) {
                    $("body").unmask();
                    messageBox.info(json.successMessage, function(){
                        window.location = "<s:url action="memberList"/>";
                    });
                },  // onFailure using the default
                onFailure : function(json, error){
                    $("body").unmask();
                    messageBox.alert(error);
                }
            });


        }
</script>

<sc:title title="title.memberEdit" />

<s:form action="memberUpdate" name="memberForm" id="memberForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <fieldset>
        <legend><s:text name="memberInformation"/></legend>
        <s:textfield name="member.memberCode" id="member.memberCode" label="%{getText('memberCode')}" disabled="false"/>
        <s:textfield name="member.memberDetail.profileName" id="member.memberDetail.profileName" label="%{getText('profileName')}" disabled="true"/>
        <s:textfield name="member.memberDetail.email" id="member.memberDetail.email" label="%{getText('email')}" disabled="true"/>
        <s:select name="member.memberDetail.gender" id="member.memberDetail.gender" label="%{getText('gender')}" list="genderList" listKey="key" listValue="value" disabled="true"/>
        <s:select name="member.memberDetail.countryCode" id="member.memberDetail.countryCode" label="%{getText('country')}" list="countryDescList" listKey="key" listValue="value" disabled="true"/>
        <s:textfield name="member.memberDetail.dob" id="member.memberDetail.dob" label="%{getText('dateOfBirth')}" disabled="true"/>
        <s:select name="member.status" id="member.status" list="statusList" label="%{getText('memberStatus')}" listKey="key" listValue="value" onchange="checkType()"/>


        <s:checkboxlist name="liveCategories" id="liveCategories" label="%{getText('liveStreamCategory')}" list="liveStreamCategoryList" listKey="key" listValue="value" cssClass="checkbox"/>

        <div class="form-group">
            <label class="col-sm-3 control-label"><s:text name="allowPrivate"/>: </label>
            <div class="col-sm-9">
                <s:if test="member.liveStreamPrivate">
                    <input type="checkbox" name="liveStreamPrivate" id="liveStreamPrivate" checked/>
                </s:if>
                <s:else>
                    <input type="checkbox" name="liveStreamPrivate" id="liveStreamPrivate"/>
                </s:else>
            </div>
        </div>

        <div class="form-group" id="remark" style="visibility: hidden">
            <div class="col-md-12">
                <s:textfield name="member.remark" id="member.remark" label="%{getText('remark')}" disabled="true"/>
            </div>
        </div>
        <ce:buttonRow>
            <s:hidden name="member.memberId"/>
            <button id="btnSave" type="submit" class="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </button>
            <s:url var="urlExit" action="memberList" />
            <ce:buttonExit url="%{urlExit}" type="button" class="btn" theme="simple">
                <i class="fa fa-times"></i>
                <s:text name="btnExit"/>
            </ce:buttonExit>
        </ce:buttonRow>
    </fieldset>
</s:form>
