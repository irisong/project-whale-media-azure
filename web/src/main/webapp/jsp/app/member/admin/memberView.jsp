<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.memberView" />

<s:form action="memberEdit" name="memberForm" id="memberForm" cssClass="form-horizontal">
    <fieldset>
    <legend><s:text name="memberInformation"/></legend>
    <s:textfield name="member.memberCode" id="member.memberCode" label="%{getText('memberCode')}" disabled="true"/>
    <s:textfield name="member.memberDetail.profileName" id="member.memberDetail.profileName" label="%{getText('profileName')}" disabled="true"/>
    <s:textfield name="member.memberDetail.email" id="member.memberDetail.email" label="%{getText('email')}" disabled="true"/>
    <s:select name="member.memberDetail.gender" id="member.memberDetail.gender" label="%{getText('gender')}" list="genderList" listKey="key" listValue="value" disabled="true"/>
    <s:select name="member.memberDetail.countryCode" id="member.memberDetail.countryCode" label="%{getText('country')}" list="countryDescList" listKey="key" listValue="value" disabled="true"/>
    <s:textfield name="member.memberDetail.dob" id="member.memberDetail.dob" label="%{getText('dateOfBirth')}" disabled="true"/>
    <s:select name="memberStatus" id="memberStatus" list="statusList" label="%{getText('memberStatus')}" listKey="key" listValue="value" disabled="true"/>
    <s:checkboxlist name="liveCategories" id="liveCategories" label="%{getText('liveStreamCategory')}" list="liveStreamCategoryList" listKey="key" listValue="value" cssClass="checkbox" disabled="true"/>

    <div class="form-group">
        <label class="col-sm-3 control-label"><s:text name="allowPrivate"/>: </label>
        <div class="col-sm-9">
            <s:if test="member.liveStreamPrivate">
                <input type="checkbox" name="liveStreamPrivate" id="liveStreamPrivate" checked disabled/>
            </s:if>
            <s:else>
                <input type="checkbox" name="liveStreamPrivate" id="liveStreamPrivate" disabled/>
            </s:else>
        </div>
    </div>

    <ce:buttonRow>
        <s:hidden name="member.memberId"/>
        <button id="btnEdit" type="submit" class="btn btn-warning">
            <i class="fa fa-pencil-square-o"></i>
            <s:text name="btnEdit"/>
        </button>
        <s:url var="urlExit" action="memberList"/>
        <ce:buttonExit url="%{urlExit}" type="button" class="btn" theme="simple">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
    </fieldset>
</s:form>