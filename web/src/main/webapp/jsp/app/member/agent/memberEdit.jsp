<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.memberAdd"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#memberForm").validate( {
            rules: {
                "member.identityType":{
                    required : true
                },
                "member.email":{
                    email : true
                }
            }
        });

    });
</script>

<s:form action="memberUpdate" name="memberForm" id="memberForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div class="widget-box transparent">
        <div class="widget-header">
            <h5><strong><s:text name="agentInformation"/></strong></h5>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('agentCode')}" readonly="true" size="20" maxlength="20"/>
                <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agentName')}" readonly="true" size="50" maxlength="100"/>
            </div>
        </div>
    </div>

    <div class="widget-box transparent">
        <div class="widget-header">
            <h5><strong><s:text name="memberInformation"/></strong></h5>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <s:textfield name="member.memberCode" id="member.memberCode" label="%{getText('memberCode')}" readonly="true" size="20" maxlength="20"/>
                <s:textfield name="member.firstName" id="member.firstName" label="%{getText('firstName')}" required="true" size="50" maxlength="100"/>
                <s:textfield name="member.lastName" id="member.lastName" label="%{getText('lastName')}" required="true" size="50" maxlength="100"/>

                <s:select name="member.identityType" id="member.identityType" label="%{getText('identityType')}" required="true" list="idTypes" listKey="key" listValue="value" />
                <s:textfield name="member.identityNo" id="member.identityNo" label="%{getText('identityNo')}" required="true" size="50" maxlength="50"/>

                <s:textfield name="member.email" id="member.email" label="%{getText('email')}" required="true" size="50" maxlength="50"/>

                <s:select name="member.nationalityCode" id="member.nationalityCode" label="%{getText('nationality')}" required="true" list="countries" listKey="countryCode" listValue="countryName" />
                <s:radio name="member.gender" id="member.gender" label="%{getText('gender')}" required="true" list="genders" listKey="key" listValue="value"/>
                <ce:datepicker name="member.dateOfBirth" id="member.dateOfBirth" label="%{getText('dateOfBirth')}"/>

                <div class="hr hr-dotted"></div>

                <s:textfield name="member.address1" id="member.address1" label="%{getText('address')}" required="true" size="100" maxlength="100" cssClass="input-xxlarge"/>
                <s:textfield name="member.address2" id="member.address2" size="100" maxlength="100" cssClass="input-xxlarge"/>
                <s:textfield name="member.address3" id="member.address3" size="100" maxlength="100" cssClass="input-xxlarge"/>

                <s:textfield name="member.state" id="member.state" label="%{getText('state')}" required="true" size="50" maxlength="50"/>
                <s:textfield name="member.postcode" id="member.postcode" label="%{getText('postcode')}" required="true" size="50" maxlength="50"/>
                <s:select name="member.countryCode" id="member.countryCode" label="%{getText('nationality')}" required="true" list="countries" listKey="countryCode" listValue="countryName" />

                <div class="hr hr-dotted"></div>
                <s:textfield name="member.phoneNo" id="member.phoneNo" label="%{getText('phoneNo')}" required="true" size="30" maxlength="30"/>
                <s:textarea name="member.remark" id="member.remark" label="%{getText('remark')}" cssClass="input-xxlarge"/>
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <s:hidden name="member.memberId"/>
        <s:submit type="button" id="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="memberList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>

</s:form>