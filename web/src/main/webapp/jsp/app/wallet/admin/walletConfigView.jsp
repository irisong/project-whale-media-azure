<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.walletConfiguration" />

<script type="text/javascript">
    function defaultJsonCallback(json){
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                location.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

    $(function () {
        $("#btnEditTransfer").click(function(event){
            <s:if test="%{walletConfig.walletTransfer}">
            var msg = "<s:text name="doYouWantToDisableWalletTransfer"/>";
            </s:if>
            <s:else>
            var msg = "<s:text name="doYouWantToEnableWalletTransfer"/>";
            </s:else>
            messageBox.confirm(msg, function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="walletConfigToggleTransfer"/>', defaultJsonCallback);
            });
        });

        $("#btnEditWithdraw").click(function(event){
            <s:if test="%{walletConfig.walletWithdraw}">
            var msg = "<s:text name="doYouWantToDisableWalletWithdraw"/>";
            </s:if>
            <s:else>
            var msg = "<s:text name="doYouWantToEnableWalletWithdraw"/>";
            </s:else>
            messageBox.confirm(msg, function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="walletConfigToggleWithdraw"/>', defaultJsonCallback);
            });
        });

        $(".btnEditWalletType").click(function(event){
            var walletType = $(this).parents("tr").attr("wallettype");
            $.getJSON("<s:url action="walletConfigGetWalletTypeInfo"/>",{
                walletType : walletType
            }, function(json){
                $("#walletType").val(json.walletType);
                $("#walletName").val(json.walletName);
                $("#walletCurrency").val(json.walletCurrency);
                $("#walletStatus").val(json.status);

                $("#editWalletTypeModal").dialog('open');
            });
        });

        $("#btnCancelEditWalletType").click(function(){
            $("#editWalletTypeModal").dialog('close');
        });

        $("#walletTypeForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "walletCurrency": {
                    required: true
                },
                "walletName": {
                    required: true
                }
            }
        });

        $("#btnEditTransferConfig").click(function(event){
            $("#editWalletTransferConfigModal").dialog('open');
        });

        $("#btnCancelEditWalletTransferConfig").click(function(){
            $("#editWalletTransferConfigModal").dialog('close');
        });

        $("#walletTransferConfigForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "walletTransferConfig.minimumAmount": {
                    number: true,
                    required: true,
                    min: 0
                },
                "walletTransferConfig.adminFee": {
                    number: true,
                    required: true,
                    min: 0
                }
            }
        });

        $("#btnAddTransferType").click(function(event){
            $("#walletTransferTypeConfigForm").attr("action", "<s:url action="walletConfigSaveWalletTransferTypeConfig"/>");
            $("#walletTransferTypeModal").dialog("open");
        });

        $("#btnCancelAddWalletTransferType").click(function(){
            $("#walletTransferTypeModal").dialog('close');
        });

        $("#walletTransferTypeConfigForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }
        });

        $(".btnEditWalletTransferType").click(function(event){
            var transferTypeId = $(this).parents("tr").attr("typeid");
            $.getJSON("<s:url action="walletConfigGetWalletTransferTypeConfigInfo"/>",{
                "transferTypeId" : transferTypeId
            }, function(json){
                $("#transferTypeId").val(transferTypeId);
                $("#walletTransferTypeConfig\\.transferSelf").val(json.walletTransferTypeConfig.transferSelf);
                $("#walletTransferTypeConfig\\.walletTypeFrom").val(json.walletTransferTypeConfig.walletTypeFrom);
                $("#walletTransferTypeConfig\\.walletTypeTo").val(json.walletTransferTypeConfig.walletTypeTo);
                $("#walletTransferTypeConfig\\.status").val(json.walletTransferTypeConfig.status);

                $("#walletTransferTypeConfigForm").attr("action", "<s:url action="walletConfigUpdateWalletTransferTypeConfig"/>");
                $("#walletTransferTypeModal").dialog('open');
            });
        });

        $(".btnDeleteWalletTransferType").click(function(event){
            var that = this;

            messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                var transferTypeId = $(that).parents("tr").attr("typeid");

                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post('<s:url action="walletConfigDeleteWalletTransferTypeConfig"/>', {
                    "transferTypeId" : transferTypeId
                }, defaultJsonCallback);
            });
        });

        $(".btnUpWalletTransferType").click(function(event){
            var transferTypeId = $(this).parents("tr").attr("typeid");

            $("body").loadmask("<s:text name="processing.msg"/>");
            $.post('<s:url action="walletConfigUpWalletTransferTypeConfig"/>', {
                "transferTypeId" : transferTypeId
            }, defaultJsonCallback);
        });

        $(".btnDownWalletTransferType").click(function(event){
            var transferTypeId = $(this).parents("tr").attr("typeid");

            $("body").loadmask("<s:text name="processing.msg"/>");
            $.post('<s:url action="walletConfigDownWalletTransferTypeConfig"/>', {
                "transferTypeId" : transferTypeId
            }, defaultJsonCallback);
        });

        $("#btnEditBlockchainProcess").click(function(event){
            <s:if test="blockchainProcess">
            var msg = "<s:text name="doYouWantToDisableBlockchainProcsss"/>";
            </s:if>
            <s:else>
            var msg = "<s:text name="doYouWantToEnableBlockchainProcess"/>";
            </s:else>
            messageBox.confirm(msg, function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="walletConfigToggleBlockchainProcess"/>', defaultJsonCallback);
            });
        });
    });
</script>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="wallet"/></legend>
            </fieldset>

            <dl class="dl-horizontal">
                <dt><s:text name="enableWalletWithraw"/></dt>
                <dd>
                    <s:if test="%{walletConfig.walletWithdraw}">
                        <i class="fa fa-check btn-success"></i>
                    </s:if>
                    <s:else>
                        <i class="fa fa-times btn-danger"></i>
                    </s:else>
                    <button id="btnEditWithdraw" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="change"/>
                    </button>
                </dd>
            </dl>

            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><s:text name="walletType"/></th>
                            <th><s:text name="name"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <s:iterator var="walletTypeConfig" value="walletTypeConfigs" status="iterStatus">
                            <tr wallettype="${walletTypeConfig.walletType}">
                                <th>${iterStatus.count}</th>
                                <td><s:property value="%{@AU@getWalletCurrency(#walletTypeConfig.walletType)}"/></td>
                                <td><s:property value="%{@AU@getWalletName(#walletTypeConfig.walletType)}"/></td>
                                <td><s:property value="%{@AU@getStatusDesc(#walletTypeConfig.status)}"/></td>
                                <td>
                                    <button class="btn btn-info btn-xs btnEditWalletType" type="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                        <s:text name="edit"/>
                                    </button>
                                </td>
                            </tr>
                        </s:iterator>

                        </tbody>
                    </table>
                </div>
            </div>

            <fieldset>
                <legend>
                    <s:text name="walletTransfer"/>

                    <button id="btnAddTransferType" class="btn btn-success btn-xs" type="button" style="float:right;margin-right: 10px;">
                        <i class="fa fa-plus-circle"></i>
                        <span><s:text name="addTransferType"/></span>
                    </button>
                </legend>
            </fieldset>

            <dl class="dl-horizontal">
                <dt><s:text name="enableWalletTransfer"/></dt>
                <dd>
                    <s:if test="%{walletConfig.walletTransfer}">
                        <i class="fa fa-check btn-success"></i>
                    </s:if>
                    <s:else>
                        <i class="fa fa-times btn-danger"></i>
                    </s:else>
                    <button id="btnEditTransfer" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="change"/>
                    </button>
                </dd>

                <dt><s:text name="minimumAmount"/></dt>
                <dd><s:property value="%{@SF@formatDecimal(walletTransferConfig.minimumAmount)}"/></dd>

                <dt><s:text name="adminFee"/> <br/>(<s:text name="transferToMember"/>) </dt>
                <dd>
                    <s:property value="%{@SF@formatDecimal(walletTransferConfig.adminFee)}"/>
                    <s:if test="walletTransferConfig.adminFeeByAmount">
                        <s:text name="perTransaction"/>
                    </s:if>
                    <s:else>
                        <s:text name="percentOfTransferAmount"/>
                    </s:else>

                    <button id="btnEditTransferConfig" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="edit"/>
                    </button>
                </dd>
            </dl>

            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><s:text name="transferType"/></th>
                            <th><s:text name="walletTypeFrom"/></th>
                            <th><s:text name="walletTypeTo"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>

                        <s:if test="walletTransferTypeConfigs.size()==0">
                            <tr><td colspan="6"><div class="alert alert-info"><s:text name="noRecordFound"/></div></td></tr>
                        </s:if>

                        <s:iterator var="walletTransferTypeConfig" value="walletTransferTypeConfigs" status="iterStatus">
                            <tr typeid="<s:property value="#walletTransferTypeConfig.transferTypeId"/>">
                                <th>${iterStatus.count}</th>
                                <td><s:text name="%{#walletTransferTypeConfig.transferSelf ? 'selfTransfer' : 'transferToMember'}"/></td>
                                <td><s:property value="%{@AU@getWalletNameWithCurrency(#walletTransferTypeConfig.walletTypeFrom)}"/></td>
                                <td><s:property value="%{@AU@getWalletNameWithCurrency(#walletTransferTypeConfig.walletTypeTo)}"/></td>
                                <td><s:property value="%{@AU@getStatusDesc(#walletTransferTypeConfig.status)}"/></td>
                                <td>
                                    <button class="btn btn-primary btn-xs btnUpWalletTransferType" type="button" ${iterStatus.first ? "disabled='true'" : ''} >
                                        <i class="fa fa-arrow-up"></i>
                                        <s:text name="up"/>
                                    </button>
                                    <button class="btn btn-success btn-xs btnDownWalletTransferType" type="button" ${iterStatus.last ? "disabled='true'" : ''} >
                                        <i class="fa fa-arrow-down"></i>
                                        <s:text name="down"/>
                                    </button>
                                    <button class="btn btn-info btn-xs btnEditWalletTransferType" type="button">
                                        <i class="fa fa-pencil-square-o"></i>
                                        <s:text name="btnEdit"/>
                                    </button>
                                    <button class="btn btn-danger btn-xs btnDeleteWalletTransferType" type="button">
                                        <i class="fa fa-trash-o"></i>
                                        <s:text name="btnDelete"/>
                                    </button>
                                </td>
                            </tr>
                        </s:iterator>

                        </tbody>
                    </table>
                </div>
            </div>

            <fieldset>
                <legend>
                    <s:text name="blockchainProcess"/>
                </legend>
            </fieldset>

            <dl class="dl-horizontal">
                <dt><s:text name="enableBlockchainProcess"/></dt>
                <dd>
                    <s:if test="%{blockchainProcess}">
                        <i class="fa fa-check btn-success"></i>
                    </s:if>
                    <s:else>
                        <i class="fa fa-times btn-danger"></i>
                    </s:else>
                    <button id="btnEditBlockchainProcess" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="change"/>
                    </button>
                </dd>
            </dl>

            <div class="row">
                <div class="col-md-8 col-sm-12">
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th>&nbsp;</th>
                            <th><s:text name="name"/></th>
                            <th><s:text name="status"/></th>
                        </tr>
                        </thead>
                        <tbody>

                        <s:iterator value="blockchainProcessMap" status="iterStatus">
                            <tr>
                                <th>${iterStatus.count}</th>
                                <td><s:property value="key"/></td>
                                <td>
                                    <s:if test="value">
                                        Running
                                    </s:if>
                                    <s:else>Stop</s:else>
                                </td>
                            </tr>
                        </s:iterator>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
</div>

<div id="editWalletTypeModal" class="easyui-dialog" style="width:500px; height:250px" title="<s:text name="walletType"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="walletConfigUpdateWalletType" cssClass="form-horizontal" name="walletTypeForm" id="walletTypeForm">
                <s:textfield name="walletCurrency" id="walletCurrency" label="%{getText('walletType')}"/>
                <s:textfield name="walletName" id="walletName" label="%{getText('name')}"/>
                <s:select name="status" id="walletStatus" label="%{getText('status')}" list="walletTypeStatusList" listKey="key" listValue="value"/>

                <input type="hidden" name="walletType" id="walletType"/>
                <ce:buttonRow>
                    <button id="btnSaveEditWalletType" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelEditWalletType" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="editWalletTransferConfigModal" class="easyui-dialog" style="width:500px; height:270px" title="<s:text name="walletTransfer"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="walletConfigUpdateWalletTransferConfig" cssClass="form-horizontal" name="walletTransferConfigForm" id="walletTransferConfigForm">
                <s:textfield name="walletTransferConfig.minimumAmount" id="walletTransferConfig.minimumAmount" label="%{getText('minimumAmount')}"/>
                <s:textfield name="walletTransferConfig.adminFee" id="walletTransferConfig.adminFee" label="%{getText('adminFee')}"/>
                <s:radio name="walletTransferConfig.adminFeeByAmount" id="walletTransferConfig.adminFeeByAmount" label="%{getText('adminFeeCalculation')}" list="adminFeeTypeList" listKey="key" listValue="value"/>

                <input type="hidden" name="walletType" id="walletType"/>
                <ce:buttonRow>
                    <button id="btnSaveEditWalletTransferConfig" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelEditWalletTransferConfig" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="walletTransferTypeModal" class="easyui-dialog" style="width:520px; height:320px" title="<s:text name="walletTransfer"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="walletConfigSaveWalletTransferTypeConfig" cssClass="form-horizontal" name="walletTransferTypeConfigForm" id="walletTransferTypeConfigForm">
                <s:radio name="walletTransferTypeConfig.transferSelf" id="walletTransferTypeConfig.transferSelf" label="%{getText('transferType')}" list="walletTransferTypeList" listKey="key" listValue="value"/>
                <s:select name="walletTransferTypeConfig.walletTypeFrom" id="walletTransferTypeConfig.walletTypeFrom" label="%{getText('walletTypeFrom')}" list="walletTypeList" listKey="key" listValue="value"/>
                <s:select name="walletTransferTypeConfig.walletTypeTo" id="walletTransferTypeConfig.walletTypeTo" label="%{getText('walletTypeTo')}" list="walletTypeList" listKey="key" listValue="value"/>
                <s:select name="walletTransferTypeConfig.status" id="walletTransferTypeConfig.status" label="%{getText('status')}" list="walletTransferTypeStatusList" listKey="key" listValue="value"/>
                <input type="hidden" name="transferTypeId" id="transferTypeId">
                <ce:buttonRow>
                    <button id="btnSaveAddWalletTransferType" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelAddWalletTransferType" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>