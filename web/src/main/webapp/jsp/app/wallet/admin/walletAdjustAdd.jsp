<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function processJsonSave(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="walletAdjustList"/>"; //go to page
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                messageBox.alert(error);
            }
        });
    }

    $(function(){
        $("#walletForm").validate({
            submitHandler : function(form) {
                var adjustType = $("input[name='walletAdjust.adjustType']:checked").val();

                if(adjustType === 'OUT' && $("#walletAdjust\\.remark").val().trim()==='') {
                    messageBox.alert("<s:text name="youMustEnterRemarkWhenYouAdjustOutAmount"/>");
                    return;
                }

                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "walletAdjust.adjustType": {
                    required: true
                },
                "walletAdjust.walletType": {
                    required: true
                },
                "walletAdjust.amount": {
                    number: true,
                    required: true,
                    min: 0
                }
            }
        });
    });
</script>

<sc:title title="title.walletAdjust" />

<sc:displayErrorMessage/>

<div class="row">
    <article class="col-md-6 col-sm-12">
        <sc:widget id="widget11" title="searchMember" cssClass="jarviswidget-color-blueLight">
            <s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
                <sc:memberTextField name="memberCode" required="true"/>

                <ce:buttonRow>
                    <button class="btn btn-primary" type="submit">
                        <i class="fa fa-angle-double-right"></i>
                        <s:text name="btnSubmit"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </sc:widget>
    </article>
</div>

<s:if test="memberExist">
    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <s:form action="walletAdjustSave" name="walletForm" id="walletForm" cssClass="form-horizontal">
                    <s:hidden name="member.memberId"/>

                    <fieldset>
                        <legend><s:text name="memberInformation"/></legend>
                    </fieldset>

                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <s:textfield name="member.memberCode" id="member.memberCode" label="%{getText('memberCode')}" readonly="true"/>
                            <s:textfield name="member.fullName" id="member.fullName" label="%{getText('name')}" readonly="true"/>
                        </div>
                        <div class="col-md-6 col-sm-12">
                            <s:textfield name="member.identityNo" id="member.identityNo" label="%{getText('identityNo')}" readonly="true"/>
                            <s:textfield name="memberDetail.email" id="memberDetail.email" label="%{getText('email')}" readonly="true"/>
                        </div>
                    </div>


                    <fieldset>
                        <legend><s:text name="walletAdjustmentInformation"/></legend>
                    </fieldset>

                    <s:radio name="walletAdjust.adjustType" id="walletAdjust.adjustType" label="%{getText('adjustmentType')}" list="adjustTypes" listKey="key" listValue="value"/>
                    <s:radio name="walletAdjust.walletType" id="walletAdjust.walletType" label="%{getText('walletType')}" list="walletTypes" listKey="key" listValue="value"/>
                    <s:textfield name="walletAdjust.amount" id="walletAdjust.amount" label="%{getText('amount')}"/>
                    <s:textarea name="walletAdjust.remark" id="walletAdjust.remark" label="%{getText('remark')}"/>

                    <ce:buttonRow>
                        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                            <i class="fa fa-hdd-o"></i>
                            <s:text name="btnSave"/>
                        </s:submit>
                        <s:url var="urlExit" action="walletAdjustList"/>
                        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                            <i class="fa fa-times"></i>
                            <s:text name="btnExit"/>
                        </ce:buttonExit>
                    </ce:buttonRow>
                </s:form>
            </div>
        </article>
    </div>
</s:if>
