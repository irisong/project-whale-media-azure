<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.walletAdjust" />

<script type="text/javascript">
    $(function(){
        $("#walletForm").validate({
            submitHandler: function(form) {
                $('#dg').datagrid('load',{
                    docno: $('#docno').val(),
                    memberCode: $('#memberCode').val(),
                    adjustType: $('#adjustType').val(),
                    walletType: $('#walletType').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val()
                });
            }
        });

        $("#dg").datagrid({
            onResizeColumn:function(field, width){
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '14%';  // reset the width. Total 7 columns (14%)
                $(this).datagrid('resize');
            }
        });

        $("#btnCreate").click(function(){
            $("#navForm").attr("action", "<s:url action="walletAdjustAdd" />");
            $("#navForm").submit();
        });
    });
</script>


<form id="navForm" method="post">
    <input type="hidden" name="user.userId" id="user.userId" />
</form>

<s:form name="walletForm" id="walletForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div id="searchPanel">
        <s:textfield name="docno" id="docno" label="%{getText('docno')}"/>
        <sc:memberTextField name="memberCode"/>
        <s:select name="adjustType" id="adjustType" label="%{getText('adjustmentType')}" list="allAdjustTypes" listKey="key" listValue="value"/>
        <s:select name="walletType" id="walletType" label="%{getText('walletType')}" list="allWalletTypes" listKey="key" listValue="value"/>
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="transactionDate"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" url="<s:url action="walletAdjustListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="trxDatetime" sortOrder="DESC">
        <thead>
        <tr>
            <th field="trxDatetime" width="130" sortable="true"><s:text name="transactionDate"/></th>
            <th field="docno" width="90" sortable="true"><s:text name="docno"/></th>
            <th field="ownerUsername" width="100" sortable="false"><s:text name="memberId"/></th>
            <th field="ownerName" width="180" ><s:text name="name"/></th>
            <th field="adjustType" width="110" sortable="true"><s:text name="adjustmentType"/></th>
            <th field="walletType" width="70" sortable="true" formatter="$.datagridUtil.formatWalletCurrency"><s:text name="walletType"/></th>
            <th field="amount" width="80" sortable="true"><s:text name="amount"/></th>
            <th field="remark" width="480" sortable="true"><s:text name="remark"/></th>
        </tr>
        </thead>
    </table>

</s:form>