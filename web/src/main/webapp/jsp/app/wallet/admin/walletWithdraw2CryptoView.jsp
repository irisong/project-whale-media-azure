<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="walletWithdraw2CryptoList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    var isApprove = true;
    $(function(){
        $("#walletForm").validate({
            submitHandler : function(form) {
                var msg = isApprove ? "<s:text name="promptApproveMsg"/>" : "<s:text name="promptRejectMsg"/>";

                messageBox.confirm(msg, function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });

        $("#btnApprove").click(function(event){
            isApprove = true;
            $("#status").val("APPROVED");
            $("#walletForm").submit();
        });

        $("#btnReject").click(function(event){
            isApprove = false;

            if($("#remark").val().trim()===''){
                messageBox.alert("<s:text name="youMustEnterRemarkWhenYouRejectIt"/>");
                return;
            }

            $("#status").val("REJECTED");
            $("#walletForm").submit();
        });
    });
</script>

<sc:title title="walletWithdrawToCrypto" />

<s:form name="walletForm" id="walletForm" cssClass="form-horizontal" action="walletWithdraw2CryptoApproveReject">
    <s:hidden name="walletExchange.exchangeId"/>
    <s:hidden name="status" id="status"/>

    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <fieldset>
                    <legend><s:text name="walletWithdrawalInformation"/></legend>
                </fieldset>

                <div class="row">
                    <div class="col-md-6">
                        <s:textfield name="walletExchange.ownerCode" id="walletExchange.ownerCode" label="%{getText('memberCode')}" disabled="true"/>
                        <s:textfield name="walletExchange.ownerName" id="walletExchange.ownerName" label="%{getText('memberName')}" disabled="true"/>
                        <s:textfield name="walletExchange.docno" id="walletExchange.docno" label="%{getText('docno')}" disabled="true"/>
                        <s:textfield name="walletExchange.trxDatetime" id="walletExchange.trxDatetime" label="%{getText('transactionDate')}" disabled="true"/>
                        <s:textfield name="walletExchange.processDatetime" id="walletExchange.processDatetime" label="%{getText('processingDate')}" disabled="true"/>
                        <s:textfield label="%{getText('walletTypeFrom')}" value="%{@AU@getWalletNameWithCurrency(walletExchange.walletTypeFrom)}" disabled="true"/>
                        <s:textfield label="%{getText('walletTypeTo')}" value="%{@AU@getWalletNameWithCurrency(walletExchange.walletTypeTo)}" disabled="true"/>
                    </div>
                    <div class="col-md-6">
                        <%--
                        <s:textfield name="walletExchange.withdrawTypeDesc" id="walletExchange.withdrawTypeDesc" label="%{getText('withdrawalType')}" disabled="true"/>
                        <s:if test="crytocurrency">
                            <s:textfield name="walletExchange.coinAddress" id="walletExchange.coinAddress" label="%{getText(crytoAddressLabel)}" disabled="true"/>
                        </s:if>
                        --%>
                        <s:textfield name="walletExchange.amount" id="walletExchange.amount" label="%{getText('withdrawAmount')}" disabled="true"/>
                        <s:textfield name="walletExchange.adminFee" id="walletExchange.adminFee" label="%{getText('adminFee')}" disabled="true"/>
                        <s:textfield name="walletExchange.totalAmount" id="walletExchange.totalAmount" label="%{getText('totalAmount')}" disabled="true"/>
                        <s:textfield label="%{getText('status')}" value="%{@AU@getStatusDesc(walletExchange.status)}" disabled="true"/>
                        <s:textfield name="walletExchange.rate" id="walletExchange.rate" label="%{getText('rate')}" disabled="true"/>
                        <s:textfield name="walletExchange.amountTo" id="walletExchange.amountTo" label="%{getText('amountReceived')}" disabled="true"/>

                    </div>
                </div>

                <s:if test="%{walletExchange.status=='PENDING'}">
                    <s:textarea name="remark" id="remark" label="%{getText('remark')}"/>
                </s:if>
                <s:if test="%{walletExchange.status=='REJECTED'}">
                    <s:textfield name="walletExchange.rejectRemark" id="walletExchange.rejectRemark" label="%{getText('rejectRemark')}" disabled="true"/>
                </s:if>
                <s:elseif test="%{walletExchange.status=='APPROVED'}">
                    <s:textarea name="walletExchange.remark" id="walletExchange.remark" label="%{getText('remark')}" disabled="true"/>
                </s:elseif>

                <ce:buttonRow>

                    <s:if test="%{walletExchange.status=='PENDING'}">
                        <button id="btnApprove" type="button" class="btn btn-warning">
                            <i class="fa fa-check-circle"></i>
                            <s:text name="btnApprove"/>
                        </button>
                        <button id="btnReject" type="button" class="btn btn-danger">
                            <i class="fa fa-ban"></i>
                            <s:text name="btnReject"/>
                        </button>
                    </s:if>

                    <s:url var="urlExit" action="walletWithdraw2CryptoList" />
                    <ce:buttonExit url="%{urlExit}" type="button" class="btn btn-primary" theme="simple">
                        <i class="fa fa-times"></i>
                        <s:text name="btnExit"/>
                    </ce:buttonExit>
                </ce:buttonRow>
            </div>
        </article>
    </div>
</s:form>
