<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.walletStatement" />

<script type="text/javascript">

$(function(){
    $("#searchForm").validate({
        rules: {
            "memberCode": {
                required: true
            }
        }
    });
}); // end $(function())

</script>

<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <sc:memberTextField name="memberCode"/>

        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
        <s:select name="walletType" id="walletType" label="%{getText('walletType')}" required="true" list="walletTypes" listKey="key" listValue="value" />

        <s:radio name="showDescEn" id="showDescEn" label="%{getText('showDescEn')}" list="showDescOptions" listKey="key" listValue="value"/>
        <s:radio name="showRemark" id="showRemark" label="%{getText('showRemark')}" list="showRemarkOptions" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<s:if test="memberExist">

    <script type="text/javascript">
        $(function(){
            $('#walletTable').DataTable({
                "ordering": false,
                "ajax": {
                    "url" : "<s:url action="walletListDatatables"/>",
                    "data" : {
                        "memberCode" : "<s:property value="memberCode"/>",
                        walletType: $('#walletType').val(),
                        dateFrom: $('#dateFrom').val(),
                        dateTo: $('#dateTo').val()
                    }
                },
                "columns" : [
                    {"data" : "trxDatetime"},
                    {
                        "data" : "inAmt",
                        className: "dt-body-right",
                        render: $.fn.dataTable.render.number(',', '.', 8)
                    },
                    {
                        "data" : "outAmt",
                        className: "dt-body-right",
                        render: $.fn.dataTable.render.number(',', '.', 8)
                    },
                    {
                        "data" : "balance",
                        className: "dt-body-right",
                        render: $.fn.dataTable.render.number(',', '.', 8)
                    },
                    <s:if test="showDescEn">
                        {"data" : "trxDesc"}
                    </s:if>
                    <s:else>
                    {"data" : "trxDescCn"}
                    </s:else>
                    <s:if test="showRemark">
                    ,{"data" : "remark"}
                    </s:if>
                ]
            });
        });
    </script>

    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <fieldset><legend><s:text name="title.walletStatement"/></legend></fieldset>

                <div class="row">
                    <article class="col-md-6 col-sm-12">
                        <dl class="dl-horizontal">
                            <dt><s:text name="memberId"/></dt>
                            <dd>${member.memberCode}</dd>
                            <dt><s:text name="fullname"/></dt>
                            <dd>${member.fullName}</dd>
                            <dt><s:text name="ranking"/></dt>
                            <dd><s:property value="%{member.bonusRank.rankName}"/></dd>
                            <dt><s:text name="dateJoin"/></dt>
                            <dd><s:property value="%{@SF@formatDate(member.joinDate)}"/></dd>
                        </dl>
                    </article>

                    <article class="col-md-6 col-sm-12">
                        <dl class="dl-horizontal">
                            <s:iterator var="balance" value="walletBalances" status="iterStatus">
                                <dt>${walletTypes[iterStatus.index].value}</dt>
                                <dd><s:property value="%{@SF@formatCrypto(#balance)}"/></dd>
                            </s:iterator>
                        </dl>
                    </article>
                </div>

                <div class="row margin-top-5">
                    <article class="col-md-12 col-sm-12">
                        <table id="walletTable" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th><s:text name="date"/></th>
                                    <th><s:text name="inAmt"/></th>
                                    <th><s:text name="outAmt"/></th>
                                    <th><s:text name="balance"/></th>
                                    <th><s:text name="description"/></th>
                                    <s:if test="showRemark">
                                        <th><s:text name="remark"/></th>
                                    </s:if>
                                </tr>
                            </thead>
                        </table>
                    </article>
                </div>
            </div>
        </article>
    </div>
</s:if>
