<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="walletWithdrawToCrypto" />

<script type="text/javascript">
    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="walletWithdraw2CryptoList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    function clickOnBulkApproveRejectButton(){
        var selectedRows = table.column(0).checkboxes.selected();
        if(!selectedRows.length){
            messageBox.alert("<s:text name="noRecordSelected"/>");
            return;
        }

        var errorMessage = "<s:text name="theFollowingWalletWithdrawlXCantBeApproved"/><br/>";

        var hasError = false;
        selectedIds = []; // clear it
        $.each(selectedRows, function(index, rowId){
            var obj = table.row("#"+rowId).data();
            if(obj.status !== 'PENDING'){
                errorMessage += obj.docno + "</br>";
                hasError = true;
            }
            selectedIds.push(rowId);
        });

        if(hasError){
            messageBox.alert(errorMessage);
            return;
        }

        openRemarkWindow();
    }

    function openRemarkWindow(){
        if(isApprove){
            $("#remarkModal").dialog("setTitle", "<s:text name="btnApprove"/>");
        }else{
            $("#remarkModal").dialog("setTitle", "<s:text name="btnReject"/>");
        }

        $("#remark").val("");
        $("#remarkModal").dialog("open");
    }

    var table;
    var isApprove = true;
    var selectedIds = [];

    $(function(){
        table = $('#walletTable').DataTable({
            "order": [[1, "desc"]],
            "ajax": {
                "url" : "<s:url action="walletWithdraw2CryptoListDatatables"/>",
                "data" : {
                    docno: $("#docno").val(),
                    memberCode: $("#memberCode").val(),
                    walletType: $('#walletType').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    processDateFrom: $('#processDateFrom').val(),
                    processDateTo: $('#processDateTo').val(),
                    withdrawType: $("#withdrawType").val(),
                    status: $("#status").val()
                }
            },
            "columns" : [
                {
                    "data" : "exchangeId",
                    "checkboxes": { "selectRow":false}
                },
                {"data" : "trxDatetime"},
                {"data" : "processDatetime"},
                {
                    "data" : "docno",
                    "orderable": false
                },
                {
                    "data" : "ownerCode",
                    "orderable": false
                },
                {
                    "data" : "ownerName",
                    "orderable": false
                },
                {
                    "data" : "cryptoTypeFrom",
                    "orderable": false
                },
                {
                    "data" : "cryptoTypeTo",
                    "orderable": false
                },
                {
                    "data" : "amount",
                    "orderable": false
                },
                {
                    "data" : "amountTo"
                },
                {
                    "data" : "statusDesc",
                    "orderable": false
                },
                {
                    "data" : "status",
                    "className": "text-nowrap",
                    "render": function(data, type, row){
                        var result = '<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';

                        if(data === 'PENDING') {
                            result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnApprove"><s:text name="btnApprove"/></button>';
                            result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnReject"><s:text name="btnReject"/></button>';
                        }

                        return result;
                    }
                }
            ],
            "rowId": "exchangeId",
            "columnDefs" : [
            ],
            "select": {
                "style": "multi"
            }
        });

        $(document).on("click", ".btnView", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#walletExchange\\.exchangeId").val(id);
            $("#navForm").attr("action", "<s:url action="walletWithdraw2CryptoView"/>").submit();
        });

        $(document).on("click", ".btnApprove", function(event){
            isApprove = true;
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            openRemarkWindow();
        });

        $(document).on("click", ".btnReject", function(event){
            isApprove = false;
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            openRemarkWindow();
        });

        $("#btnApprove").click(function(event){
            isApprove = true;
            clickOnBulkApproveRejectButton();
        });

        $("#btnReject").click(function(event){
            isApprove = false;
            clickOnBulkApproveRejectButton();
        });

        $("#btnSaveRemark").click(function(event){
            var remark = $("#remark").val();

            if(!isApprove && remark.trim() === ''){
                messageBox.alert("<s:text name="youMustEnterRemarkWhenYouRejectIt"/>");
                return;
            }

            var msg = isApprove ? "<s:text name="promptApproveMsg"/>" : "<s:text name="promptRejectMsg"/>";
            messageBox.confirm(msg, function(){
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post("<s:url action="walletWithdraw2CryptoBulkApproveReject"/>", {
                   "exchangeIds" : selectedIds.toString(),
                    "status" : isApprove ? "APPROVED" : "REJECTED",
                    "remark" : $("#remark").val()
                }, processJsonSave);
            });
        });

        $("#btnCancelRemark").click(function(event){
            $("#remarkModal").dialog('close');
        });
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="walletExchange.exchangeId" id="walletExchange.exchangeId" />
</form>

<s:form name="walletForm" id="walletForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div id="searchPanel">
        <s:textfield name="docno" id="docno" label="%{getText('docno')}"/>
        <sc:memberTextField name="memberCode"/>
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="transactionDate"/>
        <sc:datepickerFromTo nameFrom="processDateFrom" nameTo="processDateTo" labelFrom="processingDate"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"/>
        <sc:submitData/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <!--
        <button id="btnApprove" type="button" class="btn btn-warning">
            <i class="fa fa-check-circle"></i>
            <s:text name="btnApprove"/>
        </button>
        <button id="btnReject" type="button" class="btn btn-danger">
            <i class="fa fa-ban"></i>
            <s:text name="btnReject"/>
        </button>
        -->
    </ce:buttonRow>
</s:form>

<div class="row margin-top-5">
    <article class="col-md-12 col-sm-12">
        <div class="table-responsive">
            <table id="walletTable" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="text-align-center"><s:text name="date"/></th>
                    <th class="text-align-center"><s:text name="processingDate"/></th>
                    <th class="text-align-center"><s:text name="docno"/></th>
                    <th class="text-align-center"><s:text name="memberCode"/></th>
                    <th class="text-align-center"><s:text name="memberName"/></th>
                    <th class="text-align-center"><s:text name="currencyFrom"/></th>
                    <th class="text-align-center"><s:text name="currencyTo"/></th>
                    <th class="text-align-center"><s:text name="withdrawAmount"/></th>
                    <th class="text-align-center"><s:text name="amountReceived"/></th>
                    <th class="text-align-center"><s:text name="status"/></th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>

<div id="remarkModal" class="easyui-dialog" style="width:500px; height:200px; display: none;" title="<s:text name="remark"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form cssClass="form-horizontal">
                <s:textarea name="remark" id="remark" label="%{getText('remark')}"/>

                <ce:buttonRow>
                    <button id="btnSaveRemark" class="btn btn-primary" type="button">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelRemark" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>
