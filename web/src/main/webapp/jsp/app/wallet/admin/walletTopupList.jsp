<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.walletTopup"/></h1>
</div>

<script type="text/javascript">
    $(function(){
        $('#walletTable').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url" : "<s:url action="walletTopupListDatatables"/>",
                "data" : {
                    docno: $("#docno").val(),
                    memberCode: $("#memberCode").val(),
                    walletType: $('#walletType').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    status: $("#status").val()
                }
            },
            "columns" : [
                {"data" : "trxDatetime"},
                {"data" : "docno"},
                {"data" : "ownerCode"},
                {"data" : "ownerName"},
                {"data" : "walletType"},
                {"data" : "crytocurrencyRate"},
                {"data" : "crytocurrencyAmount"},
                {"data" : "amount"},
                {"data" : "status"}
            ],
            "rowId": "topupId",
            "columnDefs" : [
                { "orderable": false, "targets": [2, 3] },
                {
                    "render": function(data, type, row){
                        return $.datagridUtil.formatWalletCurrencyWithName(data);
                    }, "targets": 4
                },
                {
                    "render": function(data, type, row){
                        return $.datagridUtil.formatStatus(data);
                    }, "targets": 8
                }
            ]
        });
    });
</script>

<s:form name="walletForm" id="walletForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <div id="searchPanel">
        <s:textfield name="docno" id="docno" label="%{getText('docno')}"/>
        <sc:memberTextField name="memberCode"/>
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="transactionDate"/>
        <s:select name="walletType" id="walletType" label="%{getText('walletType')}" list="allWalletTypes" listKey="key" listValue="value"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="allStatusList" listKey="key" listValue="value"/>
        <sc:submitData/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row margin-top-5">
    <article class="col-md-12 col-sm-12">
        <div class="table-responsive">
            <table id="walletTable" class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                <tr>
                    <th class="text-align-center"><s:text name="date"/></th>
                    <th class="text-align-center"><s:text name="docno"/></th>
                    <th class="text-align-center"><s:text name="memberCode"/></th>
                    <th class="text-align-center"><s:text name="memberName"/></th>
                    <th class="text-align-center"><s:text name="walletType"/></th>
                    <th class="text-align-center"><s:text name="bitcoinRate"/></th>
                    <th class="text-align-center"><s:text name="bitcoinAmount"/></th>
                    <th class="text-align-center"><s:text name="amount"/></th>
                    <th class="text-align-center"><s:text name="status"/></th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>