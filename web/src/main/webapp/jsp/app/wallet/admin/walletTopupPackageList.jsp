<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.walletTopupPackage" />

<script type="text/javascript">
    var action = "list";
    $(function(){
        $("#btnSave").click(function(){
            if ($('#walletTopupPackage\\.walletType').val().trim().length === 0) {
                messageBox.alert("<s:text name="walletType.required"/>");
                return;
            }
            else if ($('#walletTopupPackage\\.packageAmount').val().trim().length === 0) {
                messageBox.alert("<s:text name="packageAmount.required"/>");
                return;
            }
            else if ($('#walletTopupPackage\\.packageAmount').val() === '0') {
                messageBox.alert("<s:text name="minimumWalletToupPackageIsX"> <s:param >1</s:param> </s:text>");
                return;
            }
            else if ($('#walletTopupPackage\\.status').val().trim().length === 0) {
                messageBox.alert("<s:text name="status.required"/>");
                return;
            }

            action = "save";
            $("#walletPackageForm").submit();
        });

        $("#walletPackageForm").validate({
            submitHandler: function(form) {
                if (action === 'list') {
                    $('#dg').datagrid('load',{
                        walletType: $('#walletTopupPackage\\.walletType').val(),
                        packageAmount: $('#walletTopupPackage\\.packageAmount').val(),
                        status: $('#walletTopupPackage\\.status').val()
                    })
                } else {
                    messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                        $("body").loadmask("<s:text name="processing.msg"/>");

                        $(form).ajaxSubmit({
                            dataType: 'json',
                            success: processJsonSave
                        });
                    });
                }
            }
        });
    });

    function processJsonSave(json){
        $("body").unmask();

        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function(){
                    window.location.reload(); // refresh page to return empty form
                });
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>


<form id="navForm" method="post">
    <input type="hidden" name="user.userId" id="user.userId" />
</form>

<s:form name="walletPackageForm" id="walletPackageForm" cssClass="form-horizontal" action="walletTopupPackageSave" >
    <sc:displayErrorMessage align="center" />

    <div id="searchPanel">
        <s:select name="walletTopupPackage.walletType" id="walletTopupPackage.walletType" label="%{getText('walletType')}" list="allWalletTypes" listKey="key" listValue="value"/>
        <s:textfield name="walletTopupPackage.packageAmount" id="walletTopupPackage.packageAmount" label="%{getText('packageAmount')}"/>
        <s:select name="walletTopupPackage.status" id="walletTopupPackage.status" label="%{getText('status')}" value="" list="allStatus" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnSave" type="button" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnSave"/>
        </button>
    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" url="<s:url action="walletTopupPackageListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeUpdate" sortOrder="DESC">
        <thead>
        <tr>
            <th field="walletType" width="12%" sortable="true" formatter="$.datagridUtil.formatWalletCurrency"><s:text name="walletType"/></th>
            <th field="packageAmount" width="12%" sortable="true"><s:text name="packageAmount"/></th>
            <th field="fiatCurrency" width="12%" sortable="true"><s:text name="fiatCurrency"/></th>
            <th field="fiatCurrencyAmount" width="12%" sortable="true"><s:text name="fiatCurrencyAmount"/></th>
            <th field="status" width="52%" sortable="true"><s:text name="status"/></th>
        </tr>
        </thead>
    </table>

</s:form>