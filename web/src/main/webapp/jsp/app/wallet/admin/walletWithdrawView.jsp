<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="walletWithdrawList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    var isApprove = true;
    $(function(){
        $("#walletForm").validate({
            submitHandler : function(form) {
                var msg = isApprove ? "<s:text name="promptApproveMsg"/>" : "<s:text name="promptRejectMsg"/>";

                messageBox.confirm(msg, function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });

        $("#btnApprove").click(function(event){
            isApprove = true;
            $("#status").val("APPROVED");
            $("#walletForm").submit();
        });

        $("#btnReject").click(function(event){
            isApprove = false;

            if($("#remark").val().trim()===''){
                messageBox.alert("<s:text name="youMustEnterRemarkWhenYouRejectIt"/>");
                return;
            }

            $("#status").val("REJECTED");
            $("#walletForm").submit();
        });
    });
</script>

<%--<sc:title title="walletWithdrawal" />--%>
<sc:title title="walletTransfer" />

<s:form name="walletForm" id="walletForm" cssClass="form-horizontal" action="walletWithdrawApproveReject">
    <s:hidden name="walletWithdraw.withdrawId"/>
    <s:hidden name="status" id="status"/>

    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <fieldset>
                    <%--<legend><s:text name="walletWithdrawalInformation"/></legend>--%>
                    <legend><s:text name="walletTransferInformation"/></legend>
                </fieldset>

                <div class="row">
                    <div class="col-md-6">
                        <s:textfield name="walletWithdraw.ownerCode" id="walletWithdraw.ownerCode" label="%{getText('memberCode')}" disabled="true"/>
                        <s:textfield name="walletWithdraw.ownerName" id="walletWithdraw.ownerName" label="%{getText('memberName')}" disabled="true"/>
                        <s:textfield name="walletWithdraw.docno" id="walletWithdraw.docno" label="%{getText('docno')}" disabled="true"/>
                        <s:textfield name="walletWithdraw.trxDatetime" id="walletWithdraw.trxDatetime" label="%{getText('transactionDate')}" disabled="true"/>
                        <s:textfield name="walletWithdraw.processDatetime" id="walletWithdraw.processDatetime" label="%{getText('processingDate')}" disabled="true"/>
                        <s:textfield label="%{getText('walletType')}" value="%{@AU@getWalletNameWithCurrency(walletWithdraw.walletType)}" disabled="true"/>
                    </div>
                    <div class="col-md-6">
                        <s:textfield name="walletWithdraw.withdrawTypeDesc" id="walletWithdraw.withdrawTypeDesc" label="%{getText('withdrawalType')}" disabled="true"/>
                        <s:if test="crytocurrency">
                            <s:textfield name="walletWithdraw.coinAddress" id="walletWithdraw.coinAddress" label="%{getText(crytoAddressLabel)}" disabled="true"/>
                        </s:if>
                        <s:textfield name="walletWithdraw.amount" id="walletWithdraw.amount" label="%{getText('withdrawAmount')}" disabled="true"/>
                        <s:textfield name="walletWithdraw.adminFee" id="walletWithdraw.adminFee" label="%{getText('adminFee')}" disabled="true"/>
                        <s:textfield name="walletWithdraw.totalAmount" id="walletWithdraw.totalAmount" label="%{getText('totalAmount')}" disabled="true"/>
                        <s:textfield label="%{getText('status')}" value="%{@AU@getStatusDesc(walletWithdraw.status)}" disabled="true"/>
                    </div>
                </div>

                <s:if test="%{walletWithdraw.status=='PENDING'}">
                    <s:textarea name="remark" id="remark" label="%{getText('remark')}"/>
                </s:if>
                <s:if test="%{walletWithdraw.status=='REJECTED'}">
                    <s:textfield name="walletWithdraw.rejectRemark" id="walletWithdraw.rejectRemark" label="%{getText('rejectRemark')}" disabled="true"/>
                </s:if>
                <s:elseif test="%{walletWithdraw.status=='APPROVED'}">
                    <s:textarea name="walletWithdraw.remark" id="walletWithdraw.remark" label="%{getText('remark')}" disabled="true"/>
                </s:elseif>

                <ce:buttonRow>

                    <s:if test="%{walletWithdraw.status=='PENDING'}">
                        <button id="btnApprove" type="button" class="btn btn-warning">
                            <i class="fa fa-check-circle"></i>
                            <s:text name="btnApprove"/>
                        </button>
                        <button id="btnReject" type="button" class="btn btn-danger">
                            <i class="fa fa-ban"></i>
                            <s:text name="btnReject"/>
                        </button>
                    </s:if>

                    <s:url var="urlExit" action="walletWithdrawList" />
                    <ce:buttonExit url="%{urlExit}" type="button" class="btn btn-primary" theme="simple">
                        <i class="fa fa-times"></i>
                        <s:text name="btnExit"/>
                    </ce:buttonExit>
                </ce:buttonRow>
            </div>
        </article>
    </div>
</s:form>
