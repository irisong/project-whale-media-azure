<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.walletStatement"/></h1>
</div>

<script type="text/javascript">

$(function(){
    //$("searchForm").submit()

    $('#dg').datagrid('load',{
        agentCode: $('#agentCode').val(),
        walletType: $('#walletType').val(),
        dateFrom: $('#dateFrom').val(),
        dateTo: $('#dateTo').val()
    });
}); // end $(function())

</script>

<s:form name="searchForm" id="searchForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <div class="control-group">
            <label class="control-label" for="dateFrom"><s:text name="date"/>:</label>
            <div class="controls controls-row">

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateFrom" id="dateFrom"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>

                <label class="inline" for="dateTo"><s:text name="to"/></label>

                <div class="input-append">
                    <ce:datepicker theme="simple" name="dateTo" id="dateTo"/>
                    <div class="add-on">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
            </div>
        </div>

        <s:select name="walletType" id="walletType" label="%{getText('walletType')}" required="true" list="walletTypes" listKey="key" listValue="value" />
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="icon-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<s:if test="agentExist">
    <table id="dg" class="easyui-datagrid" style="width:700px;height:250px"
           url="<s:url action="walletListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="trxDatetime" sortOrder="desc">
        <thead>
        <tr>
            <th field="trxDatetime" width="150" sortable="true"><s:text name="date"/></th>
            <th field="inAmt" width="130" sortable="true"><s:text name="inAmt"/></th>
            <th field="outAmt" width="130" sortable="true"><s:text name="outAmt"/></th>
            <th field="remark" width="300" sortable="true"><s:text name="remark"/></th>
        </tr>
        </thead>
    </table>
</s:if>