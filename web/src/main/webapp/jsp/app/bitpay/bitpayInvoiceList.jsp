<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.bitpayInvoices"/>

<script type="text/javascript">
    $(function() {
        var table = $('#bitpayInvoiceListTable').DataTable({
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url" : "<s:url action="bitpayInvoiceListDatatables"/>",
                "data" : {
                    sdkInvoiceId : $('#sdkInvoiceId').val(),
                    createDateFrom : $('#createDateFrom').val(),
                    createDateTo: $('#createDateTo').val(),
                    updateDateFrom : $('#updateDateFrom').val(),
                    updateDateTo : $('#updateDateTo').val(),
                    status : $('#status').val(),
                    invoiceType: $("#invoiceType").val()
                }
            },
            "columns" : [
                {"data" : "datetimeAdd"},
                {"data" : "datetimeUpdate"},
                {"data" : "id"},
                {"data" : "invoiceType"},
                {"data" : "price"},
                {"data" : "rate"},
                {"data" : "btcPrice"},
                {"data" : "status"},
                {"data" : "dashboardPaymentUrl"}
            ],
            "rowId": 'invoiceId',
            "columnDefs" : [
                {
                    "className": "text-nowrap",
                    "orderable": false,
                    "render": function(data, type, row){
                        var result = '<a href="' + data + '" target="_blank"><button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';
                        return result;
                    }, "targets": 8
                }
            ]
        });
    });
</script>

<s:form cssClass="form-horizontal">
    <s:textfield name="sdkInvoiceId" id="sdkInvoiceId" label="%{getText('invoiceId')}"/>
    <sc:datepickerFromTo nameFrom="createDateFrom" nameTo="createDateTo" labelFrom="creationDate"/>
    <sc:datepickerFromTo nameFrom="updateDateFrom" nameTo="updateDateTo" labelFrom="lastUpdate"/>
    <s:select name="invoiceType" id="invoiceType" list="allInvoiceTypes" label="%{getText('invoiceType')}" listKey="key" listValue="value"/>
    <s:select name="status" id="status" list="allStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.bitpayInvoices"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="bitpayInvoiceListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="creationDate"/></th>
                            <th><s:text name="lastUpdate"/></th>
                            <th><s:text name="id"/></th>
                            <th><s:text name="invoiceType"/></th>
                            <th><s:text name="price"/></th>
                            <th><s:text name="rate"/></th>
                            <th><s:text name="btcPrice"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>