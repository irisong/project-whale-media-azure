<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.announcementAdd" />

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function() {
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#btnAdd").click(function(){
            window.location = "<s:url action="announceAddLanguage"/>?announcement.announceId="+$('#announcement\\.announceId').val()
                +"&languageCode="+$('#chooseLanguage').val()+"&blnChooseLanguage=false&blnAddLanguage=true";
        });

        $("#btnExit").click(function(){
            $.ajax({
                url: "<s:url action="announceAddExit" namespace="/app/notice"/>?announcement.announceId="+$('#announcement\\.announceId').val(),
                success: proceedExit
            });


        });

        $("#announceForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "announcement.title": "required",
                "userGroups": "required",
                "announcement.publishDate": "required",
                "announcement.status": "required",
                "announcement.announcementType": "required",
                "announcement.body": "required"
            }
        });

        //change textarea to ckEditor
        $('#announcement\\.body').ckeditor({height:300});
        $('#addLanguageBody').ckeditor({height:300});
    });

    function proceedExit() {
        window.location = "<s:url action="announceList"/>";
    }

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="announceAddLanguage"/>?announcement.announceId="+json.announcementId
                        +"&languageCode="+json.languageCode+"&blnChooseLanguage="+json.blnChooseLanguage;
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="announceSave" name="announceForm" id="announceForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:hidden name="announcement.announceId" id="announcement.announceId"/>
    <s:hidden name="languageCode" id="currentLanguage"/>

    <s:if test="languageCode=='en'">
        <div class="row">
            <article class="col-md-12">
                <s:textfield name="announcement.title" id="announcement.title" label="%{getText('title')}" cssClass="input-xxlarge"/>
                <s:checkboxlist name="userGroups" id="userGroups" label="%{getText('publishGroup')}" list="publishGroups" listKey="key" listValue="value" cssClass="checkbox style-0"/>
                <ce:datepicker name="announcement.publishDate" id="announcement.publishDate" label="%{getText('publishDate')}" cssStyle="z-index: 100 !important;"/>
                <s:select name="languageCode" id="languageEn" label="%{getText('language')}" list="languages" listKey="key" listValue="value" disabled="true"/>
                <s:radio name="announcement.status" id="announcement.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
                <s:radio name="announcement.announcementType" id="announcement.announcementType" label="%{getText('announcementType')}" list="announcementTypes" listKey="key" listValue="value"/>
                <s:textarea name="announcement.body" id="announcement.body" theme="simple"/>
            </article>
        </div>
    </s:if>

    <s:elseif test="blnChooseLanguage">
        <div class="row">
            <article class="col-md-12">
                <s:label for="%{getText('Add Announcement for Other Language')}" />
                 <s:select name="languageCode" id="chooseLanguage" label="%{getText('otherLanguage')}" list="languages" listKey="key" listValue="value"/>
            </article>
        </div>
    </s:elseif>

    <s:elseif test="blnAddLanguage">
        <div class="row">
            <article class="col-md-12">
                <s:textfield name="addLanguageTitle" id="addLanguageTitle" label="%{getText('title')}" cssClass="input-xxlarge" required="true"/>
                <s:checkboxlist name="userGroups" id="userGroupsDisplay" label="%{getText('publishGroup')}" list="publishGroups" listKey="key" listValue="value" cssClass="checkbox style-0" disabled="true"/>
                <ce:datepicker name="announcement.publishDate" id="publishDateDisplay" label="%{getText('publishDate')}" cssStyle="z-index: 100 !important;" disabled="true"/>
                <s:select name="languageCode" id="languageCode" label="%{getText('language')}" list="languages" listKey="key" listValue="value" disabled="true"/>
                <s:radio name="announcement.status" id="statusDisplay" label="%{getText('status')}" list="statuses" listKey="key" listValue="value" disabled="true"/>
                <s:radio name="announcement.announcementType" id="announcementTypeDisplay" label="%{getText('announcementType')}" list="announcementTypes" listKey="key" listValue="value" disabled="true"/>
                <s:textarea name="addLanguageBody" id="addLanguageBody" theme="simple" required="true"/>
            </article>
        </div>
    </s:elseif>

    <ce:buttonRow>
        <s:if test="blnChooseLanguage">
            <button id="btnAdd" type="button" class="btn btn-success">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnAdd"/>
            </button>
        </s:if>
        <s:else>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>
        </s:else>

        <button id="btnExit" type="button" class="btn">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </button>
    </ce:buttonRow>
</s:form>
