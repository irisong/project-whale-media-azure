<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');

        $("#imageViewerModal").dialog('open').dialog('vcenter');
        $("#imageViewer").attr('src', fileUrl);
    }
</script>

<sc:title title="title.memberReportView"/>

<div class="row">
    <article class="col-sm-12">
        <div class="well">
            <s:form action="memberReport" name="memberReportViewForm" id="memberReportViewForm" cssClass="form-horizontal">
                <fieldset>
                    <s:hidden name="reportId"></s:hidden>
                    <s:textfield name="memberReportType" id="memberReportType" label="%{getText('type')}" readonly="true"/>
                    <s:textfield name="reportedMember.memberCode" id="reportedMember.memberCode" label="%{getText('member')}" readonly="true"/>
                    <s:textfield name="userMember.memberCode" id="userMember.memberCode" label="%{getText('reportBy')}" readonly="true"/>
                    <s:textarea name="memberReport.description" id="memberReport.description" label="%{getText('description')}" readonly="true"/>
                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="attachments"/>:</label>
                        <s:iterator var="imageFileView" value="memberReport.memberReportFiles">
                            <div class="col-md-9 col-sm-9">
                                <a onclick="viewImage('${imageFileView.fileUrl}')"><img src="<c:url value="/images/fileopen.png"/>" alt="attach_image"/></a>
                            </div>
                        </s:iterator>
                    </div>

                    <ce:buttonRow>
                        <s:url var="urlExit" action="memberReportList"/>
                        <ce:buttonExit url="%{urlExit}" type="button" class="btn" theme="simple">
                            <i class="fa fa-times"></i>
                            <s:text name="btnExit"/>
                        </ce:buttonExit>
                    </ce:buttonRow>
                </fieldset>
            </s:form>
        </div>
    </article>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true" resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>
