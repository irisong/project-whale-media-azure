<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.announcementEdit" />

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function() {
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#announceForm").validate( {
            submitHandler: function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });

            }, // submitHandler
            rules: {
                "announcement.title": "required",
                "userGroups": "required",
                "announcement.publishDate": "required",
                "announcement.status": "required",
                "announcement.announcementType": "required",
                "announcement.body": "required"
            }
        });

        //change textarea to ckEditor
        $('#announcement\\.body').ckeditor({height:300});
        $('#announcement\\.bodyCn').ckeditor({height:300});
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="announceList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="announceUpdate" name="announceForm" id="announceForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:hidden name="announcement.announceId" id="announcement.announceId"/>

    <div class="row">
        <article class="col-md-12">
            <div class="well">
                <s:textfield name="announcement.title" id="announcement.title" label="%{getText('title')}" cssClass="input-xxlarge"/>
                <s:checkboxlist name="userGroups" id="userGroups" label="%{getText('publishGroup')}" list="publishGroups" listKey="key" listValue="value" cssClass="checkbox style-0"/>
                <s:if test="disablePublishDate">
                    <s:textfield name="announcement.publishDate" id="announcement.publishDate" label="%{getText('publishDate')}" cssClass="input-xxlarge" readonly="true"/>
                </s:if>
                <s:else>
                    <ce:datepicker name="announcement.publishDate" id="announcement.publishDate" label="%{getText('publishDate')}" cssStyle="z-index: 100 !important;"/>
                </s:else>
                <s:select name="en" id="languageEn" label="%{getText('language')}" list="languages" listKey="key" listValue="value" readonly="true" disabled="true"/>
                <s:radio name="announcement.status" id="announcement.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
                <s:radio name="announcement.announcementType" id="announcement.announcementType" label="%{getText('announcementType')}" list="announcementTypes" listKey="key" listValue="value"/>
                <s:textarea name="announcement.body" id="announcement.body" theme="simple"/>
            </div>
        </article>
    </div>

    <s:if test="displayCn">
        <div class="row">
            <article class="col-md-12">
                <div class="well">
                    <s:select name="languageCode" id="languageCode" label="%{getText('language')}" list="languages" listKey="key" listValue="value" disabled="true"/>
                    <s:textfield name="announcement.titleCn" id="announcement.titleCn" label="%{getText('title')}" cssClass="input-xxlarge" required="true"/>
                    <s:textarea name="announcement.bodyCn" id="announcement.bodyCn" theme="simple" required="true"/>
                </div>
            </article>
        </div>
    </s:if>

<%--    <s:iterator status="iterStatus" var="languageSet" value="announcement.languageSet">--%>
<%--        <div class="row">--%>
<%--            <article class="col-md-8 col-sm-12">--%>
<%--                <div class="well">--%>
<%--                    <s:select name="#languageSet[0]" id="languageCodeDisplay" label="%{getText('language')}" list="languages" listKey="key" listValue="value" disabled="true"/>--%>
<%--                    <s:textfield name="#languageSet[1]" id="title" label="%{getText('title')}" cssClass="input-xxlarge" required="true"/>--%>
<%--                    <s:textarea name="#languageSet[2]" id="body" theme="simple" required="true"/>--%>
<%--                </div>--%>
<%--            </article>--%>
<%--        </div>--%>
<%--    </s:iterator>--%>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="announceList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple" >
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
