<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(document).ready(function() {
        $("#helpDeskForm").compalValidate({
            rules : {
                "helpDeskReply.message" : {
                    required : true
                }
            },
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success : function(){
                            window.location.reload()
                        }
                    });
                });
            }
        });

        $("#btnAdd").click(function() {
            var id = "<s:property value="helpDesk.ticketId"/>";

            $(".hiddenTicketId").val(id);
            $('#helpDeskReplyAddImageForm')[0].reset();
            $('#modal-dialog').modal('show');
        });

        $("#btnDelete").click(function() {
            var row = $('#dg').datagrid('getSelected');
            if (row) {
                $.post('<s:url action="helpDeskImageRemoveDetail"/>', {
                    "tempId" : row.tempId
                }, function(json) {
                    new JsonStat(json, {
                        onSuccess : function(json) {
                            $('#dg').datagrid('reload'); // reload the grid
                        },
                        onFailure : function(json, error) {
                            messageBox.alert(error);
                        }
                    });
                });
            }
        });

        $("#helpDeskReplyAddImageForm").compalValidate({
            submitHandler : function(form) {
                $(form).ajaxSubmit({
                    dataType : 'json',
                    success : processJsonSave
                });
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function() {
                    $('#modal-dialog').modal('hide');
                    $('#dg').datagrid('reload'); //reload the grid
                });
            }, // onFailure using the default
            onFailure : function(json, error) {
                messageBox.alert(error);
            }
        });
    }

    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');

        $("#imageViewerModal").dialog('open').dialog('vcenter');
        $("#imageViewer").attr('src', fileUrl);
    }
</script>

<style>
    #roundBorderUser{
        box-sizing: content-box;
        border: 2px solid lightgrey;
        padding: 10px;
        border-radius: 25px;
        background: lightgrey;
        height: auto;
        width: auto;
    }

    #roundBorderAdmin{
        box-sizing: content-box;
        border: 2px solid lightskyblue;
        padding: 10px;
        border-radius: 25px;
        background: lightskyblue;
        width: auto;
        float: right;
    }

    * {
        box-sizing: border-box;
    }

    .columnLeft{
        float: left;
        width: 60px;
        padding: 5px;
    }

    .columnRight{
        float: right;
        width: 60px;
        padding: 5px;
    }

    /* Clearfix (clear floats) */
    .row::after {
        content: "";
        clear: both;
        display: table;
    }

    .media-right{
        float: right;
    }
</style>

<form id="navForm" method="post">
    <input type="hidden" name="helpDeskReplyFileId" id="helpDeskReplyFileId" />
</form>

<sc:title title="title.helpDeskReply"/>

<div class="row">
    <article class="col-sm-12">
        <div class="well">
            <s:form action="helpDeskSave" name="helpDeskForm" id="helpDeskForm" cssClass="form-horizontal">
                <fieldset>
                    <legend><s:text name="details"/></legend>
                    <s:textfield name="helpDesk.ticketNo" id="helpDesk.ticketNo" label="%{getText('ticketNo')}" readonly="true"/>
                    <s:textfield name="helpDeskType" id="helpDeskType" label="%{getText('type')}" readonly="true"/>
                    <s:textfield name="helpDesk.subject" id="helpDesk.subject" label="%{getText('subject')}" readonly="true"/>

                    <legend><s:text name="chatHistory"/></legend>
                    <div class="ibox-content no-padding">
                        <ul class="list-group">
                            <s:iterator status="iterStatus" var="transView" value="helpDesk.helpDeskReplies">
                                <s:if test="#transView.senderType == 'MEMBER'">
                                    <div class="media roundBorder">
                                        <div class="media-left media-top">
                                            <a href="#">
                                                <img class="media-object" src="<c:url value="/images/user_icon.png"/>" alt="user_icon">
                                            </a>
                                        </div>
                                        <div class="media-body" id="roundBorderUser">
                                            <h4 class="media-heading" ><s:property value="#transView.sender.username"/></h4>
                                            <h6><s:property value="#transView.datetimeAdd"/></h6>
                                            <p><s:property value="#transView.messageInHtml" escapeHtml="false"/></p>

                                            <div class="row">
                                                <s:iterator status="imageIterStatusView" var="imageFileView" value="#transView.helpDeskReplyFiles">
                                                    <div class="col-xs-6 col-md-3 columnLeft">
                                                        <s:if test="#imageFileView.contentType == 'application/pdf'">
                                                            <a href="${imageFileView.fileUrl}" target="_blank"><img src="<c:url value="/images/pdfFileIcon.png"/>" alt="attach_image" style="width:60px"/></a>
                                                        </s:if>
                                                        <s:else>
                                                            <a onclick="viewImage('${imageFileView.fileUrl}')">
                                                                <img src="<c:url value="/images/imageFileIcon.png"/>" alt="attach_image" style="width:60px"/>
                                                            </a>
                                                        </s:else>
                                                    </div>
                                                </s:iterator>
                                            </div>
                                        </div>
                                    </div>
                                </s:if>
                                <s:else>
                                    <div class="media">
                                        <div class="media-right media-top">
                                            <a href="#">
                                                <img class="media-object" src="<c:url value="/images/admin_icon.png"/>" alt="user_icon">
                                            </a>
                                        </div>

                                        <div class="media-body" id="roundBorderAdmin" style="text-align: right">
                                            <h4 class="media-heading"><s:property value="#transView.sender.username"/></h4>
                                            <h6><s:property value="#transView.datetimeAdd"/></h6>
                                            <p><s:property value="#transView.messageInHtml" escapeHtml="false"/></p>

                                            <div class="row">
                                                <s:iterator status="imageIterStatus" var="imageFileView" value="#transView.helpDeskReplyFiles">
                                                    <div class="col-xs-6 col-md-3 columnRight">
                                                        <s:if test="#imageFileView.contentType == 'application/pdf'">
                                                            <a href="${imageFileView.fileUrl}" target="_blank"><img src="<c:url value="/images/pdfFileIcon.png"/>" alt="attach_image" style="width:60px"/></a>
                                                        </s:if>
                                                        <s:else>
                                                            <a onclick="viewImage('${imageFileView.fileUrl}')">
                                                                <img src="<c:url value="/images/imageFileIcon.png"/>" alt="attach_image" style="width:60px"/>
                                                            </a>
                                                        </s:else>
                                                    </div>
                                                </s:iterator>
                                            </div>
                                        </div>
                                    </div>
                                </s:else>
                            </s:iterator>
                        </ul>
                    </div>

                    <legend><s:text name="replyMessage"/></legend>
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="reply"/>: </label>
                        <div class="col-md-9">
                            <s:textarea name="helpDeskReply.message" id="helpDeskReply.message" cols="100" rows="10"/>
                        </div>
                    </div>

                    <ce:buttonRow>
                        <button type="button" class="btn btn-primary" id="btnAdd">
                            <i class="fa fa-paperclip"></i>
                            <s:text name="btnAddImage"/>
                        </button>
                        <button type="button" class="btn btn-danger" id="btnDelete">
                            <i class="fa fa-trash"></i>
                            <s:text name="btnDeleteImage"/>
                        </button>
                    </ce:buttonRow>

                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <table id="dg" class="easyui-datagrid" style="width:500px; height:200px" url="<s:url action="helpDeskReplyImagesDatatable"/>" rownumbers="true">
                                    <thead>
                                    <tr>
                                        <th field="filename" width="250"><s:text name="imageFileName"/></th>
                                        <th field="contentType" width="150"><s:text name="contentType"/></th>
                                        <th field="sortOrder" width="80"><s:text name="sortOrder"/></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>

                    <ce:buttonRow>
                        <s:hidden name ="ticketId"/>
                        <button id="btnSave" type="submit" class="btn btn-primary">
                            <i class="fa fa-floppy-o"></i>
                            <s:text name="btnSave"/>
                        </button>
                        <s:url var="urlExit" action="helpDeskList"/>
                        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                            <i class="icon-remove-sign"></i>
                            <s:text name="btnExit"/>
                        </ce:buttonExit>
                    </ce:buttonRow>

                </fieldset>
            </s:form>
        </div>
    </article>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true"
     resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>

<div class="modal fade" id="modal-dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <s:form action="attachHelpDeskReplyImages" name="helpDeskReplyAddImageForm" id="helpDeskReplyAddImageForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">X</button>
                    <h4 class="modal-title">
                        <s:text name="addImage"/>
                    </h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="image"/></label>
                        <div class="col-md-9">
                            <s:file name="fileUpload" id="fileUpload"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"><s:text name="sortOrder"/></label>
                        <div class="col-md-9">
                            <s:textfield theme="simple" name="helpDeskReplyFile.sortOrder" id="helpDeskReplyFile.sortOrder" size="3" maxlength="3" cssClass="form-control" />
                        </div>
                    </div>
                </div>

                <input type="hidden" class="hiddenTicketId" name="ticketId"/>
                <div class="modal-footer">
                    <button id="btnAddDetail" type="submit" class="btn btn-primary">
                        <i class="fa fa-floppy-o"></i>
                        <s:text name="btnAdd"/>
                    </button>
                    <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
                </div>
            </s:form>
        </div>
    </div>
</div>