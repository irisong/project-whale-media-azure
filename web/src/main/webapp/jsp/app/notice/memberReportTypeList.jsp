<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.memberReportType"/>

<script type="text/javascript">
    function loadDatatables() {
        $('#memberReportTypesListTable').DataTable({
            "destroy" : true,
            "order": [0,"desc"],
            "ajax": {
                "url" : "<s:url action="memberReportTypeListDatatables"/>",
                "data" : {
                    status: $("#status").val()
                }
            },
            "columns" : [
                {
                    "data" : "datetimeAdd",
                    "render": function(data) {
                        return $.datagridUtil.formatDate(data);
                    }
                },
                {"data" : "typeName"},
                {"data" : "typeNameCn"},
                {"data" : "typeNameMs"},
                {"data" : "sortOrder"},
                {
                    "data" : "status",
                    "render": function(data){
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {
                    "data" : "status",
                    "render": function(){
                        var result = '<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": 'reportTypeId'
        });
    }

    $(function() {
        loadDatatables();

        $("#btnCreate").click(function(){
            $("#navForm").attr("action", "<s:url action="memberReportTypeAdd"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnEdit", function(){
            var id = $(this).parents("tr").attr("id");
            $("#memberReportType\\.reportTypeId").val(id);
            $("#navForm").attr("action", "<s:url action="memberReportTypeEdit"/>")
            $("#navForm").submit();
        });
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="memberReportType.reportTypeId" id="memberReportType.reportTypeId"/>
</form>

<s:form name="memberReportTypeForm" id="memberReportTypeForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    <sc:submitData/>

    <ce:buttonRow>
        <button type="submit" id="btnSearch" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button type="button" id="btnCreate" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.memberReportTypesList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="memberReportTypesListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="datetimeAdd"/></th>
                            <th><s:text name="memberReportType"/></th>
                            <th><s:text name="memberReportTypeCn"/></th>
                            <th><s:text name="memberReportTypeMs"/></th>
                            <th><s:text name="sortOrder"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>