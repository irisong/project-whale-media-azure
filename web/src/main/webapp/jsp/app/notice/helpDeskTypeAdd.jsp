<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.helpDeskTypeAdd"/>

<script type="text/javascript">
    $(function() {
        $("#helpDeskTypeForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules:{
                "helpDeskType.sortOrder" : {
                    digits:true,
                    min:0
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="helpDeskTypeList"/>"; //refresh page
                });
            },  // onFailure using the default
            onFailure : function(json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<div class="well">
    <s:form action="helpDeskTypeSave" name="helpDeskTypeForm" id="helpDeskTypeForm" cssClass="form-horizontal">
        <sc:displayErrorMessage align="center"/>

        <s:textfield name="helpDeskType.typeName" id="helpDeskType.typeName" label="%{getText('helpDeskType')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="helpDeskType.typeNameCn" id="helpDeskType.typeNameCn" label="%{getText('helpDeskTypeCn')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="helpDeskType.typeNameMs" id="helpDeskType.typeNameMs" label="%{getText('helpDeskTypeMs')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="helpDeskType.sortOrder" id="helpDeskType.sortOrder" label="%{getText('sortOrder')}" cssClass="input-xxlarge" required="true"/>
        <s:radio name="helpDeskType.status" id="helpDeskType.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value" required="true"/>

        <ce:buttonRow>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>
            <s:url var="urlExit" action="helpDeskTypeList"/>
            <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                <i class="fa fa-times"></i>
                <s:text name="btnExit"/>
            </ce:buttonExit>
        </ce:buttonRow>
    </s:form>
</div>