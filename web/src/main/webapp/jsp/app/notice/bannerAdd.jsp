<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.bannerAdd" />

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    var updateType;
    $(function() {

        if('<s:property value="updateType"/>' == 'VJ_PROFILE'){
            $('label[for=redirectUrl], input#redirectUrl').hide();
            $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').show();
        }else{
            $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').hide();
            $('label[for=redirectUrl], input#redirectUrl').show();
        }

        $("#banner\\.bannerType").on("change",function () {
            var bannerType = $('#banner\\.bannerType').val();
            $.ajax({
                type: 'POST',
                url: "<s:url action="updateBannerType"/>",
                dataType: 'json',
                cache: false,
                data: {
                    updateType:bannerType
                }, success: function (data) {



                    if(data.updateType=="VJ_PROFILE"){
                        $('label[for=redirectUrl], input#redirectUrl').hide();
                        $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').show();
                    }else{
                        $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').hide();
                        $('label[for=redirectUrl], input#redirectUrl').show();
                    }
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }});
        });


        $("#btnExit").click(function(){
            $.ajax({
               // url: "<s:url action="bannerAddExit" namespace="/app/notice"/>?banner.bannerId="+$('#banner\\.bannerId').val(),
                success: proceedExit
            });
        });

        $("#bannerForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "banner.title": "required",
                "banner.titleCn": "required",
                "banner.titleMs": "required",
                "banner.status": "required",
                "banner.bannerType": "required",
                "banner.seq": "required",
                "fileUpload": "required"
            }
        });
    });

    function proceedExit() {
        window.location = "<s:url action="bannerList"/>";
    }

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function() {
                    window.location = "<s:url action="bannerList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    function getNextBannerSeq() {
        $.getJSON("<s:url action="getNextBannerSeq"/>", {
            "categoryId" : $("#banner\\.categoryId").val()
        }, function (json) {
            $("#banner\\.seq").val(json.seq);
        });
    }
</script>

<s:form action="bannerSave" name="bannerForm" id="bannerForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:hidden name="banner.bannerId" id="banner.bannerId"/>
    <input type="hidden" class="hiddenMemberId" name="memberId"/>

        <div class="row">
            <article class="col-md-12">
                <s:textfield name="banner.title" id="banner.title" label="%{getText('title')}" cssClass="input-xxlarge"/>
                <s:textfield name="banner.titleCn" id="banner.titleCn" label="%{getText('titleCn')}" cssClass="input-xxlarge"/>
                <s:textfield name="banner.titleMs" id="banner.titleMs" label="%{getText('titleMs')}" cssClass="input-xxlarge"/>
                <s:select name="banner.categoryId" id="banner.categoryId" label="%{getText('category')}" list="liveStreamcategories" listKey="key" listValue="value" onchange="getNextBannerSeq()" />
                <s:radio name="banner.status" id="banner.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
                <s:select name="banner.bannerType" id="banner.bannerType" label="%{getText('announcementType')}" list="bannerTypes" listKey="key" listValue="value" />
                <s:textfield name="redirectUrl" id="redirectUrl" label="%{getText('redirectUrl')}" cssClass="input-xxlarge"/>
                <sc:memberTextField name="memberCode" />
                <s:textfield name="banner.seq" id="banner.seq" label="%{getText('seq')}" cssClass="input-xxlarge"/>
                <s:file name="fileUpload" id="fileUpload" label="%{getText('File')}"/>
                <s:file name="fileUploadEn" id="fileUploadEn" label="%{getText('fileEn')}"/>
                <s:file name="fileUploadCn" id="fileUploadCn" label="%{getText('fileCn')}"/>
                <s:file name="fileUploadMs" id="fileUploadMs" label="%{getText('fileMs')}"/>
            </article>

        </div>

    <ce:buttonRow>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>


        <button id="btnExit" type="button" class="btn">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </button>

    </ce:buttonRow>
</s:form>
<!-- #modal-dialog -->
