<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(document).ready(function() {
        $("#faqForm").validate({
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success : processJsonSave
                    })
                });
            },
            rules : {
                "freqAskQues.title" : "required",
                "freqAskQues.titleCn" : "required",
                "freqAskQues.titleMs" : "required",
                "freqAskQues.status" : "required"
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function() {
                    window.location = '<s:url action="freqAskQuesList"/>'
                });
            }, // onFailure using the default
            onFailure : function(json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<sc:title title="title.faqAdd"/>

<div class="row">
    <article class="col-sm-12">
        <div class="well">
            <s:form action="freqAskQuesSave" name="faqForm" id="faqForm" cssClass="form-horizontal">
                <fieldset>
                    <legend><s:text name="title.faqAdd"/></legend>

                    <s:textfield name="freqAskQues.title" id="freqAskQues.title" label="%{getText('title')}"/>
                    <s:textfield name="freqAskQues.titleCn" id="freqAskQues.titleCn" label="%{getText('titleCn')}"/>
                    <s:textfield name="freqAskQues.titleMs" id="freqAskQues.titleMs" label="%{getText('titleMs')}"/>
                    <s:select name="freqAskQues.status" id="freqAskQues.status" label="%{getText('status')}" list="statusOption" listKey="key" listValue="value" disabled="true"/>
                    <s:textfield type="number" name="freqAskQues.sortOrder" id="freqAskQues.sortOrder" label="%{getText('sortOrder')}"/>

                    <legend><s:text name="faq_file"/></legend>

                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="fileEn"/>:</label>
                        <div class="col-md-9 col-sm-9">
                            <s:file name="enFileUpload"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="fileCn"/>:</label>
                        <div class="col-md-9 col-sm-9">
                            <s:file name="cnFileUpload"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="fileMs"/>:</label>
                        <div class="col-md-9 col-sm-9">
                            <s:file name="msFileUpload"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="fileCombine"/>:</label>
                        <div class="col-md-9 col-sm-9">
                            <s:file name="combineFileUpload"/>
                        </div>
                    </div>

                    <ce:buttonRow>
                        <button id="btnSave" type="submit" class="btn btn-primary">
                            <i class="fa fa-floppy-o"></i>
                            <s:text name="btnSave"/>
                        </button>
                        <s:url var="urlExit" action="freqAskQuesList" />
                        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                            <i class="icon-remove-sign"></i>
                            <s:text name="btnExit" />
                        </ce:buttonExit>
                    </ce:buttonRow>

                </fieldset>
            </s:form>
        </div>
    </article>
</div>