<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.memberReportTypeAdd"/>

<script type="text/javascript">
    $(function() {
        $("#memberReportTypeForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules:{
                "memberReportType.sortOrder" : {
                    number : true,
                    min : 0
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="memberReportTypeList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<div class="well">
    <s:form action="memberReportTypeSave" name="memberReportTypeForm" id="memberReportTypeForm" cssClass="form-horizontal">
        <sc:displayErrorMessage align="center"/>

        <s:textfield name="memberReportType.typeName" id="memberReportType.typeName" label="%{getText('memberReportType')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="memberReportType.typeNameCn" id="memberReportType.typeNameCn" label="%{getText('memberReportTypeCn')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="memberReportType.typeNameMs" id="memberReportType.typeNameMs" label="%{getText('memberReportTypeMs')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="memberReportType.sortOrder" id="memberReportType.sortOrder" label="%{getText('sortOrder')}" cssClass="input-xxlarge" required="true"/>
        <s:radio name="memberReportType.status" id="memberReportType.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value" required="true"/>

        <ce:buttonRow>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>
            <s:url var="urlExit" action="memberReportTypeList"/>
            <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                <i class="fa fa-times"></i>
                <s:text name="btnExit"/>
            </ce:buttonExit>
        </ce:buttonRow>
    </s:form>
</div>