<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.announcement"/>

<script type="text/javascript">
    function loadDatatables(userGroups) {
        $('#announceListTable').DataTable({
            "destroy": true,
            "order": [[0, "desc"],[4, "desc"]],
            "ajax": {
                "url": "<s:url action="announceListDatatables"/>",
                "data": {
                    userGroups : (userGroups == undefined) ? '' : userGroups,
                    status: $('#status').val(),
                    announcementType: $('#announcementType').val(),
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                }
            },
            "columns": [
                {
                    "data": "publishDate",
                    "render": function(data, type, row){
                        return $.datagridUtil.formatDate(data);
                    }
                },
                {"data": "title"},
                {"data": "status"},
                {"data": "announcementType"},
                {"data": "datetimeAdd"},
                {
                    "data": "announceId",
                    "className": "text-nowrap text-center",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-info btn-xs btnView"><i class="fa fa-file-text-o"></i>&nbsp;<s:text name="btnView"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "announceId"
        });
    }

    $(function(){
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="announceAdd"/>")
            $("#navForm").submit();
        });

       $(document).on("click", ".btnEdit", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#announcement\\.announceId").val(id);
            $("#navForm").attr("action", "<s:url action="announceEdit"/>")
            $("#navForm").submit();
       });

       $(document).on("click", ".btnView", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#announcementModal").dialog('open').dialog('refresh', "<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId="+id);
       });

       var userGroups = '';
       <s:iterator value="userGroups" >
           userGroups += '<s:property/>' + ',';
       </s:iterator>

       loadDatatables(userGroups);
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="announcement.announceId" id="announcement.announceId" />
</form>

<s:form name="announceForm" id="announceForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    <s:select name="announcementType" id="announcementType" label="%{getText('announcementType')}" list="announcementTypes" listKey="key" listValue="value"/>
    <s:checkboxlist name="userGroups" id="userGroups" list="publishGroups" label="%{getText('publishGroup')}" listKey="key" listValue="value" cssClass="checkbox style-01" />
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="date"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.announcementList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="announceListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="publishDate"/></th>
                                <th><s:text name="title"/></th>
                                <th><s:text name="status"/></th>
                                <th><s:text name="announcementType"/></th>
                                <th><s:text name="datetimeAdd"/></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="announcementModal" class="easyui-dialog" style="width:700px; height:500px; z-index: 200" title="<s:text name="title.announcement"/>" closed="true" resizable="true" maximizable="true"></div>