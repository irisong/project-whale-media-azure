<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.documentDownload" />

<script type="text/javascript">
    function loadDatatables(userGroups) {
        $('#docfileListTable').DataTable({
            "destroy" : true,
            "order": [[ 3, "desc" ]],
            "ajax": {
                "url" : "<s:url action="docfileListDatatables"/>",
                "data" : {
                    userGroups : (userGroups == undefined) ? '' : userGroups,
                    languageCode: $('#languageCode').val(),
                    status: $('#status').val()
                }
            },
            "columns" : [
                {"data" : "datetimeAdd"},
                {"data" : "title"},
                {"data" : "filename"},
                {"data" : "status"},
                {"data" : "status"}
            ],
            "rowId": 'docId',
            "columnDefs" : [
                { "orderable": false, "targets": [4] },
                {"render": function(data, type, row) {
                    return $.datagridUtil.formatDate(data);
                }, "targets": [0] },
                {"render": function(data, type, row){
                    return $.datagridUtil.formatStatus(data);
                }, "targets": [3] },
                {"className": "text-nowrap text-center",
                 "render": function(data, type, row){
                    var result = '<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnDownload"><i class="fa fa-download"></i>&nbsp;<s:text name="btnDownload"/></button>';
                    return result;
                }, "targets": [4] }
            ]
        });
    }

    $(function(){
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="docfileAdd" />")
            $("#navForm").submit();
        });

       $(document).on("click", ".btnEdit", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#docFile\\.docId").val(id);
            $("#navForm").attr("action", "<s:url action="docfileEdit" />")
            $("#navForm").submit();
       });

       $(document).on("click", ".btnDownload", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#docFile\\.docId").val(id);
            $("#navForm").attr("action", "<s:url action="docfileDownload" />")
            $("#navForm").submit();
       });

       var userGroups = '';
       <s:iterator value="userGroups" >
           userGroups += '<s:property/>' + ',';
       </s:iterator>

       loadDatatables(userGroups);

    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="docFile.docId" id="docFile.docId" />
</form>

<s:form name="docFileForm" id="docFileForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:select name="languageCode" id="languageCode" label="%{getText('language')}" list="languages" listKey="key" listValue="value"/>
    <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    <s:checkboxlist name="userGroups" id="userGroups" list="publishGroups" label="%{getText('publishGroup')}" listKey="key" listValue="value" cssClass="checkbox style-0" />
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
             <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.documentDownloadList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="docfileListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="datetimeAdd"/></th>
                                <th><s:text name="title"/></th>
                                <th><s:text name="filename"/></th>
                                <th><s:text name="status"/></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>