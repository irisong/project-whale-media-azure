<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.emailqAdd" />

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">
    $(function() {
        $("#emailqForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "emailq.emailTo":{
                    email : true
                },
                "emailq.emailCc":{
                    email : true
                }
            }
        });

        $('#emailq\\.body').ckeditor({height:500});
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    // refresh page
                    window.location = "<s:url action="emailqList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="emailqSave" name="emailqForm" id="emailqForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />

    <s:textfield name="emailq.emailTo" id="emailq.emailTo" label="%{getText('to')}" cssClass="input-xxlarge" required="true"/>
    <s:textfield name="emailq.emailCc" id="emailq.emailCc" label="%{getText('cc')}" cssClass="input-xxlarge"/>
    <s:textfield name="emailq.title" id="emailq.title" label="%{getText('title')}" cssClass="input-xxlarge" required="true"/>
    <s:textarea name="emailq.body" id="emailq.body" theme="simple" required="true"/>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="emailqList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
