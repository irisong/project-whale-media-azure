<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.memberReport"/>

<script type="text/javascript">
    function loadDatatables(){
        $('#memberReportListTable').DataTable({
            "destroy" : true,
            "order": [0,"desc"],
            "ajax" : {
                "url" : "<s:url action="memberReportListDatatables"/>",
                "data" : {
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    memberReportType: $('#memberReportType').val(),
                    reportMemberId: $('#reportedMemberId').val(),
                    readStatus: $('#isRead').val()
                }
            },
            "columns" : [
                {
                    "data" : "datetimeAdd",
                    "render": function(data) {
                        return $.datagridUtil.formatDate(data);
                    }
                },
                {"data": "memberReportType.typeName"},
                {"data": "memberCode"},
                {"data": "description"},
                {
                    "data": "readStatus",
                    "render": function (data){
                        return $.datagridUtil.formatYesNo(data)
                    }
                },
                {"data": "warningCount"},
                {
                    "data": "read",
                    "className": "text-nowrap",
                    "render": function () {
                        var result = '';

                        result += '&nbsp;<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnWarning"><s:text name="btnWarning"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": 'reportId'
        });
    }

    $(function () {
        loadDatatables();

        $(document).on("click", ".btnView", function() {
            var id = $(this).parents("tr").attr("id");
            $("#memberReport\\.reportId").val(id);
            $("#navForm").attr("action", "<s:url action="memberReportView"/>");
            $("#navForm").submit();
        });

        $(document).on("click", ".btnWarning", function() {
            var id = $(this).parents("tr").attr("id");
            $('#warningMessageForm').trigger("reset");
            // $("#warningReason").val('');
            // $("#warningReasonCn").val('');
            $("#warningMessageModal").dialog('open').dialog('vcenter');
            $("#reportId").val(id);
            $("#warningReason").focus();
        });

        $("#warningMessageForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: defaultJsonCallback
                    });

                });
            }
        });

        $(".btnCancel").click( function() {
            $("#warningMessageModal").dialog('close');
        });

        function defaultJsonCallback(json) {
            $("body").unmask();
            new JsonStat(json, {
                onSuccess: function () {
                    // table.ajax.reload();
                    $("#memberReportListSearchForm").submit();
                    $("#warningMessageModal").dialog('close');
                },
                onFailure: function (json, error) {
                    messageBox.alert(error);
                }
            });
        }


    }); //end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="memberReport.reportId" id="memberReport.reportId"/>
</form>

<s:form name="memberReportListSearchForm" id="memberReportListSearchForm" cssClass="form-horizontal">
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="creationDate"/>
    <s:select name="memberReportType" id="memberReportType" list="allMemberReportTypeList" label="%{getText('memberReportType')}" listKey="key" listValue="value"/>
    <sc:memberTextField name="reportedMemberId"/>
    <s:select name="isRead" id="isRead" list="allReadStatusList" label="%{getText('isRead')}" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.memberReportList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="memberReportListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="datetimeAdd"/></th>
                            <th><s:text name="memberReportType"/></th>
                            <th><s:text name="reportedMemberId"/></th>
                            <th><s:text name="description"/></th>
                            <th><s:text name="isRead"/></th>
                            <th><s:text name="warningCount"/></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="warningMessageModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="warningMessage"/>"
     closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="warningMessage" name="warningMessageForm" id="warningMessageForm" cssClass="form-horizontal"
                        enctype="multipart/form-data" method="post">

                    <label class="control-label" style="vertical-align: top;"><s:text name="reasonEn"/> : </label>
                    <s:textarea name="warningReason" id="warningReason" theme="simple" rows="5" cols="60" required="true"/>

                    <label class="control-label" style="vertical-align: top;"><s:text name="reasonCn"/> : </label>
                    <s:textarea name="warningReasonCn" id="warningReasonCn" theme="simple" rows="5" cols="60" required="true"/>

                    <input type="hidden" name="reportId" id="reportId"/>
                    <ce:buttonRow>
                        <button id="btnProceed" class="btn btn-primary" type="submit">
                            <s:text name="btnProceed"/>
                        </button>
                        <button class="btn btnCancel" type="button">
                            <s:text name="btnCancel"/>
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>
