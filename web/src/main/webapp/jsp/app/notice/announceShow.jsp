<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="row">
    <article class="col-md-12">
        <div class="well">
            <legend>${announcement.title}</legend>
            ${announcement.body}
        </div>
    </article>
</div>

<br/>

<s:if test="displayCn">
    <s:iterator status="iterStatus" var="languageSet" value="announcement.languageSet">
        <div class="row">
            <article class="col-md-12">
                <div class="well">
                    <legend>${languageSet[1]}</legend>
                    ${languageSet[2]}
                </div>
            </article>
        </div>
    </s:iterator>
</s:if>