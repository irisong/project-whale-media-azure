<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function loadDatatables() {
        $('#faqListTable').DataTable({
            "destroy" : true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url" : "<s:url action="freqAskQuesListDatatables"/>",
                "data" : {
                    status: $("#status").val()
                }
            },
            "columns" : [
                {"data" : "datetimeAdd"},
                {"data" : "title"},
                {"data" : "titleCn"},
                {"data" : "titleMs"},
                {"data" : "sortOrder"},
                {
                    "data" : "freqAskQuesFileUrlEn",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<center><a href="' + data + '" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a></center>';
                        }
                        return content;
                    }
                },
                {
                    "data" : "freqAskQuesFileUrlCn",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<center><a href="' + data + '" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a></center>';
                        }
                        return content;
                    }
                },
                {
                    "data" : "freqAskQuesFileUrlMs",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<center><a href="' + data + '" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a></center>';
                        }
                        return content;
                    }
                },
                {
                    "data" : "freqAskQuesFileUrlCombine",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<center><a href="' + data + '" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a></center>';
                        }
                        return content;
                    }
                },
                {"data" : "status"},
                {
                    "data": "status",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "questionId"
        });
    }

    $(function(){
        $("#btnCreate").click(function(){
            $("#navForm").attr("action", "<s:url action="freqAskQuesAdd"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnEdit", function(){
            var id = $(this).parents("tr").attr("id");
            $("#questionId").val(id);
            $("#navForm").attr("action", "<s:url action="freqAskQuesEdit"/>")
            $("#navForm").submit();
        });

        loadDatatables();
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="questionId" id="questionId" />
</form>

<sc:title title="title.faqList"/>

<s:form name="faqForm" id="faqForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
<%--    <sc:submitData/>--%>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.faqList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="faqListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="datetimeAdd"/></th>
                            <th><s:text name="title"/></th>
                            <th><s:text name="titleCn"/></th>
                            <th><s:text name="titleMs"/></th>
                            <th><s:text name="sortOrder"/></th>
                            <th><s:text name="fileEn"/></th>
                            <th><s:text name="fileCn"/></th>
                            <th><s:text name="fileMs"/></th>
                            <th><s:text name="fileCombine"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<%--<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true"--%>
<%--     resizable="true" maximizable="true">--%>
<%--    <img id="imageViewer" style="width: 100%;height: 100%">--%>
<%--</div>--%>
