<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.bannerEdit" />

<script type="text/javascript">
    $(function() {
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        if ('<s:property value="updateType"/>' == 'VJ_PROFILE') {
            $('label[for=redirectUrl], input#redirectUrl').hide();
            $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').show();
        } else {
            $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').hide();
            $('label[for=redirectUrl], input#redirectUrl').show();
        }

        $("#banner\\.bannerType").on("change",function () {
            var bannerType = $('#banner\\.bannerType').val();
            $.ajax({
                type: 'POST',
                url: "<s:url action="updateBannerType"/>",
                dataType: 'json',
                cache: false,
                data: {
                    updateType:bannerType
                }, success: function (data) {
                    if(data.updateType=="VJ_PROFILE"){
                        $('label[for=redirectUrl], input#redirectUrl').hide();
                        $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').show();
                    }else{
                        $('label[for=memberCode], input#memberCode, button#btnSearchmemberCode').hide();
                        $('label[for=redirectUrl], input#redirectUrl').show();
                    }
                   // document.getElementById('banner.redirectUrl').value = data.banner;
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                }});
        });

        $("#bannerForm").validate( {
            submitHandler: function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });

            }, // submitHandler
            rules: {
                "banner.title": "required",
                "banner.status": "required",
                "banner.bannerType": "required",
                "banner.seq": "required",
                "redirectUrl": "required",
                "memberCode": "required",
                "fileUpload": "required"
            }
        });
    });

    function changeUploadFile(fileUploadType, language) {
        $.getJSON("<s:url action="deleteBannerFile"/>", {
            bannerId : '<s:property value="banner.bannerId"/>',
            deleteLanguage : language
        }, function () {
            $("#"+fileUploadType).html('<input type="file" name="'+fileUploadType+'">');
        });
    }

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="bannerList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="bannerUpdate" name="bannerForm" id="bannerForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:hidden name="banner.bannerId" id="banner.bannerId"/>

    <div class="row">
        <article class="col-md-12">
            <div class="well">
                <s:textfield name="banner.title" id="banner.title" label="%{getText('title')}" cssClass="input-xxlarge"/>
                <s:textfield name="banner.titleCn" id="banner.titleCn" label="%{getText('titleCn')}" cssClass="input-xxlarge"/>
                <s:textfield name="banner.titleMs" id="banner.titleMs" label="%{getText('titleMs')}" cssClass="input-xxlarge"/>
                <s:select name="banner.categoryId" id="categoryId" label="%{getText('category')}" list="liveStreamcategories" listKey="key" listValue="value" />                <s:radio name="banner.status" id="banner.status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
                <s:select name="banner.bannerType" id="banner.bannerType" label="%{getText('announcementType')}" list="bannerTypes" listKey="key" listValue="value"/>
                <s:textfield name="banner.seq" id="banner.seq" label="%{getText('seq')}" cssClass="input-xxlarge"/>
                <s:textfield name="redirectUrl" id="redirectUrl" label="%{getText('redirectUrl')}" cssClass="input-xxlarge"/>
                <sc:memberTextField name="memberCode" />

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label"><s:text name="Common"/>:</label>
                    <div class="col-md-9 col-sm-9" id="fileUpload">
                        <s:if test="%{banner.fileUrl != null && banner.fileUrl.length()>0}">
                            <a href="${banner.fileUrl}" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a>
                            <button type="button" class="btn btn-xs" onclick="changeUploadFile('fileUpload','NONE')"><s:text name="btnChangeDelete"/></button>
                        </s:if>
                        <s:else>
                            <s:file name="fileUpload"/>
                        </s:else>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label"><s:text name="EN FileUpload"/>:</label>
                    <div class="col-md-9 col-sm-9" id="fileUploadEn">
                        <s:if test="%{banner.fileUrlEn != null && banner.fileUrlEn.length()>0}">
                            <a href="${banner.fileUrlEn}" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a>
                            <button type="button" class="btn btn-xs" onclick="changeUploadFile('fileUploadEn','EN')"><s:text name="btnChangeDelete"/></button>
                        </s:if>
                        <s:else>
                            <s:file name="fileUploadEn"/>
                        </s:else>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label"><s:text name="CN FileUpload"/>:</label>
                    <div class="col-md-9 col-sm-9" id="fileUploadCn">
                        <s:if test="%{banner.fileUrlCn != null && banner.fileUrlCn.length()>0}">
                            <a href="${banner.fileUrlCn}" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a>
                            <button type="button" class="btn btn-xs" onclick="changeUploadFile('fileUploadCn','CN')"><s:text name="btnChangeDelete"/></button>
                        </s:if>
                        <s:else>
                            <s:file name="fileUploadCn"/>
                        </s:else>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 col-sm-3 control-label"><s:text name="MS FileUpload"/>:</label>
                    <div class="col-md-9 col-sm-9" id="fileUploadMs">
                        <s:if test="%{banner.fileUrlMs != null && banner.fileUrlMs.length()>0}">
                            <a href="${banner.fileUrlMs}" target="_blank"><img src="<c:url value="/images/fileopen.png"/>"/></a>
                            <button type="button" class="btn btn-xs" onclick="changeUploadFile('fileUploadMs','MS')"><s:text name="btnChangeDelete"/></button>
                        </s:if>
                        <s:else>
                            <s:file name="fileUploadMs"/>
                        </s:else>
                    </div>
                </div>

            </div>
        </article>
    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="bannerList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple" >
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
