<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.helpDeskList"/>

<script type="text/javascript">
    var table = null;

    $(function () {
        table = $('#helpDeskListTable').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="helpDeskListDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    fullName: $('#fullName').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    helpDeskType: $('#helpDeskType').val(),
                    status: $('#status').val(),
                    repliedByAdmin: $('#repliedByAdmin').val()
                }
            },
            "columns": [
                {"data": "datetimeAdd"},
                {"data": "ticketNo"},
                {"data": "owner.username"},
                {"data": "helpDeskType.typeName"},
                {
                    "data": "subject",
                    "orderable": false
                },
                {
                    "data": "repliedByAdmin",
                    "render": function (data, type, row){
                        return $.datagridUtil.formatYesNo(data)
                    }
                },
                {"data": "adminReplyDatetime"},
                {
                    "data": "status",
                    "render": function (data, type, row){
                        return $.datagridUtil.formatStatus(data)
                    }
                },
                {"data": "closeDatetime"},
                {
                    "data": "closeByUser.username",
                    "render": function (data, type, row){
                        if(data == null){
                            return "";
                        }else{
                            return data;
                        }
                    }
                },
                {
                    "data": "status",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '';

                        result += '<div class="btn-group">\n' +
                            '    <button class="btn btn-danger btn-xs dropdown-toggle" data-toggle="dropdown">\n' +
                            '        <s:text name="more"/> <span class="caret"></span>\n' +
                            '    </button>\n' +
                            '    <ul class="dropdown-menu">\n' +
                            '        <li><a class="btnClose"><s:text name="closeTicket"/></a></li>\n' +
                            '        <li><a class="btnReopen"><s:text name="openTicket"/></a></li>\n' +
                            '    </ul>\n' +
                            '</div>';

                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnReply"><s:text name="btnReply"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button>';

                        return result;
                    }
                }
            ],
            "rowId": 'ticketId'
        });

        $(document).on("click", ".btnClose", function(event) {
            var id = $(this).parents("tr").attr("id");

            $("body").loadmask("<s:text name="processing.msg"/>");
            $.post("<s:url action="helpDeskTicketClose"/>", {
                "ticketId": id
            })
                .done(function(data) {
                    messageBox.info(data.successMessage, function() {
                        $("body").unmask();
                        $("#helpDeskListSearchForm").submit();
                    });
                })
                .fail(function(data,error) {
                    $("body").unmask();
                    messageBox.alert(error);
                });
        });

        $(document).on("click", ".btnReopen", function(event) {
            var id = $(this).parents("tr").attr("id");

            $("body").loadmask("<s:text name="processing.msg"/>");
            $.post("<s:url action="helpDeskTicketReopen"/>", {
                "ticketId": id
            })
                .done(function (data) {
                    messageBox.info(data.successMessage,function(){
                        $("body").unmask();
                        $("#helpDeskListSearchForm").submit();
                    });
                })
                .fail(function (data,error) {
                    $("body").unmask();
                    messageBox.alert(error);
                });
        });

        $(document).on("click", ".btnReply", function(event) {
            var id = $(this).parents("tr").attr("id");
            $("#ticketId").val(id);
            $("#navForm").attr("action", "<s:url action="helpDeskReply"/>");
            $("#navForm").submit();
        });

        $(document).on("click", ".btnView", function(event) {
            var id = $(this).parents("tr").attr("id");
            $("#ticketId").val(id);
            $("#navForm").attr("action", "<s:url action="helpDeskView" />");
            $("#navForm").submit();
        });
    }); //end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="ticketId" id="ticketId"/>
</form>

<s:form name="helpDeskListSearchForm" id="helpDeskListSearchForm" cssClass="form-horizontal">
    <sc:memberTextField name="memberCode"/>
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="creationDate"/>
    <s:select name="helpDeskType" id="helpDeskType" list="allHelpDeskTypeList" label="%{getText('type')}" listKey="key" listValue="value"/>
    <s:select name="status" id="status" list="allStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>
    <s:select name="repliedByAdmin" id="repliedByAdmin" list="allHelpDeskRepliedStatus" label="%{getText('repliedByAdmin')}" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.helpDeskList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="helpDeskListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="datetimeAdd"/></th>
                            <th><s:text name="ticketNo"/></th>
                            <th><s:text name="memberCode"/></th>
                            <th><s:text name="helpDeskType"/></th>
                            <th><s:text name="subject"/></th>
                            <th><s:text name="repliedByAdmin"/></th>
                            <th><s:text name="adminReplyDatetime"/></th>
                            <th><s:text name="status"/></th>
                            <th><s:text name="closeDatetime"/></th>
                            <th><s:text name="closedBy"/></th>
                            <th>&nbsp</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>