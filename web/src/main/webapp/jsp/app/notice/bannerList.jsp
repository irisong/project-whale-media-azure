<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.banner" />

<script type="text/javascript">
    function loadDatatables(userGroups) {
        $('#bannerListTable').DataTable({
            "destroy": true,
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="bannerListDatatables"/>",
                "data": {
                    userGroups : (userGroups == undefined) ? '' : userGroups,
                    status: $('#status').val(),
                    category :$('#categorySearch').val()


                }
            },
            "columns": [
                {"data": "seq"},
                {"data": "title"},
                {"data": "titleCn"},
                {"data": "titleMs"},
                {"data": "status"},
                {"data": "bannerTypeDesc"},
                {
                    "data" : "fileUrl",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data" : "fileUrlEn",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data" : "fileUrlCn",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data" : "fileUrlMs",
                    "orderable": false,
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data": "bannerId",
                    "className": "text-nowrap text-center",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-danger btn-xs btnDeleteBanner"><s:text name="btnDelete"/></button>';
                        result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "bannerId"
        });
    }

    $(function() {
        // make publish groups checkbox display inline
        $("div#userGroups_field label.checkboxLabel").css('display', 'inline');

        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="bannerAdd" />");
            $("#navForm").submit();
        });

        $("#btnAddNew").click(function (event) {
            $("#newCategoryForm").trigger("reset");

            $.getJSON("<s:url action="getNextBannerCategorySeq"/>", function (json) {
                $("#banner\\.seq").val(json.seq);
            });

            $("#addNewCategoryModal").dialog('open').dialog('vcenter');
        });

        $("#newCategoryForm").validate({
            submitHandler: function (form) {
                $("#addNewCategoryModal").dialog('close');
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: processJsonSave
                });
            },
            rules: {
                "seq": {
                    digits: true,
                    required: true
                },
                "category": {
                    required: true
                },
                "categoryCn": {
                    required: true
                },
                "categoryMs": {
                    required: true
                }
            }
        });

        $(document).on("click", ".btnEdit", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#banner\\.bannerId").val(id);
            $("#navForm").attr("action", "<s:url action="bannerEdit" />")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnView", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#announcementModal").dialog('open').dialog('refresh', "<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId="+id);
        });

        $(document).on("click", ".btnDelete", function (event) {
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="liveCategoryDelete"/>", {
                    "categoryId": selectedIds.toString()
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnDeleteBanner", function (event) {
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="bannerDelete"/>", {
                    "bannerId": selectedIds.toString()
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnDeactivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="liveCategoryStatusUpdate"/>", {
                    "categoryId": selectedIds.toString(),
                    "status": "INACTIVE",
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnActivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            selectedIds = [id];
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="liveCategoryStatusUpdate"/>", {
                    "categoryId": selectedIds.toString(),
                    "status": "ACTIVE",
                }, processJsonSave);
            });
        });

        var userGroups = '';
        <s:iterator value="userGroups" >
        userGroups += '<s:property/>' + ',';
        </s:iterator>

        loadDatatables(userGroups);

        $('#categoryListTable').DataTable({
            "destroy": true,
            "order": [[1, "desc"]],
            "ajax": {
                "url": "<s:url action="categoryListDatatables"/>"
            },
            "columns": [
                {
                    "searchable": false,
                    "orderable": false,
                    "className": "dt-body-center",
                    "selectRow": true,
                    'render': function (data, type, full, meta) {
                        return '<input type="checkbox">';
                    }
                },
                {"data": "seq"},
                {"data": "category"},
                {"data": "categoryCn"},
                {"data": "categoryMs"},
                {"data": "statusDesc"},

                {
                    "data": "status",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-danger btn-xs btnDelete"><s:text name="btnDelete"/></button>';

                        if (data === 'INACTIVE') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnActivate"><s:text name="btnActivate"/></button>';
                        } else {
                            result += '&nbsp;<button type="button" class="btn btn-warning btn-xs btnDeactivate"><s:text name="btnDeactivate"/></button>';
                        }

                        return result;
                    }
                },
            ], "select":
                {
                    "style": 'multi'
                },
            "rowId": "categoryId"
        });
    });

    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');

        $("#imageViewerModal").dialog('open');
        $("#imageViewer").attr('src', fileUrl);
    }

    function processJsonSave(json) {
        console.log(json);
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                messageBox.info(json.successMessage, function () {
                    // refresh page
                     window.location = "<s:url action="bannerList"/>";
                });

            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

    function getNextBannerSeq() {
        $.getJSON("<s:url action="getNextBannerSeq"/>", {
            "categoryId" : $("#banner\\.categoryId").val()
        }, function (json) {
            $("#banner\\.seq").val(json.seq);
        });
    }
</script>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <button id="btnAddNew" type="button" class="btn btn-warning">
            <i class="fa fa-check-circle"></i>
            <s:text name="btnAddNewCategory"/>
        </button>
        <div class="well">
            <fieldset><legend><s:text name="title.liveCategoryList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="categoryListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th field="ck" checkbox="true"></th>
                            <th><s:text name="seq"/></th>
                            <th><s:text name="category"/></th>
                            <th><s:text name="categoryCn"/></th>
                            <th><s:text name="categoryMs"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<form id="navForm" method="post">
    <input type="hidden" name="banner.bannerId" id="banner.bannerId" />
</form>

<s:form name="bannerForm" id="bannerForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    <s:select name="categorySearch" id="categorySearch" label="%{getText('category')}" list="liveStreamcategories" listKey="key" listValue="value"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.bannerList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="bannerListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="seq"/></th>
                            <th><s:text name="title"/></th>
                            <th><s:text name="titleCn"/></th>
                            <th><s:text name="titleMs"/></th>
                            <th><s:text name="status"/></th>
                            <th><s:text name="announcementType"/></th>
                            <th><s:text name="Common image"/></th>
                            <th><s:text name="EN image"/></th>
                            <th><s:text name="CN image"/></th>
                            <th><s:text name="MS image"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="announcementModal" class="easyui-dialog" style="width:700px; height:500px; z-index: 200" title="<s:text name="title.announcement"/>" closed="true" resizable="true" maximizable="true">

</div>
<div id="addNewCategoryModal" class="easyui-dialog" style="width:600px; height:350px"
     title="<s:text name="addNewCategory"/>"
     closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">

        <s:form action="addLiveCategory" name="newCategoryForm" id="newCategoryForm" cssClass="form-horizontal">
            <div class="form-group">
                <label class="col-md-3 control-label"> <s:text name="category"/></label>
                <div class="col-md-8">
                    <s:textfield theme="simple" name="category" id="category" size="50" maxlength="50"
                                 cssClass="form-control"/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 control-label"> <s:text name="categoryCn"/></label>
                <div class="col-md-8">
                    <s:textfield theme="simple" name="categoryCn" id="categoryCn" size="100" maxlength="100"
                                 cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"> <s:text name="categoryMs"/></label>
                <div class="col-md-8">
                    <s:textfield theme="simple" name="categoryMs" id="categoryMs" size="100" maxlength="100"
                                 cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-md-3 control-label"> <s:text name="seq"/></label>
                <div class="col-md-8">
                    <s:textfield theme="simple" name="seq" id="banner.seq" size="100" maxlength="100"
                                 cssClass="form-control"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <ce:buttonRow>
                        <button id="btnSave" type="submit" class="btn btn-primary">
                            <i class="icon-save"></i>
                            <s:text name="btnSave"/>
                        </button>
                    </ce:buttonRow>
                </div>
            </div>
        </s:form>
    </div>
</div>
<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true"
     resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>