<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th class="text-center" style="width: 20%"><s:text name="publishDate"/></th>
        <th class="text-center" style="width: 80%"><s:text name="title"/></th>
    </tr>
    </thead>
    <tbody>
    <s:if test="announcements.size()==0">
        <tr><td colspan="2"><div class="alert alert-info"><s:text name="noRecordFound"/></div></td></tr>
    </s:if>
    <s:iterator var="announcement" value="announcements">
        <tr>
            <td>${announcement.publishDate}</td>
            <td><a class="announceShow" href="<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId=${announcement.announceId}">${announcement.title}</a></td>
        </tr>
    </s:iterator>
    </tbody>
</table>