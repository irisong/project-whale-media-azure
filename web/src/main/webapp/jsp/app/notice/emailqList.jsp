<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.emailq" />

<script type="text/javascript">
    function loadDatatables() {
        $('#emailqListTable').DataTable({
            "destroy" : true,
            "order": [[ 0, "desc" ]],
            "ajax": {
                "url" : "<s:url action="emailqListDatatables"/>",
                "data" : {
                    status: $("#status").val()
                }
            },
            "columns" : [
                {"data" : "datetimeAdd"},
                {"data" : "emailTo"},
                {"data" : "title"},
                {"data" : "status"}
            ],
            "rowId": 'docId',
            "columnDefs" : [
                {"render": function(data, type, row) {
                    return $.datagridUtil.formatDate(data);
                }, "targets": [0] },
                {"render": function(data, type, row){
                    return $.datagridUtil.formatStatus(data);
                }, "targets": [3] }
            ]
        });
    }

    $(function(){
        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="emailqAdd" />")
            $("#navForm").submit();
        });

        loadDatatables();

    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="emailq.emailId" id="emailq.emailId" />
</form>

<s:form name="emailqForm" id="emailqForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:select name="status" id="status" label="%{getText('status')}" list="statuses" listKey="key" listValue="value"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
             <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.emailqList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="emailqListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="datetimeAdd"/></th>
                                <th><s:text name="email"/></th>
                                <th><s:text name="title"/></th>
                                <th><s:text name="status"/></th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>