<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');

        $("#imageViewerModal").dialog('open').dialog('vcenter');
        $("#imageViewer").attr('src', fileUrl);
    }
    $(function () {
        $("#helpDeskViewForm").validate({
            submitHandler: function(form){
                form.submit()
            }
        });
    })
</script>

<style>
    #roundBorderUser{
        box-sizing: content-box;
        border: 2px solid lightgrey;
        padding: 10px;
        border-radius: 25px;
        background: lightgrey;
        height: auto;
        width: auto;
    }

    #roundBorderAdmin{
        box-sizing: content-box;
        border: 2px solid lightskyblue;
        padding: 10px;
        border-radius: 25px;
        background: lightskyblue;
        width: auto;
        float: right;
    }

    * {
        box-sizing: border-box;
    }

    .columnLeft{
        float: left;
        width: 60px;
        padding: 5px;
    }

    .columnRight{
        float: right;
        width: 60px;
        padding: 5px;
    }

    /* Clearfix (clear floats) */
    .row::after {
        content: "";
        clear: both;
        display: table;
    }

    .media-right{
        float: right;
    }
</style>

<form id="navForm" method="post">
    <input type="hidden" name="helpDeskReplyFileId" id="helpDeskReplyFileId" />
</form>

<sc:title title="title.helpDeskView"/>

<div class="row">
    <article class="col-sm-12">
        <div class="well">
            <s:form action="helpDeskReply" name="helpDeskViewForm" id="helpDeskViewForm" cssClass="form-horizontal">
                <fieldset>
                    <legend><s:text name="details"/></legend>
                    <s:textfield name="helpDesk.ticketNo" id="helpDesk.ticketNo" label="%{getText('ticketNo')}" readonly="true" />
                    <s:textfield name="helpDeskType" id="helpDeskType" label="%{getText('type')}" readonly="true"/>
                    <s:textfield name="helpDesk.subject" id="helpDesk.subject" label="%{getText('subject')}" readonly="true"/>

                    <legend><s:text name="chatHistory"/></legend>
                    <div class="ibox-content no-padding">
                        <ul class="list-group">
                            <s:iterator status="iterStatus" var="transView" value="helpDesk.helpDeskReplies">
                                <s:if test="#transView.senderType == 'MEMBER' ">
                                    <div class="media roundBorder ">
                                        <div class="media-left media-top">
                                            <a href="#">
                                                <img class="media-object" src="<c:url value="/images/user_icon.png"/>" alt="user_icon">
                                            </a>
                                        </div>
                                        <div class="media-body" id="roundBorderUser">
                                            <h4 class="media-heading" ><s:property value="#transView.sender.username"/></h4>
                                            <h6><s:property value="#transView.datetimeAdd"/></h6>
                                            <p><s:property value="#transView.messageInHtml" escapeHtml="false" /></p>

                                            <div class="row">
                                                <s:iterator status="imageIterStatusView" var="imageFileView" value="#transView.helpDeskReplyFiles">
                                                    <div class="col-xs-6 col-md-3 columnLeft">
                                                        <s:if test="#imageFileView.contentType == 'application/pdf'">
                                                            <a href="${imageFileView.fileUrl}" target="_blank"><img src="<c:url value="/images/pdfFileIcon.png"/>" alt="attach_image" style="width:60px"/></a>
                                                        </s:if>
                                                        <s:else>
                                                            <a onclick="viewImage('${imageFileView.fileUrl}')">
                                                                <img src="<c:url value="/images/imageFileIcon.png"/>" alt="attach_image" style="width:60px"/>
                                                            </a>
                                                        </s:else>
                                                    </div>
                                                </s:iterator>
                                            </div>

                                        </div>
                                    </div>
                                </s:if>
                                <s:else>
                                    <div class="media">
                                        <div class="media-right media-top">
                                            <a href="#">
                                                <img class="media-object" src="<c:url value="/images/admin_icon.png"/>" alt="user_icon">
                                            </a>
                                        </div>

                                        <div class="media-body" id="roundBorderAdmin" style="text-align: right">
                                            <h4 class="media-heading"><s:property value="#transView.sender.username"/></h4>
                                            <h6><s:property value="#transView.datetimeAdd"/></h6>
                                            <p><s:property value="#transView.messageInHtml" escapeHtml="false" /></p>

                                            <div class="row">
                                                <s:iterator status="imageIterStatus" var="imageFileView" value="#transView.helpDeskReplyFiles">
                                                    <div class="col-xs-6 col-md-3 columnRight">
                                                        <s:if test="#imageFileView.contentType == 'application/pdf'">
                                                            <a href="${imageFileView.fileUrl}" target="_blank"><img src="<c:url value="/images/pdfFileIcon.png"/>" alt="attach_image" style="width:60px"/></a>
                                                        </s:if>
                                                        <s:else>
                                                            <a onclick="viewImage('${imageFileView.fileUrl}')">
                                                                <img src="<c:url value="/images/imageFileIcon.png"/>" alt="attach_image" style="width:60px"/>
                                                            </a>
                                                        </s:else>
                                                    </div>
                                                </s:iterator>
                                            </div>

                                        </div>
                                    </div>
                                </s:else>
                            </s:iterator>
                        </ul>
                    </div>

                    <ce:buttonRow>
                        <s:hidden name="ticketId"/>
                        <button id="btnReply" type="submit" class="btn btn-warning">
                            <i class="fa fa-pencil-square-o"></i>
                            <s:text name="btnReply"/>
                        </button>
                        <s:url var="urlExit" action="helpDeskList"/>
                        <ce:buttonExit url="%{urlExit}" type="button" class="btn" theme="simple">
                            <i class="fa fa-times"></i>
                            <s:text name="btnExit"/>
                        </ce:buttonExit>
                    </ce:buttonRow>
                </fieldset>
            </s:form>
        </div>
    </article>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true" resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%;height: 100%">
</div>
