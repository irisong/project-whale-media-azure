    <%@ taglib prefix="s" uri="/struts-tags" %>
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib uri="/struts-custom" prefix="sc" %>
        <%@ taglib uri="/compal-struts-ext" prefix="ce" %>

        <sc:title title="title.smsqAdd"/>

        <script type="text/javascript">
        $(function() {
        $("#smsqForm").validate( {
        submitHandler : function(form) {
        messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
        $("body").loadmask("<s:text name="processing.msg"/>");

        $(form).ajaxSubmit({
        dataType: 'json',
        success: processJsonSave
        });
        });
        }, //submitHandler
        rules: {
        "smsQueue.smsTo":{
        digits : true
        }
        }
        });
        });

        function processJsonSave(json) {
        new JsonStat(json, {
        onSuccess : function(json) {
        $("body").unmask();
        messageBox.info(json.successMessage, function(){
        // refresh page
        window.location = "<s:url action="smsqList"/>"
        });
        }, // onFailure using the default
        onFailure : function(json, error){
        $("body").unmask();
        messageBox.alert(error);
        }
        });
        }
        </script>

        <s:form action="smsqSave" name="smsqForm" id="smsqForm" cssClass="form-horizontal">
            <sc:displayErrorMessage align="center"/>

            <s:textfield name="smsQueue.smsTo" id="smsQueue.smsTo" label="%{getText('to')}" cssClass="input-xxlarge"
                         required="true"/>
            <s:textarea name="smsQueue.body" id="smsQueue.body" theme="simple" required="true" cols="176" rows="20"/>

            <ce:buttonRow>
                <ce:formExtra token="true"/>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple"
                          cssClass="btn btn-primary">
                    <i class="fa fa-floppy-o"></i>
                    <s:text name="btnSave"/>
                </s:submit>
                <s:url var="urlExit" action="smsqList"/>
                <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                    <i class="fa fa-times"></i>
                    <s:text name="btnExit"/>
                </ce:buttonExit>
            </ce:buttonRow>
        </s:form>