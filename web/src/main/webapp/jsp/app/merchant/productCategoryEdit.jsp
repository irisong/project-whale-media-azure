<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.productCategoryEdit"/>

<script type="text/javascript">
    $(function() {
        $("#productCategoryForm").validate({
            submitHandler: function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="productCategoryList"/>";
                });
            }, // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<div class="well">
    <s:form action="updateProductCategory" name="productCategoryForm" id="productCategoryForm" cssClass="form-horizontal">
        <sc:displayErrorMessage align="center"/>
        <s:hidden name="productCategory.categoryId" id="productCategory.categoryId"/>

        <div class="row">
            <article class="col-md-12">
                <s:textfield name="productCategory.category" id="productCategory.category" label="%{getText('category')}" size="100" maxlength="100" cssClass="form-control" required="true"/>
                <s:textfield name="productCategory.categoryCn" id="productCategory.categoryCn" label="%{getText('categoryCn')}" cssClass="form-control" required="true"/>
                <s:textfield name="productCategory.sortOrder" id="productCategory.sortOrder" label="%{getText('sortOrder')}" cssClass="form-control" required="true"/>
                <s:radio name="productCategory.status" id="productCategory.status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value" required="true"/>
            </article>
        </div>

        <ce:buttonRow>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>
            <s:url var="urlExit" action="productCategoryList"/>
            <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple">
                <i class="fa fa-times"></i>
                <s:text name="btnExit"/>
            </ce:buttonExit>
        </ce:buttonRow>
    </s:form>
</div>