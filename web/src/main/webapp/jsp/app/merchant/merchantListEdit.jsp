<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.merchantListEdit"/>

<script type="text/javascript">
    $(function() {
        $("#merchantListSearchForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function() {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="merchantList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<div class="well">
    <s:form action="merchantUpdate" name="merchantListSearchForm" id="merchantListSearchForm" cssClass="form-horizontal">
        <s:hidden name="merchant.merchantId"/>
        <sc:displayErrorMessage align="center"/>
        <s:select name="merchant.type" id="merchant.type" label="%{getText('type')}" list="merchantList" listKey="key" listValue="value" required="true"/>
        <s:textfield name="merchant.name" id="merchant.name" label="%{getText('merchantName')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="merchant.nameCn" id="merchant.nameCn" label="%{getText('merchantNameCn')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="merchant.contactNo" id="merchant.contactNo" label="%{getText('contactNo')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="merchant.email" id="merchant.email" label="%{getText('email')}" cssClass="input-xxlarge" required="true"/>
        <s:textfield name="merchant.licenseNo" id="merchant.licenseNo" label="%{getText('licenseNo')}" cssClass="input-xxlarge" required="true"/>
        <s:radio name="merchant.status" id="merchant.status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value" required="true"/>

        <ce:buttonRow>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>
            <s:url var="urlExit" action="merchantList"/>
            <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
                <i class="fa fa-times"></i>
                <s:text name="btnExit"/>
            </ce:buttonExit>
        </ce:buttonRow>
    </s:form>
</div>