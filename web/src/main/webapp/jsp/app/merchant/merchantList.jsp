<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.merchantList"/>
<script type="text/javascript">
    var table = null;

    $(function () {
        table  = $('#merchantListTable').DataTable({
            "order": [],
            "ajax":{
                "url": "<s:url action="merchantDatatables"/>",
                "data" : {
                    merchantType: $('#merchantType').val(),
                    status: $('#status').val()
                }
            },
            "columns": [
                {"data": "name"},
                {"data": "nameCn"},
                {"data": "contactNo"},
                {"data": "email"},
                {"data": "type"},
                {"data": "licenseNo"},
                {"data": "status"},
                {
                    "data": "status",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';
                        if (data === 'INACTIVE') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnActivate"><s:text name="btnActivate"/></button>';
                        } else {
                            result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnDeactivate"><s:text name="btnDeactivate"/></button>';
                        }

                        return result;
                    }
                },
            ],
            "rowId": "merchantId"
        });

        $("#btnCreate").click(function () {
            $("#newMerchantForm").trigger("reset");
            $("#addNewMerchantModal").dialog('open').dialog('vcenter');
        });

        $(document).on("click", ".btnEdit", function(){
            var id = $(this).parents("tr").attr("id");
            $("#merchant\\.merchantId").val(id);
            $("#navForm").attr("action", "<s:url action="merchantListEdit"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnActivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateMerchantStatus"/>", {
                    "merchantId": id,
                    "status": "ACTIVE",
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnDeactivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateMerchantStatus"/>", {
                    "merchantId": id,
                    "status": "INACTIVE",
                }, processJsonSave);
            });
        });

        $("#newMerchantForm").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: processJsonSave
                });
            }
        });

    })

    function processJsonSave(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                messageBox.info(json.successMessage, function () {
                    $("#merchantListSearchForm").submit();
                });
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="merchant.merchantId" id="merchant.merchantId"/>
</form>

<s:form name="merchantListSearchForm" id="merchantListSearchForm" cssClass="form-horizontal">
    <s:select name="merchantType" id="merchantType" list="allMerchantsList" label="%{getText('type')}" listKey="key" listValue="value"/>
    <s:select name="status" id="status" list="allStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.merchantList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="merchantListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="merchantName"/></th>
                            <th><s:text name="merchantNameCn"/></th>
                            <th><s:text name="contactNo"/></th>
                            <th><s:text name="email"/></th>
                            <th><s:text name="type"/></th>
                            <th><s:text name="licenseNo"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="addNewMerchantModal" class="easyui-dialog" style="width:600px; height:600px" title="<s:text name="addNewMerchant"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="saveMerchant" name="newMerchantForm" id="newMerchantForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <s:select name="merchant.type" id="merchant.type" label="%{getText('type')}" list="merchantList" listKey="key" listValue="value" required="true"/>
                    <s:textfield name="merchant.name" id="merchant.name" label="%{getText('merchantName')}" cssClass="input-xxlarge" required="true"/>
                    <s:textfield name="merchant.nameCn" id="merchant.nameCn" label="%{getText('merchantNameCn')}" cssClass="input-xxlarge" required="true"/>
                    <s:textfield name="merchant.contactNo" id="merchant.contactNo" label="%{getText('contactNo')}" cssClass="input-xxlarge" required="true"/>
                    <s:textfield name="merchant.email" id="merchant.email" label="%{getText('email')}" cssClass="input-xxlarge" required="true"/>
                    <s:textfield name="merchant.licenseNo" id="merchant.licenseNo" label="%{getText('licenseNo')}" cssClass="input-xxlarge" required="true"/>
                    <ce:buttonRow>
                        <button id="btnSaveItem" class="btn btn-primary" type="submit">
                            <i class="fa fa-plus-circle"></i>
                            <s:text name="btnSave"/>
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>