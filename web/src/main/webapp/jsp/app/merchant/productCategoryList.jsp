<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.productCategoryList"/>
<script type="text/javascript">
    var catTable = null;

    $(function () {
        catTable = $('#productCategoryList').DataTable({
            "order": [0, "desc"],
            "ajax": {
                "url": "<s:url action="productCategoryDatatables"/>",
                "data" : {
                    status: $('#status').val()
                }
            },
            "columns": [
                {"data": "sortOrder"},
                {"data": "category"},
                {"data": "categoryCn"},
                {"data": "status"},
                {
                    "data": "status",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';

                        if (data === 'INACTIVE') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnActivate"><s:text name="btnActivate"/></button>';
                        } else {
                            result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnDeactivate"><s:text name="btnDeactivate"/></button>';
                        }

                        return result;
                    }
                },
            ],
            "rowId": "categoryId"
        });

        $("#newCategoryForm").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: processJsonSave
                });
            },
            rules: {
                "productCategory.sortOrder": { digits: true }
            }
        });

        $("#btnAddNew").click(function () {
            $("#newCategoryForm").trigger("reset");

            $.getJSON("<s:url action="getNextProductCategorySortOrder"/>", function (json) {
                $("#productCategory\\.sortOrder").val(json.sortOrder);
            });

            $("#addNewCategoryModal").dialog('open').dialog('vcenter');
        });

        $(document).on("click", ".btnEdit", function() {
            var id = $(this).parents("tr").attr("id");
            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="editProductCategory"/>");
            $("#navForm").submit();
        });

        $(document).on("click", ".btnActivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateCategoryStatus"/>", {
                    "categoryId": id,
                    "status": "ACTIVE",
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnDeactivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateCategoryStatus"/>", {
                    "categoryId": id,
                    "status": "INACTIVE",
                }, processJsonSave);
            });
        });

        function processJsonSave(json) {
            $("body").unmask();

            new JsonStat(json, {
                onSuccess: function (json) {
                    messageBox.info(json.successMessage, function () {
                        $("#productCategoryListSearchForm").submit();
                        <%--window.location = "<s:url action="productCategoryList"/>";--%>
                    });
                },
                onFailure: function (json, error) {
                    messageBox.alert(error);
                }
            });
        }
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="id" id="id"/>
</form>

<s:form name="productCategoryListSearchForm" id="productCategoryListSearchForm" cssClass="form-horizontal">
    <s:select name="status" id="status" list="allStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnAddNew" type="button" class="btn btn-info">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnAddNewCategory"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.productCategory"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="productCategoryList" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="sortOrder"/></th>
                            <th><s:text name="category"/></th>
                            <th><s:text name="categoryCn"/></th>
                            <th><s:text name="status"/></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="addNewCategoryModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="addNewCategory"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="saveProductCategory" name="newCategoryForm" id="newCategoryForm" cssClass="form-horizontal">
                <s:textfield name="productCategory.category" id="productCategory.category" label="%{getText('category')}" size="100" maxlength="100" required="true"/>
                <s:textfield name="productCategory.categoryCn" id="productCategory.categoryCn" label="%{getText('categoryCn')}" required="true"/>
                <s:textfield name="productCategory.sortOrder" id="productCategory.sortOrder" label="%{getText('sortOrder')}" required="true"/>

                <div class="form-group">
                    <div class="col-md-6 col-md-offset-3">
                        <ce:buttonRow>
                            <button id="btnSave" type="submit" class="btn btn-primary">
                                <i class="icon-save"></i>
                                <s:text name="btnSave"/>
                            </button>
                        </ce:buttonRow>
                    </div>
                </div>
            </s:form>
        </div>
    </div>
</div>
