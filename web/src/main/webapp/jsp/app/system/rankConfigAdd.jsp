<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.rankAdd" />

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">

$(function() {

    $("#btnExit").click(function(){
        $.ajax({
            success: proceedExit
        });
    });

        $("#rankConfigForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "rankConfig.rankFrom": {
                    digits: true,
                    required: true
                },
                "rankConfig.rankTo": {
                    digits: true,
                    required: true
                },
                "rankConfig.perExp": {
                    digits: true,
                    required: true
                },
                "rankConfig.rewardPercent": {
                    required: true
                },
                "rankConfig.remark": "required"
            }
        });


    });

    function checkDefault(defaultRecord) {
        if (defaultRecord.checked) {
            $('input#rankConfig\\.startDate').prop('disabled', true);
            $('input#rankConfig\\.endDate').prop('disabled', true);
        } else {
            $('input#rankConfig\\.startDate').removeAttr('disabled');
            $('input#rankConfig\\.endDate').removeAttr('disabled');
        }
    }

    function proceedExit() {
        window.location = "<s:url action="rankConfigList"/>";
    }


    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="rankConfigList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="rankConfigSave" name="rankConfigForm" id="rankConfigForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <%--    <s:hidden name="banner.bannerId" id="banner.bannerId"/>--%>
    <%--    <input type="hidden" class="hiddenMemberId" name="memberId"/>--%>

    <div class="row">
        <article class="col-md-12">
            <s:textfield name="rankConfig.rankFrom" id="rankConfig.rankFrom" label="%{getText('rankFrom')}" cssClass="input-xxlarge"/>
            <s:textfield name="rankConfig.rankTo" id="rankConfig.rankTo" label="%{getText('rankTo')}" cssClass="input-xxlarge"/>
            <s:textfield name="rankConfig.perExp" id="rankConfig.perExp" label="%{getText('perExp')}" cssClass="input-xxlarge"/>
            <s:textfield name="rankConfig.rewardPercent" id="rankConfig.rewardPercent" label="%{getText('rewardPercent')}" cssClass="input-xxlarge"/>
            <ce:datepicker name="rankConfig.startDate" id="rankConfig.startDate" label="%{getText('startDate')}" cssStyle="z-index: 100 !important;" />
            <ce:datepicker name="rankConfig.endDate" id="rankConfig.endDate" label="%{getText('endDate')}" cssStyle="z-index: 100 !important;" />
            <s:textfield name="rankConfig.remark" id="rankConfig.remark" label="%{getText('label_remarks')}" cssClass="input-xxlarge"/>

            <div class="form-group">
                <label class="col-md-3 col-sm-3 control-label"><s:text name="Default Record"/>:</label>
                <div class="col-md-9 col-sm-9">
                    <input type="checkbox" id="defaultRecord" name="defaultRecord" onchange="checkDefault(this)">
                </div>
            </div>

        </article>

    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>


        <button id="btnExit" type="button" class="btn">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </button>

    </ce:buttonRow>
</s:form>
<!-- #modal-dialog -->
