<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#fansIntimacyConfigForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "fansIntimacyConfig.fansLevel": {
                    digits: true,
                    required: true
                },
                "fansIntimacyConfig.levelUpValue": {
                    digits: true,
                    required: true
                },
                "fansIntimacyConfig.maxPerDay": {
                    digits: true,
                    required: true
                },
                "fansIntimacyConfig.deductPercent": {
                    digits: true,
                    min: 1,
                    max: 100,
                    required: true
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="fansIntimacyConfigList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<sc:title title="title.fansIntimacyConfigAdd"/>

<s:form action="fansIntimacyConfigSave" name="fansIntimacyConfigForm" id="fansIntimacyConfigForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div class="row">
        <article class="col-md-12">
            <s:textfield name="fansIntimacyConfig.fansLevel" id="fansIntimacyConfig.fansLevel" label="%{getText('fansLevel')}" cssClass="input-xxlarge"/>
            <s:textfield name="fansIntimacyConfig.deductPercent" id="fansIntimacyConfig.deductPercent" label="%{getText('deductPercent')}" cssClass="input-xxlarge"/>
            <s:textfield name="fansIntimacyConfig.levelUpValue" id="fansIntimacyConfig.levelUpValue" label="%{getText('levelUpValue')}" cssClass="input-xxlarge"/>
            <s:textfield name="fansIntimacyConfig.maxPerDay" id="fansIntimacyConfig.maxPerDay" label="%{getText('maxPerDay')}" cssClass="input-xxlarge"/>
            <s:textfield name="fansIntimacyConfig.remark" id="fansIntimacyConfig.remark" label="%{getText('label_remarks')}" cssClass="input-xxlarge"/>
        </article>
    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>

        <s:url var="urlExit" action="fansIntimacyConfigList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
