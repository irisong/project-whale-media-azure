<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.processTask.view" />

<script type="text/javascript">
    $(function() {
        $("#btnViewLog").click(function(event){
            $("#viewDebugDiv").hide();
            $("#logDiv").load("<s:url action="backendShowLog"/>?taskId=<s:property value="runTask.taskId"/>&displayLog=true");
            $("#viewLogDiv").show();
        });

        $("#btnViewDebug").click(function(event){
            $("#viewLogDiv").hide();
            $("#debugDiv").load("<s:url action="backendShowLog"/>?taskId=<s:property value="runTask.taskId"/>&displayLog=false");
            $("#viewDebugDiv").show();
        });
    });
</script>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="well">
            <table class="table table-striped table-bordered table-condensed">
                <tbody>
                <tr>
                    <th><s:text name="processCode"/></th><td><s:property value="runTask.taskCode"/></td>
                </tr>
                <tr>
                    <th><s:text name="processName"/></th><td><s:property value="taskConfig.name"/></td>
                </tr>
                <tr>
                    <th><s:text name="description"/></th><td><s:property value="taskConfig.description"/></td>
                </tr>
                <tr>
                    <th><s:text name="startTime"/></th><td><s:property value="%{@SF@formatDateTime(runTask.startDatetime)}"/></td>
                </tr>
                <tr>
                    <th><s:text name="endTime"/></th><td><s:property value="%{@SF@formatDateTime(runTask.endDatetime)}"/></td>
                </tr>
                <tr>
                    <th><s:text name="doneBy"/></th><td><s:property value="runTask.addByUser.username"/></td>
                </tr>
                <s:if test="runTask.enableLog">
                    <tr>
                        <th><s:text name="logFile"/></th>
                        <td>
                            <button class="btn btn-link btn-small" id="btnViewLog"><s:text name="btnViewLog"/></button>
                            <button class="btn btn-link btn-small" id="btnViewDebug"><s:text name="btnViewDebug"/></button>
                        </td>
                    </tr>
                </s:if>
                </tbody>
            </table>
        </div>
    </div>
</div>

<div id="viewLogDiv" class="well" style="display: none">
    <div class="widget-header">
        <h5><strong><s:text name="logFileViewer"/></strong></h5>
    </div>
    <div class="widget-body">
        <div class="widget-main" id="logDiv">
        </div>
    </div>
</div>

<div id="viewDebugDiv" class="well" style="display: none">
    <div class="widget-header">
        <h5><strong><s:text name="debugFileViewer"/></strong></h5>
    </div>
    <div class="widget-body">
        <div class="widget-main" id="debugDiv">
        </div>
    </div>
</div>
