<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.giftCategoryEdit"/>

<script type="text/javascript">
    $(function() {
        $("#symbolicForm").validate( {
            submitHandler: function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            },
            rules: {
                "symbolic.seq": {
                    digits: true
                }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="liveStreamGift"/>";
                });
            }, // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="updateSymbolic" name="symbolicForm" id="symbolicForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:hidden name="symbolic.symbolicId" id="symbolic.symbolicId"/>

    <div class="row">
        <article class="col-md-12">
            <s:textfield name="symbolic.category" id="symbolic.category" label="%{getText('category')}" size="100" maxlength="100" cssClass="form-control" required="true"/>
            <s:textfield name="symbolic.categoryCn" id="symbolic.categoryCn" label="%{getText('categoryCn')}" cssClass="form-control" required="true"/>
            <s:textfield name="symbolic.seq" id="symbolic.seq" label="%{getText('seq')}" cssClass="form-control" required="true"/>
        </article>
    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="liveStreamGift"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple" >
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>