<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.rankAdd" />

<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/ckeditor.js"/>"></script>
<script type="text/javascript" src="<c:url value="/smartadmin/js/plugin/ckeditor/adapters/jquery.js"/>"></script>

<script type="text/javascript">

$(function() {

    $("#btnExit").click(function(){
        $.ajax({
            success: proceedExit
        });
    });

        $("#broadcastConfigForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "broadcastConfig.rankName": {
                    required: true
                },
                "broadcastConfig.basicSalary": {
                    digits: true,
                    required: true
                },
                "broadcastConfig.minDuration": {
                    digits: true,
                    required: true
                },
                "broadcastConfig.minPoint": {
                    digits: true,
                    required: true
                },
                "broadcastConfig.splitPercentage": {
                    digits: true,
                    required: true
                },
                "broadcastConfig.minDay":  {
                    digits: true,
                    required: true
                }
            }
        });


    });

    // function checkDefault(defaultRecord) {
    //     if (defaultRecord.checked) {
    //         $('input#rankConfig\\.startDate').prop('disabled', true);
    //         $('input#rankConfig\\.endDate').prop('disabled', true);
    //     } else {
    //         $('input#rankConfig\\.startDate').removeAttr('disabled');
    //         $('input#rankConfig\\.endDate').removeAttr('disabled');
    //     }
    // }

    function proceedExit() {
        window.location = "<s:url action="broadCastConfigList"/>";
    }


    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="broadCastConfigList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="broadCastConfigSave" name="broadcastConfigForm" id="broadcastConfigForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <%--    <s:hidden name="banner.bannerId" id="banner.bannerId"/>--%>
    <%--    <input type="hidden" class="hiddenMemberId" name="memberId"/>--%>

    <div class="row">
        <article class="col-md-12">
            <s:select name="broadcastConfig.rankName" id="broadcastConfig.rankName" label="%{getText('rank')}" list="rankList" listKey="key" listValue="value"/>
            <s:textfield name="broadcastConfig.basicSalary" id="broadcastConfig.basicSalary" label="%{getText('basicSalary')}" cssClass="input-xxlarge"/>
            <s:textfield name="broadcastConfig.minDuration" id="broadcastConfig.minDuration" label="%{getText('minDuration')}" cssClass="input-xxlarge"/>
            <s:textfield name="broadcastConfig.minPoint" id="broadcastConfig.minPoint" label="%{getText('minPoint')}" cssClass="input-xxlarge" />
            <s:textfield name="broadcastConfig.minDay" id="broadcastConfig.minDay" label="%{getText('minDay')}" cssClass="input-xxlarge"/>
            <s:textfield name="broadcastConfig.splitPercentage" id="broadcastConfig.splitPercentage" label="%{getText('splitPercentage')}" cssClass="input-xxlarge"/>

        </article>

    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>


        <button id="btnExit" type="button" class="btn">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </button>

    </ce:buttonRow>
</s:form>
<!-- #modal-dialog -->
