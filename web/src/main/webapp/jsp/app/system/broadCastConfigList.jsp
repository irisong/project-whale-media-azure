<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.broadcast" />

<script type="text/javascript">
    function loadDatatables() {
        $('#broadcastListTable').DataTable({
            "destroy": true,
            "order": [[0, "asc"],[1, "asc"]],
            "ajax": {
                "url": "<s:url action="broadCastConfigListDatatables"/>",
                "data": {
                    rankSearch : $('#broadcastConfig\\.rank').val()

                }
            },
            "columns": [
                {"data": "rankName"},
                {"data": "basicSalary"},
                {"data": "minDuration"},
                {"data": "minDay"},
                {"data": "minPoint"},
                {"data": "splitPercentage"},
                {
                    "data": "id",
                    "className": "text-nowrap text-center",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "id"
        });

        $(document).on("click", ".btnEdit", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#broadcastConfig\\.id").val(id);
            $("#navForm").attr("action", "<s:url action="broadCastConfigEdit" />")
            $("#navForm").submit();
        });
    }

    $(function(){
        // make publish groups checkbox display inline


        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="broadCastConfigAdd" />")
            $("#navForm").submit();
        });


       var userGroups = '';
       <s:iterator value="userGroups" >
           userGroups += '<s:property/>' + ',';
       </s:iterator>

       loadDatatables();
    }); // end $(function())

</script>
<form id="navForm" method="post">
    <input type="hidden" name="broadcastConfig.id" id="broadcastConfig.id" />
</form>
<s:form name="broadcastConfigForm" id="broadcastConfigForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:select name="broadcastConfig.rank" id="broadcastConfig.rank" label="%{getText('rank')}" list="rankList" listKey="key" listValue="value"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>

    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.rankConfigList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="broadcastListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="rank"/></th>
                                <th><s:text name="basicSalary"/></th>
                                <th><s:text name="minDuration"/></th>
                                <th><s:text name="minDay"/></th>
                                <th><s:text name="minPoint"/></th>
                                <th><s:text name="splitPercentage"/></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

