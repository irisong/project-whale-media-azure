<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="ACT_WHO_IS_ONLINE" />

<div class="well">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="col-lg-3 col-md-3 col-sm-4 col-xs-4"><s:text name="loginTime"/></th>
                    <th class="col-lg-9 col-md-9 col-sm-8 col-xs-8"><s:text name="username"/></th>
                </tr>
            </thead>
            <tbody>
            <s:if test="loginInfos.size()==0">
                <tr>
                    <td colspan="2"><div class="text-center"><s:text name="noRecordFound"/></div></td>
                </tr>
            </s:if>
            <s:iterator var="tempLoginInfo" value="loginInfos">
                <tr>
                    <td><s:date name="#tempLoginInfo.loginDatetime" format="%{getText('default.server.datetime.12.format')}"/></td>
                    <td>${tempLoginInfo.user.username}</td>
                </tr>
            </s:iterator>
            </tbody>
        </table>
    </div>
</div>