<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="ACT_LOGIN_LOG"/>

<script type="text/javascript">
    function loadDatatables() {
        $('#loginLogListTable').DataTable({
            "destroy" : true,
            "order": [[ 3, "desc" ]],
            "ajax": {
                "url" : "<s:url action="loginLogListDatatables"/>",
                "data" : {
                    username: $('#username').val(),
                    ipAddress: $('#ipAddress').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    status: $('#status').val()
                }
            },
            "columns" : [
                {"data" : "username"},
                {"data" : "ipAddress"},
                {"data" : "loginStatus"},
                {"data" : "datetimeAdd"}
            ],
            "rowId": 'sessionId',
            "columnDefs" : [
                {"render": function(data, type, row) {
                    return $.datagridUtil.formatDate(data);
                }, "targets": [3] },
                {"render": function(data, type, row){
                    return $.datagridUtil.formatStatus(data);
                }, "targets": [2] }
            ]
        });
    }

    var action = "list";

    $(function() {
        $("#loginLogForm").validate({
            submitHandler: function(form) {
                if (action =='list') {
                    loadDatatables();
                } else {
                    form.action="<s:url action="loginLogJasper" />";
                    form.submit();
                }
            }
        });

        $("#btnSearch").click(function(event){
            action = "list";
        });

        $("#btnReport").click(function(event){
            // stop event
            event.preventDefault();

            action = "report";
            $("#loginLogForm").submit();
        });

        loadDatatables();

    }); // end $(function())
</script>

<form id="navForm" method="post">
    <input type="hidden" name="member.memberId" id="member.memberId" />
</form>

<s:form name="loginLogForm" id="loginLogForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:textfield name="username" id="username" label="%{getText('username')}"/>
    <s:textfield name="ipAddress" id="ipAddress" label="%{getText('ipAddress')}"/>
    <s:select name="status" id="status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value"/>
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="date"/>
    <sc:submitData/>

    <ce:buttonRow>
        <div class="btn-group">
            <button id="btnSearch" type="submit" class="btn btn-success">
                <i class="fa fa-search"></i>
                <s:text name="btnSearch"/>
            </button>
        </div>
        <div class="btn-group">
            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                <i class="icon-tasks"></i>
                <s:text name="reports"/> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a id="btnReport" href="#"><s:text name="loginLogReport"/></a></li>
            </ul>
        </div>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.loginLogList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="loginLogListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="username"/></th>
                                <th><s:text name="ipAddress"/></th>
                                <th><s:text name="status"/></th>
                                <th><s:text name="datetimeAdd"/></th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>