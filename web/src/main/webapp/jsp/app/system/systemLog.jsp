<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="ACT_SYSTEM_LOG"/>

<script type="text/javascript">
    $(function(){
        $("a.viewHistory").click(function(event){
            // stop event
            event.preventDefault();

            var contentType = $(this).parents("tr").attr("contenttype");

            $("body").loadmask("<s:text name="processing.msg"/>");

            $("#logHistoryDiv").load("<s:url action="systemLogList"/>", {
                "contentType": contentType
            }, function(){
                $("body").unmask();
                $("#listDiv").show();
            });
        });

        $("body").delegate('a.viewLink', 'click', function(event){
            // stop event
            event.preventDefault();

            var id = $(this).parents("tr").attr("rowid");
            var contentType = $(this).parents("tr").attr("contenttype");

            $("body").loadmask("<s:text name="processing.msg"/>");

            $("#logDiv").load("<s:url action="systemLogShow"/>", {
                "contentType": contentType,
                "logFileName": id
            }, function(){
                $("body").unmask();
                $("#logModal").dialog('open');
            });
        });
    });
</script>

<div class="well">
    <div class="table-responsive">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th class="col-md-4 col-sm-4"><s:text name="events"/></th>
                    <th class="col-md-4 col-sm-4"><s:text name="logFile"/></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr rowid="debug.log" contenttype="debug">
                    <td><s:text name="systemDebugger"/></td>
                    <td>debug.log</td>
                    <td>
                        <a class="viewLink txt-color-blue font-md" title="<s:text name="btnView"/>" href="#">
                            <i class="fa fa-file-o"></i>
                        </a>
                        <a class="viewHistory txt-color-green font-md" title="<s:text name="btnHistory"/>" href="#">
                            <i class="fa fa-briefcase"></i>
                        </a>
                    </td>
                </tr>
                <tr rowid="error.log" contenttype="error">
                    <td><s:text name="systemErrorLog"/></td>
                    <td>error.log</td>
                    <td>
                        <a class="viewLink txt-color-blue font-md" title="<s:text name="btnView"/>" href="#">
                            <i class="fa fa-file-o"></i>
                        </a>
                        <a class="viewHistory txt-color-green font-md" title="<s:text name="btnHistory"/>" href="#">
                            <i class="fa fa-briefcase"></i>
                        </a>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>

<div class="row" id="listDiv" style="display: none">
    <article class="col-md-12">
        <sc:widget id="widget11" title="logHistory" cssClass="jarviswidget-color-white">
            <div id="logHistoryDiv">
        </sc:widget>
    </article>
</div>

<div id="logModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="logFileViewer"/>" closed="true" resizable="true" maximizable="true">
    <div id="logDiv"></div>
</div>