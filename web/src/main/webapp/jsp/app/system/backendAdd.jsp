<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:wizardScript/>

<sc:title title="title.runProcessTask" />

<script type="text/javascript">
    var taskDescs = [
        <s:iterator status="iterStatus" var="task" value="taskConfigs">
            ["${task.name}", "${task.description}"]<s:if test="!#iterStatus.last">,</s:if>
        </s:iterator>
    ];


    $(function() {
        // fuelux wizard
        var wizard = $('.wizard').wizard();

        wizard.on('finished.fu.wizard', function(event, data){
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                $("body").loadmask("<s:text name="processing.msg"/>");

                var index = $("#processCode")[0].selectedIndex;

                $.post('<s:url action="backendSave"/>',
                    $("#step2_"+index).serialize(),
                    function(json){
                        $("body").unmask();

                        new JsonStat(json, {
                            onSuccess : function(json) {
                                messageBox.info(json.successMessage, function(){
                                    // refresh page
                                    window.location = "<s:url action="backendShow"/>?taskId="+json.taskId;
                                });
                            },
                            onFailure : function(json, error) {
                                messageBox.alert(error);
                            }
                        });
                    }
                );
            });
        });

        $("#processCode").change(function(event){
            // hide all step2form
            $(".step2form").hide();

            // get DOM element.
            var index = $(this)[0].selectedIndex;
            $("#processName").val(taskDescs[index][0]);
            $("#processDesc").val(taskDescs[index][1]);
            // show form
            $("#step2_"+index).show();
        }).change();

    });
</script>

<sc:displayErrorMessage align="center" />

<s:if test="ableToRun">
<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well fuelux">
            <div class="wizard">
                <div class="steps-container">
                    <ul class="steps">
                        <li data-step="1" class="active">
                            <span class="badge badge-info">1</span><s:text name="selectProcess"/> <span class="chevron"></span>
                        </li>
                        <li data-step="2">
                            <span class="badge">2</span><s:text name="review"/><span class="chevron"></span>
                        </li>
                    </ul>
                </div>
                <div class="actions">
                    <button type="button" class="btn btn-sm btn-primary btn-prev">
                        <i class="fa fa-arrow-left"></i><s:text name="btnPrev"/>
                    </button>
                    <button type="button" class="btn btn-sm btn-success btn-next" data-last="<s:text name="btnConfirm"/>">
                        <s:text name="btnNext"/><i class="fa fa-arrow-right"></i>
                    </button>
                </div>

                <div class="step-content">
                    <div class="step-pane active" data-step="1">
                        <h3>
                            <strong><s:text name="wizardStep"><s:param name="value" value="1"/></s:text></strong>
                            - <s:text name="pleaseSelectYourProcess"/>
                        </h3>

                        <form class="form-horizontal">
                            <s:select name="processCode" id="processCode" label="%{getText('processCode')}" required="true" list="taskConfigs" listKey="code" listValue="%{getCode() + ' - ' + getName()}" cssClass="input-xxlarge"/>
                            <s:textfield name="processName" id="processName" label="%{getText('processName')}" readonly="true" cssClass="input-xxlarge"/>
                            <s:textarea name="processDesc" id="processDesc" label="%{getText('description')}" readonly="true" cssClass="input-xxlarge" rows="3"/>
                        </form>
                    </div>

                    <div class="step-pane" data-step="2">
                        <h3>
                            <strong><s:text name="wizardStep"><s:param name="value" value="2"/></s:text></strong>
                            - <s:text name="review"/>
                        </h3>

                        <s:iterator status="iterStatus" var="task" value="taskConfigs">
                            <form class="form-horizontal step2form" name="step2_${iterStatus.index}" id="step2_${iterStatus.index}">
                                <s:textfield name="processCode" id="processCode" label="%{getText('processCode')}" readonly="true" cssClass="input-xxlarge" value="%{#task.code}"/>
                                <s:textfield name="processName" id="processName" label="%{getText('processName')}" readonly="true" cssClass="input-xxlarge" value="%{#task.name}"/>
                                <s:textarea name="processDesc" id="processDesc" label="%{getText('description')}" readonly="true" cssClass="input-xxlarge" rows="3" value="%{#task.description}"/>

                                <s:iterator status="iterStatus2" var="parameter" value="#task.parameters">
                                    <s:textfield name="%{#parameter.name}" id="%{#parameter.name}" label="%{#parameter.label}" value="%{#parameter.defaultValue}" />
                                </s:iterator>
                            </form>
                        </s:iterator>
                    </div>
                </div>

            </div>
        </div>
    </article>
</div>
</s:if>