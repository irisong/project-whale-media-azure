<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.rankConfig" />

<script type="text/javascript">
    function loadDatatables() {
        $('#rankListTable').DataTable({
            "destroy": true,
            "order": [[0, "asc"],[1, "asc"]],
            "ajax": {
                "url": "<s:url action="rankConfigListDatatables"/>",
                "data": {
                    dateFrom : $('#dateFrom').val(),
                    dateTo : $('#dateTo').val()
                }
            },
            "columns": [
                {"data": "rankFrom"},
                {"data": "rankTo"},
                {"data": "perExp"},
                {"data": "rewardPercent"},
                {"data": "startDate"},
                {"data": "endDate"},
                {"data": "remark"},
                {
                    "data": "rankId",
                    "className": "text-nowrap text-center",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "rankId"
        });

        $(document).on("click", ".btnEdit", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#rankConfig\\.rankId").val(id);
            $("#navForm").attr("action", "<s:url action="rankConfigEdit" />")
            $("#navForm").submit();
        });
    }

    $(function(){
        // make publish groups checkbox display inline


        $("#btnCreate").click(function(event){
            $("#navForm").attr("action", "<s:url action="rankConfigAdd" />")
            $("#navForm").submit();
        });


       var userGroups = '';
       <s:iterator value="userGroups" >
           userGroups += '<s:property/>' + ',';
       </s:iterator>

       loadDatatables();
    }); // end $(function())

</script>
<form id="navForm" method="post">
    <input type="hidden" name="rankConfig.rankId" id="rankConfig.rankId" />
</form>
<s:form name="rankConfigForm" id="rankConfigForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="date"/>
    <sc:submitData/>
    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>

    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.rankConfigList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="rankListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="rankFrom"/></th>
                                <th><s:text name="rankTo"/></th>
                                <th><s:text name="perExp"/></th>
                                <th><s:text name="rewardPercent"/></th>
                                <th><s:text name="startDate"/></th>
                                <th><s:text name="endDate"/></th>
                                <th><s:text name="label_remarks"/></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

