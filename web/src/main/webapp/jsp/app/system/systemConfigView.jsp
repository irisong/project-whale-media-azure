<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.systemConfiguration" />

<script type="text/javascript">
    function defaultJsonCallback(json){
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                location.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

    $(function () {
        $("#btnEditRegister").click(function (event) {
            <s:if test="%{systemConfig.memberRegister}">
            var msg = "<s:text name="doYouWantToDisableMemberRegister"/>";
            </s:if>
            <s:else>
            var msg = "<s:text name="doYouWantToEnableMemberRegister"/>";
            </s:else>
            messageBox.confirm(msg, function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="systemConfigToogleRegister"/>',
                    function (json) {
                        $("body").unmask();

                        new JsonStat(json, {
                            onSuccess: function (json) {
                                location.reload();
                            },
                            onFailure: function (json, error) {
                                messageBox.alert(error);
                            }
                        });
                    });
            });
        });

        $("#btnEditLogin").click(function (event) {
            <s:if test="%{systemConfig.memberRegister}">
            var msg = "<s:text name="doYouWantToDisableMemberLogin"/>";
            </s:if>
            <s:else>
            var msg = "<s:text name="doYouWantToEnableMemberLogin"/>";
            </s:else>
            messageBox.confirm(msg, function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="systemConfigToogleLogin"/>',
                    function (json) {
                        $("body").unmask();

                        new JsonStat(json, {
                            onSuccess: function (json) {
                                location.reload();
                            },
                            onFailure: function (json, error) {
                                messageBox.alert(error);
                            }
                        });
                    });
            });
        });

        $("#btnEditTransfer").click(function(event){
            <s:if test="%{walletConfig.walletTransfer}">
            var msg = "<s:text name="doYouWantToDisableWalletTransfer"/>";
            </s:if>
            <s:else>
            var msg = "<s:text name="doYouWantToEnableWalletTransfer"/>";
            </s:else>
            messageBox.confirm(msg, function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="systemConfigToogleTransfer"/>',
                    function (json) {
                        $("body").unmask();

                        new JsonStat(json, {
                            onSuccess: function (json) {
                                location.reload();
                            },
                            onFailure: function (json, error) {
                                messageBox.alert(error);
                            }
                        });
                    });
            });
        });

        $("#btnEditApplicationName").click(function(event){
            $("#editApplicationNameModal").removeClass('hide');
            $("#editApplicationNameModal").dialog('open');
        });

        $("#btnCancelEditApplicationName").click(function(){
            $("#editApplicationNameModal").dialog('close');
        });

        $("#btnEditCompanyName").click(function(event){
            $("#editCompanyNameModal").removeClass('hide');
            $("#editCompanyNameModal").dialog('open');
        });

        $("#btnCancelEditCompanyName").click(function(){
            $("#editCompanyNameModal").dialog('close');
        });

        $("#btnEditSupportEmail").click(function(event){
            $("#editSupportEmailModal").removeClass('hide');
            $("#editSupportEmailModal").dialog('open');
        });

        $("#btnCancelEditSupportEmail").click(function(){
            $("#editSupportEmailModal").dialog('close');
        });

        $("#btnEditQrCodeExpiry").click(function(event){
            $("#editQrCodeExpiryModal").removeClass('hide');
            $("#editQrCodeExpiryModal").dialog('open');
        });

        $("#btnCancelEditQrCodeExpiry").click(function(){
            $("#editQrCodeExpiryModal").dialog('close');
        });

        $("#btnEditDefaultCountry").click(function(event){
            $("#editDefaultCountryModal").removeClass('hide');
            $("#editDefaultCountryModal").dialog('open');
        });

        $("#btnCancelDefaultCountry").click(function(){
            $("#editDefaultCountryModal").dialog('close');
        });

        $("#btnAddSystemConfigImage").click(function(event){
            $("#addSystemConfigImageModal").removeClass('hide');
            $("#addSystemConfigImageModal").dialog('open');
        });

        $("#btnCancelAddSystemConfigImage").click(function(){
            $("#addSystemConfigImageModal").dialog('close');
        });

        $("#applicationNameForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "systemConfig.applicationName": {
                    required: true
                }
            }
        });

        $("#companyNameForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "systemConfig.companyName": {
                    required: true
                }
            }
        });

        $("#supportEmailForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "systemConfig.supportEmail": {
                    required: true
                }
            }
        });

        $("#QrCodeExpiryForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "systemConfig.QrCodeExpiry": {
                    required: true
                }
            }
        });

        $("#countryForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "systemConfig.countryCode": {
                    required: true
                }
            }
        });

        $("#imageForm").validate({
            submitHandler : function(form) {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: defaultJsonCallback
                });
            }, // submitHandler
            rules: {
                "fileUpload": {
                    required: true
                }
            }
        });

        $(".btnDeleteSystemConfigImage").click(function (event) {
             var imageId = $(this).parents("tr").attr("imageId");
             messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post('<s:url action="systemConfigImageDelete"/>', {
                    "imageId" : imageId
                }, defaultJsonCallback);
            });
        });
    });
</script>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="membership"/></legend>
            </fieldset>

            <dl class="dl-horizontal">
                <dt><s:text name="enableMemberRegistration"/></dt>
                <dd>
                    <s:if test="%{systemConfig.memberRegister}">
                        <i class="fa fa-check btn-success"></i>
                    </s:if>
                    <s:else>
                        <i class="fa fa-times btn-danger"></i>
                    </s:else>
                    <button id="btnEditRegister" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="change"/>
                    </button>
                </dd>
                <dt><s:text name="enableMemberLogin"/></dt>
                <dd>
                    <s:if test="%{systemConfig.memberLogin}">
                        <i class="fa fa-check btn-success"></i>
                    </s:if>
                    <s:else>
                        <i class="fa fa-times btn-danger"></i>
                    </s:else>
                    <button id="btnEditLogin" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="change"/>
                    </button>
                </dd>
                <dt><s:text name="defaultCountry"/></dt>
                <dd>
                    <s:property value="%{@AU@getCountryNameByCountryCode(systemConfig.countryCode)}"/>
                    <button id="btnEditDefaultCountry" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="change"/>
                    </button>
                </dd>
            </dl>
        </div>
    </article>
</div>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend>
                    <s:text name="application"/>
                    <button id="btnAddSystemConfigImage" class="btn btn-success btn-xs" type="button" style="float:right;margin-right: 10px;">
                        <i class="fa fa-plus-circle"></i>
                        <span><s:text name="addImage"/></span>
                    </button>
                </legend>
            </fieldset>
            <dl class="dl-horizontal">
                <dt><s:text name="applicationName"/></dt>
                <dd>
                    <s:property value="%{systemConfig.applicationName}"/>
                    <button id="btnEditApplicationName" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="edit"/>
                    </button>
                </dd>
                <dt><s:text name="companyName"/></dt>
                <dd>
                    <s:property value="%{systemConfig.companyName}"/>
                    <button id="btnEditCompanyName" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="edit"/>
                    </button>
                </dd>
                <dt><s:text name="supportEmail"/></dt>
                <dd>
                    <s:property value="%{systemConfig.supportEmail}"/>
                    <button id="btnEditSupportEmail" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="edit"/>
                    </button>
                </dd>
                <dt><s:text name="QRCodeValidityInMinutes"/></dt>
                <dd>
                    <s:property value="%{systemConfig.qrCodeExpiry}"/>
                    <button id="btnEditQrCodeExpiry" class="btn btn-info btn-xs" type="button" style="margin-left: 20px;">
                        <i class="fa fa-pencil-square-o"></i>
                        <s:text name="edit"/>
                    </button>
                </dd>
            </dl>
            <div class="row">
                <div class="col-md-10 col-sm-12">
                    <table class="table table-striped table-bordered table-condensed">
                        <thead>
                        <tr>
                            <th class="col-md-6" style="white-space: nowrap;"><s:text name="systemLogo"/></th>
                            <th class="col-md-1" style="white-space: nowrap;"><s:text name="imageType"/></th>
                            <th class="col-md-2">&nbsp;</th>
                        </tr>
                        </thead>
                        <tbody>
                        <s:if test="systemConfigImages.size()==0">
                            <tr><td colspan="3"><div class="alert alert-info"><s:text name="noRecordFound"/></div></td></tr>
                        </s:if>
                        <s:iterator var="systemConfigImage" value="systemConfigImages" status="iterStatus">
                            <s:url var="imageUrl" action="systemConfigImageView">
                                <s:param name="imageId" value="%{#systemConfigImage.imageId}"/>
                            </s:url>
                            <tr imageId="<s:property value="#systemConfigImage.imageId"/>">
                                <td><img src="${imageUrl}" /></td>
                                <td style="white-space: nowrap;"><s:property value="%{@AU@getSystemConfigImageType(#systemConfigImage.imagetype)}"/></td>
                                <td>
                                    <button class="btn btn-danger btn-xs btnDeleteSystemConfigImage" type="button">
                                        <i class="fa fa-trash-o"></i>
                                        <s:text name="btnDelete"/>
                                    </button>
                                </td>
                            </tr>
                        </s:iterator>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </article>
</div>

<div id="editApplicationNameModal" class="easyui-dialog hide" style="width:500px; height:200px" title="<s:text name="application"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="systemConfigApplicationNameSave" cssClass="form-horizontal" name="applicationNameForm" id="applicationNameForm">
                <s:textfield name="systemConfig.applicationName" id="applicationName" label="%{getText('applicationName')}"/>
                <ce:buttonRow>
                    <button id="btnSaveEditApplicationName" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelEditApplicationName" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="editCompanyNameModal" class="easyui-dialog hide" style="width:500px; height:200px" title="<s:text name="application"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="systemConfigCompanyNameSave" cssClass="form-horizontal" name="companyNameForm" id="companyNameForm">
                <s:textfield name="systemConfig.companyName" id="companyName" label="%{getText('companyName')}"/>
                <ce:buttonRow>
                    <button id="btnSaveEditCompanyName" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelEditCompanyName" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="editSupportEmailModal" class="easyui-dialog hide" style="width:500px; height:200px" title="<s:text name="application"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="systemConfigSupportEmailSave" cssClass="form-horizontal" name="supportEmailForm" id="supportEmailForm">
                <s:textfield name="systemConfig.supportEmail" id="supportEmail" label="%{getText('supportEmail')}"/>
                <ce:buttonRow>
                    <button id="btnSaveEditSupportEmail" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelEditSupportEmail" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="editQrCodeExpiryModal" class="easyui-dialog hide" style="width:500px; height:200px" title="<s:text name="application"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="systemConfigQrCodeExpirySave" cssClass="form-horizontal" name="QrCodeExpiryForm" id="QrCodeExpiryForm">
                <s:textfield name="systemConfig.QrCodeExpiry" id="QrCodeExpiry" label="%{getText('QrCodeExpiry')}"/>
                <ce:buttonRow>
                    <button id="btnSaveEditQrCodeExpiry" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelEditQrCodeExpiry" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="addSystemConfigImageModal" class="easyui-dialog hide" style="width:500px; height:200px" title="<s:text name="application"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="systemConfigImageSave" name="imageForm" id="imageForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                <sc:displayErrorMessage align="center" />
                <s:file name="fileUpload" label="%{getText('image')}" required="true" cssClass="btn btn-default"/>
                <s:select name="systemConfigImage.imageType" id="systemConfigImage.imageType" label="%{getText('imageType')}" list="imageTypes" listKey="key" listValue="value"/>
                <ce:buttonRow>
                    <ce:formExtra token="true"/>
                    <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </s:submit>
                    <button id="btnCancelAddSystemConfigImage" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>

<div id="editDefaultCountryModal" class="easyui-dialog hide" style="width:500px; height:200px" title="<s:text name="membership"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <s:form action="systemConfigDefaultCountrySave" cssClass="form-horizontal" name="countryForm" id="countryForm">
                <s:select name="systemConfig.countryCode" id="systemConfig.countryCode" label="%{getText('country')}"  list="countries" listKey="countryCode" listValue="countryName" cssClass="form-control input-sm"/>
                <ce:buttonRow>
                    <button id="btnSaveDefaultCountry" class="btn btn-primary" type="submit">
                        <i class="fa fa-plus-circle"></i>
                        <s:text name="btnSave"/>
                    </button>
                    <button id="btnCancelDefaultCountry" class="btn" type="button">
                        <i class="fa fa-times"></i>
                        <s:text name="btnCancel"/>
                    </button>
                </ce:buttonRow>
            </s:form>
        </div>
    </div>
</div>
