<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.databaseMonitor" />

<div class="well">
    <div class="table-responsive">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="totalConnection"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="totalConnection"/></td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="totalFreeConnection"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="totalFreeConnection"/></td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="totalBusyConnection"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="totalBusyConnection"/></td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="totalMaxConnection"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="totalMaxConnection"/></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>