<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="ACT_BACKEND_TASK_VIEW" />

<script type="text/javascript">

    $(function(){
        $("#runTaskForm").validate({
            submitHandler: function(form) {
                $('#dg').datagrid('load',{
                    processCode: $('#processCode').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    status: $('#status').val(),
                    enableLog: $("#logOption").val()
                });
            }
        });

        $("#dg").datagrid({
            onResizeColumn:function(field, width){
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '13%';  // reset the width. Total 8
                $(this).datagrid('resize');
            }
        });

        $("#btnViewLog").click(function(event){
            var row = $('#dg').datagrid('getSelected');
            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            if(!row.enableLog){
                messageBox.alert("<s:text name="thisRecordNoLog"/>");
                return;
            }

            $("body").loadmask("<s:text name="processing.msg"/>");
            $("#logDiv").load("<s:url action="backendShowLog"/>?taskId="+ row.taskId + "&displayLog=true", function(){
                $("body").unmask();
                $("#logModal").dialog('open');
            });
        });

        $("#btnViewDebug").click(function(event){
            var row = $('#dg').datagrid('getSelected');
            if(!row){
                messageBox.alert("<s:text name="noRecordSelected"/>");
                return;
            }

            if(!row.enableLog){
                messageBox.alert("<s:text name="thisRecordNoLog"/>");
                return;
            }

            $("body").loadmask("<s:text name="processing.msg"/>");
            $("#logDiv").load("<s:url action="backendShowLog"/>?taskId="+ row.taskId + "&displayLog=false", function(){
                $("body").unmask();
                $("#logModal").dialog('open');
            });
        });
    }); // end $(function())

</script>

<form id="navForm" method="post">
    <input type="hidden" name="member.memberId" id="member.memberId" />
</form>

<s:form name="runTaskForm" id="runTaskForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <div id="searchPanel">
        <s:select name="processCode" id="processCode" label="%{getText('processCode')}" list="taskConfigOptions" listKey="key" listValue="value" cssClass="input-xxlarge"/>
        <s:select name="status" id="status" label="%{getText('status')}" list="statusList" listKey="key" listValue="value"/>
        <s:select name="logOption" id="logOption" label="%{getText('logOption')}" list="logOptions" listKey="key" listValue="value"/>

        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnViewLog" type="button" class="btn btn-danger">
            <i class="fa fa-file-text-o"></i>
            <s:text name="btnViewLog"/>
        </button>
        <button id="btnViewDebug" type="button" class="btn btn-warning">
            <i class="fa fa-briefcase"></i>
            <s:text name="btnViewDebug"/>
        </button>
    </ce:buttonRow>

    <table id="dg" class="easyui-datagrid" style="width:100%;height:350px"
           url="<s:url action="backendListDatagrid"/>"
           rownumbers="true" pagination="true" singleSelect="true" sortName="datetimeAdd" sortOrder="desc">
        <thead>
        <tr>
            <th field="taskCode" sortable="true"><s:text name="processCode"/></th>
            <th field="taskName" sortable="true"><s:text name="name"/></th>
            <th field="startDatetime" sortable="true"><s:text name="startTime"/></th>
            <th field="endDatetime" sortable="true"><s:text name="endTime"/></th>
            <th field="addByUser.username" sortable="true" formatter="(function(val, row){return eval('row.addByUser.username')})"><s:text name="doneBy"/></th>
            <th field="enableLog" formatter="$.datagridUtil.formatBoolean"><s:text name="log"/></th>
            <th field="status" sortable="true" formatter="$.datagridUtil.formatStatus"><s:text name="status"/></th>
            <th field="datetimeAdd" sortable="true"><s:text name="datetimeAdd"/></th>
        </tr>
        </thead>
    </table>
</s:form>

<div id="logModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="logFileViewer"/>" closed="true" resizable="true" maximizable="true">
    <div id="logDiv"></div>
</div>