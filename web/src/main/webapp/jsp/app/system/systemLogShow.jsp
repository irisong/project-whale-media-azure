<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="container">
    <h3><s:text name="logFileViewer"/></h3>

    <sc:displayErrorMessage align="center" />

    <s:if test="actionErrors.size()==0">

    <div class="table-responsive">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <td class="col-md-3 " style="white-space: nowrap;"><s:text name="logFile"/></td>
                    <td class="col-md-9" style="white-space: nowrap;"><s:property value="logFileName"/></td>
                </tr>
                <tr>
                    <td class="col-md-3" style="white-space: nowrap;"><s:text name="lastUpdate"/></td>
                    <td class="col-md-9" style="white-space: nowrap;"><s:date name="lastUpdateDate" format="%{getText('default.server.datetime.12.format')}"/></td>
                </tr>
                <tr>
                    <td class="col-md-3" style="white-space: nowrap;"><s:text name="fileSize"/></td>
                    <td class="col-md-9" style="white-space: nowrap;"><s:property value="%{@SF@formatDecimal(fileSize)}"/> KB</td>
                </tr>
            </tbody>
        </table>
    </div>
    ${logContent}
    </s:if>
</div>
