<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.livestreamgiftConfiguration"/>

<script type="text/javascript">
    var table = null;
    var catTable = null;
    $(function () {
        catTable = $('#giftCategoryList').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="liveStreamGiftListDatatables"/>"
            },
            "columns": [
                {"data": "seq"},
                {"data": "category"},
                {"data": "categoryCn"},
                {"data": "statusDesc"},
                {
                    "data": "status",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button>';

                        if (data === 'INACTIVE') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnActivate"><s:text name="btnActivate"/></button>';
                        } else {
                            result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnDeactivate"><s:text name="btnDeactivate"/></button>';
                        }

                        return result;
                    }
                },
            ],
            "rowId": "symbolicId"
        });

        table = $('#giftItemList').DataTable({
            "order": [[0, "desc"]],
            "ajax": {
                "url": "<s:url action="liveStreamGiftItemListDatatables"/>",
                "data": {
                    symbolicId: $('#symbolicId').val(),
                    status: $('#status').val()
                }
            },
            "columns": [
                {"data": "seq"},
                {"data": "name"},
                {"data": "nameCn"},
                {"data": "price"},
                {"data": "statusDesc"},
                {"data": "type"},
                {
                    "data": "fileUrl",
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data": "gifFileUrl",
                    "render": function (data, type, row) {
                        var content = '';
                        if (data && data.length > 0) {
                            content += '<a onclick=viewImage("' + data + '")><img src="<c:url value="/images/fileopen.png"/>"/></a>&nbsp;';
                        }
                        return content;
                    }
                },
                {
                    "data": "status",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '&nbsp;<button type="button" class="btn btn-warning btn-xs btnEditItem"><s:text name="btnEdit"/></button>';

                        if (data === 'INACTIVE') {
                            result += '&nbsp;<button type="button" class="btn btn-info btn-xs btnActivateItem"><s:text name="btnActivate"/></button>';
                        } else {
                            result += '&nbsp;<button type="button" class="btn btn-danger btn-xs btnDeactivateItem"><s:text name="btnDeactivate"/></button>';
                        }

                        return result;
                    }
                },
            ],
            "rowId": "id"
        });

        //------ GIFT CATEOGORY ------//

        $("#newCategoryForm").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: processJsonSave
                });
            },
            rules: {
                "symbolic.seq": {
                    digits: true
                }
            }
        });

        $("#btnAddNew").click(function () {
            $("#newCategoryForm").trigger("reset");

            $.getJSON("<s:url action="getNextCategorySeq"/>", function (json) {
                $("#symbolic\\.seq").val(json.seq);
            });

            $("#addNewCategoryModal").dialog('open').dialog('vcenter');
        });

        $(document).on("click", ".btnEdit", function() {
            var id = $(this).parents("tr").attr("id");

            $("#id").val(id); //this is symbolicId
            $("#navForm").attr("action", "<s:url action="editGiftCategory"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnActivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateCategoryStatus"/>", {
                    "symbolicId": id,
                    "status": "ACTIVE",
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnDeactivate", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateCategoryStatus"/>", {
                    "symbolicId": id,
                    "status": "INACTIVE",
                }, processJsonSave);
            });
        });

        //------ GIFT ITEM ------//

        $("#btnCreate").click(function () {
            $("#newItemForm").trigger("reset");
            getNextItemSeq();
            $("#addNewItemModal").dialog('open').dialog('vcenter');
        });

        <%--$("#btnActivateBatch").click(function () {--%>
        <%--    messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {--%>
        <%--        $("body").loadmask("<s:text name="processing.msg"/>");--%>
        <%--        $.ajax({--%>
        <%--            type: 'POST',--%>
        <%--            url: "<s:url action="activateGifBatch"/>",--%>
        <%--            dataType: 'json',--%>
        <%--            cache: false,--%>
        <%--            data: {},--%>
        <%--            success: function (data, status, xhr) {--%>
        <%--                messageBox.info(data.successMessage, function () {--%>
        <%--                    $("#simbolicItemForm").submit();--%>
        <%--                });--%>
        <%--            },--%>
        <%--            error: function (data, status, xhr) {--%>
        <%--                $("body").unmask();--%>
        <%--            }--%>
        <%--        });--%>
        <%--    });--%>
        <%--});--%>

        $("#newItemForm").validate({
            submitHandler: function (form) {
                $(form).ajaxSubmit({
                    dataType: 'json',
                    success: processJsonSave
                });
            },
            rules: {
                "symbolicItem.seq": { digits: true },
                "symbolicItem.price": { number: true }
            }
        });

        $(document).on("click", ".btnEditItem", function() {
            var id = $(this).parents("tr").attr("id");

            $("#id").val(id);
            $("#navForm").attr("action", "<s:url action="editSymbolicItem"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnActivateItem", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateItemStatus"/>", {
                    "id": id,
                    "status": "ACTIVE",
                }, processJsonSave);
            });
        });

        $(document).on("click", ".btnDeactivateItem", function (event) {
            var id = $(this).parents("tr").attr("id");
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="updateItemStatus"/>", {
                    "id": id,
                    "status": "INACTIVE",
                }, processJsonSave);
            });
        });
    })

    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');
        $("#imageViewer").attr('src', fileUrl);
        $("#imageViewerModal").dialog('open').dialog('vcenter');
    }

    function processJsonSave(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                messageBox.info(json.successMessage, function () {
                    $("#simbolicItemForm").submit();
                });
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

    function checkType() {
        if ($("#symbolicItem\\.type option:selected").val() === 'ANIMATION') {
            $("#animation").css("visibility", "visible");
        } else {
            $("#animation").css("visibility", "hidden");
        }
    }

    function getNextItemSeq() {
        $.getJSON("<s:url action="getNextItemSeq"/>", {
            "symbolicItem.symbolicId" : $("#symbolicItem\\.symbolicId").val()
        }, function (json) {
            $("#symbolicItem\\.seq").val(json.seq);
            console.log($("#symbolicItem\\.symbolicId").val());
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="id" id="id"/>
</form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <button id="btnAddNew" type="button" class="btn btn-warning">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnAddNewCategory"/>
        </button>
        <div class="well">
            <fieldset>
                <legend><s:text name="title.giftCategoryConfiguration"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="giftCategoryList" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="seq"/></th>
                            <th><s:text name="category"/></th>
                            <th><s:text name="categoryCn"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<s:form name="simbolicItemForm" id="simbolicItemForm" cssClass="form-horizontal">
    <s:select name="symbolicId" id="symbolicId" list="giftCategories" label="%{getText('category')}" listKey="key" listValue="value"/>
    <s:select name="status" id="status" list="statusList" label="%{getText('status')}" listKey="key" listValue="value"/>
    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreateItem"/>
        </button>
<%--        <button id="btnActivateBatch" class="btn" type="button">--%>
<%--            <i class="fa fa-check-circle"></i>--%>
<%--            <s:text name="btnUploadVersionBatch"/>--%>
<%--        </button>--%>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.giftItemConfiguration"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="giftItemList" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="seq"/></th>
                            <th><s:text name="itemName"/></th>
                            <th><s:text name="itemNameCn"/></th>
                            <th><s:text name="price"/></th>
                            <th><s:text name="status"/></th>
                            <th><s:text name="type"/></th>
                            <th><s:text name="image"/></th>
                            <th><s:text name="gifImage"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="addNewCategoryModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="addNewCategory"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
        <s:form action="saveGiftCategory" name="newCategoryForm" id="newCategoryForm" cssClass="form-horizontal">
            <s:textfield name="symbolic.category" id="symbolic.category" label="%{getText('category')}" size="100" maxlength="100" required="true"/>
            <s:textfield name="symbolic.categoryCn" id="symbolic.categoryCn" label="%{getText('categoryCn')}" required="true"/>
            <s:textfield name="symbolic.seq" id="symbolic.seq" label="%{getText('seq')}" required="true"/>

            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <ce:buttonRow>
                        <button id="btnSave" type="submit" class="btn btn-primary">
                            <i class="icon-save"></i>
                            <s:text name="btnSave"/>
                        </button>
                    </ce:buttonRow>
                </div>
            </div>
        </s:form>
        </div>
    </div>
</div>

<div id="addNewItemModal" class="easyui-dialog" style="width:600px; height:600px" title="<s:text name="addNewItem"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="saveSymbolicItem" name="newItemForm" id="newItemForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <s:select name="symbolicItem.symbolicId" id="symbolicItem.symbolicId" label="%{getText('category')}" list="giftCategories" listKey="key" listValue="value" onchange="getNextItemSeq()" required="true"/>
                    <s:textfield name="symbolicItem.name" id="symbolicItem.name" label="%{getText('itemName')}" cssClass="input-xxlarge" required="true"/>
                    <s:textfield name="symbolicItem.nameCn" id="symbolicItem.nameCn" label="%{getText('itemNameCn')}" cssClass="input-xxlarge" size="100" maxlength="100" required="true"/>
                    <s:textfield name="symbolicItem.seq" id="symbolicItem.seq" label="%{getText('seq')}" cssClass="input-xxlarge" required="true"/>
                    <s:textfield name="symbolicItem.price" id="symbolicItem.price" label="%{getText('price')}" cssClass="input-xxlarge" size="50" maxlength="50" required="true"/>
                    <s:select name="symbolicItem.type" id="symbolicItem.type" label="%{getText('type')}" list="symbolicItemType" listKey="key" listValue="value" onchange="checkType()" required="true"/>
                    <s:checkboxlist name="remark" id="remark" label="%{getText('Special')}" list="remarkList" listKey="key" listValue="value" cssClass="checkbox style-0"/>

                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="sendGlobalMsg"/>: </label>
                        <div class="col-md-9 col-sm-9">
                            <input type="checkbox" name="sendGlobalMsg" id="sendGlobalMsg"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="image"/>: </label>
                        <div class="col-md-9 col-sm-9">
                            <s:file name="fileUpload" id="fileUpload"/>
                        </div>
                    </div>

                    <div class="form-group" id="animation" style="visibility: hidden">
                        <label class="col-md-3 col-sm-3 control-label"><s:text name="gifImage"/>: </label>
                        <div class="col-md-9 col-sm-9">
                            <s:file name="gifFileUpload" id="gifFileUpload"/>
                        </div>
                    </div>

                    <ce:buttonRow>
                        <button id="btnSaveItem" class="btn btn-primary" type="submit">
                            <i class="fa fa-plus-circle"></i>
                            <s:text name="btnSave"/>
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true" resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%; height: 100%">
</div>
