<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.giftItemEdit"/>

<script type="text/javascript">
    $(function() {
        $("#symbolicItemForm").validate( {
            submitHandler: function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "fileUpload": "required"
            }
        });

        //when first enter this jsp
        if ('<s:property value="symbolicItem.type"/>' === 'ANIMATION') {
            $('#animation').css("visibility", "visible");
        } else {
            $("#animation").css("visibility", "hidden");
        }
    });

    function changeUploadFile(fileUploadType) {
        messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
            $("#"+fileUploadType).html('<input type="file" name="'+fileUploadType+'">');
        });
    }

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="liveStreamGift"/>";
                });
            }, // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    function viewImage(fileUrl) {
        $("#imageViewer").removeAttr('src');
        $("#imageViewer").attr('src', fileUrl);
        $("#imageViewerModal").dialog('open').dialog('vcenter');
    }

    function checkType() {
        if ($("#symbolicItem\\.type").val() === 'ANIMATION') {
            $("#animation").css("visibility", "visible");
        } else if ($("#symbolicItem\\.type").val() === 'IMAGE') {
            $("#animation").css("visibility","hidden");
        }
    }

    function getNextItemSeq() {
        $.getJSON("<s:url action="getNextItemSeq"/>", {
            "symbolicItem.symbolicId" : $("#symbolicItem\\.symbolicId").val()
        }, function (json) {
            $("#symbolicItem\\.seq").html(json.seq);
        });
    }
</script>

<s:form action="updateSymbolicItem" name="symbolicItemForm" id="symbolicItemForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:hidden name="symbolicItem.id" id="symbolicItem.id"/>

    <div class="row">
        <article class="col-md-12">
            <s:select name="symbolicItem.symbolicId" id="symbolicItem.symbolicId" label="%{getText('category')}" list="giftCategories" listKey="key" listValue="value" onchange="getNextItemSeq()" required="true"/>
            <s:textfield name="symbolicItem.name" id="symbolicItem.name" label="%{getText('itemName')}" cssClass="input-xxlarge" required="true"/>
            <s:textfield name="symbolicItem.nameCn" id="symbolicItem.nameCn" label="%{getText('itemNameCn')}" cssClass="input-xxlarge" required="true"/>
            <s:textfield name="symbolicItem.seq" id="symbolicItem.seq" label="%{getText('seq')}" cssClass="input-xxlarge" required="true"/>
            <s:textfield name="symbolicItem.price" id="symbolicItem.price" label="%{getText('price')}" cssClass="input-xxlarge" required="true"/>
            <s:select name="symbolicItem.type" id="symbolicItem.type" label="%{getText('type')}" list="symbolicItemType" listKey="key" listValue="value" onchange="checkType()" required="true"/>
            <s:checkboxlist name="remark" id="remark" label="%{getText('Special')}" list="remarkList" listKey="key" listValue="value" cssClass="checkbox style-0"/>

            <div class="form-group">
                <label class="col-sm-3 control-label"><s:text name="sendGlobalMsg"/>: </label>
                <div class="col-sm-9">
                    <s:if test="symbolicItem.sendGlobalMsg">
                        <input type="checkbox" name="sendGlobalMsg" id="sendGlobalMsg" checked/>
                    </s:if>
                    <s:else>
                        <input type="checkbox" name="sendGlobalMsg" id="sendGlobalMsg"/>
                    </s:else>
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-sm-3 control-label"><s:text name="image"/>: </label>
                <div class="col-md-9 col-sm-9" id="fileUpload">
                    <s:if test="symbolicItem.fileUrl.length() > 0">
                        <a onclick="viewImage('${symbolicItem.fileUrl}')"><img src="<c:url value="/images/fileopen.png"/>"/></a>
                        <button type="button" class="btn btn-xs" onclick="changeUploadFile('fileUpload')"><s:text name="btnChange"/></button>
                    </s:if>
                    <s:else>
                        <s:file name="fileUpload"/>
                    </s:else>
                </div>
            </div>
            <div class="form-group" id="animation">
                <label class="col-md-3 col-sm-3 control-label"><s:text name="gifImage"/>: </label>
                <div class="col-md-9 col-sm-9" id="gifFileUpload">
                    <s:if test="symbolicItem.gifFileUrl.length() > 0">
                        <a onclick="viewImage('${symbolicItem.gifFileUrl}')"><img src="<c:url value="/images/fileopen.png"/>"/></a>
                        <button type="button" class="btn btn-xs" onclick="changeUploadFile('gifFileUpload')"><s:text name="btnChange"/></button>
                    </s:if>
                    <s:else>
                        <s:file name="gifFileUpload"/>
                    </s:else>
                </div>
            </div>
        </article>
    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="liveStreamGift"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple" >
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>

<div id="imageViewerModal" class="easyui-dialog" style="width:500px; height:500px" title="Image" closed="true" resizable="true" maximizable="true">
    <img id="imageViewer" style="width: 100%; height: 100%">
</div>