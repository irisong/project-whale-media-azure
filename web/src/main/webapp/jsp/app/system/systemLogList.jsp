<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:displayErrorMessage align="center" />

<table class="table table-striped table-bordered table-condensed">
    <thead>
    <tr>
        <th class="col-md-8 col-sm-8"><s:text name="logFile"/></th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <s:iterator var="fileName" value="fileNames">
        <tr rowid="<s:property value="#fileName"/>" contenttype="<s:property value="contentType"/>">
            <td><s:property value="#fileName"/></td>
            <td>
                <a class="viewLink txt-color-blue font-md" title="<s:text name="btnView"/>" href="#">
                    <i class="fa fa-file-o"></i>
                </a>
            </td>
        </tr>
    </s:iterator>
    </tbody>
</table>