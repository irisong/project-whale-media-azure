<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.systemMonitor" />

<div class="well">
    <div class="table-responsive">
        <table class="table table-bordered">
            <tbody>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4" style="white-space: nowrap;"><s:text name="noOfProcessors"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="noOfProcessors"/></td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="totalMemory"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="totalMemory"/> MB</td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="usedMemory"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="usedMemory"/> MB</td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="freeMemory"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="freeMemory"/> MB</td>
                </tr>
                <tr>
                    <th class="col-lg-2 col-md-3 col-sm-3 col-xs-4"><s:text name="maxMemory"/></th>
                    <td class="col-lg-10 col-md-9 col-sm-9 col-xs-8"><s:property value="maxMemory"/> MB</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>