<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.fansIntimacyConfig"/>

<script type="text/javascript">
    function loadDatatables() {
        $('#fansIntimacyListTable').DataTable({
            "destroy": true,
            "order": [[0, "asc"]],
            "ajax": {
                "url": "<s:url action="fansIntimacyConfigListDatatables"/>",
            },
            "columns": [
                {"data": "fansLevel"},
                {"data": "deductPercent"},
                {"data": "levelUpValue"},
                {"data": "maxPerDay"},
                {"data": "remark"},
                {
                    "data": "id",
                    "className": "text-nowrap text-center",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-warning btn-xs btnEdit"><i class="fa fa-pencil-square-o"></i>&nbsp;<s:text name="btnEdit"/></button>';
                        return result;
                    }
                }
            ],
            "rowId": "id"
        });

        $(document).on("click", ".btnEdit", function(event){
            var id = $(this).parents("tr").attr("id");
            $("#fansIntimacyConfig\\.id").val(id);
            $("#navForm").attr("action", "<s:url action="fansIntimacyConfigEdit"/>")
            $("#navForm").submit();
        });
    }

    $(function(){
        $("#btnAddNew").click(function (event) {
            $("#navForm").attr("action", "<s:url action="fansIntimacyConfigAdd"/>")
            $("#navForm").submit();
        });
       loadDatatables();
    }); // end $(function())

</script>
<form id="navForm" method="post">
    <input type="hidden" name="fansIntimacyConfig.id" id="fansIntimacyConfig.id" />
</form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <button id="btnAddNew" type="button" class="btn btn-primary">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnAddNew"/>
        </button>
        <div class="well">
            <fieldset><legend><s:text name="title.fansIntimacyConfigList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="fansIntimacyListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th><s:text name="fansLevel"/></th>
                                <th><s:text name="deductPercent"/></th>
                                <th><s:text name="levelUpValue"/></th>
                                <th><s:text name="maxPerDay"/></th>
                                <th><s:text name="label_remarks"/></th>
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

