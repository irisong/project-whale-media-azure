<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.OMCWalletHistoryReport"/>

<script type="text/javascript">
    var table = null;
    var action = "list";

    function loadDatatables() {
        // $(function () {
        table = $('#OMCWalletHistoryListingTable').DataTable({
            "order": [0,"desc"],
            "destroy": true,
            "ajax": {
                "url": "<s:url action="OMCWalletHistoryReportDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    orderId :$('#orderId').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    remark: $('#remark').val()
                }
            },
            "columns": [
                {"data": "trxDatetime"},
                {"data": "inAmt"},
                {"data": "trxDesc"},
                {"data": "memberCode", "orderable": false},
                {"data": "profileName", "orderable": false},
                {"data": "walletRefId"}
            ],
            "rowId": "walletRefId"
        });
    }

    $(function () {
        $("#OMCWalletHistoryForm").validate({

            submitHandler:function (form) {
                if(action == 'list'){
                    loadDatatables();
                } else {
                    form.action="<s:url action="OMCWalletHistoryReportExportExcel" />";
                    form.submit();
                }
            }
        });

        $("#btnSearch").click(function(event){
            action = "list";
        });

        $("#btnExportExcel").click(function(event){
            // stop event
            event.preventDefault();

            action = "exportExcel";
            console.log("btn exportExcel");
            $("#OMCWalletHistoryForm").submit();
        });

        loadDatatables();
    });
</script>

<s:form name="OMCWalletHistoryForm" id="OMCWalletHistoryForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div id="searchPanel">
        <sc:memberTextField name="memberCode"/>
        <s:textfield name="orderId" id="orderId" label="%{getText('orderId')}"/>
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
        <s:select name="remark" id="remark" label="%{getText('remark')}" list="allRemark" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnExportExcel" type="button" class="btn btn-primary btnExportExcel">
            <s:text name="btnExportExcel"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <table id="OMCWalletHistoryListingTable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><s:text name="transactionDate"/></th>
                    <th><s:text name="inAmount"/></th>
                    <th><s:text name="trxDesc"/></th>
                    <th><s:text name="memberCode"/></th>
                    <th><s:text name="profileName"/></th>
                    <th><s:text name="orderId"/></th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>