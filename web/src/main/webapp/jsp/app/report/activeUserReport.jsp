<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.activeUserReport"/>

<script type="text/javascript">
    var table = null;
    var action = "list";

    // function loadDatatables() {
        $(function () {
        table = $('#activeUserReportListingTable').DataTable({
            "order": [0, "desc"],
            "ajax": {
                "url": "<s:url action="activeUserReportDatatables"/>",
                "data": {
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val()
                }
            },
            "columns": [
                {"data": "date"},
                {"data": "sendGiftCount"},
                {"data": "audienceCount"},
                {"data": "newUserCount"},
                {"data": "totalAmt"}
            ],
            "rowId": "date"
            });
        });
    // }

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    $("#activeUserForm").submit(); // refresh page
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    $(function () {
       $("#activeUserForm").validate({

           submitHandler:function (form) {
               if(action == 'list'){
                   loadDatatables();
               } else {
                   form.action="<s:url action="activeUserReportExportExcel" />";
                   form.submit();
               }
           }
       });

        // $("#btnSearch").click(function(event){
        //     action = "list";
        // });

        $("#btnExportExcel").click(function(event){
            // stop event
            event.preventDefault();

            action = "exportExcel";
            console.log("btn exportExcel");
            $("#activeUserForm").submit();
        });

        // loadDatatables();
    });
</script>

<s:form name="activeUserForm" id="activeUserForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div id="searchPanel">
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnExportExcel" type="button" class="btn btn-primary btnExportExcel">
            <s:text name="btnExportExcel"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <table id="activeUserReportListingTable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><s:text name="date"/></th>
                    <th><s:text name="sendGiftCount"/></th>
                    <th><s:text name="audienceCount"/></th>
                    <th><s:text name="newUserCount"/></th>
                    <th><s:text name="totalAmt"/></th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>
