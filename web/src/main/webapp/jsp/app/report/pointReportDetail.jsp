<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.point.report"/>

<script type="text/javascript">
    var table;
    $(function () {
        if ('<s:property value="giftListing"/>' == 'true') {
            table = $('#pointTrxListing').DataTable({
                "order": [],
                "ajax": {
                    "url": "<s:url action="pointReportListDatatables"/>",
                    "data": {
                        channelId: $('#channelId').val(),
                        userType: $('#userType').val(),
                        memberCode: $('#memberCode').val(),
                        giftListing: $('#giftListing').val()
                    }
                },
                "columns": [
                    {"data": "memberCode"},
                    {"data": "amt"},
                    {"data": "price"},
                    {"data": "totalAmt"},
                    {"data": "datetimeAdd"},
                    {"data": "remark"},
                ], "select":
                    {
                        "style": 'multi'
                    },
                "rowId": "memberId"
            });
        } else {
            console.log('giftListing false')
            table = $('#contributorListing').DataTable({
                "order": [],
                "ajax": {
                    "url": "<s:url action="pointReportListDatatables"/>",
                    "data": {
                        channelId: $('#channelId').val(),
                        userType: $('#userType').val(),
                        memberCode: $('#memberCode').val(),
                        giftListing: $('#giftListing').val()
                    }
                },
                "columns": [
                    {"data": "memberCode"},
                    {"data": "point"},
                    {
                        "data": "point",
                        "className": "text-nowrap",
                        "render": function (data, type, row) {
                            var result = "";
                            if (data > 0) {
                                result = '<button type="button" class="btn btn-info btn-xs btnView"><s:text name="btnView"/></button>';
                            }
                            return result;
                        }
                    },
                ], "select":
                    {
                        "style": 'multi'
                    },
                "rowId": "memberId"
            });
        }

        $("#dg").datagrid({
            onResizeColumn: function (field, width) {
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '14%';  // reset the width. Total 7 columns (14%)
                $(this).datagrid('resize');
            }
        });

        $(document).on("click", ".btnView", function (event) {
            // var memberId = $(this).parents("tr").attr("id");
            var id = $('#channelId').val();
            var memberCode = $(this).closest("tr").find("td:eq(0)").text();
            // isGiftListing
            $("#channelId").val(id);
            $("#memberCode").val(memberCode);
            $("#giftListing").val(true);
            $("#pointReportForm").attr("action", "<s:url action="pointReportDetail" />");
            $("#pointReportForm").submit();
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    // refresh page
                    // window.location = "<s:url action="pointReportList"/>";
                    $("#pointReportForm").submit();
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

</script>


<form id="navForm" method="post">
    <%--    <input type="hidden" name="channelId" id="channelId"/>--%>
    <%--    <input type="hidden" name="userType" id="userType"/>--%>
    <%--    <input type="hidden" name="memberCode" id="memberCode"/>--%>
    <%--    <input type="hidden" name="isGiftListing" id="isGiftListing"/>--%>
</form>

<s:form name="pointReportForm" id="pointReportForm" method="post" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:hidden name="channelId" id="channelId"/>
    <s:hidden name="userType" id="userType"/>
    <s:hidden name="memberCode" id="memberCode"/>
    <s:hidden name="giftListing" id="giftListing"/>

    <sc:submitData/>

</s:form>
<s:if test="%{giftListing}">
    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <fieldset>
                    <legend><s:text name="title.point.transaction"/></legend>
                </fieldset>
                <div class="row">
                    <article class="col-md-12 col-sm-12">
                        <table id="pointTrxListing" class="table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><s:text name="memberCode"/></th>
                                <th><s:text name="quantity"/></th>
                                <th><s:text name="price"/></th>
                                <th><s:text name="totalAmount"/></th>
                                <th><s:text name="datetimeAdd"/></th>
                                <th><s:text name="remark"/></th>
                            </tr>
                            </thead>
                        </table>
                    </article>
                </div>
            </div>
        </article>
    </div>
</s:if>
<s:else>
    <div class="row">
        <article class="col-md-12 col-sm-12">
            <div class="well">
                <fieldset>
                    <legend><s:text name="title.contributor.list"/></legend>
                </fieldset>
                <div class="row">
                    <article class="col-md-12 col-sm-12">
                        <table id="contributorListing" class="table table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th><s:text name="memberCode"/></th>
                                <th><s:text name="totalPoint"/></th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </article>
                </div>
            </div>
        </article>
    </div>
</s:else>