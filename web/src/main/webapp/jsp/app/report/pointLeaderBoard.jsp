<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.point.report"/>

<script type="text/javascript">
    var table;
    $(function () {

        if ('<s:property value="reportType"/>' != 'MONTHLY') {
            $("label[for=dateType], input[type=select][name='dateType']").parent().hide();
        } else {
            $("label[for=dateType], input[type=select][name='dateType']").parent().show();
        }
        table = $('#pointListingTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="pointLeaderBoardDatatables"/>",
                "data": {
                    reportType: $('#reportType').val(),
                    userType: $("input[name='userType']:checked").val(),
                    dateType: $('#dateType').val()
                }
            },
            "columns": [
                {"data": "rowNum"},
                {"data": "profileName"},
                {"data": "memberCode"},
                {"data": "point"},
                {
                    "data": "point",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = "";
                        if (data > 0) {
                            result = '<button type="button" class="btn btn-info btn-xs btnView"><s:text name="btnView"/></button>';
                        }
                        return result;
                    }
                },
            ], "select":
                {
                    "style": 'multi'
                },
            "rowId": "memberId"
        });

        $("#dg").datagrid({
            onResizeColumn: function (field, width) {
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '14%';  // reset the width. Total 7 columns (14%)
                $(this).datagrid('resize');
            }
        });

        $(document).on("click", ".btnView", function (event) {
            var memberCode = $(this).parents("tr").attr("id");
            var userType = $("input[name='userType']:checked").val();
            var reportType = $('#reportType').val();
            var dateType = $('#dateType').val();

            $("#memberId").val(memberCode);
            $("#userType").val(userType);
            $("#reportType").val(reportType);
            $("#dateType").val(dateType);
            $("#pointReportForm").attr("action", "<s:url action="pointMemberReport" />");
            $("#pointReportForm").submit();


        });

        $("#reportType").on("change", function () {
            //Collect input from html page
            var reportType = $('#reportType').val();
            if (reportType == 'MONTHLY') {
                $("label[for=dateType], input[type=select][name='dateType']").parent().show();
            } else {
                $("label[for=dateType], input[type=select][name='dateType']").parent().hide();
            }
        });


    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    // refresh page
                    // window.location = "<s:url action="pointLeaderBoard"/>";
                    $("#pointReportForm").submit();
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

</script>


<form id="navForm" method="post">
</form>

<s:form name="pointReportForm" id="pointReportForm" method="post" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div id="searchPanel">
        <s:hidden name="memberId" id="memberId"/>

        <s:select name="reportType" id="reportType" label="%{getText('reportType')}" list="reportTypes" listKey="key"
                  listValue="value"/>
        <s:radio name="userType" id="userType" label="%{getText('userType')}"
                 list="userTypes"
                 listKey="key" listValue="value"/>
        <s:select name="dateType" id="dateType" label="%{getText('month')}"
                  required="true" list="dateTypes" listKey="key" listValue="value"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.leader.board"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="pointListingTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="ranking"/></th>
                            <th><s:text name="profileName"/></th>
                            <th><s:text name="memberCode"/></th>
                            <th><s:text name="totalPoint"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>
