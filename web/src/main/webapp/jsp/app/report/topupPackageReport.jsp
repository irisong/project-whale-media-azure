<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.topupPackage"/>

<script type="text/javascript">
    var table;

    $(function () {
        table = $('#topupPackageTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="topupPackageReportDatatables"/>",
                "data": {
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val()
                }
            },
            "columns": [
                {"data": "packageAmount"},
                {"data": "topupPackageCount"},
                {
                    "data": "packageAmount",
                    "className": "text-nowrap",
                    "render": function (data) {
                        var result = "";
                        if (data > 0) {
                            result = '<button type="button" class="btn btn-info btn-xs btnView"><s:text name="btnView"/></button>';
                        }
                        return result;
                    }
                }
            ],
            "rowId": "packageAmount"
        });

        $(document).on("click", ".btnView", function() {
            var id = $(this).parents("tr").attr("id");
            $("#packageAmount").val(id);
            $("#filterDateFrom").val($("#dateFrom").val());
            $("#filterDateTo").val($("#dateTo").val());

            $("#navForm").attr("action", "<s:url action="topupPackageReportDetail"/>");
            $("#navForm").submit();
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
              // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

</script>

<form id="navForm" method="post">
    <input type="hidden" name="packageAmount" id="packageAmount">
    <input type="hidden" name="filterDateFrom" id="filterDateFrom">
    <input type="hidden" name="filterDateTo" id="filterDateTo">
</form>

<s:form name="topupPackageReportForm" id="topupPackageReportForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div id="searchPanel">
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <table id="topupPackageTable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><s:text name="packageAmount"/></th>
                    <th><s:text name="topupPackageCount"/></th>
                    <th><s:text name="details"/></th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>
