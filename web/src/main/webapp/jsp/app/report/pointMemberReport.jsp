<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.point.report"/>

<script type="text/javascript">
    var table;
    $(function () {
        table = $('#pointMemberTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="pointMemberReportDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    userType: $("input[name='userType']:checked").val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val()
                }
            },
            "columns": [
                {"data": "memberCode"},
                {"data": "whaleLiveId"},
                {"data": "hostName"},
                {"data": "startDate"},
                {"data": "endDate"},
                {"data": "totalView"},
                {"data": "totalPoint"},
                {
                    "data": "totalPoint",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = "";
                        if (data > 0) {
                            result = '<button type="button" class="btn btn-info btn-xs btnView"><s:text name="btnView"/></button>';
                        }
                        return result;
                    }
                },

            ], "select":
                {
                    "style": 'multi'
                },
            "rowId": "channelId"
        });

        $("#dg").datagrid({
            onResizeColumn: function (field, width) {
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '14%';  // reset the width. Total 7 columns (14%)
                $(this).datagrid('resize');
            }
        });

        $(document).on("click", ".btnView", function (event) {
            var id = $(this).parents("tr").attr("id");
            var memberCode = $(this).closest("tr").find("td:eq(0)").text();
            var userType = $("input[name='userType']:checked").val();
            // isGiftListing
            $("#channelId").val(id);
            $("#memberCodeForm").val(memberCode);
            $("#userType").val(userType);
            if (userType != 'POPULAR') {
                $("#giftListing").val(true);
            }
            $("#navForm").attr("action", "<s:url action="pointReportDetail" />");
            $("#navForm").submit();
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    // refresh page
                    // window.location = "<s:url action="pointReportList"/>";
                    $("#pointReportForm").submit();
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    $(function () {
        $("#pointReportForm").validate({

            submitHandler: function (form) {
                if(action == 'list') {
                    loadDatatables();
                } else {
                    form.action="<s:url action="memberPointReportExportExcel" />";
                    form.submit();
                }
            }
        });

        $("#btnExportExcel").click(function (event) {
            //stop event
            event.preventDefault();

            action = "exportExcel";
            // console.log("btn exportExcel");
            $("#pointReportForm").submit();
        });
    });
</script>

<form id="navForm" method="post">
    <input type="hidden" name="channelId" id="channelId"/>
    <input type="hidden" name="memberCodeForm" id="memberCodeForm"/>
    <input type="hidden" name="userType" id="userType"/>
    <input type="hidden" name="giftListing" id="giftListing"/>
</form>

<s:form name="pointReportForm" id="pointReportForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div id="searchPanel">
        <sc:memberTextField name="memberCode"/>
        <s:radio name="userType" id="userType" label="%{getText('userType')}"
                 list="userTypes"
                 listKey="key" listValue="value"/>
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnExportExcel" type="button" class="btn btn-primary btnExportExcel">
            <s:text name="btnExportExcel"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.member.point.history"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="pointMemberTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="memberCode"/></th>
                            <th><s:text name="hostWhalelive"/></th>
                            <th><s:text name="hostname"/></th>
                            <th><s:text name="channelStartTime"/></th>
                            <th><s:text name="channelEndTime"/></th>
                            <th><s:text name="highestView"/></th>
                            <th><s:text name="totalPoint"/></th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>
