<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.point.report"/>

<script type="text/javascript">
    var table;
    $(function () {
        table = $('#pointListingTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="pointReportListDatatables"/>",
                "data": {
                    memberCode: $('#memberCode').val(),
                    reportType: $('#reportType').val(),
                    userType: $('#userType').val(),
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val()
                }
            },
            "columns": [
                {"data": "profileName"},
                {"data": "memberCode"},
                {"data": "point"},
                {"data": "rowNum"},
            ], "select":
                {
                    "style": 'multi'
                },
            "rowId": "memberId"
        });

        $("#dg").datagrid({
            onResizeColumn: function (field, width) {
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '14%';  // reset the width. Total 7 columns (14%)
                $(this).datagrid('resize');
            }
        });

    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    // refresh page
                    // window.location = "<s:url action="pointReportList"/>";
                    $("#pointReportForm").submit();
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

</script>


<form id="navForm" method="post">
</form>

<s:form name="pointReportForm" id="pointReportForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>

    <div id="searchPanel">
        <sc:memberTextField name="memberCode"/>
        <s:select name="reportType" id="reportType" label="%{getText('reportType')}" list="reportTypes" listKey="key"
                  listValue="value"/>
        <s:select name="userType" id="userType" label="%{getText('userType')}" list="userTypes" listKey="key"
                  listValue="value"/>
        <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo"/>
    </div>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.leader.board"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="pointListingTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="profileName"/></th>
                            <th><s:text name="memberCode"/></th>
                            <th><s:text name="totalPoint"/></th>
                            <th><s:text name="ranking"/></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>
