<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.topupPackageDetail"/>

<script type="text/javascript">
    var table;

    // function loadDatatables() {
    $(function () {
        table = $('#topupPackageDetailListing').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="topupPackageReportDetailDatatables"/>",
                "data": {
                    dateFrom: $('#dateFrom').val(),
                    dateTo: $('#dateTo').val(),
                    packageAmount: $('#packageAmount').val(),
                    memberCode: $('#memberCode').val(),
                    topupWay : $('#topupWay').val()
                }
            },
            "columns": [
                {"data": "packageAmount"},
                {"data": "datetimeAdd"},
                {"data": "memberCode"},
                {"data": "remark"}
            ],
            "rowId": "packageAmount"
        });
    });
    // };

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    $("#topupPackageReportForm").submit(); // refresh page
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }

    $(function () {
        $("#topupPackageDetailReportForm").validate({

            submitHandler:function (form) {
                if(action == 'list'){
                    loadDatatables();
                } else {
                    form.action="<s:url action="topupPackageDetailReportExportExcel" />";
                    form.submit();
                }
            }
        });

        // $("#btnSearch").click(function(event){
        //     action = "list";
        // });

        $("#btnExportExcel").click(function(event){
            // stop event
            event.preventDefault();

            action = "exportExcel";
            console.log("btn exportExcel");
            $("#topupPackageDetailReportForm").submit();
        });

        // loadDatatables();
    });
</script>

<s:form name="topupPackageDetailReportForm" id="topupPackageDetailReportForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:textfield name="packageAmount" id="packageAmount" label="%{getText('packageAmount')}" cssClass="input-xxlarge" readonly="true"/>
    <s:textfield name="filterDateFrom" id="dateFrom" label="%{getText('dateFrom')}" cssClass="input-xxlarge" readonly="true"/>
    <s:textfield name="filterDateTo" id="dateTo" label="%{getText('dateTo')}" cssClass="input-xxlarge" readonly="true"/>
    <sc:memberTextField name="memberCode"/>
    <s:select name="topupWay" id="topupWay" list="allTopupwayList" label="Topup Way" listKey="key" listValue="value"/>

    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnExportExcel" type="button" class="btn btn-primary btnExportExcel">
            <s:text name="btnExportExcel"/>
        </button>
        <s:url var="urlExit" action="topupPackageReport"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <table id="topupPackageDetailListing" class="table table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th><s:text name="packageAmount"/></th>
                        <th><s:text name="datetimeAdd"/></th>
                        <th><s:text name="memberCode"/></th>
                        <th><s:text name="topUpWay"/></th>
                    </tr>
                    </thead>
                </table>
            </fieldset>
        </div>
    </article>
</div>