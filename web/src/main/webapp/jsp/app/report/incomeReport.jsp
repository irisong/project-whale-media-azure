<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.incomeReport"/>

<script type="text/javascript">
    var table;

    $(function () {
        table = $('#incomeReportTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="incomeReportDatatables"/>",
                "data": {
                    guildName: $('#guildName').val(),
                    incomeMonth: $('#incomeMonth').val(),
                    incomeYear:$('#incomeYear').val()
                }
            },
            "columns": [
                {"data": "guildId" },
                {"data": "memberId"},
                {"data": "whaleLiveId"},
                {"data": "profileName"},
                {"data": "point"},
                {"data": "basicSalary"},
                {"data": "incomeMonth","orderable":false},
                {"data": "rankName"},

            ],
            "rowId": "id"
        });

    });

    $(document).on("click", ".btnSearch", function (event) {
        $("#incomeReportForm").attr("action", "<s:url action="incomeReport" />")
        $("#incomeReportForm").submit();
    });

    $(document).on("click", ".btnGenIncomeReport", function (event) {
        var month = $("#incomeMonth").val();
        var year = $("#incomeYear").val();
        $("body").loadmask("<s:text name="processing.msg"/>");

        $.post('<s:url action="generateTemporaryIncomeReport"/>',{
            incomeMonth : month,
            incomeYear:year

        } ,function (json) {
            $("body").unmask();

            new JsonStat(json, {
                onSuccess: function (json) {
                    messageBox.info(json.successMessage, function () {
                        // refresh page
                        window.location.reload();
                    });
                }
            });
        });
    });


    $(document).on("click", ".btnExportExcel", function (event) {
        if($("#guildName").val()=="")
        {
            alert("Please enter guild name");
            return false;
        }
        if($("#incomeMonth").val()=="")
        {
            alert("Please enter month");
            return false;
        }
        if($("#incomeYear").val()=="")
        {
            alert("Please enter year");
            return false;
        }
        $("#incomeReportForm").attr("action", "<s:url action="incomeSummaryExportExcel" />")
        $("#incomeReportForm").submit();
    });



    function defaultJsonCallback(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                table.ajax.reload();
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }

</script>

<form id="navForm" method="post">
    <input type="hidden" name="id" id="id"/>
</form>

<s:form name="incomeReportForm" id="incomeReportForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <sc:guildTextField name="guildName"/>
    <s:select name="incomeMonth" id="incomeMonth" list="allMonthList" label="Month" listKey="key" listValue="value"/>
    <s:select name="incomeYear" id="incomeYear" list="allYearList" label="Year" listKey="key" listValue="value"/>

    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="button" class="btn btn-success btnSearch">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnExportExcel" type="button" class="btn btn-primary btnExportExcel">
           <s:text name="btnExportExcel"/>
        </button>
        <button id="btnGenIncomeReport" type="button" class="btn btn-primary btnGenIncomeReport">
            <s:text name="btnGenIncomeReport"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <table id="incomeReportTable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><s:text name="guildName"/></th>
                    <th><s:text name="memberCode"/></th>
                    <th><s:text name="whaleliveId"/></th>
                    <th><s:text name="profileName"/></th>
                    <th><s:text name="point"/></th>
                    <th><s:text name="basicSalary"/></th>
                    <th><s:text name="trxDateTime"/></th>
                    <th><s:text name="rankName"/></th>
                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>