prizeReport
<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.prizeReport"/>

<script type="text/javascript">
    var table;

    $(function () {
        table = $('#prizeTable').DataTable({
            "order": [],
            "ajax": {
                "url": "<s:url action="prizeReportDatatables"/>",
                "data": {
                    remark: $('#remark').val()

                }
            },
            "columns": [
                {"data": "remark"},
                {"data": "datetimeAdd"},
                {"data": "amt"},
                {"data": "price"},
                {"data": "totalAmt"},
                {"data": "memberCode"}
            ],
            "rowId": "remark"
        });

    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess: function (json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function () {
                    $("#topupPackageReportForm").submit(); // refresh page
                });
            },  // onFailure using the default
            onFailure: function (json, error) {
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }


</script>

<form id="navForm" method="post">
    <input type="hidden" name="remark" id="remarks"/>
</form>


<s:form name="prizeReportForm" id="prizeReportForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:textfield name="remark" id="remark" label="%{getText('remark')}"/>
    <sc:submitData/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <table id="prizeTable" class="table table-bordered" cellspacing="0" width="100%">
                <thead>
                <tr>
                    <th><s:text name="remark"/></th>
                    <th><s:text name="datetimeAdd"/></th>
                    <th><s:text name="quantity"/></th>
                    <th><s:text name="price"/></th>
                    <th><s:text name="totalAmt"/></th>
                    <th><s:text name="memberCode"/></th>

                </tr>
                </thead>
            </table>
        </div>
    </article>
</div>
