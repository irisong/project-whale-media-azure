<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="page-header position-relative">
    <h1><s:text name="title.agentAdd"/></h1>
</div>

<script type="text/javascript">
    $(function() {
        $("#agentForm").validate( {
            submitHandler : function(form) {
                form.submit();
            }, // submitHandler
            rules: {
                "agent.agentCode":{
                    required : true,
                    minlength : 3
                },
                "agent.agentName":{
                    required : true,
                    minlength : 5
                },
                "agent.email":{
                    required: true,
                    email: true
                },
                "password":{
                    required: true,
                    minlength: 5
                },
                "remoteAgentUser.username":{
                    required : "#enableRemote:checked",
                    minlength : 5
                },
                remotePassword:{
                    required : "#enableRemote:checked",
                    minlength: 5
                }
            }
        });

        $("#enableRemote").click(function(event){
            var check = $(this).prop("checked");
            enableRemote(check);
        });

        enableRemote($("#enableRemote").prop("checked"));
    });

    function enableRemote(enable){
        if(enable){
            $("#remoteAgentUser\\.allowIp").removeAttr("readonly");
            $("#remoteAgentUser\\.username").removeAttr("readonly");
            $("#remotePassword").removeAttr("readonly");
        }else{
            $("#remoteAgentUser\\.allowIp").attr("readonly", "readonly");
            $("#remoteAgentUser\\.username").attr("readonly", "readonly");
            $("#remotePassword").attr("readonly", "readonly");
        }
    }
</script>

<s:form action="agentSave" name="agentForm" id="agentForm" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center" />
    <s:textfield name="agent.agentCode" id="agent.agentCode" label="%{getText('agentCode')}" required="true" size="20" maxlength="20"/>
    <s:textfield name="agent.agentName" id="agent.agentName" label="%{getText('agentName')}" required="true" size="50" maxlength="100"/>
    <s:textfield name="agent.phoneNo" id="agent.phoneNo" label="%{getText('phoneNo')}" required="true" size="50" maxlength="30"/>

    <s:textfield name="agent.email" id="agent.email" label="%{getText('email')}" required="true" size="50" maxlength="50"/>
    <s:select list="currencies" name="agent.defaultCurrencyCode" id="agent.defaultCurrencyCode" label="%{getText('defaultCurrencyCode')}" listKey="currencyCode" listValue="currencyCode"/>

    <s:password name="password" id="password" label="%{getText('password')}" required="true" size="20" maxlength="10"/>

    <div class="row-fluid">
        <div class="span6">
            <div class="widget-box" id="remoteDiv">
                <div class="widget-header header-color-green">
                    <h5><strong><s:text name="remoteAccess"/></strong></h5>
                    <div class="widget-toolbar no-border">
                        <label>
                            <s:text name="enable"/>
                            <s:checkbox name="enableRemote" id="enableRemote" cssClass="ace ace-switch ace-switch-3" theme="simple"/>
                            <span class="lbl"></span>
                        </label>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main">
                        <s:textfield name="remoteAgentUser.allowIp" id="remoteAgentUser.allowIp" label="%{getText('serverIp')}" size="50" maxlength="50"/>
                        <s:textfield name="remoteAgentUser.username" id="remoteAgentUser.username" label="%{getText('username')}" size="20" maxlength="20"/>
                        <s:password name="remotePassword" id="remotePassword" label="%{getText('password')}" size="20" maxlength="10"/>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <ce:buttonRow>
        <ce:formExtra token="true"/>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="icon-save"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="agentList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>

</s:form>