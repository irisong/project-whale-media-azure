<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    $(function() {
        $("#form").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "luckyDrawPackage.itemQty":{ min:1 }
            }
        });
    });

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="luckyDrawItemList"/>?luckyDrawPackage.packageNo="+json.luckyDrawPackage.packageNo
                        +"&luckyDrawPackage.packageName="+json.luckyDrawPackage.packageName
                        +"&luckyDrawPackage.packageNameCn="+json.luckyDrawPackage.packageNameCn
                        +"&luckyDrawPackage.senderMemberId="+json.luckyDrawPackage.senderMemberId
                        +"&luckyDrawPackage.luckyDrawDate="+json.luckyDrawPackage.luckyDrawDate
                        +"&luckyDrawPackage.status="+json.luckyDrawPackage.status;
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<sc:title title="title.luckyDrawPackageAdd"/>
<s:form action="luckyDrawPackageSave" name="form" id="form" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <div class="row">
        <article class="col-md-12">
            <s:textfield name="luckyDrawPackage.packageName" id="luckyDrawPackage.packageName" label="%{getText('packageName')}" cssClass="input-xxlarge" required="true"/>
            <s:textfield name="luckyDrawPackage.packageNameCn" id="luckyDrawPackage.packageNameCn" label="%{getText('packageNameCn')}" cssClass="input-xxlarge" required="true"/>
            <sc:memberTextField name="memberCode" required="true"/>
            <ce:datepicker name="luckyDrawPackage.luckyDrawDate" id="luckyDrawPackage.luckyDrawDate" label="%{getText('eventDate')}" cssStyle="z-index: 100 !important;" required="true"/>
            <s:textfield name="luckyDrawPackage.itemName" id="luckyDrawPackage.itemName" label="%{getText('itemName')}" cssClass="input-xxlarge" required="true"/>
            <s:textfield name="luckyDrawPackage.itemNameCn" id="luckyDrawPackage.itemNameCn" label="%{getText('itemNameCn')}" cssClass="input-xxlarge" required="true"/>
            <s:textfield name="luckyDrawPackage.itemQty" id="luckyDrawPackage.itemQty" label="%{getText('itemQty')}" cssClass="input-xxlarge" type="number" required="true"/>
        </article>
    </div>

    <ce:buttonRow>
        <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
            <i class="fa fa-floppy-o"></i>
            <s:text name="btnSave"/>
        </s:submit>
        <s:url var="urlExit" action="luckyDrawPackageList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn btn-secondary" type="button">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>
