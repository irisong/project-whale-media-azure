<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function loadDatatables() {
        $('#listTable').DataTable({
            "destroy" : true,
            "order": [0, "asc"],
            "ajax": {
                "url" : "<s:url action="luckyDrawDefaultWinnerDatatables"/>",
                "data" : {
                    packageId: "<s:property value="packageId"/>"
                }
            },
            "columns" : [
                {"data" : "winSeq"},
                {"data" : "winnerMember.memberCode"},
                {
                    "data":"winSeq",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function () {
                        if ('<s:property value="blnLuckyDrawStarted"/>' === 'false' && '<s:property value="luckyDrawPackage.status"/>' === 'ACTIVE') {
                            return '<button type="button" class="btn btn-danger btn-xs btnDelete"><s:text name="btnDelete"/></button>';
                        }

                        return '';
                    }
                }
            ],
            "rowId": "id"
        });
    }

    $(function(){
        $(document).on("click", ".btnDelete", function(){
            var id = $(this).parents("tr").attr("id");

            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="luckyDrawDefaultWinnerDelete"/>", {
                    "id": id
                }, processJsonSave);
            });
        });

        $("#btnAdd").click(function () {
            $("#addDefaultWinnerModal").dialog('open').dialog('vcenter');
        });

        $("#btnExit").click(function () {
            $("#packageNo").val($("#luckyDrawDefaultWinner\\.packageNo").val());
            $("#navForm").attr("action", "<s:url action="luckyDrawItemList"/>")
            $("#navForm").submit();
        });

        $(function() {
            $("#addForm").validate( {
                submitHandler : function(form) {
                    messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                        $("body").loadmask("<s:text name="processing.msg"/>");

                        $(form).ajaxSubmit({
                            dataType: 'json',
                            success: processJsonSave
                        });
                    });
                } // submitHandler
            });
        });

        loadDatatables();
    }); // end $(function())

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function() {
                    $("#addDefaultWinnerModal").dialog('close');
                    loadDatatables();
                });
            },  // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="packageNo" id="packageNo"/>
</form>

<sc:title title="title.luckyDrawDefaultWinnerList"/>
<div class="row">
    <article class="col-md-12 col-sm-12">
        <s:if test="luckyDrawPackage.status == 'ACTIVE' && blnLuckyDrawStarted == false">
            <button id="btnAdd" type="button" class="btn btn-primary">
                <i class="fa fa-plus-circle"></i>
                <s:text name="btnAdd"/>
            </button>
        </s:if>
        <div class="well">
            <fieldset><legend><s:text name="title.luckyDrawDefaultWinnerList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="listTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="winSeq"/></th>
                            <th><s:text name="memberCode"/></th>
                            <th> </th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>

        <ce:buttonRow>
            <button id="btnExit" class="btn btn-secondary" type="button">
                <i class="icon-remove-sign"></i>
                <s:text name="btnExit"/>
            </button>
        </ce:buttonRow>
    </article>
</div>

<div id="addDefaultWinnerModal" class="easyui-dialog" style="width:500px; height:500px" title="<s:text name="title.luckyDrawDefaultWinnerAdd"/>" closed="true" resizable="true" maximizable="true">
    <s:form action="luckyDrawPackageSaveDefaultWinner" name="addForm" id="addForm" cssClass="form-horizontal">
            <div class="row">
                <article class="col-md-12">
                    <s:textfield type="hidden" name="luckyDrawDefaultWinner.packageId" id="luckyDrawDefaultWinner.packageId"/>
                    <s:textfield type="hidden" name="luckyDrawDefaultWinner.packageNo" id="luckyDrawDefaultWinner.packageNo"/>
                    <s:textfield type="hidden" name="luckyDrawDefaultWinner.luckyDrawPackage.senderMemberId" id="luckyDrawDefaultWinner.luckyDrawPackage.senderMemberId"/>
                    <s:textfield name="luckyDrawDefaultWinner.luckyDrawPackage.itemName" id="luckyDrawDefaultWinner.luckyDrawPackage.itemName" label="%{getText('itemName')}" cssClass="input-xxlarge" readonly="true"/>
                    <s:textfield name="luckyDrawDefaultWinner.luckyDrawPackage.itemQty" id="luckyDrawDefaultWinner.luckyDrawPackage.itemQty" label="%{getText('itemQty')}" cssClass="input-xxlarge" readonly="true"/>
                    <s:textfield name="luckyDrawDefaultWinner.winSeq" id="luckyDrawDefaultWinner.winSeq" label="%{getText('seq')}" cssClass="input-xxlarge" required="true"/>
                    <sc:memberTextField name="memberCode" required="true"/>
                </article>
            </div>

            <ce:buttonRow>
                <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                    <i class="fa fa-floppy-o"></i>
                    <s:text name="btnSave"/>
                </s:submit>
            </ce:buttonRow>
    </s:form>
</div>