<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<sc:title title="title.missionView"/>

<script type="text/javascript">
    $(function() {
        $('#missionDetailListTable').DataTable({
            "order": [],
            "ajax": {
                "url" : "<s:url action="vjMissionDetailListDatatables"/>",
                "data" : {
                    missionId: $('#missionId').val()
                }
            },
            "columns" : [
                {"data" : "name"},
                {"data" : "prize"},
                {"data" : "point"},
                {
                    "data" : "specialMissionId",
                    "render": function (data, type, row) {
                        if (data != null) {
                            return '<s:text name="yes"/>';
                        } else {
                            return '<s:text name="no"/>';
                        }
                    }
                }
            ],
            "rowId": "detailId"
        });

        $("#missionForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });

        $(document).on("click", "#btnReject", function() {
            var id = $("#missionId").val();
            $("#rejectModal").dialog('open').dialog('vcenter');
            $(".missionId").val(id);
            $("#rejectRemark").val('');
            $("#rejectRemark").focus();
        });

        $("#rejectMissionForm").validate({
            submitHandler: function (form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }
        });

        $("#btnCancel").click( function() {
            $("#rejectModal").dialog('close');
        });
    })

    function processJsonSave(json) {
        $("body").unmask();
        new JsonStat(json, {
            onSuccess : function(json) {
                messageBox.info(json.successMessage, function(){
                    window.location = "<s:url action="missionList"/>";
                });
            },  // onFailure using the default
            onFailure : function(json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<s:form action="approveMission" name="missionForm" id="missionForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
    <sc:displayErrorMessage align="center"/>
    <s:hidden name="missionId" id="missionId"/>

    <div class="row">
        <article class="col-md-12">
            <div class="well">
                <s:textfield name="mission.member.memberCode" id="mission.member.memberCode" label="%{getText('memberCode')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="mission.submitDatetime" id="mission.submitDatetime" label="%{getText('submitDatetime')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="mission.status" id="mission.status" label="%{getText('status')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="mission.startDatetime" id="mission.startDatetime" label="%{getText('startDatetime')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="mission.endDatetime" id="mission.endDatetime" label="%{getText('endDatetime')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="mission.vjMissionConfig.missionPeriod" id="mission.vjMissionConfig.missionPeriod" label="%{getText('missionPeriod')}" cssClass="input-xxlarge" readonly="true"/>

                <s:if test="mission.status=='REJECTED'">
                    <s:textfield name="mission.remark" id="mission.remark" label="%{getText('remark')}" cssClass="input-xxlarge" readonly="true"/>
                </s:if>
            </div>
        </article>
    </div>

    <ce:buttonRow>
        <s:if test="mission.status=='PENDING'">
            <s:submit type="button" name="btnApprove" id="btnApprove" key="btnApprove" theme="simple" cssClass="btn btn-info">
                <s:text name="btnApprove"/>
            </s:submit>
            <button id="btnReject" class="btn btn-danger" type="button">
                <s:text name="btnReject"/>
            </button>
        </s:if>

        <s:url var="urlExit" action="missionList"/>
        <ce:buttonExit url="%{urlExit}" cssClass="btn" type="button" theme="simple">
            <i class="fa fa-times"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>

<div id="rejectModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="rejectMission"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="rejectMission" name="rejectMissionForm" id="rejectMissionForm" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <s:textarea name="rejectRemark" id="rejectRemark" label="%{getText('Remark')}" cssClass="input-xxlarge" required="true"/>

                    <input type="hidden" name="missionId" class="missionId"/>
                    <ce:buttonRow>
                        <button id="btnProceed" class="btn btn-primary" type="submit">
                            <s:text name="btnProceed"/>
                        </button>
                        <button id="btnCancel" class="btn btnCancel" type="button">
                            <s:text name="btnCancel"/>
                        </button>
                    </ce:buttonRow>
                </s:form>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.missionDetailList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="missionDetailListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="name"/></th>
                            <th><s:text name="prize"/></th>
                            <th><s:text name="point"/></th>
                            <th><s:text name="isSpecialMission"/></th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>