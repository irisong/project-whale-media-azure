<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>

<sc:title title="title.missionList"/>
<script type="text/javascript">
    var table = null;

    $(function () {
        table  = $('#missionListTable').DataTable({
            "order": [0,"desc"],
            "ajax":{
                "url": "<s:url action="vjMissionListDatatables"/>",
                "data" : {
                    status: $('#status').val()
                }
            },
            "columns": [
                {"data" : "submitDatetime"},
                {"data" : "member.memberCode"},
                {"data" : "status",
                    "render": function (data, type, row) {
                        return $.datagridUtil.formatStatus(data);
                    }},
                {"data" : "startDatetime"},
                {"data" : "endDatetime"},
                {"data" : "vjMissionConfig.missionPeriod"},
                {
                    "data": "hasWinner",
                    "className": "text-nowrap",
                    "render": function (data, type, row) {
                        var result = '<button type="button" class="btn btn-primary btn-xs btnView"><s:text name="btnView"/></button> ';

                        if (data === true) {
                            result += '<button type="button" class="btn btn-warning btn-xs btnWinnerInfo"><s:text name="btnWinnerInfo"/></button>';
                        }

                        return result;
                    }
                },
            ],
            "rowId": "missionId"
        });

        $(document).on("click", ".btnView", function (event) {
            var id = $(this).parents("tr").attr("id");
            $("#missionId").val(id);
            $("#navForm").attr("action", "<s:url action="missionView"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnWinnerInfo", function (event) {
            $("#winnerInfoModal").trigger("reset");

            var id = $(this).parents("tr").attr("id");
            $.getJSON("<s:url action="getWinnerInfo"/>", { missionId: id }, function (json) {
                $("#profileName").val(json.member.memberDetail.profileName);
                $("#whaleliveId").val(json.member.memberDetail.whaleliveId);
                $("#memberCode").val(json.member.memberCode);
                $("#point").val(json.member.point);
            });

            $("#winnerInfoModal").dialog('open').dialog('vcenter');
        });
    })

    function processJsonSave(json) {
        $("body").unmask();

        new JsonStat(json, {
            onSuccess: function (json) {
                messageBox.info(json.successMessage, function () {
                    $("#missionListSearchForm").submit();
                });
            },
            onFailure: function (json, error) {
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="missionId" id="missionId"/>
</form>

<s:form name="missionListSearchForm" id="missionListSearchForm" cssClass="form-horizontal">
    <s:select name="status" id="status" list="missionStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset>
                <legend><s:text name="title.missionList"/></legend>
            </fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="missionListTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="submitDatetime"/></th>
                            <th><s:text name="memberCode"/></th>
                            <th><s:text name="status"/></th>
                            <th><s:text name="startDatetime"/></th>
                            <th><s:text name="endDatetime"/></th>
                            <th><s:text name="missionPeriod"/></th>
                            <th>&nbsp</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="winnerInfoModal" class="easyui-dialog" style="width:600px; height:350px" title="<s:text name="winnerDetails"/>" closed="true" resizable="true" maximizable="true" modal="true">
    <div class="row padding-top-10">
        <div class="col-md-offset-1 col-md-10 col-sm-10">
            <div cssClass="form-horizontal">
                <s:form action="" name="form" id="form" cssClass="form-horizontal" enctype="multipart/form-data" method="post">
                    <s:textfield name="profileName" id="profileName" label="%{getText('profileName')}" cssClass="input-xxlarge" readonly="true"/>
                    <s:textfield name="whaleliveId" id="whaleliveId" label="%{getText('whaleliveId')}" cssClass="input-xxlarge" readonly="true"/>
                    <s:textfield name="memberCode" id="memberCode" label="%{getText('memberCode')}" cssClass="input-xxlarge" readonly="true"/>
                    <s:textfield name="point" id="point" label="%{getText('point')}" cssClass="input-xxlarge" readonly="true"/>
                </s:form>
            </div>
        </div>
    </div>
</div>