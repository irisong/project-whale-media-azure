<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function loadDatatables() {
        $('#listTable').DataTable({
            "destroy" : true,
            "order": false, //cannot use this if use sqlDao
            "ajax": {
                "url" : "<s:url action="luckyDrawPackageDatatables"/>",
                "data" : {
                    memberCode: $("#memberCode").val(),
                    dateFrom: $("#dateFrom").val(),
                    dateTo: $("#dateTo").val(),
                    status: $("#status").val()
                }
            },
            "columns" : [
                {"data" : "packageNo", "orderable" : false},
                {"data" : "packageName", "orderable" : false},
                {"data" : "packageNameCn", "orderable" : false},
                {"data" : "senderMember.memberCode", "orderable" : false},
                {
                    "data" : "luckyDrawDate",
                    "orderable" : false,
                    "render": function (data) {
                        return $.datagridUtil.formatDate(data);
                    }
                },
                {
                    "data" : "status",
                    "orderable" : false,
                    "render": function (data) {
                        return $.datagridUtil.formatStatus(data);
                    }
                },
                {
                    "data": "luckyDrawStarted",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function (data) {
                        var result = '<button type="button" class="btn btn-info btn-xs btnView"><s:text name="btnView"/></button> ';
                        if (data === false) {
                            result += '<button type="button" class="btn btn-danger btn-xs btnDeactivate"><s:text name="btnDeactivate"/></button>';
                        }
                        return result;
                    }
                }
            ],
            "rowId": "packageNo"
        });
    }

    $(function(){
        $("#btnCreate").click(function(){
            $("#navForm").attr("action", "<s:url action="luckyDrawPackageAdd"/>");
            $("#navForm").submit();
        });

        $(document).on("click", ".btnView", function(){
            var id = $(this).parents("tr").attr("id");
            $("#packageNo").val(id);
            $("#navForm").attr("action", "<s:url action="luckyDrawItemList"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnDeactivate", function(){
            var id = $(this).parents("tr").attr("id");

            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="luckyDrawPackageDeactivate"/>", {
                    "packageNo": id
                }, processJsonSave);
            });
        });

        loadDatatables();
    }); // end $(function())

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    loadDatatables();
                });
            }, // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="packageNo" id="packageNo"/>
</form>

<sc:title title="title.luckyDrawPackageList"/>

<s:form name="form" id="form" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <sc:memberTextField name="memberCode"/>
    <sc:datepickerFromTo nameFrom="dateFrom" nameTo="dateTo" labelFrom="eventDate"/>
    <s:select name="status" id="status" list="allStatusList" label="%{getText('status')}" listKey="key" listValue="value"/>

    <ce:buttonRow>
        <button id="btnSearch" type="submit" class="btn btn-success">
            <i class="fa fa-search"></i>
            <s:text name="btnSearch"/>
        </button>
        <button id="btnCreate" class="btn btn-primary" type="button">
            <i class="fa fa-plus-circle"></i>
            <s:text name="btnCreate"/>
        </button>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.luckyDrawPackageList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="listTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="packageNo"/></th>
                            <th><s:text name="packageName"/></th>
                            <th><s:text name="packageNameCn"/></th>
                            <th><s:text name="memberCode"/></th>
                            <th><s:text name="eventDate"/></th>
                            <th><s:text name="status"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>
