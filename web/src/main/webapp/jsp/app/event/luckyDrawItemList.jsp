<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<script type="text/javascript">
    function loadDatatables() {
        $('#listTable').DataTable({
            "destroy" : true,
            "order": [0, "asc"],
            "ajax": {
                "url" : "<s:url action="luckyDrawItemDatatables"/>",
                "data" : {
                    packageNo: $("#luckyDrawPackage\\.packageNo").val(),
                    luckyDrawDate: $("#luckyDrawPackage\\.luckyDrawDate").val()
                }
            },
            "columns" : [
                {"data" : "itemSeq"},
                {"data" : "itemName"},
                {"data" : "itemNameCn"},
                {"data" : "itemQty"},
                {"data" : "remainingQty"},
                {
                    "data":"status",
                    "orderable": false,
                    "className": "text-nowrap",
                    "render": function () {
                        var result = '<button type="button" class="btn btn-info btn-xs btnView"><s:text name="defaultWinner"/></button> ';
                        if ('<s:property value="blnLuckyDrawStarted"/>' === 'false' && '<s:property value="luckyDrawPackage.status"/>' === 'ACTIVE') {
                            result += '<button type="button" class="btn btn-warning btn-xs btnEdit"><s:text name="btnEdit"/></button> '
                                   + '<button type="button" class="btn btn-danger btn-xs btnDelete"><s:text name="btnDelete"/></button>';
                        }

                        return result;
                    }
                }
            ],
            "rowId": "id"
        });
    }

    $(function(){
        $("#btnAddItem").click(function () {
            $("#luckyDrawPackage\\.itemName").val("");
            $("#luckyDrawPackage\\.itemNameCn").val("");
            $("#luckyDrawPackage\\.itemQty").val("");
            $("#addItemModal").dialog('open').dialog('vcenter');
        });

        $(document).on("click", ".btnView", function() {
            var id = $(this).parents("tr").attr("id");
            $("#packageId").val(id);
            $("#navForm").attr("action", "<s:url action="luckyDrawDefaultWinnerList"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnEdit", function() {
            var id = $(this).parents("tr").attr("id");
            $("#packageId").val(id);
            $("#navForm").attr("action", "<s:url action="luckyDrawPackageEdit"/>")
            $("#navForm").submit();
        });

        $(document).on("click", ".btnDelete", function(){
            var id = $(this).parents("tr").attr("id");

            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");
                $.post("<s:url action="luckyDrawPackageDelete"/>", {
                    "packageId": id
                }, processJsonSave);
            });
        });

        $("#itemForm").validate( {
            submitHandler : function(form) {
                messageBox.confirm('<s:text name="promptProceedMsg"/>', function(){
                    $("body").loadmask("<s:text name="processing.msg"/>");

                    $(form).ajaxSubmit({
                        dataType: 'json',
                        success: processJsonSave
                    });
                });
            }, // submitHandler
            rules: {
                "luckyDrawPackage.itemQty":{ min:1 }
            }
        });

        loadDatatables();
    }); // end $(function())

    function processJsonSave(json) {
        new JsonStat(json, {
            onSuccess : function(json) {
                $("body").unmask();
                messageBox.info(json.successMessage, function(){
                    $("#addItemModal").dialog('close');
                    loadDatatables();
                });
            }, // onFailure using the default
            onFailure : function(json, error){
                $("body").unmask();
                messageBox.alert(error);
            }
        });
    }
</script>

<form id="navForm" method="post">
    <input type="hidden" name="packageId" id="packageId"/>
    <input type="hidden" name="packageNo" id="packageNo"/>
    <input type="hidden" name="luckyDrawDate" id="luckyDrawDate"/>
</form>

<sc:title title="title.luckyDrawItemList"/>
<s:form name="form" id="form" cssClass="form-horizontal">
    <sc:displayErrorMessage align="center"/>
    <s:textfield type="hidden" name="luckyDrawPackage.packageNo" id="luckyDrawPackage.packageNo"/>
    <s:textfield name="luckyDrawPackage.packageName" id="luckyDrawPackage.packageName" label="%{getText('packageName')}" cssClass="input-xxlarge" readonly="true"/>
    <s:textfield name="luckyDrawPackage.packageNameCn" id="luckyDrawPackage.packageNameCn" label="%{getText('packageNameCn')}" cssClass="input-xxlarge" readonly="true"/>
    <s:textfield name="luckyDrawPackage.senderMember.memberCode" id="luckyDrawPackage.senderMember.memberCode" label="%{getText('memberCode')}" cssClass="input-xxlarge" readonly="true"/>
    <s:textfield name="luckyDrawPackage.luckyDrawDate" id="luckyDrawPackage.luckyDrawDate" label="%{getText('eventDate')}" cssClass="input-xxlarge" readonly="true"/>
    <s:textfield name="luckyDrawPackage.status" id="luckyDrawPackage.status" label="%{getText('status')}" cssClass="input-xxlarge" readonly="true"/>

    <ce:buttonRow>
        <s:if test="luckyDrawPackage.status == 'ACTIVE' && blnLuckyDrawStarted == false">
            <button id="btnAddItem" class="btn btn-primary" type="button">
                <i class="fa fa-plus-circle"></i>
                <s:text name="btnAddItem"/>
            </button>
        </s:if>
        <s:url var="urlExit" action="luckyDrawPackageList"/>
        <ce:buttonExit type="button" url="%{urlExit}" cssClass="btn btn-light" theme="simple">
            <i class="icon-remove-sign"></i>
            <s:text name="btnExit"/>
        </ce:buttonExit>
    </ce:buttonRow>
</s:form>

<div class="row">
    <article class="col-md-12 col-sm-12">
        <div class="well">
            <fieldset><legend><s:text name="title.luckyDrawItemList"/></legend></fieldset>
            <div class="row">
                <article class="col-md-12 col-sm-12">
                    <table id="listTable" class="table table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th><s:text name="itemSeq"/></th>
                            <th><s:text name="itemName"/></th>
                            <th><s:text name="itemNameCn"/></th>
                            <th><s:text name="itemQty"/></th>
                            <th><s:text name="remainingQty"/></th>
                            <th>&nbsp;</th>
                        </tr>
                        </thead>
                    </table>
                </article>
            </div>
        </div>
    </article>
</div>

<div id="addItemModal" class="easyui-dialog" style="width:500px; height:500px" title="<s:text name="title.luckyDrawPackageAdd"/>" closed="true" resizable="true" maximizable="true">
    <s:form action="luckyDrawPackageSaveItem" name="itemForm" id="itemForm" cssClass="form-horizontal">
        <div class="row">
            <article class="col-md-12">
                <s:textfield type="hidden" name="luckyDrawPackage.packageNo" id="luckyDrawPackage.packageNo"/>
                <s:textfield name="luckyDrawPackage.packageName" id="luckyDrawPackage.packageName" label="%{getText('packageName')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="luckyDrawPackage.packageNameCn" id="luckyDrawPackage.packageNameCn" label="%{getText('packageNameCn')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="luckyDrawPackage.senderMember.memberCode" id="luckyDrawPackage.senderMember.memberCode" label="%{getText('memberCode')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="luckyDrawPackage.luckyDrawDate" id="luckyDrawPackage.luckyDrawDate" label="%{getText('eventDate')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="luckyDrawPackage.status" id="luckyDrawPackage.status" label="%{getText('status')}" cssClass="input-xxlarge" readonly="true"/>
                <s:textfield name="luckyDrawPackage.itemName" id="luckyDrawPackage.itemName" label="%{getText('itemName')}" cssClass="input-xxlarge" required="true"/>
                <s:textfield name="luckyDrawPackage.itemNameCn" id="luckyDrawPackage.itemNameCn" label="%{getText('itemNameCn')}" cssClass="input-xxlarge" required="true"/>
                <s:textfield name="luckyDrawPackage.itemQty" id="luckyDrawPackage.itemQty" label="%{getText('itemQty')}" cssClass="input-xxlarge" type="number" required="true"/>
            </article>
        </div>

        <ce:buttonRow>
            <s:submit type="button" name="btnSave" id="btnSave" key="btnSave" theme="simple" cssClass="btn btn-primary">
                <i class="fa fa-floppy-o"></i>
                <s:text name="btnSave"/>
            </s:submit>
        </ce:buttonRow>
    </s:form>
</div>