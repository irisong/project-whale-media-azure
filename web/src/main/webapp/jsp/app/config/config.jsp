<%@ taglib prefix="s" uri="/struts-tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc" %>
<%@ taglib uri="/compal-struts-ext" prefix="ce" %>

<div class="page-header position-relative">
    <h1>Config Dashboard</h1>
</div>

<script type="text/javascript">
    $(function () {
        // menu select 'Dashboard'
        var $menuKey = $("#dashboard");
        if ($menuKey.length) {
            $menuKey.addClass("active");
            $menuKey.parents("li").addClass("active").addClass("open");
        }

        $("#btnGenerateMenu").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configGenerateMenu"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

        $("#btnResetWalletTypeDesc").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configResetWalletTypeDesc"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });
        $("#btnMigrateWallet81").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configRunMigrateWallet81ToPointWallet"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

        $("#btnAddWhaleCoin").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configRunFreeCoinExistingUser"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

        $("#btnResetPointSummary").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configRunResetPointSuccess"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

        $("#btnResetPointReport").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configRunResetPointReportSuccess"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

        $("#btnResetIncomeReport").click(function (event) {
            var month = $("#incomeMonth").val();
            var year = $("#incomeYear").val();
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configRunResetIncomeReportSuccess"/>',{
                    incomeMonth : month,
                    incomeYear:year

                } ,function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                // refresh page
                                window.location.reload();
                            });
                        }
                    });
                });
            });
        });

        $("#btnGenerateAgoraUid").click(function (event) {
            messageBox.confirm('<s:text name="promptProceedMsg"/>', function () {
                $("body").loadmask("<s:text name="processing.msg"/>");

                $.post('<s:url action="configGenerateAgoraUid"/>', function (json) {
                    $("body").unmask();

                    new JsonStat(json, {
                        onSuccess: function (json) {
                            messageBox.info(json.successMessage, function () {
                                window.location.reload(); // refresh page
                            });
                        }
                    });
                });
            });
        });
    });
</script>

<div class="row-fluid">
    <div class="span6">
        <div class="widget-box transparent">
            <div class="widget-header widget-header-flat">
                <h4 class="lighter">
                    <i class="icon-star orange"></i>
                    Main Functions
                </h4>

                <div class="widget-toolbar">
                    <a href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div class="widget-body">
                <div class="widget-main no-padding">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>
                                <i class="icon-caret-right blue"></i>
                                Name
                            </th>
                            <th>
                                <i class="icon-caret-right blue"></i>
                                Description
                            </th>

                            <th class="hidden-phone">
                                <i class="icon-caret-right blue"></i>
                                Action
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>Re-Generate Menu</td>
                            <td>
                                Re-Generate Menu after function added
                            </td>
                            <td>
                                <button id="btnGenerateMenu" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Reset Wallet Type description</td>
                            <td>
                                Reset Wallet Type description
                            </td>
                            <td>
                                <button id="btnResetWalletTypeDesc" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Migrate Wallet Trx to Wallet Point Trx wallet 81</td>
                            <td>
                                Migrate Wallet Trx to Wallet Point Trx wallet 81
                            </td>
                            <td>
                                <button id="btnMigrateWallet81" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Add Free Whale Coin For first time download</td>
                            <td>
                                Add Free Whale Coin For first time download
                            </td>
                            <td>
                                <button id="btnAddWhaleCoin" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Reset Point Summary</td>
                            <td>
                                Reset Point Summary
                            </td>
                            <td>
                                <button id="btnResetPointSummary" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Reset Point Report</td>
                            <td>
                                Reset Point Report
                            </td>
                            <td>
                                <button id="btnResetPointReport" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        <tr>
                            <td>Reset Income Report</td>
                            <td>
                                <s:select name="incomeMonth" id="incomeMonth" list="allMonthList" listKey="key" listValue="value"/>
                                <s:select name="incomeYear" id="incomeYear" list="allYearList" listKey="key" listValue="value"/>
                            </td>
                            <td>
                                <button id="btnResetIncomeReport" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>

                        <tr>
                            <td>Generate Agora Uid</td>
                            <td>
                                Generate Agora Uid
                            </td>
                            <td>
                                <button id="btnGenerateAgoraUid" class="btn btn-mini btn-danger">
                                    <i class="icon-bolt"></i>
                                    Run
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div><!--/widget-main-->
            </div><!--/widget-body-->
        </div><!--/widget-box-->
    </div>
</div>
