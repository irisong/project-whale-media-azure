/**
 This js only applicable by jquery-struts2 framework only.
 */


/**
 * This function used to detect the JSON return from server is valid or not. if
 * return is 'ERROR', meaning the server throws error. if return is 'SUCCESS',
 * meaning OK. if return is 'UNDEFINED', meaning the JSON is null or undefined.
 *
 * @param json
 * @return
 */
function verifyReturnJsonStatus(json) {
    if (typeof json === 'undefined' || json === null) {
        return 'UNDEFINED';
    }

    if (typeof json['exception.message'] === 'string') {
        return 'ERROR';
    }

    return 'SUCCESS';
}

function JsonStat(json, options) {
    var defaults = {
        onSuccess: function (json) {
            alert("this is default success function for JsonStat. onSuccess function need to be overwrite");
        },
        onFailure: function (json) {
            _showErrorMessage();
        }
    };

    // Extend our default options with those provided.
    var opts = $.extend({}, defaults, options);

    var _json = json;

    function _getJsonStatus() {
        if (typeof _json === 'undefined' || _json === null) {
            return 'UNDEFINED';
        }

        if (typeof _json['exception.message'] === 'string'
            && _json['exception.message'] !== '') {
            return 'ERROR';
        }

        if (typeof _json.fieldErrors === 'object'
            && _json.fieldErrors.length) {
            return 'ERROR';
        }

        if (typeof _json.actionErrors === 'object' && _json.actionErrors.length > 0) {
            return 'ERROR';
        }

        return 'SUCCESS';
    }

    function _getErrorMessage() {
        if (typeof _json === 'undefined' || _json === null) {
            return '';
        }

        if (typeof _json['exception.message'] === 'string'
            && _json['exception.message'] !== '') {
            return _json['exception.message'];
        }

        if (typeof _json.fieldErrors === 'object'
            && _json.fieldErrors.length > 0) {
            var s = '';

            $.each(_json.fieldErrors, function (n, value) {
                s += value.name + " - " + value.msg + "\n";
            });

            return s;
        }

        if (typeof _json.actionErrors === 'object'
            && _json.actionErrors.length) {
            var s = '';

            $.each(_json.actionErrors, function (n, value) {
                s += value + "\n";
            });

            return s;
        }

        return '';
    }

    function _showErrorMessage() {
        messageBox.alert(_getErrorMessage());
    }

    this.getJsonStatus = _getJsonStatus;
    this.getErrorMessage = _getErrorMessage;
    this.showErrorMessage = _showErrorMessage;

    var stat = _getJsonStatus();
    if (stat === 'SUCCESS') {
        opts.onSuccess(_json);
    } else {
        opts.onFailure(_json, _getErrorMessage());
    }
}

/* jquery ui implementation */
__msgBoxJUI = (function () {

        var alert = function (msg) {
            // if not defined
            if (!$("#messageBox_alert").length) {
                var s = "<div id='messageBox_alertModal'>\n<div class='ui-widget'>\n<div class='ui-state-error ui-corner-all' style='padding: 0 .7em;'>"
                    + "<p><span class='ui-icon ui-icon-alert' style='float: left; margin-right: .3em;'></span>"
                    + "<span id='messageBox_alert'></span></p>"
                    + "\n</div>\n</div>\n</div>";

                $(s).appendTo("body");

                $("#messageBox_alertModal").compalDialog({
                    title: "Alert",
                    // position : 'center',
                    buttons: {
                        "OK": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }

            $("#messageBox_alert").html(msg);
            $("#messageBox_alertModal").dialog('open');
        };

        var info = function (msg, okFunc) {
            var _confirm = {
                okFunc: function () {
                }
            };

            if (typeof okFunc === 'function') {
                _confirm.okFunc = okFunc;
            }

            // if not defined
            if (!$("#messageBox_info").length) {
                var s = "<div id='messageBox_infoModal'>\n<div class='ui-widget'>\n<div class='ui-widget-content ui-corner-all' style='padding: 0 .7em;'>"
                    + "<p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>"
                    + "<span id='messageBox_info'></span></p>"
                    + "\n</div>\n</div>\n</div>";

                $(s).appendTo("body");

                $("#messageBox_infoModal").compalDialog({
                    title: "Information",
                    // position : 'center',
                    buttons: {
                        "OK": function () {
                            _confirm.okFunc();
                            $(this).dialog('close');
                        }
                    }
                });
            }

            $("#messageBox_info").html(msg);
            $("#messageBox_infoModal").dialog('open');
        };

        var confirmYesNoCancel = function (msg, yesFunc, noFunc, cancelFunc) {
            var _confirm = {
                yesFunc : function(){},
                noFunc : function(){},
                cancelFunc : function(){}
            };

            if (typeof yesFunc === 'function') {
                _confirm.yesFunc = yesFunc;
            }

            if (typeof noFunc === 'function') {
                _confirm.noFunc = noFunc;
            }

            if (typeof cancelFunc === 'function') {
                _confirm.cancelFunc = cancelFunc;
            }

            // if not defined
            if (!$("#messageBox_confirmYesNoCancel").length) {
                var s = "<div id='messageBox_confirmYesNoCancelModal'>\n<div class='ui-widget'>\n<div class='ui-widget-content ui-corner-all' style='padding: 0 .7em;'>"
                    + "<p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>"
                    + "<span id='messageBox_confirmYesNoCancel'></span></p>"
                    + "\n</div>\n</div>\n</div>";

                $(s).appendTo("body");

                $("#messageBox_confirmYesNoCancelModal").compalDialog({
                    title: "Confirmation",
                    // position : 'center',
                    buttons: {
                        "Cancel": function () {
                            $(this).dialog('close');
                        },
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Yes": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }

            $("#messageBox_confirmYesNoCancel").html(msg);

            $("#messageBox_confirmYesNoCancelModal").dialog('option', 'buttons', {
                    "Cancel": function () {
                        _confirm.cancelFunc();
                        $(this).dialog('close');
                    },
                    "No": function () {
                        _confirm.noFunc();
                        $(this).dialog('close');
                    },
                    "Yes": function () {
                        _confirm.yesFunc();
                        $(this).dialog('close');
                    }
                }
            );

            $("#messageBox_confirmYesNoCancelModal").dialog('option', 'close', function (event, ui) {
                _confirm.cancelFunc();
            });

            $("#messageBox_confirmYesNoCancelModal").dialog('open');
        };

        var confirm = function (msg, yesFunc, noFunc) {
            var _confirm = {};

            _confirm.yesFunc = function () {
            };
            _confirm.noFunc = function () {
            };
            _confirm.cancelFunc = function () {
            };

            if (typeof yesFunc === 'function') {
                _confirm.yesFunc = yesFunc;
            }

            if (typeof noFunc === 'function') {
                _confirm.noFunc = noFunc;
            }

            if (typeof cancelFunc === 'function') {
                _confirm.cancelFunc = cancelFunc;
            }

            // if not defined
            if (!$("#messageBox_confirm").length) {
                var s = "<div id='messageBox_confirmModal'>\n<div class='ui-widget'>\n<div class='ui-widget-content ui-corner-all' style='padding: 0 .7em;'>"
                    + "<p><span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>"
                    + "<span id='messageBox_confirm'></span></p>"
                    + "\n</div>\n</div>\n</div>";

                $(s).appendTo("body");

                $("#messageBox_confirmModal").compalDialog({
                    title: "Confirmation",
                    // position : 'center',
                    buttons: {
                        "No": function () {
                            $(this).dialog('close');
                        },
                        "Yes": function () {
                            $(this).dialog('close');
                        }
                    }
                });
            }

            $("#messageBox_confirm").html(msg);

            $("#messageBox_confirmModal").dialog('option', 'buttons', {
                    "No": function () {
                        _confirm.noFunc();
                        $(this).dialog('close');
                    },
                    "Yes": function () {
                        _confirm.yesFunc();
                        $(this).dialog('close');
                    }
                }
            );
            $("#messageBox_confirmModal").dialog('option', 'close', function (event, ui) {
                _confirm.cancelFunc();
            });

            $("#messageBox_confirmModal").dialog('open');
        };

        return {
            alert: alert,
            info: info,
            confirmYesNoCancel: confirmYesNoCancel,
            confirm: confirm
        }
    }());


// jeasyui implementation
__msgBoxJEasyUI = (function () {
        var alert = function (msg, okFunc) {
            var _confirm = {
                okFunc: function () {
                }
            };

            if (typeof okFunc === 'function') {
                _confirm.okFunc = okFunc;
            }

            $.messager.alert("Information", msg, "warning", _confirm.okFunc);
        };

        var info = function (msg, okFunc) {
            var _confirm = {
                okFunc: function () {
                }
            };

            if (typeof okFunc === 'function') {
                _confirm.okFunc = okFunc;
            }

            $.messager.alert("Information", msg, "info", _confirm.okFunc);
        };

        var confirmYesNoCancel = function (msg, yesFunc, noFunc, cancelFunc) {
            $.messager.alert("Error", "Jeasyui not yet implement this 'messageBox.confirmYesNoCancel' function", "error");
        };

        var confirm = function (msg, yesFunc, noFunc) {
            var result = true;
            var _confirm = {};
            _confirm.yesFunc = function () {
            };
            _confirm.noFunc = function () {
            };

            if (typeof yesFunc === 'function') {
                _confirm.yesFunc = yesFunc;
            }
            if (typeof noFunc === 'function') {
                _confirm.noFunc = noFunc;
            }

            $.messager.confirm("Confirm", msg, function (r) {
                if (r) {
                    _confirm.yesFunc();
                    result = true;
                } else {
                    _confirm.noFunc();
                    result = false;
                }
            });

            return result;
        };

        return {
            alert: alert,
            info: info,
            confirmYesNoCancel: confirmYesNoCancel,
            confirm: confirm
        }
    }());


// messageBox default implementation is EasyUI
messageBox = __msgBoxJEasyUI;
