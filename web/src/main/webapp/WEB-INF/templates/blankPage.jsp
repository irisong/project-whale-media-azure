<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>
<script type="text/javascript">
$(function(){
	
}); // end $(function())
</script>


<tr>
    <td class="tdLabel"><label for="deptName" class="label">Department <span class="required">*</span></label></td>
    <td>:</td>
    <td>
        <s:textfield name="deptName" id="deptName" label="Department" theme="simple" size="30"/>
        <s:hidden id="user.deptId" name="user.deptId" />
        <input type="button" name="btnSearchDept" id="btnSearchDept" value="Search" />
    </td>
    <td><s:fielderror fieldName="deptName"/></td>
</tr>

<tr>
    <td class="tdLabel"><label for="deptName" class="label"><s:text name=""/><span class="required">*</span></label></td>
    <td>:</td>
    <td>
        <s:textfield name="deptName" id="deptName" label="Department" theme="simple" size="30"/>
    </td>
    <td><s:fielderror fieldName="deptName"/></td>
</tr>