<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">

<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%-- set theme to simple --%>
<s:set var="theme" value="'simple'" scope="request"/>

<sc:serverConfiguration />
<s:set var="applicationName" value="%{@AU@getApplicationName()}"/>

<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>${applicationName}</title>

    <link href="/inspinia/css/bootstrap.min.css" rel="stylesheet">
    <link href="/inspinia/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/inspinia/css/animate.css" rel="stylesheet">
    <link href="/inspinia/css/style.css" rel="stylesheet">
    <link href="/inspinia/css/style2.css" rel="stylesheet">

    <script src="/inspinia/js/jquery-3.1.1.min.js"></script>
    <script src="/inspinia/js/bootstrap.min.js"></script>
</head>
<body class="gray-bg">
<ce:view>
  <tiles:insertAttribute name="content" />
</ce:view>
</body>
</html>