<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<script type="text/javascript">
    //Ext.BLANK_IMAGE_URL = '<c:url value="/css/ext-js/images/default/s.gif"/>';
    //Ext.QuickTips.init();

    // init for checking user status
    $.compalUtil.loginStatusUrl = '<s:url action="loginStatus" namespace="/pub/login"/>';
    //$.compalUtil.loginUrl = '<s:url action="login" namespace="/open/login"/>';
    $.compalUtil.loginUrl = '<s:url value="/pub/login/loginFrame.jsp"/>';
    $.ajaxSetup({ cache: false });

    (function ($) { // hide the namespace
        $.datagridUtil = $.datagridUtil || (function(){
            var formatDate = function(val, row){
                var dateFormat = "dd/mm/yy";
                if(val instanceof Date){
                    return  $.datepicker.formatDate(dateFormat, val);
                }else if(isDate(val)){
                    var d = parseDate(val);
                    return  $.datepicker.formatDate(dateFormat, d);
                }
                return val;
            };

            // checking is 'xxxx-xx-xxT00:00:00' format or not.
            var isDate = function(val){
                var index = val.indexOf("T");
                if(index != 10)
                    return false;
                var res = val.split("-");
                if(res.length == 3)
                    return true;
                else
                    return false;
            };

            // parse 'xxxx-xx-xxT00:00:00' to date.
            var parseDate = function(val){
                var dt = val.split("T");
                var dd = dt[0].split("-");
                var tt = dt[1].split(":");

                return new Date(dd[0], dd[1]-1, dd[2], tt[0], tt[1], tt[2]);
            };

            var formatBoolean = function(val,row){
                if(val=='true'.toUpperCase() || val==true)
                    return 'Y';
                else if(val=='false'.toUpperCase() || val==false)
                    return 'N';
                else
                    return val;
            };

            var formatStatus = function (val,row){
                if(val==='A')
                    return '<s:text name="statApprove"/>';
                else if(val==='N')
                    return '<s:text name="statNew"/>';
                else if(val==='R')
                    return '<s:text name="statReject"/>';
                else if(val==='ACPT')
                    return '<s:text name="statAccepted"/>';
                else if(val==='I')
                    return '<s:text name="statInactive"/>';
                else if(val==='D')
                    return '<s:text name="statDisabled"/>';
                else if(val==='ACCEPTED')
                    return '<s:text name="statAccepted"/>';
                else if(val==='ACTIVE')
                    return '<s:text name="statActive"/>';
                else if(val==='APPROVED')
                    return '<s:text name="statApprove"/>';
                else if(val==='CANCELLED')
                    return '<s:text name="statCancel"/>';
                else if(val==='CHECKED')
                    return '<s:text name="statChecked"/>';
                else if(val==='CLOSED')
                    return '<s:text name="statClosed"/>';
                else if(val==='COMPLETED')
                    return '<s:text name="statCompleted"/>';
                else if(val==='DELETED')
                    return '<s:text name="statDeleted"/>';
                else if(val==='DISABLED')
                    return '<s:text name="statDisabled"/>';
                else if(val==='ERROR')
                    return '<s:text name="statError"/>';
                else if(val==='INACTIVE')
                    return '<s:text name="statInactive"/>';
                else if(val==='NO_ERROR')
                    return '<s:text name="statNoError"/>';
                else if(val==='NOT_APPLICABLE')
                    return '<s:text name="statNotApplicable"/>';
                else if(val==='NEW')
                    return '<s:text name="statNew"/>';
                else if(val==='OPEN')
                    return '<s:text name="statOpen"/>';
                else if(val==='PARTIAL')
                    return '<s:text name="statPartial"/>';
                else if(val==='PENDING')
                    return '<s:text name="statPending"/>';
                else if(val==='PAYMENT_PENDING')
                    return '<s:text name="statPendingPayment"/>';
                else if(val==='REJECTED')
                    return '<s:text name="statRejected"/>';
                else if(val==='USED')
                    return '<s:text name="statUsed"/>';
                else if(val==='PENDING_APPROVAL')
                    return '<s:text name="statPendingApproval"/>';
                else if(val==='ENDED')
                    return '<s:text name="statEnded"/>';
                else
                    return val;
            };

            var formatYesNo = function(val, row){
                if(val==='true'.toUpperCase() || val===true)
                    return '<s:text name="yes"/>';
                if(val==='false'.toUpperCase() || val===false)
                    return '<s:text name="no"/>';
                else
                    return val;
            };

            var formatWalletCurrency = function(val, row){
                <s:iterator var="__walletType" value="%{@AU@getActiveMemberWalletTypes()}">
                if(val=='${__walletType}'){
                    return '<s:text name="%{@AU@getWalletCurrency(#__walletType)}"/>';
                }
                </s:iterator>
                return val;
            };

            var formatWalletName = function(val, row){
                <s:iterator var="__walletType" value="%{@AU@getActiveMemberWalletTypes()}">
                if(val=='${__walletType}'){
                    return '<s:text name="%{@AU@getWalletName(#__walletType)}"/>';
                }
                </s:iterator>
                return val;
            };

            var formatWalletCurrencyWithName = function(val, row){
                <s:iterator var="__walletType" value="%{@AU@getActiveMemberWalletTypes()}">
                if(val=='${__walletType}'){
                    return '<s:text name="%{@AU@getWalletCurrency(#__walletType)}"/> - <s:text name="%{@AU@getWalletName(#__walletType)}"/>';
                }
                </s:iterator>
                return val;
            };

            return {
                formatDate : formatDate,
                formatBoolean : formatBoolean,
                formatYesNo : formatYesNo,
                formatStatus : formatStatus,
                formatWalletCurrency : formatWalletCurrency,
                formatWalletName : formatWalletName,
                formatWalletCurrencyWithName : formatWalletCurrencyWithName
            }
        }());
    })(jQuery);
</script>