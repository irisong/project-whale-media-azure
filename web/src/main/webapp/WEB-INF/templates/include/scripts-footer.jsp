<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<script type="text/javascript">
  jQuery(document).ready(function() {
    // MINIFY MENU
    function minifyMenu($this){
      if (!$(":root").hasClass("menu-on-top")){
        $(":root").toggleClass("minified");
        $(":root").removeClass("hidden-menu");
        $('html').removeClass("hidden-menu-mobile-lock");
        $this.effect("highlight", {}, 500);
      }
    }

    // TOGGLE MENU 
    function toggleMenu (){
      if (!$(":root").hasClass("menu-on-top")){
        $('html').toggleClass("hidden-menu-mobile-lock");
        $(":root").toggleClass("hidden-menu");
        $(":root").removeClass("minified");
      //} else if ( $.root_.hasClass("menu-on-top") && $.root_.hasClass("mobile-view-activated") ) {
      // suggested fix from Christian J�ger 
      } else if ( $(":root").hasClass("menu-on-top") && $(window).width() < 979 ) {  
        $('html').toggleClass("hidden-menu-mobile-lock");
        $(":root").toggleClass("hidden-menu");
        $(":root").removeClass("minified");
      }
    }

    // LAUNCH FULLSCREEN 
    function launchFullscreen (element){
      if (!$.root_.hasClass("full-screen")) {
        $.root_.addClass("full-screen");
        if (element.requestFullscreen) {
          element.requestFullscreen();
        } else if (element.mozRequestFullScreen) {
          element.mozRequestFullScreen();
        } else if (element.webkitRequestFullscreen) {
          element.webkitRequestFullscreen();
        } else if (element.msRequestFullscreen) {
          element.msRequestFullscreen();
        }
      } else {
        $.root_.removeClass("full-screen");
        if (document.exitFullscreen) {
          document.exitFullscreen();
        } else if (document.mozCancelFullScreen) {
          document.mozCancelFullScreen();
        } else if (document.webkitExitFullscreen) {
          document.webkitExitFullscreen();
        }
      }
    }

    $(":root").on('click', '[data-action="minifyMenu"]', function(e) {
      var $this = $(this);
      minifyMenu($this);
      e.preventDefault();
      //clear memory reference
      $this = null;
    });

    $(":root").on('click', '[data-action="toggleMenu"]', function(e) { 
      toggleMenu();
      e.preventDefault();
    });

    $(":root").on('click', '[data-action="launchFullscreen"]', function(e) { 
      launchFullscreen(document.documentElement);
      e.preventDefault();
    });

  });
</script>