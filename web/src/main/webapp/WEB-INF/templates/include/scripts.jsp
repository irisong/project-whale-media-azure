<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<!--<link rel="StyleSheet" href="<c:url value="/struts/compal/css/common.css"/>" type="text/css"/>-->

<!-- #CSS Links -->
<!-- Basic Styles -->
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/bootstrap.min.css"/>">
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/font-awesome.min.css"/>">

<!-- SmartAdmin Styles : Caution! DO NOT change the order -->
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/smartadmin-production-plugins.min.css"/>">
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/smartadmin-production.min.css"/>">
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/smartadmin-skins.min.css"/>">

<!-- SmartAdmin RTL Support -->
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/smartadmin-rtl.min.css"/>">

<!-- We recommend you use "your_style.css" to override SmartAdmin
     specific styles this will also ensure you retrain your customization with each SmartAdmin update. -->
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/smartadmin-ex.css"/>">

<!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/smartadmin/css/demo.min.css"/>">

<!-- #FAVICONS -->
<link rel="shortcut icon" href="<c:url value="/smartadmin/img/favicon/favicon.ico"/>" type="image/x-icon">
<link rel="icon" href="<c:url value="/smartadmin/img/favicon/favicon.ico"/>" type="image/x-icon">

<!-- #GOOGLE FONT -->
<!--<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">-->

<!-- no need to include this because SmartAdmin already included the dataTables.bootstrap.css. -->
<!--<link rel="stylesheet" href="<c:url value="/asset/plugin/datatables/dataTables.bootstrap.css"/>" type="text/css" />-->
<link rel="stylesheet" href="<c:url value="/asset/plugin/datatables-checkboxes/dataTables.checkboxes.css"/>" type="text/css" />
<!-- old library or not in used libraries for references.
<link rel="stylesheet" href="<c:url value="/struts/compal/css/fullcalendar/fullcalendar.css"/>" type="text/css" />
-->
<link rel="stylesheet" href="<c:url value="/struts/compal/css/loadmask/jquery.loadmask.css"/>" type="text/css" />

<link rel="stylesheet" href="<c:url value="/asset/plugin/jeasyui/themes/bootstrap/easyui.css"/>" type="text/css" />
<link rel="stylesheet" href="<c:url value="/asset/plugin/jeasyui/themes/icon.css"/>" type="text/css" />

<!-- #APP SCREEN / ICONS -->
<!-- Specifying a Webpage Icon for Web Clip
   Ref: https://developer.apple.com/library/ios/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html -->
<link rel="apple-touch-icon" href="<c:url value="/smartadmin/img/splash/sptouch-icon-iphone.png"/>">
<link rel="apple-touch-icon" sizes="76x76" href="<c:url value="/smartadmin/img/splash/touch-icon-ipad.png"/>">
<link rel="apple-touch-icon" sizes="120x120" href="<c:url value="/smartadmin/img/splash/touch-icon-iphone-retina.png"/>">
<link rel="apple-touch-icon" sizes="152x152" href="<c:url value="/smartadmin/img/splash/touch-icon-ipad-retina.png"/>">

<!-- iOS web-app metas : hides Safari UI Components and Changes Status Bar Appearance -->
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<!-- Startup image for web apps -->
<link rel="apple-touch-startup-image" href="<c:url value="/smartadmin/img/splash/ipad-landscape.png"/>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:landscape)">
<link rel="apple-touch-startup-image" href="<c:url value="/smartadmin/img/splash/ipad-portrait.png"/>" media="screen and (min-device-width: 481px) and (max-device-width: 1024px) and (orientation:portrait)">
<link rel="apple-touch-startup-image" href="<c:url value="/smartadmin/img/splash/iphone.png"/>" media="screen and (max-device-width: 320px)">



<!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
<script data-pace-options='{ "restartOnRequestAfter": true }' src="<c:url value="/smartadmin/js/plugin/pace/pace.min.js"/>"></script>

<s:if test="%{#request.serverConfiguration.productionMode}">
    <script src="<c:url value="/smartadmin/js/libs/jquery-2.1.1.min.js"/>"></script>
    <script src="<c:url value="/smartadmin/js/libs/jquery-ui-1.10.3.min.js"/>"></script>
</s:if>
<s:else>
    <script src="<c:url value="/smartadmin/js/libs/jquery-2.1.1.js"/>"></script>
    <script src="<c:url value="/smartadmin/js/libs/jquery-ui-1.10.3.js"/>"></script>
</s:else>


<!-- IMPORTANT: APP CONFIG -->
<script src="<c:url value="/smartadmin/js/app.config.js"/>"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events-->
<script src="<c:url value="/smartadmin/js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"/>"></script>

<!-- BOOTSTRAP JS -->
<script src="<c:url value="/smartadmin/js/bootstrap/bootstrap.min.js"/>"></script>

<!-- CUSTOM NOTIFICATION -->
<script src="<c:url value="/smartadmin/js/notification/SmartNotification.min.js"/>"></script>

<!-- JARVIS WIDGETS -->
<script src="<c:url value="/smartadmin/js/smartwidgets/jarvis.widget.min.js"/>"></script>

<!-- EASY PIE CHARTS -->
<script src="<c:url value="/smartadmin/js/plugin/easy-pie-chart/jquery.easy-pie-chart.min.js"/>"></script>

<!-- SPARKLINES -->
<script src="<c:url value="/smartadmin/js/plugin/sparkline/jquery.sparkline.min.js"/>"></script>

<!-- JQUERY VALIDATE -->
<script src="<c:url value="/smartadmin/js/plugin/jquery-validate/jquery.validate.min.js"/>"></script>

<!-- JQUERY MASKED INPUT -->
<script src="<c:url value="/smartadmin/js/plugin/masked-input/jquery.maskedinput.min.js"/>"></script>

<!-- JQUERY SELECT2 INPUT -->
<script src="<c:url value="/smartadmin/js/plugin/select2/select2.min.js"/>"></script>

<!-- JQUERY UI + Bootstrap Slider -->
<script src="<c:url value="/smartadmin/js/plugin/bootstrap-slider/bootstrap-slider.min.js"/>"></script>

<!-- browser msie issue fix -->
<script src="<c:url value="/smartadmin/js/plugin/msie-fix/jquery.mb.browser.min.js"/>"></script>

<!-- FastClick: For mobile devices -->
<script src="<c:url value="/smartadmin/js/plugin/msie-fix/jquery.mb.browser.min.js"/>"></script>

<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->


<!-- Demo purpose only -->
<!--<script src="<c:url value="/smartadmin/js/demo.min.js"/>"></script>-->

<!-- MAIN APP JS FILE -->
<script src="<c:url value="/smartadmin/js/app.min.js"/>"></script>

<!-- ENHANCEMENT PLUGINS : NOT A REQUIREMENT -->
<!-- Voice command : plugin -->
<script src="<c:url value="/smartadmin/js/speech/voicecommand.min.js"/>"></script>

<!-- SmartChat UI : plugin -->
<script src="<c:url value="/smartadmin/js/smart-chat-ui/smart.chat.ui.min.js"/>"></script>
<script src="<c:url value="/smartadmin/js/smart-chat-ui/smart.chat.manager.min.js"/>"></script>



<s:if test="%{#request.serverConfiguration.productionMode}">
    <!-- PRODUCTION JAVASCRIPT -->

    <script type="text/javascript" src="<c:url value="/asset/plugin/datatables/jquery.dataTables.min.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/asset/plugin/datatables-checkboxes/dataTables.checkboxes.min.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/asset/plugin/datatables/dataTables.bootstrap.min.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.scrollTo-min.js"/>"></script>
    <!-- using smartadmin version
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.maskedinput-1.3.1.min.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.additional-methods.js"/>"></script>
    -->
    <script type="text/javascript" src="<c:url value="/asset/plugin/jquery-form/jquery.form.min.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.loadmask.min.js"/>"></script>
    <!-- old library or not in used libraries for references.
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.fullcalendar.min.js"/>"></script>
    -->
</s:if>
<s:else>
    <!-- DEVELOPMENT JAVASCRIPT -->

    <script type="text/javascript" src="<c:url value="/asset/plugin/datatables/jquery.dataTables.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/asset/plugin/datatables-checkboxes/dataTables.checkboxes.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/asset/plugin/datatables/dataTables.bootstrap.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.scrollTo-min.js"/>"></script>
    <!-- using smartadmin version
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.maskedinput-1.3.1.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.validate.additional-methods.js"/>"></script>
    -->
    <script type="text/javascript" src="<c:url value="/asset/plugin/jquery-form/jquery.form.js"/>" ></script>
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.loadmask.js"/>"></script>
    <!-- old library or not in used libraries for references.
    <script type="text/javascript" src="<c:url value="/struts/compal/scripts/jquery/jquery.fullcalendar.js"/>"></script>
    -->
</s:else>


<script type="text/javascript" src="<c:url value="/asset/plugin/jeasyui/jquery.easyui.custom.min.js"/>"></script>
<script type="text/javascript" src="<c:url value="/asset/plugin/jeasyui/jquery.easyui.datagrid-filter.js"/>"></script>

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.extend.js"/>" ></script>
<!--
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.dataTables.extend.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.fullCalendar.extend.js"/>" ></script>
-->
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/compal/jquery.compal.pagination.js"/>" ></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common.js"/>"></script>
<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common-jquery-struts2.js"/>"></script>


<!-- PAGE RELATED PLUGIN(S)
<script src="..."></script>-->

<script type="text/javascript">

    $(document).ready(function() {

        /* DO NOT REMOVE : GLOBAL FUNCTIONS!
         *
         * pageSetUp(); WILL CALL THE FOLLOWING FUNCTIONS
         *
         * // activate tooltips
         * $("[rel=tooltip]").tooltip();
         *
         * // activate popovers
         * $("[rel=popover]").popover();
         *
         * // activate popovers with hover states
         * $("[rel=popover-hover]").popover({ trigger: "hover" });
         *
         * // activate inline charts
         * runAllCharts();
         *
         * // setup widgets
         * setup_widgets_desktop();
         *
         * // run form elements
         * runAllForms();
         *
         ********************************
         *
         * pageSetUp() is needed whenever you load a page.
         * It initializes and checks for all basic elements of the page
         * and makes rendering easier.
         *
         */

        pageSetUp();

        /*
         * ALL PAGE RELATED SCRIPTS CAN GO BELOW HERE
         * eg alert("my home function");
         *
         * var pagefunction = function() {
         *   ...
         * }
         * loadScript("js/plugin/_PLUGIN_NAME_.js", pagefunction);
         *
         * TO LOAD A SCRIPT:
         * var pagefunction = function (){
         *  loadScript(".../plugin.js", run_after_loaded);
         * }
         *
         * OR
         *
         * loadScript(".../plugin.js", run_after_loaded);
         */

    })

</script>

<!-- Your GOOGLE ANALYTICS CODE Below -->
<!--
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>
-->


<c:import url="/WEB-INF/templates/include/common-scripts.jsp" />
