<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<nav>
    <!--
    NOTE: Notice the gaps after each icon usage <i></i>..
    Please note that these links work a bit different than
    traditional href="" links. See documentation for details.
    -->

    <s:set var="menuIcon" value="{'fa-cube','fa-inbox','fa-table', 'fa-pencil-square-o', 'fa-desktop', 'fa-list-alt', 'fa-puzzle-piece', 'fa-calendar', 'fa-briefcase', 'fa-database', 'fa-cube','fa-inbox','fa-table', 'fa-pencil-square-o', 'fa-desktop', 'fa-list-alt', 'fa-puzzle-piece', 'fa-calendar', 'fa-briefcase', 'fa-database'}"/>
    <ul>
        <li>
            <s:a namespace="/app" action="app">
                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent">Dashboard</span>
            </s:a>
        </li>
        <s:iterator var="mainMenu" status="iterStatus" value="userMenus">
            <s:iterator var="level2Menu" status="iterStatus2" value="#mainMenu.subMenus">
                <li>
                    <a href="#" title="<s:text name="%{#level2Menu.menuName}"/>"><i class="fa fa-lg fa-fw ${menuIcon[iterStatus2.index]}"></i> <span class="menu-item-parent"><s:text name="%{#level2Menu.menuName}"/></span></a>
                    <ul>
                        <s:iterator var="level3Menu" value="#level2Menu.subMenus">
                            <s:url var="subMenuUrl" value="%{#level3Menu.menuUrl}" />
                            <li id="${level3Menu.menuId}">
                                <a href="${subMenuUrl}" title="<s:text name="%{#level3Menu.menuDesc}"/>"><span class="menu-item-parent"><s:text name="%{#level3Menu.menuDesc}"/></span></a>
                            </li>
                        </s:iterator>
                    </ul>
                </li>
            </s:iterator>
        </s:iterator>
    </ul>
</nav>