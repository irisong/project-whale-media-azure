<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:serverConfiguration />

<s:set var="applicationName" value="%{@AU@getApplicationName()}"/>
<s:if test="%{#applicationName == ''}">
  <s:set var="applicationName"><tiles:getAsString name="title" /></s:set>
</s:if>

<html>
<head>

<meta charset="utf-8">
<title>${applicationName}</title>
<meta name="description" content="">
<meta name="author" content="">
  
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

<c:import url="/WEB-INF/templates/include/scripts.jsp" />

<script type="text/javascript">
$(function(){
    var keys = "${__menuKey}";
    if(keys !== ""){
        var kk = keys.split(",");
        for(var i=0; i < kk.length; i++){
          var $menuKey = $("#"+kk[i]);
          if($menuKey.length){
              $menuKey.addClass("active");
              $menuKey.parents("li").addClass("active");
              // $menuKey.parents("li").addClass("active").addClass("open");

              var $menuParent = $menuKey.parents("li");
              if($menuParent.length){
                  $menuParent.find("a:first").click();
              }
          }
        }
    }
});
</script>
</head>
  <!--

  TABLE OF CONTENTS.
  
  Use search to find needed section.
  
  ===================================================================
  
  |  01. #CSS Links                |  all CSS links and file paths  |
  |  02. #FAVICONS                 |  Favicon links and file paths  |
  |  03. #GOOGLE FONT              |  Google font link              |
  |  04. #APP SCREEN / ICONS       |  app icons, screen backdrops   |
  |  05. #BODY                     |  body tag                      |
  |  06. #HEADER                   |  header tag                    |
  |  07. #PROJECTS                 |  project lists                 |
  |  08. #TOGGLE LAYOUT BUTTONS    |  layout buttons and actions    |
  |  09. #MOBILE                   |  mobile view dropdown          |
  |  10. #SEARCH                   |  search field                  |
  |  11. #NAVIGATION               |  left panel & navigation       |
  |  12. #MAIN PANEL               |  main panel                    |
  |  13. #MAIN CONTENT             |  content holder                |
  |  14. #PAGE FOOTER              |  page footer                   |
  |  15. #SHORTCUT AREA            |  dropdown shortcuts area       |
  |  16. #PLUGINS                  |  all scripts and plugins       |
  
  ===================================================================
  
  -->
  
  <!-- #BODY -->
  <!-- Possible Classes

    * 'smart-style-{SKIN#}'
    * 'smart-rtl'         - Switch theme mode to RTL
    * 'menu-on-top'       - Switch to top navigation (no DOM change required)
    * 'no-menu'       - Hides the menu completely
    * 'hidden-menu'       - Hides the main menu but still accessable by hovering over left edge
    * 'fixed-header'      - Fixes the header
    * 'fixed-navigation'  - Fixes the main menu
    * 'fixed-ribbon'      - Fixes breadcrumb
    * 'fixed-page-footer' - Fixes footer
    * 'container'         - boxed layout mode (non-responsive: will not work with fixed-navigation & fixed-ribbon)
  -->
  <body class="smart-style-4">
    <!-- #HEADER -->
    <header id="header">
      <div id="logo-group">

        <!-- PLACE YOUR LOGO HERE -->
        <span id="logo">
        <s:action var="systemLogo" name="systemConfigImageViewByType">
            <s:param name="imageType">AD</s:param>
        </s:action>
        <s:if test="%{#systemLogo.contentType != null}">
            <s:url var="imageUrl" action="systemConfigImageViewByType">
                <s:param name="imageType">AD</s:param>
            </s:url>
            <img src="${imageUrl}" style="width: 35px"/>
        </s:if>
        <s:else>
            <img src="<c:url value="/smartadmin/img/logo.png"/>" alt="SmartAdmin"> 
        </s:else>
        </span>
        <!-- END LOGO PLACEHOLDER -->

      </div>

      <!-- #TOGGLE LAYOUT BUTTONS -->
      <!-- pulled right: nav area -->
      <div class="pull-right">

        <!-- collapse menu button -->
        <div id="hide-menu" class="btn-header pull-right">
          <span> <a href="javascript:void(0);" data-action="toggleMenu" title="Collapse Menu"><i class="fa fa-reorder"></i></a> </span>
        </div>
        <!-- end collapse menu -->

        <!-- #MOBILE -->
        <!-- Top menu profile link : this shows only when top menu is active -->
        <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">
          <li class="">
            <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">
              <!--<img src="<c:url value="/smartadmin/img/avatars/sunny.png"/>" alt="John Doe" class="online" />-->
            </a>
            <ul class="dropdown-menu pull-right">
              <li>
                <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0"><i class="fa fa-cog"></i> Setting</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="profile.html" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i> <u>P</u>rofile</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="javascript:void(0);" class="padding-10 padding-top-0 padding-bottom-0" data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>
              </li>
              <li class="divider"></li>
              <li>
                <a href="login.html" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>
              </li>
            </ul>
          </li>
        </ul>

        <!-- logout button -->
        <div id="logout" class="btn-header transparent pull-right">
          <span> <a href="<c:url value="/logout"/>" title="Sign Out" data-action="userLogout" data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i class="fa fa-sign-out"></i></a> </span>
        </div>
        <!-- end logout button -->

        <!-- multiple lang dropdown : find all flags in the flags page -->
        <!--
        <ul class="header-dropdown-list hidden-xs">
          <li>
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-us" alt="United States"> <span> English (US) </span> <i class="fa fa-angle-down"></i> </a>
            <ul class="dropdown-menu pull-right">
              <li class="active">
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-us" alt="United States"> English (US)</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-fr" alt="France"> Français</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-es" alt="Spanish"> Español</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-de" alt="German"> Deutsch</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-jp" alt="Japan"> 日本語</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-cn" alt="China"> 中文</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-it" alt="Italy"> Italiano</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-pt" alt="Portugal"> Portugal</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-ru" alt="Russia"> Русский язык</a>
              </li>
              <li>
                <a href="javascript:void(0);"><img src="<c:url value="/smartadmin/img/blank.gif"/>" class="flag flag-kr" alt="Korea"> 한국어</a>
              </li>

            </ul>
          </li>
        </ul>
        -->
        <!-- end multiple lang -->

      </div>
      <!-- end pulled right: nav area -->

    </header>
    <!-- END HEADER -->

    <!-- #NAVIGATION -->
    <!-- Left panel : Navigation area -->
    <!-- Note: This width of the aside area can be adjusted through LESS variables -->

    <aside id="left-panel">

      <!-- User info -->
      <div class="login-info">
        <span> <!-- User image size is adjusted inside CSS, it should stay as it -->
          &nbsp;&nbsp;
          <a href="javascript:void(0);" id="show-shortcut" data-action="toggleShortcut">
            <span>
              <s:property value="#session.loginUser.username"/>
            </span>
          </a>

        </span>
      </div>
      <!-- end user info -->

      <tiles:insertAttribute name="menu" />

      <span class="minifyme" data-action="minifyMenu">
        <i class="fa fa-arrow-circle-left hit"></i>
      </span>

    </aside>
    <!-- END NAVIGATION -->

    <!-- MAIN PANEL -->
    <div id="main" role="main">

      <%--
      <!-- RIBBON -->
      <div id="ribbon">

        <span class="ribbon-button-alignment">
          <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
          </span>
        </span>

        <!-- breadcrumb -->
        <ol class="breadcrumb">
          <li>Home</li><li>Miscellaneous</li><li>Blank Page</li>
        </ol>
        <!-- end breadcrumb -->

        <!-- You can also add more buttons to the
        ribbon for further usability

        Example below:

        <span class="ribbon-button-alignment pull-right">
        <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
        <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
        <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
        </span> -->

      </div>
      <!-- END RIBBON -->
      --%>

      <!-- MAIN CONTENT -->
      <div id="content">

        <!--
          The ID "widget-grid" will start to initialize all widgets below
          You do not need to use widgets if you dont want to. Simply remove
          the <section></section> and you can use wells or panels instead
          -->

        <!-- widget grid -->
        <section id="widget-grid" class="">

          <!-- row -->

          <div class="row">

            <!-- a blank row to get started -->
            <div class="col-sm-12">
              <ce:view>
                <tiles:insertAttribute name="content" />
              </ce:view>
            </div>

          </div>

          <!-- end row -->

        </section>
        <!-- end widget grid -->

      </div>
      <!-- END MAIN CONTENT -->

    </div>
    <!-- END MAIN PANEL -->

    <!-- PAGE FOOTER -->
    <div class="page-footer">
      <div class="row">
        <div class="col-xs-12 col-sm-6">
          <s:set var="companyName" value="%{@AU@getCompanyName()}"/>
          <s:if test="%{#companyName == ''}">
            <s:set var="companyName"><tiles:getAsString name="title" /></s:set>
          </s:if>
          <span class="txt-color-white">${companyName} © 2019</span>
        </div>
      </div>
    </div>
    <!-- END PAGE FOOTER -->

    <!-- SHORTCUT AREA : With large tiles (activated via clicking user name tag)
    Note: These tiles are completely responsive,
    you can add as many as you like
    -->
    <div id="shortcut">
      <ul>
        <li>
          <a href="inbox.html" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>
        </li>
        <li>
          <a href="calendar.html" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>
        </li>
        <li>
          <a href="gmap-xml.html" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>
        </li>
        <li>
          <a href="invoice.html" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>
        </li>
        <li>
          <a href="gallery.html" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>
        </li>
        <li>
          <a href="profile.html" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>
        </li>
      </ul>
    </div>
    <!-- END SHORTCUT AREA -->

    <!--================================================== -->

    <c:import url="/WEB-INF/templates/include/scripts-footer.jsp" />

  </body>
</html>
