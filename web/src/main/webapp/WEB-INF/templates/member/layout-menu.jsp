<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<nav>
    <!--
    NOTE: Notice the gaps after each icon usage <i></i>..
    Please note that these links work a bit different than
    traditional href="" links. See documentation for details.
    -->

    <s:set var="menuIcon" value="{'fa-cube','fa-inbox','fa-table', 'fa-pencil-square-o', 'fa-desktop', 'fa-list-alt', 'fa-puzzle-piece', 'fa-calendar', 'fa-briefcase', 'fa-database', 'fa-cube','fa-inbox','fa-table', 'fa-pencil-square-o', 'fa-desktop', 'fa-list-alt', 'fa-puzzle-piece', 'fa-calendar', 'fa-briefcase', 'fa-database'}"/>
    <ul>
        <%--
        <li>
            <s:a namespace="/member" action="member">
                <i class="fa fa-lg fa-fw fa-home"></i> <span class="menu-item-parent"><s:text name="title.dashboard"/></span>
            </s:a>
        </li>
        --%>
        <li>
            <s:a namespace="/member" action="bankInfoEdit">
                <i class="fa fa-lg fa-fw fa-cube"></i> <span class="menu-item-parent"><s:text name="title.bankAccountInfo"/></span>
            </s:a>
        </li>
    </ul>
</nav>
