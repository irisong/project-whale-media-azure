<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%-- if this tag change or update, you neeed to update displayMemberSuccessMessage and displayMemberErrorMessage --%>
<%-- copy logic from displayMemberSuccessMessage.tag --%>
<s:if test="%{(#parameters.successMessage!=null && #parameters.successMessage!='') or (successMessage!=null && successMessage!='')}">
    <s:set var="msg" value="%{#parameters.successMessage!=null && #parameters.successMessage!='' ? #parameters.successMessage[0] : successMessage}"/>
    <div class="ui-widget">
        <div style="margin-top: 10px; margin-bottom: 10px; padding: 0 .7em;" class="ui-state-highlight ui-corner-all">
            <p style="margin: 10px"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                <strong><s:property value="msg"/></strong></p>
        </div>
    </div>
</s:if>

<%-- copy logic from displayMemberErrorMessage.tag --%>
<s:if test="%{actionErrors.size>0}">
    <s:iterator value="actionErrors">
        <div class="ui-widget">
            <div style="margin-top: 10px; margin-bottom: 10px; padding: 0 .7em;" class="ui-state-error ui-corner-all">
                <p style="margin: 10px"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-alert"></span>
                    <strong><s:property/></strong></p>
            </div>
        </div>
    </s:iterator>
</s:if>

<s:if test="%{exception!=null}">
    <div class="ui-widget">
        <div style="margin-top: 10px; margin-bottom: 10px; padding: 0 .7em;" class="ui-state-error ui-corner-all">
            <p style="margin: 10px"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-alert"></span>
                <strong><s:property value="exception.message"/></strong></p>
        </div>
    </div>
</s:if>

<s:if test="%{!(#request.serverConfiguration.productionMode) && fieldErrors.size > 0}">
    <div class="ui-widget">
        <div style="margin-top: 10px; margin-bottom: 10px; padding: 0 .7em;" class="ui-state-error ui-corner-all">
            <p style="margin: 10px"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-alert"></span>
                <strong>${fieldErrors}</strong></p>
        </div>
    </div>
</s:if>