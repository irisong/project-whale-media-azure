<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="title" required="true"%>
<%@ attribute name="localizedTitle" required="false" %>

<s:set var="i18nTitle" value="%{getText(#attr.title)}"/>
<s:set var="__title" value="%{#attr.localizedTitle != null ? #attr.localizedTitle : #i18nTitle}"/>

<!-- row -->
<div class="row">
    <!-- col -->
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <h1 class="page-title txt-color-blueDark">
            <!-- PAGE HEADER -->
            <i class="fa-fw fa fa-home"></i>
            ${__title}
        </h1>
    </div>
    <!-- end col -->

    <jsp:doBody/>
</div>
<!-- end row -->