<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="data" required="true" type="java.util.List" %>
<s:set var="__featuredProducts" value="%{#attr.data}"/>

<s:if test="#__featuredProducts != null && #__featuredProducts.size > 0">
<div class="col-xs-12">
    <div class="page-header">
        <h4><s:text name="title.featuredProduct" /></h4>
    </div>
</div>

<div class="col-xs-12">
    <div class="owl-carousel featuredProductsSlider">

    <s:iterator var="featuredProduct" value="__featuredProducts" status="iterStatus">
        <s:set var="product" value="%{#featuredProduct.product}" />
        <s:set var="productPrice" value="%{#product.productPrices[0]}" />
        <s:set var="productImage" value="%{#product.productImages[0]}" />
        <s:if test="%{#productPrice == null}">
            <s:set var="price">N.A</s:set>
        </s:if>
        <s:else>
            <s:set var="price" value="%{@SF@formatDecimal(#productPrice.unitPrice)}" />
        </s:else>
        <s:if test="%{#productImage == null}">
            <s:set var="imageUrl">/bigbag/img/products/placeholder.jpg</s:set>
        </s:if>
        <s:else>
            <s:url var="imageUrl" action="productImageView">
                <s:param name="imageId" value="%{#productImage.productImageId}"/>
            </s:url>
        </s:else>

        <div class="slide">
            <div class="productImage clearfix">
                <img src="<s:property value='%{#imageUrl}'/>" alt="featured-product-img" style="width: 260px; height: 260px;">
                <div class="productMasking">
                    <ul class="list-inline btn-group" role="group">
                        <li><a data-toggle="modal" href="#" class="btn btn-default"><i class="fa fa-heart"></i></a></li>
                        <li>
                            <s:url var="cartUrl" action="cartAdd" namespace="/pub/shop/cart">
                                <s:param name="productId" value="%{#product.productId}" />
                            </s:url>
                            <a href="<s:property value='%{#cartUrl}'/>" class="btn btn-default"><i class="fa fa-shopping-cart"></i></a>
                        </li>
                        <li>
                            <s:url var="viewUrl" action="productDetail" namespace="/pub/shop">
                                <s:param name="productId" value="%{#product.productId}" />
                            </s:url>
                            <a class="btn btn-default" data-toggle="modal" href="<s:property value='%{#viewUrl}'/>" ><i class="fa fa-eye"></i></a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="productCaption clearfix">
                <a href="<s:property value='%{#viewUrl}'/>">
                    <h5><s:property value="%{#product.productName}"/></h5>
                </a>
                <h3>$<s:property value="%{#price}"/></h3>
            </div>
        </div>
    </s:iterator>

    </div>
</div>
</s:if>
