<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="data" required="true" type="java.util.List" %>
<s:set var="__productImages" value="%{#attr.data}"/>

<s:if test="#__productImages != null && #__productImages.size > 0">
<div id="carousel" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
    <s:iterator var="image" value="__productImages" status="iterStatus">
        <s:if test="#iterStatus.count == 1">
            <s:set var="cssClass">item active</s:set>
        </s:if>
        <s:else>
            <s:set var="cssClass">item</s:set>
        </s:else>
        <s:url var="imageUrl" action="productDetailImageView">
            <s:param name="imageId" value="%{#image.productImageId}"/>
        </s:url>
        <div class="<s:property value="#cssClass"/>" data-thumb="${iterStatus.count - 1}">
            <img src="${imageUrl}">
        </div>
    </s:iterator>
    </div>
</div>
<div class="clearfix">
    <div id="thumbcarousel" class="carousel slide" data-interval="false">
        <div class="carousel-inner">
        <s:iterator var="image" value="__productImages" status="iterStatus">
            <s:url var="imageUrl" action="productDetailImageView">
                <s:param name="imageId" value="%{#image.productImageId}"/>
            </s:url>
            <div data-target="#carousel" data-slide-to="${iterStatus.count - 1}" class="thumb thumb-ext">
                <img src="${imageUrl}">
            </div>
        </s:iterator>
        </div>
        <a class="left carousel-control" href="#thumbcarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a class="right carousel-control" href="#thumbcarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
    </div>
</div>
</s:if>
<s:else>
    <img src="/bigbag/img/products/placeholder.jpg">
</s:else>