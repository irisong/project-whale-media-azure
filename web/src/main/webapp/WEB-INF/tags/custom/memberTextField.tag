<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<%@ attribute name="name" description="input name & input id for memberCode" required="true" %>
<%@ attribute name="required" description="default is false" %>

<script type="text/javascript">
    $(function() {
        $(jq("<s:property value="%{'btnSearch'+#attr.name}"/>")).click(function (event){
            $("#__memberModal${name}").dialog('open');
        });

        $("#__memberDatagrid${name}").datagrid({
            onResizeColumn:function(field, width){
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '25%';  // reset the width. Total 4 columns
                $(this).datagrid('resize');
            },
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#attr.name}"/>")).val(rowData.memberCode);

                $("#__memberModal${name}").dialog('close');
            },
            remoteFilter: true
        });

        // enable filter
        $("#__memberDatagrid${name}").datagrid('enableFilter');
    });
</script>

<div class="form-group">
    <label for="${name}" class="col-sm-3 control-label"><s:text name="phoneNo"/>:</label>
    <div class="col-sm-9">
        <div class="input-group">
            <s:if test="#attr.required != null && #attr.required">
                <s:textfield theme="simple" name="%{#attr.name}" id="%{#attr.name}" cssClass="form-control" required="true"/>
            </s:if>
            <s:else>
                <s:textfield theme="simple" name="%{#attr.name}" id="%{#attr.name}" cssClass="form-control"/>
            </s:else>
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" id="btnSearch${name}">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>
</div>


<div id="__memberModal${name}" class="easyui-dialog" style="width:500px; height:350px;display: none" title="<s:text name="title.memberList"/>" closed="true" resizable="true" maximizable="true">
    <table id="__memberDatagrid${name}" class="easyui-datagrid" style="width:100%; height:300px" pagination="true" url="<s:url action="memberListDatagrid" namespace="appMemberPackage"/>" fit="true">
        <thead>
        <tr>
            <th field="memberCode"><s:text name="phoneNo"/></th>
            <th field="memberDetail.profileName" formatter="(function(val, row){return eval('row.memberDetail.profileName')})"><s:text name="name"/></th>
            <th field="memberDetail.whaleliveId" formatter="(function(val, row){return eval('row.memberDetail.whaleliveId')})"><s:text name="whaleliveId"/></th>
            <th field="memberDetail.email" formatter="(function(val, row){return eval('row.memberDetail.email')})"><s:text name="email"/></th>
        </tr>
        </thead>
    </table>
</div>