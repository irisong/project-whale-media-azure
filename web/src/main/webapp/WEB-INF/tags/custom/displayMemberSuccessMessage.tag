<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%-- if this tag change or update, you neeed to update displayMemberMessage.tag as well --%>
<s:if test="%{(#parameters.successMessage!=null && #parameters.successMessage!='') or (successMessage!=null && successMessage!='')}">
    <s:set var="msg" value="%{#parameters.successMessage!=null && #parameters.successMessage!='' ? #parameters.successMessage[0] : successMessage}"/>
    <div class="ui-widget">
        <div style="margin-top: 10px; margin-bottom: 10px; padding: 0 .7em;" class="ui-state-highlight ui-corner-all">
            <p style="margin: 10px"><span style="float: left; margin-right: .3em;" class="ui-icon ui-icon-info"></span>
                <strong><s:property value="msg"/></strong></p>
        </div>
    </div>
</s:if>