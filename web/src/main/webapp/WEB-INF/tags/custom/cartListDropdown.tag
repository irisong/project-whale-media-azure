<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<s:set var="cart" value="%{@AU@getCartList()}" />
<s:set var="grandTotal" value="%{#cart != null ? #cart.grandTotal : 0}"/>

<script type="text/javascript">
    $(function(){
        $("#btnCartCheckoutOnTop").click(function(event){
            <s:if test="%{!#cart.cartItems.isEmpty}">
            window.location = "<s:url action="cartCheckout" namespace="/pub/shop/cart" />";
            </s:if>
            <s:else>
            messageBox.alert("<s:text name="cartIsEmpty"/>");
            </s:else>
        });
    });
</script>

<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-shopping-cart"></i>$ <s:property value="%{@SF@formatDecimal(#grandTotal)}" /></a>

<ul class="dropdown-menu dropdown-menu-right">
    <li><s:text name="itemInYourCart" /></li>
    <s:iterator var="cartItem" value="%{#cart.cartItems}" status="iterStatus">
    <li>
        <s:url var="productDetailUrl" action="productDetail">
            <s:param name="productId" value="%{#cartItem.productId}" />
        </s:url>
        <s:url var="imageUrl" action="productImageView">
            <s:param name="imageId" value="%{#cartItem.product.productImages[0].productImageId}"/>
        </s:url>
        <a href="<s:property value="%{#productDetailUrl}" />">
            <div class="media">
                <img class="media-left media-object" src="<s:property value="%{#imageUrl}" />" alt="cart-Image" width="60" height="50">
                <div class="media-body">
                    <h5 class="media-heading" style="display: inline-grid; line-height: 0.8;">
                        <span style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"><s:property value="%{#cartItem.product.productName}" /></span><br>
                        <span><s:property value="%{quantity}" /> X $ <s:property value="%{@SF@formatDecimal(subTotalPrice)}" /></span>
                    </h5>
                </div>
            </div>
        </a>
    </li>
    </s:iterator>
    <li>
        <div class="btn-group" role="group" aria-label="...">
            <s:url var="cartListUrl" action="cartList" namespace="/pub/shop/cart" />
            <button type="button" class="btn btn-default" onclick="location.href='${cartListUrl}';"><s:text name="shoppingCart" /></button>
            <button type="button" class="btn btn-default" id="btnCartCheckoutOnTop"><s:text name="checkout" /></button>
        </div>
    </li>
</ul>