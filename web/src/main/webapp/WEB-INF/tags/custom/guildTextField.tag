<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<%@ attribute name="name" description="input name & input id for guildName" required="true" %>
<%@ attribute name="required" description="default is false" %>

<script type="text/javascript">
    $(function() {
        $(jq("<s:property value="%{'btnSearch'+#attr.name}"/>")).click(function (event){
            $("#__guildModal${name}").dialog('open');
        });

        $("#__guildDatagrid${name}").datagrid({
            onResizeColumn:function(field, width){
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '25%';  // reset the width. Total 4 columns
                $(this).datagrid('resize');
            },
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#attr.name}"/>")).val(rowData.name);

                $("#__guildModal${name}").dialog('close');
            },
            remoteFilter: true
        });

        // enable filter
        $("#__guildDatagrid${name}").datagrid('enableFilter');
    });
</script>

<div class="form-group">
    <label for="${name}" class="col-sm-3 control-label"><s:text name="guildName"/>:</label>
    <div class="col-sm-9">
        <div class="input-group">
            <s:if test="#attr.required != null && #attr.required">
                <s:textfield theme="simple" name="%{#attr.name}" id="%{#attr.name}" cssClass="form-control" required="true"/>
            </s:if>
            <s:else>
                <s:textfield theme="simple" name="%{#attr.name}" id="%{#attr.name}" cssClass="form-control"/>
            </s:else>
            <span class="input-group-btn">
                <button class="btn btn-success" type="button" id="btnSearch${name}">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
    </div>
</div>


<div id="__guildModal${name}" class="easyui-dialog" style="width:500px; height:350px;display: none" title="<s:text name="title.guildList"/>" closed="true" resizable="true" maximizable="true">
    <table id="__guildDatagrid${name}" class="easyui-datagrid" style="width:100%; height:300px" pagination="true" url="<s:url action="guildListDatagrid" namespace="appMemberPackage"/>" fit="true">
        <thead>
        <tr>
            <th field="name"><s:text name="guildName"/></th>
            <th field="displayId" ><s:text name="guildDisplayId"/></th>
            <th field="president" ><s:text name="president"/></th>
             </tr>
        </thead>
    </table>
</div>