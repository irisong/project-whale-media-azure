<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
$(function(){
	// when window scrolled, set window top position to 'screenPosY'
	$(window).scroll(function(){
		$("#screenPosY").val($(window).scrollTop());	
	});

	// make window scrollTo position before submit
	$.scrollTo(<s:property value="screenPosY" default="0"/>, 0);
});
</script>

<s:hidden name="screenPosY" id="screenPosY" />