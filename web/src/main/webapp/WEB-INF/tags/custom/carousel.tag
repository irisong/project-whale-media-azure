<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="data" required="true" type="java.util.List" %>
<s:set var="__carousels" value="%{#attr.data}"/>

<s:if test="#__carousels != null && #__carousels.size > 0">
<div class="bannercontainer bannerV1">
    <div class="fullscreenbanner-container">
        <div class="fullscreenbanner">
            <ul>
            <s:iterator var="carousel" value="__carousels" status="iterStatus">
                <li data-transition="slidehorizontal" data-slotamount="5" data-masterspeed="1000" data-title="Slide ${iterStatus.count}">
                    <img src="/bigbag/img/home/banner-slider/slider-bg.jpg" alt="slidebg${iterStatus.count}" data-bgfit="cover" data-bgposition="center center" data-bgrepeat="no-repeat">
                    <div class="slider-caption slider-captionV1 container">
                        <div class="tp-caption rs-caption-1 sft start"
                             data-hoffset="0"
                             <s:if test="#iterStatus.count % 2 == 0 ">
                                 data-y="85"
                             </s:if>
                             <s:else>
                                 data-x="370"
                                 data-y="54"
                             </s:else>
                             data-speed="800"
                             data-start="1500"
                             data-easing="Back.easeInOut"
                             data-endspeed="300">
                            <s:if test="#carousel.carouselImage != null">
                                <s:url var="imageUrl" action="productCarouselImageView">
                                    <s:param name="imageId" value="%{#carousel.carouselImage.carouselImageId}"/>
                                </s:url>
                                <img src="${imageUrl}" alt="slider-image">
                            </s:if>
                        </div>
                        <div class="tp-caption rs-caption-2 sft"
                             data-hoffset="0"
                             data-y="119"
                             <s:if test="#iterStatus.count % 2 == 0 ">
                                 data-x="800"
                             </s:if>
                             data-speed="800"
                             data-start="2000"
                             data-easing="Back.easeInOut"
                             data-endspeed="300">
                            ${carousel.caption}
                        </div>
                        <div class="tp-caption rs-caption-3 sft"
                             data-hoffset="0"
                             data-y="185"
                             <s:if test="#iterStatus.count % 2 == 0 ">
                                 data-x="800"
                             </s:if>
                             data-speed="1000"
                             data-start="3000"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="off">
                            ${carousel.lead}
                            <br />
                            <small>${carousel.paragraph}</small>
                        </div>
                        <div class="tp-caption rs-caption-4 sft"
                             data-hoffset="0"
                             data-y="320"
                             <s:if test="#iterStatus.count % 2 == 0 ">
                                 data-x="800"
                             </s:if>
                             data-speed="800"
                             data-start="3500"
                             data-easing="Power4.easeOut"
                             data-endspeed="300"
                             data-endeasing="Power1.easeIn"
                             data-captionhidden="off">
                            <span class="page-scroll">
                                <a href="${carousel.link}" class="btn primary-btn"><s:text name="buyNow" /><i class="glyphicon glyphicon-chevron-right"></i></a>
                            </span>
                        </div>
                    </div>
                </li>
            </s:iterator>
            </ul>
        </div>
    </div>
</div>
</s:if>