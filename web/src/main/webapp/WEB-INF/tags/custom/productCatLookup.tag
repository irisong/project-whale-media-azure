<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="btnSearchId" required="false" description="search button id which to trigger Agent Popup Window. Default should be 'btnSearch'" %>
<%@ attribute name="productCatNameId" required="false" description="productCatName html ID. Default should be productCatName"  %>
<%@ attribute name="productCatIdId" required="false" description="productCatId html ID. Default should be productCatId"  %>

<s:set var="btnSearchId" value="%{#attr.btnSearchId == null ? 'btnSearch' : #attr.btnSearchId}"/>
<s:set var="productCatNameId" value="%{#attr.productCatNameId == null ? 'productCatName' : #attr.productCatNameId}"/>
<s:set var="productCatIdId" value="%{#attr.productCatIdId == null ? 'productCatId' : #attr.productCatIdId}"/>
<script type="text/javascript">
    function formatProductParent(val, row){
        if(row.parentProductCat){
            return row.parentProductCat.productCatName;
        }
        return "";
    }

    $(function() {
        $(jq("<s:property value="%{#btnSearchId}"/>")).click(function (event){
            $("#__productCatModal").dialog('open');

            $("#__productCatDatagrid").datagrid("clearSelections");
        });

        $("#__productCatDatagrid").datagrid({
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#productCatNameId}"/>")).val(rowData.productCatName);
                $(jq("<s:property value="%{#productCatIdId}"/>")).val(rowData.productCatId);

                $("#__productCatModal").dialog('close');
            },
            remoteFilter: true,
            singleSelect: true,
            onResizeColumn:function(field, width){
                var col = $(this).datagrid('getColumnOption', field);
                col.width = '50%';  // reset the width. Total 5 columns
                $(this).datagrid('resize');
            }
        });

        // enable filter
        $("#__productCatDatagrid").datagrid('enableFilter');
    });
</script>
<div id="__productCatModal" class="easyui-dialog" style="width:500px; height:300px" title="<s:text name="productCategory"/>" closed="true" resizable="true" maximizable="true">
    <table id="__productCatDatagrid" style="width:480px; height:250px" fitColumns="true" rownumbers="true" pagination="true" url="<s:url action="productCatListDatagrid" />" sortName="productCatName" fit="true">
        <thead>
        <tr>
            <th field="productCatName" sortable="true"><s:text name="name"/></th>
            <th field="parentProductCat.productCatName" formatter="formatProductParent"><s:text name="parent"/></th>
        </tr>
        </thead>
    </table>
</div>