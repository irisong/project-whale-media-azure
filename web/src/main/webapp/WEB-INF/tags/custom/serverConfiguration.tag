<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@tag import="com.compalsolutions.compal.application.Application"%>
<%@tag import="com.compalsolutions.compal.application.ServerConfiguration"%>
<%
	ServerConfiguration serverConfiguration = (ServerConfiguration) Application
			.lookupBean(ServerConfiguration.BEAN_NAME);
	request.setAttribute(ServerConfiguration.BEAN_NAME, serverConfiguration);
%>