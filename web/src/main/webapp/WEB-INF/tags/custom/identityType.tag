<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="type" required="true"%>
<s:set var="__Type" value="%{#attr.type}"/>


<s:if test="%{#__Type == 'PB'}">
    <s:set var="__IdentityType" value="%{getText('passport').toUpperCase()}" />
</s:if>
<s:elseif test="%{#__Type == 'DL'}">
    <s:set var="__IdentityType" value="%{getText('drivingLicence').toUpperCase()}" />
</s:elseif>
<s:elseif test="%{#__Type == 'IC'}">
    <s:set var="__IdentityType" value="%{getText('idCard').toUpperCase()}" />
</s:elseif>
<s:elseif test="%{#__Type == 'OT'}">
    <s:set var="__IdentityType" value="%{getText('other').toUpperCase()}" />
</s:elseif>
<s:else>
    <s:set var="__IdentityType" value="%{__Type}" />
</s:else>


<s:property value="%{__IdentityType}" />