<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="btnSearchId" required="false" description="search button id which to trigger Agent Popup Window. Default should be 'btnSearch'" %>
<%@ attribute name="agentCodeId" required="false" description="agent code html ID. Default should be agentCode"  %>

<s:set var="btnSearchId" value="%{#attr.btnSearchId == null ? 'btnSearch' : #attr.btnSearchId}"/>
<s:set var="agentCodeId" value="%{#attr.agentCodeId == null ? 'agentCode' : #attr.agentCodeId}"/>
<script type="text/javascript">
    $(function() {
        $(jq("<s:property value="%{#btnSearchId}"/>")).click(function (event){
            $("#__agentModal").dialog('open');
        });

        $("#__agentDatagrid").datagrid({
            onClickRow: function(rowIndex, rowData){
                $(jq("<s:property value="%{#agentCodeId}"/>")).val(rowData.agentCode);

                $("#__agentModal").dialog('close');
            }
        });
    });
</script>
<div id="__agentModal" class="easyui-dialog" style="width:500px; height:300px;display: none" title="<s:text name="title.agentList"/>" closed="true" resizable="true" maximizable="true">
    <table id="__agentDatagrid" class="easyui-datagrid" style="width:480px; height:250px" fitColumns="true" rownumbers="true" pagination="true" url="<s:url action="agentListDatagrid" namespace="appAgentPackage"/>" fit="true">
        <thead>
        <tr>
            <th field="agentCode" width="80"><s:text name="agentCode"/></th>
            <th field="agentName" width="120"><s:text name="agentName"/></th>
            <th field="email" width="200"><s:text name="email"/></th>
            <th field="status" width="80" formatter="$.datagridUtil.formatStatus"><s:text name="status"/></th>
            <th field="datetimeAdd" width="150"><s:text name="datetime.add"/></th>
        </tr>
        </thead>
    </table>
</div>