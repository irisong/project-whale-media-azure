<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="debug" required="false" description="default is false"%>

<c:if test="${empty debug}">
    <c:set var="debug" value="true"/>
</c:if>
<c:if test="${debug}">
<script type="text/javascript" src="<c:url value="/scripts/firebug-lite-compressed.js"/>" ></script>
</c:if>