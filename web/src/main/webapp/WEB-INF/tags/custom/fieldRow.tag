<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<%@ attribute name="labelFor" %>
<%@ attribute name="label" %>
<%@ attribute name="required" %>
<%@ attribute name="labelSeparator" %>

<tr>
    <td class="tdLabel" valign="top">
    <c:if test="${not empty labelFor}">
    	<label for="${labelFor}" class="label">${label}&nbsp;
    	<c:if test="${required==true}">
    	<span class="required">*</span>
    	</c:if>
    	</label>
    </c:if>
    <c:if test="${empty labelFor}">
    <c:if test="${not empty label}">
    ${label}&nbsp;
    </c:if>
    	<c:if test="${required==true}">
    	<span class="required">*</span>
    	</c:if>
    </c:if>
    </td>
 	<td valign="top">:</td>
 	<td><jsp:doBody/></td>
	<td></td>
</tr>