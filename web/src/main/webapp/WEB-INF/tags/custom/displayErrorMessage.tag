<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ attribute name="align" required="false" description="default is center"%>
<s:if test="%{actionErrors.size>0}">
	<s:iterator value="actionErrors">
		<div class="alert alert-warning">
			<i class="fa-fw fa fa-warning"></i>
			<strong><s:property/></strong><br/>
		</div>
	</s:iterator>
</s:if>

<s:if test="%{exception!=null}">
	<div class="alert alert-warning">
		<i class="fa-fw fa fa-warning"></i>
		<strong><s:property value="exception.message"/></strong><br/>
	</div>
</s:if>

<s:if test="%{!(#request.serverConfiguration.productionMode) && fieldErrors.size > 0}">
	<div class="alert alert-warning">
		<i class="fa-fw fa fa-warning"></i>
		<strong>${fieldErrors}</strong><br/>
	</div>
</s:if>