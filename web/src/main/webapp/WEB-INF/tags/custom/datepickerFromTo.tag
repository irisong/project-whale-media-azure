<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<%@ attribute name="nameFrom" description="input name & input id for dateFrom" required="true" %>
<%@ attribute name="nameTo" description="input name & input id for dateTo" required="true" %>
<%@ attribute name="requiredFrom" description="default is false" %>
<%@ attribute name="requiredTo" description="default is false" %>
<%@ attribute name="labelFrom" %>
<%@ attribute name="labelTo" %>

<s:set var="i18nFrom" value="%{#attr.labelFrom != null ? getText(#attr.labelFrom) : getText('date')}"/>
<s:set var="i18nTo" value="%{#attr.labelTo != null ? getText(#attr.labelTo) : getText('to')}"/>

<div class="form-group">
    <label for="${nameFrom}" class="col-sm-3 control-label">${i18nFrom}:</label>
    <div class="col-sm-9">
        <div class="form-group">
            <div class="col-sm-5">
                <div class="input-group">
                    <s:if test="#attr.requiredFrom != null && #attr.requiredFrom">
                        <ce:datepicker theme="simple" name="%{#attr.nameFrom}" id="%{#attr.nameFrom}" cssClass="form-control" required="true" cssStyle="z-index: 100 !important;"/>
                    </s:if>
                    <s:else>
                        <ce:datepicker theme="simple" name="%{#attr.nameFrom}" id="%{#attr.nameFrom}" cssClass="form-control" cssStyle="z-index: 100 !important;"/>
                    </s:else>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>

            <label class="col-sm-2 control-label" for="${nameTo}">${i18nTo}</label>

            <div class="col-sm-5">
                <div class="input-group">
                    <s:if test="#attr.requiredTo != null && #attr.requiredTo">
                        <ce:datepicker theme="simple" name="%{#attr.nameTo}" id="%{#attr.nameTo}" cssClass="form-control" required="true" cssStyle="z-index: 100 !important;"/>
                    </s:if>
                    <s:else>
                        <ce:datepicker theme="simple" name="%{#attr.nameTo}" id="%{#attr.nameTo}" cssClass="form-control" cssStyle="z-index: 100 !important;"/>
                    </s:else>
                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                </div>
            </div>
        </div>
    </div>
</div>