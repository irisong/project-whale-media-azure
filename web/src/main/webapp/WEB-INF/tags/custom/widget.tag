<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="id" required="true" description="require a unigue ID" %>
<%@ attribute name="title" required="true"%>
<%@ attribute name="localizedTitle" required="false" %>

<%@ attribute name="editButton" description="default is false" %>
<%@ attribute name="colorButton" description="defalut is false" %>
<%@ attribute name="deleteButton" description="default is false" %>
<%@ attribute name="fullScreenButton" description="default is false" %>
<%@ attribute name="cssClass" description="css applied on widget root DOM" %>

<s:set var="i18nTitle" value="%{getText(#attr.title)}"/>
<s:set var="__title" value="%{#attr.localizedTitle != null ? #attr.localizedTitle : #i18nTitle}"/>

<s:set var="__editButton" value="%{#attr.editButton == 'true' ? '' : 'data-widget-editbutton=\"false\"'}"/>
<s:set var="__colorButton" value="%{#attr.colorButton == 'true' ? '' : 'data-widget-colorbutton=\"false\"'}"/>
<s:set var="__deleteButton" value="%{#attr.deleteButton == 'true' ? '' : 'data-widget-deletebutton=\"false\"'}"/>
<s:set var="__fullScreenButton" value="%{#attr.fullScreenButton == 'true' ? '' : 'data-widget-fullscreenbutton=\"false\"'}"/>

<!-- Widget ID (each widget will need unique ID)-->
<div class="jarviswidget ${cssClass}" id="${id}" ${__editButton} ${__colorButton} ${__deleteButton} ${__fullScreenButton}>
    <!-- widget options:
      usage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">

      data-widget-colorbutton="false"
      data-widget-editbutton="false"
      data-widget-togglebutton="false"
      data-widget-deletebutton="false"
      data-widget-fullscreenbutton="false"
      data-widget-custombutton="false"
      data-widget-collapsed="true"
      data-widget-sortable="false"

    -->
    <header>
        <h2>${__title}</h2>
    </header>

    <!-- widget div-->
    <div>

        <!-- widget edit box -->
        <div class="jarviswidget-editbox">
            <!-- This area used as dropdown edit box -->
        </div>
        <!-- end widget edit box -->

        <!-- widget content -->
        <div class="widget-body">
            <!-- this is what the user will see -->
            <jsp:doBody/>
        </div>
        <!-- end widget content -->

    </div>
    <!-- end widget div -->

</div>
<!-- end widget -->