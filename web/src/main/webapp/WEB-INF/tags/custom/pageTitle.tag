<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="title" required="true"%>
<%@ attribute name="localizedTitle" required="false" %>
<%@ attribute name="bgImage" required="false" %>

<s:set var="i18nTitle" value="%{getText(#attr.title)}"/>
<s:set var="__title" value="%{#attr.localizedTitle != null ? getText(#attr.localizedTitle) : #i18nTitle}"/>
<s:set var="__bgImage" value="%{#attr.bgImage != null ? #attr.bgImage : 'false'}"/>

<s:if test="%{#__bgImage == 'false'}">
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeader">
  <div class="container">
    <div class="row">
      <div class="col-xs-6">
        <div class="page-title">
          <h2><s:property value="%{__title}" /></h2>
        </div>
      </div>
      <div class="col-xs-6">
        <ol class="breadcrumb pull-right">
          <li>
            <a href="<s:url action="index" namespace="/pub/shop" />"><s:text name="menuItemHome" /></a>
          </li>
          <li class="active"><s:property value="%{__title}" /></li>
        </ol>
      </div>
    </div>
  </div>
</section>
</s:if>
<s:else>
<!-- LIGHT SECTION -->
<section class="lightSection clearfix pageHeaderImage">
  <div class="container">
    <div class="tableBlock">
      <div class="row tableInner">
        <div class="col-xs-12">
          <div class="page-title">
            <h2><s:property value="%{__title}" /></h2>
            <ol class="breadcrumb">
              <li>
                <a href="<s:url action="index" namespace="/pub/shop" />"><s:text name="menuItemHome" /></a>
              </li>
              <li class="active"><s:property value="%{__title}" /></li>
            </ol>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
</s:else>