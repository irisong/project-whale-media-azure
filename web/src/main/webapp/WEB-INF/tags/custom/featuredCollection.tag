<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ tag body-content="scriptless" %>

<%@ attribute name="data" required="true" type="java.util.List" %>
<s:set var="__featuredCollections" value="%{#attr.data}"/>

<s:if test="#__featuredCollections != null && #__featuredCollections.size > 0">
<div class="col-xs-12">
    <div class="page-header">
        <h4><s:text name="title.featuredCollection" /></h4>
    </div>
</div>

<s:iterator var="featuredCollection" value="__featuredCollections" status="iterStatus">
<s:url var="imageUrl" action="featuredCollectionImageView">
    <s:param name="imageId" value="%{#featuredCollection.featuredCollectionImage.featuredCollectionImageId}"/>
</s:url>
<div class="col-sm-4 col-xs-12">
    <div class="thumbnail" onclick="location.href='${featuredCollection.link}';">
        <div class="imageWrapper">
            <img src="${imageUrl}" alt="feature-collection-image">
            <div class="masking">
                <a href="${featuredCollection.link}" class="btn viewBtn"><s:text name="btnViewProducts" /></a>
            </div>
        </div>
        <div class="caption">
            <h4>${featuredCollection.title}</h4>
        </div>
    </div>
</div>
</s:iterator>
</s:if>