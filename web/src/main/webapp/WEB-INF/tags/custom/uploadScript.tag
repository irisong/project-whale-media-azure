<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
<link rel="stylesheet" href="<c:url value="/asset/plugin/fileupload/jquery.fileupload.css"/>" type="text/css" />

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script type="text/javascript" src="<c:url value="/asset/plugin/fileupload/jquery.iframe-transport.js"/>"></script>
<!-- The basic File Upload plugin -->
<script type="text/javascript" src="<c:url value="/asset/plugin/fileupload/jquery.fileupload.js"/>"></script>