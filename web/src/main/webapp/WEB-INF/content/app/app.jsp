<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.dashboard" />

<script type="text/javascript">
    $(function(){
        // menu select 'Dashboard'
        var $menuKey = $("#dashboard");
        if($menuKey.length){
            $menuKey.addClass("active");

            $menuKey.parents("li").addClass("active").addClass("open");
        }

        $('#dg').datagrid({
            onClickRow: function(index, row){
                console.log(row.announceId);
                $("#announcementModal").dialog('open').dialog('refresh', "<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId="+row.announceId);
            }
        });

        $("#announcePagination").compalPagination({
            url: "<s:url namespace="/app/notice" action="announcePaging"/>",
            totalPage: ${totalPageAnnouncements},
            pageSize: ${pageSizeAnnouncements}
        });

        $("body").delegate("a.announceShow", "click",function(event){
            event.preventDefault();

            var url = $(this).attr("href");
            var title = $(this).html();

            $("#announcementModal").dialog('open')
                .dialog("setTitle", "<s:text name="title.announcement"/>: " + title)
                .dialog('refresh', url);
        })
    });

    function formatActionColumn(val, row){
        ///return "<a href='#'>"+"View"+"</a>";
        return "<a class=\"blue btnView\" ><i class=\"icon-zoom-in bigger-130\"></i><s:text name="btnView"/></a>";
    }
</script>

<div class="row">
    <article class="col-md-6 col-sm-12">
        <sc:widget id="widget00" title="currencyExchangeRate">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th class="text-center" colspan="2" style="width: 70%"><s:text name="currency"/></th>
                    <th class="text-center" rowspan="2" style="width: 30%"><s:text name="rate"/></th>
                </tr>
                <tr><th class="text-center"><s:text name="from"/></th><th class="text-center"><s:text name="to"/></th></tr>
                </thead>
                <tbody>
                <s:iterator var="currencyExchange" value="currencyExchanges">
                    <tr>
                        <td>${currencyExchange.currencyCodeFrom}</td>
                        <td>${currencyExchange.currencyCodeTo}</td>
                        <td class="text-right"><s:property value="%{@SF@formatCosting(#currencyExchange.rate)}"/></td>
                    </tr>
                </s:iterator>
                </tbody>
            </table>
        </sc:widget>
    </article>
</div>

<div class="row">
    <article class="col-md-6 col-sm-12">
        <sc:widget id="widget10" title="title.announcement">
            <div id="announcePagination"></div>
        </sc:widget>
    </article>
    <article class="col-md-6 col-sm-12">
        <sc:widget id="widget11" title="title.documentDownload">
            <table class="table table-striped table-bordered table-condensed">
                <thead>
                <tr>
                    <th class="text-center" style="width: 20%"><s:text name="date"/></th>
                    <th class="text-center" style="width: 80%"><s:text name="title"/></th>
                </tr>
                </thead>
                <tbody>
                <s:if test="docFiles.size()==0">
                    <tr><td colspan="2"><div class="alert alert-info"><s:text name="noRecordFound"/></div></td></tr>
                </s:if>
                <s:iterator var="docFile" value="docFiles">
                    <tr>
                        <td><s:date name="#docFile.datetimeAdd" format="%{getText('default.server.date.format')}"/></td>
                        <td><a href="<s:url namespace="/app/notice" action="docfileDownload"/>?docFile.docId=${docFile.docId}">${docFile.title}</a></td>
                    </tr>
                </s:iterator>
                </tbody>
            </table>
        </sc:widget>
    </article>
</div>


<%--
<div class="row-fluid">
    <div class="span6 widget-container-span ui-sortable">
        <div class="widget-header">
            <h5 class="lighter smaller"><s:text name="title.announcement"/></h5>
        </div>
        <div class="widget-body">
            <div class="widget-main">
                <table id="dg" style="width:720px;height:250px"
                       url="<s:url namespace="/app/notice" action="announceListDatagridForDashboard"/>"
                       rownumbers="true" pagination="true" singleSelect="true" sortName="publishDate" sortOrder="desc">
                    <thead>
                    <tr>
                        <th field="publishDate" width="120" sortable="true" formatter="$.datagridUtil.formatDate"><s:text name="publishDate"/></th>
                        <th field="title" width="500" sortable="true"><s:text name="title"/></th>
                        <th field="announceId" width="80" sortable="true" formatter="formatActionColumn">&nbsp;</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
--%>

<div id="announcementModal" class="easyui-dialog" style="width:700px; height:500px" title="<s:text name="title.announcement"/>" closed="true" resizable="true" maximizable="true">
</div>