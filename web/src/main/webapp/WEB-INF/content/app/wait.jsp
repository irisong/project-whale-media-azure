<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<html>
<head>
    <meta http-equiv="refresh" content="5;url=<s:url includeParams="all"/>"/>
</head>

<body>
    <p style="border: 1px solid silver; padding: 5px; background: #ffd; text-align: center;">
        We are processing your request. Please wait. 
	<img  src="<c:url value="/css/autocomplete/indicator.gif"/>" />
	<!--<br/>
    You can click this link to <a href="<s:url includeParams="all"/>">refresh</a>.
	</p>-->
	
</body>
</html>