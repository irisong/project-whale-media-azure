<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<script type="text/javascript">
$(function() {
	<s:if test="#parameters.showSuccessExitButton">
		<s:url var="exitUrl" action="%{#parameters.successExitAction}" namespace="%{#parameters.successExitNamespace}"/>
		$("#btnExit").compalExit("${exitUrl}");
	</s:if>
	<s:if test="#parameters.showSuccessAddNewButton">
		<s:url var="addNewUrl" action="%{#parameters.successAddNewAction}" namespace="%{#parameters.successAddNewNamespace}"/>
		$("#btnAddNew").compalGoto("${addNewUrl}");
	</s:if>
}); // end $(function())
</script>
<br/>
<br/>

<%--
<sc:displayErrorMessage align="center" />
<div class='ui-widget' style="width: 90%">
<div class='ui-widget-content ui-corner-all' style='padding: 0 .7em;'>
<p>
	<center>
	<span class='ui-icon ui-icon-info' style='float: left; margin-right: .3em;'></span>
	<span>${param.successMessage}</span>
	<div><br/></div>
	<s:if test="#parameters.showSuccessExitButton || #parameters.showSuccessAddNewButton">
	<div>
		<s:if test="#parameters.showSuccessExitButton">
			<input type="button" name="btnExit" id="btnExit" value="${param.successExitButtonLabel}"/>
		</s:if>
		<s:if test="#parameters.showSuccessAddNewButton">
			<input type="button" name="btnAddNew" id="btnAddNew" value="${param.successAddNewButtonLabel}"/>
		</s:if>
		<div><br/></div>
	</div>
	</s:if>
	</center>
</p>
</div>
</div>
--%>

<article class="col-sm-12 col-xs-6">
	<div class="alert alert-success">
		<i class="fa-fw fa fa-info"></i>
		<strong>${param.successMessage}</strong>
	</div>
	<s:if test="#parameters.showSuccessExitButton || #parameters.showSuccessAddNewButton">
		<div class="col-sm-12">
			<s:if test="#parameters.showSuccessExitButton">
				<input type="button" name="btnExit" id="btnExit" value="${param.successExitButtonLabel}"/>
			</s:if>
			<s:if test="#parameters.showSuccessAddNewButton">
				<input type="button" name="btnAddNew" id="btnAddNew" value="${param.successAddNewButtonLabel}"/>
			</s:if>
			<div><br/></div>
		</div>
	</s:if>
</article>