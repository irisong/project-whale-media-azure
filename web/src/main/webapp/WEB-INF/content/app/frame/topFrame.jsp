<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<sc:serverConfiguration />

<html>
  <head>
    <title></title>
    <c:import url="/WEB-INF/templates/include/scripts.jsp" />
    <style type="text/css">
	    .topMenu {
	    	padding-left : 30px;
	    	padding-right : 10px;
	    	padding-top : 5px;
	    	padding-bottom: 5px;
	    	background : #EBF2FD none repeat scroll 0 0; 
	    	color : #AAAAAA;
	    }
	    
	    .topMenuItem {
	    	font-size : 14px;
	    	color : #FF6801;
	    }
    </style>
    
    <script type="text/javascript">
	jQuery(document).ready(function(){
	});
</script>
  </head>
  <body>
  	<sc:displayErrorMessage align="center" />
  	<div style="background-color: #457093" align="left">
  		<img src="<s:url value="/image/toplogo.jpg"/>" border="0">
  	</div>
  	<table class="topMenu" width="100%" cellpadding="0" cellspacing="0" border="0">
  		<tr>
  			<td align="left"><span style="color : #000000"><s:text name="user"/> : <s:property value="#session.loginUser.username"/> &nbsp;&nbsp;&nbsp;&nbsp;</span></td>
  			<td align="right">
  				<s:iterator var="mainMenu" status="iterStatus" value="mainMenus">
		  			<s:url var="menuUrl" action="menuFrame" namespace="/app/frame">
						<s:param name="mainMenuId" value="%{#mainMenu.menuId}" />
					</s:url>
		  			<a href="${menuUrl}" class="topMenuItem" target="leftFrame"><s:text name="%{#mainMenu.menuName}"/></a>
		  			|
		  		</s:iterator>
		  		<s:url var="logoutUrl" value="/logout"/>
  				<a href="${logoutUrl}" class="topMenuItem" target="_parent"><s:text name="btnLogout"/></a>
  				&nbsp;&nbsp;&nbsp;&nbsp;
  			</td>
  		</tr>
  	</table>
  </body>
</html>