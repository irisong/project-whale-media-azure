<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>

<br/>
<br/>
<br/>
<br/>


<article class="col-sm-12 col-xs-6">
    <div class="alert alert-danger">
        <i class="fa-fw fa fa-warning"></i>
        <strong><s:text name="errorMessage.no.access"/></strong>
    </div>
</article>