<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<sc:title title="title.dashboard" />

<script type="text/javascript">
    $(function(){
        // menu select 'Dashboard'
        var $menuKey = $("#dashboard");
        if($menuKey.length){
            $menuKey.addClass("active");

            $menuKey.parents("li").addClass("active").addClass("open");
        }

        $('#dg').datagrid({
            onClickRow: function(index, row){
                console.log(row.announceId);
                $("#announcementModal").dialog('open').dialog('refresh', "<s:url namespace="/app/notice" action="announceShow"/>?announcement.announceId="+row.announceId);
            }
        });

        $("#announcePagination").compalPagination({
            url: "<s:url namespace="/app/notice" action="announcePaging"/>",
            totalPage: ${totalPageAnnouncements},
            pageSize: ${pageSizeAnnouncements}
        });

        $("body").delegate("a.announceShow", "click",function(event){
            event.preventDefault();

            var url = $(this).attr("href");
            var title = $(this).html();

            $("#announcementModal").dialog('open')
                .dialog("setTitle", "<s:text name="title.announcement"/>: " + title)
                .dialog('refresh', url);
        })
    });

    function formatActionColumn(val, row){
        ///return "<a href='#'>"+"View"+"</a>";
        return "<a class=\"blue btnView\" ><i class=\"icon-zoom-in bigger-130\"></i><s:text name="btnView"/></a>";
    }
</script>
