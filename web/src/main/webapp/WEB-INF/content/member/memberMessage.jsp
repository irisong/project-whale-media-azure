<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="padding">
    <div class="content_title"></div>
</div>
<!--<div class="content_line"></div>-->
<br class="clear">

<table cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td><br/><br/></td>
    </tr>
    <tr>
        <td><br/><sc:displayMemberMessage/></td>
    </tr>
    </tbody>
</table>

<c:import url="/WEB-INF/templates/include/footer.jsp"/>