<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-custom" prefix="sc"%>
<%@ taglib uri="/compal-struts-ext" prefix="ce"%>

<div class="padding">
        <div class="content_title"></div>
    </div>
    <!--<div class="content_line"></div>-->
    <br class="clear">

<script type="text/javascript" language="javascript">
    $(function() {
        $("#topupForm").validate({
            messages : {
                transactionPassword: {
                    remote: "<s:text name="password_is_not_valid"/>"
                }
            },
            rules : {
                "transactionPassword" : {
                    required : true
                }
            },
            submitHandler: function(form) {
                waiting();
                form.submit();
            }
        });
    });
</script>

<table cellpadding="0" cellspacing="0">
    <tbody>
    <tr>
        <td class="tbl_sprt_bottom"><span class="txt_title"><s:text name="login_password_is_required"/></span></td>
    </tr>
    <tr>
        <td><br/><sc:displayMemberErrorMessage/></td>
    </tr>
    <tr>
        <td>
            <s:form action="securityPasswordRequired" id="topupForm" name="topupForm" method="post">
                <input type="hidden" name="submitData" value="true"/>
                <table cellspacing="0" cellpadding="0" class="tbl_form">
                    <colgroup>
                        <col width="1%">
                        <col width="30%">
                        <col width="69%">
                        <col width="1%">
                    </colgroup>
                    <tbody>
                    <tr>
                        <th class="tbl_header_left">
                            <div class="border_left_grey">&nbsp;</div>
                        </th>
                        <th colspan="2"><s:text name="please_key_in_your_login_password"/></th>
                        <th class="tbl_header_right">
                            <div class="border_right_grey">&nbsp;</div>
                        </th>
                    </tr>

                    <tr class="tbl_form_row_odd">
                        <td>&nbsp;</td>
                        <td><s:text name="login_password"/></td>
                        <td>
                            <input name="transactionPassword" type="password" id="transactionPassword"/>
                        </td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr class="tbl_form_row_odd">
                        <td>&nbsp;</td>
                        <td></td>
                        <td align="right">
                            <button id="btSubmit"><s:text name="btnSubmit"/></button>
                        </td>
                        <td>&nbsp;</td>
                    </tr>

                    </tbody>
                </table>
            </s:form>
        </td>
    </tr>
    </tbody>
</table>

<c:import url="/WEB-INF/templates/include/footer.jsp"/>