<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<script type="text/javascript" src="<c:url value="/struts/compal/scripts/common.js"/>"></script>

<script type="text/javascript">
var isCaptchaFailed = "<%= "true".equalsIgnoreCase(request.getParameter("captchaFailed"))%>";
var isLoginFailed = "<%= "true".equalsIgnoreCase(request.getParameter("loginFailed"))%>";
var cookie = readCookie("loginUrlCookies");
var url = "/pub/login";
if(cookie!=null){
    url = cookie;
}
if(isCaptchaFailed == 'true'){
    if(url.indexOf('?')>= 0){
        url += "&captchaFailed=true";
    }else{
        url += "?captchaFailed=true";
    }
}
if(isLoginFailed == 'true'){
    if(url.indexOf('?')>= 0){
        url += "&loginFailed=true";
    }else{
        url += "?loginFailed=true";
    }
}
parent.location = url;
</script>
