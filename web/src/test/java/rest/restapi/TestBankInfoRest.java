package rest.restapi;

import com.compalsolutions.compal.WebUtil;
import com.compalsolutions.compal.member.vo.MemberFile;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.file.FileDataBodyPart;
import org.junit.Ignore;
import org.junit.Test;
import rest.RestUtil;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;
import java.io.File;

public class TestBankInfoRest {
    private static final Log log = LogFactory.getLog(TestBankInfoRest.class);

    private String token = "eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJvbW5pY3BsdXMtc2VydmVyIiwiZXhwIjoxNTc2ODE1NTY1LCJzdWIiOiI2MDEyMjI3NTI5MSIsImp0aSI6IkRKTTJoWDZ2MkhUUHV4aGxQbjlqOUEiLCJ1c2VyQWdlbnQiOiJQb3N0bWFuUnVudGltZS83LjE5LjAiLCJpZCI6ImYxYjQwNjdkNmJmMWZjOTIwMTZiZjIwMGYzOWIwMDJiIiwidHlwZSI6Ik1FTUJFUiIsImxvY2FsZUNvZGUiOiJ6aCIsImRldmljZU5hbWUiOiJBTkRST0lEIiwicmVmZXJyYWxDb2RlIjoicWgyNDRzIiwidXNlcm5hbWUiOiI2MDEyMjI3NTI5MSIsImFjY2Vzc2VzIjpbIkFDVF9ST0xFX01FTUJFUiIsIlJPTEVfVVNFUiJdfQ.oChvCdF-Ico-8RfwRbQyP9Azh0lJrXipXtaKgfIaX1A";

    @Ignore
    @Test
    public void test() {
        Client client = RestUtil.newJerseyClient();

        WebTarget webTarget = client.target("http://localhost:9010/rest/bankInfo/uploadFile");

        String fileUrl = "src/main/webapp/asset/image/coin/omc.png";
        File file = new File(fileUrl);

        if(!file.exists()) {
            log.debug("file not exists!!");
            return;
        }

        final FileDataBodyPart filePart = new FileDataBodyPart("fileUpload", file);
        final FormDataMultiPart multiPart = new FormDataMultiPart()
                .field("uploadType", MemberFile.UPLOAD_TYPE_BANK);
        multiPart.bodyPart(filePart);

        Response response = webTarget.request() //
                .header("Authorization", "Bearer " + token) //
                .post(Entity.entity(multiPart, multiPart.getMediaType()));

//        String jsonString = response.readEntity(String.class);
//        log.debug(jsonString);
    }
}
