package rest;

import java.security.Key;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;
import org.jose4j.keys.HmacKey;
import org.junit.Assert;
import org.junit.Test;

public class JwtKeyTest {
    @Test
    public void testJwtKeyTest() throws Exception {
        JwtClaims claims = new JwtClaims();
        claims.setExpirationTimeMinutesInTheFuture(5);
        claims.setSubject("foki");
        claims.setIssuer("the issuer");
        claims.setAudience("the audience");

        String secret = "helloworld";
        Key key = new HmacKey(secret.getBytes("UTF-8"));

        JsonWebSignature jws = new JsonWebSignature();
        jws.setPayload(claims.toJson());
        jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.HMAC_SHA256);
        jws.setKey(key);
        jws.setDoKeyValidation(false); // relaxes the key length requirement

        String jwt = jws.getCompactSerialization();
        JwtConsumer jwtConsumer = new JwtConsumerBuilder().setRequireExpirationTime() //
                .setAllowedClockSkewInSeconds(30) //
                .setRequireSubject() //
                .setExpectedIssuer("the issuer") //
                .setExpectedAudience("the audience") //
                .setVerificationKey(key).setRelaxVerificationKeyValidation() // relaxes key length requirement
                .build();

        JwtClaims processedClaims = jwtConsumer.processToClaims(jwt);
        Assert.assertNotNull(processedClaims);
    }
}
