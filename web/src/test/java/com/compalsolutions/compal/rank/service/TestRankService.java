package com.compalsolutions.compal.rank.service;

import com.compalsolutions.compal.BDUtil;
import com.compalsolutions.compal.Global;
import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.cache.service.TestJwtTokenCacheService;
import com.compalsolutions.compal.function.schedule.RunTaskLogger;
import com.compalsolutions.compal.income.IncomeProvider;
import com.compalsolutions.compal.live.streaming.service.LiveStreamService;
import com.compalsolutions.compal.live.streaming.vo.LiveStreamChannel;
import com.compalsolutions.compal.member.service.MemberService;
import com.compalsolutions.compal.member.vo.Member;
import com.compalsolutions.compal.paymentGateway.RazerPayClient;
import com.compalsolutions.compal.rank.vo.MemberRankTrx;
import com.compalsolutions.compal.rank.vo.RankConfig;
import com.compalsolutions.compal.util.DateUtil;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.helpers.DateTimeDateFormat;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class TestRankService {
    private static final Log log = LogFactory.getLog(TestJwtTokenCacheService.class);

    private RankService rankService;

    private MemberService memberService;

    private LiveStreamService liveStreamService;

    private IncomeProvider incomeProvider;

    @Before
    public void init() {
        rankService = Application.lookupBean(RankService.BEAN_NAME, RankService.class);
        memberService = Application.lookupBean(MemberService.BEAN_NAME, MemberService.class);
        liveStreamService = Application.lookupBean(LiveStreamService.BEAN_NAME, LiveStreamService.class);
        incomeProvider = Application.lookupBean(IncomeProvider.BEAN_NAME, IncomeProvider.class);
    }

    @Ignore
    @Test
    public void testFollowExperience() {
        String memberCode = "60149456773";
        Member member = memberService.findMemberByMemberCode(memberCode);

        rankService.doProcessMemberExperience(Global.LOCALE_CN, member.getMemberId(), "Follower", "ABC123",
                "primaryKey", MemberRankTrx.FOLLOW, 0, BigDecimal.ZERO, "601488889999");
    }

    @Ignore
    @Test
    public void testViewLiveStreamExperience() {
        String memberCode = "60149456773";
        Member member = memberService.findMemberByMemberCode(memberCode);

        rankService.doProcessMemberExperience(Global.LOCALE_CN, member.getMemberId(), "LiveStreamChannel", "ABC123",
                "primaryKey", MemberRankTrx.VIEW_STREAM, 60, BigDecimal.ZERO, "601488889999");
    }

    @Ignore
    @Test
    public void testGiftPointStreamExperience() {
        String memberCode = "60149456773";
        Member member = memberService.findMemberByMemberCode(memberCode);

        rankService.doProcessMemberExperience(Global.LOCALE_CN, member.getMemberId(), "PointTrx", "ABC123",
                "primaryKey", MemberRankTrx.SPEND, 0, new BigDecimal("1000"), "601488889999");
    }

    @Ignore
    @Test
    public void testIncome() throws ParseException {
        incomeProvider.doProcessVjIncome(new RunTaskLogger());
    }

    @Ignore
    @Test
    public void testThreedayVerify() throws ParseException {
        DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String memberId = "ff808181771367f101771453d1da6324";
        Calendar cal = Calendar.getInstance();
        cal.set(2021, 3, 1, 00, 00, 00); //Year, month, day of month, hours, minutes and seconds
        Date now = cal.getTime();
        Date[] previousMonthDates = DateUtil.getFirstAndLastDateOfMonth(now);
        Date dateFrom = previousMonthDates[0];
        Date dateTo = previousMonthDates[1];
        LinkedHashMap startDuration = liveStreamService.getDatesOfLiveStreamChannel(memberId,dateFrom,dateTo);
        long duration = 0l;
        int noOfDay = 0;
        long initDuration = 0l;
        int initDay = 0;
        List<Date> dateList = new ArrayList<>();
        Set<String> keys = startDuration.keySet();
        for(String k:keys){
            long channelduration = Long.parseLong(startDuration.get(k).toString());

            if(channelduration>3600)
            {
                dateList.add(dateFormat.parse(k));
                noOfDay ++;
                if(channelduration<10800) {
                    duration += channelduration;
                }else
                {
                    duration += 10800;
                }
            }
            initDuration += channelduration;
            initDay++;
        }

        ImmutableTriple<Boolean,String,String> triple = liveStreamService.doVerifyThreeDaysChecking(dateList, dateFrom, dateTo);
        boolean threedays = triple.left;

        if(threedays)
        {
            String incomeRemark = "From : "+triple.middle+" to "+triple.right+" did not open live. ";
            System.out.println(incomeRemark);
        }
    }


    @Ignore
    @Test
    public void testTotalExperienceLogic() {
        BigDecimal memberExp = new BigDecimal("42000");
        RankConfig rankConfig = rankService.findRankByExperience(memberExp);
        RankConfig previousRankConfig = rankService.findPreviousRankByExperience(memberExp);
        BigDecimal requiredExp = BigDecimal.ZERO;
        BigDecimal nextRankExp = BigDecimal.ZERO;
        int nextLevel = 0;
        if (rankConfig == null) {
            rankConfig = rankService.findHighestRank();
        }
        int currentRank = 0;
        BigDecimal totalExp = memberExp;
        log.debug("totalExp :" + totalExp);
        if (previousRankConfig != null) {
            totalExp = totalExp.subtract(previousRankConfig.getTotalExp());
            currentRank = previousRankConfig.getRankTo();
        }
        for (int i = rankConfig.getRankFrom(); i <= rankConfig.getRankTo(); i++) {
            log.debug("LOOP :" + i + ";EXP:" + totalExp);
            totalExp = totalExp.subtract(rankConfig.getPerExp());
            if (BDUtil.geZero(totalExp)) {
                currentRank = i;
            } else {
                requiredExp = totalExp.multiply(new BigDecimal("-1"));
                break;
            }
        }
        nextRankExp = rankService.getExperienceByRank(currentRank + 1);
        nextLevel = currentRank + 1;
        if (BDUtil.leZero(nextRankExp)) {
            nextLevel = currentRank;
        }
        log.debug("RANK :" + currentRank + "; next level:" + nextLevel + "; required:" + requiredExp);
    }

}
