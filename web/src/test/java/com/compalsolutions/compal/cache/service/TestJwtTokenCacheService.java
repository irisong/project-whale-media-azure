package com.compalsolutions.compal.cache.service;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.compalsolutions.compal.application.Application;
import com.compalsolutions.compal.security.vo.JwtToken;

public class TestJwtTokenCacheService {
    private static final Log log = LogFactory.getLog(TestJwtTokenCacheService.class);

    private JwtTokenCacheService jwtTokenCacheService;

    @Before
    public void init() {
        jwtTokenCacheService = Application.lookupBean(JwtTokenCacheService.BEAN_NAME, JwtTokenCacheService.class);
    }

    @Ignore
    @Test
    public void testCache() {
        String jwtId = "47Er68CG2poZMF_3XtN7sw";
        log.debug("Get token from cache");
        JwtToken jwtToken = jwtTokenCacheService.getJwtToken(jwtId);

        log.debug("2nd time: Get token from cache");
        JwtToken jwtToken2 = jwtTokenCacheService.getJwtToken(jwtId);

        Assert.assertTrue("jwtToken & jwtToken2 is not same instance ", jwtToken == jwtToken2);

        log.debug("evict token");
        jwtTokenCacheService.evictJwtToken(jwtId);

        log.debug("3rd time: Get token from cache");
        JwtToken jwtToken3 = jwtTokenCacheService.getJwtToken(jwtId);

        Assert.assertFalse("jwtToken & jwtToken3 is same instance", jwtToken != jwtToken3);
    }
}
