package com.compalsolutions.compal.wallet.service;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.compalsolutions.compal.application.Application;

public class TestTempPoint {
    private WalletService walletService;
    private TempPointService tempPointService;

    @Before
    public void init() {
        walletService = Application.lookupBean(WalletService.class);
        tempPointService = Application.lookupBean(TempPointService.class);
    }

    @Ignore
    @Test
    public void testInsert() {
        tempPointService.doProcessConvertTempPoint();
    }
}
