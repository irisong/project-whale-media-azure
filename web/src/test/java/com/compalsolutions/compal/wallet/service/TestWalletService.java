package com.compalsolutions.compal.wallet.service;

import java.math.BigDecimal;

import org.junit.Before;

import com.compalsolutions.compal.application.Application;

public class TestWalletService {
    private WalletService walletService;

    @Before
    public void init() {
        walletService = Application.lookupBean(WalletService.class);
    }

    // @Test
    public void testDoProcessExchangeLockIfHaveAny() {
        String memberId = "402880be6bb5f16e016bb5f214dc0006";

        int walletType = 10;

        walletService.doProcessExchangeLockIfHaveAny(memberId, walletType, new BigDecimal("4.71740"));
    }
}
