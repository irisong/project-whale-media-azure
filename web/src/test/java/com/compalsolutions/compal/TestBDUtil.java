package com.compalsolutions.compal;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Test;

import java.math.BigDecimal;

public class TestBDUtil {
    private static final Log log = LogFactory.getLog(TestBDUtil.class);

    @Test
    public void testRoundDown2dp() {
        BigDecimal amount = new BigDecimal("7.556");

        log.debug(amount);
        BigDecimal roundDown = BDUtil.roundDown2dp(amount);
        log.debug(roundDown);
    }
}
