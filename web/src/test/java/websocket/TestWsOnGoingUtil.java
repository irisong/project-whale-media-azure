package websocket;

import com.compalsolutions.compal.live.streaming.dto.LiveStreamDto;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.compalsolutions.compal.websocket.vo.WsMessage;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class TestWsOnGoingUtil {
    @Before
    public void init() {
    }

    @Ignore
    @Test
    public void testVJPointBundleBroadcast() {
        String channelId = "402880f864ee28300164ee53b91a00b9";
        WsMessage wsMessage = new WsMessage(true);
        wsMessage.setMessageId(channelId);
        LiveStreamDto liveStreamDto = new LiveStreamDto();
        liveStreamDto.setChannelId(channelId);
        liveStreamDto.setPoint(BigDecimal.TEN);
        liveStreamDto.setView(3L);
        liveStreamDto.setMemberList(null);

        WsOutGoingUtil.broadcastMessage_processVJPoint(liveStreamDto);
    }

    @Ignore
    @Test
    public void testVJPointPersonalBundleBroadcast() {
        LiveStreamWebsocket liveStreamWebsocket = new LiveStreamWebsocket();
        liveStreamWebsocket.sendLatestChannelInfo("test", BigDecimal.ONE, 2, null);
    }

}
