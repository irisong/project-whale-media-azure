SmartAdmin version 1.8.7.5
=======================================
customisation
smartadmin-production-plugins.min.css
- comment out tree css because it conflict with jeasyui tree css

smartadmin-production.min.css
1. comment following css, because jquery-ui datepicker unable to show 'Prev' & 'Next' icon.
.ui-datepicker-next>:first-child,.ui-datepicker-prev>:first-child
.ui-datepicker .ui-datepicker-next span,.ui-datepicker .ui-datepicker-prev span

INSPINIA version 2.7
=======================================
no customisation for time being

BIGBAG version 1.2
=======================================
no customisation for time being